<?php 
    error_reporting(0);
    session_start();
    require_once('php/config.php');
    require_once("php/services/ServiceAdministrativo.php");

    $produccion = PRODUCTION_SERVER;

    $estado = $_SESSION["estado"];
    $tipo = $_SESSION["tipo"];

 ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SISTEMA INTEGRADO UPC</title>

    <!-- Bootstrap Core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="bower_components/bootstrap/dist/css/bootstrap-select.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->


    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- ESTILO PERSONALIZADO UPC -->
    <link rel="stylesheet" type="text/css" href="dist/css/style_upc.css">

    <!-- ICONO DE PAGINA -->
    <link rel="icon" type="image/png" href="img/favicon.ico" />

    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="dist/css/kendo.common.min.css" rel="stylesheet"> <!-- CSS PARA QUE FUNCIONE EL GRAFICO DE KENDO-->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->



    <?php if($estado != 1 or $tipo != 10){ ?>

        <script type="text/javascript">

        window.open("login.php?r=1","_top");

        </script>

    <?php } ?>

    <script type="text/javascript">

        var produccion = <?php echo $produccion?1:0 ?>;

    </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
            <?php
                include("secciones/menu_principal.php");
            ?>
            <!-- /.navbar-top-links -->




            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">

                                        <h4><label id="usuario"></label><br>
                                            <small><span id="cargo"></span></small>
                                        </h4>

                            </div>
                            <!-- /input-group -->
                        </li>
                    </ul>
                    <ul class="nav" id="lista_opciones"> <!-- id="side-menu" -->


                    <?php if($estado == 1 or $tipo == 10){

                    $service = new ServiceAdministrativo();
                    $permisos = $_SESSION["permisos"];
                    $data = $service->getMenu($permisos);
                    $registros = count($data);

                    for($i=0 ; $i < $registros ; $i++){
                        echo '<li class=""><a href="#'.$data[$i]->enlace.'" data-toggle="tab"><i class="fa fa-list-alt fa-fw"></i> '.$data[$i]->titulo.' </a></li>';
                     }

                    }?>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">


        <!-- Tab panes -->

            <div class="tab-content contenedor">
                    <BR>


                <!-- PAGINA PRINCIPAL DE INICIO -->
                <div class="tab-pane active" id="principal">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            BIENVENIDO AL SISTEMA INTEGRADO UPC
                        </div>
                        <div class="panel-body titulo">



                        </div>
                    </div>

                </div>


                <?php if($estado == 1 or $tipo == 10){

                    for($i=0 ; $i < $registros ; $i++){
                      include("secciones/modulo_".$data[$i]->enlace.".php");
                    }

                }?>




            </div>
            <!-- /#page-wrapper -->

        </div>
    <!-- /#wrapper -->




        <div class="modal fade" id="modalDesconectarse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">SISTEMA INTEGRAL UPC</h4>
              </div>
              <div class="modal-body">
                    ¿SEGURO DE CERRAR SESION?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                <a href="login.php?r=1" type="button" class="btn btn-primary">ACEPTAR</a>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="modalGuardarDatos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">SISTEMA INTEGRAL UPC</h4>
              </div>
              <div class="modal-body">
                    ¿SEGURO DE GUARDAR LOS DATOS DEL FORMULARIO?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                <button id="enviarDatosEnsayoParte1" type="button" class="btn btn-primary" data-dismiss="modal">ACEPTAR</button>
              </div>
            </div>
          </div>
        </div>


    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

    <script type="text/javascript" src="js/vendor/S.js"></script>
    <script type="text/javascript" src="js/vendor/service.js"></script>
    <script type="text/javascript" src="js/vendor/jquery.json-2.4.min.js"></script>

    <script type="text/javascript" src="js/vendor/kendo/kendo.all.min.js"></script>
    <script type="text/javascript" src="js/vendor/kendo/console.js"></script>

    <script type="text/javascript" src="js/main.js"></script>

    
    <script type="text/javascript" src="js/ControlAdministrativo.js"></script>

    <?php if($estado == 1 or $tipo == 10){

        $data = $service->getControles($permisos);
        $registros = count($data); //[0]->idPerfil;

        for($i=0 ; $i < $registros ; $i++){
           echo ('<script type="text/javascript" src="js/Control_'.$data[$i]->control.'.js"></script>');
        }

    }?>

</body>

</html>

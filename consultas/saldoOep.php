<?php

    session_start();
    require_once('../php/config.php');
    require_once('../php/services/ServicePresupuestoSolicitud.php');
   

    $service = new ServicePresupuestoSolicitud();

    $area = $_GET["areaid"];
    $string = $_GET["stringid"];
    $categoria = $_GET["categoriaid"];
    $proyecto = $_GET["proyectoid"];
    $comentario = $_GET["comentarioid"];
    $anio = $_GET["year"];
    $mes = $_GET["mes"];



    $objeto = new stdClass();
    $objeto->area = $area;
    $objeto->string = $string;
    $objeto->categoria = $categoria;
    $objeto->proyecto = $proyecto;
    $objeto->comentario = $comentario;
    $objeto->anio = $anio;
    $objeto->mes = $mes;



    $data = $service->consultarSolicitudSaldoPresupuesto($objeto);
    $resultado = (array)$data;
    $resultado['status'] = '200';
    //foreach ($resultado as $valor) {
    //    echo $valor."-";
    //}
      header('Content-Type: application/json');
     header('X-PHP-Response-Code: 200', true);
    echo json_encode($resultado);
    exit();

?>
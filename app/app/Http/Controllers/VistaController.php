<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use App\Models\Modulo;
class VistaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function ver(){

        $arrayModulos = Modulo::all();
        $data = ["url"=>Config::get("app.url"),"arrayModulos"=>$arrayModulos];
        return view("sistema",$data);
    }
}

<!DOCTYPE html>
<html>
<head>
	<title>
		{{config("app.name")}}
	</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="stylesheet" href="{{$url}}/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{$url}}/css/style.css">


	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', "{{config('app.code_ga')}}" , 'auto');
		ga('send', 'pageview');
	</script>
</head>
<body ng-app="presupuesto">
	<div id="appPresupuesto"  ng-controller="vistaController">
		@include("components.menu")
        @yield('content')
    </div>    
    <script type="text/javascript" src="{{$url}}/js/vendor/jquery.min.js"></script>
    <script type="text/javascript" src="{{$url}}/js/vendor/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{$url}}/js/vendor/angular.min.js"></script>
    <script type="text/javascript" src="{{$url}}/js/app.js"></script>
    <script type="text/javascript" src="{{$url}}/js/controllers/vista.js"></script>
    <script type="text/javascript" src="{{$url}}/js/controllers/sections/formatos.js"></script>
    <script type="text/javascript" src="{{$url}}/js/controllers/sections/ingreso.js"></script>
    <script type="text/javascript" src="{{$url}}/js/controllers/sections/solicitudes.js"></script>
    
</body>
</html>
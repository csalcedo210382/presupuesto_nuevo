<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel Users app.blade.php language lines
    |--------------------------------------------------------------------------
    */

    'nav' => [
        'toggle-nav-alt'    => 'Navegación',
        'login'             => 'Ingresar',
        'register'          => 'Registrase',
        'users'             => 'Usuarios',
        'logout'            => 'Salir',
    ],

];

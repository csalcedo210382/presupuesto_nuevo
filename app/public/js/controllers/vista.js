appAngular.controller("vistaController", function ($scope, $rootScope, $timeout) {
// app.controller("viewsController", function($scope,$rootScope) {

    //$scope.pageCurrent = $rootScope.page;
    var arrayBan = ["intro","juego"];
    var arrayPages = [];
    var isAtras = true;
    $rootScope.changePage = function (vista, data) {
        $rootScope.pageCurrent = vista;
        //$rootScope.page = vista;
        //$scope.pageCurrent = vista;
        $rootScope.$broadcast($rootScope.pageCurrent, data);
        try {
            $scope.$digest();
        } catch (e) {
            //console.log("error",e);
        }
        isAtras = false;
        var digito = vista.substr(0, 1);
        digito = digito.toUpperCase();
        var view = "vista" + digito + vista.substr(1);
        $(".pagina").hide();
        var vista = $("#"+view);
        vista.fadeIn();
        setTimeout(function(){
            vista.css({opacity:1});    
        },100);
        location.hash = $rootScope.pageCurrent;
        agregarPagina($rootScope.pageCurrent);
        var array = $("#" + view + " .animate");
        var i, element;
        for (i = 0; i < array.length; i++) {
            element = $(array[i]);
            setTimeout(function (ele) {
                ele.addClass("show");
            }, 600+ i * 500, element);
        }
        isAtras = true;
        saveEvent($rootScope.pageCurrent,"vista");
    }   
    function agregarPagina(page){
        for (var i = 0; i < arrayBan.length; i++) {
            if(arrayBan[i] == page){
                return;
            }
        }
        arrayPages.push(page);
    }
    setTimeout(navegacion,10);
    function navegacion(){
        var hash = getHash();        
        if(hash != ""){
            $rootScope.changePage(hash);
        }
        else{
            $rootScope.changePage("ingreso");   
        }
    }
    window.addEventListener('popstate', function(event) {
        var hash = getHash();
        if(hash != ""){
            $rootScope.changePage(hash);
        }
    }, false);
    function getHash(){
        var hash = location.hash;        
        if(hash.length>1){
            hash = hash.split("#")[1];
            return hash;
        }
        return "";
    }
    $rootScope.showModal = function (name,data) {
        //console.log("abrir modal", name);
        $rootScope.$broadcast(name,data);
        saveEvent(name,"modal");
    }
    $("#appPresupuesto").fadeIn();    
    
});
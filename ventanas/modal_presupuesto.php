    <div class="modal fade" id="modalBuscarStringPresupuesto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">BUSQUEDA DE STRING</h4>
          </div>
          <div class="modal-body">

                        <div class="row">
                          <div class="col-md-2 texto titulo">STRING</div>
                          <div class="col-md-6"><input id="campoBuscarStringPresupuesto" type="text" class="form-control input-sm" placeholder="CODIGO O STRING"></div>
                          <div class="col-md-4"><button id="botonBuscarStringPresupuesto" type="button" class="btn btn-primary btn-sm btn-block">BUSCAR</button></div>
                        </div>

                        <table id="tablaListaStringEncontradaPresupuesto" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>CODIGO</th>
                                    <th>STRING</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr >
                                    <td>NO</td>
                                    <td>NO</td>
                                    <td>NO</td>
                                </tr>
                            </tbody>
                        </table>
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
          </div>
        </div>
      </div>
    </div> 


    <div class="modal fade" id="modalBuscarSubCategoriaPresupuesto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">BUSQUEDA DE SUB CATEGORIA</h4>
          </div>
          <div class="modal-body">

                        <div class="row">
                          <div class="col-md-2 texto titulo">S.CATEGORIA</div>
                          <div class="col-md-6"><input id="campoBuscarSubCategoriaPresupuesto" type="text" class="form-control input-sm" placeholder="CODIGO O SUB CATEGORIA"></div>
                          <div class="col-md-4"><button id="botonBuscarSubCategoriaPresupuesto" type="button" class="btn btn-primary btn-sm btn-block">BUSCAR</button></div>
                        </div>

                        <table id="tablaListaSubCategoriaEncontradaPresupuesto" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>CODIGO</th>
                                    <th>NODO</th>
                                    <th>SUB CATEGORIA</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr >
                                    <td>NO</td>
                                    <td>NO</td>
                                    <td>NO</td>
                                    <td>NO</td>
                                </tr>
                            </tbody>
                        </table>
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modalBuscarNodoPresupuesto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">BUSQUEDA DE CUENTA NODO</h4>
          </div>
          <div class="modal-body">

                        <div class="row">
                          <div class="col-md-2 texto titulo">NODO</div>
                          <div class="col-md-6"><input id="campoBuscarNodoPresupuesto" type="text" class="form-control input-sm" placeholder="CUENTA NODO"></div>
                          <div class="col-md-4"><button id="botonBuscarNodoPresupuesto" type="button" class="btn btn-primary btn-sm btn-block">BUSCAR</button></div>
                        </div>

                        <table id="tablaListaNodoEncontradaPresupuesto" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>NODO</th>
                                    <th>DESCRIPCION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr >
                                    <td>NO</td>
                                    <td>NO</td>
                                    <td>NO</td>
                                </tr>
                            </tbody>
                        </table>
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="modalBuscarTipoPresupuesto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">BUSQUEDA DE TIPO</h4>
          </div>
          <div class="modal-body">

                        <div class="row">
                          <div class="col-md-2 texto titulo">TIPO</div>
                          <div class="col-md-6"><input id="campoBuscarTipoPresupuesto" type="text" class="form-control input-sm" placeholder="TIPO"></div>
                          <div class="col-md-4"><button id="botonBuscarTipoPresupuesto" type="button" class="btn btn-primary btn-sm btn-block">BUSCAR</button></div>
                        </div>

                        <table id="tablaListaTipoEncontradaPresupuesto" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>TIPO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr >
                                    <td>NO</td>
                                    <td>NO</td>
                                </tr>
                            </tbody>
                        </table>
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modalBuscarSubTipoPresupuesto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">BUSQUEDA DE SUB TIPO</h4>
          </div>
          <div class="modal-body">

                        <div class="row">
                          <div class="col-md-2 texto titulo">SUB TIPO</div>
                          <div class="col-md-6"><input id="campoBuscarSubTipoPresupuesto" type="text" class="form-control input-sm" placeholder="SUB TIPO"></div>
                          <div class="col-md-4"><button id="botonBuscarSubTipoPresupuesto" type="button" class="btn btn-primary btn-sm btn-block">BUSCAR</button></div>
                        </div>

                        <table id="tablaListaSubTipoEncontradaPresupuesto" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>SUB TIPO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr >
                                    <td>NO</td>
                                    <td>NO</td>
                                </tr>
                            </tbody>
                        </table>
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modalBuscarItemPresupuesto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">BUSQUEDA DE ITEM</h4>
          </div>
          <div class="modal-body">

                        <div class="row">
                          <div class="col-md-2 texto titulo">ITEM</div>
                          <div class="col-md-6"><input id="campoBuscarItemPresupuesto" type="text" class="form-control input-sm" placeholder="ITEM"></div>
                          <div class="col-md-4"><button id="botonBuscarItemPresupuesto" type="button" class="btn btn-primary btn-sm btn-block">BUSCAR</button></div>
                        </div>

                        <table id="tablaListaItemEncontradaPresupuesto" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ITEM</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr >
                                    <td>NO</td>
                                    <td>NO</td>
                                </tr>
                            </tbody>
                        </table>
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
          </div>
        </div>
      </div>
    </div>



















    <div class="modal fade" id="modalFiltroPersonalizadoPresupuesto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">CARGA MASIVA DE COORDINADORES DE CURSO</h4>
          </div>
          <div class="modal-body">

            <div class="row contenedor">
              <div class="col-md-4">
                <label class="btn btn-primary btn-sm btn-file btn-block">
                  SELECCIONE ARCHIVO<input type="file" name="xlfile" id="archivoCoordinadorCurso" style="display: none;">
                </label>
              </div>
              <div class="col-md-4"></div>
              <div class="col-md-4"><button id="botonCargaMasivaCoordinadoresCurso" type="button" class="btn btn-success btn-sm btn-block"><span class="glyphicon glyphicon-glyphicon-cog" aria-hidden="true"></span> GUARDAR REGISTROS</button></div>
            </div>
            <div class="row contenedor">
              <div class="col-md-4"><p id="archivoSeleccionadoCoordinadorCurso">...</p></div>
              <div class="col-md-4"></div>
              <div class="col-md-4"><div id="barraCargaMasivaCoordinadorCurso" class="myProgress"></div>
              </div>
            </div>

            <div class="row contenedor">
              <div class="col-md-12">
                <table id="tablaListaCargaMasivaCoordinadorCurso" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>DOCENTE</th>
                            <th>ASIGNATURA</th>
                            <th>PERIODO</th>
                            <th>MODALIDAD</th>
                            <th>SEDE</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
              </div>
            </div>


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="modalAprobarRechazarRegistro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: scroll;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 class="modal-title" id="myModalLabel">FORMULARIO DE SEGUIMIENTO</h5>
          </div>
          <div class="modal-body">
              <div class="row informativa">
                  <div class="col-md-2 texto negrita">ESTADO</div>
                  <div class="col-md-8 texto negrita estadoAprobacionSeguimiento"></div>
              </div>
              <div class="row informativa">
                  <div class="col-md-2 texto negrita">FECHA</div>
                  <div class="col-md-8 texto negrita fechaAprobacionSeguimiento"></div>
              </div>
              <div class="row informativa">
                  <div class="col-md-2 texto negrita">USUARIO</div>
                  <div class="col-md-8 texto negrita usuarioAprobacionSeguimiento"></div>
              </div>
              <div class="row motivoRechazo">
                  <div class="col-md-2 texto negrita">MOTIVO</div>
                  <div class="col-md-8 texto negrita motivoAprobacionSeguimiento"></div>
              </div>
              <div class="row efectiva">
                  <div class="col-md-1"></div>
                  <div class="col-md-4"><button id="aprobarFormularioSeguimiento" type="button" class="btn btn-default btn-lg botonSeguimiento">APROBAR</button></div>
                  <div class="col-md-2"></div>
                  <div class="col-md-4"><button id="rechazarFormularioSeguimiento" type="button" class="btn btn-default btn-lg botonSeguimiento">RECHAZAR</button></div>
                  <div class="col-md-1"></div>
              </div>
              <br>
              <div class="row efectiva_motivo">
                  <div class="col-md-1"></div>
                  <div class="col-md-1 texto negrita">MOTIVO</div>
                  <div class="col-md-9"><textarea id="motivoFormularioSeguimiento" class="form-control" rows="1"></textarea></div>
                  <div class="col-md-1"></div>
              </div>

          </div>
          <div class="modal-footer">

          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="modalAprobarRechazarRegistroAprobado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 class="modal-title" id="myModalLabel">MODIFICAR ESTADO DE SEGUIMIENTO</h5>
          </div>
          <div class="modal-body">
              <div class="row efectiva">
                  <div class="col-md-1"></div>
                  <div class="col-md-4"><button id="aprobarFormularioSeguimientoAprobado" type="button" class="btn btn-default btn-lg botonSeguimiento" disabled>APROBAR</button></div>
                  <div class="col-md-2"></div>
                  <div class="col-md-4"><button id="rechazarFormularioSeguimientoAprobado" type="button" class="btn btn-default btn-lg botonSeguimiento">RECHAZAR</button></div>
                  <div class="col-md-1"></div>
              </div>
              <br>
              <div class="row efectiva_motivo">
                  <div class="col-md-1"></div>
                  <div class="col-md-1 texto negrita">MOTIVO</div>
                  <div class="col-md-9"><textarea id="motivoFormularioSeguimientoAprobado" class="form-control" rows="1"></textarea></div>
                  <div class="col-md-1"></div>
              </div>

          </div>
          <div class="modal-footer">

          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="modalConsultarSaldoModuloAprobacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 class="modal-title" id="myModalLabel">NO CUENTA CON SALDO SUFICIENTE PARA APROBAR ESTE GASTO</h5>
          </div>
          <div class="modal-body">
              <div class="row efectiva">
                  <div class="col-md-1 negrita texto derecha">MONTO</div>
                  <div class="col-md-4 negrita texto centro montoAprobacion"></div>
              </div>
              <br>
              <div class="row efectiva_motivo">
                  <div class="col-md-4 negrita centro mAprobacion">SALDO MES</div>
                  <div class="col-md-4 negrita centro qAprobacion">SALDO Q</div>
                  <div class="col-md-4 negrita centro">SALDO ANUAL</div>
              </div>
              <div class="row efectiva_motivo">
                  <div class="col-md-4 centro saldoMAprobacion">0.00</div>
                  <div class="col-md-4 centro saldoQAprobacion">0.00</div>
                  <div class="col-md-4 centro saldoYAprobacion">0.00</div>
              </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
          </div>
        </div>
      </div>
    </div>
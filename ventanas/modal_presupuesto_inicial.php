    <div class="modal fade" id="modalLimpiarPresupuesto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">SISTEMA INTEGRADO UPC</h4>
          </div>
          <div class="modal-body">
                ¿SEGURO DE LIMPIAR LOS VALORES EN LAS CELDAS DE DINERO?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
            <button id="limpiar_meses_presupuesto" type="button" class="btn btn-primary btn-sm" data-dismiss="modal">ACEPTAR</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modalBuscarCategoria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">BUSQUEDA DE CATEGORIAS</h4>
          </div>
          <div class="modal-body">

                        <div class="row">
                          <div class="col-md-2 texto titulo">CATEGORIA</div>
                          <div class="col-md-6"><input id="campoBuscarCategoria" type="text" class="form-control input-sm" placeholder="CODIGO O CATEGORIA"></div>
                          <div class="col-md-4"><button id="botonBuscarCategoria" type="button" class="btn btn-default btn-sm texto">BUSCAR</button></div>
                        </div>

                        <table id="tablaListaCategoriaEncontrada" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>CODIGO</th>
                                    <th>CATEGORIA</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr >
                                    <td>NO</td>
                                    <td>NO</td>
                                </tr>
                            </tbody>
                        </table>
                
          </div>
          <div class="modal-footer">
            <button id="botonActualizarBusquedaCategoria" type="button" class="btn btn-default btn-sm" data-dismiss="modal">ACTUALIZAR</button>
          </div>
        </div>
      </div>
    </div> 


    <div class="modal fade" id="modalBuscarString" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">BUSQUEDA DE STRING</h4>
          </div>
          <div class="modal-body">

                        <div class="row">
                          <div class="col-md-2 texto titulo">STRING</div>
                          <div class="col-md-6"><input id="campoBuscarString" type="text" class="form-control input-sm" placeholder="CODIGO O STRING"></div>
                          <div class="col-md-4"><button id="botonBuscarString" type="button" class="btn btn-default btn-sm texto">BUSCAR</button></div>
                        </div>

                        <table id="tablaListaStringEncontrada" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>CODIGO</th>
                                    <th>STRING</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr >
                                    <td>NO</td>
                                    <td>NO</td>
                                </tr>
                            </tbody>
                        </table>
                
          </div>
          <div class="modal-footer">
            <button id="botonActualizarBusquedaString" type="button" class="btn btn-default btn-sm" data-dismiss="modal">ACTUALIZAR</button>
          </div>
        </div>
      </div>
    </div> 

    <div class="modal fade" id="modalGuardarPresupuesto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">SISTEMA INTEGRAL UPC</h4>
          </div>
          <div class="modal-body">
                ¿SEGURO DE GUARDAR Y/O ACTUALIZAR EL REGISTRO DE PRESUPUESTO?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
            <button id="guardaRegistroPresupuesto" type="button" class="btn btn-primary btn-sm" data-dismiss="modal">ACEPTAR</button>
          </div>
        </div>
      </div>
    </div>    

    <div class="modal fade" id="modalNuevoPresupuesto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">SISTEMA INTEGRAL UPC</h4>
          </div>
          <div class="modal-body">
                ¿SEGURO DE INICIAR EL REGISTRO DE PRESUPUESTO?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
            <button id="nuevo_datos_presupuesto" type="button" class="btn btn-primary btn-sm" data-dismiss="modal">ACEPTAR</button>
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="modalCancelarPresupuesto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">SISTEMA INTEGRAL UPC</h4>
          </div>
          <div class="modal-body">
                ¿SEGURO DE CANCELAR EL REGISTRO DE PRESUPUESTO?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
            <button id="cancelar_datos_presupuesto" type="button" class="btn btn-primary btn-sm" data-dismiss="modal">ACEPTAR</button>
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="modalDetallePresupuesto" tabindex="-1" role="dialog" aria-labelledby="encabezadoModal" aria-hidden="true">
      <div class="modal-dialog" id="mdtDetallePresupuesto">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 class="modal-title negrita" id="encabezadoModal">DETALLE DE PRESUPUESTO</h5>
          </div>
          <div class="modal-body">

            <div class="row">
              <div class="col-md-2 texto negrita">VARIABLES </div>
              <div class="col-md-10 texto" id="detallePresupuesto"></div>
            </div> 

            <div class="row">
              <div class="col-md-2 texto negrita">BUSCAR </div>
              <div class="col-md-10"><input type="text" id="buscarDetallePresupuesto" placeholder="BUSCAR DATO" class="form-control input-sm"></div>
            </div>

            <br>

            <div class="row">
              <div class="col-md-12">
                                            <table id="tablaListaDetalledePresupuesto" class="table table-bordered table-hover reportePresupuesto">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>-</th>
                                                        <th class="qd1mes" style="display: none;">ENERO</th>
                                                        <th class="qd2mes" style="display: none;">FEBRERO</th>
                                                        <th class="qd3mes" style="display: none;">MARZO</th>
                                                        <th class="qd1" style="display: none;">Q1</th>
                                                        <th class="qd4mes" style="display: none;">ABRIL</th>
                                                        <th class="qd5mes" style="display: none;">MAYO</th>
                                                        <th class="qd6mes" style="display: none;">JUNIO</th>
                                                        <th class="qd2" style="display: none;">Q2</th>
                                                        <th class="qd7mes" style="display: none;">JULIO</th>
                                                        <th class="qd8mes" style="display: none;">AGOSTO</th>
                                                        <th class="qd9mes" style="display: none;">SEPTIEMBRE</th>
                                                        <th class="qd3" style="display: none;">Q3</th>
                                                        <th class="qd10mes" style="display: none;">OCTUBRE</th>
                                                        <th class="qd11mes" style="display: none;">NOVIEMBRE</th>
                                                        <th class="qd12mes" style="display: none;">DICIEMBRE</th>
                                                        <th class="qd4" style="display: none;">Q4</th>
                                                        <th>TOTAL</th>
                                                        <!--<th>OPCIONES</th>-->
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                </tbody>

                                                <tfoot>
                                                </tfoot>
                                            </table>
              </div>
            </div>     


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
            <button id="cancelar_datos_presupuesto" type="button" class="btn btn-primary btn-sm" data-dismiss="modal">ACEPTAR</button>
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="modalDetallePresupuestoTransferencia" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 class="modal-title negrita">DETALLE DE PRESUPUESTO</h5>
          </div>
          <div class="modal-body">

            <div class="row">
              <div class="col-md-12 texto negrita negrita cuentaDetallePresupuestoTransferencia"></div>
            </div>

            <div class="row">
              <div class="col-md-2 texto negrita">SALDO INICIAL</div>
              <div class="col-md-4">
                <input type="text" class="form-control input-sm negrita saldoInicialDetallePresupuestoTransferencia" disabled>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-12 texto negrita">DETALLE DE TRANSFERENCIAS</div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <table id="tablaDetallePresupuestoTransferencia" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>TIPO</th>
                      <th>DETALLE DE MOVIMIENTO</th>
                      <th>MES</th>
                      <th>MONTO</th>
                      </tr>
                  </thead>
                  <tbody>
                    <tr >
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2 texto negrita">SALDO FINAL </div>
              <div class="col-md-4">
                <input type="text" class="form-control input-sm negrita saldoFinalDetallePresupuestoTransferencia" disabled>
              </div>
            </div>


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
          </div>
        </div>
      </div>
    </div>





    <div class="modal fade" id="modalReportePresupuesto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">REPORTE PRESUPUESTO</h4>
          </div>
          <div class="modal-body">

              <div class="row">
                <div class="col-md-12 centro">
                  <button id="reporteGeneralPresupuesto" type="button" class="btn btn-primary btn-sm">REPORTE GENERAL</button>
                </div>
              </div>
              <br><br><br>
              <div class="row">
                <div class="col-md-8 centro texto">
                  <div class="checkbox">
                    <input id="chkpnuevoInicialPresupuesto" type="checkbox">
                    <label for="chkpnuevoInicialPresupuesto">REPORTE CON AMPLIACIÓN DE PRESUPUESTO</label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-2 centro texto">
                  AREA
                </div>
                <div class="col-md-4">
                  <select id="areaReportePresupuesto" class="form-control input-sm">
                  </select>
                </div>
                <div class="col-md-4 centro">
                  <button id="reporteAreaPresupuesto" type="button" class="btn btn-success btn-block btn-sm">GASTO DETALLADO</button>
                </div>
                <div class="col-md-2 centro">
                  <button id="reporteAreaPresupuestoSaldos" type="button" class="btn btn-success btn-block btn-sm">SALDOS</button>
                </div>
              </div>
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
          </div>
        </div>
      </div>
    </div> 






    <div class="modal fade" id="modalResultadoAreaReporteProyecto" tabindex="-1" data-focus-on="input:first" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal_largo">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel">REPORTE DE GASTO DETALLADO POR ÁREA</h4>
            </div>
            <div class="modal-body">

                    <div class="row">
                      <div class="col-md-12">
                        <button id="excelResultadoAreaReportePresupuesto" type="button" class="btn btn-success btn-sm">EXPORTAR EXCEL</button>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <table id="tablaResultadoAreaReportePresupuesto" class="table table-bordered table-hover tablaregistros">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>CARRERA/ AGENCIA ACREDITADORA</th>
                                    <th>PROYECTO</th>
                                    <th>CUENTA CONTABLE</th>
                                    <th>CONCEPTO</th>
                                    <th>COMENTARIO</th>
                                    <th>FECHA</th>
                                    <th>PROVEEDOR</th>
                                    <th>PRESUPUESTO ANUAL</th>
                                    <th>GASTOS A LA FECHA</th>
                                    <th>SALDOS A LA FECHA</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                      </div>
                    </div>


            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="modalResultadoAreaReporteProyectoSaldos" tabindex="-1" data-focus-on="input:first" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal_largo">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel">REPORTE DE SALDOS POR ÁREA</h4>
            </div>
            <div class="modal-body">

                    <div class="row">
                      <div class="col-md-12">
                        <button id="excelResultadoAreaReportePresupuestoSaldos" type="button" class="btn btn-success btn-sm">EXPORTAR EXCEL</button>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <table id="tablaResultadoAreaReportePresupuestoSaldos" class="table table-bordered table-hover tablaregistros">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>PROYECTO</th>
                                    <th>CUENTA CONTABLE</th>
                                    <th>CONCEPTO</th>
                                    <th>P Q1</th>
                                    <th>G Q1</th>
                                    <th>S Q1</th>
                                    <th>P Q2</th>
                                    <th>G Q2</th>
                                    <th>S Q2</th>
                                    <th>P Q3</th>
                                    <th>G Q3</th>
                                    <th>S Q3</th>
                                    <th>P Q4</th>
                                    <th>G Q4</th>
                                    <th>S Q4</th>
                                    <th>PRESUPUESTO ANUAL</th>
                                    <th>GASTOS A LA FECHA</th>
                                    <th>SALDOS A LA FECHA</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                      </div>
                    </div>


            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- FORMULARIO SOLICITUD ( NUEVO - EDITAR - ELIMINAR ) -->
    <div class="modal fade" id="formularioSolicitudGastoPresupuesto" tabindex="-1" data-focus-on="input:first" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal_largo">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel">FORMULARIO SOLICITUD</h4>
            </div>
            <div class="modal-body">


                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">CÓDIGO S-</div>    
                                                            <div class="col-md-1 texto negrita" id="idPresupuestoSolicitudGastoPresupuesto">
                                                            </div>
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-1 texto negrita">ESTADO</div>
                                                            <div class="col-md-2">
                                                                <select id="estadoFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                                                                    <option value='INGRESADO'>INGRESADO</option>
                                                                    <option value='POR APROBAR'>POR APROBAR</option>
                                                                    <option value='RECHAZADO'>RECHAZADO</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-1 texto negrita">FECHA :</div>
                                                            <div class="col-md-2 negrita">
                                                                <div class='input-group date' id='datetimepickerFechaSP'>
                                                                    <input type='text' id="fechaFormularioSolicitudGastoPresupuesto" class="form-control input-sm"></input>
                                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">ÁREA</div>    
                                                            <div class="col-md-2">
                                                                <select id="areaFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">STRING</div>    
                                                            <div class="col-md-4">
                                                                <select id="stringFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                                                                    <option value=''>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">CUENTA GLOBAL</div>    
                                                            <div class="col-md-3">
                                                                <select id="categoriaFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                                                                    <option value=''>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">PROYECTO</div>    
                                                            <div class="col-md-5">
                                                                <select id="proyectoFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                                                                    <option value=''>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">DETALLE</div>    
                                                            <div class="col-md-5">
                                                                <select id="detalleFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                                                                    <option value='0'>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <!--<div class="col-md-2 texto "><spam id="saldoDetalleFormularioSolicitudGastoPresupuesto"></spam></div>-->
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">CARRERA</div>    
                                                            <div class="col-md-5">
                                                                <select id="carreraFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                                                                    <option value=''>---SELECCIONAR---</option>
                                                                    <option value='1'>CARRERA DEMO</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button id="nuevaCarreraPresupuesto" href="#modal_nueva_carrera_solicitud" type="button" class="btn btn-primary btn-sm btn-block" data-toggle="modal">NUEVA CARRERA</button>
                                                            </div>

                                                            <div class="col-md-3 centro">
                                                                <div class="checkbox">
                                                                    <input id="chkNuevoPresupuestoSolicitudPresupuesto" type="checkbox">
                                                                    <label for="chkNuevoPresupuestoSolicitudPresupuesto">MARCAR SI ES AMPLIACIÓN DE PRESUPUESTO</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row saldos"> 
                                                            <div class="col-md-3 texto negrita derecha"> SALDOS : </div>
                                                            <div class="col-md-3 texto negrita centro saldoSolicitudPresupuestoM">( JULIO ) S/. 0.00</div>
                                                            <div class="col-md-3 texto negrita centro saldoSolicitudPresupuestoQ">( Q3 ) S/. 0.00</div>
                                                            <div class="col-md-3 texto negrita centro saldoSolicitudPresupuestoT">( TOTAL ) S/. 0.00</div>
                                                            <!--<div class="col-md-5 texto negrita centro saldoSolicitudPresupuesto">SALDO PRESUPUESTO : S/. 0.00</div>-->
                                                        </div>

                                                        <br>
                                                        <div class="row ndocumento">
                                                            <div class="col-md-1 texto negrita">N° DOCUMENTO</div>    
                                                            <div class="col-md-2">
                                                                <input type="text" id="ndocumentoFormularioSolicitudGastoPresupuesto" name="buscar_monto" placeholder="N° DE DOCUMENTO" class="form-control input-sm">
                                                            </div>
                                                            <div class="col-md-1 texto negrita datetimepickerFechaFF">FECHA FACTURA</div>
                                                            <div class="col-md-2 datetimepickerFechaFF">
                                                                <div class='input-group date' id='datetimepickerFechaFF'>
                                                                    <input type='text' id="fechaFacturaFormularioSolicitudGastoPresupuesto" class="form-control input-sm"></input>
                                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 texto negrita derecha datetimepickerFechaFF">ASIGNAR GASTO A FORMATO</div>
                                                            <div class="col-md-2">
                                                                <select class="form-control input-sm formato datetimepickerFechaFF">
                                                                    <option value="">---SELECCIONAR---</option>
                                                                    <!--option value='PIN'>PEDIDO INTERNO</option-->
                                                                    <option value='OCA'>ORDEN DE COMPRA ABIERTA</option>
                                                                    <option value='PRO'>PROVISION</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <select class="form-control input-sm nformato datetimepickerFechaFF">
                                                                    <option value="">---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <!--<div class="col-md-5 texto negrita centro saldoSolicitudPresupuesto">SALDO PRESUPUESTO : S/. 0.00</div>-->
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">PROVEEDOR</div>    
                                                            <div class="col-md-5">
                                                                <select id="proveedorFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                                                                    <option value='0'>---SELECCIONAR---</option>
                                                                </select>                                                                
                                                                <button id="cargarPagoOepSolicitud" href="#modal_cargar_pago_oep_solicitud" type="button" class="btn btn-primary btn-sm btn-block" data-toggle="modal" style="display:none">REGISTRO PERSONAL DE APOYO - OEP</button>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button id="nuevoProveedorPresupuesto" href="#modal_nuevo_proveedor_solicitud" type="button" class="btn btn-primary btn-sm btn-block" data-toggle="modal">NUEVO PROVEEDOR</button>
                                                            </div>
                                                            <div class="col-md-2 texto negrita">
                                                                <p id="tinProveedor"></p>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row">
                                                            <!--<div class="col-md-1 texto negrita">ACTIVIDAD</div>    
                                                            <div class="col-md-4">
                                                                <input type="text" id="actividadFormularioSolicitudGastoPresupuesto" name="buscar_monto" placeholder="ACTIVIDAD" class="form-control input-sm">
                                                            </div>-->
                                                            <div class="col-md-1 texto negrita">DESCRIPCIÓN</div>    
                                                            <div class="col-md-7">
                                                                <textarea rows="2" id="descripcionFormularioSolicitudGastoPresupuesto" name="buscar_descripcion" placeholder="DESCRIPCION DE LA SOLICITUD" class="form-control input-sm"></textarea>
                                                            </div>
                                                            <!--<div class="col-md-4"></div>
                                                            <div class="col-md-1 texto negrita"></div>
                                                            <div class="col-md-1"></div>-->
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">MONTO S/.</div>    
                                                            <div class="col-md-2">
                                                                <input type="number" id="montoFormularioSolicitudGastoPresupuesto" name="buscar_monto" placeholder="0" class="form-control input-sm">
                                                            </div>
                                                            <div class="col-md-1">
                                                                <select id="seleccionarMonedaFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                                                                    <option value="SOLES">SOLES</option>
                                                                    <option value="DOLARES">DOLARES</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita tipocambio">TIPO CAMBIO</div>
                                                            <div class="col-md-1 tipocambio"><input id="tipoCambioFormularioSolicitudGastoPresupuesto" type="number" placeholder="T.C" class="form-control input-sm"></div>
                                                            <div class="col-md-2 tipocambio"><button id="recalcularFormularioSolicitudGastoPresupuesto" type="button" name="nuevo" class="btn btn-primary btn-sm btn-block"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> RECALCULAR</button></div> 
                                                            <div class="col-md-2 texto negrita tipocambio">MONTO TOTAL EN SOLES S/.</div>
                                                            <div class="col-md-2 tipocambio"><input id="totalFormularioSolicitudGastoPresupuesto" type="number" placeholder="NUEVO MONTO" class="form-control input-sm"></div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
          
                                                            <div class="col-md-2">
                                                                <!--<input id="idFormatoFormularioSolicitudGastoPresupuesto" type="hidden" class="form-control input-sm">
                                                                <input id="nformatoFormularioSolicitudGastoPresupuesto" type="hidden" class="form-control input-sm">-->
                                                                <button id="botonGuardarFormularioSolicitud" href="#" type="button" class="btn btn-success btn-sm btn-block" data-toggle="modal"><span id="procedimientoSolicitudGastoPresupuesto">NUEVO</span></button>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <button id="cancelarGuardarFormularioSolicitud" type="button" class="btn btn-danger btn-sm btn-block" data-toggle="modal">CANCELAR</button>
                                                            </div> 

                                                            <div class="col-md-4 texto negrita centro mensajeSaldo" style="display: none;">NO CUENTA CON SALDO SUFICIENTE PARA REALIZAR ESTA SOLICITUD</button>
                                                            </div>
                                                           
                                                        </div>









            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
            </div>
          </div>
        </div>
      </div>
    </div>





    <div class="modal fade" id="modal_transferir_solicitud" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">FORMULARIO DE TRANSFERENCIA</h4>
          </div>
          <div class="modal-body">

            <div class="row">
              <div class="col-md-2 texto negrita">PROYECTO</div>    
              <div class="col-md-10">
                <select id="proyectoFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                </select>
              </div>
            </div>
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
          </div>
        </div>
      </div>
    </div>





    <!-- AREA DE NUEVA SOLICITUD PARA INGRESO DE PROVEEDORES -->
    <div class="modal fade" id="modal_nuevo_proveedor_solicitud" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel">FORMULARIO PROVEEDOR DE SERVICIO</h4>
            </div>
            <div class="modal-body">


                    <div class="row contenedor">
                      <div class="col-md-2 texto negrita">N° RUC</div>
                      <div class="col-md-3">
                        <input type="number" id="rucProveedorPresupuesto" name="buscar_monto" placeholder="N° RUC" class="form-control input-sm">
                      </div>
                      <div class="col-md-1"></div>
                      <div class="col-md-1 texto negrita">TIN</div>
                      <div class="col-md-2">
                        <input type="number" id="tinProveedorPresupuesto" name="buscar_monto" placeholder="TIN" class="form-control input-sm">
                      </div>
                      <div class="col-md-1"></div>
                      <div class="col-md-2">
                        <button id="botonGuardarProveedorPresupuesto" type="button" class="btn btn-success btn-sm btn-block">GUARDAR</button>
                      </div>
                    </div>
                    <div class="row contenedor">
                      <div class="col-md-2 texto negrita">RAZON SOCIAL</div>
                      <div class="col-md-7">
                        <input type="text" id="empresaProveedorPresupuesto" name="buscar_monto" placeholder="RAZON SOCIAL" class="form-control input-sm">
                      </div>
                      <div class="col-md-1"></div>
                      <div class="col-md-2">
                        <button id="botonCancelarProveedorPresupuesto" type="button" class="btn btn-danger btn-sm btn-block">CANCELAR</button>
                      </div>
                    </div>

                    <div class="row contenedor">
                      <div class="col-md-4">
                        <input type="hidden" id="procesoProveedorPresupuesto" name="buscar_monto" placeholder="0" class="form-control input-sm" value="0">
                      </div>
                      <div class="col-md-3">
                        <input type="hidden" id="codigoProveedorPresupuesto" name="buscar_monto" placeholder="0" class="form-control input-sm" value="0">
                      </div>
                    </div>

                    <hr>
                    <div class="row contenedor">
                      <div class="col-md-12">
                        <table id="tablaListaProveedorPresupuesto" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>RUC</th>
                                    <th>RAZON SOCIAL</th>
                                    <th>TIN</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                      </div>
                    </div>



                  
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
            </div>
          </div>
        </div>
      </div>
    </div>




    <!-- AREA DE NUEVA SOLICITUD PARA INGRESO DE PROVEEDORES -->
    <div class="modal fade" id="modal_nueva_carrera_solicitud" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel">FORMULARIO CARRERA</h4>
            </div>
            <div class="modal-body">


                    <div class="row contenedor">
                      <div class="col-md-2 texto negrita">CODIGO</div>
                      <div class="col-md-3">
                        <input type="number" id="codigoCarreraPresupuesto" name="buscar_monto" placeholder="CODIGO" class="form-control input-sm">
                      </div>
                      <div class="col-md-1"></div>
                      <div class="col-md-1 texto negrita"></div>
                      <div class="col-md-2">
                      </div>
                      <div class="col-md-1"></div>
                      <div class="col-md-2">
                        <button id="botonGuardarCarreraPresupuesto" type="button" class="btn btn-success btn-sm btn-block">GUARDAR</button>
                      </div>
                    </div>
                    <div class="row contenedor">
                      <div class="col-md-2 texto negrita">CARRERA</div>
                      <div class="col-md-7">
                        <input type="text" id="descripcionCarreraPresupuesto" name="buscar_monto" placeholder="CARRERA" class="form-control input-sm">
                      </div>
                      <div class="col-md-1"></div>
                      <div class="col-md-2">
                        <button id="botonCancelarCarreraPresupuesto" type="button" class="btn btn-danger btn-sm btn-block">CANCELAR</button>
                      </div>
                    </div>

                    <div class="row contenedor">
                      <div class="col-md-4">
                        <input type="hidden" id="procesoCarreraPresupuesto" name="buscar_monto" placeholder="0" class="form-control input-sm" value="0">
                      </div>
                      <div class="col-md-3">
                        <input type="hidden" id="idCarreraPresupuesto" name="buscar_monto" placeholder="0" class="form-control input-sm" value="0">
                      </div>
                    </div>

                    <hr>
                    <div class="row contenedor">
                      <div class="col-md-12">
                        <table id="tablaListaCarreraPresupuesto" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>CODIGO</th>
                                    <th>RAZON SOCIAL</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                      </div>
                    </div>



                  
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal_cargar_pago_oep_solicitud" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            
            <ul class="nav nav-tabs">
              <li class="menuGasto active "><a href="#FORMULARIOPAGOOEP" data-toggle="tab" aria-expanded="false">FORMULARIO</a>
              </li>
              <li class="menuGasto"><a href="#COLABORADORPAGOOEP" data-toggle="tab" aria-expanded="false">COLABORADOR</a>
              </li>
              <li class="menuGasto"><a href="#ACTIVIDADPAGOOEP" data-toggle="tab" aria-expanded="false">ACTIVIDAD</a>
              </li>
            </ul> 



            

                                <!-- Tab panes -->
            <div class="tab-content">
                <div class="areaGasto tab-pane fade active in" id="FORMULARIOPAGOOEP">
                <br>
                  <div class="tab-pane">




                    <div class="row contenedor">
                      <div class="col-md-1 texto negrita">NOMBRE</div>
                      <div class="col-md-5">
                        <select id="nombreOepPresupuesto" class="form-control input-sm">
                        </select>
                      </div>
                      <div class="col-md-1">
                        <input type="hidden" id="posicionOepPresupuesto" placeholder="" class="form-control input-sm">
                      </div>
                      <div class="col-md-3"></div>
                      <div class="col-md-2">
                        <button id="guardarPagoOepSolicitud" type="button" class="btn btn-primary btn-sm btn-block">REGISTRAR</button>
                      </div>
                    </div>
                    <div class="row contenedor">
                      <div class="col-md-1 texto negrita">MONTO S/.</div>
                      <div class="col-md-2">
                        <input type="number" id="montoOepPresupuesto" name="buscar_monto" placeholder="0.00" class="form-control input-sm">
                      </div>
                      <div class="col-md-2 texto negrita">ACTIVIDAD</div>
                      <div class="col-md-4">
                        <select id="actividadOepPresupuesto" class="form-control input-sm">
                        </select>
                      </div>
                      <div class="col-md-1">
                      </div>
                      <div class="col-md-2">
                        <button id="cancelarPagoOepSolicitud" type="button" class="btn btn-danger btn-sm btn-block">CANCELAR</button>
                      </div>
                    </div>
                    <div class="row contenedor">
                      <div class="col-md-1 texto negrita">SEDE</div>
                      <div class="col-md-2">
                        <select id="sedeOepPresupuesto" class="form-control input-sm">
                        </select>
                      </div>
                      <div class="col-md-2 texto negrita">OBSERVACIONES</div>
                      <div class="col-md-4">
                        <textarea id="observacionesOepPresupuesto" rows="2" class="form-control input-sm" placeholder="OBSERVACIONES"></textarea>
                      </div>
                    </div>


                    <hr>
                    <div class="row contenedor">
                      <div class="col-md-12">
                        <table id="tablaListaPagoOep" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>DNI</th>
                                    <th>NOMBRE</th>
                                    <th>MONTO</th>
                                    <th>ACTIVIDAD</th>
                                    <th>SEDE</th>
                                    <th>OBSERVACIONES</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                      </div>
                    </div>








                  </div>
                </div>

                <div class="areaGasto tab-pane fade" id="COLABORADORPAGOOEP">
                <br>
                  <div class="tab-pane">
                    <div class="row contenedor">
                      <div class="col-md-1 texto negrita">
                        DNI
                      </div>
                      <div class="col-md-2">
                        <input type="number" maxlength="8" id="dniColaboradoresPagoOep" name="buscar_monto" placeholder="N_DNI" class="form-control input-sm">
                      </div>
                      <div class="col-md-1 texto negrita">
                        NOMBRE
                      </div>
                      <div class="col-md-4">
                        <input type="text" id="nombreColaboradoresPagoOep" name="buscar_monto" placeholder="AP_PATERNO AP_MATERNO, NOMBRES" class="form-control input-sm">
                      </div>
                      <div class="col-md-1 texto negrita">
                      </div>
                      <div class="col-md-1">
                      </div>
                      <div class="col-md-2">
                        <button id="botonGuardarColaboradoresPagoOep" type="button" class="btn btn-success btn-sm btn-block">GUARDAR</button>
                      </div>
                    </div>
                    <div class="row contenedor">
                      <div class="col-md-1 texto negrita">
                        <!--ESTADO-->
                      </div>
                      <div class="col-md-2">
                        <select id="estadoColaboradoresPagoOep" class="form-control input-sm" style="display:none">
                          <option value="1">ACTIVO</option>
                          <option value="2">SUSPENDIDO</option>
                        </select>
                      </div>
                      <div class="col-md-4">
                        <input type="hidden" id="procesoColaboradoresPagoOep" name="buscar_monto" placeholder="0" class="form-control input-sm" value="0">
                      </div>
                      <div class="col-md-3">
                        <input type="hidden" id="codigoColaboradoresPagoOep" name="buscar_monto" placeholder="0" class="form-control input-sm" value="0">
                      </div>
                      <div class="col-md-2">
                        <button id="botonCancelarColaboradoresPagoOep" type="button" class="btn btn-danger btn-sm btn-block">CANCELAR</button>
                      </div>
                    </div>
                    <br>
                    <div class="row contenedor">
                      <div class="col-md-12">
                        <table id="tablaListaColaboradoresPagoOep" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>DNI</th>
                                    <th>NOMBRE</th>
                                    <!--<th>ESTADO</th>-->
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                      </div>
                    </div>


                  </div>
                </div>

                <div class="areaGasto tab-pane fade" id="ACTIVIDADPAGOOEP">
                <br>
                  <div class="tab-pane">
                    <div class="row contenedor">
                      <div class="col-md-2 texto negrita">
                        ACTIVIDAD
                      </div>
                      <div class="col-md-7">
                        <input type="text" id="nombreActividadPagoOep" name="buscar_monto" placeholder="REGISTRAR ACTIVIDAD" class="form-control input-sm">
                      </div>
                      <div class="col-md-1">
                      </div>
                      <div class="col-md-2">
                        <button id="botonGuardarActividadPagoOep" type="button" class="btn btn-success btn-sm btn-block">GUARDAR</button>
                      </div>
                    </div>
                    <div class="row contenedor">
                      <div class="col-md-1 texto negrita">
                      </div>
                      <div class="col-md-2">
                      </div>
                      <div class="col-md-4">
                        <input type="hidden" id="procesoActividadPagoOep" name="buscar_monto" placeholder="0" class="form-control input-sm" value="0">
                      </div>
                      <div class="col-md-3">
                        <input type="hidden" id="codigoActividadPagoOep" name="buscar_monto" placeholder="0" class="form-control input-sm" value="0">
                      </div>
                      <div class="col-md-2">
                        <button id="botonCancelarActividadPagoOep" type="button" class="btn btn-danger btn-sm btn-block">CANCELAR</button>
                      </div>
                    </div>
                    <br>
                    <div class="row contenedor">
                      <div class="col-md-12">
                        <table id="tablaListaActividadPagoOep" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ACTIVIDAD</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                      </div>
                    </div>


                  </div>
                </div>

            </div>




            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="modal_cargar_pago_oep_solicitud2222" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">CARGA FORMATO DE PAGOS POR OEP</h4>
          </div>
          <div class="modal-body">

            <div class="row contenedor">
              <div class="col-md-4">
                <label class="btn btn-primary btn-sm btn-file btn-block">
                  SELECCIONE ARCHIVO<input type="file" name="xlfile" id="archivoPagoOep" style="display: none;">
                </label>
              </div>
              <div class="col-md-4"></div>
              <div class="col-md-4"><button id="botonPagoOep" type="button" class="btn btn-success btn-sm btn-block"><span class="glyphicon glyphicon-glyphicon-cog" aria-hidden="true"></span> GUARDAR REGISTROS</button></div>
            </div>
            <div class="row contenedor">
              <div class="col-md-4"><p id="archivoSeleccionadoPagoOep">...</p></div>
              <div class="col-md-4"></div>
              <div class="col-md-4"><div id="barraPagoOep" class="myProgress"></div>
              </div>
            </div>

            <div class="row contenedor">
              <div class="col-md-12">
                <table id="tablaListaPagoOep" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>DNI</th>
                            <th>NOMBRE</th>
                            <th>MONTO</th>
                            <th>PRESUPUESTO</th>
                            <th>ACTIVIDAD</th>
                            <th>SEDE</th>
                            <th>OBSERVACIONES</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
              </div>
            </div>


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="modal_lista_formatos" tabindex="-1" data-focus-on="input:first" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">ASIGNAR FORMATO</h4>
          </div>
          <div class="modal-body">

              <div class="row">
                  <div class="col-md-4 texto negrita">
                      SELECCIONAR FORMATO
                  </div>
                  <div class="col-md-6">
                      <select class="form-control input-sm formato">
                          <option value="PIN">PEDIDO INTERNO</option>
                          <option value='ACT'>ACTAS</option>
                          <!--<option value='OCA'>ORDEN DE COMPRA ABIERTA</option>
                          <option value='PRO'>PROVISION</option>-->
                      </select>                  
                  </div>
                  <div class="col-md-2">
                      <button type="button" class="btn btn-primary btn-sm btn-block nuevoformato">NUEVO</button>
                  </div>
              </div>
              <br>
              <div class="row">
                  <div class="col-md-12">
                      <table class="table table-bordered tablaformatos">
                          <thead>
                              <tr>
                                  <th>#</th>
                                  <th>NUMERO</th>
                                  <th>FECHA EMISIÓN</th>
                                  <th>PROVEEDOR</th>
                                  <th>MONEDA</th>
                                  <th>MONTO</th>
                                  <th>ESTADO</th>
                                  <th>OPCIONES</th>
                              </tr>
                          </thead>
                          <tfoot>
                          </tfoot>
                          <tbody>
                                                                            
                          </tbody>
                      </table>
                  </div>
              </div>
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
          </div>
        </div>
      </div>
    </div>
    
    <!-- NUEVO MODAL PARA PEDIDO INTERNO -->
    <div class="modal fade" id="modal_nuevo_pedido_interno" tabindex="-1" data-focus-on="input:first" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel">FORMULARIO PEDIDO INTERNO</h4>
            </div>
            <div class="modal-body">
                    <div class="row">
                        <div class="col-md-7"></div>
                        <div class="col-md-2 texto negrita derecha">P/I NRO.</div>
                        <div class="col-md-3">
                            <input type="text" name="buscar_monto" placeholder="AUTOGENERADO" class="form-control input-sm numeropedidointerno">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7"></div>
                        <div class="col-md-2 texto negrita derecha">RUC/TIN/DNI</div>
                        <div class="col-md-3">
                            <input type="number" name="buscar_monto" placeholder="RUC/TIN/DNI" class="form-control input-sm ructindni">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7"></div>
                        <div class="col-md-2 texto negrita derecha">GRUPO PAGOS</div>
                        <div class="col-md-3">
                            <select class="form-control input-sm grupopagos">
                                <option value='PROVEEDORES VARIOS'>PROVEEDORES VARIOS</option>
                                <option value='DÉBITO AUTOMÁTICO'>DÉBITO AUTOMÁTICO</option>
                                <option value='IMPUESTOS AFP'>IMPUESTOS AFP</option>
                                <option value='RETENCIONES U OTROS'>RETENCIONES U OTROS</option>
                            </select>
                        </div>
                    </div>                    

                    <div class="row">
                      <div class="col-md-2 texto negrita">PERSONA QUE SOLICITA</div>
                      <div class="col-md-5">
                        <input type="text" name="buscar_monto" value="MANUEL CORTÉS FONTCUBERTA" placeholder="SOLICITANTE" class="form-control input-sm personaquesolicita">
                      </div>
                      <div class="col-md-2 texto negrita">MONEDA</div>
                      <div class="col-md-3 texto negrita">TOTAL</div>
                    </div>
                    <div class="row">
                      <div class="col-md-2 texto negrita">DIRECCIÓN</div>
                      <div class="col-md-5">
                        <input type="text" name="buscar_monto" value="DIRECCIÓN DE ASEGURAMIENTO DE LA CALIDAD" placeholder="DIRECCION" class="form-control input-sm direccion">
                      </div>
                      <div class="col-md-2">
                        <select class="form-control input-sm moneda">
                          <option value='DOLARES'>DOLARES</option>
                          <option value='SOLES'>SOLES</option>
                        </select>
                      </div>
                      <div class="col-md-3">
                        <input type="text" name="buscar_monto" placeholder="0.00" class="form-control input-sm total">
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-2 texto negrita">FECHA DE EMISION</div>
                      <div class="col-md-5">
                        <div class='input-group date' id='datetimepickerFechaPIN'>
                          <input type='text' class="form-control input-sm fechaemision"></input>
                          <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                          </span>
                        </div>
                      </div>
                      <div class="col-md-2 texto negrita">TIPO DOCUMENTO</div>
                      <div class="col-md-3">
                        <select class="form-control input-sm tipodocumento">
                          <option value='PAGOS VARIOS'>PAGOS VARIOS</option>
                          <option value='FACTURA'>FACTURA</option>
                          <option value='ANTICIPO PROVEEDOR'>ANTICIPO PROVEEDOR</option>
                        </select>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-2 texto negrita">REALIZADO POR</div>
                      <div class="col-md-5">
                        <input type="text" value="MIRYAM SALAZAR ZEGARRA" class="form-control input-sm realizadopor">
                      </div>
                      <div class="col-md-2 texto negrita">UBICACIÓN</div>
                      <div class="col-md-3">
                        <select class="form-control input-sm ubicacion">
                          <option value='PRINCIPAL'>PRINCIPAL</option>
                          <option value='PAGOS'>PAGOS</option>
                        </select>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-2 texto negrita">GIRAR A NOMBRE DE </div>
                      <div class="col-md-10">
                          <select class="form-control input-sm giraranombrede">
                              <option value='0'>---SELECCIONAR---</option>
                          </select>
                          <!--<input type="text" placeholder="GIRAR A NOMBRE DE " class="form-control input-sm giraranombrede">-->
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-2 texto negrita">OBSERVACIÓN</div>
                      <div class="col-md-10">
                        <input type="text" placeholder="OBSERVACIÓN" class="form-control input-sm observacion">
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-md-12">
                        <table class="table table-bordered table-hover tablaregistros">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>CUENTA GLOBAL</th>
                                    <th>STRING</th>
                                    <th>MONTO</th>
                                    <th>DETALLE</th>
                                    <th>PROVEEDOR</th>
                                    <th>NRO FACTURA</th>
                                    <th>FECHA FACTURA</th>
                                    <th>OPCIONES</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-md-2 texto negrita">TOTAL DEL PEDIDO</div>
                      <div class="col-md-3">
                        <input type="text" name="buscar_monto" placeholder="0.00" class="form-control input-sm totalpedido" disabled>
                      </div>
                      <div class="col-md-1"></div>
                      <div class="col-md-2 texto negrita">CONDICIÓN DE PAGO</div>
                      <div class="col-md-4">
                        <input type="text" value="INMEDIATO" placeholder="CONDICION DE PAGO" class="form-control input-sm condicion">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-2 texto negrita">FIRMA DEL SOLICITANTE</div>
                      <div class="col-md-4">
                        <select class="form-control input-sm firmasolicitante">
                          <option value='mcortes'>MANUEL CORTES FONTCUBERTA ABUCCI</option>
                        </select>
                      </div>
                      <div class="col-md-2 texto negrita">VB DIRECTOR DE ÁREA</div>
                      <div class="col-md-4">
                        <input type="text" value="MANUEL CORTÉS FONTCUBERTA" placeholder="VISTO BUENO" class="form-control input-sm firmavistobueno">
                      </div>
                    </div>
                    <br>
                    <div class="row areabotones">
                      <div class="col-md-1"></div>
                      <div class="col-md-5">
                        <button type="button" class="btn btn-success btn-sm btn-block guardarpedidointerno">GUARDAR</button>
                      </div>
                      <div class="col-md-5">
                        <button type="button" class="btn btn-danger btn-sm btn-block cancelarpedidointerno">CANCELAR</button>
                      </div>
                      <div class="col-md-1"></div>
                    </div>
                    <div class="row areaimpresion" style="display:none">
                      <div class="col-md-1"></div>
                      <div class="col-md-10">
                        <button type="button" class="btn btn-info btn-sm btn-block imprimirpedidointerno">GENERAR EXCEL DE PEDIDO INTERNO</button>
                      </div>
                      <div class="col-md-1"></div>
                    </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- FORMATO DE ACTAS -->
    <div class="modal fade" id="modal_nueva_acta" tabindex="-1" data-focus-on="input:first" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel">FORMULARIO ACTA</h4>
            </div>
            <div class="modal-body">
                    <div class="row">
                        <div class="col-md-7"></div>
                        <div class="col-md-2 texto negrita derecha">ACTA NRO.</div>
                        <div class="col-md-3">
                            <input type="text" name="buscar_monto" placeholder="AUTOGENERADO" class="form-control input-sm numero_acta">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3 texto negrita">PROVEEDOR</div>
                        <div class="col-md-9">
                            <select class="form-control input-sm proveedor">
                                <option value='0'>---SELECCIONAR---</option>
                            </select>
                        </div>
                    </div>                    

                    <div class="row">
                        <div class="col-md-3 texto negrita">RUC</div>
                        <div class="col-md-5">
                        <input type="text" placeholder="RUC" class="form-control input-sm ruc_proveedor">
                      </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 texto negrita">N° ORDEN DE SERVICIO</div>
                        <div class="col-md-5">
                            <input type="text" placeholder="N° ORDEN DE SERVICIO" class="form-control input-sm orden_de_servicio">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 texto negrita">IMPORTE</div>
                        <div class="col-md-5">
                            <input type="text" placeholder="IMPORTE" class="form-control input-sm importe">
                        </div>
                        <div class="col-md-4">
                            <select class="form-control input-sm moneda" disabled="">
                                <option value="DOLARES">DOLARES</option>
                                <option value="SOLES">SOLES</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 texto negrita">FECHA DE INICIO</div>
                        <div class="col-md-4">
                            <div class='input-group date' id='datetimepickerFechaACTFI'>
                                <input type='text' class="form-control input-sm fecha_de_inicio"></input>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 texto negrita">FECHA DE ENTREGA</div>
                        <div class="col-md-4">
                            <div class='input-group date' id='datetimepickerFechaACTFE'>
                                <input type='text' class="form-control input-sm fecha_de_entrega"></input>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 texto negrita">SOLICITANTE</div>
                        <div class="col-md-9">
                            <input type="text" placeholder="SOLICITANTE" class="form-control input-sm solicitante">
                      </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 texto negrita">OBSERVACIONES</div>
                        <div class="col-md-9">
                            <textarea rows="2" placeholder="OBSERVACIONES" class="form-control input-sm observaciones"></textarea>
                        </div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-md-3 texto negrita">RECIBIDO POR</div>
                        <div class="col-md-9">
                            <input type="text" placeholder="RECIBIDO POR" class="form-control input-sm recibido">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 texto negrita">FECHA DE RECEPCIÓN</div>
                        <div class="col-md-4">
                            <div class='input-group date' id='datetimepickerFechaACTFR'>
                                <input type='text' class="form-control input-sm fecha_de_recepcion"></input>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    <br>
                    <div class="row">
                        <div class="col-md-3 texto negrita">VISTO BUENO</div>
                        <div class="col-md-9">
                            <input type="text" placeholder="CONDICION DE PAGO" class="form-control input-sm visto_bueno">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 texto negrita">DIRECCION</div>
                        <div class="col-md-9">
                            <input type="text" placeholder="CONDICION DE PAGO" class="form-control input-sm direccion">
                        </div>
                    </div>

                    <br>
                    <div class="row areabotones">
                        <div class="col-md-1"></div>
                        <div class="col-md-5">
                            <button type="button" class="btn btn-success btn-sm btn-block guardaracta">GUARDAR</button>
                        </div>
                        <div class="col-md-5">
                            <button type="button" class="btn btn-danger btn-sm btn-block cancelaracta">CANCELAR</button>
                        </div>
                        <div class="col-md-1"></div>
                    </div>

                    <div class="row areaimpresion" style="display:none">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <button type="button" class="btn btn-info btn-sm btn-block imprimiracta">GENERAR EXCEL ACTA</button>
                        </div>
                        <div class="col-md-1"></div>
                    </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
            </div>
          </div>
        </div>
      </div>
    </div>















    <!-- FORMULARIO SOLICITUD ( NUEVO - EDITAR - ELIMINAR ) -->
    <div class="modal fade" id="modalProvisionarSolicitudGastoPresupuesto" tabindex="-1" data-focus-on="input:first" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal_largo">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel">FORMULARIO ORDEN DE COMPRA ABIERTA - PROVISION</h4>
            </div>
            <div class="modal-body">


                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">TIPO</div>
                                                            <div class="col-md-2">
                                                                <select class="form-control input-sm tipoOCA">
                                                                    <option value='SOCA'>SOLICITUD ORDEN DE COMPRA ABIERTA</option>
                                                                    <option value='SPRO'>SOLICITUD PROVISION</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">NUMERO</div>
                                                            <div class="col-md-1">
                                                                <input type="text" placeholder="" class="form-control input-sm numeroOCA">
                                                            </div>

                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-1 texto negrita">ESTADO</div>
                                                            <div class="col-md-2">
                                                                <select class="form-control input-sm estadoOCA">
                                                                    <option value='INGRESADO'>INGRESADO</option>
                                                                    <option value='POR APROBAR'>POR APROBAR</option>
                                                                    <option value='APROBADO'>APROBADO</option>
                                                                    <option value='LIQUIDADO'>LIQUIDADO</option>
                                                                </select>
                                                            </div>

                                                            <div class="col-md-1 texto negrita derecha">FECHA :</div>
                                                            <div class="col-md-2 negrita">
                                                                <div class='input-group date' id='datetimepickerFechaOCA'>
                                                                    <input type='text' class="form-control input-sm fechaOCA"></input>
                                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">ÁREA</div>    
                                                            <div class="col-md-2">
                                                                <select class="form-control input-sm areaOCA">
                                                                    <option value='0'>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">STRING</div>    
                                                            <div class="col-md-4">
                                                                <select class="form-control input-sm stringOCA">
                                                                    <option value='0'>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">CUENTA GLOBAL</div>    
                                                            <div class="col-md-3">
                                                                <select class="form-control input-sm categoriaOCA">
                                                                    <option value='0'>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">PROYECTO</div>    
                                                            <div class="col-md-5">
                                                                <select class="form-control input-sm proyectoOCA">
                                                                    <option value='0'>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">DETALLE</div>    
                                                            <div class="col-md-5">
                                                                <select class="form-control input-sm detalleOCA">
                                                                    <option value='0'>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <!--<div class="col-md-2 texto "><spam id="saldoDetalleFormularioSolicitudGastoPresupuesto"></spam></div>-->
                                                        </div>
                                                        <div class="row saldos">  
                                                            <div class="col-md-3 texto negrita derecha"> SALDOS : </div>
                                                            <div class="col-md-3 texto negrita centro saldoOCAM">( MES ) S/. 0.00</div>
                                                            <div class="col-md-3 texto negrita centro saldoOCAQ">( Q ) S/. 0.00</div>
                                                            <div class="col-md-3 texto negrita centro saldoOCAT">( TOTAL ) S/. 0.00</div>
                                                            <!--<div class="col-md-5 texto negrita centro saldoSolicitudPresupuesto">SALDO PRESUPUESTO : S/. 0.00</div>-->
                                                        </div>

                                                        <br>
                                                        <div class="row proveedor">
                                                            <div class="col-md-1 texto negrita">PROVEEDOR</div>    
                                                            <div class="col-md-5">
                                                                <select class="form-control input-sm proveedorOCA">
                                                                    <option value='0'>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">DESCRIPCIÓN</div>    
                                                            <div class="col-md-11">
                                                                <textarea rows="2" placeholder="DESCRIPCION DE LA SOLICITUD" class="form-control input-sm descripcionOCA"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">MONTO S/.</div>    
                                                            <div class="col-md-2">
                                                                <input type="number" placeholder="0" class="form-control input-sm montoOCA">
                                                            </div>
                                                            <div class="col-md-1">
                                                                <select class="form-control input-sm monedaOCA">
                                                                    <option value="SOLES">SOLES</option>
                                                                    <option value="DOLARES">DOLARES</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita derecha tcOCA">TIPO CAMBIO</div>
                                                            <div class="col-md-1 tcOCA"><input type="number" placeholder="T.C" class="form-control input-sm tipocambioOCA"></div>
                                                            <div class="col-md-2 tcOCA"><button type="button" name="nuevo" class="btn btn-primary btn-sm btn-block recalcularOCA"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> RECALCULAR</button></div> 
                                                            <div class="col-md-2 texto negrita derecha tcOCA">MONTO TOTAL EN SOLES S/.</div>
                                                            <div class="col-md-2 tcOCA"><input type="number" placeholder="NUEVO MONTO" class="form-control input-sm totalOCA"></div>
                                                        </div>
                                                        <br>
                                                        <div class="row detalleProvisionar">
                                                            <div class="col-md-12 texto negrita">DETALLE DE GASTOS  [ ORDEN DE COMPRA ABIERTA - PROVISION ]</div>    
                                                        </div>
                                                        <br>
                                                        <div class="row detalleProvisionar">
                                                            <div class="col-md-12">
                                                                <table class="table table-bordered table-hover tablaDetalleProvisionarSolicitudGastoPresupuesto">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th>AREA</th>
                                                                            <th>STRING</th>
                                                                            <th>CUENTA</th>
                                                                            <th>PROYECTO</th>
                                                                            <th>DETALLE</th>
                                                                            <th>MES</th>
                                                                            <th>MONTO</th>
                                                                            <th>SOLICITANTE</th>
                                                                            <th>DETALLE</th>
                                                                            <!--<th>OPCIONES</th>-->
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row detalleProvisionar">
                                                            <div class="col-md-1 texto negrita">SALDO S/.</div>    
                                                            <div class="col-md-2">
                                                                <input type="number" placeholder="0" class="form-control input-sm saldoOCA">
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
          
                                                            <div class="col-md-3">
                                                                <input type="hidden" class="form-control input-sm codigoOCA">
                                                                <button href="#" type="button" class="btn btn-success btn-sm btn-block guardarformularioOCA" data-toggle="modal">GUARDAR</button>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <input type="hidden" class="form-control input-sm codigoOCA">
                                                                <button href="#" type="button" class="btn btn-info btn-sm btn-block imprimirformularioOCA" data-toggle="modal">IMPRIMIR REPORTE</button>
                                                            </div>
                                                           
                                                        </div>







            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
            </div>
          </div>
        </div>
      </div>
    </div>

var service = new Service("webService/index.php");
var FormValidator = {};
//var cuestionario = Array;
$(function () {
    $("#menu_presupuesto_transferencias").on('click', function () {
        cargaInicial();
    });

    $(".menu_presupuesto_transferencias").on('click', function () {
        cargaInicial();
    });



    function cargaInicial(){
        var nanio = $("#selectGlobalYear").val();
        service.procesarPost("getAreasTransferencias",resultadoGetAreasTransferencias);
    };

    $("#selectGlobalYear").on('change', function(){
        nanio = $("#selectGlobalYear").val();
    });


    function resultadoGetAreasTransferencias(evt){
        resultado = evt;
        $(".area_trans").html("<option value='0'>---SELECCIONAR---</option>");
        //$("#seguimientoSolicitudGastoPresupuesto .area").html("<option value='0'>---SELECCIONAR---</option>");
        //$("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $(".area_trans" ).append( "<option value='"+ resultado[i].chr_area_id +"'>"+ resultado[i].chr_area +"</option>" );
            //$("#seguimientoSolicitudGastoPresupuesto .area" ).append( "<option value='"+ resultado[i].chr_area_id +"'>"+ resultado[i].chr_area +"</option>" );
            //$("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").append( "<option value='"+ resultado[i].chr_area_id +"'>"+ resultado[i].chr_area +"</option>" );
        }
    }

   $(".area_trans").on('change',function(){
        $(".string_trans").html("<option value='0'>---SELECCIONAR---</option>");
        $(".categoria_trans").html("<option value='0'>---SELECCIONAR---</option>");
        $(".proyecto_trans").html("<option value='0'>---SELECCIONAR---</option>");
        $(".detalle_trans").html("<option value='0'>---SELECCIONAR---</option>");
        area = $(".area_trans").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.anio = nanio;
        service.procesar("getStringTransferencias",objeto,resultadoGetStringsTransferencias);
    });

    function resultadoGetStringsTransferencias (evt){
        resultado = evt;

        $(".string_trans").html("<option value='0'>---SELECCIONAR---</option>");
        //$("#categoriaFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        //$("#proyectoFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        //$("#detalleFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");

        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $(".string_trans" ).append( "<option value='"+ resultado[i].chr_string_id +"'>"+ resultado[i].string +"</option>" );
        }
    }


   $(".string_trans").on('change',function(){
        $(".categoria_trans").html("<option value='0'>---SELECCIONAR---</option>");
        $(".proyecto_trans").html("<option value='0'>---SELECCIONAR---</option>");
        $(".detalle_trans").html("<option value='0'>---SELECCIONAR---</option>");
        area = $(".area_trans").val();
        string = $(".string_trans").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.anio = nanio;
        service.procesar("getCategoriasTransferencias",objeto,resultadoGetCategoriasTransferencias);
    });

    function resultadoGetCategoriasTransferencias (evt){
        resultado = evt;

        $(".categoria_trans").html("<option value='0'>---SELECCIONAR---</option>");
        //$("#categoriaFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        //$("#proyectoFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        //$("#detalleFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");

        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $(".categoria_trans" ).append( "<option value='"+ resultado[i].chr_categoria_id +"'>"+ resultado[i].categoria +"</option>" );
        }
    }


    $(".categoria_trans").on('change',function(){
        $(".proyecto_trans").html("<option value='0'>---SELECCIONAR---</option>");
        $(".detalle_trans").html("<option value='0'>---SELECCIONAR---</option>");
        area = $(".area_trans").val();
        string = $(".string_trans").val();
        categoria = $(".categoria_trans").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.anio = nanio;
        service.procesar("getProyectosTransferencias",objeto,resultadoGetProyectosTransferencias);
    });

    function resultadoGetProyectosTransferencias (evt){
        resultado = evt;

        $(".proyecto_trans").html("<option value='0'>---SELECCIONAR---</option>");
        //$("#categoriaFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        //$("#proyectoFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        //$("#detalleFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");

        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $(".proyecto_trans" ).append( "<option value='"+ resultado[i].chr_proyecto_id +"'>"+ resultado[i].proyecto +"</option>" );
        }
    }

    $(".proyecto_trans").on('change',function(){
        $(".detalle_trans").html("<option value='0'>---SELECCIONAR---</option>");
        area = $(".area_trans").val();
        string = $(".string_trans").val();
        categoria = $(".categoria_trans").val();
        proyecto = $(".proyecto_trans").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.proyecto = proyecto;
            objeto.anio = nanio;
        service.procesar("getDetallesTransferencias",objeto,resultadoGetDetallesTransferencias);
    });

    function resultadoGetDetallesTransferencias (evt){
        resultado = evt;

        $(".detalle_trans").html("<option value='0'>---SELECCIONAR---</option>");
        //$("#categoriaFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        //$("#proyectoFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        //$("#detalleFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");

        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $(".detalle_trans" ).append( "<option value='"+ resultado[i].chr_comentario_id +"'>"+ resultado[i].chr_comentario +"</option>" );
        }
    }

    $(".procesarbusqueda_trans").on('click', function(){
        var varea = $(".area_trans").val();
        var vstring = $(".string_trans").val();
        var vcategoria = $(".categoria_trans").val();
        var vproyecto = $(".proyecto_trans").val();
        var vdetalle = $(".detalle_trans").val();

        if(varea != 0 && vstring != 0 && vcategoria != 0){
            loadSpinner();
            var objRequest = new Object();
            objRequest.subject = 'getCuentas';
            objRequest.year = $("#selectGlobalYear").val();
            objRequest.area = $(".area_trans").val();
            objRequest.string = $(".string_trans").val();
            objRequest.categoria = $(".categoria_trans").val();
            objRequest.proyecto = $(".proyecto_trans").val();
            objRequest.detalle = $(".detalle_trans").val();
            service.procesarPost("getCuentas", objRequest, printLoadService);
        }else{
            alertify.error("SELECCIONAR ÁREA, STRING Y CATEGORIA");
        }
    })







    //cargamos el evento para click en el tab
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href") // activated tab
        if (target == '#listaMovimientos') {
            listarMovimientos();
        }
    });
//Compose template string
    String.prototype.compose = (function () {
        var re = /\{{(.+?)\}}/g;
        return function (o) {
            return this.replace(re, function (_, k) {
                return typeof o[k] != 'undefined' ? o[k] : '';
            });
        }
    }());
});

function listarMovimientos() {
    var objRequest = new Object();
    objRequest.subject = 'listarMovimientos';
    objRequest.year = $("#selectGlobalYear").val();
    service.procesar("listarMovimientos", objRequest, printLoadService);
}

function showDetailMovimiento(movimiento_id) {
    var objRequest = new Object();
    objRequest.subject = 'mostrarDetalleMovimiento';
    objRequest.movimiento_id = movimiento_id;
    service.procesar("mostrarDetalleMovimiento", objRequest, printLoadService);
}

function exportarMovimiento() {
    var objRequest = new Object();
    objRequest.subject = 'exportarMovimiento';
    objRequest.movimiento_id = $(this).attr('data-id');
    service.procesar("exportarMovimiento", objRequest, printLoadService);
}

function printLoadService(response) {
    switch (response.subject) {
        case  'anularMovimiento':
            //listamos los registros de movimientos actualizados
            listarMovimientos();
            break;
        case 'ejecutarTransferencia':
            $("#modalDetalleMovimiento").modal('hide');
            //actualizar la lista de los movimientos
            listarMovimientos();
            break;
        case 'exportarMovimiento':

            break;
        case 'mostrarDetalleMovimiento':
            //llenamos el detalle del movimiento
            $("#divModalDetalleMovimiento").html(response.data.html);
            //abrimos el modal
            $("#modalDetalleMovimiento").modal('show');
            //creamos el evento para exportar el movimiento
            $("#btnExportarMovimiento").on('click', exportarMovimiento);
            break;
        case 'listarMovimientos':
            $("#listaMovimientos").html(response.data.html);
            //tableListaMovimientos
            loadDataTable('tableListaMovimientos');
            break;
        case 'saveMovimiento':
            $("#divFormPresupuestoTransferencia").hide();
            $("#divTablePresupuestoTransferencia").show();
            break;
        case 'eliminarMovimiento':
            $('#row_' + response.data.orden_movimiento_detalle_id).remove();
            //sumamos los transferido hasta
            var monto_transferido = parseFloat(response.data.objOrdenMovimiento.dec_total, 10);
            monto_transferido = Number((monto_transferido).toFixed(3));
            //monto_transferido = monto_transferido.toLocaleString('en');
            $("#inputMontoTotalTransferido").val(monto_transferido);
            //calculamos la diferencia
            var monto_requerido = $("#inputMontoRequerido").val();
            monto_requerido = monto_requerido.toString().replace(/[,]/, "");
            var diferencia = parseFloat(monto_requerido, 10) + parseFloat(response.data.objOrdenMovimiento.dec_total, 10);
            diferencia = Number((diferencia).toFixed(3));
            //diferencia = diferencia.toLocaleString('en');
            $("#inputDiferenciaTransferido").val(diferencia);
            break;
        case 'addCuentaMovimiento':
            //eliminamos la fila antes de reemplazarla
            if (response.data.orden_movimiento_detalle_id > 0) {
                $('#row_' + response.data.orden_movimiento_detalle_id).remove();
            }
            $("#modalTransferenciasStringPresupuesto").modal('hide');
            //llenamos los datos en el listado
            //tableListMovimientoPresupuesto
            var tbody = $('#tableListMovimientoPresupuesto').children('tbody');
            var table = tbody.length ? tbody : $('#tableListMovimientoPresupuesto');
            var row = '<tr id="row_{{id}}">' +
                    '<td>{{count}}</td>' +
                    '<td>{{area}}</td>' +
                    '<td>{{string}}</td>' +
                    '<td>{{categoria}}</td>' +
                    '<td>{{proyecto}}</td>' +
                    '<td>{{comentario}}</td>' +
                    '<td>{{subtotal}}</td>' +
                    '<td>{{acciones}}</td>' +
                    '</tr>';
            var monto_detalle = parseFloat(response.data.movimiento.monto, 10);
            monto_detalle = monto_detalle.toLocaleString('en');
            $("#tableListMovimientoPresupuesto tbody").prepend(row.compose({
                'id': response.data.movimiento.id,
                'count': 1,
                'area': response.data.movimiento.area,
                'string': response.data.movimiento.string,
                'categoria': response.data.movimiento.categoria,
                'proyecto': response.data.movimiento.proyecto,
                'comentario': response.data.movimiento.comentario,
                'subtotal': monto_detalle,
                'acciones': response.data.movimiento.acciones,
            }));
            //sumamos los transferido hasta
            var monto_transferido = Number((parseFloat(response.data.objOrdenMovimiento.dec_total)).toFixed(3));
            //monto_transferido = monto_transferido.toLocaleString('en');
            $("#inputMontoTotalTransferido").val(monto_transferido);
            //calculamos la diferencia
            var monto_requerido = $("#inputMontoRequerido").val();
            monto_requerido = monto_requerido.toString().replace(/[,]/, "");
            var diferencia = parseFloat(monto_requerido, 10) + parseFloat(response.data.objOrdenMovimiento.dec_total, 10);
            diferencia = Number((diferencia).toFixed(3));
            //diferencia = diferencia.toLocaleString('en');
            $("#inputDiferenciaTransferido").val(diferencia);
            //crear el evento para la clase eliminar
            //$(".delete_movimiento").on('click', eliminarMovimiento);
            break;
        case 'getCuentas':
            removeDataTable('tablePresupuestoTransferencia');
            $("#tablePresupuestoTransferenciaBody").html(response.data.html);
            //cargamos el evento para el link transferencia
            $(".presupuestoTransferencia_transferir").on('click', solicitarTransferencia);
            //cargamos el datatable
            loadDataTable('tablePresupuestoTransferencia');
            stopSpinner();
            break;
        case 'solicitarTransferencia':
            $("#divFormPresupuestoTransferencia").show();
            $("#divTablePresupuestoTransferencia").hide();
            $("#divContentPresupuestoTransferencia").html(response.data.html);
            //Habilitar boton para activar el modal
            $("#btnAgregarCuenta").on('click', openModalTransferencia);
            //habilitar evento para cancelar
            $("#btnCancelarProcesarTransferencia").on('click', cancelarFormularioTransferenciaPresupuesto);
            //agregar validadores para el formulario de la orden de movimiento
            addValidateForm('formMovimientosPresupuesto');
            break;
        case 'loadFormCuenta':
            $("#divFormContentModalTransferencias").html(response.data.html);
            $("#modalTransferenciasStringPresupuesto").modal();
            //activamos el evente change para el combo area
            $("#inputArea").on('change', loadStringTransferenciaPresupuesto);
            $("#inputMes").on('change', loadSaldoTransferenciaPresupuesto);
            //cargamos las  validaciones para el formulario
            addValidateForm('formModalTransferenciaCuenta');
            break;
        case 'editarOrdeMovimientoDetalle':
            $("#divFormContentModalTransferencias").html(response.data.html);
            $("#modalTransferenciasStringPresupuesto").modal();
            //activamos el evente change para el combo area
            $("#inputArea").on('change', loadStringTransferenciaPresupuesto);
            $("#inputString").on('change', loadProyectoTransferenciaPresupuesto);
            $("#inputProyecto").on('change', loadCategoriaTransferenciaPresupuesto);
            $("#inputCategoria").on('change', loadComentarioTransferenciaPresupuesto);
            $("#inputMes").on('change', loadSaldoTransferenciaPresupuesto);
            //cargamos las  validaciones para el formulario
            addValidateForm('formModalTransferenciaCuenta');
            break;
        case 'loadString':
            $("#divDropDownString").html(response.data.html);
            $("#divDropDownProyecto").html(response.data.proyecto);
            $("#divDropDownCategoria").html(response.data.categoria);
            $("#divDropDownComentario").html(response.data.comentario);
            $("#inputString").on('change', loadProyectoTransferenciaPresupuesto);
            //reseteamos el dropdown de meses inputMes
            $("#inputMes").val($("#inputMes option:first").val());
            //$("#inputMontoTransferir").prop('disabled', true);
            $("#inputMontoTransferir").attr("readonly", true);
            $("#inputMontoTransferir").val('');
            $("#inputSaldoActual").attr("readonly", true);
            $("#inputSaldoActual").val('');
            break;
        case 'loadProyecto':
            $("#divDropDownProyecto").html(response.data.html);
            $("#divDropDownCategoria").html(response.data.categoria);
            $("#divDropDownComentario").html(response.data.comentario);
            $("#inputProyecto").on('change', loadCategoriaTransferenciaPresupuesto);
            //reseteamos el dropdown de meses inputMes
            $("#inputMes").val($("#inputMes option:first").val());
//            $("#inputMontoTransferir").prop('disabled', true);
//            $("#inputMontoTransferir").val('');
            $("#inputMontoTransferir").attr("readonly", true);
            $("#inputMontoTransferir").val('');
            $("#inputSaldoActual").attr("readonly", true);
            $("#inputSaldoActual").val('');
            break;
        case 'loadCategoria':
            $("#divDropDownCategoria").html(response.data.html);
            $("#divDropDownComentario").html(response.data.comentario);
            $("#inputCategoria").on('change', loadComentarioTransferenciaPresupuesto);
            //reseteamos el dropdown de meses inputMes
            $("#inputMes").val($("#inputMes option:first").val());
//            $("#inputMontoTransferir").prop('disabled', true);
//            $("#inputMontoTransferir").val('');
            $("#inputMontoTransferir").attr("readonly", true);
            $("#inputMontoTransferir").val('');
            $("#inputSaldoActual").attr("readonly", true);
            $("#inputSaldoActual").val('');
            break;
        case 'loadComentario':
            $("#divDropDownComentario").html(response.data.html);
            //reseteamos el dropdown de meses inputMes
            $("#inputMes").val($("#inputMes option:first").val());
            //cargamos el evento change en el dropdown  de meses
            $("#inputComentario").on('change', loadMesTransferenciaPresupuesto);

//            $("#inputMontoTransferir").prop('disabled', true);
//            $("#inputMontoTransferir").val('');
            $("#inputMontoTransferir").attr("readonly", true);
            $("#inputMontoTransferir").val('');
            $("#inputSaldoActual").attr("readonly", true);
            $("#inputSaldoActual").val('');
            break;
        case 'loadSaldo':
            $("#inputSaldoActual").val(response.data.saldoActual);
            $("#chr_year_movimiento_detalle").val(response.data.chr_year);
            $("#inputMontoTransferir").val('');
            //Si el saldo es positivo activamos la caja de monto a transferir
            if (parseInt(response.data.saldoActual) > 0) {
                //$("#inputMontoTransferir").prop('disabled', false);
                $("#inputMontoTransferir").attr("readonly", false);
                $("#inputMontoTransferir").focus();
            } else {
                //$("#inputMontoTransferir").prop('disabled', true);
                $("#inputMontoTransferir").attr("readonly", true);
            }
            break;
    }
}

function cancelarFormularioTransferenciaPresupuesto() {
    $("#divFormPresupuestoTransferencia").hide();
    $("#divTablePresupuestoTransferencia").show();
}

function loadMesTransferenciaPresupuesto() {
    $("#inputMes").val($("#inputMes option:first").val());
    //$("#inputMontoTransferir").prop('disabled', true);
    $("#inputMontoTransferir").attr("readonly", true);
    $("#inputMontoTransferir").val('');
    $("#inputSaldoActual").attr("readonly", true);
    $("#inputSaldoActual").val('');

}

function loadSaldoTransferenciaPresupuesto() {
    var objRequest = new Object();
    objRequest.subject = 'loadSaldo';
    objRequest.area = $("#inputArea").val();
    objRequest.string = $("#inputString").val();
    objRequest.proyecto = $("#inputProyecto").val();
    objRequest.categoria = $("#inputCategoria").val();
    objRequest.comentario = $("#inputComentario").val();
    objRequest.int_orden_movimiento_id = $("#orden_movimiento_id").val();
    objRequest.mes = $(this).val();
    objRequest.year = $("#chr_year_cuenta").val();
    service.procesar("loadSaldo", objRequest, printLoadService);
}

function anularMovimiento(int_movimiento_id) {
    var conf = confirm('¿Realmente deseas realizar esta acción?');
    if (conf) {
        var objRequest = new Object();
        objRequest.subject = 'anularMovimiento';
        objRequest.id = int_movimiento_id;
        service.procesar("anularMovimiento", objRequest, printLoadService);
    }
}

function eliminarMovimientoDetalle(id) {
    var conf = confirm('¿Realmente deseas realizar esta acción?');
    if (conf) {
        var objRequest = new Object();
        objRequest.subject = 'eliminarMovimiento';
        objRequest.id = id;
        service.procesar("eliminarMovimiento", objRequest, printLoadService);
    }
}

function loadComentarioTransferenciaPresupuesto() {
    var objRequest = new Object();
    objRequest.subject = 'loadComentario';
    objRequest.area = $("#inputArea").val();
    objRequest.string = $("#inputString").val();
    objRequest.proyecto = $("#inputProyecto").val();
    objRequest.categoria = $(this).val();
    objRequest.year = $("#chr_year_cuenta").val();
    service.procesar("loadComentario", objRequest, printLoadService);
}

function loadCategoriaTransferenciaPresupuesto() {
    var objRequest = new Object();
    objRequest.subject = 'loadCategoria';
    objRequest.area = $("#inputArea").val();
    objRequest.string = $("#inputString").val();
    objRequest.proyecto = $(this).val();
    objRequest.year = $("#chr_year_cuenta").val();
    service.procesar("loadCategoria", objRequest, printLoadService);
}

function loadProyectoTransferenciaPresupuesto() {
    var objRequest = new Object();
    objRequest.subject = 'loadProyecto';
    objRequest.area = $("#inputArea").val();
    objRequest.string = $(this).val();
    objRequest.year = $("#chr_year_cuenta").val();
    service.procesar("loadProyecto", objRequest, printLoadService);
}

function loadStringTransferenciaPresupuesto() {
    var objRequest = new Object();
    objRequest.subject = 'loadString';
    objRequest.area = $(this).val();
    objRequest.year = $("#chr_year_cuenta").val();
    service.procesar("loadString", objRequest, printLoadService);
}

function openModalTransferencia() {
    //obtenemos el formulario de un ajax
    //divFormContentModalTransferencias
    var objRequest = new Object();
    objRequest.subject = 'loadFormCuenta';
    objRequest.orden_movimiento_id = $("#orden_movimientoid").val();
    objRequest.year = $("#chr_year").val();
    objRequest.chr_area = $("#chr_area").val();
    objRequest.chr_string = $("#chr_string").val();
    objRequest.chr_categoria = $("#chr_categoria").val();
    objRequest.chr_proyecto = $("#chr_proyecto").val();
    objRequest.chr_comentario = $("#chr_comentario").val();
    objRequest.chr_mes = $("#chr_mes").val();
    service.procesar("loadFormCuenta", objRequest, printLoadService);
}

function editarCuentaMovimiento(orden_movimiento_detalle_id) {
    var objRequest = new Object();
    objRequest.subject = 'editarOrdeMovimientoDetalle';
    objRequest.orden_movimiento_id = $("#orden_movimientoid").val();
    objRequest.orden_movimiento_detalle_id = orden_movimiento_detalle_id;
    service.procesar("editarOrdeMovimientoDetalle", objRequest, printLoadService);
}

function solicitarTransferencia() {
    var monto_requerido = $(this).attr('data-requerido');
    var objRequest = new Object();
    objRequest.monto_requerido = monto_requerido;
    objRequest.area = $(this).attr('data-area');
    objRequest.string = $(this).attr('data-string');
    objRequest.categoria = $(this).attr('data-categoria');
    objRequest.proyecto = $(this).attr('data-proyecto');
    objRequest.comentario = $(this).attr('data-comentario');
    objRequest.chr_area_id = $(this).attr('data-area-id');
    objRequest.chr_string_id = $(this).attr('data-string-id');
    objRequest.chr_categoria_id = $(this).attr('data-categoria-id');
    objRequest.chr_proyecto_id = $(this).attr('data-proyecto-id');
    objRequest.chr_comentario_id = $(this).attr('data-comentario-id');
    objRequest.mes = $(this).attr('data-mes');
    objRequest.year = $(this).attr('data-year');
    objRequest.subject = 'solicitarTransferencia';
    service.procesar("solicitarTransferencia", objRequest, printLoadService);
}

function addCuentaToMovimiento() {
    var objRequest = new Object();
    objRequest.subject = 'addCuentaMovimiento';
    objRequest.chr_area_id = $("#inputArea").val();
    objRequest.chr_string_id = $("#inputString").val();
    objRequest.chr_proyecto_id = $("#inputProyecto").val();
    objRequest.chr_categoria_id = $("#inputCategoria").val();
    objRequest.chr_comentario_id = $("#inputComentario").val();

    objRequest.area = $("#inputArea option:selected").text();
    objRequest.string = $("#inputString option:selected").text();
    objRequest.proyecto = $("#inputProyecto option:selected").text();
    objRequest.categoria = $("#inputCategoria option:selected").text();
    objRequest.comentario = $("#inputComentario option:selected").text();

    objRequest.mes = $("#inputMes").val();
    objRequest.saldo = $("#inputSaldoActual").val();
    objRequest.monto = $("#inputMontoTransferir").val();
    objRequest.orden_movimiento_id = $("#orden_movimientoid").val();
    objRequest.orden_movimiento_detalle_id = $("#orden_movimiento_detalle_id").val();
    objRequest.chr_year = $("#chr_year_movimiento_detalle").val();
    service.procesar("addCuentaMovimiento", objRequest, printLoadService);

}

function saveOrdenMovimiento() {
    var conf = confirm('¿Realmente deseas realizar esta acción?');
    if (conf) {
        var objRequest = new Object();
        objRequest.subject = 'saveMovimiento';
        objRequest.txt_sustento = $("#inputSustento").val();
        objRequest.dec_total = $("#inputMontoTotalTransferido").val();
        objRequest.dec_new_saldo = $("#inputDiferenciaTransferido").val();
        objRequest.orden_movimiento_id = $("#orden_movimientoid").val();
        objRequest.name = $("#inputNameTransferencia").val();
        objRequest.year = $("#chr_year").val();
        service.procesar("saveMovimiento", objRequest, printLoadService);
    }
}
/**
 * Método para ejeutar el movimiento y pasarlo a un estado, pendiente por ejecutar
 */
function ejecutarTransferencia(movimientoId) {
    var conf = confirm('¿Realmente deseas realizar esta acción?');
    if (conf) {
        var objRequest = new Object();
        objRequest.subject = 'ejecutarTransferencia';
        objRequest.movimientoId = movimientoId;
        service.procesar("ejecutarTransferencia", objRequest, printLoadService);
    }
}



function actualizarListaCuentas() {
    //obtenemos los datos del servidor
    //loadSpinner();
    //listarMovimientos();
    //var objRequest = new Object();
    //objRequest.subject = 'getCuentas';
    //objRequest.year = $("#selectGlobalYear").val();
    //service.procesarPost("getCuentas", objRequest, printLoadService);
}

function loadDataTable(idElement) {
    $('#' + idElement).DataTable({
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "lengthMenu": [[20, 100, 500, -1], [20, 100, 500, "Todo"]]
    });
}
function removeDataTable(idElement) {
    if ($.fn.DataTable.isDataTable('#' + idElement)) {
        $('#' + idElement).DataTable().clear().destroy();
    }
}

function addValidateForm(idForm) {
    switch (idForm) {
        case 'formMovimientosPresupuesto':
            FormValidator[idForm] = $("#" + idForm).validate({
                rules: {
                    inputSustento: "required",
                    inputNameTransferencia: "required",
                    inputMontoTotalTransferido: {
                        required: true,
                        number: true,
                        min: 1
                    }
                },
                messages: {
                    inputSustento: "Ingrese un sustento para ejecutar el movimiento",
                    inputNameTransferencia: "Ingrese un título descriptivo de la transferencia",
                    inputMontoTotalTransferido: {
                        required: "Es necesario un monto",
                        number: "Sólo dígitos",
                        min: "Ingresar un monto positivo"
                    }
                },
                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help-block");
                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".divFeedbackCustom").addClass("has-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[ 0 ]) {
                        $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[ 0 ]) {
                        $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).parents(".divFeedbackCustom").addClass("has-error").removeClass("has-success");
                    $(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".divFeedbackCustom").addClass("has-success").removeClass("has-error");
                    $(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
                },
                submitHandler: function () {
                    saveOrdenMovimiento();
                    return true;
                }
            });
            break;
        case 'formModalTransferenciaCuenta':
            jQuery.validator.addMethod("monto_menor_igual", function (value, element) {
                if (parseFloat($("#inputMontoTransferir").val()) <= parseFloat($("#inputSaldoActual").val()))
                    return true;
            }, jQuery.validator.format("El monto debe ser menor o igual al saldo"));

            jQuery.validator.addMethod("yourself", function (value, element) {
                if (
                        $("#inputArea").val() != $("#chr_area_cuenta").val() ||
                        $("#inputString").val() != $("#chr_string_cuenta").val() ||
                        $("#inputCategoria").val() != $("#chr_categoria_cuenta").val() ||
                        $("#inputProyecto").val() != $("#chr_proyecto_cuenta").val() ||
                        $("#inputComentario").val().toString() != $("#chr_comentario_cuenta").val().toString() ||
                        $("#inputMes").val().toString() != $("#chr_mes_cuenta").val().toString()
                        ) {
                    return true;
                }
            }, jQuery.validator.format("No se puede realizar una transferencia en la misma cuenta"));

            FormValidator[idForm] = $("#" + idForm).validate({
                rules: {
                    inputArea: "required",
                    inputString: "required",
                    inputProyecto: "required",
                    inputCategoria: "required",
                    inputComentario: {
                        required: true
                    },
                    inputMes: {
                        required: true,
                        yourself: true
                    },
                    inputMontoTransferir: {
                        required: true,
                        number: true,
                        min: 1,
                        monto_menor_igual: true
                    }
                },
                messages: {
                    inputArea: "Seleccione un área",
                    inputString: "Seleccione un string",
                    inputProyecto: "Seleccione un proyecto",
                    inputCategoria: "Seleccione una categoría",
                    inputComentario: {
                        required: "Seleccione un comentario"
                    },
                    inputMes: {
                        required: "Seleccione un mes",
                        yourself: "No se puede realizar una transferencia en la misma cuenta"
                    },
                    inputMontoTransferir: {
                        required: "Es necesario un monto",
                        number: "Sólo dígitos",
                        min: "Ingresar un monto positivo",
                        monto_menor_igual: "El monto debe ser menor o igual al saldo"
                    }
                },
                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help-block");
                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".divFeedbackCustom").addClass("has-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[ 0 ]) {
                        $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[ 0 ]) {
                        $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).parents(".divFeedbackCustom").addClass("has-error").removeClass("has-success");
                    $(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".divFeedbackCustom").addClass("has-success").removeClass("has-error");
                    $(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
                },
                submitHandler: function () {
                    addCuentaToMovimiento();
                    return true;
                }
            });
            break;
    }
    return FormValidator;
}

function loadSpinner() {
    var opts = {
        lines: 13,
        length: 28,
        width: 14,
        radius: 42,
        scale: 1,
        corners: 1,
        color: '#000',
        opacity: 0.25,
        rotate: 0,
        direction: 1,
        speed: 1,
        trail: 60,
        fps: 20,
        zIndex: 2e9,
        className: 'spinner',
        top: '50%',
        left: '50%',
        shadow: false,
        hwaccel: false,
        position: 'absolute',
    },
            target = document.getElementById('spinner'),
            spinner = new Spinner(opts).spin(target);
    $(target).data('spinner', spinner);
}

function stopSpinner() {
    $('#spinner').data('spinner').stop();
}
var service = new Service("webService/index.php");

//var cuestionario = Array;


$(function()
{

iniciarControlPresupuestoSolicitud();

});

var arrayDatosFormularioOep = [];
var arrayDatosCortosFormularioOep = [];

var arrayRegistroFormato = [];

function actualizarModuloSolicitud(){
    alertify.success("ACTUALIZANDO MODULO");
    iniciarControlPresupuestoSolicitud();
}

function iniciarControlPresupuestoSolicitud(){
    var f = new Date();
    var nmes = (f.getMonth()+1);
    var nanio = $("#anioPresupuestoSolicitud").val();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var hora = f.getHours();
    var minuto = f.getMinutes();
    var segundo = f.getSeconds();
    var canio = nanio +''+ mes +''+ dia +''+ hora +''+ minuto +''+ segundo;


    var saldoMesOCA = 0.00;
    var saldoRegistroActual = 0.00;

    var nregistro = "";

    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var fechaActual = f.getFullYear() +"-"+ mes +"-"+ dia;

    $("#anioPresupuestoSolicitud").on('change', function(){
        nanio = $("#anioPresupuestoSolicitud").val();
    });
    
    $("#fechaFormularioSolicitudGastoPresupuesto").val(fecha);
    $("#fechaFacturaFormularioSolicitudGastoPresupuesto").val(fecha);
    $("#modal_nuevo_pedido_interno .fechaemision").val(fecha);
    $("#modalProvisionarSolicitudGastoPresupuesto .fechaOCA").val(fecha);



    var usuario = sessionStorage.getItem("usuario");
    var permisos = sessionStorage.getItem("permisos");

    var area = "";
    var ingreso = 0.00;
    var gasto = 0.00;
    var registros = "";
    var periodo = 0;

    var ndocumentos = "";

    var meses = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "setiembre", "octubre", "noviembre", "diciembre"];
    $(".tipocambio").hide();

    $("#menu_presupuesto_solicitud").on('click', function(){
        cargaConsultasIniciales();  
    })
    $(".menu_presupuesto_solicitud").on('click', function(){
        cargaConsultasIniciales();  
    })
    $(".boton_pedido_interno").on('click', function(){
        cargaListaFormatos();
    })
    $(".boton_acta").on('click', function(){
        cargaListaFormatos();
    })
    $(".boton_personal_apoyo").on('click', function(){
        cargaListaFormatos();
    })

    function cargaConsultasIniciales(){
    	service.procesar("getDatosporUsuario",usuario,cargaAreasporUsuario); //CARGA DE AREAS POR USUARIO
        service.procesar("getColaboradorOepPresupuesto",usuario,cargaColaboradorOepPresupuesto);
        service.procesar("getProveedorPresupuesto",usuario,cargaProveedorPresupuesto);
        service.procesar("getActividadOepPresupuesto",usuario,cargaActividadOepPresupuesto);
        service.procesar("getSedePresupuesto",usuario,cargaSedePresupuesto);
        service.procesar("getListaFormato",0,nanio,cargaListaFormatoFormulario);
        service.procesar("getSolicitante",0,cargaListaSolicitante);
        service.procesar("getConsultarProvisionarSolicitudGastoPresupuesto",0,nanio,resultadoConsultarProvisionarSolicitudGastoPresupuesto);

        service.procesar("getCarreraPresupuesto",usuario,cargaCarreraPresupuesto);

        cargaListaFormatos();

        $("#formularioSolicitudGastoPresupuesto .input-sm").prop('disabled', true);
    	//cargarPeriodos();

        //listarRegistrosGastoPresupuesto();
        //cargaRegistrosPresupuestoGasto();
    }


    function cargaAreasporUsuario(evt){
        resultado = corregirNull(evt[0].area);
        var objeto = new Object()
            objeto.area = resultado;
            objeto.anio = nanio;
            objeto.usuario = usuario;
        service.procesar("getAreasSolicitud",objeto,cargaAreasSolicitud); //CARGA REGISTROS POR USUARIO
    }

    //CARGA LAS AREAS POR CADA USUARIO QUE INGRESA AL SISTEMA
    function cargaAreasSolicitud(evt){
        resultado = evt;
        $("#areaFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#seguimientoSolicitudGastoPresupuesto .area").html("<option value='0'>---SELECCIONAR---</option>");
        $("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#areaFormularioSolicitudGastoPresupuesto" ).append( "<option value='"+ resultado[i].chr_area_id +"'>"+ resultado[i].chr_area +"</option>" );
            $("#seguimientoSolicitudGastoPresupuesto .area" ).append( "<option value='"+ resultado[i].chr_area_id +"'>"+ resultado[i].chr_area +"</option>" );
            $("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").append( "<option value='"+ resultado[i].chr_area_id +"'>"+ resultado[i].chr_area +"</option>" );
        }
    } 

    function cargaListaSolicitante(evt){
        resultado = evt;
        $("#modal_nuevo_pedido_interno .firmasolicitante").html("<option value='mcortes'>MANUEL CORTES FONTCUBERTA ABUCCI</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#modal_nuevo_pedido_interno .firmasolicitante").append( "<option value='"+ resultado[i].usuario +"'>"+ resultado[i].nombre_completo +"</option>" );
        }
    } 

    function cargaListaFormatoFormulario(evt){
        resultado = evt;
        $("#seguimientoSolicitudGastoPresupuesto .nformato").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#seguimientoSolicitudGastoPresupuesto .nformato" ).append( "<option value='"+ resultado[i].numero +"'>"+ resultado[i].numero +"</option>" );
        }        
    }

    function cargaListaNumeroFormatoFormulario(evt){
        resultado = evt;
        $("#formularioSolicitudGastoPresupuesto .nformato").html("<option value=''>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#formularioSolicitudGastoPresupuesto .nformato" ).append( "<option value='"+ resultado[i].numero +"'>"+ resultado[i].numero +"</option>" );
        }        
    }

    $("#areaFormularioSolicitudGastoPresupuesto").on('change',function(){
        area = $("select#areaFormularioSolicitudGastoPresupuesto").val();

        var objeto = new Object()
            objeto.area = area;
            objeto.anio = nanio;
        service.procesar("getStringSolicitud",objeto,cargaStringsSolicitud);
    });

    $("#modal_nuevo_pedido_interno .giraranombrede").on('change',function(){
        var proveedor = $("#modal_nuevo_pedido_interno .giraranombrede").val();
        service.procesar("getRucTinDniSolicitud",proveedor,function(evt){
            if(evt > 0){
               $("#modal_nuevo_pedido_interno .ructindni").val(evt) 
            }else{
                $("#modal_nuevo_pedido_interno .ructindni").val("")
            }
        });
    })

    $("#seguimientoSolicitudGastoPresupuesto .area" ).on('change',function(){
        area = $("#seguimientoSolicitudGastoPresupuesto .area" ).val();
        var objeto = new Object()
            objeto.area = area;
            objeto.anio = nanio;
        service.procesar("getStringSolicitud",objeto,cargaStringsSeguimiento);
    });

    $("#seguimientoSolicitudGastoPresupuesto .formato").on('change',function(){
        cargaListaFormatosFormulario();
    });

    $("#formularioSolicitudGastoPresupuesto .formato").on('change',function(){
        var formato = $("#formularioSolicitudGastoPresupuesto .formato").val();
        service.procesar("getListaFormato",formato,nanio,cargaListaNumeroFormatoFormulario);
    });

    function cargaListaFormatosFormulario(){
        var formato = $("#seguimientoSolicitudGastoPresupuesto .formato").val();
        service.procesar("getListaFormato",formato,nanio,cargaListaFormatoFormulario);
    }

    function cargaStringsSolicitud(evt){
        resultado = evt;

        $("#stringFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#categoriaFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#proyectoFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#detalleFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");

        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#stringFormularioSolicitudGastoPresupuesto" ).append( "<option value='"+resultado[i].chr_string_id +"'>"+ resultado[i].chr_string_id + " - " + resultado[i].string +"</option>" );
        }
    }

    function cargaStringsSeguimiento(evt){
        resultado = evt;

        $("#seguimientoSolicitudGastoPresupuesto .string").html("<option value='0'>---SELECCIONAR---</option>");
        $("#seguimientoSolicitudGastoPresupuesto .categoria").html("<option value='0'>---SELECCIONAR---</option>");
        $("#seguimientoSolicitudGastoPresupuesto .proyecto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#seguimientoSolicitudGastoPresupuesto .detalle").html("<option value='0'>---SELECCIONAR---</option>");

        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#seguimientoSolicitudGastoPresupuesto .string" ).append( "<option value='"+ resultado[i].chr_string_id +"'>"+ resultado[i].string +"</option>" );
        }
    }

    function cargaStringSolicitudVME(){
        area = $("select#areaFormularioSolicitudGastoPresupuesto").val();

        var objeto = new Object()
            objeto.area = area;
            objeto.anio = nanio;
            service.procesar("getStringSolicitud",objeto,function(evt){

            if ( evt == undefined ) return;
            for(var i=0; i<evt.length ; i++){
                $("#stringFormularioSolicitudGastoPresupuesto" ).append( "<option value='"+ evt[i].chr_string_id +"'>"+ evt[i].string +"</option>" );
            }  
                      
        });

    }

    //CARGAR STRING POR CADA AREA
    $("#stringFormularioSolicitudGastoPresupuesto").on('change',function(){
        area = $("select#areaFormularioSolicitudGastoPresupuesto").val();
        string = $("select#stringFormularioSolicitudGastoPresupuesto").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.anio = nanio;
        service.procesar("getCategoriasSolicitud",objeto,cargaCategoriasSolicitud);
    });

    //CARGAR STRING POR CADA AREA
    $("#seguimientoSolicitudGastoPresupuesto .string").on('change',function(){
        area = $("#seguimientoSolicitudGastoPresupuesto .area").val();
        string = $("#seguimientoSolicitudGastoPresupuesto .string").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.anio = nanio;
        service.procesar("getCategoriasSolicitud",objeto,cargaCategoriasSeguimiento);
    });

    function cargaCategoriasSolicitud(evt){
        resultado = evt;
        $("#categoriaFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#proyectoFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#detalleFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#categoriaFormularioSolicitudGastoPresupuesto" ).append( "<option value='"+ resultado[i].chr_categoria_id +"'>"+ resultado[i].chr_categoria_id + " - " + resultado[i].categoria +"</option>" );
        }
    }

    function cargaCategoriasSeguimiento(evt){
        resultado = evt;
        $("#seguimientoSolicitudGastoPresupuesto .categoria").html("<option value='0'>---SELECCIONAR---</option>");
        $("#seguimientoSolicitudGastoPresupuesto .proyecto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#seguimientoSolicitudGastoPresupuesto .detalle").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#seguimientoSolicitudGastoPresupuesto .categoria" ).append( "<option value='"+ resultado[i].chr_categoria_id +"'>"+ resultado[i].chr_categoria_id + " - " + resultado[i].categoria +"</option>" );
        }
    }



    function cargaCategoriasSolicitudVME(){
        area = $("select#areaFormularioSolicitudGastoPresupuesto").val();
        string = $("select#stringFormularioSolicitudGastoPresupuesto").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.anio = nanio;
            service.procesar("getCategoriasSolicitud",objeto,function(evt){

            if ( evt == undefined ) return;
            for(var i=0; i<evt.length ; i++){
                $("#categoriaFormularioSolicitudGastoPresupuesto" ).append( "<option value='"+ evt[i].chr_categoria_id +"'>"+ evt[i].chr_categoria_id + " - " + evt[i].categoria +"</option>" );
            }

        });

    }

    $("#categoriaFormularioSolicitudGastoPresupuesto").on('change',function(){
        if(ndocumentos == ""){
            ndocumentos = $("#ndocumentoFormularioSolicitudGastoPresupuesto").val();
        }
        area = $("select#areaFormularioSolicitudGastoPresupuesto").val();
        string = $("select#stringFormularioSolicitudGastoPresupuesto").val();
        categoria = $("select#categoriaFormularioSolicitudGastoPresupuesto").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.anio = nanio;
        service.procesar("getProyectosSolicitud",objeto,cargaProyectosSolicitud);


        var f2 = new Date();
        var nmes2 = (f2.getMonth()+1);
        var nanio2 = f2.getFullYear();
        var mes2 = f2.getMonth() < 9 ? "0" + (f2.getMonth()+1) : f2.getMonth()+1;
        var dia2 = f2.getDate() < 10 ? "0" + (f2.getDate()) : f2.getDate();
        var hora2 = f2.getHours();
        var minuto2 = f2.getMinutes();
        var segundo2 = f2.getSeconds();
        var canio2 = nanio2 +''+ mes2 +''+ dia2 +''+ hora2 +''+ minuto2 +''+ segundo2;


        if(categoria == "OEP"){
            $("#proveedorFormularioSolicitudGastoPresupuesto").hide();
            $(".datetimepickerFechaFF").hide();
            $("#cargarPagoOepSolicitud").show();
            $("#nuevoProveedorPresupuesto").hide();
            $("#tinProveedor").hide();
            //$("#ndocumentoFormularioSolicitudGastoPresupuesto").val("OEP-" + canio2);
            //$("#proveedorFormularioSolicitudGastoPresupuesto").val("OEP-" + canio2);
            if(ndocumentos != ""){
                $("#ndocumentoFormularioSolicitudGastoPresupuesto").val(ndocumentos);
                $("#proveedorFormularioSolicitudGastoPresupuesto").val(ndocumentos);
            }else{
                $("#ndocumentoFormularioSolicitudGastoPresupuesto").val(canio2);
                $("#proveedorFormularioSolicitudGastoPresupuesto").val(canio2);
            }
            
            $("#ndocumentoFormularioSolicitudGastoPresupuesto").prop('disabled', true);
            //$(".ndocumento").hide();
        }else{
            
            if(ndocumentos != ""){
                $("#ndocumentoFormularioSolicitudGastoPresupuesto").val(ndocumentos);
            }else{
                $("#ndocumentoFormularioSolicitudGastoPresupuesto").val("");
            }

            $("#proveedorFormularioSolicitudGastoPresupuesto").show();
            $(".datetimepickerFechaFF").show();
            $("#cargarPagoOepSolicitud").hide();
            $("#nuevoProveedorPresupuesto").show();
            $("#tinProveedor").show();
            
            $("#ndocumentoFormularioSolicitudGastoPresupuesto").prop('disabled', false);
        }



    });

    $("#seguimientoSolicitudGastoPresupuesto .categoria").on('change',function(){
        area = $("#seguimientoSolicitudGastoPresupuesto .area").val();
        string = $("#seguimientoSolicitudGastoPresupuesto .string").val();
        categoria = $("#seguimientoSolicitudGastoPresupuesto .categoria").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.anio = nanio;
        service.procesar("getProyectosSolicitud",objeto,cargaProyectosSeguimiento);

    });

    function cargaProyectosSolicitud(evt){
        resultado = evt;
        $("#proyectoFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#actividadFormularioSolicitudGastoPresupuesto").val("");
        $("#detalleFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#proyectoFormularioSolicitudGastoPresupuesto" ).append( "<option value='"+ resultado[i].chr_proyecto_id +"'>"+ resultado[i].proyecto +"</option>" );
        }
    }

    function cargaProyectosSeguimiento(evt){
        resultado = evt;
        $("#seguimientoSolicitudGastoPresupuesto .proyecto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#seguimientoSolicitudGastoPresupuesto .detalle").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#seguimientoSolicitudGastoPresupuesto .proyecto" ).append( "<option value='"+ resultado[i].chr_proyecto_id +"'>"+ resultado[i].proyecto +"</option>" );
        }
    }

    function cargaProyectosSolicitudVME(){
        area = $("select#areaFormularioSolicitudGastoPresupuesto").val();
        string = $("select#stringFormularioSolicitudGastoPresupuesto").val();
        categoria = $("select#categoriaFormularioSolicitudGastoPresupuesto").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.anio = nanio;
        service.procesar("getProyectosSolicitud",objeto,function(evt){

            if ( evt == undefined ) return;
            for(var i=0; i<evt.length ; i++){
                $("#proyectoFormularioSolicitudGastoPresupuesto" ).append( "<option value='"+ evt[i].chr_proyecto_id +"'>"+ evt[i].proyecto +"</option>" );
            } 

        });

        if(categoria == "OEP"){
            $("#proveedorFormularioSolicitudGastoPresupuesto").hide();
            $(".datetimepickerFechaFF").hide();
            $("#cargarPagoOepSolicitud").show();
            $("#nuevoProveedorPresupuesto").hide();
            $("#tinProveedor").hide();
            //$("#ndocumentoFormularioSolicitudGastoPresupuesto").val(canio);
            $("#ndocumentoFormularioSolicitudGastoPresupuesto").prop('disabled', true);
            //$(".ndocumento").hide();
        }else{
            $("#proveedorFormularioSolicitudGastoPresupuesto").show();
            $(".datetimepickerFechaFF").show();
            $("#cargarPagoOepSolicitud").hide();
            $("#nuevoProveedorPresupuesto").show();
            $("#tinProveedor").show();
            $("#ndocumentoFormularioSolicitudGastoPresupuesto").val("");
            $("#ndocumentoFormularioSolicitudGastoPresupuesto").prop('disabled', false);
            //$(".ndocumento").show();

        }

    }




    $("#proyectoFormularioSolicitudGastoPresupuesto").on('change',function(){
        area = $("select#areaFormularioSolicitudGastoPresupuesto").val();
        string = $("select#stringFormularioSolicitudGastoPresupuesto").val();
        categoria = $("select#categoriaFormularioSolicitudGastoPresupuesto").val();
        proyecto = $("select#proyectoFormularioSolicitudGastoPresupuesto").val();
        $("#actividadFormularioSolicitudGastoPresupuesto").val(proyecto);
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.proyecto = proyecto;
            objeto.anio = nanio;
        service.procesar("getDetallesSolicitud",objeto,cargaDetallesSolicitud);
    });

    $("#seguimientoSolicitudGastoPresupuesto .proyecto").on('change',function(){
        area = $("#seguimientoSolicitudGastoPresupuesto .area").val();
        string = $("#seguimientoSolicitudGastoPresupuesto .string").val();
        categoria = $("#seguimientoSolicitudGastoPresupuesto .categoria").val();
        proyecto = $("#seguimientoSolicitudGastoPresupuesto .proyecto").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.proyecto = proyecto;
            objeto.anio = nanio;
        service.procesar("getDetallesSolicitud",objeto,cargaDetallesSeguimiento);
    });







    $("#detalleFormularioSolicitudGastoPresupuesto").on('change',function(){
        area = $("select#areaFormularioSolicitudGastoPresupuesto").val();
        string = $("select#stringFormularioSolicitudGastoPresupuesto").val();
        categoria = $("select#categoriaFormularioSolicitudGastoPresupuesto").val();
        proyecto = $("select#proyectoFormularioSolicitudGastoPresupuesto").val();
        comentario = $("select#detalleFormularioSolicitudGastoPresupuesto").val();
        fechaGasto = $("#fechaFormularioSolicitudGastoPresupuesto").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.proyecto = proyecto;
            objeto.comentario = comentario;
            objeto.fecha = fechaGasto;
            objeto.anio = nanio;
        service.procesar("consultarSolicitudSaldoPresupuesto",objeto,resultadoConsultarSolicitudSaldoPresupuesto);
    })

    function resultadoConsultarSolicitudSaldoPresupuesto(evt){
        saldoRegistroActual = 0;
        var resultado = evt;
        var trimestre = evt.trimestre;
        var mes = evt.mes;
        $("#formularioSolicitudGastoPresupuesto .saldoSolicitudPresupuestoM").html("( "+ mes +" ) " + formatNumber.new(parseFloat(corregirNullNumero(resultado.saldo_m)).toFixed(2), "S/. "));
        $("#formularioSolicitudGastoPresupuesto .saldoSolicitudPresupuestoQ").html("( Q-"+ trimestre +" ) " + formatNumber.new(parseFloat(corregirNullNumero(resultado.saldo_q)).toFixed(2), "S/. "));
        $("#formularioSolicitudGastoPresupuesto .saldoSolicitudPresupuestoT").html("( ANUAL ) " + formatNumber.new(parseFloat(corregirNullNumero(resultado.saldo_y)).toFixed(2), "S/. "));

        if(parseFloat(resultado.saldo_m) <= 0){
            saldoRegistroActual = 0;
            //$("#botonGuardarFormularioSolicitud").prop("disabled", true);
            $(".mensajeSaldo").show();
        }else{
            saldoRegistroActual = resultado.saldo_m;
            //$("#botonGuardarFormularioSolicitud").prop("disabled", false);
            $(".mensajeSaldo").hide();
        }


    }




    function cargaDetallesSolicitud(evt){
        resultado = evt;
        $("#detalleFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#detalleFormularioSolicitudGastoPresupuesto" ).append( "<option value='"+ resultado[i].chr_comentario_id +"'>"+ resultado[i].chr_comentario +"</option>" );
        }
    }

    function cargaDetallesSeguimiento(evt){
        resultado = evt;
        $("#seguimientoSolicitudGastoPresupuesto .detalle").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#seguimientoSolicitudGastoPresupuesto .detalle" ).append( "<option value='"+ resultado[i].chr_comentario_id +"'>"+ resultado[i].chr_comentario +"</option>" );
        }
    }


    $("#seguimientoSolicitudGastoPresupuesto .procesarbusqueda").on('click',function(){
        Procesando();

        area = $("#seguimientoSolicitudGastoPresupuesto .area").val(); 
        string = $("#seguimientoSolicitudGastoPresupuesto .string").val();
        categoria = $("#seguimientoSolicitudGastoPresupuesto .categoria").val();
        proyecto = $("#seguimientoSolicitudGastoPresupuesto .proyecto").val();
        detalle = $("#seguimientoSolicitudGastoPresupuesto .detalle").val();
        proveedor = $("#seguimientoSolicitudGastoPresupuesto .proveedor").val();
        documento = $("#seguimientoSolicitudGastoPresupuesto .documento").val();
        estado = $("#seguimientoSolicitudGastoPresupuesto .estado").val();
        carrera = $("#seguimientoSolicitudGastoPresupuesto .carrera").val();
        ampliacion = $("#chkpnuevoSolicitudPresupuesto:checked")["length"];
        sq1 = $("#seguimientoq1:checked")["length"];
        sq2 = $("#seguimientoq2:checked")["length"];
        sq3 = $("#seguimientoq3:checked")["length"];
        sq4 = $("#seguimientoq4:checked")["length"];
        formato = $("#seguimientoSolicitudGastoPresupuesto .formato").val();
        nformato = $("#seguimientoSolicitudGastoPresupuesto .nformato").val();

        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.proyecto = proyecto;
            objeto.detalle = detalle;
            objeto.proveedor = proveedor;
            objeto.documento = documento;
            objeto.estado = estado;
            objeto.carrera = carrera;
            objeto.ampliacion = ampliacion;
            objeto.sq1 = sq1;
            objeto.sq2 = sq2;
            objeto.sq3 = sq3;
            objeto.sq4 = sq4;
            objeto.formato = formato;
            objeto.nformato = nformato;
            objeto.anio = nanio;
        service.procesar("getListaSolicitudGasto",objeto,usuario,cargaListaSolicitudGasto);
    })

    function cargaRegistrosPresupuestoGasto(){
        //Procesando();
        area = $("#seguimientoSolicitudGastoPresupuesto .area").val();
        string = $("#seguimientoSolicitudGastoPresupuesto .string").val();
        categoria = $("#seguimientoSolicitudGastoPresupuesto .categoria").val();
        proyecto = $("#seguimientoSolicitudGastoPresupuesto .proyecto").val();
        detalle = $("#seguimientoSolicitudGastoPresupuesto .detalle").val();
        proveedor = $("#seguimientoSolicitudGastoPresupuesto .proveedor").val();
        documento = $("#seguimientoSolicitudGastoPresupuesto .documento").val();
        estado = $("#seguimientoSolicitudGastoPresupuesto .estado").val();
        sq1 = $("#seguimientoq1:checked")["length"];
        sq2 = $("#seguimientoq2:checked")["length"];
        sq3 = $("#seguimientoq3:checked")["length"];
        sq4 = $("#seguimientoq4:checked")["length"];
        formato = $("#seguimientoSolicitudGastoPresupuesto .formato").val();
        nformato = $("#seguimientoSolicitudGastoPresupuesto .nformato").val();

        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.proyecto = proyecto;
            objeto.detalle = detalle;
            objeto.proveedor = proveedor;
            objeto.documento = documento;
            objeto.estado = estado;
            objeto.sq1 = sq1;
            objeto.sq2 = sq2;
            objeto.sq3 = sq3;
            objeto.sq4 = sq4;
            objeto.formato = formato;
            objeto.nformato = nformato;
            objeto.anio = nanio;
        service.procesar("getListaSolicitudGasto",objeto,usuario,cargaListaSolicitudGasto);
    }

    function cargaDetallesSolicitudVME(){
        area = $("select#areaFormularioSolicitudGastoPresupuesto").val();
        string = $("select#stringFormularioSolicitudGastoPresupuesto").val();
        categoria = $("select#categoriaFormularioSolicitudGastoPresupuesto").val();
        proyecto = $("select#proyectoFormularioSolicitudGastoPresupuesto").val();
        $("#actividadFormularioSolicitudGastoPresupuesto").val(proyecto);
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.proyecto = proyecto;
            objeto.anio = nanio;
            service.procesar("getDetallesSolicitud",objeto,function(evt){
                if ( evt == undefined ) return;
                for(var i=0; i<evt.length ; i++){
                    $("#detalleFormularioSolicitudGastoPresupuesto" ).append( "<option value='"+ evt[i].chr_comentario_id +"'>"+ evt[i].chr_comentario +"</option>" );
                }
            });             

    }

    function cargarSaldoFiltroSolicitudVME(){
        area = $("select#areaFormularioSolicitudGastoPresupuesto").val();
        string = $("select#stringFormularioSolicitudGastoPresupuesto").val();
        categoria = $("select#categoriaFormularioSolicitudGastoPresupuesto").val();
        proyecto = $("select#proyectoFormularioSolicitudGastoPresupuesto").val();
        comentario = $("select#detalleFormularioSolicitudGastoPresupuesto").val();
        fechaGasto = $("#fechaFormularioSolicitudGastoPresupuesto").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.proyecto = proyecto;
            objeto.comentario = comentario;
            objeto.fecha = fechaGasto;
            objeto.anio = nanio;
        service.procesar("consultarSolicitudSaldoPresupuesto",objeto,resultadoConsultarSolicitudSaldoPresupuesto);
    }


    //AL CAMBIAR PROVEEDOR SE TIENE QUE CAMBIAR ESTE DATO

    $("#proveedorFormularioSolicitudGastoPresupuesto").on('change',function(){
        proveedor = $("select#proveedorFormularioSolicitudGastoPresupuesto").val();
        service.procesar("getTinProveedor",proveedor,cargaTinProveedor);
    });

    function cargaTinProveedor(evt){
        resultado = evt;
        $("#tinProveedor").html("");
        if ( resultado == undefined ) return;
        $("#tinProveedor").html("TIN : " + resultado[0].tin);

    }




    function cargaProveedorPresupuesto(evt){
        proveedor = evt;
        $("#proveedorFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#seguimientoSolicitudGastoPresupuesto .proveedor").html("<option value='0'>---SELECCIONAR---</option>");
        for(var i=0; i<proveedor.length ; i++){
            $("#proveedorFormularioSolicitudGastoPresupuesto" ).append( "<option value='"+ proveedor[i].dni_ruc +"'>"+ proveedor[i].nombre_razon_social +"</option>" );
            $("#seguimientoSolicitudGastoPresupuesto .proveedor" ).append( "<option value='"+ proveedor[i].dni_ruc +"'>"+ proveedor[i].nombre_razon_social +"</option>" );
            $("#modal_nuevo_pedido_interno .giraranombrede" ).append( "<option value='"+ proveedor[i].dni_ruc +"'>"+ proveedor[i].nombre_razon_social +"</option>" );
            $("#modal_nueva_acta .proveedor" ).append( "<option value='"+ proveedor[i].dni_ruc +"'>"+ proveedor[i].nombre_razon_social +"</option>" );
            $("#modalProvisionarSolicitudGastoPresupuesto .proveedorOCA" ).append( "<option value='"+ proveedor[i].dni_ruc +"'>"+ proveedor[i].nombre_razon_social +"</option>" );
        }

        //$("#modal_nuevo_pedido_interno .giraranombrede")
        cargaListaProveedorPresupuesto(evt);
    }

    function cargaCarreraPresupuesto(evt){
        carrera = evt;
        $("#carreraFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#seguimientoSolicitudGastoPresupuesto .carrera").html("<option value='0'>---SELECCIONAR---</option>");
        for(var i=0; i<carrera.length ; i++){
            $("#carreraFormularioSolicitudGastoPresupuesto" ).append( "<option value='"+ carrera[i].chr_carrera_id +"'>"+ carrera[i].chr_carrera +"</option>" );
            $("#seguimientoSolicitudGastoPresupuesto .carrera" ).append( "<option value='"+ carrera[i].chr_carrera_id +"'>"+ carrera[i].chr_carrera +"</option>" );
        }
        cargaListaCarreraPresupuesto(evt);
    }

    function cargaListaProveedorPresupuesto(evt){
        listadeProveedores = evt;

        $("#tablaListaProveedorPresupuesto tbody").html("");

        if ( listadeProveedores == undefined ) return;

        for(var i=0; i<listadeProveedores.length ; i++){

            var fila = $("<tr>");
            var celdaBotones = $("<td>");
            datoRegistro = listadeProveedores[i];
            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeProveedores[i].dni_ruc +'</td><td>'+ listadeProveedores[i].nombre_razon_social +'</td><td>'+ listadeProveedores[i].tin +'</td>');

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-primary btn-xs' title='Editar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            btnSeleccionar.data("data",datoRegistro);
            btnSeleccionar.on("click",editarProveedorPresupuesto);

            var btnEliminar = $("<button class='btn btn-danger btn-xs' title='Eliminar'>");
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span');
            btnEliminar.data("data",datoRegistro);
            btnEliminar.on("click",eliminarProveedorPresupuesto);

            contenedorBotones.append(btnSeleccionar);
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#tablaListaProveedorPresupuesto tbody").append(fila);

        }

    }

    function cargaListaCarreraPresupuesto(evt){
        listadeCarreras = evt;

        $("#tablaListaCarreraPresupuesto tbody").html("");

        if ( listadeCarreras == undefined ) return;

        for(var i=0; i<listadeCarreras.length ; i++){

            var fila = $("<tr>");
            var celdaBotones = $("<td>");
            datoRegistro = listadeCarreras[i];
            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeCarreras[i].codigo +'</td><td>'+ listadeCarreras[i].chr_carrera +'</td>');

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-primary btn-xs' title='Editar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            btnSeleccionar.data("data",datoRegistro);
            btnSeleccionar.on("click",editarCarreraPresupuesto);

            var btnEliminar = $("<button class='btn btn-danger btn-xs' title='Eliminar'>");
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span');
            btnEliminar.data("data",datoRegistro);
            btnEliminar.on("click",eliminarCarreraPresupuesto);

            contenedorBotones.append(btnSeleccionar);
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#tablaListaCarreraPresupuesto tbody").append(fila);

        }

    }




    function editarProveedorPresupuesto(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE MODIFICAR DATOS DEL PROVEEDOR : " + data.nombre_razon_social +" ?", function (e) {
            if (e) {
                $("#rucProveedorPresupuesto").val(data.dni_ruc);
                $("#empresaProveedorPresupuesto").val(data.nombre_razon_social);
                $("#tinProveedorPresupuesto").val(data.tin);
                $("#procesoProveedorPresupuesto").val("1");
                $("#codigoProveedorPresupuesto").val(data.idProveedor);
                
                $("#botonGuardarProveedorPresupuesto").html("MODIFICAR");

                alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO DE MODIFICACION");
            }
        });
    }

    function eliminarProveedorPresupuesto(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR DATOS DEL PROVEEDOR : " + data.nombre_razon_social +" ?", function (e) {
            if (e) {
                service.procesar("deleteProveedorPresupuesto",data.idProveedor,resultadoDeleteProveedorPresupuesto);
                alertify.success("HA ACEPTADO ELIMINAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO DE ELIMINACION");
            }
        });
    }

    function resultadoDeleteProveedorPresupuesto(evt){
        service.procesar("getProveedorPresupuesto",usuario,cargaProveedorPresupuesto);
    }


    function editarCarreraPresupuesto(){
        var data = $(this).data("data");
        console.log(data);
        alertify.confirm("¿ SEGURO DE MODIFICAR DATOS DE LA CARRERA : " + data.chr_carrera +" ?", function (e) {
            if (e) {
                $("#codigoCarreraPresupuesto").val(data.codigo);
                $("#descripcionCarreraPresupuesto").val(data.chr_carrera);
                $("#procesoCarreraPresupuesto").val("1");
                $("#idCarreraPresupuesto").val(data.chr_carrera_id);
                
                $("#botonGuardarCarreraPresupuesto").html("MODIFICAR");

                alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO DE MODIFICACION");
            }
        });
    }

    function eliminarCarreraPresupuesto(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR DATOS DE LA CARRERA : " + data.chr_carrera +" ?", function (e) {
            if (e) {
                service.procesar("deleteCarreraPresupuesto",data.chr_carrera_id,resultadoDeleteCarreraPresupuesto);
                alertify.success("HA ACEPTADO ELIMINAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO DE ELIMINACION");
            }
        });
    }

    function resultadoDeleteCarreraPresupuesto(evt){
        service.procesar("getCarreraPresupuesto",usuario,cargaCarreraPresupuesto);
    }


    $("#botonGuardarProveedorPresupuesto").on('click', function(){
        var ruc = $("#rucProveedorPresupuesto").val();
        var empresa = $("#empresaProveedorPresupuesto").val();
        alertify.confirm("¿ SEGURO DE GUARDAR DATOS DEL PROVEEDOR : " + empresa +" ?", function (e) {
            if (e) {
                if (ruc == "" || empresa == ""){
                    alertify.error("EXISTEN CAMPOS VACIOS OBLIGATORIOS");
                }else{
                    var objeto = new Object()
                        objeto.dni = ruc;
                        objeto.nombre = empresa;
                        objeto.tin = $("#tinProveedorPresupuesto").val();
                        objeto.proceso = $("#procesoProveedorPresupuesto").val();
                        objeto.codigo = $("#codigoProveedorPresupuesto").val();
                    service.procesar("saveProveedorPresupuesto",objeto,resultadoSaveProveedorPresupuesto); //CARGA REGISTROS POR USUARIO
                }
            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO DE GUARDADO");
            }
        });
    })

    $("#botonGuardarCarreraPresupuesto").on('click', function(){
        var id = $("#idCarreraPresupuesto").val();
        var codigo = $("#codigoCarreraPresupuesto").val();
        var carrera = $("#descripcionCarreraPresupuesto").val();
        alertify.confirm("¿ SEGURO DE GUARDAR DATOS DE CARRERA : " + carrera +" ?", function (e) {
            if (e) {
                if (carrera == ""){
                    alertify.error("EXISTEN CAMPOS VACIOS OBLIGATORIOS");
                }else{
                    var objeto = new Object()
                        objeto.id = id;
                        objeto.carrera = carrera;
                        objeto.proceso = $("#procesoCarreraPresupuesto").val();
                        objeto.codigo = $("#codigoCarreraPresupuesto").val();
                    service.procesar("saveCarreraPresupuesto",objeto,resultadoSaveCarreraPresupuesto); //CARGA REGISTROS POR USUARIO
                }
            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO DE GUARDADO");
            }
        });
    })

    $("#botonCancelarProveedorPresupuesto").on('click', function(){
        alertify.confirm("¿ SEGURO DE CANCELAR EL PROCESO", function (e) {
            if (e) {
                formularioProveedorEstandar();
                alertify.success("HA ACEPTADO CANCELAR EL REGISTRO");
            } else {
                //alertify.error("HA CANCELADO EL PROCEDIMIENTO DE MODIFICACION");
            }
        });

    })

    $("#botonCancelarCarreraPresupuesto").on('click', function(){
        alertify.confirm("¿ SEGURO DE CANCELAR EL PROCESO", function (e) {
            if (e) {
                formularioCarreraEstandar();
                alertify.success("HA ACEPTADO CANCELAR EL REGISTRO");
            } else {
                //alertify.error("HA CANCELADO EL PROCEDIMIENTO DE MODIFICACION");
            }
        });

    })
    function resultadoSaveProveedorPresupuesto(evt){
        if(evt == 1 ){
            alertify.success("PROVEEDOR GUARDADO SATISFACTORIAMENTE");
            service.procesar("getProveedorPresupuesto",usuario,cargaProveedorPresupuesto);
            formularioProveedorEstandar();
        }else if(evt == 2 ){
            alertify.success("PROVEEDOR MODIFICADO SATISFACTORIAMENTE");
            service.procesar("getProveedorPresupuesto",usuario,cargaProveedorPresupuesto);
            formularioProveedorEstandar();
        }else{
            alertify.error("PROVEEDOR NO GUARDADO"); 
        }
    }

    function resultadoSaveCarreraPresupuesto(evt){
        if(evt == 1 ){
            alertify.success("CARRERA GUARDADA SATISFACTORIAMENTE");
            service.procesar("getCarreraPresupuesto",usuario,cargaCarreraPresupuesto);
            formularioCarreraEstandar();
        }else if(evt == 2 ){
            alertify.success("CARRERA MODIFICADA SATISFACTORIAMENTE");
            service.procesar("getCarreraPresupuesto",usuario,cargaCarreraPresupuesto);
            formularioCarreraEstandar();
        }else{
            alertify.error("CARRERA NO GUARDADA"); 
        }
    }

    function formularioProveedorEstandar(){
        $("#rucProveedorPresupuesto").val("");
        $("#empresaProveedorPresupuesto").val("");
        $("#tinProveedorPresupuesto").val("");
        $("#procesoProveedorPresupuesto").val("0");
        $("#botonGuardarProveedorPresupuesto").html("GUARDAR");
    }

    function formularioCarreraEstandar(){
        $("#idCarreraPresupuesto").val("");
        $("#codigoCarreraPresupuesto").val("");
        $("#descripcionCarreraPresupuesto").val("");
        $("#procesoCarreraPresupuesto").val("0");
        $("#botonGuardarCarreraPresupuesto").html("GUARDAR");
    }

    $("#botonGuardarFormularioSolicitud").on('click', function(){

        var f3 = new Date();
        var nmes3 = (f3.getMonth()+1);
        var nanio3 = $("#anioPresupuestoSolicitud").val();
        var mes3 = f3.getMonth() < 9 ? "0" + (f3.getMonth()+1) : f3.getMonth()+1;
        var dia3 = f3.getDate() < 10 ? "0" + (f3.getDate()) : f3.getDate();
        var hora3 = f3.getHours();
        var minuto3 = f3.getMinutes();
        var segundo3 = f3.getSeconds();
        //var canio3 = "OEP-" + nanio3 +''+ mes3 +''+ dia3 +''+ hora3 +''+ minuto3 +''+ segundo3;

        
        //if($("select#categoriaFormularioSolicitudGastoPresupuesto").val() == "OEP"){
            //$("#ndocumentoFormularioSolicitudGastoPresupuesto").val("OEP-" + canio3);
            //$("#proveedorFormularioSolicitudGastoPresupuesto").val("OEP-" + canio3);
        //}


        var procedimiento = $("#procedimientoSolicitudGastoPresupuesto").html();

        if(procedimiento == "NUEVO"){
            limpiarFormularioSolicitud();
            $("#formularioSolicitudGastoPresupuesto .input-sm").prop('disabled', false);
            service.procesar("getCodigoCorrelativoSolicitud",function(evt){
                nregistro = parseFloat(evt[0].idPresupuesto) + 1;
                $("#idPresupuestoSolicitudGastoPresupuesto").html(nregistro);
            });
            //$("#idPresupuestoSolicitudGastoPresupuesto").val();
            $("#procedimientoSolicitudGastoPresupuesto").html("GUARDAR");
            $("#formularioSolicitudGastoPresupuesto .input-sm").prop('disabled', false);

            $("#estadoFormularioSolicitudGastoPresupuesto").prop('disabled', true); //DESABILITAR EL BOTON ESTADO PARA QUE SIEMPRE CARGUE EL INGRESADO

        }else{


            var idPresupuesto = $("#idPresupuestoSolicitudGastoPresupuesto").html();
            var estado = $("select#estadoFormularioSolicitudGastoPresupuesto").val();
            var fechaGasto  = $("#fechaFormularioSolicitudGastoPresupuesto").val();
            var fechaRegistro = fecha;
            var area = $("select#areaFormularioSolicitudGastoPresupuesto").val();
            var string = $("select#stringFormularioSolicitudGastoPresupuesto").val();
            var categoria = $("select#categoriaFormularioSolicitudGastoPresupuesto").val();
            var proyecto = $("select#proyectoFormularioSolicitudGastoPresupuesto").val();
            var detalle = $("select#detalleFormularioSolicitudGastoPresupuesto").val();

            var carrera = $("select#carreraFormularioSolicitudGastoPresupuesto").val();
            var pnuevo = $("#chkNuevoPresupuestoSolicitudPresupuesto:checked")["length"]; //campo para presupuesto nuevo

            var ndocumento = $("#ndocumentoFormularioSolicitudGastoPresupuesto").val();
            var fechafactura = $("#fechaFacturaFormularioSolicitudGastoPresupuesto").val();

            var idFormato = $("#formularioSolicitudGastoPresupuesto .formato").val();
            var nformato = $("#formularioSolicitudGastoPresupuesto .nformato").val();

            var proveedor = $("select#proveedorFormularioSolicitudGastoPresupuesto").val();

            //var colaboradores2 = Base64.encode("arrayDatosFormularioOep"); //SI EXISTE COLABORADORES EN EL GASTO

            //console.log(colaboradores2);

            var colaboradores = arrayDatosCortosFormularioOep; //arrayDatosFormularioOep; //SI EXISTE COLABORADORES EN EL GASTO

            var actividad = $("#actividadFormularioSolicitudGastoPresupuesto").val();
            var descripcion = $("#descripcionFormularioSolicitudGastoPresupuesto").val();
            var monto = $("#montoFormularioSolicitudGastoPresupuesto").val();
            var moneda = $("select#seleccionarMonedaFormularioSolicitudGastoPresupuesto").val();
            var tipocambio  = $("#tipoCambioFormularioSolicitudGastoPresupuesto").val();
            var total = $("#totalFormularioSolicitudGastoPresupuesto").val();

            //var idFormato = $("#idFormatoFormularioSolicitudGastoPresupuesto").val();
            //var nformato = $("#nformatoFormularioSolicitudGastoPresupuesto").val();

        //monto = 0; //CAMBIAR CUANDO ESTE EN PRODUCCION

        //if(parseFloat(saldoRegistroActual) >= parseFloat(monto)){ 
        //    $(".mensajeSaldo").hide();
            


            if(detalle != "0" && ndocumento != "" && proveedor != "0" && descripcion != "" && monto != ""){

                alertify.confirm("¿ SEGURO DE GUARDAR EL REGISTRO ?", function (e) {
                    if (e) {

                            service.procesar("deleteFormularioSolicitudOep",ndocumento,function(evt){


                                    //AREA DE GUARDADO DE LOS COLABORADORES PRIMERO
                                var fechaGasto  = $("#fechaFormularioSolicitudGastoPresupuesto").val();



                                    for(var i=0; i<arrayDatosCortosFormularioOep.length ; i++){
                                        var objetoFormulario = new Object()
                                            objetoFormulario.ndocumento = ndocumento; //NUMERO DE DOCUMENTO OEP
                                            objetoFormulario.fechaGasto = fechaGasto;
                                            objetoFormulario.dni = arrayDatosCortosFormularioOep[i].dni;
                                            //objetoFormulario.nombre = ""; //arrayDatosCortosFormularioOep[i].nombre;
                                            objetoFormulario.monto = arrayDatosCortosFormularioOep[i].monto;
                                            objetoFormulario.actividad = arrayDatosCortosFormularioOep[i].actividad;
                                            objetoFormulario.sede = arrayDatosCortosFormularioOep[i].sede;
                                            objetoFormulario.observaciones = arrayDatosCortosFormularioOep[i].observaciones;
                                            objetoFormulario.usuario = usuario;
                                        service.procesar("saveFormularioSolicitudOep",objetoFormulario,function(evt){
                                                alertify.success(evt + " REGISTRADO");
                                        });
                                        //window.delay(500);
                                        //console.log(i);
                                    }
                                


                                
                                //AREA DE GUARDADO DEL FORMULARIO GENERAL
                                var objetoFormulario = new Object()
                                    objetoFormulario.nregistro = nregistro;
                                    objetoFormulario.procedimiento = procedimiento;
                                    objetoFormulario.idPresupuesto = idPresupuesto;
                                    objetoFormulario.estado = estado;
                                    objetoFormulario.fechaGasto = fechaGasto;
                                    objetoFormulario.fechaRegistro = fechaRegistro;
                                    objetoFormulario.area = area;
                                    objetoFormulario.string = string;
                                    objetoFormulario.categoria = categoria;
                                    objetoFormulario.proyecto = proyecto;
                                    objetoFormulario.comentario = detalle;
                                    objetoFormulario.carrera = carrera;

                                    objetoFormulario.pnuevo = pnuevo;

                                    objetoFormulario.ndocumento = ndocumento; //NUMERO DE DOCUMENTO OEP
                                    objetoFormulario.fechafactura = fechafactura;

                                    objetoFormulario.proveedor = proveedor;
                                    //objetoFormulario.colaboradores = colaboradores; //LISTA DE COLABORADORES
                                    objetoFormulario.actividad = actividad;
                                    objetoFormulario.descripcion = descripcion;
                                    objetoFormulario.monto = monto;
                                    objetoFormulario.moneda = moneda;
                                    objetoFormulario.tipocambio = tipocambio;
                                    objetoFormulario.total = total;
                                    
                                    objetoFormulario.idFormato = idFormato;
                                    objetoFormulario.nformato = nformato;
                                    objetoFormulario.usuario = usuario;

                                    service.procesar("saveFormularioSolicitud",objetoFormulario,cargaFormularioSolicitud);                                                 
                                    alertify.success("HA ACEPTADO GUARDAR EL REGISTRO");
                            })

                    } else {
                        alertify.error("HA CANCELADO EL PROCESO");
                    }
                });

                //deleteFormularioSolicitudOep();

            }else{
                alertify.error("COMPLETAR INFORMACION DEL FORMULARIO");
            }

        //}else{
        //    $(".mensajeSaldo").show();
        //    alertify.error("NO CUENTA CON SALDO SUFICIENTE PARA REGISTRAR ESTA SOLICITUD");
        //}



        }

    })

    function deleteFormularioSolicitudOep(){

        service.procesar("deleteFormularioSolicitudOep",ndocumento,function(evt){
            //AREA DE GUARDADO DE LOS COLABORADORES PRIMERO
            var fechaGasto  = $("#fechaFormularioSolicitudGastoPresupuesto").val();
            for(var i=0; i<arrayDatosCortosFormularioOep.length ; i++){
                var objetoFormulario = new Object()
                    objetoFormulario.ndocumento = ndocumento; //NUMERO DE DOCUMENTO OEP
                    objetoFormulario.fechaGasto = fechaGasto;
                    objetoFormulario.dni = arrayDatosCortosFormularioOep[i].dni;
                    objetoFormulario.nombre = arrayDatosCortosFormularioOep[i].nombre;
                    objetoFormulario.monto = arrayDatosCortosFormularioOep[i].monto;
                    objetoFormulario.actividad = arrayDatosCortosFormularioOep[i].actividad;
                    objetoFormulario.sede = arrayDatosCortosFormularioOep[i].sede;
                    objetoFormulario.observaciones = arrayDatosCortosFormularioOep[i].observaciones;
                    objetoFormulario.usuario = usuario;
                service.procesar("saveFormularioSolicitudOep",objetoFormulario,function(evt){
                                                        //alertify.success(nombre + " - REGISTRADO");
                });
            }
        })
    }


    $("#cancelarGuardarFormularioSolicitud").click(function(){
        alertify.confirm("¿ SEGURO DE CANCELAR LA MODIFICACION ?", function (e) {
            if (e) {
                alertify.error("EL PROCEDIMIENTO HA SIDO CANCELADO");
                limpiarFormularioSolicitud();
                //activaTab('seguimientoSolicitudGastoPresupuesto');
            }else{

            }
        });

    })

    $("#botonFormularioSolicitudGastoPresupuesto").on('click', function(){
        limpiarFormularioSolicitud();
    })

    function cargaFormularioSolicitud(evt){
        //Terminado();
        resultado = evt;
        if ( resultado == undefined ) return;
        alertify.success("FORMULARIO DE SOLICITUD GUARDADO SATISFACTORIAMENTE");
        
        limpiarFormularioSolicitud();
        //listarRegistrosGastoPresupuesto();
        cargaRegistrosPresupuestoGasto();

    }



    function limpiarFormularioSolicitud(){
        ndocumentos = "";
        $("#procedimientoSolicitudGastoPresupuesto").html("NUEVO");
        $("#botonGuardarFormularioSolicitud").prop("disabled", false);
        $("#mensajeFormularioGastoPresupuesto").html("FORMULARIO INGRESO DE NUEVO REGISTRO");

        $("#estadoFormularioSolicitudGastoPresupuesto option[value='INGRESADO']").attr("selected",true);
        $("#areaFormularioSolicitudGastoPresupuesto").val(0);
        $("#stringFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#categoriaFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#proyectoFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#detalleFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");

        $("#carreraFormularioSolicitudGastoPresupuesto").val(0);
        //$("#chkNuevoPresupuestoSolicitudPresupuesto").val(0);
        $("#chkNuevoPresupuestoSolicitudPresupuesto").prop("checked", false); 


        $("#ndocumentoFormularioSolicitudGastoPresupuesto").val("");
        $("#ndocumentoFormularioSolicitudGastoPresupuesto").prop('disabled', false);



        $("#formularioSolicitudGastoPresupuesto .formato").val("");
        $("#formularioSolicitudGastoPresupuesto .nformato").val("");

        $("#proveedorFormularioSolicitudGastoPresupuesto").val(0);
        $("#tinProveedor").html("");

        $("#actividadFormularioSolicitudGastoPresupuesto").val("");
        $("#descripcionFormularioSolicitudGastoPresupuesto").val("");
        $("#montoFormularioSolicitudGastoPresupuesto").val("");
        $("#seleccionarMonedaFormularioSolicitudGastoPresupuesto").val("SOLES");
        cambioMoneda("SOLES");
        $("#tipoCambioFormularioSolicitudGastoPresupuesto").val("");
        $("#totalFormularioSolicitudGastoPresupuesto").val("");

        $("#idFormatoFormularioSolicitudGastoPresupuesto").val("");
        $("#nformatoFormularioSolicitudGastoPresupuesto").val("");

        $("#proveedorFormularioSolicitudGastoPresupuesto").show();
        $("#cargarPagoOepSolicitud").hide();
        $("#nuevoProveedorPresupuesto").show();
        $("#tinProveedor").show();
        $("#formularioSolicitudGastoPresupuesto .input-sm").prop('disabled', true);

        $("#idPresupuestoSolicitudGastoPresupuesto").html("");

        //console.log("aqui");

        cargaRegistrosFormularioOep();

    }




    $("#seleccionarMonedaFormularioSolicitudGastoPresupuesto").on('change',function(){
        dato = $("select#seleccionarMonedaFormularioSolicitudGastoPresupuesto").val();
        cambioMoneda(dato);
    });

    function cambioMoneda(dato){
        if (dato == "DOLARES"){
            $(".tipocambio").show();
        }else{
            $(".tipocambio").hide();
        }
    }


    $("#recalcularFormularioSolicitudGastoPresupuesto").click(function(){
        monto =  parseFloat($("#montoFormularioSolicitudGastoPresupuesto").val());
        tipocambio =  parseFloat($("#tipoCambioFormularioSolicitudGastoPresupuesto").val());
        total = monto * tipocambio;
        $("#totalFormularioSolicitudGastoPresupuesto").val(total);
    })

    $("#modalProvisionarSolicitudGastoPresupuesto .recalcularOCA").on('click', function(){
        console.log("local");
        monto =  parseFloat($("#modalProvisionarSolicitudGastoPresupuesto .montoOCA").val());
        tipocambio =  parseFloat($("#modalProvisionarSolicitudGastoPresupuesto .tipocambioOCA").val());
        total = monto * tipocambio;
        $("#modalProvisionarSolicitudGastoPresupuesto .totalOCA").val(total);
    })



    //AREA DE FORMULARIO OEP DE COLABORADORES

    function cargaColaboradorOepPresupuesto(evt){
        colaborador_registros = evt;
        $("#nombreOepPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        for(var i=0; i<colaborador_registros.length ; i++){
            $("#nombreOepPresupuesto" ).append( "<option value='"+ colaborador_registros[i].dni +"'>"+ colaborador_registros[i].nombre +"</option>" );
        }

        cargarListaColaboradoresOepPresupuesto(evt);
    }

    function cargaActividadOepPresupuesto(evt){
        activdad_registros = evt;
        $("#actividadOepPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        for(var i=0; i<activdad_registros.length ; i++){
            $("#actividadOepPresupuesto" ).append( "<option value='"+ activdad_registros[i].idActividad +"'>"+ activdad_registros[i].actividad +"</option>" );
        }

        cargarListaActividadesOepPresupuesto(evt);
    }

    function cargaSedePresupuesto(evt){
        sede = evt;
        $("#sedeOepPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        for(var i=0; i<sede.length ; i++){
            $("#sedeOepPresupuesto" ).append( "<option value='"+ sede[i].sede +"'>"+ sede[i].sede +"</option>" );
        }
    }

    $("#guardarPagoOepSolicitud").on('click', function(){
        alertify.confirm("¿ SEGURO DE GUARDAR EL REGISTRO", function (e) {
            if (e) {
                var dni = $("select#nombreOepPresupuesto").val();
                var nombre = $("select#nombreOepPresupuesto option:selected").text();
                var monto = $("#montoOepPresupuesto").val();
                var actividad = $("select#actividadOepPresupuesto").val();
                var desActividad = $("select#actividadOepPresupuesto option:selected").text();
                var sede = $("select#sedeOepPresupuesto").val();
                var observaciones = $("#observacionesOepPresupuesto").val();
                var posicion = $("#posicionOepPresupuesto").val();

                    if (dni == "0" || nombre == "---SELECCIONAR---" || monto == "" || actividad == "0" || sede == "0"){
                        alertify.error("EXISTEN CAMPOS VACIOS OBLIGATORIOS");
                    }else{
                        if($("#guardarPagoOepSolicitud").html() == "REGISTRAR"){
                            arrayDatosFormularioOep.push({dni:dni,nombre:nombre,monto:monto,actividad:actividad,desActividad:desActividad,sede:sede,observaciones:observaciones});                            
                            arrayDatosCortosFormularioOep.push({dni:dni,nombre:nombre,monto:monto,actividad:actividad,sede:sede,observaciones:observaciones});                                                        
                            //arrayDatosFormularioOep.push({dni:dni,monto:monto,actividad:actividad,desActividad:desActividad,sede:sede,observaciones:observaciones});
                            cargaRegistrosFormularioOep(arrayDatosFormularioOep);

                            alertify.success("SE HA GUARDADO EL REGISTRO SATISFACTORIAMENTE");
                            formularioRegistroEstandar(); 
                        }else if($("#guardarPagoOepSolicitud").html() == "MODIFICAR"){
                            arrayDatosFormularioOep[posicion] = ({dni:dni,nombre:nombre,monto:monto,actividad:actividad,desActividad:desActividad,sede:sede,observaciones:observaciones});
                            arrayDatosCortosFormularioOep[posicion] = ({dni:dni,nombre:nombre,monto:monto,actividad:actividad,sede:sede,observaciones:observaciones});                           
                            //arrayDatosFormularioOep[posicion] = ({dni:dni,monto:monto,actividad:actividad,desActividad:desActividad,sede:sede,observaciones:observaciones});
                            cargaRegistrosFormularioOep(arrayDatosFormularioOep);

                            alertify.success("SE HA MODIFICADO EL REGISTRO SATISFACTORIAMENTE");
                            formularioRegistroEstandar();    
                        }
                         
                    }

            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    })

    $("#cancelarPagoOepSolicitud").on('click', function(){
        alertify.confirm("¿ SEGURO DE CANCELAR Y LIMPIAR EL FORMULARIO ?", function (e) {
            if (e) {
                formularioRegistroEstandar()
                alertify.success("HA CANCELADO EL PROCEDIMIENTO DE NUEVO REGISTRO OEP");
            } else {
                //alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    })


    //function guardarRegistroOEP(){
    //}


    function cargaRegistrosFormularioOep(evt){
        listadeRegistros = evt;
        var total = 0.00;
        arrayDatosFormularioOep = [];
        arrayDatosCortosFormularioOep = [];
        $("#tablaListaPagoOep tbody").html("");

        if ( listadeRegistros == undefined ) return;

        for(var i=0; i<listadeRegistros.length ; i++){
        
            var fila = $("<tr>");
            var celdaBotones = $("<td>");
            datoRegistro = listadeRegistros[i];

            //CARGA DE VALORES AL ARRAY
            var dni = listadeRegistros[i].dni;
            var nombre = listadeRegistros[i].nombre;
            var monto = listadeRegistros[i].monto;
            var actividad = listadeRegistros[i].actividad;
            var desActividad = listadeRegistros[i].desActividad;
            var sede = listadeRegistros[i].sede;
            var observaciones = listadeRegistros[i].observaciones;

            arrayDatosFormularioOep.push({dni:dni,nombre:nombre,monto:monto,actividad:actividad,desActividad:desActividad,sede:sede,observaciones:observaciones});
            arrayDatosCortosFormularioOep.push({dni:dni,nombre:nombre,monto:monto,actividad:actividad,sede:sede,observaciones:observaciones});
            //arrayDatosFormularioOep.push({dni:dni,monto:monto,actividad:actividad,desActividad:desActividad,sede:sede,observaciones:observaciones});
            //FINAL DE PROCESO
            fila.html('<td>'+ (i + 1) +'</td><td>'+ dni +'</td><td>'+ nombre +'</td><td>'+ monto +'</td><td>'+ desActividad +'</td><td>'+ sede +'</td><td>'+ observaciones +'</td>'); //<td>'+ estadoLetra +'</td>

            total = total + parseFloat(listadeRegistros[i].monto);

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-primary btn-xs' title='Editar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            btnSeleccionar.data("data",i);
            btnSeleccionar.on("click",editarRegistroPagoOep);

            var btnEliminar = $("<button class='btn btn-danger btn-xs' title='Eliminar'>");
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span');
            btnEliminar.data("data",i);
            btnEliminar.on("click",eliminarRegistroPagoOep);

            contenedorBotones.append(btnSeleccionar);
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#tablaListaPagoOep tbody").append(fila);

        }

        $("#tablaListaPagoOep tbody").append('<tr class="semaforo_final"><td></td><td></td><td class="negrita centrado">TOTAL</td><td class="negrita">'+ (total).toFixed(2) +'</td><td></td><td></td><td></td><td></td></tr>');
        $("#montoFormularioSolicitudGastoPresupuesto").val( (total).toFixed(2) );

    }

    function editarRegistroPagoOep(){
        var posicion = $(this).data();
        alertify.confirm("¿ SEGURO DE MODIFICAR EL REGISTRO", function (e) {
            if (e) {
                var registro = arrayDatosFormularioOep[posicion.data];

                $("#nombreOepPresupuesto").val(registro.dni);
                $("#montoOepPresupuesto").val(registro.monto);
                $("#actividadOepPresupuesto").val(registro.actividad);
                $("#sedeOepPresupuesto").val(registro.sede);
                $("#observacionesOepPresupuesto").val(registro.observaciones);
                $("#posicionOepPresupuesto").val(posicion.data);

                $("#guardarPagoOepSolicitud").html("MODIFICAR");

                alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    }

    function eliminarRegistroPagoOep(){
        var posicion = $(this).data();
        alertify.confirm("¿ SEGURO DE ELIMINAR EL REGISTRO", function (e) {
            if (e) {
                arrayDatosFormularioOep.splice(posicion.data, 1);
                arrayDatosCortosFormularioOep.splice(posicion.data, 1); //ELIMINAR DEL ARREGLO CORTO
                registros = arrayDatosFormularioOep.length;

                alertify.success("SE HA ELIMINADO EL REGISTRO SATISFACTORIAMENTE");
                cargaRegistrosFormularioOep(arrayDatosFormularioOep);
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    }

    function formularioRegistroEstandar(){
       $("select#nombreOepPresupuesto").val("0");
       $("#montoOepPresupuesto").val("");
       $("select#actividadOepPresupuesto").val("0");
       $("select#sedeOepPresupuesto").val("0");
       $("#observacionesOepPresupuesto").val("");
       $("#guardarPagoOepSolicitud").html("REGISTRAR");
    }


    //FINAL FORMULARIO OEP COLABORADORES


    //INICIO AREA DE FORMULARIO DE COLABORADORES
    function cargarListaColaboradoresOepPresupuesto(evt){
        listadeColaboradores = evt;
        var estadoLetra = "";

        $("#tablaListaColaboradoresPagoOep tbody").html("");

        if ( listadeColaboradores == undefined ) return;

        for(var i=0; i<listadeColaboradores.length ; i++){

            if(listadeColaboradores[i].estado == "1"){
                var fila = $("<tr>");
                estadoLetra = "ACTIVADO";
            }else if(listadeColaboradores[i].estado == "2"){
                var fila = $("<tr class='semaforo_rojo'>");
                estadoLetra = "SUSPENDIDO";
            }

            
            var celdaBotones = $("<td>");
            datoRegistro = listadeColaboradores[i];
            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeColaboradores[i].dni +'</td><td>'+ listadeColaboradores[i].nombre +'</td>'); //<td>'+ estadoLetra +'</td>

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-primary btn-xs' title='Editar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            btnSeleccionar.data("data",datoRegistro);
            btnSeleccionar.on("click",editarColaboradoresPagoOep);

            var btnEliminar = $("<button class='btn btn-danger btn-xs' title='Eliminar'>");
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span');
            btnEliminar.data("data",datoRegistro);
            btnEliminar.on("click",eliminarColaboradoresPagoOep);

            contenedorBotones.append(btnSeleccionar);
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#tablaListaColaboradoresPagoOep tbody").append(fila);

        }
    }

    function editarColaboradoresPagoOep(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE MODIFICAR DATOS DEL COLABORADOR : " + data.nombre +" ?", function (e) {
            if (e) {
                $("#dniColaboradoresPagoOep").val(data.dni);
                $("#nombreColaboradoresPagoOep").val(data.nombre);
                $("select#estadoColaboradoresPagoOep").val(data.estado);
                $("#procesoColaboradoresPagoOep").val("1");
                $("#codigoColaboradoresPagoOep").val(data.idColaborador);
                
                $("#botonGuardarColaboradoresPagoOep").html("MODIFICAR");

                alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO DE MODIFICACION");
            }
        });
    }

    function eliminarColaboradoresPagoOep(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR DATOS DEL COLABORADOR : " + data.nombre +" ?", function (e) {
            if (e) {
                service.procesar("deleteColaboradorOepPresupuesto",data.idColaborador,resultadoDeleteColaboradorOepPresupuesto);
                alertify.success("HA ACEPTADO ELIMINAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO DE ELIMINACION");
            }
        });
    }

    function resultadoDeleteColaboradorOepPresupuesto(evt){
        service.procesar("getColaboradorOepPresupuesto",usuario,cargaColaboradorOepPresupuesto);
    }

    $("#botonGuardarColaboradoresPagoOep").on('click', function(){
        var dni = $("#dniColaboradoresPagoOep").val();
        var nombre = $("#nombreColaboradoresPagoOep").val();
        alertify.confirm("¿ SEGURO DE GUARDAR LA ACTIVIDAD : " + nombre +" ?", function (e) {
            if (e) {
                if (dni == "" || nombre == ""){
                    alertify.error("EXISTEN CAMPOS VACIOS OBLIGATORIOS");
                }else{
                    var objeto = new Object()
                        objeto.dni = dni;
                        objeto.nombre = nombre;
                        objeto.estado = $("select#estadoColaboradoresPagoOep").val();
                        objeto.proceso = $("#procesoColaboradoresPagoOep").val();
                        objeto.codigo = $("#codigoColaboradoresPagoOep").val();
                    service.procesar("saveColaboradorOepPresupuesto",objeto,resultadoSaveColaboradorOepPresupuesto);
                }
            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO DE MODIFICACION");
            }
        });
    })

    $("#botonCancelarColaboradoresPagoOep").on('click', function(){
        alertify.confirm("¿ SEGURO DE CANCELAR EL PROCESO", function (e) {
            if (e) {
                formularioColaboradorEstandar();
                alertify.success("HA ACEPTADO CANCELAR EL REGISTRO");
            } else {
                //alertify.error("HA CANCELADO EL PROCEDIMIENTO DE MODIFICACION");
            }
        });

    })

    function resultadoSaveColaboradorOepPresupuesto(evt){
        if(evt == 1 ){
            alertify.success("COLABORADOR GUARDADO SATISFACTORIAMENTE");
            service.procesar("getColaboradorOepPresupuesto",usuario,cargaColaboradorOepPresupuesto);
            formularioColaboradorEstandar();
        }else if(evt == 2 ){
            alertify.success("COLABORADOR MODIFICADO SATISFACTORIAMENTE");
            service.procesar("getColaboradorOepPresupuesto",usuario,cargaColaboradorOepPresupuesto);
            formularioColaboradorEstandar();
        }else{
            alertify.error("COLABORADOR NO GUARDADO"); 
        }
    }

    function formularioColaboradorEstandar(){
        $("#dniColaboradoresPagoOep").val("");
        $("#nombreColaboradoresPagoOep").val("");
        $("select#estadoColaboradoresPagoOep").val(1);
        $("#procesoColaboradoresPagoOep").val("0");
        $("#botonGuardarColaboradoresPagoOep").html("GUARDAR");
    }

    //FINAL AREA DE COLABORADORES


    //INICIO AREA DE ACTIVIDADES

    function cargarListaActividadesOepPresupuesto(evt){
        listadeActividades = evt;
        var estadoLetra = "";

        $("#tablaListaActividadPagoOep tbody").html("");

        if ( listadeActividades == undefined ) return;

        for(var i=0; i<listadeActividades.length ; i++){

            if(listadeActividades[i].estado == "1"){
                var fila = $("<tr>");
                estadoLetra = "ACTIVADO";
            }else if(listadeActividades[i].estado == "2"){
                var fila = $("<tr class='semaforo_rojo'>");
                estadoLetra = "SUSPENDIDO";
            }

            
            var celdaBotones = $("<td>");
            datoRegistro = listadeActividades[i];
            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeActividades[i].actividad +'</td>'); //<td>'+ estadoLetra +'</td>

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-primary btn-xs' title='Editar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            btnSeleccionar.data("data",datoRegistro);
            btnSeleccionar.on("click",editarActividadPagoOep);

            var btnEliminar = $("<button class='btn btn-danger btn-xs' title='Eliminar'>");
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span');
            btnEliminar.data("data",datoRegistro);
            btnEliminar.on("click",eliminarActividadPagoOep);

            contenedorBotones.append(btnSeleccionar);
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#tablaListaActividadPagoOep tbody").append(fila);

        }
    }

    function editarActividadPagoOep(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE MODIFICAR DATOS DE LA ACTIVIDAD : " + data.actividad +" ?", function (e) {
            if (e) {
                $("#nombreActividadPagoOep").val(data.actividad);
                $("#procesoActividadPagoOep").val("1");
                $("#codigoActividadPagoOep").val(data.idActividad);
                
                $("#botonGuardarActividadPagoOep").html("MODIFICAR");

                alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO DE MODIFICACION");
            }
        });
    }

    function eliminarActividadPagoOep(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR DATOS DE LA : " + data.actividad +" ?", function (e) {
            if (e) {
                service.procesar("deleteActividadOepPresupuesto",data.idActividad,resultadoDeleteActividadOepPresupuesto);
                alertify.success("HA ACEPTADO ELIMINAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO DE ELIMINACION");
            }
        });
    }

    function resultadoDeleteActividadOepPresupuesto(evt){
        service.procesar("getActividadOepPresupuesto",usuario,cargaActividadOepPresupuesto);
    }

    $("#botonGuardarActividadPagoOep").on('click', function(){
        var actividad = $("#nombreActividadPagoOep").val();
        alertify.confirm("¿ SEGURO DE GUARDAR LA ACTIVIDAD : " + actividad +" ?", function (e) {
            if (e) {
                if (actividad == ""){
                    alertify.error("EXISTEN CAMPOS VACIOS OBLIGATORIOS");
                }else{
                    var objeto = new Object()
                        objeto.actividad = actividad;
                        objeto.proceso = $("#procesoActividadPagoOep").val();
                        objeto.codigo = $("#codigoActividadPagoOep").val();
                    service.procesar("saveActividadOepPresupuesto",objeto,resultadoSaveActividadOepPresupuesto);
                }
            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO DE GUARDADO");
            }
        });
    })

    $("#botonCancelarActividadPagoOep").on('click', function(){
        alertify.confirm("¿ SEGURO DE CANCELAR EL PROCESO", function (e) {
            if (e) {
                formularioActividadEstandar();
                alertify.success("HA ACEPTADO CANCELAR EL REGISTRO");
            } else {
                //alertify.error("HA CANCELADO EL PROCEDIMIENTO DE MODIFICACION");
            }
        });

    })

    function resultadoSaveActividadOepPresupuesto(evt){
        if(evt == 1 ){
            alertify.success("ACTIVIDAD GUARDADA SATISFACTORIAMENTE");
            service.procesar("getActividadOepPresupuesto",usuario,cargaActividadOepPresupuesto);
            formularioActividadEstandar();
        }else if(evt == 2 ){
            alertify.success("ACTIVIDAD MODIFICADA SATISFACTORIAMENTE");
            service.procesar("getActividadOepPresupuesto",usuario,cargaActividadOepPresupuesto);
            formularioActividadEstandar();
        }else{
            alertify.error("ACTIVIDAD NO GUARDADA"); 
        }
    }

    function formularioActividadEstandar(){
        $("#nombreActividadPagoOep").val("");
        $("#procesoActividadPagoOep").val("0");
        $("#botonGuardarActividadPagoOep").html("GUARDAR");
    }

    //FINAL AREA DE ACTIVIDADES



    function listarRegistrosGastoPresupuesto(){
        //cargaRegistrosPresupuestoGasto();
        //service.procesar("getListaSolicitudGasto",0,cargaListaSolicitudGasto);
    }




    function cargaListaSolicitudGasto(evt){
        Terminado();
        resultado = evt;

        $("#tablaListadeSolicitudGastosdePresupuesto tbody").html("");

        if ( resultado == undefined ) return;
            //ultimoregistro = parseFloat(resultado[0].idPresupuesto) + 1;
            //$("#numeroAutogeneradoGastoPresupuesto").val(ultimoregistro);
        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            if((resultado[i].estado == "APROBADO")){ //corregirNull(resultado[i].nformato) != "" && 
                eliminarRegistroDesabilitado = "none";
                //console.log(resultado[i].idPresupuesto + " - " + resultado[i].estado);
            }else{ 
                eliminarRegistroDesabilitado = ""; 
            }


            var formato = corregirNull(resultado[i].idFormato);
            var claseFormato = "";

            if(formato == "PIN"){ claseFormato = "formatoPin"; }
            if(formato == "ACT"){ claseFormato = "formatoAct"; }
            if(formato == "OEP"){ claseFormato = "formatoOep"; }
            if(formato == "OCA"){ claseFormato = "formatoOca"; }
            if(formato == "PRO"){ claseFormato = "formatoPro"; }

            if(resultado[i].ampliacion == "1"){ claseFormato = "formatoAmp"; }

            var fila = $("<tr class='"+claseFormato+"'>");
            var celdaBotones = $("<td>");
            var total = total + parseFloat(resultado[i].total);
            var subtotal = parseFloat(resultado[i].total);

            var oep = resultado[i].proveedor.indexOf("OEP")
            var desproveedor = resultado[i].proveedor;
            if (oep != 0){
                desproveedor = resultado[i].nombre_razon_social;
            }else{

            }
            //(i + 1)
            fila.html('<td>'+ resultado[i].idPresupuesto +'</td><td>'+ resultado[i].chr_area.toUpperCase() +'</td><td>'+ resultado[i].chr_string_id +'</td><td>'+ resultado[i].chr_string.toUpperCase() +'</td><td>'+ resultado[i].chr_categoria_id +'</td><td>'+ resultado[i].chr_categoria.toUpperCase() +'</td><td>'+ corregirNull(resultado[i].documento) +'</td><td>'+ formatNumber.new(subtotal.toFixed(2), "") +'</td><td>'+ corregirNull(desproveedor) +'</td><td>'+ (resultado[i].descripcion).toUpperCase() +'</td><td>'+ resultado[i].idFormato +'</td><td>'+ corregirNull(resultado[i].estado) +'</td><td>'+ corregirNull(resultado[i].fechaGasto) +'</td>'); //<td>'+ resultado[i].descripcion +'</td>

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnVisualizar = $("<button class='btn btn-primary btn-xs' title='Ver'>");
            var btnModificar = $("<button class='btn btn-success btn-xs' title='Modificar' style='display:"+eliminarRegistroDesabilitado+"'>");
            var btnAsignarFormato = $("<button class='btn btn-info btn-xs' title='Asignar Formato' style='display:"+eliminarRegistroDesabilitado+"'>");
            var btnEliminar = $("<button class='btn btn-danger btn-xs' title='Eliminar' style='display:"+eliminarRegistroDesabilitado+"'>");

            btnVisualizar.html('<span class="glyphicon glyphicon-eye-open"></span');
            btnModificar.html('<span class="glyphicon glyphicon-edit"></span');
            btnAsignarFormato.html('<span class="glyphicon glyphicon-list-alt"></span');
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span');

            btnVisualizar.data("data",datoRegistro);
            btnModificar.data("data",datoRegistro);
            btnAsignarFormato.data("data",datoRegistro);
            btnEliminar.data("data",datoRegistro);

            btnVisualizar.on("click",visualizarRegistroSolicitudGasto);
            btnModificar.on("click",modificarRegistroSolicitudGasto);
            btnAsignarFormato.on("click",asignarFormatoRegistroSolicitudGasto);
            btnEliminar.on("click",eliminarRegistroSolicitudGasto);

            contenedorBotones.append(btnVisualizar);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnModificar);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnAsignarFormato);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#tablaListadeSolicitudGastosdePresupuesto tbody").append(fila);

        }
            //$("#tablaListadeSolicitudGastosdePresupuesto tfoot").html("<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td class='negrita'>TOTAL</td><td class=''>"+formatNumber.new(total.toFixed(2), "")+"</td><td></td><td></td><td></td><td></td><td></td></tr>");
        //Terminado();

    }
    //FINAL DEL AREA

    function asignarFormatoRegistroSolicitudGasto(){
        var data = $(this).data("data");
        arrayRegistroFormato = data;
        $("#modal_lista_formatos").modal("show");

        listarFormatosModal();

    }

    function listarFormatosModal(){
        var formato = $("#modal_lista_formatos .formato").val();     
        service.procesar("getTablaFormatos",formato,nanio,resultadoCargaFormatos);
    }

    function resultadoCargaFormatos(evt){
            //CARGA DE DATOS EN LA TABLA DE FORMATOS
            listadeRegistros = evt;
            var formato = $("#modal_lista_formatos .formato").val();
            $("#modal_lista_formatos .tablaformatos tbody").html("");

            if(corregirNull(arrayRegistroFormato.nformato) != "" || formato == "OCA" || formato == "PRO"){ 
                $("#modal_lista_formatos .nuevoformato").css("display","none"); 
            }else{ 
                $("#modal_lista_formatos .nuevoformato").css("display",""); 
            }

            if ( listadeRegistros == undefined ) return;

            for(var i=0; i<listadeRegistros.length ; i++){
                datoRegistro = listadeRegistros[i];

                if(listadeRegistros[i].numero == arrayRegistroFormato.nformato){ claseFormato = "formatoPin"; quitarFormatoDesabilitado =""; }else{ claseFormato = ""; quitarFormatoDesabilitado ="none"; }

                if(arrayRegistroFormato.proveedor == listadeRegistros[i].rucempresa){ asignarFormatoDesabilitado =""; }else{ asignarFormatoDesabilitado ="none"; }

                var fila = $("<tr class='"+claseFormato+"'>");
                var celdaBotones = $("<td>");

                fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeRegistros[i].numero +'</td><td>'+ listadeRegistros[i].emision +'</td><td>'+ listadeRegistros[i].nombre_razon_social +'</td><td>'+ listadeRegistros[i].moneda +'</td><td>'+ listadeRegistros[i].total +'</td><td>'+ listadeRegistros[i].estado +'</td>');

                var contenedorBotones = $("<td><div class='input-group-btn'>");

                var btnSeleccionar = $("<button class='btn btn-success btn-xs' title='Asignar Formato' style='display:"+asignarFormatoDesabilitado+"'>");
                btnSeleccionar.html('<span class="glyphicon glyphicon-ok"></span');

                var btnEliminar = $("<button class='btn btn-danger btn-xs' title='Quitar Formato' style='display:"+quitarFormatoDesabilitado+"'>");
                btnEliminar.html('<span class="glyphicon glyphicon-remove"></span');

                var btnVisualizar = $("<button class='btn btn-info btn-xs' title='Visualizar'>");
                btnVisualizar.html('<span class="glyphicon glyphicon-eye-open"></span');

                btnSeleccionar.data("data",datoRegistro);
                btnSeleccionar.on("click",asignarFormato);

                btnEliminar.data("data",datoRegistro);
                btnEliminar.on("click",quitarFormato);

                btnVisualizar.data("data",datoRegistro);
                btnVisualizar.on("click",verFormato);

                if(listadeRegistros[i].formato != "ACT"){
                    contenedorBotones.append(btnSeleccionar);
                    contenedorBotones.append(" ");
                    contenedorBotones.append(btnEliminar);
                    contenedorBotones.append(" ");
                }

                contenedorBotones.append(btnVisualizar);

                fila.append(contenedorBotones);

                $("#modal_lista_formatos .tablaformatos tbody").append(fila);

            }

    }

    function verFormato(){
        var data = $(this).data("data");
        //console.log(data);
        //$("#modal_lista_formatos").modal("hide");
        if(data.formato == "PIN"){
            limpiarFormularioPedidoInterno();
            $("#modal_nuevo_pedido_interno").modal("show");
            cargarFormatoPedidoInterno(data.numero,"ver");            
        }

        if(data.formato == "ACT"){
            limpiarFormularioActa();
            $("#modal_nueva_acta").modal("show");
            cargarFormatoActa(data.numero,"ver");  
        }

    }

    function quitarFormato(){

        var data = $(this).data("data");
        var registro = arrayRegistroFormato.idPresupuesto;
        alertify.confirm("¿SEGURO DE QUITAR FORMATO A LA SOLICITUD N : " + registro  +" ?", function (e) {
            if (e) {
                service.procesar("removeAgregarRegistroFormato",registro,function(evt){
                    cargaRegistrosPresupuestoGasto();
                    $("#modal_lista_formatos").modal("hide");
                    alertify.success("FORMATO QUITADO CON EXITO");
                })
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    }

    function asignarFormato(){
        var data = $(this).data("data");
        //console.log(arrayRegistroFormato);
        var numero_formato = corregirNull(arrayRegistroFormato.nformato);
        var estado = corregirNull(arrayRegistroFormato.estado);
        var objeto = new Object()
            objeto.formato = $("#modal_lista_formatos .formato").val();
            objeto.numero = data.numero;
            objeto.registro = arrayRegistroFormato.idPresupuesto;

        if(numero_formato == data.numero && estado == "POR APROBAR"){
            alertify.error("SOLICITUD YA REGISTRADO EN FORMATO N : "+ data.numero);
        }else if(numero_formato == "" || estado == "RECHAZADO"){
            alertify.confirm("¿SEGURO DE REGISTRAR LA SOLICITUD EN FORMATO N : " + data.numero  +" ?", function (e) {
                if (e) {
                    service.procesar("saveAgregarRegistroFormato",objeto,function(evt){
                        cargaRegistrosPresupuestoGasto();
                        $("#modal_lista_formatos").modal("hide");
                        alertify.success("SOLICITUD REGISTRADA EN FORMATO");
                    })
                } else {
                    alertify.error("HA CANCELADO EL PROCESO");
                }
            });
        }else if(numero_formato != "" && numero_formato != data.numero){
            alertify.confirm("¿SEGURO DE CAMBIAR DE FORMATO LA SOLICITUD REGISTRADA EN : " + data.numero  +" ?", function (e) {
                if (e) {
                    objeto.nanterior = numero_formato;
                    service.procesar("saveAgregarRegistroFormato",objeto,function(evt){
                        cargaRegistrosPresupuestoGasto();
                        $("#modal_lista_formatos").modal("hide");
                        alertify.success("SOLICITUD REGISTRADA EN FORMATO");
                    })
                } else {
                    alertify.error("HA CANCELADO EL PROCESO");
                }
            });
        }
    }

    $("#modal_lista_formatos .nuevoformato").on('click',function(){
        var formato = $("#modal_lista_formatos .formato").val(); 
        if(formato == "PIN"){
            limpiarFormularioPedidoInterno();
            service.procesar("nuevoNumeroPedidoInterno",respuestaNumeroPedidoInterno);
            var registros = arrayRegistroFormato;
            var monto = 0.00;
            //$("#modal_lista_formatos").modal("hide");
            $("#modal_nuevo_pedido_interno").modal("show");

            $("#modal_nuevo_pedido_interno .ructindni").val(registros.tin);
            $("#modal_nuevo_pedido_interno .moneda").val(registros.moneda);

            if(registros.moneda == "DOLARES"){
                monto = registros.monto;
            }else{
                monto = registros.total;
            }
            $("#modal_nuevo_pedido_interno .total").val(monto);
            $("#modal_nuevo_pedido_interno .fechaemision").val(registros.fechaGasto);
            $("#modal_nuevo_pedido_interno .giraranombrede").val(registros.proveedor);

            $("#modal_nuevo_pedido_interno .tablaregistros tbody").html("");
            $("#modal_nuevo_pedido_interno .tablaregistros tbody").append("<tr><td>1</td><td>"+registros.chr_categoria_id+"</td><td>"+registros.chr_string_id+"</td><td>"+monto+"</td><td>"+registros.descripcion.toUpperCase()+"</td><td>"+registros.nombre_razon_social+"</td><td>"+registros.documento+"</td><td>"+registros.fecha_documento+"</td><td></td></tr>");

            $("#modal_nuevo_pedido_interno .totalpedido").val(monto);
        }

        if(formato == "ACT"){
            limpiarFormularioActa();
            service.procesar("nuevoNumeroActa",respuestaNumeroActa);
            var registros = arrayRegistroFormato;
            $("#modal_nueva_acta").modal("show");
            $("#modal_nueva_acta .proveedor").val(registros.proveedor);
            $("#modal_nueva_acta .ruc_proveedor").val(registros.proveedor);
            $("#modal_nueva_acta .moneda").val(registros.moneda);
            $("#modal_nueva_acta .importe").val(registros.total);
            $("#modal_nueva_acta .observaciones").val(registros.descripcion);

            //console.log(registros);
        }

        if(formato == "OEP"){
            console.log("OEP");
            
        }


    })

    function respuestaNumeroActa(evt){
        var nuevoNumero = evt;
        $("#modal_nueva_acta .numero_acta").val(nuevoNumero);
        //$("#modal_nuevo_pedido_interno .numeropedidointerno").val(nuevoNumero);
    }




    $("#modal_nuevo_pedido_interno .guardarpedidointerno").on('click',function(){

        var numeropedidointerno = $("#modal_nuevo_pedido_interno .numeropedidointerno").val();
        var ructindni = $("#modal_nuevo_pedido_interno .ructindni").val();
        var grupopagos = $("#modal_nuevo_pedido_interno .grupopagos").val();
        var personaquesolicita = $("#modal_nuevo_pedido_interno .personaquesolicita").val();
        var direccion = $("#modal_nuevo_pedido_interno .direccion").val();
        var moneda = $("#modal_nuevo_pedido_interno .moneda").val();
        var total = $("#modal_nuevo_pedido_interno .total").val();
        var fechaemision = $("#modal_nuevo_pedido_interno .fechaemision").val();
        var tipodocumento = $("#modal_nuevo_pedido_interno .tipodocumento").val();
        var realizadopor = $("#modal_nuevo_pedido_interno .realizadopor").val();
        var ubicacion = $("#modal_nuevo_pedido_interno .ubicacion").val();
        var proveedor = arrayRegistroFormato.proveedor;
        var observacion = $("#modal_nuevo_pedido_interno .observacion").val();
        var condicion = $("#modal_nuevo_pedido_interno .condicion").val();
        var firmasolicitante = $("#modal_nuevo_pedido_interno .firmasolicitante").val();
        var firmavistobueno = $("#modal_nuevo_pedido_interno .firmavistobueno").val();
        var registro_pago = arrayRegistroFormato.idPresupuesto;

        var proceso = $("#modal_nuevo_pedido_interno .guardarpedidointerno").html();

        if(proceso == "GUARDAR"){
            alertify.confirm("¿SEGURO DE GUARDAR PEDIDO INTERNO N : " + numeropedidointerno  +" ?", function (e) {
                if (e) {
                    var objeto = new Object()
                        objeto.numeropedidointerno = numeropedidointerno;
                        objeto.ructindni = ructindni;
                        objeto.grupopagos = grupopagos;
                        objeto.personaquesolicita = personaquesolicita;
                        objeto.direccion = direccion;
                        objeto.moneda = moneda;
                        objeto.total = total;
                        objeto.fechaemision = fechaemision;
                        objeto.tipodocumento = tipodocumento;
                        objeto.realizadopor = realizadopor;
                        objeto.ubicacion = ubicacion;
                        objeto.proveedor = proveedor;
                        objeto.observacion = observacion;
                        objeto.condicion = condicion;
                        objeto.firmasolicitante = firmasolicitante;
                        objeto.firmavistobueno = firmavistobueno;
                        objeto.registro_pago = registro_pago;
                        objeto.proceso = "NUEVO";
                        service.procesar("savePedidoInterno",objeto,function(evt){
                            if(evt == 1){
                                alertify.success("PEDIDO INTERNO GENERADO SATISFACTORIAMENTE");
                                $("#modal_nuevo_pedido_interno").modal("hide");
                                $("#modal_lista_formatos").modal("hide");
                                cargaRegistrosPresupuestoGasto();
                                cargaListaFormatos();
                                cargaListaFormatosFormulario();                                
                            }else{
                                alertify.error("PEDIDO INTERNO NO GUARDADO");
                            }

                        })
                } else {
                    alertify.error("HA CANCELADO EL PROCESO");
                }
            });
        }else{
            alertify.confirm("¿SEGURO DE MODIFICAR PEDIDO INTERNO N : " + numeropedidointerno  +" ?", function (e) {
                if (e) {
                    var objeto = new Object()
                        objeto.numeropedidointerno = numeropedidointerno;
                        objeto.grupopagos = grupopagos;
                        objeto.personaquesolicita = personaquesolicita;
                        objeto.direccion = direccion;
                        objeto.moneda = moneda;
                        objeto.total = total;
                        objeto.fechaemision = fechaemision;
                        objeto.tipodocumento = tipodocumento;
                        objeto.realizadopor = realizadopor;
                        objeto.ubicacion = ubicacion;
                        objeto.observacion = observacion;
                        objeto.condicion = condicion;
                        objeto.firmasolicitante = firmasolicitante;
                        objeto.firmavistobueno = firmavistobueno;
                        objeto.registro_pago = registro_pago;
                        objeto.proceso = "MODIFICAR";
                        service.procesar("savePedidoInterno",objeto,function(evt){
                            if(evt == 2){
                                alertify.success("PEDIDO INTERNO MODIFICADO SATISFACTORIAMENTE");
                                $("#modal_nuevo_pedido_interno").modal("hide");
                                cargaRegistrosPresupuestoGasto();
                                cargaListaFormatos();
                                cargaListaFormatosFormulario();                             
                            }else{
                                alertify.error("PEDIDO INTERNO NO MODIFICADO");
                            }
                        })
                } else {
                    alertify.error("HA CANCELADO EL PROCESO");
                }
            });
        }


    })

    $("#modal_nueva_acta .guardaracta").on('click',function(){

        var numero_acta = $("#modal_nueva_acta .numero_acta").val();
        var ruc_proveedor = $("#modal_nueva_acta .ruc_proveedor").val();
        var orden_de_servicio = $("#modal_nueva_acta .orden_de_servicio").val();
        var importe = $("#modal_nueva_acta .importe").val();
        var fecha_de_inicio = $("#modal_nueva_acta .fecha_de_inicio").val();
        var fecha_de_entrega = $("#modal_nueva_acta .fecha_de_entrega").val();
        var solicitante = $("#modal_nueva_acta .solicitante").val();
        var observaciones = $("#modal_nueva_acta .observaciones").val();
        var recibido = $("#modal_nueva_acta .recibido").val();
        var fecha_de_recepcion = $("#modal_nueva_acta .fecha_de_recepcion").val();
        var visto_bueno = $("#modal_nueva_acta .visto_bueno").val();
        var direccion = $("#modal_nueva_acta .direccion").val();

        var proceso = $("#modal_nueva_acta .guardaracta").html();

        if(proceso == "GUARDAR"){
            alertify.confirm("¿SEGURO DE GUARDAR ACTA N : " + numero_acta  +" ?", function (e) {
                if (e) {
                    var objeto = new Object()
                        objeto.numero_acta = numero_acta;
                        objeto.ruc_proveedor = ruc_proveedor;
                        objeto.orden_de_servicio = orden_de_servicio;
                        objeto.moneda = arrayRegistroFormato.moneda;
                        objeto.importe = importe;
                        objeto.fecha_de_inicio = fecha_de_inicio;
                        objeto.fecha_de_entrega = fecha_de_entrega;
                        objeto.solicitante = solicitante;
                        objeto.observaciones = observaciones;
                        objeto.recibido = recibido;
                        objeto.fecha_de_recepcion = fecha_de_recepcion;
                        objeto.visto_bueno = visto_bueno;
                        objeto.direccion = direccion;
                        objeto.registro_pago = arrayRegistroFormato.idPresupuesto;
                        objeto.proceso = "NUEVO";
                        service.procesar("saveActa",objeto,function(evt){
                            if(evt == 1){
                                alertify.success("ACTA GENERADA SATISFACTORIAMENTE");
                                $("#modal_nueva_acta").modal("hide");
                                $("#modal_lista_formatos").modal("hide");
                                cargaRegistrosPresupuestoGasto();
                                cargaListaFormatos();
                                cargaListaFormatosFormulario();                                
                            }else{
                                alertify.error("ACTA NO GUARDADA");
                            }

                        })
                } else {
                    alertify.error("HA CANCELADO EL PROCESO");
                }
            });
        }else{
            alertify.confirm("¿SEGURO DE MODIFICAR ACTA N : " + numero_acta  +" ?", function (e) {
                if (e) {
                    var objeto = new Object()
                        objeto.numero_acta = numero_acta;
                        objeto.ruc_proveedor = ruc_proveedor;
                        objeto.orden_de_servicio = orden_de_servicio;
                        objeto.moneda = arrayRegistroFormato.moneda;
                        objeto.importe = importe;
                        objeto.fecha_de_inicio = fecha_de_inicio;
                        objeto.fecha_de_entrega = fecha_de_entrega;
                        objeto.solicitante = solicitante;
                        objeto.observaciones = observaciones;
                        objeto.recibido = recibido;
                        objeto.fecha_de_recepcion = fecha_de_recepcion;
                        objeto.visto_bueno = visto_bueno;
                        objeto.direccion = direccion;
                        objeto.registro_pago = arrayRegistroFormato.idPresupuesto;
                        objeto.proceso = "MODIFICAR";
                        service.procesar("saveActa",objeto,function(evt){
                            if(evt == 2){
                                alertify.success("ACTA MODIFICADA SATISFACTORIAMENTE");
                                $("#modal_nueva_acta").modal("hide");
                                cargaRegistrosPresupuestoGasto();
                                cargaListaFormatos();
                                cargaListaFormatosFormulario();                             
                            }else{
                                alertify.error("ACTA NO MODIFICADA");
                            }
                        })
                } else {
                    alertify.error("HA CANCELADO EL PROCESO");
                }
            });
        }


    })

    function visualizarRegistroSolicitudGasto(){
        $("#areaFormularioSolicitudGastoPresupuesto").change();

        var data = $(this).data("data");

        alertify.confirm("¿ SEGURO DE VISUALIZAR EL REGISTRO : " + data.idPresupuesto +" ?", function (e) {
                if (e) {

                    limpiarFormularioSolicitud();
                    //activaTab('formularioSolicitudGastoPresupuesto');
                    $("#formularioSolicitudGastoPresupuesto").modal("show");

                    if(data.estado != "INGRESADO"){
                        $(".motivo").show();
                    }

                    $("#fechaFormularioSolicitudGastoPresupuesto").val(data.fechaGasto);
                    //$("#procedimientoSolicitudGastoPresupuesto").html("MODIFICAR");
                    $("#mensajeFormularioGastoPresupuesto").html("FORMULARIO VISUALIZACION DE REGISTRO");
                    $("#idPresupuestoSolicitudGastoPresupuesto").html(data.idPresupuesto);
                    //console.log(data.idPresupuesto);


                    $("#estadoFormularioSolicitudGastoPresupuesto").val(data.estado);

                    $("#areaFormularioSolicitudGastoPresupuesto").val(data.chr_area_id);
                    cargaStringSolicitudVME();
                    $("#stringFormularioSolicitudGastoPresupuesto").append("<option value='"+data.chr_string_id+"' selected>"+ data.chr_string.toUpperCase() +"</option>");
                    cargaCategoriasSolicitudVME();
                    $("#categoriaFormularioSolicitudGastoPresupuesto").append("<option value='"+data.chr_categoria_id+"' selected>"+ data.chr_categoria_id + " - " + data.chr_categoria.toUpperCase() +"</option>");
                    cargaProyectosSolicitudVME();
                    $("#proyectoFormularioSolicitudGastoPresupuesto").append("<option value='"+data.chr_proyecto_id+"' selected>"+ data.chr_proyecto.toUpperCase() +"</option>");
                    cargaDetallesSolicitudVME();
                    $("#detalleFormularioSolicitudGastoPresupuesto").append("<option value='"+data.chr_comentario_id+"' selected>"+ data.chr_comentario.toUpperCase() +"</option>");
                    cargarSaldoFiltroSolicitudVME();
                    $("#ndocumentoFormularioSolicitudGastoPresupuesto").val(data.documento);

                    $("#proveedorFormularioSolicitudGastoPresupuesto").val(data.proveedor);

                    $("#formularioSolicitudGastoPresupuesto .formato").html("<option value=''>---SELECCIONAR---</option><option value='PIN'>PEDIDO INTERNO</option><option value='OCA'>ORDEN DE COMPRA ABIERTA</option><option value='PRO'>PROVISION</option>");

                    if(data.idFormato != "ACT"){
                        $("#formularioSolicitudGastoPresupuesto .formato").val(data.idFormato);
                    }else{
                        $("#formularioSolicitudGastoPresupuesto .formato").html("<option value='ACT'>ACT</option>");
                    }

                    $("#formularioSolicitudGastoPresupuesto .nformato").html("<option value='"+data.nformato+"'>"+data.nformato+"</option>");

                    var oep = data.chr_categoria_id;
                    if (oep == "OEP"){
                        var objeto = new Object()
                            objeto.idOep = data.proveedor;
                            objeto.anio = nanio;
                        service.procesar("getListaDetalleSolicitudPagoOep",objeto,cargaListaDetalleSolicitudPagoOep);
                    }

                    $("#proveedorFormularioSolicitudGastoPresupuesto").change();

                    //$("#actividadFormularioSolicitudGastoPresupuesto").val(data.actividad);
                    $("#descripcionFormularioSolicitudGastoPresupuesto").val(data.descripcion);
                    $("#montoFormularioSolicitudGastoPresupuesto").val( data.monto );

                    cambioMoneda(data.moneda);

                    $("#seleccionarMonedaFormularioSolicitudGastoPresupuesto" ).val(data.moneda);
                    $("#tipoCambioFormularioSolicitudGastoPresupuesto").val(data.tipocambio);
                    $("#totalFormularioSolicitudGastoPresupuesto").val(data.total);   

                    $("#ndocumentoFormularioSolicitudGastoPresupuesto").prop('disabled', true);

                    //cambioMoneda(data.moneda)

                    //botonModificarRegistro();

                    //$("#montoGastoPresupuesto").focus();

                    //$("#formularioSolicitudGastoPresupuesto .input-sm").prop('disabled', false);

                    alertify.success("HA ACEPTADO VISUALIZAR EL REGISTRO");
                } else {
                    alertify.error("HA CANCELADO EL PROCEDIMIENTO DE VISUALIZACION");
                }
            });
    }


    function modificarRegistroSolicitudGasto(){
        //$("#areaFormularioSolicitudGastoPresupuesto").change();

        var data = $(this).data("data");

        ndocumentos = data.documento;

        alertify.confirm("¿ SEGURO DE MODIFICAR EL REGISTRO : " + data.idPresupuesto +" ?", function (e) {
                if (e) {

                    limpiarFormularioSolicitud();
                    //activaTab('formularioSolicitudGastoPresupuesto');
                    $("#formularioSolicitudGastoPresupuesto").modal("show");

                    if(data.estado != "INGRESADO"){
                        $(".motivo").show();
                    }

                    $("#fechaFormularioSolicitudGastoPresupuesto").val(data.fechaGasto);
                    $("#procedimientoSolicitudGastoPresupuesto").html("MODIFICAR");
                    $("#mensajeFormularioGastoPresupuesto").html("FORMULARIO MODIFICACION DE REGISTRO");
                    $("#idPresupuestoSolicitudGastoPresupuesto").html(data.idPresupuesto);
                    //console.log(data.idPresupuesto);
                    $("#carreraFormularioSolicitudGastoPresupuesto").val(data.chr_carrera_id);
                    //$("#chkNuevoPresupuestoSolicitudPresupuesto").val(0);
                    if(corregirNullNumero(data.ampliacion) == 1){
                        $("#chkNuevoPresupuestoSolicitudPresupuesto").prop("checked", true); 
                    }else{
                        $("#chkNuevoPresupuestoSolicitudPresupuesto").prop("checked", false); 
                    }
                    

                    $("#estadoFormularioSolicitudGastoPresupuesto").val(data.estado);

                    $("#areaFormularioSolicitudGastoPresupuesto").val(data.chr_area_id);
                    cargaStringSolicitudVME();
                    $("#stringFormularioSolicitudGastoPresupuesto").append("<option value='"+data.chr_string_id+"' selected>"+ data.chr_string.toUpperCase() +"</option>");
                    cargaCategoriasSolicitudVME();
                    $("#categoriaFormularioSolicitudGastoPresupuesto").append("<option value='"+data.chr_categoria_id+"' selected>"+data.chr_categoria_id+" - "+ data.chr_categoria.toUpperCase() +"</option>");
                    cargaProyectosSolicitudVME();
                    $("#proyectoFormularioSolicitudGastoPresupuesto").append("<option value='"+data.chr_proyecto_id+"' selected>"+ data.chr_proyecto.toUpperCase() +"</option>");
                    cargaDetallesSolicitudVME();
                    $("#detalleFormularioSolicitudGastoPresupuesto").append("<option value='"+data.chr_comentario_id+"' selected>"+ data.chr_comentario.toUpperCase() +"</option>");
                    cargarSaldoFiltroSolicitudVME();
                    $("#ndocumentoFormularioSolicitudGastoPresupuesto").val(data.documento);

                    $("#proveedorFormularioSolicitudGastoPresupuesto").val(data.proveedor);

                    $("#formularioSolicitudGastoPresupuesto .formato").html("<option value=''>---SELECCIONAR---</option><option value='PIN'>PEDIDO INTERNO</option><option value='OCA'>ORDEN DE COMPRA ABIERTA</option><option value='PRO'>PROVISION</option>");

                    if(data.idFormato != "ACT"){
                        $("#formularioSolicitudGastoPresupuesto .formato").val(data.idFormato);
                    }else{
                        $("#formularioSolicitudGastoPresupuesto .formato").html("<option value='ACT'>ACT</option>");
                    }

                    //$("#formularioSolicitudGastoPresupuesto .formato").change();

                    $("#formularioSolicitudGastoPresupuesto .nformato").html("<option value='"+data.nformato+"'>"+data.nformato+"</option>");






                    //var oep = data.proveedor.indexOf("OEP")
                    //console.log(data.chr_categoria_id);
                    var oep = data.chr_categoria_id
                    if (oep == "OEP"){
                        var objeto = new Object()
                            objeto.idOep = data.proveedor;
                            objeto.anio = nanio;
                        service.procesar("getListaDetalleSolicitudPagoOep",objeto,cargaListaDetalleSolicitudPagoOep);
                    }

                    $("#proveedorFormularioSolicitudGastoPresupuesto").change();

                    $("#actividadFormularioSolicitudGastoPresupuesto").val(data.actividad);
                    $("#descripcionFormularioSolicitudGastoPresupuesto").val(data.descripcion);
                    $("#montoFormularioSolicitudGastoPresupuesto").val(data.monto);

                    cambioMoneda(data.moneda);

                    $("#seleccionarMonedaFormularioSolicitudGastoPresupuesto" ).val(data.moneda);
                    $("#tipoCambioFormularioSolicitudGastoPresupuesto").val(data.tipocambio);
                    $("#totalFormularioSolicitudGastoPresupuesto").val(data.total);

                    $("#idFormatoFormularioSolicitudGastoPresupuesto").val(data.idFormato);
                    $("#nformatoFormularioSolicitudGastoPresupuesto").val(data.nformato);

                    //cambioMoneda(data.moneda)

                    //botonModificarRegistro();

                    $("#montoGastoPresupuesto").focus();

                    $("#formularioSolicitudGastoPresupuesto .input-sm").prop('disabled', false);

                    $("#estadoFormularioSolicitudGastoPresupuesto").prop('disabled', false); //DESABILITAR EL BOTON ESTADO PARA QUE SIEMPRE CARGUE EL INGRESADO

                    alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
                } else {
                    alertify.error("HA CANCELADO EL PROCEDIMIENTO DE MODIFICACION");
                }
            });
    }

    function cargaListaDetalleSolicitudPagoOep(evt){
        cargaRegistrosFormularioOep(evt);

    }




    function eliminarRegistroSolicitudGasto(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR EL REGISTRO : " + data.idPresupuesto +" ?", function (e) {
            if (e) {
                service.procesar("deleteRegistroSolicitudGastoPresupuesto",data.idPresupuesto,mensajeEliminacionSolicitudGastoPresupuesto);
                alertify.success("HA ACEPTADO ELIMINAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO");
            }
        });

    }

    function mensajeEliminacionSolicitudGastoPresupuesto(evt){
        if(evt == undefined){
            alertify.error("REGISTRO NO ELIMINADO");
        }else{
            //listarRegistrosGastoPresupuesto();
            cargaRegistrosPresupuestoGasto();
            alertify.success("REGISTRO ELIMINADO");
        }

    }


    function cambioMoneda(dato){
        if (dato == "DOLARES"){
            $(".tipocambio").show();
        }else{
            $(".tipocambio").hide();
        }
    }





    //DESARROLLO DE MODULOS NUEVOS FORMATOS

    $("#area_pedido_interno .boton_nuevo_pedido_interno").on('click', function(){
        limpiarFormularioPedidoInterno();
        service.procesar("nuevoNumeroPedidoInterno",respuestaNumeroPedidoInterno);
        $("#modal_nuevo_pedido_interno").modal("show");
    })

    function respuestaNumeroPedidoInterno(evt){
        var nuevoNumero = evt;
        $("#modal_nuevo_pedido_interno .numeropedidointerno").val(nuevoNumero);
        //$("#modal_nuevo_pedido_interno .numeropedidointerno").val(nuevoNumero);
    }


    function limpiarFormularioPedidoInterno(){
        $("#modal_nuevo_pedido_interno .numeropedidointerno").val("");
        $("#modal_nuevo_pedido_interno .ructindni").val("");
        $("#modal_nuevo_pedido_interno .grupopagos").val("PROVEEDORES VARIOS");
        $("#modal_nuevo_pedido_interno .personaquesolicita").val("MANUEL CORTES FONTCUBERTA ABUCCI");
        $("#modal_nuevo_pedido_interno .direccion").val("DIRECCIÓN DE ASEGURAMIENTO DE LA CALIDAD");
        $("#modal_nuevo_pedido_interno .moneda").val("DOLARES");
        $("#modal_nuevo_pedido_interno .total").val("");
        $("#modal_nuevo_pedido_interno .fechaemision").val(fecha);
        $("#modal_nuevo_pedido_interno .tipodocumento").val("PAGOS VARIOS");
        $("#modal_nuevo_pedido_interno .realizadopor").val("MIRYAM SALAZAR ZEGARRA");
        $("#modal_nuevo_pedido_interno .ubicacion").val("PRINCIPAL");
        $("#modal_nuevo_pedido_interno .giraranombrede").val(0);
        $("#modal_nuevo_pedido_interno .observacion").val("");

        $("#modal_nuevo_pedido_interno .tablaregistros tbody").html("");

        $("#modal_nuevo_pedido_interno .condicion").val("INMEDIATO");
        $("#modal_nuevo_pedido_interno .firmasolicitante").val("mcortes");
        $("#modal_nuevo_pedido_interno .firmavistobueno").val("MANUEL CORTES FONTCUBERTA ABUCCI");

        $("#modal_nuevo_pedido_interno .guardarpedidointerno").html("GUARDAR");

        $("#modal_nuevo_pedido_interno .areabotones").css("display","");
        $("#modal_nuevo_pedido_interno .areaimpresion").css("display","none");

        $("#modal_nuevo_pedido_interno input").prop("disabled",false);
        $("#modal_nuevo_pedido_interno select").prop("disabled",false);
        $("#modal_nuevo_pedido_interno table").prop("disabled",false);

        $("#modal_nuevo_pedido_interno .numeropedidointerno").prop("disabled",true);
        $("#modal_nuevo_pedido_interno .giraranombrede").prop("disabled",true);
        $("#modal_nuevo_pedido_interno .ructindni").prop("disabled",true);
        $("#modal_nuevo_pedido_interno .moneda").prop("disabled",true);
        $("#modal_nuevo_pedido_interno .total").prop("disabled",true);

    }

    function limpiarFormularioActa(){
        $("#modal_nueva_acta .numero_acta").val("");
        $("#modal_nueva_acta .proveedor").val(0);
        $("#modal_nueva_acta .ruc_proveedor").val("");
        $("#modal_nueva_acta .orden_de_servicio").val("");
        $("#modal_nueva_acta .moneda").val(0);
        $("#modal_nueva_acta .importe").val(0);
        $("#modal_nueva_acta .fecha_de_inicio").val(fecha);
        $("#modal_nueva_acta .fecha_de_entrega").val(fecha);
        $("#modal_nueva_acta .solicitante").val("MIRYAM SALAZAR ZEGARRA");
        $("#modal_nueva_acta .observaciones").val("");
        $("#modal_nueva_acta .recibido").val("MIRYAM SALAZAR ZEGARRA");
        $("#modal_nueva_acta .fecha_de_recepcion").val(fecha);
        $("#modal_nueva_acta .visto_bueno").val("MANUEL CORTES FONTCUBERTA ABUCCI");
        $("#modal_nueva_acta .direccion").val("DIRECTOR DE ASEGURAMIENTO DE LA CALIDAD");

        $("#modal_nueva_acta .guardaracta").html("GUARDAR");

        $("#modal_nueva_acta .areabotones").css("display","");
        $("#modal_nueva_acta .areaimpresion").css("display","none");

        $("#modal_nueva_acta input").prop("disabled",false);
        $("#modal_nueva_acta select").prop("disabled",false);
        $("#modal_nueva_acta table").prop("disabled",false);
        $("#modal_nueva_acta textarea").prop("disabled",false);

        $("#modal_nueva_acta .numero_acta").prop("disabled",true);
        $("#modal_nueva_acta .proveedor").prop("disabled",true);
        $("#modal_nueva_acta .ruc_proveedor").prop("disabled",true);
        $("#modal_nueva_acta .importe").prop("disabled",true);

    }

    function cargaListaFormatos(){

        service.procesar("getTablaFormatos",'PIN',nanio,function(evt){
            resultado = evt;
            //console.log(resultado);
            $("#area_pedido_interno .listaFormatosPedidoInterno tbody").html("");

            if ( resultado == undefined ) return;

            for(var i=0; i<resultado.length ; i++){
                datoRegistro = resultado[i];

                var fila = $("<tr>");
                var celdaBotones = $("<td>");

                fila.html('<td>'+ (i + 1) +'</td><td>'+ resultado[i].numero +'</td><td>'+ resultado[i].emision +'</td><td>'+ resultado[i].nombre_razon_social +'</td><td>'+ resultado[i].moneda +'</td><td>'+ resultado[i].total +'</td><td>'+ resultado[i].estado +'</td>');

                var contenedorBotones = $("<td><div class='input-group-btn'>");

                var btnVisualizar = $("<button class='btn btn-primary btn-xs' title='Visualizar Formato'>");
                btnVisualizar.html('<span class="glyphicon glyphicon-eye-open"></span');

                var btnModificar = $("<button class='btn btn-success btn-xs' title='Modificar Formato'>");
                btnModificar.html('<span class="glyphicon glyphicon-edit"></span');

                var btnAsignar = $("<button class='btn btn-info btn-xs' title='Asignar Formato'>");
                btnAsignar.html('<span class="glyphicon glyphicon-list-alt"></span');

                var btnEliminar = $("<button class='btn btn-danger btn-xs' title='Eliminar Formato'>");
                btnEliminar.html('<span class="glyphicon glyphicon-remove"></span');

                var btnExportar = $("<button class='btn btn-info btn-xs' title='Exportar Formato'>");
                btnExportar.html('<span class="glyphicon glyphicon-save-file"></span');

                btnVisualizar.data("data",datoRegistro);
                btnModificar.data("data",datoRegistro);
                btnAsignar.data("data",datoRegistro);
                btnEliminar.data("data",datoRegistro);
                btnExportar.data("data",datoRegistro);

                btnVisualizar.on("click",visualizarFormatoPedidoInterno);
                btnModificar.on("click",modificarFormatoPedidoInterno);
                btnAsignar.on("click",asignarFormatoPedidoInterno);
                btnEliminar.on("click",eliminarFormatoPedidoInterno);
                btnExportar.on("click",exportarFormatoPedidoInterno);

                var btnLiquidar = $("<button class='btn btn-warning btn-xs' title='Cerrar Formato'>");
                btnLiquidar.html('<span class="glyphicon glyphicon-off"></span');
                btnLiquidar.data("data",datoRegistro);
                btnLiquidar.on("click",liquidarFormatoPedidoInterno);

                if(resultado[i].estado == "PENDIENTE"){
                    contenedorBotones.append(btnVisualizar);
                    contenedorBotones.append(" ");
                    contenedorBotones.append(btnModificar);
                    contenedorBotones.append(" ");
                    contenedorBotones.append(btnEliminar);
                    contenedorBotones.append(" ");
                    contenedorBotones.append(btnLiquidar);
                    contenedorBotones.append(" ");
                    contenedorBotones.append(btnExportar);
                }else{
                    contenedorBotones.append(btnVisualizar);
                    contenedorBotones.append(" ");
                    contenedorBotones.append(btnExportar);                    
                }

                fila.append(contenedorBotones);

                $("#area_pedido_interno .listaFormatosPedidoInterno tbody").append(fila);

            }
        });

        service.procesar("getTablaFormatos",'ACT',nanio,function(evt){
            resultado = evt;
            //console.log(resultado);
            $("#area_acta .listaFormatosActa tbody").html("");

            if ( resultado == undefined ) return;

            for(var i=0; i<resultado.length ; i++){
                datoRegistro = resultado[i];

                var fila = $("<tr>");
                var celdaBotones = $("<td>");

                fila.html('<td>'+ (i + 1) +'</td><td>'+ resultado[i].numero +'</td><td>'+ resultado[i].emision +'</td><td>'+ resultado[i].nombre_razon_social +'</td><td>'+ resultado[i].moneda +'</td><td>'+ resultado[i].total +'</td><td>'+ resultado[i].estado +'</td>');

                var contenedorBotones = $("<td><div class='input-group-btn'>");

                var btnVisualizar = $("<button class='btn btn-primary btn-xs' title='Visualizar Formato'>");
                btnVisualizar.html('<span class="glyphicon glyphicon-eye-open"></span');

                var btnModificar = $("<button class='btn btn-success btn-xs' title='Modificar Formato'>");
                btnModificar.html('<span class="glyphicon glyphicon-edit"></span');

                var btnAsignar = $("<button class='btn btn-info btn-xs' title='Asignar Formato'>");
                btnAsignar.html('<span class="glyphicon glyphicon-list-alt"></span');

                var btnEliminar = $("<button class='btn btn-danger btn-xs' title='Eliminar Formato'>");
                btnEliminar.html('<span class="glyphicon glyphicon-remove"></span');

                var btnExportar = $("<button class='btn btn-info btn-xs' title='Exportar Formato'>");
                btnExportar.html('<span class="glyphicon glyphicon-save-file"></span');

                btnVisualizar.data("data",datoRegistro);
                btnModificar.data("data",datoRegistro);
                btnAsignar.data("data",datoRegistro);
                btnEliminar.data("data",datoRegistro);
                btnExportar.data("data",datoRegistro);

                btnVisualizar.on("click",visualizarFormatoActa);
                btnModificar.on("click",modificarFormatoActa);
                btnAsignar.on("click",asignarFormatoActa);
                btnEliminar.on("click",eliminarFormatoActa);
                btnExportar.on("click",exportarFormatoActa);

                var btnLiquidar = $("<button class='btn btn-warning btn-xs' title='Cerrar Formato'>");
                btnLiquidar.html('<span class="glyphicon glyphicon-off"></span');
                btnLiquidar.data("data",datoRegistro);
                btnLiquidar.on("click",liquidarFormatoActa);

                if(resultado[i].estado == "PENDIENTE"){
                    contenedorBotones.append(btnVisualizar);
                    contenedorBotones.append(" ");
                    contenedorBotones.append(btnModificar);
                    contenedorBotones.append(" ");
                    contenedorBotones.append(btnEliminar);
                    contenedorBotones.append(" ");
                    contenedorBotones.append(btnLiquidar);
                    contenedorBotones.append(" ");
                    contenedorBotones.append(btnExportar);
                }else{
                    contenedorBotones.append(btnVisualizar);
                    contenedorBotones.append(" ");
                    contenedorBotones.append(btnExportar);                    
                }

                fila.append(contenedorBotones);

                $("#area_acta .listaFormatosActa tbody").append(fila);

            }
        });


    }

    //OPCIONES DE HOJA FORMATOS
    function visualizarFormatoActa(){
        var data = $(this).data("data");
        limpiarFormularioActa();
        $("#modal_nueva_acta").modal("show");
        cargarFormatoActa(data.numero,"ver");
    }
    function modificarFormatoActa(){
        var data = $(this).data("data");
        limpiarFormularioActa();
        $("#modal_nueva_acta").modal("show");
        cargarFormatoActa(data.numero,"editar");
    }
    function asignarFormatoActa(){
        limpiarFormularioActa();
    }
    function eliminarFormatoActa(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR EL ACTA N : " + data.numero +" ?", function (e) {
            if (e) {
                alertify.success("HA ACEPTADO ELIMINAR EL ACTA");
                service.procesar("deleteActaCompleto",data.numero,function(evt){
                    cargaListaFormatos();
                    alertify.success("ACTA ELIMINADO SATISFACTORIAMENTE");
                });

            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO");
            }
        });
    }
    function exportarFormatoActa(){
        var data = $(this).data("data");
        var nformato = data.numero;
        service.procesar("getExcelActa",nformato,function(evt){
            document.location = "webService/reportes/acta_"+nformato+".xlsx"
        });
    }

    function cargarFormatoActa(nacta,tipo){
        service.procesar("getFormularioActa",nacta,function(evt){
            //console.log(evt[0]);
            formato = evt[0];
            $("#modal_nueva_acta .numero_acta").val(formato.numero);
            $("#modal_nueva_acta .proveedor").val(formato.ruc);
            $("#modal_nueva_acta .ruc_proveedor").val(formato.ruc);
            $("#modal_nueva_acta .orden_de_servicio").val(formato.orden_servicio);
            $("#modal_nueva_acta .moneda").val(formato.moneda);
            $("#modal_nueva_acta .importe").val(formato.total);
            $("#modal_nueva_acta .fecha_de_inicio").val(formato.fecha_inicio);
            $("#modal_nueva_acta .fecha_de_entrega").val(formato.fecha_entrega);
            $("#modal_nueva_acta .solicitante").val(formato.solicitante);
            $("#modal_nueva_acta .observaciones").val(formato.observacion);
            $("#modal_nueva_acta .recibido").val(formato.recibido);
            $("#modal_nueva_acta .fecha_de_recepcion").val(formato.fecha_recepcion);
            $("#modal_nueva_acta .visto_bueno").val(formato.persona_visto_bueno);
            $("#modal_nueva_acta .direccion").val(formato.direccion);

            if(tipo == "editar"){
                $("#modal_nueva_acta input").prop("disabled",false);
                $("#modal_nueva_acta select").prop("disabled",false);
                $("#modal_nueva_acta textarea").prop("disabled",false);
                $("#modal_nueva_acta .areabotones").css("display","");
                $("#modal_nueva_acta .guardaracta").html("MODIFICAR");
            }

            if(tipo == "ver"){
                $("#modal_nueva_acta input").prop("disabled",true);
                $("#modal_nueva_acta select").prop("disabled",true);
                $("#modal_nueva_acta textarea").prop("disabled",true);
                $("#modal_nueva_acta .areabotones").css("display","none");
            }

            $("#modal_nueva_acta .areaimpresion").css("display","");
            $("#modal_nueva_acta .numero_acta").prop("disabled",true);
            $("#modal_nueva_acta .proveedor").prop("disabled",true);
            $("#modal_nueva_acta .ruc_proveedor").prop("disabled",true);
            $("#modal_nueva_acta .moneda").prop("disabled",true);
            $("#modal_nueva_acta .importe").prop("disabled",true);

        })
    }

    $("#modal_nueva_acta .imprimiracta").on('click',function(){
        var nformato = $("#modal_nueva_acta .numero_acta").val();
        service.procesar("getExcelActa",nformato,function(evt){
            document.location = "webService/reportes/acta_"+nformato+".xlsx"
        });
    })

    function liquidarFormatoActa(){
        var resultado = $(this).data("data");
        alertify.confirm("¿ SEGURO DE CERRAR FORMATO ACTA : " + resultado.numero, function (e) {
            if (e) {
                alertify.success("HA ACEPTADO CERRAR EL FORMATO");
                service.procesar("liquidarFormatoActa",resultado.numero,function(evt){
                    cargaListaFormatos();
                    if(evt == 2){
                        alertify.error("REGISTROS PENDIENTES DE APROBACION");
                    }else{
                        alertify.success("FORMATO CERRADO SATISFACTORIAMENTE");
                    }
                    
                });
                
            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO");
            }
        });   
    }


    //OPCIONES DE HOJA FORMATOS

    function liquidarFormatoPedidoInterno(){
        var resultado = $(this).data("data");

        alertify.confirm("¿ SEGURO DE CERRAR FORMATO PEDIDO INTERNO : " + resultado.numero, function (e) {
            if (e) {
                alertify.success("HA ACEPTADO CERRAR EL FORMATO");
                service.procesar("liquidarFormatoPedidoInterno",resultado.numero,function(evt){
                    cargaListaFormatos();
                    if(evt == 2){
                        alertify.error("REGISTROS PENDIENTES DE APROBACION");
                    }else{
                        alertify.success("FORMATO CERRADO SATISFACTORIAMENTE");
                    }
                    
                });
                
            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO");
            }
        });  
    }
    function visualizarFormatoPedidoInterno(){
        var data = $(this).data("data");
        limpiarFormularioPedidoInterno();
        $("#modal_nuevo_pedido_interno").modal("show");
        cargarFormatoPedidoInterno(data.numero,"ver");
    }
    function modificarFormatoPedidoInterno(){
        var data = $(this).data("data");
        limpiarFormularioPedidoInterno();
        $("#modal_nuevo_pedido_interno").modal("show");
        cargarFormatoPedidoInterno(data.numero,"editar");
    }
    function asignarFormatoPedidoInterno(){
        limpiarFormularioPedidoInterno();
    }
    function eliminarFormatoPedidoInterno(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR EL PEDIDO INTERNO N : " + data.numero +" ?", function (e) {
            if (e) {
                alertify.success("HA ACEPTADO ELIMINAR EL PEDIDO INTERNO");
                service.procesar("deletePedidoInternoCompleto",data.numero,function(evt){
                    cargaListaFormatos();
                    alertify.success("PEDIDO INTERNO ELIMINADO SATISFACTORIAMENTE");
                });

            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO");
            }
        });
    }
    function exportarFormatoPedidoInterno(){
        var data = $(this).data("data");
        var nformato = data.numero;
        service.procesar("getExcelPedidoInterno",nformato,function(evt){
            document.location = "webService/reportes/pedidoInterno_"+nformato+".xlsx"
        });
    }

    function cargarFormatoPedidoInterno(npedido,tipo){
        service.procesar("getFormularioPedidoInterno",npedido,function(evt){
            //console.log(evt);
            formato = evt.formato[0];
            registros = evt.registros;
            $("#modal_nuevo_pedido_interno .numeropedidointerno").val(formato.numero);
            $("#modal_nuevo_pedido_interno .ructindni").val(formato.ructindni);
            $("#modal_nuevo_pedido_interno .grupopagos").val(formato.grupo_pagos);
            $("#modal_nuevo_pedido_interno .personaquesolicita").val(formato.persona_solicita);
            $("#modal_nuevo_pedido_interno .direccion").val(formato.direccion);
            $("#modal_nuevo_pedido_interno .moneda").val(formato.moneda);
            $("#modal_nuevo_pedido_interno .fechaemision").val(formato.fecha_emision);
            $("#modal_nuevo_pedido_interno .tipodocumento").val(formato.tipo_documento);
            $("#modal_nuevo_pedido_interno .realizadopor").val(formato.realizado);
            $("#modal_nuevo_pedido_interno .ubicacion").val(formato.ubicacion);
            $("#modal_nuevo_pedido_interno .giraranombrede").val(formato.rucempresa);
            $("#modal_nuevo_pedido_interno .observacion").val(formato.observacion);
            $("#modal_nuevo_pedido_interno .condicion").val(formato.condicion);
            $("#modal_nuevo_pedido_interno .firmasolicitante").val(formato.persona_firma);
            $("#modal_nuevo_pedido_interno .firmavistobueno").val(formato.persona_visto_bueno);

            if(tipo == "editar"){
                $("#modal_nuevo_pedido_interno input").prop("disabled",false);
                $("#modal_nuevo_pedido_interno select").prop("disabled",false); 
                $("#modal_nuevo_pedido_interno .areabotones").css("display","");
                $("#modal_nuevo_pedido_interno .guardarpedidointerno").html("MODIFICAR");
            }

            if(tipo == "ver"){
                $("#modal_nuevo_pedido_interno input").prop("disabled",true);
                $("#modal_nuevo_pedido_interno select").prop("disabled",true);
                $("#modal_nuevo_pedido_interno .areabotones").css("display","none");
            }

            $("#modal_nuevo_pedido_interno .areaimpresion").css("display","");
            $("#modal_nuevo_pedido_interno .numeropedidointerno").prop("disabled",true);
            $("#modal_nuevo_pedido_interno .ructindni").prop("disabled",true);
            $("#modal_nuevo_pedido_interno .moneda").prop("disabled",true);
            $("#modal_nuevo_pedido_interno .total").prop("disabled",true);
            $("#modal_nuevo_pedido_interno .totalpedido").prop("disabled",true);
            $("#modal_nuevo_pedido_interno .giraranombrede").prop("disabled",true);
            //REGISTROS RELACIONADOS CON EL FORMATO
            $("#modal_nuevo_pedido_interno .tablaregistros tbody").html("");

            if ( registros == undefined ) return;
            var monto_total = 0.00;
            for(var i=0; i<registros.length ; i++){
                datoRegistro = registros[i];

                var fila = $("<tr>");
                var celdaBotones = $("<td>");

                fila.html('<td>'+ (i + 1) +'</td><td>'+ registros[i].chr_categoria_id +'</td><td>'+ registros[i].chr_string_id +'</td><td>'+ registros[i].monto +'</td><td>'+ registros[i].descripcion +'</td><td>'+ formato.nombre_razon_social +'</td><td>'+ registros[i].documento +'</td><td>'+ registros[i].fecha_documento +'</td>');

                if(tipo == "editar"){
                    var contenedorBotones = $("<td><div class='input-group-btn'>");
                    var btnEliminar = $("<button class='btn btn-danger btn-xs' title='Eliminar Registro de Formato'>");
                    btnEliminar.html('<span class="glyphicon glyphicon-remove"></span');
                    btnEliminar.data("data",datoRegistro);
                    btnEliminar.on("click",eliminarRegistroFormatoPedidoInterno);
                    contenedorBotones.append(btnEliminar);
                    fila.append(contenedorBotones);
                }else{
                    fila.append("<td></td>");
                }

                $("#modal_nuevo_pedido_interno .tablaregistros tbody").append(fila);

                monto_total = parseFloat(monto_total) + parseFloat(registros[i].monto);

            }

            $("#modal_nuevo_pedido_interno .total").val(monto_total.toFixed(2));
            $("#modal_nuevo_pedido_interno .totalpedido").val(monto_total.toFixed(2));



        })
    }

    function eliminarRegistroFormatoPedidoInterno(){
        var data = $(this).data("data");
        var registro = data.idPresupuesto;
        alertify.confirm("¿SEGURO DE QUITAR FORMATO A LA SOLICITUD N : " + registro  +" ?", function (e) {
            if (e) {
                service.procesar("removeAgregarRegistroFormato",registro,function(evt){
                    cargaRegistrosPresupuestoGasto();
                    npedido = $("#modal_nuevo_pedido_interno .numeropedidointerno").val();
                    cargarFormatoPedidoInterno(npedido,"editar");
                    alertify.success("FORMATO QUITADO CON EXITO");

                })
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    }

    $("#modal_nuevo_pedido_interno .imprimirpedidointerno").on('click',function(){
        var nformato = $("#modal_nuevo_pedido_interno .numeropedidointerno").val();
        service.procesar("getExcelPedidoInterno",nformato,function(evt){
            document.location = "webService/reportes/pedidoInterno_"+nformato+".xlsx"
        });
    })

    //FINAL



    //AREA DE ACTAS

    $("#modal_lista_formatos .formato").on('change', function(){
        var formato = $("#modal_lista_formatos .formato").val();
        service.procesar("getTablaFormatos",formato,nanio,resultadoCargaFormatos);
    })

    //FINAL DE ACTAS




    //AREA DE FORMULARIO ORDEN DE COMPRA - PROVISION - EXPENSES

    function cambioMonedaSistema(moneda,objeto){
        if (moneda == "DOLARES"){
            $("."+objeto).show();
        }else{
            $("."+objeto).hide();
        }
    }    

    function limpiarFormularioOCA(){
        $("#modalProvisionarSolicitudGastoPresupuesto .guardarformularioOCA").html("GUARDAR");
        $("#modalProvisionarSolicitudGastoPresupuesto .guardarformularioOCA").css("display","");
        $("#modalProvisionarSolicitudGastoPresupuesto .codigoOCA").html("");
        //$("#mensajeFormularioGastoPresupuesto").html("FORMULARIO INGRESO DE NUEVO REGISTRO");

        //$("#estadoFormularioSolicitudGastoPresupuesto option[value='INGRESADO']").attr("selected",true);
        $("#modalProvisionarSolicitudGastoPresupuesto .tipoOCA").val("SOCA");
        //CALCULAR EL VALOR CORRELATIVO DEL NUMERO SEGUN TIPO
        $("#modalProvisionarSolicitudGastoPresupuesto .numeroOCA").val("");
        $("#modalProvisionarSolicitudGastoPresupuesto .numeroOCA").prop("disabled",true);
        $("#modalProvisionarSolicitudGastoPresupuesto .estadoOCA").val("INGRESADO");
        $("#modalProvisionarSolicitudGastoPresupuesto .fechaOCA").val(fecha);

        $("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").val(0);
        $("#modalProvisionarSolicitudGastoPresupuesto .stringOCA").html("<option value='0'>---SELECCIONAR---</option>");
        $("#modalProvisionarSolicitudGastoPresupuesto .categoriaOCA").html("<option value='0'>---SELECCIONAR---</option>");
        $("#modalProvisionarSolicitudGastoPresupuesto .proyectoOCA").html("<option value='0'>---SELECCIONAR---</option>");
        $("#modalProvisionarSolicitudGastoPresupuesto .detalleOCA").html("<option value='0'>---SELECCIONAR---</option>");

        $("#modalProvisionarSolicitudGastoPresupuesto .proveedorOCA").val("0");
        $("#modalProvisionarSolicitudGastoPresupuesto .descripcionOCA").val("");

        $("#modalProvisionarSolicitudGastoPresupuesto .montoOCA").val("");
        $("#modalProvisionarSolicitudGastoPresupuesto .monedaOCA").val("SOLES");
        cambioMonedaSistema("SOLES","tcOCA");
        $("#modalProvisionarSolicitudGastoPresupuesto .tipocambioOCA").val("");
        $("#modalProvisionarSolicitudGastoPresupuesto .totalOCA").val("");

        $("#modalProvisionarSolicitudGastoPresupuesto .input-sm").prop("disabled", false);


        $("#modalProvisionarSolicitudGastoPresupuesto .detalleProvisionar").css("display","none");
    }

    function bloquearFormularioOCA(){
        $("#modalProvisionarSolicitudGastoPresupuesto .input-sm").prop("disabled", true);


    }

    $("#nuevoProvisionarSolicitudGastoPresupuesto").on('click', function(){
        limpiarFormularioOCA();
        correlativoTipoOCA("SOCA");
    })

    function correlativoTipoOCA(tipo){
        service.procesar("getCorrelativoTipoOCA",tipo,resultadoCorrelativoTipoOCA);
    }

    function resultadoCorrelativoTipoOCA(evt){
        var nuevoCorrelativoOCA = parseFloat(corregirNullNumero(evt)) + 1;
        $("#modalProvisionarSolicitudGastoPresupuesto .numeroOCA").val(nuevoCorrelativoOCA);
    }

    $("#modalProvisionarSolicitudGastoPresupuesto .monedaOCA").on('change', function(){
        var moneda = $("#modalProvisionarSolicitudGastoPresupuesto .monedaOCA").val();
        if (moneda == "DOLARES"){
            $("#modalProvisionarSolicitudGastoPresupuesto .tcOCA").show();
        }else{
            $("#modalProvisionarSolicitudGastoPresupuesto .tcOCA").hide();
        }
    })

    $("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").on('change',function(){
        area = $("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").val();

        var objeto = new Object()
            objeto.area = area;
            objeto.anio = nanio;
        service.procesar("getStringSolicitud",objeto,cargaStringOCA);
    });

    function cargaStringOCA(evt){
        resultado = evt;

        $("#modalProvisionarSolicitudGastoPresupuesto .stringOCA").html("<option value='0'>---SELECCIONAR---</option>");
        $("#modalProvisionarSolicitudGastoPresupuesto .categoriaOCA").html("<option value='0'>---SELECCIONAR---</option>");
        $("#modalProvisionarSolicitudGastoPresupuesto .proyectoOCA").html("<option value='0'>---SELECCIONAR---</option>");
        $("#modalProvisionarSolicitudGastoPresupuesto .detalleOCA").html("<option value='0'>---SELECCIONAR---</option>");

        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#modalProvisionarSolicitudGastoPresupuesto .stringOCA").append( "<option value='"+ resultado[i].chr_string_id +"'>"+ resultado[i].string +"</option>" );
        }
    }

    $("#modalProvisionarSolicitudGastoPresupuesto .stringOCA").on('change',function(){
        area = $("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").val();
        string = $("#modalProvisionarSolicitudGastoPresupuesto .stringOCA").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.anio = nanio;
        service.procesar("getCategoriasSolicitud",objeto,cargaCategoriaOCA);
    });

    function cargaCategoriaOCA(evt){
        resultado = evt;
        $("#modalProvisionarSolicitudGastoPresupuesto .categoriaOCA").html("<option value='0'>---SELECCIONAR---</option>");
        $("#modalProvisionarSolicitudGastoPresupuesto .proyectoOCA").html("<option value='0'>---SELECCIONAR---</option>");
        $("#modalProvisionarSolicitudGastoPresupuesto .detalleOCA").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#modalProvisionarSolicitudGastoPresupuesto .categoriaOCA" ).append( "<option value='"+ resultado[i].chr_categoria_id +"'>"+ resultado[i].chr_categoria_id + " - " + resultado[i].categoria +"</option>" );
        }
    }

    $("#modalProvisionarSolicitudGastoPresupuesto .categoriaOCA").on('change',function(){
        area = $("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").val();
        string = $("#modalProvisionarSolicitudGastoPresupuesto .stringOCA").val();
        categoria = $("#modalProvisionarSolicitudGastoPresupuesto .categoriaOCA").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.anio = nanio;
        service.procesar("getProyectosSolicitud",objeto,cargaProyectoOCA);
    });

    function cargaProyectoOCA(evt){
        resultado = evt;
        $("#modalProvisionarSolicitudGastoPresupuesto .proyectoOCA").html("<option value='0'>---SELECCIONAR---</option>");
        $("#modalProvisionarSolicitudGastoPresupuesto .detalleOCA").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#modalProvisionarSolicitudGastoPresupuesto .proyectoOCA" ).append( "<option value='"+ resultado[i].chr_proyecto_id +"'>"+ resultado[i].proyecto +"</option>" );
        }
    }


    $("#modalProvisionarSolicitudGastoPresupuesto .proyectoOCA").on('change',function(){
        area = $("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").val();
        string = $("#modalProvisionarSolicitudGastoPresupuesto .stringOCA").val();
        categoria = $("#modalProvisionarSolicitudGastoPresupuesto .categoriaOCA").val();
        proyecto = $("#modalProvisionarSolicitudGastoPresupuesto .proyectoOCA").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.proyecto = proyecto;
            objeto.anio = nanio;
        service.procesar("getDetallesSolicitud",objeto,cargaDetalleOCA);
    });

    function cargaDetalleOCA(evt){
        resultado = evt;
        $("#modalProvisionarSolicitudGastoPresupuesto .detalleOCA").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#modalProvisionarSolicitudGastoPresupuesto .detalleOCA" ).append( "<option value='"+ resultado[i].chr_comentario_id +"'>"+ resultado[i].chr_comentario +"</option>" );
        }
    }




    $("#modalProvisionarSolicitudGastoPresupuesto .detalleOCA").on('change',function(){
        consultarSaldoOCA();
    })

    function consultarSaldoOCA(){
        area = $("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").val();
        string = $("#modalProvisionarSolicitudGastoPresupuesto .stringOCA").val();
        categoria = $("#modalProvisionarSolicitudGastoPresupuesto .categoriaOCA").val();
        proyecto = $("#modalProvisionarSolicitudGastoPresupuesto .proyectoOCA").val();
        comentario = $("#modalProvisionarSolicitudGastoPresupuesto .detalleOCA").val();
        fechaOCA = $("#modalProvisionarSolicitudGastoPresupuesto .fechaOCA").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.proyecto = proyecto;
            objeto.comentario = comentario;
            objeto.fecha = fechaOCA;
            objeto.anio = nanio;
        service.procesar("consultarSolicitudSaldoPresupuesto",objeto,resultadoConsultarOcaSaldoPresupuesto);        
    }

    function resultadoConsultarOcaSaldoPresupuesto(evt){
        var resultado = evt;
        var trimestre = evt.trimestre;
        var mes = evt.mes;
        saldoMesOCA = corregirNullNumero(resultado.saldo_m);
        $("#modalProvisionarSolicitudGastoPresupuesto .saldoOCAM").html("( "+ mes +" ) " + formatNumber.new(parseFloat(corregirNullNumero(resultado.saldo_m)).toFixed(2), "S/. "));
        $("#modalProvisionarSolicitudGastoPresupuesto .saldoOCAQ").html("( Q-"+ trimestre +" ) " + formatNumber.new(parseFloat(corregirNullNumero(resultado.saldo_q)).toFixed(2), "S/. "));
        $("#modalProvisionarSolicitudGastoPresupuesto .saldoOCAT").html("( ANUAL ) " + formatNumber.new(parseFloat(corregirNullNumero(resultado.saldo_y)).toFixed(2), "S/. "));
    }



    $("#modalProvisionarSolicitudGastoPresupuesto .guardarformularioOCA").on('click', function(){
        consultarSaldoOCA();
        var tipoOCA = $("#modalProvisionarSolicitudGastoPresupuesto .tipoOCA").val();
        var numeroOCA = $("#modalProvisionarSolicitudGastoPresupuesto .numeroOCA").val();
        var estadoOCA = $("#modalProvisionarSolicitudGastoPresupuesto .estadoOCA").val();
        var fechaOCA = $("#modalProvisionarSolicitudGastoPresupuesto .fechaOCA").val();
        var areaOCA = $("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").val();
        var stringOCA = $("#modalProvisionarSolicitudGastoPresupuesto .stringOCA").val();
        var categoriaOCA = $("#modalProvisionarSolicitudGastoPresupuesto .categoriaOCA").val();
        var proyectoOCA = $("#modalProvisionarSolicitudGastoPresupuesto .proyectoOCA").val();
        var detalleOCA = $("#modalProvisionarSolicitudGastoPresupuesto .detalleOCA").val();
        var proveedorOCA = $("#modalProvisionarSolicitudGastoPresupuesto .proveedorOCA").val();
        var descripcionOCA = $("#modalProvisionarSolicitudGastoPresupuesto .descripcionOCA").val();
        var montoOCA = $("#modalProvisionarSolicitudGastoPresupuesto .montoOCA").val();
        var monedaOCA = $("#modalProvisionarSolicitudGastoPresupuesto .monedaOCA").val();
        var tipocambioOCA = $("#modalProvisionarSolicitudGastoPresupuesto .tipocambioOCA").val();
        var totalOCA = $("#modalProvisionarSolicitudGastoPresupuesto .totalOCA").val();
        var procedimiento = $("#modalProvisionarSolicitudGastoPresupuesto .guardarformularioOCA").html();
        var codigoOCA = $("#modalProvisionarSolicitudGastoPresupuesto .codigoOCA").val();

        var fechaRegistro = fecha;

        if(monedaOCA == "SOLES"){
            tipocambioOCA = 0;
            totalOCA = montoOCA;
        }

        //var proceso = $("#modalProvisionarSolicitudGastoPresupuesto .guardarformularioOCA").html();

        if(fechaOCA != "" && areaOCA != 0 && stringOCA != 0 && categoriaOCA != 0 && proyectoOCA != 0 && detalleOCA != 0 && descripcionOCA != 0){

            if(parseFloat(saldoMesOCA) >= parseFloat(montoOCA)){

                alertify.confirm("¿SEGURO DE GUARDAR FORMULARIO : "+ tipoOCA +" - " + numeroOCA  +" ?", function (e) {
                    if (e) {
                        
                        var objetoFormulario = new Object()
                            objetoFormulario.procedimiento = procedimiento;
                            objetoFormulario.idPresupuesto = codigoOCA; //PARA EDITAR
                            objetoFormulario.estado = estadoOCA;
                            objetoFormulario.fechaGasto = fechaOCA;
                            objetoFormulario.fechaRegistro = fechaRegistro;
                            objetoFormulario.area = areaOCA;
                            objetoFormulario.string = stringOCA;
                            objetoFormulario.categoria = categoriaOCA;
                            objetoFormulario.proyecto = proyectoOCA;
                            objetoFormulario.comentario = detalleOCA;
                            objetoFormulario.ndocumento = ''; //NUMERO DE DOCUMENTO OEP
                            objetoFormulario.fechafactura = fechaOCA;
                            objetoFormulario.proveedor = proveedorOCA;
                                                //objetoFormulario.colaboradores = colaboradores; //LISTA DE COLABORADORES
                                                //objetoFormulario.actividad = actividad; YA NO EXISTE
                            objetoFormulario.descripcion = descripcionOCA;
                            objetoFormulario.monto = montoOCA;
                            objetoFormulario.moneda = monedaOCA;
                            objetoFormulario.tipocambio = tipocambioOCA;
                            objetoFormulario.total = totalOCA;
                                                //
                            objetoFormulario.idFormato = tipoOCA;
                            objetoFormulario.nformato = numeroOCA;
                            objetoFormulario.usuario = usuario;

                            service.procesar("saveFormularioSolicitud",objetoFormulario,function(evt){
                                if(evt != "ERROR"){
                                    alertify.success("INFORMACION GUARDADA CORRECTAMENTE");
                                    $("#modalProvisionarSolicitudGastoPresupuesto").modal("hide");
                                    resultadoSaveProvisionarSolicitudGastoPresupuesto();
                                }else{
                                    alertify.error("INFORMACION NO GUARDADA CORRECTAMENTE");
                                }
                            });


                    } else {
                        alertify.error("PROCEDIMIENTO CANCELADO");
                    }
                });

            }else{

                alertify.confirm("NO CUENTA CON SALDO ¿SEGURO DE GUARDAR FORMULARIO : "+ tipoOCA +" - " + numeroOCA  +" ?", function (e) {
                    if (e) {

                        var objetoFormulario = new Object()
                            objetoFormulario.procedimiento = procedimiento;
                            objetoFormulario.idPresupuesto = codigoOCA; //PARA EDITAR
                            objetoFormulario.estado = estadoOCA;
                            objetoFormulario.fechaGasto = fechaOCA;
                            objetoFormulario.fechaRegistro = fechaRegistro;
                            objetoFormulario.area = areaOCA;
                            objetoFormulario.string = stringOCA;
                            objetoFormulario.categoria = categoriaOCA;
                            objetoFormulario.proyecto = proyectoOCA;
                            objetoFormulario.comentario = detalleOCA;
                            objetoFormulario.ndocumento = ''; //NUMERO DE DOCUMENTO OEP
                            objetoFormulario.fechafactura = fechaOCA;
                            objetoFormulario.proveedor = proveedorOCA;
                                                //objetoFormulario.colaboradores = colaboradores; //LISTA DE COLABORADORES
                                                //objetoFormulario.actividad = actividad; YA NO EXISTE
                            objetoFormulario.descripcion = descripcionOCA;
                            objetoFormulario.monto = montoOCA;
                            objetoFormulario.moneda = monedaOCA;
                            objetoFormulario.tipocambio = tipocambioOCA;
                            objetoFormulario.total = totalOCA;
                                                //
                            objetoFormulario.idFormato = tipoOCA;
                            objetoFormulario.nformato = numeroOCA;
                            objetoFormulario.usuario = usuario;

                            service.procesar("saveFormularioSolicitud",objetoFormulario,function(evt){
                                if(evt != "ERROR"){
                                    alertify.success("INFORMACION GUARDADA CORRECTAMENTE");
                                    $("#modalProvisionarSolicitudGastoPresupuesto").modal("hide");
                                    resultadoSaveProvisionarSolicitudGastoPresupuesto();
                                }else{
                                    alertify.error("INFORMACION NO GUARDADA CORRECTAMENTE");
                                }
                            });

                    } else {
                        alertify.error("PROCEDIMIENTO CANCELADO");
                    }
                });

            }

        }else{
            alertify.error("COMPLETAR INFORMACION DEL FORMULARIO");
        }
    })


    $("#modalProvisionarSolicitudGastoPresupuesto .tipoOCA").on('change', function(){
        var tipo = $("#modalProvisionarSolicitudGastoPresupuesto .tipoOCA").val();
        if(tipo == "SOCA"){
            $("#modalProvisionarSolicitudGastoPresupuesto .proveedor").css("display","");
        }else{
            $("#modalProvisionarSolicitudGastoPresupuesto .proveedor").css("display","none");
        }
        correlativoTipoOCA(tipo);
    })

    function saveProvisionarSolicitudGastoPresupuesto(){
        var tipoOCA = $("#modalProvisionarSolicitudGastoPresupuesto .tipoOCA").val();
        var numeroOCA = $("#modalProvisionarSolicitudGastoPresupuesto .numeroOCA").val();
        var estadoOCA = $("#modalProvisionarSolicitudGastoPresupuesto .estadoOCA").val();
        var fechaOCA = $("#modalProvisionarSolicitudGastoPresupuesto .fechaOCA").val();
        var areaOCA = $("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").val();
        var stringOCA = $("#modalProvisionarSolicitudGastoPresupuesto .stringOCA").val();
        var categoriaOCA = $("#modalProvisionarSolicitudGastoPresupuesto .categoriaOCA").val();
        var proyectoOCA = $("#modalProvisionarSolicitudGastoPresupuesto .proyectoOCA").val();
        var detalleOCA = $("#modalProvisionarSolicitudGastoPresupuesto .detalleOCA").val();
        var proveedorOCA = $("#modalProvisionarSolicitudGastoPresupuesto .proveedorOCA").val();
        var descripcionOCA = $("#modalProvisionarSolicitudGastoPresupuesto .descripcionOCA").val();
        var montoOCA = $("#modalProvisionarSolicitudGastoPresupuesto .montoOCA").val();
        var monedaOCA = $("#modalProvisionarSolicitudGastoPresupuesto .monedaOCA").val();
        var tipocambioOCA = $("#modalProvisionarSolicitudGastoPresupuesto .tipocambioOCA").val();
        var totalOCA = $("#modalProvisionarSolicitudGastoPresupuesto .totalOCA").val();

        var procedimiento = $("#modalProvisionarSolicitudGastoPresupuesto .guardarformularioOCA").html();
        var codigoOCA = $("#modalProvisionarSolicitudGastoPresupuesto .codigoOCA").val();

        var fechaRegistro = fecha;

        if(monedaOCA == "SOLES"){
            tipocambioOCA = 0;
            totalOCA = montoOCA;
        }


        var objetoFormulario = new Object()
            objetoFormulario.procedimiento = procedimiento;
            objetoFormulario.idPresupuesto = codigoOCA; //PARA EDITAR
            objetoFormulario.estado = estadoOCA;
            objetoFormulario.fechaGasto = fechaOCA;
            objetoFormulario.fechaRegistro = fechaRegistro;
            objetoFormulario.area = areaOCA;
            objetoFormulario.string = stringOCA;
            objetoFormulario.categoria = categoriaOCA;
            objetoFormulario.proyecto = proyectoOCA;
            objetoFormulario.comentario = detalleOCA;
            objetoFormulario.ndocumento = ''; //NUMERO DE DOCUMENTO OEP
            objetoFormulario.fechafactura = fechaOCA;
            objetoFormulario.proveedor = proveedorOCA;
                                //objetoFormulario.colaboradores = colaboradores; //LISTA DE COLABORADORES
                                //objetoFormulario.actividad = actividad; YA NO EXISTE
            objetoFormulario.descripcion = descripcionOCA;
            objetoFormulario.monto = montoOCA;
            objetoFormulario.moneda = monedaOCA;
            objetoFormulario.tipocambio = tipocambioOCA;
            objetoFormulario.total = totalOCA;
                                //
            objetoFormulario.idFormato = tipoOCA;
            objetoFormulario.nformato = numeroOCA;
            objetoFormulario.usuario = usuario;

            service.procesar("saveFormularioSolicitud",objetoFormulario,function(evt){
                if(evt != "ERROR"){
                    alertify.success("INFORMACION GUARDADA CORRECTAMENTE");
                    $("#modalProvisionarSolicitudGastoPresupuesto").modal("hide");
                    resultadoSaveProvisionarSolicitudGastoPresupuesto();
                }else{
                    alertify.error("INFORMACION NO GUARDADA CORRECTAMENTE");
                }
            });











/*        var objeto = new Object()
            objeto.tipoOCA = tipoOCA;
            objeto.numeroOCA = numeroOCA;
            objeto.estadoOCA = estadoOCA;
            objeto.fechaOCA = fechaOCA;
            objeto.areaOCA = areaOCA;
            objeto.stringOCA = stringOCA;
            objeto.categoriaOCA = categoriaOCA;
            objeto.proyectoOCA = proyectoOCA;
            objeto.detalleOCA = detalleOCA;
            objeto.proveedorOCA = proveedorOCA;
            objeto.descripcionOCA = descripcionOCA;
            objeto.montoOCA = montoOCA;
            objeto.monedaOCA = monedaOCA;
            objeto.tipocambioOCA = tipocambioOCA;
            objeto.tipoOCA = tipoOCA;
            objeto.totalOCA = totalOCA;
            objeto.proceso = proceso;
            //objeto.anio = nanio;
            */

    }


    function resultadoSaveProvisionarSolicitudGastoPresupuesto(){
        var tipo = $("#provisionarSolicitudGastoPresupuesto .tipo").val();
        var estado = $("#provisionarSolicitudGastoPresupuesto .estado").val();

        var objeto = new Object()
            objeto.tipo = tipo;
            objeto.estado = estado;

        service.procesar("getConsultarProvisionarSolicitudGastoPresupuesto",objeto,nanio,resultadoConsultarProvisionarSolicitudGastoPresupuesto);
    }


    function resultadoConsultarProvisionarSolicitudGastoPresupuesto(evt){
        resultado = evt;

        $("#provisionarSolicitudGastoPresupuesto .tablaProvisionarSolicitudGastoPresupuesto tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){

            var fila = $("<tr>");
            var celdaBotones = $("<td>");
            datoRegistro = resultado[i];
            var monto = parseFloat(resultado[i].total);
            monto = formatNumber.new(monto.toFixed(2), "S/. ");
            var saldo = parseFloat(resultado[i].total) - parseFloat(corregirNullNumero(resultado[i].gastos));
            saldo = formatNumber.new(saldo.toFixed(2), "S/. ");
            fila.html('<td>'+ (i + 1) +'</td><td>'+ resultado[i].tipo +'</td><td>'+ resultado[i].numero +'</td><td>'+ resultado[i].chr_area +'</td><td>'+ resultado[i].chr_string_id +'</td><td>'+ resultado[i].chr_categoria_id +'</td><td>'+ resultado[i].chr_proyecto +'</td><td>'+ resultado[i].chr_comentario +'</td><td>'+ monto +'</td><td>'+ saldo +'</td><td>'+ resultado[i].fechaGasto +'</td><td>'+ resultado[i].usuario +'</td><td>'+ resultado[i].estado +'</td>');

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-primary btn-xs' title='Ver'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-eye-open"></span');
            btnSeleccionar.data("data",datoRegistro);
            btnSeleccionar.on("click",visualizarProvisionarSolicitudGastoPresupuesto);

            var btnEditar = $("<button class='btn btn-success btn-xs' title='Editar'>");
            btnEditar.html('<span class="glyphicon glyphicon-edit"></span');
            btnEditar.data("data",datoRegistro);
            btnEditar.on("click",editarProvisionarSolicitudGastoPresupuesto);

            var btnEliminar = $("<button class='btn btn-danger btn-xs' title='Eliminar'>");
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span');
            btnEliminar.data("data",datoRegistro);
            btnEliminar.on("click",eliminarProvisionarSolicitudGastoPresupuesto);

            var btnLiquidar = $("<button class='btn btn-warning btn-xs' title='Liquidar'>");
            btnLiquidar.html('<span class="glyphicon glyphicon-off"></span');
            btnLiquidar.data("data",datoRegistro);
            btnLiquidar.on("click",liquidarProvisionarSolicitudGastoPresupuesto);

            var btnImprimir = $("<button class='btn btn-info btn-xs' title='Imprimir'>");
            btnImprimir.html('<span class="glyphicon glyphicon-print"></span');
            btnImprimir.data("data",datoRegistro);
            btnImprimir.on("click",imprimirProvisionarSolicitudGastoPresupuesto);

            contenedorBotones.append(btnSeleccionar);
            contenedorBotones.append(" ");

            if(resultado[i].estado == "APROBADO"){
                contenedorBotones.append(btnLiquidar);
                contenedorBotones.append(" ");
            }

            if(resultado[i].estado != "APROBADO" && resultado[i].estado != "LIQUIDADO"){
                contenedorBotones.append(btnEditar);
                contenedorBotones.append(" ");
                contenedorBotones.append(btnEliminar);
                contenedorBotones.append(" "); 
            }

            contenedorBotones.append(btnImprimir);
            contenedorBotones.append(btnImprimir);

            fila.append(contenedorBotones);

            $("#provisionarSolicitudGastoPresupuesto .tablaProvisionarSolicitudGastoPresupuesto tbody").append(fila);

        }
    }

    function liquidarProvisionarSolicitudGastoPresupuesto(){
        var resultado = $(this).data("data");
        alertify.confirm("¿ SEGURO DE LIQUIDAR : " + resultado.tipo + "-" + resultado.numero +" ?", function (e) {
            if (e) {
                alertify.success("HA ACEPTADO LIQUIDAR EL REGISTRO");
                var objeto = new Object()
                    objeto.tipo = resultado.tipo;
                    objeto.numero = resultado.numero;
                    objeto.mes = resultado.mes;
                    objeto.monto = parseFloat(resultado.monto) - parseFloat(resultado.gastos);
                    objeto.fechaRegistro = fecha;
                    objeto.anio = nanio;
                    objeto.usuario = usuario;

                    service.procesar("liquidarRegistroSolicitudGastoPresupuesto",objeto,function(evt){
                    resultadoSaveProvisionarSolicitudGastoPresupuesto();
                    alertify.success("REGISTRO LIQUIDADO SATISFACTORIAMENTE");
                });
                
            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO");
            }
        });
    }

    function visualizarProvisionarSolicitudGastoPresupuesto(){
        var resultado = $(this).data("data");
        limpiarFormularioOCA();
        $("#modalProvisionarSolicitudGastoPresupuesto").modal("show");
        bloquearFormularioOCA();
        mostrarInformacionFormularioOCA(resultado,"VER");

    }

    function editarProvisionarSolicitudGastoPresupuesto(){
        var resultado = $(this).data("data");
        limpiarFormularioOCA();
        $("#modalProvisionarSolicitudGastoPresupuesto").modal("show");
        mostrarInformacionFormularioOCA(resultado,"EDITAR");
        
    }

    function eliminarProvisionarSolicitudGastoPresupuesto(){
        var resultado = $(this).data("data");

        alertify.confirm("¿ SEGURO DE ELIMINAR EL REGISTRO : " + resultado.tipo + "-" + resultado.numero +" ?", function (e) {
            if (e) {
                alertify.success("HA ACEPTADO ELIMINAR EL REGISTRO");
                service.procesar("deleteRegistroSolicitudGastoPresupuesto",resultado.idOca,function(evt){
                    resultadoSaveProvisionarSolicitudGastoPresupuesto();
                    alertify.success("REGISTRO ELIMINADO SATISFACTORIAMENTE");
                });
                
            } else {
                alertify.error("HA CANCELADO EL PROCEDIMIENTO DE ELIMINACION");
            }
        });
    }

    function imprimirProvisionarSolicitudGastoPresupuesto(){
        var resultado = $(this).data("data");
        
        var formato = (resultado.tipo).substr(1,3);
        var numero = formato+'-'+resultado.numero;
        var codigo = resultado.idOca;

        window.open("reportes/reporteSolicitudOrdenProvision.php?formato="+formato+"&codigo="+codigo+"&numero="+numero);
    }

    function mostrarInformacionFormularioOCA(evt,tipo){
        var resultado = evt;
        $("#modalProvisionarSolicitudGastoPresupuesto .tipoOCA").val(resultado.tipo);
        $("#modalProvisionarSolicitudGastoPresupuesto .numeroOCA").val(resultado.numero);
        $("#modalProvisionarSolicitudGastoPresupuesto .estadoOCA").val(resultado.estado);
        $("#modalProvisionarSolicitudGastoPresupuesto .fechaOCA").val(resultado.fechaGasto);



        $("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").val(resultado.chr_area_id);
        cargaStringOCA_VMC();
        $("#modalProvisionarSolicitudGastoPresupuesto .stringOCA").append("<option value='"+resultado.chr_string_id+"' selected>"+corregirNull(resultado.string).toUpperCase()+"</option>");
        cargaCategoriaOCA_VMC();
        $("#modalProvisionarSolicitudGastoPresupuesto .categoriaOCA").append("<option value='"+resultado.chr_categoria_id+"' selected>"+ resultado.chr_categoria_id + " - " + corregirNull(resultado.categoria).toUpperCase() +"</option>");
        cargaProyectoOCA_VMC();
        $("#modalProvisionarSolicitudGastoPresupuesto .proyectoOCA").append("<option value='"+resultado.chr_proyecto_id+"' selected>"+corregirNull(resultado.chr_proyecto).toUpperCase()+"</option>");
        cargaDetalleOCA_VMC();
        $("#modalProvisionarSolicitudGastoPresupuesto .detalleOCA").append("<option value='"+resultado.chr_comentario_id+"' selected>"+corregirNull(resultado.chr_comentario).toUpperCase()+"</option>");
        cargaSaldoOCA_VMC();

        $("#modalProvisionarSolicitudGastoPresupuesto .proveedorOCA").val(resultado.proveedor);
        $("#modalProvisionarSolicitudGastoPresupuesto .descripcionOCA").val(resultado.descripcion);

        $("#modalProvisionarSolicitudGastoPresupuesto .montoOCA").val(resultado.monto);
        $("#modalProvisionarSolicitudGastoPresupuesto .monedaOCA").val(resultado.moneda);
        cambioMonedaSistema(resultado.moneda,"tcOCA");
        $("#modalProvisionarSolicitudGastoPresupuesto .tipocambioOCA").val(resultado.tipocambio);
        $("#modalProvisionarSolicitudGastoPresupuesto .totalOCA").val(resultado.total);

        if(tipo == "EDITAR"){
            $("#modalProvisionarSolicitudGastoPresupuesto .guardarformularioOCA").html("MODIFICAR");
            $("#modalProvisionarSolicitudGastoPresupuesto .guardarformularioOCA").css("display","");
            $("#modalProvisionarSolicitudGastoPresupuesto .codigoOCA").val(resultado.idOca);
            $("#modalProvisionarSolicitudGastoPresupuesto .detalleProvisionar").css("display","none");
        }
        if(tipo == "VER"){
            $("#modalProvisionarSolicitudGastoPresupuesto .guardarformularioOCA").css("display","none");
            $("#modalProvisionarSolicitudGastoPresupuesto .detalleProvisionar").css("display","");
        }

        var saldo = parseFloat(resultado.total) - parseFloat(corregirNullNumero(resultado.gastos));
        saldo = saldo.toFixed(0);

        $("#modalProvisionarSolicitudGastoPresupuesto .saldoOCA").val(saldo);

        var tipo = resultado.tipo.substr(1, 3);
        var codigo = resultado.idOca;

        service.procesar("getDetallesSolicitudSOCASPRO",tipo,codigo,function(evt){
            var resultado = evt;
            $("#modalProvisionarSolicitudGastoPresupuesto .tablaDetalleProvisionarSolicitudGastoPresupuesto tbody").html("");

            if ( resultado == undefined ) return;

            for(var i=0; i<resultado.length ; i++){

                var fila = $("<tr>");
                var celdaBotones = $("<td>");
                datoRegistro = resultado[i];
                var monto = parseFloat(resultado[i].total);
                monto = formatNumber.new(monto.toFixed(2), "S/. ");
                var saldo = parseFloat(resultado[i].total) - parseFloat(corregirNullNumero(resultado[i].gastos));
                saldo = formatNumber.new(saldo.toFixed(2), "S/. ");
                fila.html('<td>'+ (i + 1) +'</td><td>'+ corregirNull(resultado[i].chr_area) +'</td><td>'+ corregirNull(resultado[i].chr_string_id) +'</td><td>'+ corregirNull(resultado[i].chr_categoria_id) +'</td><td>'+ corregirNull(resultado[i].chr_proyecto) +'</td><td>'+ corregirNull(resultado[i].chr_comentario) +'</td><td>'+ resultado[i].mes +'</td><td>'+ resultado[i].total +'</td><td>'+ resultado[i].usuario +'</td><td>'+ resultado[i].estado +'</td>');

                var contenedorBotones = $("<td><div class='input-group-btn'>");

                var btnEliminar = $("<button class='btn btn-danger btn-xs' title='Eliminar'>");
                btnEliminar.html('<span class="glyphicon glyphicon-remove"></span');
                btnEliminar.data("data",datoRegistro);
                btnEliminar.on("click",eliminarProvisionarSolicitudGastoPresupuesto);

                if(resultado[i].estado != "APROBADO"){
                    contenedorBotones.append(btnEliminar);
                }else{
                    contenedorBotones.append(" ");
                }

                //fila.append(contenedorBotones);

                $("#modalProvisionarSolicitudGastoPresupuesto .tablaDetalleProvisionarSolicitudGastoPresupuesto tbody").append(fila);

            }



        })



    }

    $("#provisionarSolicitudGastoPresupuesto .tipo").on('change', function(){
        resultadoSaveProvisionarSolicitudGastoPresupuesto();
    })

    $("#provisionarSolicitudGastoPresupuesto .estado").on('change', function(){
        resultadoSaveProvisionarSolicitudGastoPresupuesto();
    })

    function cargaStringOCA_VMC(){
        area = $("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.anio = nanio;
            service.procesar("getStringSolicitud",objeto,function(evt){
                if ( evt == undefined ) return;
                for(var i=0; i<evt.length ; i++){
                    $("#modalProvisionarSolicitudGastoPresupuesto .stringOCA").append( "<option value='"+ evt[i].chr_string_id +"'>"+ evt[i].string +"</option>" );
                }  
                      
            });
    }

    function cargaCategoriaOCA_VMC(){
        area = $("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").val();
        string = $("#modalProvisionarSolicitudGastoPresupuesto .stringOCA").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.anio = nanio;
        service.procesar("getCategoriasSolicitud",objeto,function(evt){
            if ( evt == undefined ) return;
            for(var i=0; i<evt.length ; i++){
                $("#modalProvisionarSolicitudGastoPresupuesto .categoriaOCA" ).append( "<option value='"+ evt[i].chr_categoria_id +"'>"+ evt[i].chr_categoria_id + " - " + evt[i].categoria +"</option>" );
            }
        });
    }

    function cargaProyectoOCA_VMC(){
        area = $("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").val();
        string = $("#modalProvisionarSolicitudGastoPresupuesto .stringOCA").val();
        categoria = $("#modalProvisionarSolicitudGastoPresupuesto .categoriaOCA").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.anio = nanio;
        service.procesar("getProyectosSolicitud",objeto,function(evt){
            if ( evt == undefined ) return;
            for(var i=0; i<evt.length ; i++){
                $("#modalProvisionarSolicitudGastoPresupuesto .proyectoOCA" ).append( "<option value='"+ evt[i].proyecto +"'>"+ evt[i].proyecto +"</option>" );
            }
        });
    }

    function cargaDetalleOCA_VMC(){
        area = $("#modalProvisionarSolicitudGastoPresupuesto .areaOCA").val();
        string = $("#modalProvisionarSolicitudGastoPresupuesto .stringOCA").val();
        categoria = $("#modalProvisionarSolicitudGastoPresupuesto .categoriaOCA").val();
        proyecto = $("#modalProvisionarSolicitudGastoPresupuesto .proyectoOCA").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.proyecto = proyecto;
            objeto.anio = nanio;
        service.procesar("getDetallesSolicitud",objeto,function(evt){
            if ( evt == undefined ) return;
            for(var i=0; i<evt.length ; i++){
                $("#modalProvisionarSolicitudGastoPresupuesto .detalleOCA" ).append( "<option value='"+ evt[i].chr_comentario_id +"'>"+ evt[i].chr_comentario +"</option>" );
            }
        });
    }


    function cargaSaldoOCA_VMC(){
        consultarSaldoOCA();
    }

    /*
    $("#chkNuevoPresupuestoSolicitudPresupuesto").on('click', function(){
        var dato = $("#chkNuevoPresupuestoSolicitudPresupuesto:checked")["length"];
        console.log(dato);
    })
    */
    























    function activaTab(tab){
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    };


    function corregirNullNumero(valor){
        var nuevovalor = 0.00;
        if(valor == null){
          nuevovalor = 0.00;
        } else {
          nuevovalor = valor;
        }
        return nuevovalor;
    }

    function corregirNull(valor){
        var nuevovalor = "";
        if(valor == null){
          nuevovalor = "";
        } else {
          nuevovalor = valor;
        }
        return nuevovalor;
    }

	function distinct(array,indice){
	  var uniques = []; //temporal
	  return array.filter(function(item){
	    //indexOf buscará el valor, si no existe retornará true, por lo cual se mantendrá en el arreglo 
	    //false en caso el valor ya exista en la variable uniques
	    return uniques.indexOf(item[indice]) < 0 ? uniques.push(item[indice]) : false
	  })
	}

	function sinduplicados(arr) {
		var i,
	    	len=arr.length,
	    	out=[],
	    	obj={};

		for (i=0;i<len;i++) {
			obj[arr[i]]=0;
		}
		for (i in obj) {
			out.push(i);
		}
		return out;
	}

    var formatNumber = {
     separador: ",", // separador para los miles
     sepDecimal: '.', // separador para los decimales
     formatear:function (num){
      num +='';
      var splitStr = num.split(',');
      var splitLeft = splitStr[0];
      var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
      var regx = /(\d+)(\d{3})/;
      while (regx.test(splitLeft)) {
      splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
      }
      return this.simbol + splitLeft  +splitRight;
     },
     new:function(num, simbol){
      this.simbol = simbol ||'';
      return this.formatear(num);
     }
    }

    function exceltojson(e) {
        var files = e.target.files;
        var f = files[0];
        var X = XLSX;
        {
            var reader = new FileReader();
            //var name = f.name;
            reader.onload = function(e) {
                var data = e.target.result;
                wb = X.read(data, {type: 'binary'});
                var roa = "";
                wb.SheetNames.forEach(function(sheetName) {
                    roa = X.utils.sheet_to_json(wb.Sheets[sheetName]);
                });
                jsonExcel = roa;
                cargaPagoOep(jsonExcel);
            };
            reader.readAsBinaryString(f);
        }
    }

    function removeItemArray ( arr, item ) {
        var i = arr.indexOf( item );
        arr.splice( i, 1 );
    }

    function getLista (array,atributo){
        var nuevoArray = []
        for ( i = 0; i < array.length; i++) {
            valor = array[i][atributo];
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }

    function buscarProcedencia(valor,base){
      for(var i = 0; i < base.length; i++){
        if(base[i].procedencia == valor){
          return base[i].idProcedencia;
        }

      }
    }

    function buscarIdProcedencia(valor,base){
      for(var i = 0; i < base.length; i++){
        if(base[i].procedencia == valor){
          return base[i].idProcedencia;
        }

      }
    }

    function uniqueArray(arr){
        let unique_array = []
        for(let i = 0;i < arr.length; i++){
            if(unique_array.indexOf(arr[i]) == -1){
                unique_array.push(arr[i])
            }
        }
        return unique_array
    }




}

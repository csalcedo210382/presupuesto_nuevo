var service = new Service("webService/index.php");
var FormValidatorTask = {};
//var cuestionario = Array;
$(function () {
    //solo iniciar las cargas cuando se ingresa al módulo
    $("#menu_presupuesto_programacion").on('click', getTareas);
});

function getTareas() {
    //obtenemos las tareas desde el servidor
    var objRequest = new Object();
    objRequest.subject = 'getTareas';
    service.procesar("getTareas", objRequest, printLoadServiceTaskManager);
}

function printLoadServiceTaskManager(response) {
    switch (response.subject) {
        case 'activeNotification':
            getTareas();
            break;
        case 'saveAlerta':
            //recargar el listado de notificaciones en la vista inicial de manejador de notificaciones
            getTareas();
            cancelarFormularioNotificacion();
            break;
        case 'getTareas':
            removeDataTableTaskManager('tableTaskManager');
            $("#tableTaskManagerBody").html(response.data.html);
            loadDataTableTaskManager('tableTaskManager');
            break;
        case 'loadFormularioTask':
            $("#divFormTaskManager").show();
            $("#divListaTaskManager").hide();
            $("#divContentFormTaskManager").html(response.data.form);
            //Agregar evento onchange para mostrar el detalle de la periocidad
            $("#inputPeriodo").on('change', loadDetallePeriocidad);
            //Validamos el formulario
            addValidateFormTareas('formCrudTaskManager');
            break;
        case 'loadDetallePeriocidad':
            $("#divDetallePeriocidad").html(response.data.html);
            //eliminamos las validaciones que existiera
            $("#inputHora").rules("remove");
            $("#inputDia").rules("remove");
            $("#inputDiaMes").rules("remove");
            $("#inputMes").rules("remove");
            //agregamos las validaciones a los nuevo inputs
            switch (response.data.periocidadId) {
                case '1'://diario
                    $("#inputHora").rules('add', {required: true, messages: {required: "Seleccione la hora de la alerta"}});
                    break;
                case '2'://semanal
                    $("#inputHora").rules('add', {required: true, messages: {required: "Seleccione la hora de la alerta"}});
                    $("#inputDia").rules('add', {required: true, messages: {required: "Seleccione un día de la semana"}});
                    break;
                case '3'://mensual
                    $("#inputHora").rules('add', {required: true, messages: {required: "Seleccione la hora de la alerta"}});
                    $("#inputDiaMes").rules('add', {required: true, messages: {required: "Seleccione un día del mes"}});
                    break;
                case '4'://trimestral
                    $("#inputHora").rules('add', {required: true, messages: {required: "Seleccione la hora de la alerta"}});
                    $("#inputDiaMes").rules('add', {required: true, messages: {required: "Seleccione un día del mes"}});
                    $("#inputMes").rules('add', {required: true, messages: {required: "Seleccione un mes del trimestre"}});
                    break;
            }
            break;
    }
}

function loadDetallePeriocidad() {
    var objRequest = new Object();
    objRequest.subject = 'loadDetallePeriocidad';
    objRequest.periocidadId = $(this).val();
    service.procesar("loadDetallePeriocidad", objRequest, printLoadServiceTaskManager);
}

function cancelarFormularioNotificacion() {
    $("#divFormTaskManager").hide();
    $("#divListaTaskManager").show();
}

function loadFormularioTask(tareaId) {
    var objRequest = new Object();
    objRequest.subject = 'loadFormularioTask';
    objRequest.tareaId = tareaId;
    service.procesar("loadFormularioTask", objRequest, printLoadServiceTaskManager);
}

function saveAlerta() {
    var objRequest = new Object();
    objRequest.subject = 'saveAlerta';
    objRequest.tareaId = $("#tareaId").val();
    objRequest.int_periocidadId = $("#inputPeriodo").val();
    objRequest.int_typeId = $("#typeId").val();
    objRequest.chr_name = $("#inputTitle").val();
    objRequest.chr_description = $("#inputDescription").val();
    objRequest.chr_hora = $("#inputHora").val();
    objRequest.chr_dia_semana = $("#inputDia").val();
    objRequest.chr_dia_mes = $("#inputDiaMes").val();
    objRequest.chr_mes = $("#inputMes").val();
    objRequest.chr_from = $("#inputFrom").val();
    objRequest.chr_to = $("#inputTo").val();
    objRequest.chr_subject = $("#inputSubject").val();
    objRequest.txt_body = $("#inputTemplate").val();
    objRequest.int_categoryid = $("#inputCategory").val();
    service.procesarPost("saveAlerta", objRequest, printLoadServiceTaskManager);
}

function activeNotification(id, status) {
    swal({
        title: "¿Estás seguro de realizar esta acción?",
        text: "Podrás reestaurar este item haciendo click en el mismo icono",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
            .then((willDelete) => {
                if (willDelete) {
                    var objRequest = new Object();
                    objRequest.subject = 'activeNotification';
                    objRequest.id = id;
                    objRequest.is_active = status;
                    service.procesarPost("activeNotification", objRequest, printLoadServiceTaskManager);
                }
            });
}

function testNotificacion() {
    swal("Ingrese el correo al cual se realizará el test:", {
        content: "input"
    })
            .then((value) => {
                var objRequest = new Object();
                objRequest.subject = 'testNotificacion';
                objRequest.tareaId = $("#tareaId").val();
                objRequest.int_periocidadId = $("#inputPeriodo").val();
                objRequest.int_typeId = $("#typeId").val();
                objRequest.chr_name = $("#inputTitle").val();
                objRequest.chr_description = $("#inputDescription").val();
                objRequest.chr_hora = $("#inputHora").val();
                objRequest.chr_dia_semana = $("#inputDia").val();
                objRequest.chr_dia_mes = $("#inputDiaMes").val();
                objRequest.chr_mes = $("#inputMes").val();
/*                objRequest.chr_from = $("#inputFrom").val();
                objRequest.chr_to = $("#inputTo").val();*/
                objRequest.chr_subject = $("#inputSubject").val();
                objRequest.txt_body = $("#inputTemplate").val();
                objRequest.chr_email = value;
                objRequest.int_categoryid = $("#inputCategory").val();
                service.procesarPost("testNotificacion", objRequest, printLoadServiceTaskManager);
            });
}

function loadDataTableTaskManager(idElement) {
    $('#' + idElement).DataTable({
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "lengthMenu": [[20, 100, 500, -1], [20, 100, 500, "Todo"]]
    });
}
function removeDataTableTaskManager(idElement) {
    if ($.fn.DataTable.isDataTable('#' + idElement)) {
        $('#' + idElement).DataTable().clear().destroy();
    }
}

function addValidateFormTareas(idForm) {
    switch (idForm) {
        case 'formCrudTaskManager':
            FormValidatorTask[idForm] = $("#" + idForm).validate({
                rules: {
                    inputTitle: "required",
                    inputPeriodo: "required",
                    inputDescription: "required",
                    inputSubject: "required",
                    inputTemplate: "required"
                },
                messages: {
                    inputTitle: "Ingrese un nombre para la alerta",
                    inputPeriodo: "Seleccione la periocidad de la alerta",
                    inputDescription: "Ingrese una descripción de la alerta",
                    inputSubject: "Ingrese el asunto del correo o alerta",
                    inputTemplate: "Ingrese el contenido del correo o alerta"
                },
                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help-block");
                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".divFeedbackCustom").addClass("has-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[ 0 ]) {
                        $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[ 0 ]) {
                        $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).parents(".divFeedbackCustom").addClass("has-error").removeClass("has-success");
                    $(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".divFeedbackCustom").addClass("has-success").removeClass("has-error");
                    $(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
                },
                submitHandler: function () {
                    saveAlerta();
                    return true;
                }
            });
            break;
        case 'formMovimientosPresupuesto':
            FormValidatorTask[idForm] = $("#" + idForm).validate({
                rules: {
                    inputSustento: "required",
                    inputNameTransferencia: "required",
                    inputMontoTotalTransferido: {
                        required: true,
                        number: true,
                        min: 1
                    }
                },
                messages: {
                    inputSustento: "Ingrese un sustento para ejecutar el movimiento",
                    inputNameTransferencia: "Ingrese un título descriptivo de la transferencia",
                    inputMontoTotalTransferido: {
                        required: "Es necesario un monto",
                        number: "Sólo dígitos",
                        min: "Ingresar un monto positivo"
                    }
                },
                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help-block");
                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".divFeedbackCustom").addClass("has-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[ 0 ]) {
                        $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[ 0 ]) {
                        $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).parents(".divFeedbackCustom").addClass("has-error").removeClass("has-success");
                    $(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".divFeedbackCustom").addClass("has-success").removeClass("has-error");
                    $(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
                },
                submitHandler: function () {
                    saveOrdenMovimiento();
                    return true;
                }
            });
            break;
        case 'formModalTransferenciaCuenta':
            jQuery.validator.addMethod("monto_menor_igual", function (value, element) {
                if (parseFloat($("#inputMontoTransferir").val()) <= parseFloat($("#inputSaldoActual").val()))
                    return true;
            }, jQuery.validator.format("El monto debe ser menor o igual al saldo"));

            jQuery.validator.addMethod("yourself", function (value, element) {
                if (
                        $("#inputArea").val() != $("#chr_area_cuenta").val() ||
                        $("#inputString").val() != $("#chr_string_cuenta").val() ||
                        $("#inputCategoria").val() != $("#chr_categoria_cuenta").val() ||
                        $("#inputProyecto").val() != $("#chr_proyecto_cuenta").val() ||
                        $("#inputComentario").val().toString() != $("#chr_comentario_cuenta").val().toString() ||
                        $("#inputMes").val().toString() != $("#chr_mes_cuenta").val().toString()
                        ) {
                    return true;
                }
            }, jQuery.validator.format("No se puede realizar una transferencia en la misma cuenta"));

            FormValidatorTask[idForm] = $("#" + idForm).validate({
                rules: {
                    inputArea: "required",
                    inputString: "required",
                    inputProyecto: "required",
                    inputCategoria: "required",
                    inputComentario: {
                        required: true
                    },
                    inputMes: {
                        required: true,
                        yourself: true
                    },
                    inputMontoTransferir: {
                        required: true,
                        number: true,
                        min: 1,
                        monto_menor_igual: true
                    }
                },
                messages: {
                    inputArea: "Seleccione un área",
                    inputString: "Seleccione un string",
                    inputProyecto: "Seleccione un proyecto",
                    inputCategoria: "Seleccione una categoría",
                    inputComentario: {
                        required: "Seleccione un comentario"
                    },
                    inputMes: {
                        required: "Seleccione un mes",
                        yourself: "No se puede realizar una transferencia en la misma cuenta"
                    },
                    inputMontoTransferir: {
                        required: "Es necesario un monto",
                        number: "Sólo dígitos",
                        min: "Ingresar un monto positivo",
                        monto_menor_igual: "El monto debe ser menor o igual al saldo"
                    }
                },
                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help-block");
                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".divFeedbackCustom").addClass("has-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[ 0 ]) {
                        $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[ 0 ]) {
                        $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).parents(".divFeedbackCustom").addClass("has-error").removeClass("has-success");
                    $(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".divFeedbackCustom").addClass("has-success").removeClass("has-error");
                    $(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
                },
                submitHandler: function () {
                    addCuentaToMovimiento();
                    return true;
                }
            });
            break;
    }
    return FormValidatorTask;
}
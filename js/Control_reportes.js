var service = new Service("webService/index.php");

//var cuestionario = Array;


$(function()
{

iniciarControlPresupuestoSolicitud();

});


function iniciarControlPresupuestoSolicitud(){

    var f = new Date();
    var nmes = (f.getMonth()+1);
    var nanio = f.getFullYear();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;
    
    $("#fechaFormularioSolicitudGastoPresupuesto").val(fecha);

    var arrayDatosFormularioOep = [];

    var usuario = sessionStorage.getItem("usuario");
    var area = "";
    var ingreso = 0.00;
    var gasto = 0.00;
    var registros = "";
    var periodo = 0;

    var meses = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "setiembre", "octubre", "noviembre", "diciembre"];


    cargaConsultasIniciales();


    function cargaConsultasIniciales(){
    	service.procesar("getDatosporUsuario",usuario,cargaAreasporUsuario); //CARGA DE AREAS POR USUARIO
        service.procesar("getColaboradorOepPresupuesto",usuario,cargaColaboradorOepPresupuesto);
        service.procesar("getProveedorPresupuesto",usuario,cargaProveedorPresupuesto);
        service.procesar("getActividadOepPresupuesto",usuario,cargaActividadOepPresupuesto);
        service.procesar("getSedePresupuesto",usuario,cargaSedePresupuesto);
    	//cargarPeriodos();

    }


    function cargaAreasporUsuario(evt){
        resultado = evt[0].area;
        var objeto = new Object()
            objeto.area = resultado;
            objeto.anio = nanio;
        service.procesar("getAreasSolicitud",objeto,cargaAreasSolicitud); //CARGA REGISTROS POR USUARIO
    }

    //CARGA LAS AREAS POR CADA USUARIO QUE INGRESA AL SISTEMA
    function cargaAreasSolicitud(evt){
        resultado = evt;
        $("#areaFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#areaFormularioSolicitudGastoPresupuesto" ).append( "<option value='"+ resultado[i].areaNombre +"'>"+ resultado[i].areaNombre +"</option>" );
        }
    } 

    $("#areaFormularioSolicitudGastoPresupuesto").on('change',function(){
        area = $("select#areaFormularioSolicitudGastoPresupuesto").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.anio = nanio;
        service.procesar("getStringSolicitud",objeto,cargaStringsSolicitud);
    });
    
    function cargaStringsSolicitud(evt){
        resultado = evt;
        $("#stringFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#categoriaFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#proyectoFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#detalleFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#stringFormularioSolicitudGastoPresupuesto" ).append( "<option value='"+ resultado[i].codigo_string +"'>"+ resultado[i].string +"</option>" );
        }
    }


    //CARGAR STRING POR CADA AREA
    $("#stringFormularioSolicitudGastoPresupuesto").on('change',function(){
        console.log("aquis");
        area = $("select#areaFormularioSolicitudGastoPresupuesto").val();
        string = $("select#stringFormularioSolicitudGastoPresupuesto").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.anio = nanio;
        service.procesar("getCategoriasSolicitud",objeto,cargaCategoriasSolicitud);
    });

    function cargaCategoriasSolicitud(evt){
        resultado = evt;
        $("#categoriaFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#proyectoFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#detalleFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#categoriaFormularioSolicitudGastoPresupuesto" ).append( "<option value='"+ resultado[i].cuenta_categoria +"'>"+ resultado[i].categoria +"</option>" );
        }
    }


    $("#categoriaFormularioSolicitudGastoPresupuesto").on('change',function(){
        area = $("select#areaFormularioSolicitudGastoPresupuesto").val();
        string = $("select#stringFormularioSolicitudGastoPresupuesto").val();
        categoria = $("select#categoriaFormularioSolicitudGastoPresupuesto").val();
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.anio = nanio;
        service.procesar("getProyectosSolicitud",objeto,cargaProyectosSolicitud);

        if(categoria == "OEP"){
            $("#proveedorFormularioSolicitudGastoPresupuesto").hide();
            $("#cargarPagoOepSolicitud").show();
            $("#nuevoProveedorPresupuesto").hide();
        }else{
            $("#proveedorFormularioSolicitudGastoPresupuesto").show();
            $("#cargarPagoOepSolicitud").hide();
            $("#nuevoProveedorPresupuesto").show();
        }

    });

    function cargaProyectosSolicitud(evt){
        resultado = evt;
        $("#proyectoFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        $("#actividadFormularioSolicitudGastoPresupuesto").val("");
        $("#detalleFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#proyectoFormularioSolicitudGastoPresupuesto" ).append( "<option value='"+ resultado[i].proyecto +"'>"+ resultado[i].proyecto +"</option>" );
        }
    }

    $("#proyectoFormularioSolicitudGastoPresupuesto").on('change',function(){
        area = $("select#areaFormularioSolicitudGastoPresupuesto").val();
        string = $("select#stringFormularioSolicitudGastoPresupuesto").val();
        categoria = $("select#categoriaFormularioSolicitudGastoPresupuesto").val();
        proyecto = $("select#proyectoFormularioSolicitudGastoPresupuesto").val();
        $("#actividadFormularioSolicitudGastoPresupuesto").val(proyecto);
        var objeto = new Object()
            objeto.area = area;
            objeto.string = string;
            objeto.categoria = categoria;
            objeto.proyecto = proyecto;
            objeto.anio = nanio;
        service.procesar("getDetallesSolicitud",objeto,cargaDetallesSolicitud);
    });

    function cargaDetallesSolicitud(evt){
        resultado = evt;
        $("#detalleFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#detalleFormularioSolicitudGastoPresupuesto" ).append( "<option value='"+ resultado[i].comentarios +"'>"+ resultado[i].comentarios +"</option>" );
        }
    }

    function cargaProveedorPresupuesto(evt){
        proveedor = evt;
        $("#proveedorFormularioSolicitudGastoPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        for(var i=0; i<proveedor.length ; i++){
            $("#proveedorFormularioSolicitudGastoPresupuesto" ).append( "<option value='"+ proveedor[i].dni_ruc +"'>"+ proveedor[i].nombre_razon_social +"</option>" );
        }
        cargaListaProveedorPresupuesto(evt);
    }

    function cargaListaProveedorPresupuesto(evt){
        listadeProveedores = evt;

        $("#tablaListaProveedorPresupuesto tbody").html("");

        if ( listadeProveedores == undefined ) return;

        for(var i=0; i<listadeProveedores.length ; i++){

            var fila = $("<tr>");
            var celdaBotones = $("<td>");
            datoRegistro = listadeProveedores[i];
            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeProveedores[i].dni_ruc +'</td><td>'+ listadeProveedores[i].nombre_razon_social +'</td>');

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-primary btn-xs' title='Editar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            btnSeleccionar.data("data",datoRegistro);
            btnSeleccionar.on("click",editarProveedorPresupuesto);

            var btnEliminar = $("<button class='btn btn-danger btn-xs' title='Eliminar'>");
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span');
            btnEliminar.data("data",datoRegistro);
            btnEliminar.on("click",eliminarProveedorPresupuesto);

            contenedorBotones.append(btnSeleccionar);
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#tablaListaProveedorPresupuesto tbody").append(fila);

        }

    }






    function editarProveedorPresupuesto(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE MODIFICAR DATOS DEL PROVEEDOR : " + data.nombre_razon_social +" ?", function (e) {
            if (e) {
                $("#rucProveedorPresupuesto").val(data.dni_ruc);
                $("#empresaProveedorPresupuesto").val(data.nombre_razon_social);
                $("#procesoProveedorPresupuesto").val("1");
                $("#codigoProveedorPresupuesto").val(data.idProveedor);
                
                $("#botonGuardarProveedorPresupuesto").html("MODIFICAR");

                alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
            }
        });
    }

    function eliminarProveedorPresupuesto(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR DATOS DEL PROVEEDOR : " + data.nombre_razon_social +" ?", function (e) {
            if (e) {
                service.procesar("deleteProveedorPresupuesto",data.idProveedor,resultadoDeleteProveedorPresupuesto);
                alertify.success("HA ACEPTADO ELIMINAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });
    }

    function resultadoDeleteProveedorPresupuesto(evt){
        service.procesar("getProveedorPresupuesto",usuario,cargaProveedorPresupuesto);
    }

    $("#botonGuardarProveedorPresupuesto").on('click', function(){
        var ruc = $("#rucProveedorPresupuesto").val();
        var empresa = $("#empresaProveedorPresupuesto").val();
        alertify.confirm("¿ SEGURO DE GUARDAR DATOS DEL PROVEEDOR : " + empresa +" ?", function (e) {
            if (e) {
                if (ruc == "" || empresa == ""){
                    alertify.error("EXISTEN CAMPOS VACIOS OBLIGATORIOS");
                }else{
                    var objeto = new Object()
                        objeto.dni = ruc;
                        objeto.nombre = empresa;
                        objeto.proceso = $("#procesoProveedorPresupuesto").val();
                        objeto.codigo = $("#codigoProveedorPresupuesto").val();
                    service.procesar("saveProveedorPresupuesto",objeto,resultadoSaveProveedorPresupuesto); //CARGA REGISTROS POR USUARIO
                }
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE GUARDADO");
            }
        });
    })

    $("#botonCancelarProveedorPresupuesto").on('click', function(){
        alertify.confirm("¿ SEGURO DE CANCELAR EL PROCESO", function (e) {
            if (e) {
                formularioProveedorEstandar();
                alertify.success("HA ACEPTADO CANCELAR EL REGISTRO");
            } else {
                //alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
            }
        });

    })

    function resultadoSaveProveedorPresupuesto(evt){
        if(evt == 1 ){
            alertify.success("PROVEEDOR GUARDADO SATISFACTORIAMENTE");
            service.procesar("getProveedorPresupuesto",usuario,cargaProveedorPresupuesto);
            formularioProveedorEstandar();
        }else if(evt == 2 ){
            alertify.success("PROVEEDOR MODIFICADO SATISFACTORIAMENTE");
            service.procesar("getProveedorPresupuesto",usuario,cargaProveedorPresupuesto);
            formularioProveedorEstandar();
        }else{
            alertify.error("PROVEEDOR NO GUARDADO"); 
        }
    }

    function formularioProveedorEstandar(){
        $("#rucProveedorPresupuesto").val("");
        $("#empresaProveedorPresupuesto").val("");
        $("#procesoProveedorPresupuesto").val("0");
        $("#botonGuardarProveedorPresupuesto").html("GUARDAR");
    }


    $("#botonGuardarFormularioSolicitud").on('click', function(){
        var estado = $("select#estadoFormularioSolicitudGastoPresupuesto").val();
        var fecha  = $("#fechaFormularioSolicitudGastoPresupuesto").val();
        var area = $("select#areaFormularioSolicitudGastoPresupuesto").val();
        var string = $("select#stringFormularioSolicitudGastoPresupuesto").val();
        var categoria = $("select#categoriaFormularioSolicitudGastoPresupuesto").val();
        var proyecto = $("select#proyectoFormularioSolicitudGastoPresupuesto").val();
        var detalle = $("select#detalleFormularioSolicitudGastoPresupuesto").val();

        var proveedor = $("select#proveedorFormularioSolicitudGastoPresupuesto").val();
        var colaboradores = arrayDatosFormularioOep; //SI EXISTE COLABORADORES EN EL GASTO

        var actividad = $("#actividadFormularioSolicitudGastoPresupuesto").val();
        var descripcion = $("#descripcionFormularioSolicitudGastoPresupuesto").val();
        var monto = $("#montoFormularioSolicitudGastoPresupuesto").val();
        var moneda = $("select#seleccionarMonedaFormularioSolicitudGastoPresupuesto").val();
        var tipocambio  = $("#tipoCambioFormularioSolicitudGastoPresupuesto").val();
        var total = $("#totalFormularioSolicitudGastoPresupuesto").val();

        /*console.log(estado);
        console.log(fecha);
        console.log(area);
        console.log(string);
        console.log(categoria);
        console.log(proyecto);
        console.log(detalle);
        console.log(proveedor);
        console.log(colaboradores);
        console.log(actividad);
        console.log(descripcion);
        console.log(monto);
        console.log(moneda);
        console.log(tipocambio);
        console.log(total);*/

        var objetoFormulario = new Object()
            objetoFormulario.estado = estado;
            objetoFormulario.fecha = fecha;
            objetoFormulario.area = area;
            objetoFormulario.string = string;
            objetoFormulario.categoria = categoria;
            objetoFormulario.proyecto = proyecto;
            objetoFormulario.detalle = detalle;
            objetoFormulario.proveedor = proveedor;
            objetoFormulario.colaboradores = colaboradores;
            objetoFormulario.actividad = actividad;
            objetoFormulario.descripcion = descripcion;
            objetoFormulario.monto = monto;
            objetoFormulario.moneda = moneda;
            objetoFormulario.tipocambio = tipocambio;
            objetoFormulario.total = total;

        service.procesar("saveFormularioSolicitud",objetoFormulario,cargaFormularioSolicitud);

    })


    function cargaFormularioSolicitud(evt){
        console.log(evt);
    }

    $("#seleccionarMonedaFormularioSolicitudGastoPresupuesto").on('change',function(){
        dato = $("select#seleccionarMonedaFormularioSolicitudGastoPresupuesto").val();
        cambioMoneda(dato);
    });

    function cambioMoneda(dato){
        if (dato == "DOLARES"){
            $(".tipocambio").show();
        }else{
            $(".tipocambio").hide();
        }
    }

    $("#recalcularFormularioSolicitudGastoPresupuesto").click(function(){
        monto =  parseFloat($("#montoFormularioSolicitudGastoPresupuesto").val());
        tipocambio =  parseFloat($("#tipoCambioFormularioSolicitudGastoPresupuesto").val());
        total = monto * tipocambio;
        $("#totalFormularioSolicitudGastoPresupuesto").val(total);
    })





    //AREA DE FORMULARIO OEP DE COLABORADORES

    function cargaColaboradorOepPresupuesto(evt){
        colaborador_registros = evt;
        $("#nombreOepPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        for(var i=0; i<colaborador_registros.length ; i++){
            $("#nombreOepPresupuesto" ).append( "<option value='"+ colaborador_registros[i].dni +"'>"+ colaborador_registros[i].nombre +"</option>" );
        }

        cargarListaColaboradoresOepPresupuesto(evt);
    }

    function cargaActividadOepPresupuesto(evt){
        activdad_registros = evt;
        $("#actividadOepPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        for(var i=0; i<activdad_registros.length ; i++){
            $("#actividadOepPresupuesto" ).append( "<option value='"+ activdad_registros[i].idActividad +"'>"+ activdad_registros[i].actividad +"</option>" );
        }

        cargarListaActividadesOepPresupuesto(evt);
    }

    function cargaSedePresupuesto(evt){
        sede = evt;
        $("#sedeOepPresupuesto").html("<option value='0'>---SELECCIONAR---</option>");
        for(var i=0; i<sede.length ; i++){
            $("#sedeOepPresupuesto" ).append( "<option value='"+ sede[i].codigo +"'>"+ sede[i].sede +"</option>" );
        }
    }

    $("#guardarPagoOepSolicitud").on('click', function(){
        alertify.confirm("¿ SEGURO DE GUARDAR EL REGISTRO", function (e) {
            if (e) {
                var dni = $("select#nombreOepPresupuesto").val();
                var nombre = $("select#nombreOepPresupuesto option:selected").text();
                var monto = $("#montoOepPresupuesto").val();
                var idActividad = $("select#actividadOepPresupuesto").val();
                var actividad = $("select#actividadOepPresupuesto option:selected").text();
                var idSede = $("select#sedeOepPresupuesto").val();
                var sede = $("select#sedeOepPresupuesto option:selected").text();
                var observaciones = $("#observacionesOepPresupuesto").val();
                var posicion = $("#posicionOepPresupuesto").val();

                    if (dni == "0" || nombre == "---SELECCIONAR---" || monto == "" || actividad == "0" || sede == "0"){
                        alertify.error("EXISTEN CAMPOS VACIOS OBLIGATORIOS");
                    }else{
                        if($("#guardarPagoOepSolicitud").html() == "REGISTRAR"){
                            arrayDatosFormularioOep.push({dni:dni,nombre:nombre,monto:monto,idActividad:idActividad,actividad:actividad,idSede:idSede,sede:sede,observaciones:observaciones});
                            cargaRegistrosFormularioOep(arrayDatosFormularioOep);

                            alertify.success("SE HA GUARDADO EL REGISTRO SATISFACTORIAMENTE");
                            formularioRegistroEstandar(); 
                        }else if($("#guardarPagoOepSolicitud").html() == "MODIFICAR"){
                            arrayDatosFormularioOep[posicion] = ({dni:dni,nombre:nombre,monto:monto,idActividad:idActividad,actividad:actividad,idSede:idSede,sede:sede,observaciones:observaciones});
                            cargaRegistrosFormularioOep(arrayDatosFormularioOep);

                            alertify.success("SE HA MODIFICADO EL REGISTRO SATISFACTORIAMENTE");
                            formularioRegistroEstandar();    
                        }
                         
                    }

            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    })

    $("#cancelarPagoOepSolicitud").on('click', function(){
        alertify.confirm("¿ SEGURO DE CANCELAR Y LIMPIAR EL FORMULARIO ?", function (e) {
            if (e) {
                formularioRegistroEstandar()
                alertify.success("HA CANCELADO EL PROCESO DE NUEVO REGISTRO OEP");
            } else {
                //alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    })


    //function guardarRegistroOEP(){
    //}


    function cargaRegistrosFormularioOep(evt){
        listadeRegistros = evt;
        var total = 0.00;
        $("#tablaListaPagoOep tbody").html("");

        if ( listadeRegistros == undefined ) return;

        for(var i=0; i<listadeRegistros.length ; i++){
        
            var fila = $("<tr>");
            var celdaBotones = $("<td>");
            datoRegistro = listadeRegistros[i];
            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeRegistros[i].dni +'</td><td>'+ listadeRegistros[i].nombre +'</td><td>'+ listadeRegistros[i].monto +'</td><td>'+ listadeRegistros[i].actividad +'</td><td>'+ listadeRegistros[i].sede +'</td><td>'+ listadeRegistros[i].observaciones +'</td>'); //<td>'+ estadoLetra +'</td>

            total = total + parseFloat(listadeRegistros[i].monto);

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-primary btn-xs' title='Editar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            btnSeleccionar.data("data",i);
            btnSeleccionar.on("click",editarRegistroPagoOep);

            var btnEliminar = $("<button class='btn btn-danger btn-xs' title='Eliminar'>");
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span');
            btnEliminar.data("data",i);
            btnEliminar.on("click",eliminarRegistroPagoOep);

            contenedorBotones.append(btnSeleccionar);
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#tablaListaPagoOep tbody").append(fila);

        }

        $("#tablaListaPagoOep tbody").append('<tr class="semaforo_final"><td></td><td></td><td class="negrita centrado">TOTAL</td><td class="negrita">'+ total +'</td><td></td><td></td><td></td><td></td></tr>');
        $("#montoFormularioSolicitudGastoPresupuesto").val(total);

    }

    function editarRegistroPagoOep(){
        var posicion = $(this).data();
        alertify.confirm("¿ SEGURO DE MODIFICAR EL REGISTRO", function (e) {
            if (e) {
                var registro = arrayDatosFormularioOep[posicion.data];

                $("#nombreOepPresupuesto").val(registro.dni);
                $("#montoOepPresupuesto").val(registro.monto);
                $("#actividadOepPresupuesto").val(registro.idActividad);
                $("#sedeOepPresupuesto").val(registro.idSede);
                $("#observacionesOepPresupuesto").val(registro.observaciones);
                $("#posicionOepPresupuesto").val(posicion.data);

                $("#guardarPagoOepSolicitud").html("MODIFICAR");

                alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    }

    function eliminarRegistroPagoOep(){
        var posicion = $(this).data();
        alertify.confirm("¿ SEGURO DE ELIMINAR EL REGISTRO", function (e) {
            if (e) {
                arrayDatosFormularioOep.splice(posicion.data, 1)
                registros = arrayDatosFormularioOep.length;
                alertify.success("SE HA ELIMINADO EL REGISTRO SATISFACTORIAMENTE");
                cargaRegistrosFormularioOep(arrayDatosFormularioOep);
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    }

    function formularioRegistroEstandar(){
       $("select#nombreOepPresupuesto").val("0");
       $("#montoOepPresupuesto").val("");
       $("select#actividadOepPresupuesto").val("0");
       $("select#sedeOepPresupuesto").val("0");
       $("#observacionesOepPresupuesto").val("");
       $("#guardarPagoOepSolicitud").html("REGISTRAR");
    }


    //FINAL FORMULARIO OEP COLABORADORES


    //INICIO AREA DE FORMULARIO DE COLABORADORES
    function cargarListaColaboradoresOepPresupuesto(evt){
        listadeColaboradores = evt;
        var estadoLetra = "";

        $("#tablaListaColaboradoresPagoOep tbody").html("");

        if ( listadeColaboradores == undefined ) return;

        for(var i=0; i<listadeColaboradores.length ; i++){

            if(listadeColaboradores[i].estado == "1"){
                var fila = $("<tr>");
                estadoLetra = "ACTIVADO";
            }else if(listadeColaboradores[i].estado == "2"){
                var fila = $("<tr class='semaforo_rojo'>");
                estadoLetra = "SUSPENDIDO";
            }

            
            var celdaBotones = $("<td>");
            datoRegistro = listadeColaboradores[i];
            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeColaboradores[i].dni +'</td><td>'+ listadeColaboradores[i].nombre +'</td>'); //<td>'+ estadoLetra +'</td>

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-primary btn-xs' title='Editar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            btnSeleccionar.data("data",datoRegistro);
            btnSeleccionar.on("click",editarColaboradoresPagoOep);

            var btnEliminar = $("<button class='btn btn-danger btn-xs' title='Eliminar'>");
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span');
            btnEliminar.data("data",datoRegistro);
            btnEliminar.on("click",eliminarColaboradoresPagoOep);

            contenedorBotones.append(btnSeleccionar);
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#tablaListaColaboradoresPagoOep tbody").append(fila);

        }
    }

    function editarColaboradoresPagoOep(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE MODIFICAR DATOS DEL COLABORADOR : " + data.nombre +" ?", function (e) {
            if (e) {
                $("#dniColaboradoresPagoOep").val(data.dni);
                $("#nombreColaboradoresPagoOep").val(data.nombre);
                $("select#estadoColaboradoresPagoOep").val(data.estado);
                $("#procesoColaboradoresPagoOep").val("1");
                $("#codigoColaboradoresPagoOep").val(data.idColaborador);
                
                $("#botonGuardarColaboradoresPagoOep").html("MODIFICAR");

                alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
            }
        });
    }

    function eliminarColaboradoresPagoOep(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR DATOS DEL COLABORADOR : " + data.nombre +" ?", function (e) {
            if (e) {
                service.procesar("deleteColaboradorOepPresupuesto",data.idColaborador,resultadoDeleteColaboradorOepPresupuesto);
                alertify.success("HA ACEPTADO ELIMINAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });
    }

    function resultadoDeleteColaboradorOepPresupuesto(evt){
        service.procesar("getColaboradorOepPresupuesto",usuario,cargaColaboradorOepPresupuesto);
    }

    $("#botonGuardarColaboradoresPagoOep").on('click', function(){
        var dni = $("#dniColaboradoresPagoOep").val();
        var nombre = $("#nombreColaboradoresPagoOep").val();
        alertify.confirm("¿ SEGURO DE GUARDAR LA ACTIVIDAD : " + nombre +" ?", function (e) {
            if (e) {
                if (dni == "" || nombre == ""){
                    alertify.error("EXISTEN CAMPOS VACIOS OBLIGATORIOS");
                }else{
                    var objeto = new Object()
                        objeto.dni = dni;
                        objeto.nombre = nombre;
                        objeto.estado = $("select#estadoColaboradoresPagoOep").val();
                        objeto.proceso = $("#procesoColaboradoresPagoOep").val();
                        objeto.codigo = $("#codigoColaboradoresPagoOep").val();
                    service.procesar("saveColaboradorOepPresupuesto",objeto,resultadoSaveColaboradorOepPresupuesto);
                }
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
            }
        });
    })

    $("#botonCancelarColaboradoresPagoOep").on('click', function(){
        alertify.confirm("¿ SEGURO DE CANCELAR EL PROCESO", function (e) {
            if (e) {
                formularioColaboradorEstandar();
                alertify.success("HA ACEPTADO CANCELAR EL REGISTRO");
            } else {
                //alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
            }
        });

    })

    function resultadoSaveColaboradorOepPresupuesto(evt){
        if(evt == 1 ){
            alertify.success("COLABORADOR GUARDADO SATISFACTORIAMENTE");
            service.procesar("getColaboradorOepPresupuesto",usuario,cargaColaboradorOepPresupuesto);
            formularioColaboradorEstandar();
        }else if(evt == 2 ){
            alertify.success("COLABORADOR MODIFICADO SATISFACTORIAMENTE");
            service.procesar("getColaboradorOepPresupuesto",usuario,cargaColaboradorOepPresupuesto);
            formularioColaboradorEstandar();
        }else{
            alertify.error("COLABORADOR NO GUARDADO"); 
        }
    }

    function formularioColaboradorEstandar(){
        $("#dniColaboradoresPagoOep").val("");
        $("#nombreColaboradoresPagoOep").val("");
        $("select#estadoColaboradoresPagoOep").val(1);
        $("#procesoColaboradoresPagoOep").val("0");
        $("#botonGuardarColaboradoresPagoOep").html("GUARDAR");
    }

    //FINAL AREA DE COLABORADORES


    //INICIO AREA DE ACTIVIDADES

    function cargarListaActividadesOepPresupuesto(evt){
        listadeActividades = evt;
        var estadoLetra = "";

        $("#tablaListaActividadPagoOep tbody").html("");

        if ( listadeActividades == undefined ) return;

        for(var i=0; i<listadeActividades.length ; i++){

            if(listadeActividades[i].estado == "1"){
                var fila = $("<tr>");
                estadoLetra = "ACTIVADO";
            }else if(listadeActividades[i].estado == "2"){
                var fila = $("<tr class='semaforo_rojo'>");
                estadoLetra = "SUSPENDIDO";
            }

            
            var celdaBotones = $("<td>");
            datoRegistro = listadeActividades[i];
            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeActividades[i].actividad +'</td>'); //<td>'+ estadoLetra +'</td>

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-primary btn-xs' title='Editar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            btnSeleccionar.data("data",datoRegistro);
            btnSeleccionar.on("click",editarActividadPagoOep);

            var btnEliminar = $("<button class='btn btn-danger btn-xs' title='Eliminar'>");
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span');
            btnEliminar.data("data",datoRegistro);
            btnEliminar.on("click",eliminarActividadPagoOep);

            contenedorBotones.append(btnSeleccionar);
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#tablaListaActividadPagoOep tbody").append(fila);

        }
    }

    function editarActividadPagoOep(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE MODIFICAR DATOS DE LA ACTIVIDAD : " + data.actividad +" ?", function (e) {
            if (e) {
                $("#nombreActividadPagoOep").val(data.actividad);
                $("#procesoActividadPagoOep").val("1");
                $("#codigoActividadPagoOep").val(data.idActividad);
                
                $("#botonGuardarActividadPagoOep").html("MODIFICAR");

                alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
            }
        });
    }

    function eliminarActividadPagoOep(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR DATOS DE LA : " + data.actividad +" ?", function (e) {
            if (e) {
                service.procesar("deleteActividadOepPresupuesto",data.idActividad,resultadoDeleteActividadOepPresupuesto);
                alertify.success("HA ACEPTADO ELIMINAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });
    }

    function resultadoDeleteActividadOepPresupuesto(evt){
        service.procesar("getActividadOepPresupuesto",usuario,cargaActividadOepPresupuesto);
    }

    $("#botonGuardarActividadPagoOep").on('click', function(){
        var actividad = $("#nombreActividadPagoOep").val();
        alertify.confirm("¿ SEGURO DE GUARDAR LA ACTIVIDAD : " + actividad +" ?", function (e) {
            if (e) {
                if (actividad == ""){
                    alertify.error("EXISTEN CAMPOS VACIOS OBLIGATORIOS");
                }else{
                    var objeto = new Object()
                        objeto.actividad = actividad;
                        objeto.proceso = $("#procesoActividadPagoOep").val();
                        objeto.codigo = $("#codigoActividadPagoOep").val();
                    service.procesar("saveActividadOepPresupuesto",objeto,resultadoSaveActividadOepPresupuesto);
                }
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE GUARDADO");
            }
        });
    })

    $("#botonCancelarActividadPagoOep").on('click', function(){
        alertify.confirm("¿ SEGURO DE CANCELAR EL PROCESO", function (e) {
            if (e) {
                formularioActividadEstandar();
                alertify.success("HA ACEPTADO CANCELAR EL REGISTRO");
            } else {
                //alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
            }
        });

    })

    function resultadoSaveActividadOepPresupuesto(evt){
        if(evt == 1 ){
            alertify.success("ACTIVIDAD GUARDADA SATISFACTORIAMENTE");
            service.procesar("getActividadOepPresupuesto",usuario,cargaActividadOepPresupuesto);
            formularioActividadEstandar();
        }else if(evt == 2 ){
            alertify.success("ACTIVIDAD MODIFICADA SATISFACTORIAMENTE");
            service.procesar("getActividadOepPresupuesto",usuario,cargaActividadOepPresupuesto);
            formularioActividadEstandar();
        }else{
            alertify.error("ACTIVIDAD NO GUARDADA"); 
        }
    }

    function formularioActividadEstandar(){
        $("#nombreActividadPagoOep").val("");
        $("#procesoActividadPagoOep").val("0");
        $("#botonGuardarActividadPagoOep").html("GUARDAR");
    }

    //FINAL AREA DE ACTIVIDADES



















	function distinct(array,indice){
	  var uniques = []; //temporal
	  return array.filter(function(item){
	    //indexOf buscará el valor, si no existe retornará true, por lo cual se mantendrá en el arreglo 
	    //false en caso el valor ya exista en la variable uniques
	    return uniques.indexOf(item[indice]) < 0 ? uniques.push(item[indice]) : false
	  })
	}

	function sinduplicados(arr) {
		var i,
	    	len=arr.length,
	    	out=[],
	    	obj={};

		for (i=0;i<len;i++) {
			obj[arr[i]]=0;
		}
		for (i in obj) {
			out.push(i);
		}
		return out;
	}

    var formatNumber = {
     separador: ",", // separador para los miles
     sepDecimal: '.', // separador para los decimales
     formatear:function (num){
      num +='';
      var splitStr = num.split(',');
      var splitLeft = splitStr[0];
      var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
      var regx = /(\d+)(\d{3})/;
      while (regx.test(splitLeft)) {
      splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
      }
      return this.simbol + splitLeft  +splitRight;
     },
     new:function(num, simbol){
      this.simbol = simbol ||'';
      return this.formatear(num);
     }
    }

    function exceltojson(e) {
        var files = e.target.files;
        var f = files[0];
        var X = XLSX;
        {
            var reader = new FileReader();
            //var name = f.name;
            reader.onload = function(e) {
                var data = e.target.result;
                wb = X.read(data, {type: 'binary'});
                var roa = "";
                wb.SheetNames.forEach(function(sheetName) {
                    roa = X.utils.sheet_to_json(wb.Sheets[sheetName]);
                });
                jsonExcel = roa;
                cargaPagoOep(jsonExcel);
            };
            reader.readAsBinaryString(f);
        }
    }

    function removeItemArray ( arr, item ) {
        var i = arr.indexOf( item );
        arr.splice( i, 1 );
    }

    function getLista (array,atributo){
        var nuevoArray = []
        for ( i = 0; i < array.length; i++) {
            valor = array[i][atributo];
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }

    function buscarProcedencia(valor,base){
      for(var i = 0; i < base.length; i++){
        if(base[i].procedencia == valor){
          return base[i].idProcedencia;
        }

      }
    }

    function buscarIdProcedencia(valor,base){
      for(var i = 0; i < base.length; i++){
        if(base[i].procedencia == valor){
          return base[i].idProcedencia;
        }

      }
    }

    function uniqueArray(arr){
        let unique_array = []
        for(let i = 0;i < arr.length; i++){
            if(unique_array.indexOf(arr[i]) == -1){
                unique_array.push(arr[i])
            }
        }
        return unique_array
    }





}

var service = new Service("webService/index.php");

//var cuestionario = Array;


$(function()
{

iniciarControlPresupuesto();

});


function iniciarControlPresupuesto(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var usuario = sessionStorage.getItem("usuario");
    var ultimoregistro = "";
    //$('#fechaRegistro').val(fecha);
    //$('#fechaRegistroDesde').val(f.getFullYear() +"-"+ mes +"-01");
    $('#fechaRegistroGastoPresupuesto').val(fecha);

    $("#areaRegistroGastoPresupuesto").hide();

    $(".tipocambio").hide();
    $(".motivo").hide();

    listarRegistrosGastoPresupuesto();





























    $("#botonBuscarStringPresupuesto").on("click",function(){
        stringBuscada = $("#campoBuscarStringPresupuesto").val();
        service.procesar("getListaStringEncontradas",stringBuscada,cargaListaStringCoincidencias);
    })

    function cargaListaStringCoincidencias(evt){
        listadeRegistrosdePresupuesto = evt;

        $("#tablaListaStringEncontradaPresupuesto tbody").html("");

        if ( listadeRegistrosdePresupuesto == undefined ) return;

        for(var i=0; i<listadeRegistrosdePresupuesto.length ; i++){
            datoRegistro = listadeRegistrosdePresupuesto[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");

            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeRegistrosdePresupuesto[i].string_sin_PER +'</td><td>'+ listadeRegistrosdePresupuesto[i].centro_de_costo +'</td>');

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-success btn-xs' title='Seleccionar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-ok"></span');

            btnSeleccionar.data("data",datoRegistro);

            btnSeleccionar.on("click",seleccionarRegistroString);

            contenedorBotones.append(btnSeleccionar);

            fila.append(contenedorBotones);

            $("#tablaListaStringEncontradaPresupuesto tbody").append(fila);

        }
    }

    function seleccionarRegistroString(){
        var data = $(this).data("data");
        $(".descStringPresupuesto").css("display", "");
        $("#buscarStringPresupuesto").val(data.string_sin_PER);
        $("#descripcionStringPresupuesto").html(data.centro_de_costo);
        $('#modalBuscarStringPresupuesto').modal("toggle");
    }

    $("#buscarSaldoPresupuesto").on("click",function(){
        listarRegistrosGastoPresupuesto();
    })

    function listarRegistrosGastoPresupuesto(){
            var objDatosdeObjeto = new Object()
            objDatosdeObjeto.string = $("#direccionPresupuesto").val();
            objDatosdeObjeto.scategoria = $("#subCategoriaPresupuesto").val();
            objDatosdeObjeto.area = $("#subAreaPresupuesto").val();
            objDatosdeObjeto.nodo = $("#cuentaNodoPresupuesto").val();
            objDatosdeObjeto.proveedor = $("#proveedorPresupuesto").val();
            objDatosdeObjeto.texto = $("#buscarPresupuesto").val();
            objDatosdeObjeto.estado = $("select#seleccionarEstadoPresupuestoBusqueda").val();
            objDatosdeObjeto.q1 = $("#q1Presupuesto:checked")["length"];
            objDatosdeObjeto.q2 = $("#q2Presupuesto:checked")["length"];
            objDatosdeObjeto.q3 = $("#q3Presupuesto:checked")["length"];
            objDatosdeObjeto.q4 = $("#q4Presupuesto:checked")["length"];
            service.procesar("getSaldoStringNodo",objDatosdeObjeto,cargaSaldoStringNodo);
    }



    var formatNumber = {
     separador: ",", // separador para los miles
     sepDecimal: '.', // separador para los decimales
     formatear:function (num){
      num +='';
      var splitStr = num.split(',');
      var splitLeft = splitStr[0];
      var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
      var regx = /(\d+)(\d{3})/;
      while (regx.test(splitLeft)) {
      splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
      }
      return this.simbol + splitLeft  +splitRight;
     },
     new:function(num, simbol){
      this.simbol = simbol ||'';
      return this.formatear(num);
     }
    }

    function cargaSaldoStringNodo(evt){
        data = evt.saldo;

        $("#tablaListadeGastosdePresupuesto tbody").html("");
        $("#tablaListadeGastosdePresupuesto tfoot").html("");

        if ( evt.gasto == undefined ) return;
        cargaGastoStringNodo(evt.gasto);
    }


    function cargaGastoStringNodo(evt){
        resultado = evt;
        var total = 0.00;
        var subtotal = 0.00;

        $("#tablaListadeGastosdePresupuesto tbody").html("");
        $("#tablaListadeGastosdePresupuesto tfoot").html("");

        if ( resultado == undefined ) return;
            ultimoregistro = parseFloat(resultado[0].idPresupuesto) + 1;
            $("#numeroAutogeneradoGastoPresupuesto").val(ultimoregistro);
        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");
            total = total + parseFloat(resultado[i].total);
            subtotal = parseFloat(resultado[i].total);
            fila.html('<td>'+ (i + 1) +'</td><td>'+ resultado[i].codigo_string +'</td><td>'+ resultado[i].string +'</td><td>'+ resultado[i].cuenta_nodo +'</td><td>'+ resultado[i].orden +'</td><td>'+ resultado[i].subarea +'</td><td>'+ resultado[i].item +'</td><td>'+ resultado[i].clave +'</td><td>'+ formatNumber.new(subtotal.toFixed(2), "") +'</td><td>'+ resultado[i].fecha +'</td><td>'+ resultado[i].proveedor +'</td><td>'+ resultado[i].estado +'</td><td>'+ resultado[i].motivo +'</td>'); //<td>'+ resultado[i].descripcion +'</td>

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-primary btn-xs' title='Modificar'>");
            //var btnHistorico = $("<button class='btn btn-info btn-xs' title='Historico'>");
            var btnEliminar = $("<button class='btn btn-danger btn-xs' title='Eliminar'>");

            btnSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            //btnHistorico.html('<span class="glyphicon glyphicon-list-alt"></span');
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span');

            btnSeleccionar.data("data",datoRegistro);
            //btnHistorico.data("data",datoRegistro);
            btnEliminar.data("data",datoRegistro);

            btnSeleccionar.on("click",seleccionarRegistroGastoPresupuesto);
            //btnHistorico.on("click",historicoRegistroGastoPresupuesto);
            btnEliminar.on("click",eliminarRegistroGastoPresupuesto);

            contenedorBotones.append(btnSeleccionar);
            //contenedorBotones.append(btnHistorico);
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#tablaListadeGastosdePresupuesto tbody").append(fila);

        }
            $("#tablaListadeGastosdePresupuesto tfoot").html("<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td class='negrita'>TOTAL</td><td class=''>"+formatNumber.new(total.toFixed(2), "")+"</td><td></td><td></td><td></td><td></td><td></td></tr>");


    }

    $("#botonNuevoGastoPresupuesto").click(function(){
        $("#procedimientoDatosGastoPresupuesto").html("GUARDAR");
        $("#mensajeFormularioGastoPresupuesto").html("FORMULARIO INGRESO DE NUEVO REGISTRO");
        limpiaCamposGastoPresupuesto();


        $("#areaRegistroGastoPresupuesto").show("slow");
        $("#numeroOrdenGastoPresupuesto").focus();
    })

    function historicoRegistroGastoPresupuesto(){
        var data = $(this).data("data");
        console.log(data);
    }

    function seleccionarRegistroGastoPresupuesto(){
        var data = $(this).data("data");

        alertify.confirm("¿ SEGURO DE MODIFICAR EL REGISTRO : " + data.orden +" ?", function (e) {
                if (e) {

                    limpiaCamposGastoPresupuesto();
                    activaTab('nuevoIngresoGastoPresupuesto');

                    if(data.estado != "INGRESADO"){
                        $(".motivo").show();
                    }

                    $("#procedimientoDatosGastoPresupuesto").html("MODIFICAR");
                    $("#mensajeFormularioGastoPresupuesto").html("FORMULARIO MODIFICACION DE REGISTRO");

                    $("#datoDireccionPresupuesto" ).val(data.string);
                    $("#numeroAutogeneradoGastoPresupuesto").val(data.idPresupuesto);
                    $("#datoSubCategoriaPresupuesto" ).val(data.nscategoria);
                    $("#datoCuentaNodoPresupuesto" ).val(data.cuenta_nodo);

                    $("#idPresupuestoGastoPresupuesto").val(data.idPresupuesto);
                    $("#numeroOrdenGastoPresupuesto" ).val(data.orden);
                    $("#montoGastoPresupuesto" ).val(data.monto);
                    $("#seleccionarMonedaGatoPresupuesto" ).val(data.moneda);

                    cambioMoneda(data.moneda)

                    $("#tipoCambioGastoPresupuesto" ).val(data.tipocambio);
                    $("#totalGastoPresupuesto" ).val(data.total);
                    $("#fechaRegistroGastoPresupuesto" ).val(data.fecha);
                    $("#proveedorGastoPresupuesto" ).val(data.proveedor);
                    $("#descripcionGastoPresupuesto" ).val(data.descripcion);
                    $("#palablaClavePresupuesto" ).val(data.clave);
                    $("#datoSubAreaPresupuesto" ).val(data.subarea);
                    $("#seleccionarSubTipoPresupuesto" ).val(data.subtipo);
                    $("#seleccionarItemPresupuesto" ).val(data.item);
                    $("#seleccionarEstadoPresupuesto" ).val(data.estado);
                    $("#motivoGastoPresupuesto" ).val(data.motivo);
                    $("#areaRegistroGastoPresupuesto").show("slow");

                    botonModificarRegistro();

                    $("#montoGastoPresupuesto").focus();

                    alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
                } else {
                    alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
                }
            });

    }

    function eliminarRegistroGastoPresupuesto(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR EL REGISTRO : " + data.orden +" ?", function (e) {
                if (e) {
                    service.procesar("deleteRegistroGastoPresupuesto",data.orden,mensajeEliminacionGastoPresupuesto);
                    alertify.success("HA ACEPTADO ELIMNAR EL REGISTRO");
                } else {
                    alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
                }
            });

    }

    function mensajeEliminacionGastoPresupuesto(evt){
        if(evt == undefined){
            alertify.error("REGISTRO NO ELIMINADO");
        }else{
            listarRegistrosGastoPresupuesto();
            alertify.success("REGISTRO ELIMINADO");
        }

    }


    $("#botonBuscarSubCategoriaPresupuesto").click(function(){
        stringBuscada = $("#campoBuscarSubCategoriaPresupuesto").val();
        service.procesar("getListaSubcategoriasEncontradas",stringBuscada,cargaListaSubCategoriasCoincidencias);
    })

    function cargaListaSubCategoriasCoincidencias(evt){
        listadeRegistros = evt;

        $("#tablaListaSubCategoriaEncontradaPresupuesto tbody").html("");

        if ( listadeRegistros == undefined ) return;

        for(var i=0; i<listadeRegistros.length ; i++){
            datoRegistro = listadeRegistros[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");

            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeRegistros[i].codigo_subcategoria +'</td><td>'+ listadeRegistros[i].cuenta +'</td><td>'+ listadeRegistros[i].nombre_sub_categoria +'</td>');

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-success btn-xs' title='Seleccionar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-ok"></span');

            btnSeleccionar.data("data",datoRegistro);

            btnSeleccionar.on("click",seleccionarRegistroSubCategoria);

            contenedorBotones.append(btnSeleccionar);

            fila.append(contenedorBotones);

            $("#tablaListaSubCategoriaEncontradaPresupuesto tbody").append(fila);

        }
    }

    function seleccionarRegistroSubCategoria(){
        var data = $(this).data("data");
        $(".descStringPresupuesto").css("display", "");
        $("#buscarSubCategoriaPresupuesto").val(data.codigo_subcategoria);
        $("#descripcionSubCategoriaPresupuesto").html(data.nombre_sub_categoria);
        $('#modalBuscarSubCategoriaPresupuesto').modal("toggle");

    }

    //service.procesar("getListaCuentaNodoPresupuesto",cargaListaCuentaNodoPresupuesto);

    //function cargaListaCuentaNodoPresupuesto(evt){
    //    resultado = evt;
    //    $("#buscarNodoPresupuesto").html("<option value=''>---SELECCIONAR---</option>");
    //    if ( resultado == undefined ) return;
    //    for(var i=0; i<resultado.length ; i++){
    //        $("#buscarNodoPresupuesto" ).append( "<option value='"+ resultado[i].cuenta +"'>"+ resultado[i].cuenta +"</option>" );
    //    }
    //
    //}

    $("#limpiarCampoSubCategoria").click(function(){
        alertify.confirm("¿ SEGURO DE LIMPIAR EL CAMPO SUB CATEGORIA ?", function (e) {
                if (e) {
                    $("#buscarSubCategoriaPresupuesto").val("");
                    $("#descripcionSubCategoriaPresupuesto").html("");
                    alertify.success("PROCESO ACEPTADO");
                }else{
                    alertify.error("PROCESO CANCELADO");
                }
        })
    })

    $("#limpiarCampoString").click(function(){
        alertify.confirm("¿ SEGURO DE LIMPIAR EL CAMPO STRING ?", function (e) {
                if (e) {
                    $("#buscarStringPresupuesto").val("");
                    $("#descripcionStringPresupuesto").html("");
                    alertify.success("PROCESO ACEPTADO");
                }else{
                    alertify.error("PROCESO CANCELADO");
                }
        })
    })


    $("#limpiarCampoNodo").click(function(){
        alertify.confirm("¿ SEGURO DE LIMPIAR EL CAMPO NODO ?", function (e) {
                if (e) {
                    $("#buscarNodoPresupuesto").val("");
                    alertify.success("PROCESO ACEPTADO");
                }else{
                    alertify.error("PROCESO CANCELADO");
                }
        })
    })


    $("#enviarDatosGastoPresupuesto").click(function(){

            alertify.confirm("¿ SEGURO DE GUARDAR EL REGISTRO ?", function (e) {
                if (e) {

                    if($("#numeroOrdenGastoPresupuesto" ).val() == "" || $("#montoGastoPresupuesto" ).val() == "" || $("#fechaRegistroGastoPresupuesto" ).val() == "" || $("#proveedorGastoPresupuesto" ).val() == "" || $("#descripcionGastoPresupuesto").val() == "" || $("#datoSubAreaPresupuesto").val() == "" ){
                        alertify.error("EXISTEN CAMPOS VACIOS EN EL FORMULARIO");
                        return;
                    }

                    if ( $("#buscarStringPresupuesto" ).val() == "" && $("#buscarNodoPresupuesto" ).val() == "" && $("#buscarSubCategoriaPresupuesto" ).val() == "" ) return;
                    fecha = $("#fechaRegistroGastoPresupuesto" ).val();
                    var objDatosdeObjeto = new Object()
                        objDatosdeObjeto.usuario = usuario;
                        objDatosdeObjeto.procedimiento = $("#procedimientoDatosGastoPresupuesto").html();
                        objDatosdeObjeto.codigo_string = $("#datoDireccionPresupuesto" ).val();
                        objDatosdeObjeto.subCategoria = $("#datoSubCategoriaPresupuesto" ).val();
                        objDatosdeObjeto.cuenta_nodo = $("#datoCuentaNodoPresupuesto" ).val();
                        objDatosdeObjeto.norden = $("#numeroOrdenGastoPresupuesto" ).val();
                        objDatosdeObjeto.monto = $("#montoGastoPresupuesto" ).val();
                        objDatosdeObjeto.moneda = $("#seleccionarMonedaGatoPresupuesto" ).val();

                        if($("#seleccionarMonedaGatoPresupuesto" ).val() == "SOLES"){
                            objDatosdeObjeto.tipocambio = "";
                            objDatosdeObjeto.total = $("#montoGastoPresupuesto" ).val();
                        }else{
                            objDatosdeObjeto.tipocambio = $("#tipoCambioGastoPresupuesto" ).val();
                            objDatosdeObjeto.total = $("#totalGastoPresupuesto" ).val();
                        }
                        objDatosdeObjeto.fecha = fecha;
                        objDatosdeObjeto.mes = fecha.substring(5, 7);

                        if(fecha.substring(5, 7) == "01" || fecha.substring(5, 7) == "02" || fecha.substring(5, 7) == "03"){
                            objDatosdeObjeto.trimestre = "1";
                        }
                        if(fecha.substring(5, 7) == "04" || fecha.substring(5, 7) == "05" || fecha.substring(5, 7) == "06"){
                            objDatosdeObjeto.trimestre = "2";
                        }
                        if(fecha.substring(5, 7) == "07" || fecha.substring(5, 7) == "08" || fecha.substring(5, 7) == "09"){
                            objDatosdeObjeto.trimestre = "3";
                        }
                        if(fecha.substring(5, 7) == "10" || fecha.substring(5, 7) == "11" || fecha.substring(5, 7) == "12"){
                            objDatosdeObjeto.trimestre = "4";
                        }

                        objDatosdeObjeto.proveedor = $("#proveedorGastoPresupuesto" ).val();
                        objDatosdeObjeto.descripcion = $("#descripcionGastoPresupuesto" ).val();
                        objDatosdeObjeto.clave = $("#palablaClavePresupuesto" ).val();
                        objDatosdeObjeto.subarea = $("#datoSubAreaPresupuesto" ).val();
                        objDatosdeObjeto.subtipo = $("#seleccionarSubTipoPresupuesto" ).val();
                        objDatosdeObjeto.item = $("#seleccionarItemPresupuesto" ).val();
                        objDatosdeObjeto.estado = $("#seleccionarEstadoPresupuesto" ).val();
                        objDatosdeObjeto.motivo = $("#motivoGastoPresupuesto" ).val();
                        objDatosdeObjeto.idPresupuesto = $("#idPresupuestoGastoPresupuesto" ).val();

                    alertify.success("HA ACEPTADO GUARDAR EL REGISTRO");

                    service.procesar("saveGastoPresupuesto",objDatosdeObjeto,resultadoGuardadoGastoPresupuesto);

                } else {

                    alertify.error("PROCESO CANCELADO");
                }
            });

    })

    function resultadoGuardadoGastoPresupuesto(evt){
        resultado = evt;
        if(resultado == "ERROR"){
            alertify.error("REGISTRO DE GASTO NO GUARDADO");
            listarRegistrosGastoPresupuesto();
        }else{
            alertify.success("REGISTRO DE GASTO GUARDADO");
            listarRegistrosGastoPresupuesto();
            limpiaCamposGastoPresupuesto();
            activaTab('listaIngresoGastoPresupuesto');
        }

    }


    function limpiaCamposGastoPresupuesto(){
            $("#procedimientoDatosGastoPresupuesto").html("GUARDAR");
            $("#mensajeFormularioGastoPresupuesto").html("FORMULARIO INGRESO DE NUEVO REGISTRO");

            $("#numeroAutogeneradoGastoPresupuesto").val(ultimoregistro);

            $("#datoDireccionPresupuesto").val("");
            $("#datoCuentaNodoPresupuesto").val("");
            $("#datoSubCategoriaPresupuesto").val("");
            $("#datoSubAreaPresupuesto").val("");

            $("#numeroOrdenGastoPresupuesto").prop('disabled', false);
            $("#numeroOrdenGastoPresupuesto" ).val("");
            $("#montoGastoPresupuesto" ).val("");
            $("#seleccionarMonedaGatoPresupuesto" ).val("SOLES");
            $("#tipoCambioGastoPresupuesto" ).val("");
            $("#totalGastoPresupuesto" ).val("");
            $("#fechaRegistroGastoPresupuesto" ).val(fecha);
            $("#proveedorGastoPresupuesto" ).val("");
            $("#descripcionGastoPresupuesto" ).val("");
            $("#palablaClavePresupuesto" ).val("");
            $("#seleccionarSubTipoPresupuesto" ).val("");
            $("#seleccionarItemPresupuesto" ).val("");
            $("#seleccionarEstadoPresupuesto" ).val("INGRESADO");
            $("#areaRegistroGastoPresupuesto").hide('slow');
            $(".motivo").hide();
    }

    $("#cancelarDatosGastoPresupuesto").click(function(){
        alertify.confirm("¿ SEGURO DE CANCELAR EL REGISTRO ?", function (e) {
            if (e) {
                alertify.error("PROCESO CANCELADO");
                $("#areaRegistroGastoPresupuesto").hide('slow');
                limpiaCamposGastoPresupuesto();
                activaTab('listaIngresoGastoPresupuesto');
            }else{

            }
        });
    })

    $("#botonBuscarNodoPresupuesto").on("click",function(){
        nodoBuscada = $("#campoBuscarNodoPresupuesto").val();
        service.procesar("getListaNodogEncontradas",nodoBuscada,cargaListaNodoCoincidencias);
    })

    function cargaListaNodoCoincidencias(evt){
        listadeRegistrosdePresupuesto = evt;

        $("#tablaListaNodoEncontradaPresupuesto tbody").html("");

        if ( listadeRegistrosdePresupuesto == undefined ) return;

        for(var i=0; i<listadeRegistrosdePresupuesto.length ; i++){
            datoRegistro = listadeRegistrosdePresupuesto[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");

            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeRegistrosdePresupuesto[i].cuenta +'</td><td></td>');

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-success btn-xs' title='Seleccionar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-ok"></span');

            btnSeleccionar.data("data",datoRegistro);

            btnSeleccionar.on("click",seleccionarRegistroNodo);

            contenedorBotones.append(btnSeleccionar);

            fila.append(contenedorBotones);

            $("#tablaListaNodoEncontradaPresupuesto tbody").append(fila);

        }
    }

    function seleccionarRegistroNodo(){
        var data = $(this).data("data");
        $(".descStringPresupuesto").css("display", "");
        $("#buscarNodoPresupuesto").val(data.cuenta);
        $('#modalBuscarNodoPresupuesto').modal("toggle");
    }








    service.procesar("getListaTipoEncontradas",cargaListaTipoCoincidencias);

    $("#botonBuscarTipoPresupuesto").click(function(){
        dato = $("#campoBuscarTipoPresupuesto").val();
        service.procesar("getListaTipoEncontradas",dato,cargaListaTipoCoincidencias);
    })

    function cargaListaTipoCoincidencias(evt){
        resultado = evt;

        $("#tablaListaTipoEncontradaPresupuesto tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");

            fila.html('<td>'+ (i + 1) +'</td><td>'+ resultado[i].subarea +'</td>');

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-success btn-xs' title='Seleccionar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-ok"></span');

            btnSeleccionar.data("data",datoRegistro);

            btnSeleccionar.on("click",seleccionarRegistroTipo);

            contenedorBotones.append(btnSeleccionar);

            fila.append(contenedorBotones);

            $("#tablaListaTipoEncontradaPresupuesto tbody").append(fila);

        }
    }

    function seleccionarRegistroTipo(){
        var data = $(this).data("data");
        $("#seleccionarTipoPresupuesto").val(data.subarea);
        $('#modalBuscarTipoPresupuesto').modal("toggle");
    }


    service.procesar("getListaSubTipoEncontradas",cargaListaSubTipoCoincidencias);

    $("#botonBuscarSubTipoPresupuesto").click(function(){
        dato = $("#campoBuscarSubTipoPresupuesto").val();
        service.procesar("getListaSubTipoEncontradas",dato,cargaListaSubTipoCoincidencias);
    })

    function cargaListaSubTipoCoincidencias(evt){
        resultado = evt;

        $("#tablaListaSubTipoEncontradaPresupuesto tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");

            fila.html('<td>'+ (i + 1) +'</td><td>'+ resultado[i].subTipo +'</td>');

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-success btn-xs' title='Seleccionar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-ok"></span');

            btnSeleccionar.data("data",datoRegistro);

            btnSeleccionar.on("click",seleccionarRegistroSubTipo);

            contenedorBotones.append(btnSeleccionar);

            fila.append(contenedorBotones);

            $("#tablaListaSubTipoEncontradaPresupuesto tbody").append(fila);

        }
    }

    function seleccionarRegistroSubTipo(){
        var data = $(this).data("data");
        $("#seleccionarSubTipoPresupuesto").val(data.subTipo);
        $('#modalBuscarSubTipoPresupuesto').modal("toggle");
    }


    service.procesar("getListaItemEncontradas",cargaListaItemCoincidencias);

    $("#botonBuscarItemPresupuesto").click(function(){
        dato = $("#campoBuscarItemPresupuesto").val();
        service.procesar("getListaItemEncontradas",dato,cargaListaItemCoincidencias);
    })

    function cargaListaItemCoincidencias(evt){
        resultado = evt;

        $("#tablaListaItemEncontradaPresupuesto tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");

            fila.html('<td>'+ (i + 1) +'</td><td>'+ resultado[i].item +'</td>');

            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-success btn-xs' title='Seleccionar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-ok"></span');

            btnSeleccionar.data("data",datoRegistro);

            btnSeleccionar.on("click",seleccionarRegistroItem);

            contenedorBotones.append(btnSeleccionar);

            fila.append(contenedorBotones);

            $("#tablaListaItemEncontradaPresupuesto tbody").append(fila);

        }
    }

    function seleccionarRegistroItem(){
        var data = $(this).data("data");
        $("#seleccionarItemPresupuesto").val(data.item);
        $('#modalBuscarItemPresupuesto').modal("toggle");
    }

    $('select#seleccionarMonedaGatoPresupuesto').on('change',function(){
        dato = $('select#seleccionarMonedaGatoPresupuesto').val();
        cambioMoneda(dato);
    });

    $('select#seleccionarEstadoPresupuesto').on('change',function(){
        if($('select#seleccionarEstadoPresupuesto').val() != "INGRESADO"){
            $(".motivo").show();
        }else{
            $(".motivo").hide();
        }
        botonModificarRegistro();
    })

    function botonModificarRegistro(){
        if($('select#seleccionarEstadoPresupuesto').val() == "APROBADO"){
            $('#enviarDatosGastoPresupuesto').hide();
        }else{
            $('#enviarDatosGastoPresupuesto').show();
        }
    }

    function cambioMoneda(dato){
        if (dato == "DOLARES"){
            $(".tipocambio").show();
        }else{
            $(".tipocambio").hide();
        }
    }

    $("#recalcularMontoGastoPresupuesto").click(function(){
        monto =  parseFloat($("#montoGastoPresupuesto").val());
        tipocambio =  parseFloat($("#tipoCambioGastoPresupuesto").val());
        total = monto * tipocambio;
        $("#totalGastoPresupuesto").val(total);
    })




    function getLista (array,atributo){
        var nuevoArray = []
        for ( i = 0; i < array.length; i++) {
            valor = array[i][atributo];
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }


    service.procesar("getListaStringPresupuesto",0,cargaListaStringPresupuesto);

    function cargaListaStringPresupuesto(evt){

        string = evt;
        $("#direccionPresupuesto").html("");
        $("#direccionPresupuesto").append('<option value="">---SELECCIONAR---</option>');
        $("#datoDireccionPresupuesto").html("");
        $("#datoDireccionPresupuesto").append('<option value="">---SELECCIONAR---</option>');
        if ( string == undefined ) return;
        for(var i=0; i<string.length ; i++){
            $("#direccionPresupuesto").append('<option value='+ string[i].codigo +'>'+ string[i].descripcion + '</option>');
            $("#datoDireccionPresupuesto").append('<option value='+ string[i].codigo +'>'+ string[i].descripcion + '</option>');

        }

    }

    service.procesar("getListaSubcategoriasPresupuesto",0,cargaListaSubCategoriasPresupuesto);

    function cargaListaSubCategoriasPresupuesto(evt){
        array = evt;
        arreglo = getLista(array,"nombre_sub_categoria");
        $("#subCategoriaPresupuesto").shieldAutoComplete({
            dataSource: {
                data: arreglo
            },
            minLength: 1
        });
        $("#datoSubCategoriaPresupuesto").shieldAutoComplete({
            dataSource: {
                data: arreglo
            },
            minLength: 1
        });
    }

    service.procesar("getListaCuentaNodoPresupuesto",cargaListaCuentaNodoPresupuesto);

    function cargaListaCuentaNodoPresupuesto(evt){
        array = evt;
        arreglo = getLista(array,"cuenta");
        $("#cuentaNodoPresupuesto").shieldAutoComplete({
            dataSource: {
                data: arreglo
            },
            minLength: 1
        });
        $("#datoCuentaNodoPresupuesto").shieldAutoComplete({
            dataSource: {
                data: arreglo
            },
            minLength: 1
        });
    }

    service.procesar("getListaTipoPresupuesto",0,cargaListaTipoPresupuesto);

    function cargaListaTipoPresupuesto(evt){

        tipo = evt;
        $("#subAreaPresupuesto").html("");
        $("#subAreaPresupuesto").append('<option value="">---SELECCIONAR---</option>');
        $("#datoSubAreaPresupuesto").html("");
        $("#datoSubAreaPresupuesto").append('<option value="">---SELECCIONAR---</option>');
        if ( tipo == undefined ) return;
        for(var i=0; i<tipo.length ; i++){
            $("#subAreaPresupuesto").append('<option value='+ tipo[i].tipo +'>'+ tipo[i].tipo + '</option>');
            $("#datoSubAreaPresupuesto").append('<option value='+ tipo[i].tipo +'>'+ tipo[i].tipo + '</option>');

        }


    }

    service.procesar("getListaProveedorPresupuesto",0,cargaListaProveedorPresupuesto);

    function cargaListaProveedorPresupuesto(evt){
        array = evt;
        arreglo = getLista(array,"proveedor");
        $("#proveedorGastoPresupuesto").shieldAutoComplete({
            dataSource: {
                data: arreglo
            },
            minLength: 1
        });
        $("#proveedorPresupuesto").shieldAutoComplete({
            dataSource: {
                data: arreglo
            },
            minLength: 1
        });
    }




    $("#buscarPresupuesto").keyup(function(){
        listarRegistrosGastoPresupuesto();
    });





    $("#botonAgregarFiltroPersonalizadoPresupuesto").click(function(){
        valor = $("#valorFiltroPersonalizadoPresupuesto").val();
        if(valor != ""){
            var objDatos = new Object()
            objDatos.variable = $("#seleccionarFiltroPersonalizadoPresupuesto").val();
            objDatos.operador = $("#seleccionarOperadorFiltroPersonalizadoPresupuesto").val();
            objDatos.valor = valor;
            console.log(objDatos);
            alertify.success("DATOS INGRESADOS CORRECTAMENTE");
        }else{
            alertify.error("DATOS INCOMPLETOS");
        }

    })


    $("#numeroOrdenGastoPresupuesto").click(function(){
        valor = $("#datoDireccionPresupuesto").val();
        console.log(valor);
    })

    $("#montoGastoPresupuesto").keyup(function(){
        console.log($("#montoGastoPresupuesto").val());
    })


    function activaTab(tab){
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    };








}

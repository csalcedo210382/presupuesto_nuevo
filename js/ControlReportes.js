var service = new Service("webService/index.php");

$(function() 
{

iniciarControlReportes();

});


function iniciarControlReportes(){
	var f = new Date();
	var mes = f.getMonth() < 10 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
	var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
	var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;
	$("#fecha").html(fecha);

	$('#desde').val(f.getFullYear() +"-"+ mes +"-01");
	$('#hasta').val(fecha);
	
	var codigo_usuario = sessionStorage.getItem("logeado");
	
	service.procesar("getUsuario",codigo_usuario,cargaUsuario);

	service.procesar("getCategorias",cargaCategorias);

	service.procesar("getColaboradores",cargaColaboradores);

	service.procesar("getTipodeEstudio",cargaTipodeEstudio);

	function cargaUsuario(evt){
		datoUsuario = evt;
		$("#usuario").html(datoUsuario[0].nombre);
	}

	function cargaCategorias(evt){
		categorias = evt;
		$("#categorias").html("");
		$("#categorias").append('<option value="0">SELECCIONAR</option>');
		if ( categorias == undefined ) return;
		for(var i=0; i<categorias.length ; i++){
			$("#categorias").append('<option value="'+categorias[i].idCategoria+'">'+categorias[i].gcategoria+'</option>');	
		}
	}

	function cargaColaboradores(evt){
		colaboradores = evt;
		$("#colaboradores").html("");
		$("#colaboradores").append('<option value="0">SELECCIONAR</option>');
		if ( colaboradores == undefined ) return;
		for(var i=0; i<colaboradores.length ; i++){
			$("#colaboradores").append('<option value="'+colaboradores[i].idUsuario+'">'+colaboradores[i].gnombre+'</option>');	
		}
	}

	function cargaTipodeEstudio(evt){
		tipodeestudio = evt;
		$("#tipodeestudio").html("");
		$("#tipodeestudio").append('<option value="0">SELECCIONAR</option>');
		if ( tipodeestudio == undefined ) return;
		for(var i=0; i<tipodeestudio.length ; i++){
			$("#tipodeestudio").append('<option value="'+tipodeestudio[i].idTipoEstudio+'">'+tipodeestudio[i].gtipodeestudio+'</option>');	
		}
	}


}



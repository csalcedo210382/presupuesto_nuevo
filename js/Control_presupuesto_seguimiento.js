var service = new Service("webService/index.php");

var cuestionario = Array;


$(function() 
{

iniciarControl_presupuesto_seguimiento();

});


function iniciarControl_presupuesto_seguimiento(){
	var f = new Date();
	var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
	var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
	var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;
    var desde = f.getFullYear() +"-"+ mes +"-01";

    var registro_aprobacion = [];

    $("#presupuestoFechaReporteDesde").val(desde);
    $("#presupuestoFechaReporteHasta").val(fecha);

    var usuario = sessionStorage.getItem("usuario");

    var decimales = 0;




   $("#historicoRegistroPresupuesto").hide();
   
   
   //service.procesar("getListaFormatoSeguimiento",0,cargaListaFormatoSeguimiento);

    $("#menu_presupuesto_seguimiento").on('click', function(){
        listarRegistrosGastoPresupuestoAdmin();
        service.procesar("getProveedorSeguimiento",cargaProveedorSeguimiento);
    })

    function listarRegistrosGastoPresupuestoAdmin(){

            var objDatosdeObjeto = new Object()
            //objDatosdeObjeto.string = "";
            //objDatosdeObjeto.scategoria = "";
            //objDatosdeObjeto.nodo = "";
            //objDatosdeObjeto.variable = "";
            objDatosdeObjeto.usuario = usuario;
            //objDatosdeObjeto.desde = $("#presupuestoFechaReporteDesde").val();
            //objDatosdeObjeto.hasta = $("#presupuestoFechaReporteHasta").val();
            objDatosdeObjeto.proveedor = $("#presupuesto_seguimiento .proveedorseguimiento").val();
            objDatosdeObjeto.formato = $("#presupuesto_seguimiento .formato").val();
            objDatosdeObjeto.nformato = $("#presupuesto_seguimiento .nformato").val();
            objDatosdeObjeto.estado = $("select#seleccionarEstadoPresupuestoAdm").val();
            objDatosdeObjeto.texto = $("#presupuesto_seguimiento .textoBuscado").val();
            service.procesar("getSaldoStringNodoAdm",objDatosdeObjeto,cargaGastoStringNodoAdmin);
    }

    $('select#seleccionarEstadoPresupuestoAdm').on('change',function(){
        $("#historicoRegistroPresupuesto").hide();
        listarRegistrosGastoPresupuestoAdmin();
    });

    function cargaProveedorSeguimiento(evt){
        proveedor = evt;
        $("#presupuesto_seguimiento .proveedorseguimiento").html("<option value='0'>---SELECCIONAR---</option>");
        for(var i=0; i<proveedor.length ; i++){
            $("#presupuesto_seguimiento .proveedorseguimiento").append( "<option value='"+ proveedor[i].dni_ruc +"'>"+ proveedor[i].nombre_razon_social +"</option>" );
        }
    }

    $("#presupuesto_seguimiento .formato").on('change',function(){
        cargaListaFormatosSeguimiento();
    });

    function cargaListaFormatosSeguimiento(){
        var formato = $("#presupuesto_seguimiento .formato").val();
        service.procesar("getListaFormato",formato,nanio,cargaListaFormatoSeguimiento);
    }

    function cargaListaFormatoSeguimiento(evt){
        resultado = evt;
        $("#presupuesto_seguimiento .nformato").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#presupuesto_seguimiento .nformato" ).append( "<option value='"+ resultado[i].numero +"'>"+ resultado[i].numero +"</option>" );
        }        
    }

    $("#actualizarListaAdminGastosdePresupuesto").click(function(){
        Procesando();
        $("#historicoRegistroPresupuesto").hide();
        listarRegistrosGastoPresupuestoAdmin();
    })

    function cargaGastoStringNodoAdmin(evt){
        Terminado();

        resultado = evt;

        $("#tablaListaAdminGastosdePresupuesto tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");
            
            total = parseFloat(resultado[i].total);

            total_soles = formatNumber.new(total.toFixed(decimales), "S/. ")

            var empresa = corregirNull(resultado[i].nombre_razon_social);

            if(empresa == ""){
                empresa = corregirNull(resultado[i].proveedor);
            }

            //ESTADO PRINCIPAL QUE CONTROLA A TODOS
            if(datoRegistro.estado == 'RECHAZADO' || datoRegistro.estado == 'APROBADO'){ estado = datoRegistro.estado; }else{ estado = datoRegistro.estadoc; }

            fila.html('<td>'+ (i + 1) +'</td><td>'+ resultado[i].chr_area +'</td><td>'+ resultado[i].chr_proyecto +'</td><td>'+ resultado[i].chr_string.toUpperCase() +'</td><td>'+ empresa +'</td><td>'+ total_soles +'</td><td>'+ resultado[i].fechaGasto +'</td><td>'+ resultado[i].documento +'</td><td>'+ resultado[i].idFormato +'</td><td>'+ resultado[i].nformato +'</td><td>'+ datoRegistro.estadoc +'</td><td>'+ resultado[i].estado +'</td><td>'+ resultado[i].veces +'</td>');
            
            var contenedorBotones = $("<td><div class='input-group-btn'>");

            var btnSeleccionar = $("<button class='btn btn-success btn-sm' title='APROBACIONES'>");
            var btnHistorico = $("<button class='btn btn-info btn-sm' title='HISTORICO'>");
            //var btnRemove = $("<button class='btn btn-danger btn-xs' title='Cancelar'>");

            btnSeleccionar.html('<span class="glyphicon"></span> APROBACIONES');
            btnHistorico.html('<span class="glyphicon"></span> HISTORICO');
            //btnRemove.html('<span class="glyphicon glyphicon-remove"></span');            

            btnSeleccionar.data("data",datoRegistro);
            btnHistorico.data("data",datoRegistro);
            //btnRemove.data("data",datoRegistro);

            btnSeleccionar.on("click",aprobacionesRegistroGastoPresupuestoAdmin);
            //btnSeleccionar.on("click",aprobarRegistroGastoPresupuestoAdmin);
            btnHistorico.on("click",historicoRegistroGastoPresupuestoAdmin);
            //btnRemove.on("click",cancelarRegistroGastoPresupuestoAdmin);

            //if(resultado[i].estado != 'APROBADO' && resultado[i].estado != 'CANCELADO'){
            //    contenedorBotones.append(btnSeleccionar);
            //    contenedorBotones.append(btnRemove);
            //}
            //contenedorBotones.append(btnHistorico);
            
            contenedorBotones.append(btnSeleccionar);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnHistorico);

            fila.append(contenedorBotones);

            $("#tablaListaAdminGastosdePresupuesto tbody").append(fila);

        }


    }

    function aprobacionesRegistroGastoPresupuestoAdmin(){
        var data = $(this).data("data");

        service.procesar("getAprobaciones",data,"UNICO",mostrarAprobacionesPresupuesto);
        $("#historicoRegistroPresupuesto").show();
    }

    function historicoRegistroGastoPresupuestoAdmin(){
        var data = $(this).data("data");
        service.procesar("getAprobaciones",data,"HISTORICO",mostrarAprobacionesPresupuesto);
        $("#historicoRegistroPresupuesto").show();
    }

    function mostrarAprobacionesPresupuesto(evt){
        resultado_saldos = evt;
        resultado = evt.registros;
        filas = evt.filas;

        saldo_mes = evt.saldo_m;
        monto = evt.monto;

        //console.log(corregirNull(saldo_mes));
        //console.log(corregirNull(monto));
        if((corregirNull(resultado_saldos.saldo_y)).toFixed(2) > 0){
            if(parseFloat(corregirNull(saldo_mes)) < parseFloat(corregirNull(monto)) || corregirNull(saldo_mes) == ""){
                //console.log("alerta");
                $("#modalConsultarSaldoModuloAprobacion").modal("show");
                $("#modalConsultarSaldoModuloAprobacion .mAprobacion").html("SALDO MES "+resultado_saldos.mes);
                $("#modalConsultarSaldoModuloAprobacion .qAprobacion").html("SALDO Q-"+resultado_saldos.trimestre);
                $("#modalConsultarSaldoModuloAprobacion .montoAprobacion").html(formatNumber.new(parseFloat(resultado_saldos.monto).toFixed(2), "S/. "));
                $("#modalConsultarSaldoModuloAprobacion .saldoMAprobacion").html(formatNumber.new(parseFloat(corregirNull(resultado_saldos.saldo_m)).toFixed(2), "S/. "));
                $("#modalConsultarSaldoModuloAprobacion .saldoQAprobacion").html(formatNumber.new(parseFloat(corregirNull(resultado_saldos.saldo_q)).toFixed(2), "S/. "));
                $("#modalConsultarSaldoModuloAprobacion .saldoYAprobacion").html(formatNumber.new(parseFloat(corregirNull(resultado_saldos.saldo_y)).toFixed(2), "S/. "));
            }
        }

        $("#tablaListaAprobacionesdePresupuesto tbody").html("");

        if ( resultado == undefined ) return;

        for(var x=0; x<filas.length ; x++){
            var aprobaciones = filas[x].aprobaciones;
            var columnas = aprobaciones / 2;
            var cuenta = 0;
            var estadoBoton = "P";
            var activado = "";
            var colorBoton = "btn-default";

            var flecha = "flechaderecha";


            var filaResponsables = $("<tr>");
            var filaIconos = $("<tr>");
            var filaBotones = $("<tr>");

            var filavacia = "<tr class='celdavacia'>";


            //FOR DE BOTONES
            for(var y=0; y<resultado.length ; y++){
                //BOTONES
                if(resultado[y].idSeguimiento == filas[x].idSeguimiento){
                    cuenta++;
                    datoRegistro = resultado[y];
                    estadoBoton = resultado[y].estado;
                    if(estadoBoton == "A" || estadoBoton == "T"){ colorBoton = "btn-success"; activado = ""; }
                    if(estadoBoton == "R"){ colorBoton = "btn-danger"; activado = ""; }
                    if(estadoBoton == "B"){ colorBoton = "btn-primary"; activado = ""; }
                    if(estadoBoton == "C"){ colorBoton = "btn-default"; activado = "disabled"; }
                    if(estadoBoton == "P"){ colorBoton = "btn-warning"; activado = ""; }

                    var celdaBotones = $("<td class='centro'>");
                    var btnSeleccionar = $("<button type='button' class='"+colorBoton+" botonSeguimiento' "+activado+">");
                    //btnSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
                    btnSeleccionar.html(resultado[y].contenido);
                    btnSeleccionar.data("data",datoRegistro);
                    btnSeleccionar.on("click",mostrarValorBoton);
                    celdaBotones.append(btnSeleccionar);

                    filavacia = filavacia + "<td></td><td></td>";

                    filaResponsables.append("<td class='centro negrita'>"+resultado[y].cargo+"</td><td></td>");
                    filaIconos.append("<td class='centro'>ICO</td></td><td></td>");
                    filaBotones.append(celdaBotones);

                    //AQUI PARTE DEL CODIGO PARA QUE SOLO PINTE FLECHA CUANDO ES UN VALOR PAR

                    if(cuenta < columnas){
                        filaBotones.append("<td><img src='img/"+flecha+".png'></td>");
                    }


                    if(cuenta == columnas){
                        cuenta = 0;
                        filavacia = filavacia + "</tr>";
                        if(flecha == "flechaderecha"){
                            $("#tablaListaAprobacionesdePresupuesto tbody").append(filaResponsables);
                            $("#tablaListaAprobacionesdePresupuesto tbody").append(filaIconos);                        
                            $("#tablaListaAprobacionesdePresupuesto tbody").append(filaBotones);   
                        }else{
                            $("#tablaListaAprobacionesdePresupuesto tbody").append(filaBotones);
                            $("#tablaListaAprobacionesdePresupuesto tbody").append(filaResponsables);
                            $("#tablaListaAprobacionesdePresupuesto tbody").append(filaIconos);                        
                        }
                        
                        filaResponsables = $("<tr>");
                        filaIconos = $("<tr>");
                        filaBotones = $("<tr>");

                        $("#tablaListaAprobacionesdePresupuesto tbody").append(filavacia);

                        filavacia = "<tr class='celdavacia'>";
                        flecha = "flechaizquierda";

                    }
                    
                }
            }

            filavacia = filavacia + "</tr>";
            $("#tablaListaAprobacionesdePresupuesto tbody").append(filavacia);
            filavacia = "<tr>"         
        }

    }

    function mostrarValorBoton(){
        var data = $(this).data("data");
        //console.log(data);
        //console.log(usuario);
        registro_aprobacion = data;
        var estado = data.estado;

        if((estado == "A" || estado == "R") && data.aprobador != "") {
            $("#modalAprobarRechazarRegistro .informativa").css("display","");
            $("#modalAprobarRechazarRegistro .efectiva").css("display","none");
            $("#modalAprobarRechazarRegistro .efectiva_motivo").css("display","none");
            $("#modalAprobarRechazarRegistro").modal("show");
        }

        if(estado == "R"){
            $("#modalAprobarRechazarRegistro .motivoRechazo").show();
            $("#modalAprobarRechazarRegistro .motivoAprobacionSeguimiento").html(data.motivo);
        }else{
            $("#modalAprobarRechazarRegistro .motivoRechazo").hide();
        }

        if(estado == "P" && data.aprobador == usuario){
            $("#modalAprobarRechazarRegistro .informativa").css("display","none");
            $("#modalAprobarRechazarRegistro .efectiva").css("display","");
            $("#modalAprobarRechazarRegistro .efectiva_motivo").css("display","none");
            $("#aprobarFormularioSeguimiento").removeClass("btn-success").addClass("btn-default");
            $("#rechazarFormularioSeguimiento").removeClass("btn-danger").addClass("btn-default");
            $("#motivoFormularioSeguimiento").val("");
            $("#modalAprobarRechazarRegistro").modal("show");

        }
        
        if(estado == "A" && data.aprobador == "" && usuario == "mirsalaz"){
            $("#modalAprobarRechazarRegistroAprobado .informativa").css("display","none");
            $("#modalAprobarRechazarRegistroAprobado .efectiva").css("display","");
            $("#modalAprobarRechazarRegistroAprobado .efectiva_motivo").css("display","none");
            $("#modalAprobarRechazarRegistroAprobado").modal("show");
        }

        $("#modalAprobarRechazarRegistro .estadoAprobacionSeguimiento").html(data.estado);
        $("#modalAprobarRechazarRegistro .fechaAprobacionSeguimiento").html(data.fechaAprobacion);
        $("#modalAprobarRechazarRegistro .usuarioAprobacionSeguimiento").html(data.nombre_completo);
       
    }

    $("#aprobarFormularioSeguimiento").on('click',function(){
        $("#modalAprobarRechazarRegistro .efectiva_motivo").css("display","none");
        $("#aprobarFormularioSeguimiento").removeClass("btn-default").addClass("btn-success");
        $("#rechazarFormularioSeguimiento").removeClass("btn-danger").addClass("btn-default");

        alertify.confirm("¿ SEGURO DE APROBAR EL REGISTRO ?", function (e) {
            if (e) {
                var objDatosdeObjeto = new Object()
                objDatosdeObjeto.usuario = usuario;
                objDatosdeObjeto.idSeguimiento = registro_aprobacion.idSeguimiento;
                objDatosdeObjeto.idPresupuesto = registro_aprobacion.idPresupuesto;
                objDatosdeObjeto.aprobacion = registro_aprobacion.orden;
                objDatosdeObjeto.estado = "A";
                service.procesar("aprobarGastoPresupuesto",objDatosdeObjeto,actualizarGraficoSeguimiento);
                //$("#historicoRegistroPresupuesto").hide("slow");
                alertify.success("HA ACEPTADO APROBAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE APROBACION");
            }
        }); 
    })

    $("#rechazarFormularioSeguimiento").on('click',function(){
        $("#modalAprobarRechazarRegistro .efectiva_motivo").css("display","");
        $("#rechazarFormularioSeguimiento").removeClass("btn-default").addClass("btn-danger");
        $("#aprobarFormularioSeguimiento").removeClass("btn-success").addClass("btn-default");

        var motivo = $("#motivoFormularioSeguimiento").val();
        if(motivo != ""){
            alertify.confirm("¿ SEGURO DE RECHAZAR EL REGISTRO ?", function (e) {
                if (e) {
                    var objDatosdeObjeto = new Object()
                    objDatosdeObjeto.usuario = usuario;
                    objDatosdeObjeto.idSeguimiento = registro_aprobacion.idSeguimiento;
                    objDatosdeObjeto.idPresupuesto = registro_aprobacion.idPresupuesto;
                    objDatosdeObjeto.aprobacion = registro_aprobacion.orden;
                    objDatosdeObjeto.motivo = motivo;
                    objDatosdeObjeto.estado = "R";
                    service.procesar("rechazarGastoPresupuesto",objDatosdeObjeto,actualizarGraficoSeguimiento);
                    //$("#historicoRegistroPresupuesto").hide("slow");
                    alertify.success("HA ACEPTADO RECHAZAR EL REGISTRO");
                } else {
                    alertify.error("HA CANCELADO EL PROCESO DE APROBACION");
                }
            });             
        }else{
            alertify.error("REGISTRAR MOTIVO");
        }
    })

    $("#rechazarFormularioSeguimientoAprobado").on('click',function(){
        $("#modalAprobarRechazarRegistroAprobado .efectiva_motivo").css("display","");
        $("#rechazarFormularioSeguimientoAprobado").addClass("btn-danger");
        $("#aprobarFormularioSeguimientoAprobado").removeClass("btn-success");

        var motivo = $("#motivoFormularioSeguimientoAprobado").val();
        if(motivo != ""){
            alertify.confirm("¿ SEGURO DE RECHAZAR EL REGISTRO ?", function (e) {
                if (e) {
                    var objDatosdeObjeto = new Object()
                    objDatosdeObjeto.usuario = usuario;
                    objDatosdeObjeto.idSeguimiento = registro_aprobacion.idSeguimiento;
                    objDatosdeObjeto.idPresupuesto = registro_aprobacion.idPresupuesto;
                    objDatosdeObjeto.aprobacion = registro_aprobacion.orden;
                    objDatosdeObjeto.motivo = motivo;
                    objDatosdeObjeto.estado = "R";
                    service.procesar("rechazarGastoPresupuestoAprobado",objDatosdeObjeto,actualizarGraficoSeguimientoAprobado);
                    //$("#historicoRegistroPresupuesto").hide("slow");
                    alertify.success("HA ACEPTADO RECHAZAR EL REGISTRO");
                } else {
                    alertify.error("HA CANCELADO EL PROCESO DE APROBACION");
                }
            });             
        }else{
            alertify.error("REGISTRAR MOTIVO");
        }
    })

    function actualizarGraficoSeguimiento(evt){
        if(evt != "ERROR"){
            service.procesar("getAprobaciones",registro_aprobacion,"UNICO",mostrarAprobacionesPresupuesto);
            $("#historicoRegistroPresupuesto").show();
            $("#modalAprobarRechazarRegistro").modal("hide");
            alertify.success("PROCESO GUARDADO CORRECTAMENTE");
            listarRegistrosGastoPresupuestoAdmin();      
        }else{
            alertify.error("PROCESO NO GUARDADO");
        }
    }

    function actualizarGraficoSeguimientoAprobado(evt){
        if(evt != "ERROR"){
            service.procesar("getAprobaciones",registro_aprobacion,"UNICO",mostrarAprobacionesPresupuesto);
            $("#historicoRegistroPresupuesto").show();
            $("#modalAprobarRechazarRegistroAprobado").modal("hide");
            alertify.success("PROCESO GUARDADO CORRECTAMENTE");
            listarRegistrosGastoPresupuestoAdmin();      
        }else{
            alertify.error("PROCESO NO GUARDADO");
        }

    }


    //FUNCION ANTERIOR PARA APROBAR EL FLUJO
    /*$("#aprobarRegistroGastoPresupuestoAdmin").on('click', function(){
        alertify.confirm("¿ SEGURO DE APROBAR EL REGISTRO ?", function (e) {
            if (e) {
                var objDatosdeObjeto = new Object()
                objDatosdeObjeto.codigo = data.idPresupuesto;
                objDatosdeObjeto.usuario = usuario;
                objDatosdeObjeto.estado = "APROBADO";
                objDatosdeObjeto.motivo = "APROBADO";
                //service.procesar("updateGastoPresupuesto",objDatosdeObjeto,listarRegistrosGastoPresupuestoAdmin);
                //$("#historicoRegistroPresupuesto").hide("slow");
                alertify.success("HA ACEPTADO APROBAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE APROBACION");
            }
        });    
    })*/

    /*function aprobarRegistroGastoPresupuestoAdmin(){
        var data = $(this).data("data");
            alertify.confirm("¿ SEGURO DE APROBAR EL REGISTRO : " + data.orden +" ?", function (e) {
                if (e) {
                    var objDatosdeObjeto = new Object()
                    objDatosdeObjeto.codigo = data.idPresupuesto;
                    objDatosdeObjeto.usuario = usuario;
                    objDatosdeObjeto.estado = "APROBADO";
                    objDatosdeObjeto.motivo = "APROBADO";
                    service.procesar("updateGastoPresupuesto",objDatosdeObjeto,listarRegistrosGastoPresupuestoAdmin);
                    $("#historicoRegistroPresupuesto").hide("slow");
                    alertify.success("HA ACEPTADO APROBAR EL REGISTRO");
                } else {
                    alertify.error("HA CANCELADO EL PROCESO DE APROBACION");
                }
            });
    }*/

    /*function cancelarRegistroGastoPresupuestoAdmin(){
        var data = $(this).data("data");
            alertify.prompt("¿ SEGURO DE CANCELAR EL REGISTRO : " + data.orden +" ? <BR><BR> INGRESAR MOTIVO", function (e, str) {
                if (e) {
                    if(str){
                        var objDatosdeObjeto = new Object()
                        objDatosdeObjeto.codigo = data.idPresupuesto;
                        objDatosdeObjeto.usuario = usuario;
                        objDatosdeObjeto.estado = "CANCELADO";
                        objDatosdeObjeto.motivo = str;
                        service.procesar("updateGastoPresupuesto",objDatosdeObjeto,listarRegistrosGastoPresupuestoAdmin);
                        $("#historicoRegistroPresupuesto").hide("slow");
                        alertify.success("HA ACEPTADO CANCELAR EL REGISTRO");
                    }else{
                        alertify.error("INGRESAR MOTIVO DE LA CANCELACION");
                    }
                } else {
                    alertify.error("HA CANCELADO EL PROCESO DE CANCELACION");
                }
            });
    }*/

    /*function historicoRegistroGastoPresupuestoAdmin(){
        $("#historicoRegistroPresupuesto").hide("slow");
        var data = $(this).data("data");
        
        var objDatosdeObjeto = new Object()
            objDatosdeObjeto.codigo = data.idPresupuesto;
            objDatosdeObjeto.usuario = usuario;

        service.procesar("getHistoricoPresupuestoAdm",objDatosdeObjeto,cargaHistoricoStringNodoAdmin);
        $("#historicoRegistroPresupuesto").show("slow");
    }*/




    function cargaHistoricoStringNodoAdmin(evt){
        resultado = evt;

        $("#tablaListaAdminGastosHistoricosdePresupuesto tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");

            fechaApCa = resultado[i].fechaApCa;
            fechaApCa = fechaApCa.substring(1, 10);

            fila.html('<td>'+ (i + 1) +'</td><td>'+ resultado[i].codigo_string +'</td><td>'+ resultado[i].string_nom +'</td><td>'+ resultado[i].cuenta_nodo +'</td><td>'+ resultado[i].orden +'</td><td>'+ resultado[i].tipo +'</td><td>'+ resultado[i].item +'</td><td>'+ resultado[i].clave +'</td><td>'+ resultado[i].total +'</td><td>'+ resultado[i].fecha +'</td><td>'+ resultado[i].proveedor +'</td><td>'+ resultado[i].estado +'</td><td>'+ resultado[i].motivo +'</td><td>'+ fechaApCa +'</td><td>'+ resultado[i].usuario +'</td>');
            
            $("#tablaListaAdminGastosHistoricosdePresupuesto tbody").append(fila);

        }

    }



    $("#tablaListaAdminGastosdePresupuesto").on('click', function(){

    })






    function corregirNull(valor){
        var nuevovalor = "";
        if(valor == null){
          nuevovalor = "";
        } else {
          nuevovalor = valor;
        }
        return nuevovalor;
    }


    var formatNumber = {
     separador: ",", // separador para los miles
     sepDecimal: '.', // separador para los decimales
     formatear:function (num){
      num +='';
      var splitStr = num.split(',');
      var splitLeft = splitStr[0];
      var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
      var regx = /(\d+)(\d{3})/;
      while (regx.test(splitLeft)) {
      splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
      }
      return this.simbol + splitLeft  +splitRight;
     },
     new:function(num, simbol){
      this.simbol = simbol ||'';
      return this.formatear(num);
     }
    }













}



﻿var service = new Service("webService/index.php");

//var cuestionario = Array;


$(function()
{

iniciarControlPresupuestoInicial();

});

function iniciarControlPresupuestoInicial(){
    var f = new Date();
    var nmes = (f.getMonth()+1);
    var nanio = f.getFullYear();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;


    var decimales = 0;
    var listadeRegistrosdePresupuesto = "";
    var listadeRegistrosdeEjecutado = "";

    //var objetoSubVariable = "";

    var botonMostrarDetalleGasto = "";
    var columnasTabla = 0;




    $("#anioFormato").html(nanio);


    /*
    var tableOffset = $("#tablaReportePresupuesto").offset().top;
    var $header = $("#tablaListadeRegistrosdePresupuesto > thead").clone();
    var $fixedHeader = $("#header-fixed").append($header);

    $(window).bind("scroll", function() {
        var offset = $(this).scrollTop();
        
        if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
            $fixedHeader.show();
        }
        else if (offset < tableOffset) {
            $fixedHeader.hide();
        }
    });
    */


    service.procesar("getAreaReportePresupuesto", cargaAreaReportePresupuesto);
    //$(".tablaFija").fixedtableheader();

    function cargaConsultasIniciales(){
        //service.procesar("getAreaReportePresupuesto", cargaAreaReportePresupuesto);
    }

    function cargaAreaReportePresupuesto(evt){
        resultado = evt;
        $("#areaReportePresupuesto").html("<option value=''>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#areaReportePresupuesto" ).append( "<option value='"+ resultado[i].chr_area_id +"'>"+ resultado[i].chr_area.toUpperCase() +"</option>" );
        }
    }



    $('#anioPresupuesto').on('change',function(){
        nanio = $('#anioPresupuesto').val();
        $("#anioFormato").html(nanio);
        procesarRegistrosPresupuesto();
    });



    // AGREGAR FILTROS AL REPORTE - CON LA OPCION DE SUBFILTRO

    
    service.procesar("getFiltroReporte",0,cargaFiltro1Presupuesto);

    function cargaFiltro1Presupuesto(evt){
        resultado = evt;
        $("#variable1").html("<option value=''>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#variable1" ).append( "<option value='"+ resultado[i].codigo +"'>"+ resultado[i].filtro.toUpperCase() +"</option>" );
        }
    }

    $("#variable1").on('change', function(){
        var variable1 = $("#variable1").val();
        var variable = $("#variable1").val();
        
        $("#variable2").html("<option value=''>---SELECCIONAR---</option>");
        $("#variable3").html("<option value=''>---SELECCIONAR---</option>");
        $("#variable4").html("<option value=''>---SELECCIONAR---</option>");

        $("#subvariable1").css("display", "none");
        $("#subvariable2").css("display", "none");
        $("#subvariable3").css("display", "none");
        $("#subvariable4").css("display", "none");
        $("#subvariable1").html("<option value=''>---SELECCIONAR---</option>");
        $("#subvariable2").html("<option value=''>---SELECCIONAR---</option>");
        $("#subvariable3").html("<option value=''>---SELECCIONAR---</option>");
        $("#subvariable4").html("<option value=''>---SELECCIONAR---</option>");

        service.procesar("getSubVariable",variable1,nanio,function(evt){
            resultado = evt;
            if ( resultado == undefined ) return;
            $("#subvariable1").css("display", "");
            for(var i=0; i<resultado.length ; i++){
                $("#subvariable1" ).append( "<option value='"+ resultado[i].codigosubvariable +"'>"+ resultado[i].listasubvariable.toUpperCase() +"</option>" );
            }
            service.procesar("getFiltroReporte",variable,cargaFiltro2Presupuesto);
        });
    })

    //CAMBIOS DE LAS SUBVARIABLES

    $("#subvariable1").on('change', function(){
        var subvariable1 = $("#subvariable1").val();
        var subvariable = $("#subvariable1").val();
        var variable1 = $("#variable1").val();
        var variable2 = $("#variable2").val();
        
        $("#subvariable2").css("display", "none");
        $("#subvariable3").css("display", "none");
        $("#subvariable4").css("display", "none");
        $("#subvariable2").html("<option value=''>---SELECCIONAR---</option>");
        $("#subvariable3").html("<option value=''>---SELECCIONAR---</option>");
        $("#subvariable4").html("<option value=''>---SELECCIONAR---</option>");

            $("#subvariable2").css("display", "");
            service.procesar("getSubVariable2",variable1,subvariable1,variable2,nanio,function(evt){
                resultado = evt;
                if ( resultado == undefined ) return;
                $("#subvariable2").css("display", "");
                for(var i=0; i<resultado.length ; i++){
                    $("#subvariable2" ).append( "<option value='"+ resultado[i].codigosubvariable +"'>"+ resultado[i].listasubvariable.toUpperCase() +"</option>" );
                }
                service.procesar("getFiltroReporte",variable,cargaFiltro3Presupuesto);
            });

        
    })

    $("#subvariable2").on('change', function(){
        var subvariable1 = $("#subvariable1").val();
        var subvariable2 = $("#subvariable2").val();
        var subvariable = $("#subvariable2").val();
        var variable1 = $("#variable1").val();
        var variable2 = $("#variable2").val();
        var variable3 = $("#variable3").val();

        $("#subvariable3").css("display", "none");
        $("#subvariable4").css("display", "none");
        $("#subvariable3").html("<option value=''>---SELECCIONAR---</option>");
        $("#subvariable4").html("<option value=''>---SELECCIONAR---</option>");

            $("#subvariable3").css("display", "");
            service.procesar("getSubVariable3",variable1,subvariable1,variable2,subvariable2,variable3,nanio,function(evt){
                resultado = evt;
                if ( resultado == undefined ) return;
                $("#subvariable3").css("display", "");
                for(var i=0; i<resultado.length ; i++){
                    $("#subvariable3" ).append( "<option value='"+ resultado[i].codigosubvariable +"'>"+ resultado[i].listasubvariable.toUpperCase() +"</option>" );
                }
                service.procesar("getFiltroReporte",variable,cargaFiltro3Presupuesto);
            });

        
    })

    $("#subvariable3").on('change', function(){
        var subvariable1 = $("#subvariable1").val();
        var subvariable2 = $("#subvariable2").val();
        var subvariable3 = $("#subvariable3").val();
        var variable1 = $("#variable1").val();
        var variable2 = $("#variable2").val();
        var variable3 = $("#variable3").val();
        var variable4 = $("#variable4").val();

        $("#subvariable4").css("display", "none");
        $("#subvariable4").html("<option value=''>---SELECCIONAR---</option>");

            $("#subvariable4").css("display", "");
            service.procesar("getSubVariable4",variable1,subvariable1,variable2,subvariable2,variable3,subvariable3,variable4,nanio,function(evt){
                resultado = evt;
                if ( resultado == undefined ) return;
                $("#subvariable4").css("display", "");
                for(var i=0; i<resultado.length ; i++){
                    $("#subvariable4" ).append( "<option value='"+ resultado[i].codigosubvariable +"'>"+ resultado[i].listasubvariable.toUpperCase() +"</option>" );
                }
                service.procesar("getFiltroReporte",variable,cargaFiltro3Presupuesto);
            });

        
    })
    //


    function cargaFiltro2Presupuesto(evt){
        resultado = evt;
        $("#variable2").html("<option value=''>---SELECCIONAR---</option>");
        $("#variable3").html("<option value=''>---SELECCIONAR---</option>");
        $("#variable4").html("<option value=''>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#variable2" ).append( "<option value='"+ resultado[i].codigo +"'>"+ resultado[i].filtro.toUpperCase() +"</option>" );
        }
    }


    $("#variable2").on('change', function(){
        var subvariable1 = $("#subvariable1").val();
        var variable1 = $("#variable1").val();
        var variable2 = $("#variable2").val();
        var variable = $("#variable1").val() + "','" + $("#variable2").val();
        
        $("#variable3").html("<option value=''>---SELECCIONAR---</option>");
        $("#variable4").html("<option value=''>---SELECCIONAR---</option>");

        $("#subvariable2").css("display", "");
        $("#subvariable3").css("display", "none");
        $("#subvariable4").css("display", "none");

        $("#subvariable2").html("<option value=''>---SELECCIONAR---</option>");
        $("#subvariable3").html("<option value=''>---SELECCIONAR---</option>");
        $("#subvariable4").html("<option value=''>---SELECCIONAR---</option>");

            $("#subvariable2").css("display", "");
            service.procesar("getSubVariable2",variable1,subvariable1,variable2,nanio,function(evt){
                resultado = evt;
                if ( resultado == undefined ) return;
                $("#subvariable2").css("display", "");
                for(var i=0; i<resultado.length ; i++){
                    $("#subvariable2" ).append( "<option value='"+ resultado[i].codigosubvariable +"'>"+ resultado[i].listasubvariable.toUpperCase() +"</option>" );
                }
                service.procesar("getFiltroReporte",variable,cargaFiltro3Presupuesto);
            });

        

    })

    function cargaFiltro3Presupuesto(evt){
        resultado = evt;
        $("#variable3").html("<option value=''>---SELECCIONAR---</option>");
        $("#variable4").html("<option value=''>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#variable3" ).append( "<option value='"+ resultado[i].codigo +"'>"+ resultado[i].filtro.toUpperCase() +"</option>" );
        }
    }


    $("#variable3").on('change', function(){
        var variable1 = $("#variable1").val();
        var variable2 = $("#variable2").val();
        var variable3 = $("#variable3").val();
        var subvariable1 = $("#subvariable1").val();
        var subvariable2 = $("#subvariable2").val();
        var variable = $("#variable1").val() + "','" + $("#variable2").val() + "','" + $("#variable3").val();
        
        $("#variable4").html("<option value=''>---SELECCIONAR---</option>");

        $("#subvariable4").css("display", "none");

        $("#subvariable4").html("<option value=''>---SELECCIONAR---</option>");

            $("#subvariable3").css("display", "");
            service.procesar("getSubVariable3",variable1,subvariable1,variable2,subvariable2,variable3,nanio,function(evt){
                resultado = evt;
                if ( resultado == undefined ) return;
                $("#subvariable3").css("display", "");
                for(var i=0; i<resultado.length ; i++){
                    $("#subvariable3" ).append( "<option value='"+ resultado[i].codigosubvariable +"'>"+ resultado[i].listasubvariable.toUpperCase() +"</option>" );
                }
                service.procesar("getFiltroReporte",variable,cargaFiltro4Presupuesto);
            });
        
    })

    function cargaFiltro4Presupuesto(evt){
        resultado = evt;
        $("#variable4").html("<option value=''>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#variable4" ).append( "<option value='"+ resultado[i].codigo +"'>"+ resultado[i].filtro.toUpperCase() +"</option>" );
        }
    }


    $("#variable4").on('change', function(){
        var variable1 = $("#variable1").val();
        var variable2 = $("#variable2").val();
        var variable3 = $("#variable3").val();
        var subvariable1 = $("#subvariable1").val();
        var subvariable2 = $("#subvariable2").val();
        var subvariable3 = $("#subvariable3").val();
        var variable4 = $("#variable4").val();
        var variable = $("#variable1").val() + "','" + $("#variable2").val() + "','" + $("#variable3").val() + "','" + $("#variable4").val();
        
        $("#subvariable4").css("display", "none");

        $("#subvariable4").html("<option value=''>---SELECCIONAR---</option>");

            $("#subvariable4").css("display", "");
            service.procesar("getSubVariable4",variable1,subvariable1,variable2,subvariable2,variable3,subvariable3,variable4,nanio,function(evt){
                resultado = evt;
                if ( resultado == undefined ) return;
                $("#subvariable4").css("display", "");
                for(var i=0; i<resultado.length ; i++){
                    $("#subvariable4" ).append( "<option value='"+ resultado[i].codigosubvariable +"'>"+ resultado[i].listasubvariable.toUpperCase() +"</option>" );
                }
                //service.procesar("getFiltroReporte",variable,cargaFiltro3Presupuesto);
            });

        //service.procesar("getFiltroReporte",variable,cargaFiltro3Presupuesto);
    })

















    function cargaListadeRegistrosdePresupuesto(evt){

        var meses_largo = ["ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"];
        var meses_largo = ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"];
        var campos_reporte_titulo = []; //LIMPIAR LOS CAMPOS ANTERIORES
        var campos_reporte_corto = []; //LIMPIAR LOS CAMPOS ANTERIORES
        var campos_reporte = []; //LIMPIAR LOS CAMPOS ANTERIORES
        var variable1 = $("#variable1").val();
        var variable2 = $("#variable2").val();
        var variable3 = $("#variable3").val();
        var variable4 = $("#variable4").val();

        var svariable1 = ($("#variable1").val()).replace('_id','');
        var svariable2 = ($("#variable2").val()).replace('_id','');
        var svariable3 = ($("#variable3").val()).replace('_id','');
        var svariable4 = ($("#variable4").val()).replace('_id','');

        //console.log(svariable1);
        //console.log(svariable2);
        //console.log(svariable3);
        //console.log(svariable4);

        var tvariable1 = $("#variable1 option:selected").text(); //$("#variable1").val();
        var tvariable2 = $("#variable2 option:selected").text(); //$("#variable2").val();
        var tvariable3 = $("#variable3 option:selected").text(); //$("#variable3").val();
        var tvariable4 = $("#variable4 option:selected").text(); //$("#variable4").val();

        
        var titulos = "<tr class='fondoheader'>";

        var resultado = evt;

        //console.log(evt);

        var resultado_reporte_general = evt.reporte;
        var resultado_total = evt.total;
        //console.log(resultado_total);
        var resultado_reporte_buscador = evt.reporte_buscador;
        var resultado_ingreso = evt.ingreso;
        var resultado_ingreso_suma = evt.ingreso_suma;
        var resultado_ingreso_resta = evt.ingreso_resta;
        var resultado_egreso = evt.egreso;
        var resultado_egreso_socaspro = evt.egreso_socaspro;
        var resultado_egreso_pendiente = evt.egreso_pendiente;
        var resultado_detalle_egreso = evt.detalle_egreso;

        var resultado_reporte = resultado_reporte_general;

        var texto_buscado = $("#buscarProveedorPersonaldeApoyo").val();

        var num_var = 0;
        var num_var_sum = 0;

        var tit_var = [];

        
        if(texto_buscado != ""){
            var resultado_reporte = resultado_reporte_buscador;
        }
        

        $("#tablaReportePresupuesto thead").html("");
        $("#tablaReportePresupuesto tbody").html("");

        campos_reporte_titulo.push("#");   campos_reporte_corto.push("#");
        campos_reporte_titulo.push("-");   campos_reporte_corto.push("-");
        //titulos = titulos + "<th>#</th><th>-</th>";

        //if ( corregirNull(resultado_reporte).length > 1){
            //if(variable1 != 0){ titulos = titulos + "<th>" + variable1.toUpperCase() + "</th>"; }
            //if(variable2 != 0){ titulos = titulos + "<th>" + variable2.toUpperCase() + "</th>"; }
            //if(variable3 != 0){ titulos = titulos + "<th>" + variable3.toUpperCase() + "</th>"; }
            //if(variable4 != 0){ titulos = titulos + "<th>" + variable4.toUpperCase() + "</th>"; }

        if(variable1 != 0 || variable2 != 0 || variable3 != 0 || variable4 != 0){
            if(variable1 != 0){ campos_reporte_titulo.push( tvariable1.toUpperCase() ); campos_reporte_corto.push( tvariable1.toUpperCase() ); num_var = num_var + 1; tit_var.push( tvariable1.toUpperCase() ); }
            if(variable2 != 0){ campos_reporte_titulo.push( tvariable2.toUpperCase() ); campos_reporte_corto.push( tvariable2.toUpperCase() ); num_var = num_var + 1; tit_var.push( tvariable2.toUpperCase() ); }
            if(variable3 != 0){ campos_reporte_titulo.push( tvariable3.toUpperCase() ); campos_reporte_corto.push( tvariable3.toUpperCase() ); num_var = num_var + 1; tit_var.push( tvariable3.toUpperCase() ); }
            if(variable4 != 0){ campos_reporte_titulo.push( tvariable4.toUpperCase() ); campos_reporte_corto.push( tvariable4.toUpperCase() ); num_var = num_var + 1; tit_var.push( tvariable4.toUpperCase() ); }            
        }

        num_var_sum = num_var + 2;

        if(variable1 == 0 && variable2 == 0 && variable3 == 0 && variable4 == 0){
            campos_reporte_titulo.push("DIRECCION");   campos_reporte_corto.push("DIRECCION");
        }

        //MESES
        var mes_trimestre_1 = $("#chk_q1mes_presupuesto:checked")["length"];
        if(mes_trimestre_1 > 0){
            campos_reporte_titulo.push("ENE"); campos_reporte_corto.push("01");
            campos_reporte_titulo.push("FEB"); campos_reporte_corto.push("02");
            campos_reporte_titulo.push("MAR"); campos_reporte_corto.push("03");
        }

        var trimestre_1 = $("#chk_q1_presupuesto:checked")["length"];
        if(trimestre_1 > 0){
            campos_reporte_titulo.push("Q1");  campos_reporte_corto.push("Q1");
        }

        var mes_trimestre_2 = $("#chk_q2mes_presupuesto:checked")["length"];
        if(mes_trimestre_2 > 0){
            campos_reporte_titulo.push("ABR"); campos_reporte_corto.push("04");
            campos_reporte_titulo.push("MAY"); campos_reporte_corto.push("05");
            campos_reporte_titulo.push("JUN"); campos_reporte_corto.push("06");
        }

        var trimestre_2 = $("#chk_q2_presupuesto:checked")["length"];
        if(trimestre_2 > 0){
            campos_reporte_titulo.push("Q2");  campos_reporte_corto.push("Q2");
        }

        var mes_trimestre_3 = $("#chk_q3mes_presupuesto:checked")["length"];
        if(mes_trimestre_3 > 0){
            campos_reporte_titulo.push("JUL"); campos_reporte_corto.push("07");
            campos_reporte_titulo.push("AGO"); campos_reporte_corto.push("08");
            campos_reporte_titulo.push("SEP"); campos_reporte_corto.push("09");
        }

        var trimestre_3 = $("#chk_q3_presupuesto:checked")["length"];
        if(trimestre_3 > 0){
            campos_reporte_titulo.push("Q3");  campos_reporte_corto.push("Q3");
        }

        var mes_trimestre_4 = $("#chk_q4mes_presupuesto:checked")["length"];
        if(mes_trimestre_4 > 0){
            campos_reporte_titulo.push("OCT"); campos_reporte_corto.push("10");
            campos_reporte_titulo.push("NOV"); campos_reporte_corto.push("11");
            campos_reporte_titulo.push("DIC"); campos_reporte_corto.push("12");
        }

        var trimestre_4 = $("#chk_q4_presupuesto:checked")["length"];
        if(trimestre_4 > 0){
            campos_reporte_titulo.push("Q4");  campos_reporte_corto.push("Q4");
        }
        //FIN MESES





        campos_reporte_titulo.push("TOTAL_YEAR"); campos_reporte_corto.push("TOTAL_YEAR");

        //CARGAR LOS TITULOS DE LA TABLA

        if ( campos_reporte_titulo == undefined ) return;

        var fila = $("<tr class='cheader'>");


        for(var t=0; t<campos_reporte_titulo.length ; t++){

            if(t>1 && (num_var_sum - t) > 0){
                clase = tit_var[ (num_var - (num_var_sum - t)) ];
            }else{
                if(num_var_sum == 2){
                    clase = "direccion";
                }else{
                    clase = "mes";
                }
            }
            if(t==0) clase = "n";
            if(t==1) clase = "d";
            if(campos_reporte_titulo[t] == "TOTAL_YEAR") clase = "total_year";


            fila.append('<th class="'+clase+'">'+ campos_reporte_titulo[t] +'</th>');

            //$("#tablaReportePresupuesto thead").append(fila);

        }

        clase = "";

        $("#tablaReportePresupuesto thead").append(fila);

        //FINAL DE LA CARGA DE TITULOS

        //CARGA DE INGRESOS
        var registro = "";
        //var codigos_total = getLista (resultado_total,"concatenado");
        //var codigos_ingreso = getLista (resultado_ingreso,"concatenado");
        //var codigos_egreso = getLista (resultado_egreso,"concatenado");


        var objeto = new Object()
            objeto.variable1 = $("select#variable1").val();
            objeto.variable2 = $("select#variable2").val();
            objeto.variable3 = $("select#variable3").val();
            objeto.variable4 = $("select#variable4").val();
            objeto.subvariable1 = $("select#subvariable1").val();
            objeto.subvariable2 = $("select#subvariable2").val();
            objeto.subvariable3 = $("select#subvariable3").val();
            objeto.subvariable4 = $("select#subvariable4").val();
            objeto.anio = nanio;
            objeto.texto = $("#buscarProveedorPersonaldeApoyo").val();

            objeto.q1 = $("#chk_q1_presupuesto:checked")["length"];
            objeto.q2 = $("#chk_q2_presupuesto:checked")["length"];
            objeto.q3 = $("#chk_q3_presupuesto:checked")["length"];
            objeto.q4 = $("#chk_q4_presupuesto:checked")["length"];

            objeto.mq1 = $("#chk_q1mes_presupuesto:checked")["length"];
            objeto.mq2 = $("#chk_q2mes_presupuesto:checked")["length"];
            objeto.mq3 = $("#chk_q3mes_presupuesto:checked")["length"];
            objeto.mq4 = $("#chk_q4mes_presupuesto:checked")["length"];

        
            //console.log(resultado_reporte);
        for(var rp=0; rp<corregirNull(resultado_reporte).length ; rp++){
            var codigo_reporte = resultado_reporte[rp].concatenado;
            //console.log(codigo_reporte);
            //console.log(resultado_total);
            //console.log(resultado_ingreso);
            //console.log(resultado_ingreso_suma);
            //console.log(resultado_ingreso_resta);

            //CARGAR CODIGOS DE INGRESO A ARREGLO
                var posTotal = buscarCodigo(codigo_reporte,corregirNull(resultado_total));
                var posIngreso = buscarCodigo(codigo_reporte,corregirNull(resultado_ingreso));
                var posIngresoSuma = buscarCodigo(codigo_reporte,corregirNull(resultado_ingreso_suma));
                var posIngresoResta = buscarCodigo(codigo_reporte,corregirNull(resultado_ingreso_resta));


                if(posIngreso != undefined){

                    registro = ""; //DEJAR EN BLANCO EL REGISTRO PARA LLENARLO CON OTROS VALORES

                    registro = registro + (rp + 1);
                    registro = registro + "|||I";

                    if(variable1 != ""){
                        registro = registro + "|||" + resultado_ingreso[posIngreso][svariable1].toUpperCase(); 
                    }
                    if(variable2 != ""){
                        registro = registro + "|||" + resultado_ingreso[posIngreso][svariable2].toUpperCase(); 
                    }
                    if(variable3 != ""){
                        registro = registro + "|||" + resultado_ingreso[posIngreso][svariable3].toUpperCase(); 
                    }
                    if(variable4 != ""){
                        registro = registro + "|||" + resultado_ingreso[posIngreso][svariable4].toUpperCase();
                    }
                    if(variable1 == "" && variable2 == "" && variable3 == "" && variable4 == ""){
                        registro = registro + "|||" + resultado_ingreso[posIngreso]["direccion"];  
                    }

                    if(mes_trimestre_1 > 0){
                        ingreso_01 = corregirNumeroNull(resultado_ingreso[posIngreso]["01"]);
                        ingreso_02 = corregirNumeroNull(resultado_ingreso[posIngreso]["02"]);
                        ingreso_03 = corregirNumeroNull(resultado_ingreso[posIngreso]["03"]);

                        ingreso_01_ori = ingreso_01;
                        ingreso_02_ori = ingreso_02;
                        ingreso_03_ori = ingreso_03;

                        if(posIngresoSuma != undefined){
                            ingreso_01 = parseFloat(ingreso_01) + parseFloat(corregirNumeroNull(resultado_ingreso_suma[posIngresoSuma]["01"]));
                            ingreso_02 = parseFloat(ingreso_02) + parseFloat(corregirNumeroNull(resultado_ingreso_suma[posIngresoSuma]["02"]));
                            ingreso_03 = parseFloat(ingreso_03) + parseFloat(corregirNumeroNull(resultado_ingreso_suma[posIngresoSuma]["03"]));
                        }

                        if(posIngresoResta != undefined){
                            ingreso_01 = parseFloat(ingreso_01) - parseFloat(corregirNumeroNull(resultado_ingreso_resta[posIngresoResta]["01"]));
                            ingreso_02 = parseFloat(ingreso_02) - parseFloat(corregirNumeroNull(resultado_ingreso_resta[posIngresoResta]["02"]));
                            ingreso_03 = parseFloat(ingreso_03) - parseFloat(corregirNumeroNull(resultado_ingreso_resta[posIngresoResta]["03"]));
                        }

                        if(ingreso_01_ori != ingreso_01){
                            registro = registro + "|||<p onClick=\'detallePresupuestoTransferencia(\""+codigo_reporte+"|01\");\'>" + formatNumber.new(ingreso_01.toFixed(decimales), "") + " *</p>";
                        }else{
                            registro = registro + "|||" + ingreso_01;
                        }
                        if(ingreso_02_ori != ingreso_02){
                            registro = registro + "|||<p onClick=\'detallePresupuestoTransferencia(\""+codigo_reporte+"|02\");\'>" + formatNumber.new(ingreso_02.toFixed(decimales), "") + " *</p>";
                        }else{
                            registro = registro + "|||" + ingreso_02;
                        }
                        if(ingreso_03_ori != ingreso_03){
                            registro = registro + "|||<p onClick=\'detallePresupuestoTransferencia(\""+codigo_reporte+"|03\");\'>" + formatNumber.new(ingreso_03.toFixed(decimales), "") + " *</p>";
                        }else{
                            registro = registro + "|||" + ingreso_03;
                        }
                        //registro = registro + "|||" + ingreso_01;
                        //registro = registro + "|||" + ingreso_02;
                        //registro = registro + "|||" + ingreso_03;
                    }

                    if(trimestre_1 > 0){
                        ingreso_q1 = corregirNumeroNull(resultado_ingreso[posIngreso]["Q1"]);

                        if(posIngresoSuma != undefined){
                            ingreso_q1 = parseFloat(ingreso_q1) + parseFloat(corregirNumeroNull(resultado_ingreso_suma[posIngresoSuma]["Q1"]));
                        }

                        if(posIngresoResta != undefined){
                            ingreso_q1 = parseFloat(ingreso_q1) - parseFloat(corregirNumeroNull(resultado_ingreso_resta[posIngresoResta]["Q1"]));
                        }

                        registro = registro + "|||" + ingreso_q1;
                    }

                    if(mes_trimestre_2 > 0){
                        ingreso_04 = corregirNumeroNull(resultado_ingreso[posIngreso]["04"]);
                        ingreso_05 = corregirNumeroNull(resultado_ingreso[posIngreso]["05"]);
                        ingreso_06 = corregirNumeroNull(resultado_ingreso[posIngreso]["06"]);

                        ingreso_04_ori = ingreso_04;
                        ingreso_05_ori = ingreso_05;
                        ingreso_06_ori = ingreso_06;

                        if(posIngresoSuma != undefined){
                            ingreso_04 = parseFloat(ingreso_04) + parseFloat(corregirNumeroNull(resultado_ingreso_suma[posIngresoSuma]["04"]));
                            ingreso_05 = parseFloat(ingreso_05) + parseFloat(corregirNumeroNull(resultado_ingreso_suma[posIngresoSuma]["05"]));
                            ingreso_06 = parseFloat(ingreso_06) + parseFloat(corregirNumeroNull(resultado_ingreso_suma[posIngresoSuma]["06"]));
                        }

                        if(posIngresoResta != undefined){
                            ingreso_04 = parseFloat(ingreso_04) - parseFloat(corregirNumeroNull(resultado_ingreso_resta[posIngresoResta]["04"]));
                            ingreso_05 = parseFloat(ingreso_05) - parseFloat(corregirNumeroNull(resultado_ingreso_resta[posIngresoResta]["05"]));
                            ingreso_06 = parseFloat(ingreso_06) - parseFloat(corregirNumeroNull(resultado_ingreso_resta[posIngresoResta]["06"]));
                        }

                        if(ingreso_04_ori != ingreso_04){
                            registro = registro + "|||<p onClick=\'detallePresupuestoTransferencia(\""+codigo_reporte+"|04\");\'>" + formatNumber.new(ingreso_04.toFixed(decimales), "") + " *</p>";
                        }else{
                            registro = registro + "|||" + ingreso_04;
                        }
                        if(ingreso_05_ori != ingreso_05){
                            registro = registro + "|||<p onClick=\'detallePresupuestoTransferencia(\""+codigo_reporte+"|05\");\'>" + formatNumber.new(ingreso_05.toFixed(decimales), "") + " *</p>";
                        }else{
                            registro = registro + "|||" + ingreso_05;
                        }
                        if(ingreso_06_ori != ingreso_06){
                            registro = registro + "|||<p onClick=\'detallePresupuestoTransferencia(\""+codigo_reporte+"|06\");\'>" + formatNumber.new(ingreso_06.toFixed(decimales), "") + " *</p>";
                        }else{
                            registro = registro + "|||" + ingreso_06;
                        }

                    }

                    if(trimestre_2 > 0){
                        ingreso_q2 = corregirNumeroNull(resultado_ingreso[posIngreso]["Q2"]);

                        if(posIngresoSuma != undefined){
                            ingreso_q2 = parseFloat(ingreso_q2) + parseFloat(corregirNumeroNull(resultado_ingreso_suma[posIngresoSuma]["Q2"]));
                        }

                        if(posIngresoResta != undefined){
                            ingreso_q2 = parseFloat(ingreso_q2) - parseFloat(corregirNumeroNull(resultado_ingreso_resta[posIngresoResta]["Q2"]));
                        }

                        registro = registro + "|||" + ingreso_q2;
                    }

                    if(mes_trimestre_3 > 0){
                        ingreso_07 = corregirNumeroNull(resultado_ingreso[posIngreso]["07"]);
                        ingreso_08 = corregirNumeroNull(resultado_ingreso[posIngreso]["08"]);
                        ingreso_09 = corregirNumeroNull(resultado_ingreso[posIngreso]["09"]);

                        ingreso_07_ori = ingreso_07;
                        ingreso_08_ori = ingreso_08;
                        ingreso_09_ori = ingreso_09;

                        if(posIngresoSuma != undefined){
                            ingreso_07 = parseFloat(ingreso_07) + parseFloat(corregirNumeroNull(resultado_ingreso_suma[posIngresoSuma]["07"]));
                            ingreso_08 = parseFloat(ingreso_08) + parseFloat(corregirNumeroNull(resultado_ingreso_suma[posIngresoSuma]["08"]));
                            ingreso_09 = parseFloat(ingreso_09) + parseFloat(corregirNumeroNull(resultado_ingreso_suma[posIngresoSuma]["09"]));
                        }

                        if(posIngresoResta != undefined){
                            ingreso_07 = parseFloat(ingreso_07) - parseFloat(corregirNumeroNull(resultado_ingreso_resta[posIngresoResta]["07"]));
                            ingreso_08 = parseFloat(ingreso_08) - parseFloat(corregirNumeroNull(resultado_ingreso_resta[posIngresoResta]["08"]));
                            ingreso_09 = parseFloat(ingreso_09) - parseFloat(corregirNumeroNull(resultado_ingreso_resta[posIngresoResta]["09"]));
                        }

                        if(ingreso_07_ori != ingreso_07){
                            registro = registro + "|||<p onClick=\'detallePresupuestoTransferencia(\""+codigo_reporte+"|07\");\'>" + formatNumber.new(ingreso_07.toFixed(decimales), "") + " *</p>";
                        }else{
                            registro = registro + "|||" + ingreso_07;
                        }
                        if(ingreso_08_ori != ingreso_08){
                            registro = registro + "|||<p onClick=\'detallePresupuestoTransferencia(\""+codigo_reporte+"|08\");\'>" + formatNumber.new(ingreso_08.toFixed(decimales), "") + " *</p>";
                        }else{
                            registro = registro + "|||" + ingreso_08;
                        }
                        if(ingreso_09_ori != ingreso_09){
                            registro = registro + "|||<p onClick=\'detallePresupuestoTransferencia(\""+codigo_reporte+"|09\");\'>" + formatNumber.new(ingreso_09.toFixed(decimales), "") + " *</p>";
                        }else{
                            registro = registro + "|||" + ingreso_09;
                        }

                    }

                    if(trimestre_3 > 0){
                        ingreso_q3 = corregirNumeroNull(resultado_ingreso[posIngreso]["Q3"]);

                        if(posIngresoSuma != undefined){
                            ingreso_q3 = parseFloat(ingreso_q3) + parseFloat(corregirNumeroNull(resultado_ingreso_suma[posIngresoSuma]["Q3"]));
                        }

                        if(posIngresoResta != undefined){
                            ingreso_q3 = parseFloat(ingreso_q3) - parseFloat(corregirNumeroNull(resultado_ingreso_resta[posIngresoResta]["Q3"]));
                        }

                        registro = registro + "|||" + ingreso_q3;
                    }

                    if(mes_trimestre_4 > 0){
                        ingreso_10 = corregirNumeroNull(resultado_ingreso[posIngreso]["10"]);
                        ingreso_11 = corregirNumeroNull(resultado_ingreso[posIngreso]["11"]);
                        ingreso_12 = corregirNumeroNull(resultado_ingreso[posIngreso]["12"]);

                        ingreso_10_ori = ingreso_10;
                        ingreso_11_ori = ingreso_11;
                        ingreso_12_ori = ingreso_12;

                        if(posIngresoSuma != undefined){
                            ingreso_10 = parseFloat(ingreso_10) + parseFloat(corregirNumeroNull(resultado_ingreso_suma[posIngresoSuma]["10"]));
                            ingreso_11 = parseFloat(ingreso_11) + parseFloat(corregirNumeroNull(resultado_ingreso_suma[posIngresoSuma]["11"]));
                            ingreso_12 = parseFloat(ingreso_12) + parseFloat(corregirNumeroNull(resultado_ingreso_suma[posIngresoSuma]["12"]));
                        }

                        if(posIngresoResta != undefined){
                            ingreso_10 = parseFloat(ingreso_10) - parseFloat(corregirNumeroNull(resultado_ingreso_resta[posIngresoResta]["10"]));
                            ingreso_11 = parseFloat(ingreso_11) - parseFloat(corregirNumeroNull(resultado_ingreso_resta[posIngresoResta]["11"]));
                            ingreso_12 = parseFloat(ingreso_12) - parseFloat(corregirNumeroNull(resultado_ingreso_resta[posIngresoResta]["12"]));
                        }

                        if(ingreso_10_ori != ingreso_10){
                            registro = registro + "|||<p onClick=\'detallePresupuestoTransferencia(\""+codigo_reporte+"|10\");\'>" + formatNumber.new(ingreso_10.toFixed(decimales), "") + " *</p>";
                        }else{
                            registro = registro + "|||" + ingreso_10;
                        }
                        if(ingreso_11_ori != ingreso_11){
                            registro = registro + "|||<p onClick=\'detallePresupuestoTransferencia(\""+codigo_reporte+"|11\");\'>" + formatNumber.new(ingreso_11.toFixed(decimales), "") + " *</p>";
                        }else{
                            registro = registro + "|||" + ingreso_11;
                        }
                        if(ingreso_12_ori != ingreso_12){
                            registro = registro + "|||<p onClick=\'detallePresupuestoTransferencia(\""+codigo_reporte+"|12\");\'>" + formatNumber.new(ingreso_12.toFixed(decimales), "") + " *</p>";
                        }else{
                            registro = registro + "|||" + ingreso_12;
                        }
                    }

                    if(trimestre_4 > 0){
                        ingreso_q4 = corregirNumeroNull(resultado_ingreso[posIngreso]["Q4"]);

                        if(posIngresoSuma != undefined){
                            ingreso_q4 = parseFloat(ingreso_q4) + parseFloat(corregirNumeroNull(resultado_ingreso_suma[posIngresoSuma]["Q4"]));
                        }

                        if(posIngresoResta != undefined){
                            ingreso_q4 = parseFloat(ingreso_q4) - parseFloat(corregirNumeroNull(resultado_ingreso_resta[posIngresoResta]["Q4"]));
                        }

                        registro = registro + "|||" + ingreso_q4;

                    }

                    ingreso_total = corregirNumeroNull(resultado_total[posTotal]["total_registro"]);

                        if(posIngresoSuma != undefined){
                            ingreso_total = parseFloat(ingreso_total) + parseFloat(corregirNumeroNull(resultado_ingreso_suma[posIngresoSuma]["total_registro"]));
                        }

                        if(posIngresoResta != undefined){
                            ingreso_total = parseFloat(ingreso_total) - parseFloat(corregirNumeroNull(resultado_ingreso_resta[posIngresoResta]["total_registro"]));
                        }

                        //if(posIngresoSuma != undefined || posIngresoResta != undefined){
                        //    registro = registro + "|||" + formatNumber.new(parseFloat(ingreso_total.toFixed(decimales), "")) +"-(T)";
                        //}else{
                            registro = registro + "|||" + ingreso_total;
                        //}

                    
                    campos_reporte.push({registro: registro});

                }else{

                    registro = ""; //DEJAR EN BLANCO EL REGISTRO PARA LLENARLO CON OTROS VALORES

                    registro = registro + (rp + 1);
                    registro = registro + "|||I";

                    if(variable1 != ""){
                        registro = registro + "|||" + resultado_reporte[rp][svariable1].toUpperCase();
                    }
                    if(variable2 != ""){
                        registro = registro + "|||" + resultado_reporte[rp][svariable2].toUpperCase();
                    }
                    if(variable3 != ""){
                        registro = registro + "|||" + resultado_reporte[rp][svariable3].toUpperCase();
                    }
                    if(variable4 != ""){
                        registro = registro + "|||" + resultado_reporte[rp][svariable4].toUpperCase();
                    }
                    if(variable1 == "" && variable2 == "" && variable3 == "" && variable4 == ""){
                        registro = registro + "|||" + resultado_reporte[rp]["direccion"];  
                    }

                    if(mes_trimestre_1 > 0){
                        ingreso_01 = corregirNumeroNull(0);
                        ingreso_02 = corregirNumeroNull(0);
                        ingreso_03 = corregirNumeroNull(0);
                        registro = registro + "|||" + ingreso_01;
                        registro = registro + "|||" + ingreso_02;
                        registro = registro + "|||" + ingreso_03;
                    }

                    if(trimestre_1 > 0){
                        ingreso_q1 = corregirNumeroNull(0);
                        registro = registro + "|||" + ingreso_q1;
                    }

                    if(mes_trimestre_2 > 0){
                        ingreso_04 = corregirNumeroNull(0);
                        ingreso_05 = corregirNumeroNull(0);
                        ingreso_06 = corregirNumeroNull(0);
                        registro = registro + "|||" + ingreso_04;
                        registro = registro + "|||" + ingreso_05;
                        registro = registro + "|||" + ingreso_06;
                    }

                    if(trimestre_2 > 0){
                        ingreso_q2 = corregirNumeroNull(0);
                        registro = registro + "|||" + ingreso_q2;
                    }

                    if(mes_trimestre_3 > 0){
                        ingreso_07 = corregirNumeroNull(0);
                        ingreso_08 = corregirNumeroNull(0);
                        ingreso_09 = corregirNumeroNull(0);
                        registro = registro + "|||" + ingreso_07;
                        registro = registro + "|||" + ingreso_08;
                        registro = registro + "|||" + ingreso_09;
                    }

                    if(trimestre_3 > 0){
                        ingreso_q3 = corregirNumeroNull(0);
                        registro = registro + "|||" + ingreso_q3;
                    }

                    if(mes_trimestre_4 > 0){
                        ingreso_10 = corregirNumeroNull(0);
                        ingreso_11 = corregirNumeroNull(0);
                        ingreso_12 = corregirNumeroNull(0);
                        registro = registro + "|||" + ingreso_10;
                        registro = registro + "|||" + ingreso_11;
                        registro = registro + "|||" + ingreso_12;
                    }

                    if(trimestre_4 > 0){
                        ingreso_q4 = corregirNumeroNull(0);
                        registro = registro + "|||" + ingreso_q4;
                    }

                    //if(resultado_total[posTotal]["total_registro"] == undefined){
                    ingreso_total = 0;
                    //}else{
                    //ingreso_total = corregirNumeroNull(resultado_total[posTotal]["total_registro"]);
                    //}

                    
                    registro = registro + "|||" + ingreso_total;

                    campos_reporte.push({registro: registro});

                }
            //FINAL

            //CARGAR CODIGOS DE DETALLE DE EGRESO

            for(var rde=0; rde<corregirNumeroNull(resultado_detalle_egreso).length ; rde++){
                var datos = resultado_detalle_egreso[rde];
                if(codigo_reporte == resultado_detalle_egreso[rde].concatenado){

                    registro = ""; //DEJAR EN BLANCO EL REGISTRO PARA LLENARLO CON OTROS VALORES

                    

                    if(resultado_detalle_egreso[rde].estado == "APROBADO"){ estado = "DE"; }
                    if(resultado_detalle_egreso[rde].estado == "POR APROBAR"){ estado = "DP"; }
                    if(resultado_detalle_egreso[rde].estado == "RECHAZADO"){ estado = "DR"; }
                    if(resultado_detalle_egreso[rde].estado == "CANCELADO"){ estado = "DC"; }

                    registro = registro + "";
                    registro = registro + "|||" + estado;

                    var descripcion = "CODIGO : "+datos.idPresupuesto+" | DOCUMENTO : "+datos.documento+" | "+datos.nombre_razon_social;
                    var monto_oep = resultado_detalle_egreso[rde]["monto_oep"];
                    if(corregirNull(datos.idFormato) == "SOCA"){
                        descripcion = "SOLICITUD ORDEN DE COMPRA ABIERTA | CODIGO : "+datos.idPresupuesto+" | "+datos.nombre_razon_social;
                    }
                    if(corregirNull(datos.idFormato) == "SPRO"){
                        descripcion = "SOLICITUD PROVISION | CODIGO : "+datos.idPresupuesto;
                    }
                    if(corregirNull(datos.dni) != ""){
                        descripcion = datos.nombre+" - "+datos.documento;
                    }

                    if(variable1 != "" && variable2 == "" && variable3 == "" && variable4 == ""){
                        registro = registro + "|||" + descripcion.toUpperCase();
                    }
                    if(variable1 != "" && variable2 != "" && variable3 == "" && variable4 == ""){
                        registro = registro + "||| |||" + descripcion.toUpperCase();
                    }
                    if(variable1 != "" && variable2 != "" && variable3 != "" && variable4 == ""){
                        registro = registro + "||| ||| |||" + descripcion.toUpperCase();
                    }
                    if(variable1 != "" && variable2 != "" && variable3 != "" && variable4 != ""){
                        registro = registro + "||| ||| ||| |||" + descripcion.toUpperCase();
                    }
                    if(variable1 == "" && variable2 == "" && variable3 == "" && variable4 == ""){
                        registro = registro + "|||" + descripcion.toUpperCase();
                    }

                    if(mes_trimestre_1 > 0){
                        detalle_egreso_01 = corregirNumeroNull(resultado_detalle_egreso[rde]["01"]);
                        detalle_egreso_02 = corregirNumeroNull(resultado_detalle_egreso[rde]["02"]);
                        detalle_egreso_03 = corregirNumeroNull(resultado_detalle_egreso[rde]["03"]);
                        if(detalle_egreso_01 > 0 && corregirNull(datos.dni) != ""){ detalle_egreso_01 = corregirNumeroNull(monto_oep); }
                        if(detalle_egreso_02 > 0 && corregirNull(datos.dni) != ""){ detalle_egreso_02 = corregirNumeroNull(monto_oep); }
                        if(detalle_egreso_03 > 0 && corregirNull(datos.dni) != ""){ detalle_egreso_03 = corregirNumeroNull(monto_oep); }                  
                        registro = registro + "|||" + detalle_egreso_01;
                        registro = registro + "|||" + detalle_egreso_02;
                        registro = registro + "|||" + detalle_egreso_03;
                    }

                    if(trimestre_1 > 0){
                        detalle_egreso_q1 = corregirNumeroNull(resultado_detalle_egreso[rde]["Q1"]);
                        if(detalle_egreso_q1 > 0 && corregirNull(datos.dni) != ""){ detalle_egreso_q1 = corregirNumeroNull(monto_oep); }
                        registro = registro + "|||" + detalle_egreso_q1;
                    }

                    if(mes_trimestre_2 > 0){
                        detalle_egreso_04 = corregirNumeroNull(resultado_detalle_egreso[rde]["04"]);
                        detalle_egreso_05 = corregirNumeroNull(resultado_detalle_egreso[rde]["05"]);
                        detalle_egreso_06 = corregirNumeroNull(resultado_detalle_egreso[rde]["06"]);
                        if(detalle_egreso_04 > 0 && corregirNull(datos.dni) != ""){ detalle_egreso_04 = corregirNumeroNull(monto_oep); }
                        if(detalle_egreso_05 > 0 && corregirNull(datos.dni) != ""){ detalle_egreso_05 = corregirNumeroNull(monto_oep); }
                        if(detalle_egreso_06 > 0 && corregirNull(datos.dni) != ""){ detalle_egreso_06 = corregirNumeroNull(monto_oep); } 
                        registro = registro + "|||" + detalle_egreso_04;
                        registro = registro + "|||" + detalle_egreso_05;
                        registro = registro + "|||" + detalle_egreso_06;
                    }

                    if(trimestre_2 > 0){
                        detalle_egreso_q2 = corregirNumeroNull(resultado_detalle_egreso[rde]["Q2"]);
                        if(detalle_egreso_q2 > 0 && corregirNull(datos.dni) != ""){ detalle_egreso_q2 = corregirNumeroNull(monto_oep); }
                        registro = registro + "|||" + detalle_egreso_q2;
                    }

                    if(mes_trimestre_3 > 0){
                        detalle_egreso_07 = corregirNumeroNull(resultado_detalle_egreso[rde]["07"]);
                        detalle_egreso_08 = corregirNumeroNull(resultado_detalle_egreso[rde]["08"]);
                        detalle_egreso_09 = corregirNumeroNull(resultado_detalle_egreso[rde]["09"]);
                        if(detalle_egreso_07 > 0 && corregirNull(datos.dni) != ""){ detalle_egreso_07 = corregirNumeroNull(monto_oep); }
                        if(detalle_egreso_08 > 0 && corregirNull(datos.dni) != ""){ detalle_egreso_08 = corregirNumeroNull(monto_oep); }
                        if(detalle_egreso_09 > 0 && corregirNull(datos.dni) != ""){ detalle_egreso_09 = corregirNumeroNull(monto_oep); } 
                        registro = registro + "|||" + detalle_egreso_07;
                        registro = registro + "|||" + detalle_egreso_08;
                        registro = registro + "|||" + detalle_egreso_09;
                    }

                    if(trimestre_3 > 0){
                        detalle_egreso_q3 = corregirNumeroNull(resultado_detalle_egreso[rde]["Q3"]);
                        if(detalle_egreso_q3 > 0 && corregirNull(datos.dni) != ""){ detalle_egreso_q3 = corregirNumeroNull(monto_oep); }
                        registro = registro + "|||" + detalle_egreso_q3;
                    }

                    if(mes_trimestre_4 > 0){
                        detalle_egreso_10 = corregirNumeroNull(resultado_detalle_egreso[rde]["10"]);
                        detalle_egreso_11 = corregirNumeroNull(resultado_detalle_egreso[rde]["11"]);
                        detalle_egreso_12 = corregirNumeroNull(resultado_detalle_egreso[rde]["12"]);
                        if(detalle_egreso_10 > 0 && corregirNull(datos.dni) != ""){ detalle_egreso_10 = corregirNumeroNull(monto_oep); }
                        if(detalle_egreso_11 > 0 && corregirNull(datos.dni) != ""){ detalle_egreso_11 = corregirNumeroNull(monto_oep); }
                        if(detalle_egreso_12 > 0 && corregirNull(datos.dni) != ""){ detalle_egreso_12 = corregirNumeroNull(monto_oep); } 
                        registro = registro + "|||" + detalle_egreso_10;
                        registro = registro + "|||" + detalle_egreso_11;
                        registro = registro + "|||" + detalle_egreso_12;
                    }

                    if(trimestre_4 > 0){
                        detalle_egreso_q4 = corregirNumeroNull(resultado_detalle_egreso[rde]["Q4"]);
                        if(detalle_egreso_q4 > 0 && corregirNull(datos.dni) != ""){ detalle_egreso_q4 = corregirNumeroNull(monto_oep); }
                        registro = registro + "|||" + detalle_egreso_q4;
                    }

                    detalle_egreso_total = corregirNumeroNull(resultado_detalle_egreso[rde]["total_registro"]);
                    if(detalle_egreso_total > 0 && corregirNull(datos.dni) != ""){ detalle_egreso_total = corregirNumeroNull(monto_oep); }
                    registro = registro + "|||" + detalle_egreso_total;

                    //console.log( registro );

                    campos_reporte.push({registro: registro});

                    //FINAL DETALLE 
                }

            }

            //FINAL DE LA CARGA



            //CARGAR CODIGOS DE EGRESO A ARREGLO
                var valorAjuste = 0;
                var posEgreso = buscarCodigo(codigo_reporte,corregirNull(resultado_egreso));
                /*
                var posEgresoSOCASPRO = buscarCodigo(codigo_reporte,corregirNull(resultado_egreso_socaspro));
                if(posEgresoSOCASPRO != undefined){
                    valorAjuste = parseFloat(resultado_egreso_socaspro[posEgresoSOCASPRO]["total_registro"]);
                    console.log(valorAjuste);
                }
                */
                if(posEgreso != undefined){

                    registro = ""; //DEJAR EN BLANCO EL REGISTRO PARA LLENARLO CON OTROS VALORES

                    registro = registro + "";
                    registro = registro + "|||E";

                    if(variable1 != ""){
                        registro = registro + "|||" + resultado_egreso[posEgreso][svariable1].toUpperCase();
                    }
                    if(variable2 != ""){
                        registro = registro + "|||" + resultado_egreso[posEgreso][svariable2].toUpperCase();
                    }
                    if(variable3 != ""){
                        registro = registro + "|||" + resultado_egreso[posEgreso][svariable3].toUpperCase();
                    }
                    if(variable4 != ""){
                        registro = registro + "|||" + resultado_egreso[posEgreso][svariable4].toUpperCase();
                    }
                    if(variable1 == "" && variable2 == "" && variable3 == "" && variable4 == ""){
                        registro = registro + "|||" + resultado_egreso[posEgreso]["direccion"];  
                    }

                    if(mes_trimestre_1 > 0){
                        egreso_01 = corregirNumeroNull(resultado_egreso[posEgreso]["01"]);
                        egreso_02 = corregirNumeroNull(resultado_egreso[posEgreso]["02"]);
                        egreso_03 = corregirNumeroNull(resultado_egreso[posEgreso]["03"]);
                        registro = registro + "|||" + egreso_01;
                        registro = registro + "|||" + egreso_02;
                        registro = registro + "|||" + egreso_03;
                    }

                    if(trimestre_1 > 0){
                        egreso_q1 = corregirNumeroNull(resultado_egreso[posEgreso]["Q1"]);
                        registro = registro + "|||" + egreso_q1;
                    }

                    if(mes_trimestre_2 > 0){
                        egreso_04 = corregirNumeroNull(resultado_egreso[posEgreso]["04"]);
                        egreso_05 = corregirNumeroNull(resultado_egreso[posEgreso]["05"]);
                        egreso_06 = corregirNumeroNull(resultado_egreso[posEgreso]["06"]);
                        registro = registro + "|||" + egreso_04;
                        registro = registro + "|||" + egreso_05;
                        registro = registro + "|||" + egreso_06;
                    }

                    if(trimestre_2 > 0){
                        egreso_q2 = corregirNumeroNull(resultado_egreso[posEgreso]["Q2"]);
                        registro = registro + "|||" + egreso_q2;
                    }

                    if(mes_trimestre_3 > 0){
                        egreso_07 = corregirNumeroNull(resultado_egreso[posEgreso]["07"]);
                        egreso_08 = corregirNumeroNull(resultado_egreso[posEgreso]["08"]);
                        egreso_09 = corregirNumeroNull(resultado_egreso[posEgreso]["09"]);
                        registro = registro + "|||" + egreso_07;
                        registro = registro + "|||" + egreso_08;
                        registro = registro + "|||" + egreso_09;
                    }

                    if(trimestre_3 > 0){
                        egreso_q3 = corregirNumeroNull(resultado_egreso[posEgreso]["Q3"]);
                        registro = registro + "|||" + egreso_q3;
                    }

                    if(mes_trimestre_4 > 0){
                        egreso_10 = corregirNumeroNull(resultado_egreso[posEgreso]["10"]);
                        egreso_11 = corregirNumeroNull(resultado_egreso[posEgreso]["11"]);
                        egreso_12 = corregirNumeroNull(resultado_egreso[posEgreso]["12"]);
                        registro = registro + "|||" + egreso_10;
                        registro = registro + "|||" + egreso_11;
                        registro = registro + "|||" + egreso_12;
                    }

                    if(trimestre_4 > 0){
                        egreso_q4 = corregirNumeroNull(resultado_egreso[posEgreso]["Q4"]);
                        registro = registro + "|||" + egreso_q4;
                    }

                    egreso_total = corregirNumeroNull(resultado_egreso[posEgreso]["total_registro"]);
                    registro = registro + "|||" + egreso_total;

                    campos_reporte.push({registro: registro});

                }else{

                    registro = ""; //DEJAR EN BLANCO EL REGISTRO PARA LLENARLO CON OTROS VALORES

                    registro = registro + "";
                    registro = registro + "|||E";

                    if(variable1 != ""){
                        registro = registro + "|||" + resultado_reporte[rp][svariable1].toUpperCase();
                    }
                    if(variable2 != ""){
                        registro = registro + "|||" + resultado_reporte[rp][svariable2].toUpperCase();
                    }
                    if(variable3 != ""){
                        registro = registro + "|||" + resultado_reporte[rp][svariable3].toUpperCase();
                    }
                    if(variable4 != ""){
                        registro = registro + "|||" + resultado_reporte[rp][svariable4].toUpperCase();
                    }
                    if(variable1 == "" && variable2 == "" && variable3 == "" && variable4 == ""){
                        registro = registro + "|||" + resultado_reporte[rp]["direccion"];  
                    }

                    if(mes_trimestre_1 > 0){
                        egreso_01 = corregirNumeroNull(0);
                        egreso_02 = corregirNumeroNull(0);
                        egreso_03 = corregirNumeroNull(0);
                        registro = registro + "|||" + egreso_01;
                        registro = registro + "|||" + egreso_02;
                        registro = registro + "|||" + egreso_03;
                    }

                    if(trimestre_1 > 0){
                        egreso_q1 = corregirNumeroNull(0);
                        registro = registro + "|||" + egreso_q1;
                    }

                    if(mes_trimestre_2 > 0){
                        egreso_04 = corregirNumeroNull(0);
                        egreso_05 = corregirNumeroNull(0);
                        egreso_06 = corregirNumeroNull(0);
                        registro = registro + "|||" + egreso_04;
                        registro = registro + "|||" + egreso_05;
                        registro = registro + "|||" + egreso_06;
                    }

                    if(trimestre_2 > 0){
                        egreso_q2 = corregirNumeroNull(0);
                        registro = registro + "|||" + egreso_q2;
                    }

                    if(mes_trimestre_3 > 0){
                        egreso_07 = corregirNumeroNull(0);
                        egreso_08 = corregirNumeroNull(0);
                        egreso_09 = corregirNumeroNull(0);
                        registro = registro + "|||" + egreso_07;
                        registro = registro + "|||" + egreso_08;
                        registro = registro + "|||" + egreso_09;
                    }

                    if(trimestre_3 > 0){
                        egreso_q3 = corregirNumeroNull(0);
                        registro = registro + "|||" + egreso_q3;
                    }

                    if(mes_trimestre_4 > 0){
                        egreso_10 = corregirNumeroNull(0);
                        egreso_11 = corregirNumeroNull(0);
                        egreso_12 = corregirNumeroNull(0);
                        registro = registro + "|||" + egreso_10;
                        registro = registro + "|||" + egreso_11;
                        registro = registro + "|||" + egreso_12;
                    }

                    if(trimestre_4 > 0){
                        egreso_q4 = corregirNumeroNull(0);
                        registro = registro + "|||" + egreso_q4;
                    }

                    egreso_total = corregirNumeroNull(0);
                    registro = registro + "|||" + egreso_total;

                    campos_reporte.push({registro: registro});

                }
            //FINAL DE PROCEDIMIENTO



            //CARGAR CODIGOS DE EGRESO PENDIENTE A ARREGLO
                var posEgresoPendiente = buscarCodigo(codigo_reporte,corregirNull(resultado_egreso_pendiente));
                if(posEgresoPendiente != undefined){

                    registro = ""; //DEJAR EN BLANCO EL REGISTRO PARA LLENARLO CON OTROS VALORES

                    registro = registro + "";
                    registro = registro + "|||P";

                    if(variable1 != ""){
                        registro = registro + "|||" + resultado_egreso_pendiente[posEgresoPendiente][svariable1].toUpperCase();
                    }
                    if(variable2 != ""){
                        registro = registro + "|||" + resultado_egreso_pendiente[posEgresoPendiente][svariable2].toUpperCase();
                    }
                    if(variable3 != ""){
                        registro = registro + "|||" + resultado_egreso_pendiente[posEgresoPendiente][svariable3].toUpperCase();
                    }
                    if(variable4 != ""){
                        registro = registro + "|||" + resultado_egreso_pendiente[posEgresoPendiente][svariable4].toUpperCase();
                    }
                    if(variable1 == "" && variable2 == "" && variable3 == "" && variable4 == ""){
                        registro = registro + "|||" + resultado_egreso_pendiente[posEgresoPendiente]["direccion"];  
                    }

                    if(mes_trimestre_1 > 0){
                        egreso_pendiente_01 = corregirNumeroNull(resultado_egreso_pendiente[posEgresoPendiente]["01"]);
                        egreso_pendiente_02 = corregirNumeroNull(resultado_egreso_pendiente[posEgresoPendiente]["02"]);
                        egreso_pendiente_03 = corregirNumeroNull(resultado_egreso_pendiente[posEgresoPendiente]["03"]);
                        registro = registro + "|||" + egreso_pendiente_01;
                        registro = registro + "|||" + egreso_pendiente_02;
                        registro = registro + "|||" + egreso_pendiente_03;
                    }

                    if(trimestre_1 > 0){
                        egreso_pendiente_q1 = corregirNumeroNull(resultado_egreso_pendiente[posEgresoPendiente]["Q1"]);
                        registro = registro + "|||" + egreso_pendiente_q1;
                    }

                    if(mes_trimestre_2 > 0){
                        egreso_pendiente_04 = corregirNumeroNull(resultado_egreso_pendiente[posEgresoPendiente]["04"]);
                        egreso_pendiente_05 = corregirNumeroNull(resultado_egreso_pendiente[posEgresoPendiente]["05"]);
                        egreso_pendiente_06 = corregirNumeroNull(resultado_egreso_pendiente[posEgresoPendiente]["06"]);
                        registro = registro + "|||" + egreso_pendiente_04;
                        registro = registro + "|||" + egreso_pendiente_05;
                        registro = registro + "|||" + egreso_pendiente_06;
                    }

                    if(trimestre_2 > 0){
                        egreso_pendiente_q2 = corregirNumeroNull(resultado_egreso_pendiente[posEgresoPendiente]["Q2"]);
                        registro = registro + "|||" + egreso_pendiente_q2;
                    }

                    if(mes_trimestre_3 > 0){
                        egreso_pendiente_07 = corregirNumeroNull(resultado_egreso_pendiente[posEgresoPendiente]["07"]);
                        egreso_pendiente_08 = corregirNumeroNull(resultado_egreso_pendiente[posEgresoPendiente]["08"]);
                        egreso_pendiente_09 = corregirNumeroNull(resultado_egreso_pendiente[posEgresoPendiente]["09"]);
                        registro = registro + "|||" + egreso_pendiente_07;
                        registro = registro + "|||" + egreso_pendiente_08;
                        registro = registro + "|||" + egreso_pendiente_09;
                    }

                    if(trimestre_3 > 0){
                        egreso_pendiente_q3 = corregirNumeroNull(resultado_egreso_pendiente[posEgresoPendiente]["Q3"]);
                        registro = registro + "|||" + egreso_pendiente_q3;
                    }

                    if(mes_trimestre_4 > 0){
                        egreso_pendiente_10 = corregirNumeroNull(resultado_egreso_pendiente[posEgresoPendiente]["10"]);
                        egreso_pendiente_11 = corregirNumeroNull(resultado_egreso_pendiente[posEgresoPendiente]["11"]);
                        egreso_pendiente_12 = corregirNumeroNull(resultado_egreso_pendiente[posEgresoPendiente]["12"]);
                        registro = registro + "|||" + egreso_pendiente_10;
                        registro = registro + "|||" + egreso_pendiente_11;
                        registro = registro + "|||" + egreso_pendiente_12;
                    }

                    if(trimestre_4 > 0){
                        egreso_pendiente_q4 = corregirNumeroNull(resultado_egreso_pendiente[posEgresoPendiente]["Q4"]);
                        registro = registro + "|||" + egreso_pendiente_q4;
                    }

                    egreso_pendiente_total = corregirNumeroNull(resultado_egreso_pendiente[posEgresoPendiente]["total_registro"]);
                    registro = registro + "|||" + egreso_pendiente_total;

                    campos_reporte.push({registro: registro});

                }else{

                    registro = ""; //DEJAR EN BLANCO EL REGISTRO PARA LLENARLO CON OTROS VALORES

                    registro = registro + "";
                    registro = registro + "|||P";

                    if(variable1 != ""){
                        registro = registro + "|||" + resultado_reporte[rp][svariable1].toUpperCase();
                    }
                    if(variable2 != ""){
                        registro = registro + "|||" + resultado_reporte[rp][svariable2].toUpperCase();
                    }
                    if(variable3 != ""){
                        registro = registro + "|||" + resultado_reporte[rp][svariable3].toUpperCase();
                    }
                    if(variable4 != ""){
                        registro = registro + "|||" + resultado_reporte[rp][svariable4].toUpperCase();
                    }
                    if(variable1 == "" && variable2 == "" && variable3 == "" && variable4 == ""){
                        registro = registro + "|||" + resultado_reporte[rp]["direccion"];  
                    }

                    if(mes_trimestre_1 > 0){
                        egreso_pendiente_01 = corregirNumeroNull(0);
                        egreso_pendiente_02 = corregirNumeroNull(0);
                        egreso_pendiente_03 = corregirNumeroNull(0);
                        registro = registro + "|||" + egreso_pendiente_01;
                        registro = registro + "|||" + egreso_pendiente_02;
                        registro = registro + "|||" + egreso_pendiente_03;
                    }

                    if(trimestre_1 > 0){
                        egreso_pendiente_q1 = corregirNumeroNull(0);
                        registro = registro + "|||" + egreso_pendiente_q1;
                    }

                    if(mes_trimestre_2 > 0){
                        egreso_pendiente_04 = corregirNumeroNull(0);
                        egreso_pendiente_05 = corregirNumeroNull(0);
                        egreso_pendiente_06 = corregirNumeroNull(0);
                        registro = registro + "|||" + egreso_pendiente_04;
                        registro = registro + "|||" + egreso_pendiente_05;
                        registro = registro + "|||" + egreso_pendiente_06;
                    }

                    if(trimestre_2 > 0){
                        egreso_pendiente_q2 = corregirNumeroNull(0);
                        registro = registro + "|||" + egreso_pendiente_q2;
                    }

                    if(mes_trimestre_3 > 0){
                        egreso_pendiente_07 = corregirNumeroNull(0);
                        egreso_pendiente_08 = corregirNumeroNull(0);
                        egreso_pendiente_09 = corregirNumeroNull(0);
                        registro = registro + "|||" + egreso_pendiente_07;
                        registro = registro + "|||" + egreso_pendiente_08;
                        registro = registro + "|||" + egreso_pendiente_09;
                    }

                    if(trimestre_3 > 0){
                        egreso_pendiente_q3 = corregirNumeroNull(0);
                        registro = registro + "|||" + egreso_pendiente_q3;
                    }

                    if(mes_trimestre_4 > 0){
                        egreso_pendiente_10 = corregirNumeroNull(0);
                        egreso_pendiente_11 = corregirNumeroNull(0);
                        egreso_pendiente_12 = corregirNumeroNull(0);
                        registro = registro + "|||" + egreso_pendiente_10;
                        registro = registro + "|||" + egreso_pendiente_11;
                        registro = registro + "|||" + egreso_pendiente_12;
                    }

                    if(trimestre_4 > 0){
                        egreso_pendiente_q4 = corregirNumeroNull(0);
                        registro = registro + "|||" + egreso_pendiente_q4;
                    }

                    egreso_pendiente_total = corregirNumeroNull(0);
                    registro = registro + "|||" + egreso_pendiente_total;

                    campos_reporte.push({registro: registro});

                }
            //FINAL DE PROCEDIMIENTO




            //PROCEDIMIENTO DE SALDOS
                //var posEgreso = buscarCodigo(codigo_reporte,resultado_egreso);


                    registro = ""; //DEJAR EN BLANCO EL REGISTRO PARA LLENARLO CON OTROS VALORES

                    registro = registro + "";
                    registro = registro + "|||S";

                    if(variable1 != ""){
                        registro = registro + "|||" + resultado_reporte[rp][svariable1].toUpperCase();
                    }
                    if(variable2 != ""){
                        registro = registro + "|||" + resultado_reporte[rp][svariable2].toUpperCase();
                    }
                    if(variable3 != ""){
                        registro = registro + "|||" + resultado_reporte[rp][svariable3].toUpperCase();
                    }
                    if(variable4 != ""){
                        registro = registro + "|||" + resultado_reporte[rp][svariable4].toUpperCase();
                    }
                    if(variable1 == "" && variable2 == "" && variable3 == "" && variable4 == ""){
                        registro = registro + "|||" + resultado_reporte[rp]["direccion"];  
                    }

                    if(mes_trimestre_1 > 0){
                        saldo_01 = parseFloat(ingreso_01) - parseFloat(egreso_01) - parseFloat(egreso_pendiente_01);
                        saldo_02 = parseFloat(ingreso_02) - parseFloat(egreso_02) - parseFloat(egreso_pendiente_02);
                        saldo_03 = parseFloat(ingreso_03) - parseFloat(egreso_03) - parseFloat(egreso_pendiente_03);
                        registro = registro + "|||" + saldo_01;
                        registro = registro + "|||" + saldo_02;
                        registro = registro + "|||" + saldo_03;
                    }

                    if(trimestre_1 > 0){
                        saldo_q1 = parseFloat(ingreso_q1) - parseFloat(egreso_q1) - parseFloat(egreso_pendiente_q1);
                        registro = registro + "|||" + saldo_q1;
                    }

                    if(mes_trimestre_2 > 0){
                        saldo_04 = parseFloat(ingreso_04) - parseFloat(egreso_04) - parseFloat(egreso_pendiente_04);
                        saldo_05 = parseFloat(ingreso_05) - parseFloat(egreso_05) - parseFloat(egreso_pendiente_05);
                        saldo_06 = parseFloat(ingreso_06) - parseFloat(egreso_06) - parseFloat(egreso_pendiente_06);
                        registro = registro + "|||" + saldo_04;
                        registro = registro + "|||" + saldo_05;
                        registro = registro + "|||" + saldo_06;
                    }

                    if(trimestre_2 > 0){
                        saldo_q2 = parseFloat(ingreso_q2) - parseFloat(egreso_q2) - parseFloat(egreso_pendiente_q2);
                        registro = registro + "|||" + saldo_q2;
                    }

                    if(mes_trimestre_3 > 0){
                        saldo_07 = parseFloat(ingreso_07) - parseFloat(egreso_07) - parseFloat(egreso_pendiente_07);
                        saldo_08 = parseFloat(ingreso_08) - parseFloat(egreso_08) - parseFloat(egreso_pendiente_08);
                        saldo_09 = parseFloat(ingreso_09) - parseFloat(egreso_09) - parseFloat(egreso_pendiente_09);
                        registro = registro + "|||" + saldo_07;
                        registro = registro + "|||" + saldo_08;
                        registro = registro + "|||" + saldo_09;
                    }

                    if(trimestre_3 > 0){
                        saldo_q3 = parseFloat(ingreso_q3) - parseFloat(egreso_q3) - parseFloat(egreso_pendiente_q3);
                        registro = registro + "|||" + saldo_q3;
                    }

                    if(mes_trimestre_4 > 0){
                        saldo_10 = parseFloat(ingreso_10) - parseFloat(egreso_10) - parseFloat(egreso_pendiente_10);
                        saldo_11 = parseFloat(ingreso_11) - parseFloat(egreso_11) - parseFloat(egreso_pendiente_11);
                        saldo_12 = parseFloat(ingreso_12) - parseFloat(egreso_12) - parseFloat(egreso_pendiente_12);
                        registro = registro + "|||" + saldo_10;
                        registro = registro + "|||" + saldo_11;
                        registro = registro + "|||" + saldo_12;
                    }

                    if(trimestre_4 > 0){
                        saldo_q4 = parseFloat(ingreso_q4) - parseFloat(egreso_q4) - parseFloat(egreso_pendiente_q4);
                        registro = registro + "|||" + saldo_q4;
                    }

                    saldo_total = parseFloat(ingreso_total) - parseFloat(egreso_total) - parseFloat(egreso_pendiente_total);
                    registro = registro + "|||" + saldo_total;

                    campos_reporte.push({registro: registro});

            
            //FINAL DE PROCEDIMIENTO


            //PROCEDIMIENTO DE LINEA QUE CORTA CADA GRUPO
                //var posEgreso = buscarCodigo(codigo_reporte,resultado_egreso);


                    registro = ""; //DEJAR EN BLANCO EL REGISTRO PARA LLENARLO CON OTROS VALORES

                    registro = registro + "";
                    registro = registro + "||| ";

                    if(variable1 != ""){
                        registro = registro + "||| ";
                    }
                    if(variable2 != ""){
                        registro = registro + "||| ";
                    }
                    if(variable3 != ""){
                        registro = registro + "||| ";
                    }
                    if(variable4 != ""){
                        registro = registro + "||| ";
                    }
                    if(variable1 == "" && variable2 == "" && variable3 == "" && variable4 == ""){
                        registro = registro + "||| ";  
                    }

                    if(mes_trimestre_1 > 0){
                        registro = registro + "||| ";
                        registro = registro + "||| ";
                        registro = registro + "||| ";
                    }

                    if(trimestre_1 > 0){
                        registro = registro + "||| ";
                    }

                    if(mes_trimestre_2 > 0){
                        registro = registro + "||| ";
                        registro = registro + "||| ";
                        registro = registro + "||| ";
                    }

                    if(trimestre_2 > 0){
                        registro = registro + "||| ";
                    }

                    if(mes_trimestre_3 > 0){
                        registro = registro + "||| ";
                        registro = registro + "||| ";
                        registro = registro + "||| ";
                    }

                    if(trimestre_3 > 0){
                        registro = registro + "||| ";
                    }

                    if(mes_trimestre_4 > 0){
                        registro = registro + "||| ";
                        registro = registro + "||| ";
                        registro = registro + "||| ";
                    }

                    if(trimestre_4 > 0){
                        registro = registro + "||| ";
                    }

                    registro = registro + "||| ";

                    campos_reporte.push({registro: registro});

            
            //FINAL DE PROCEDIMIENTO



        }
        //console.log(codigos_ingreso);
        //num_var = num_var + 2; //PARA COMPLETAR EL ANCHO EN LA TABLA
        //console.log(campos_reporte);

        for(var rpi=0; rpi<campos_reporte.length ; rpi++){
            var arreglo_registro = [];
            arreglo_registro = (campos_reporte[rpi].registro).split('|||');
            var claseFila = "celdamarcada";
            var estiloFila = "";
            var tipoRegistro = arreglo_registro[1];

            if(tipoRegistro == "DE"){
                claseFila = "detalleReporte detalle_color_rojo";
                estiloFila = " style='display: none;'";
            }
            if(tipoRegistro == "DP"){
                claseFila = "detalleReporte detalle_color_verde";
                estiloFila = " style='display: none;'";
            }
            if(tipoRegistro == "E"){
                claseFila = "reporte ejecutadas";
                estiloFila = " style='display: none;'";
            }
            if(tipoRegistro == "P"){
                claseFila = "reporte detalle_color_verde";
                estiloFila = " style='display: none;'";
            }
            if(tipoRegistro == "S"){
                claseFila = "reporte saldo";
                estiloFila = " style='display: none;'";
            }
            if(tipoRegistro == " "){
                claseFila = "reporte saldo";
                estiloFila = " style='display: none;'";
            }



            var fila = $("<tr class='"+ claseFila +"' "+ estiloFila+">");
            fila.html("");

            for(var rpia=0; rpia<arreglo_registro.length ; rpia++){

                if(rpia>1 && (num_var_sum - rpia) > 0){
                    clase = tit_var[ (num_var - (num_var_sum - rpia)) ];
                }else{
                    if(num_var_sum == 2){
                        clase = "direccion";
                    }else{
                        clase = "mes";
                    }
                } 
                if(rpia==0) clase = "n";
                if(rpia==1) clase = "d";

                if(campos_reporte_titulo[rpia] == "TOTAL_YEAR") clase = "total_year";



                if(arreglo_registro[rpia] != ""){
                    if(isNaN( arreglo_registro[rpia] ) ){ //COMPROBAR SI ES NUMERICO O NO
                        if(tipoRegistro == "I" || tipoRegistro == "DE" || tipoRegistro == "DP" || arreglo_registro[rpia] == "P" || arreglo_registro[rpia] == "E" || arreglo_registro[rpia] == "S"){


                            fila.append('<td class="'+clase+'">'+ arreglo_registro[rpia] +'</td>'); 

                            
                        }else{
                            fila.append('<td class="'+clase+'"></td>');
                        }
                    }else{

                        if(arreglo_registro[rpia] != " "){
                            //console.log("-"+tipoRegistro+"-");
                            fila.append('<td class="'+clase+'">'+ formatNumber.new(parseFloat(arreglo_registro[rpia]).toFixed(decimales), "") +'</td>');
                        }else{
                            fila.append('<td class="'+clase+'"></td>');
                        }
                    }                    
                }else{
                    fila.append('<td class="'+clase+'">'+ arreglo_registro[rpia] +'</td>');
                }


                //fila.append('<td>'+ arreglo_registro[rpia] +'</td>');
            }
            $("#tablaReportePresupuesto tbody").append(fila);

        }

        //FINAL DE INGRESOS



        
        recorrer_marcas_reporte();
        //celdasBasicas();
        //ajustarColumnas();

        //goheadfixed('table.reportePresupuesto');
        //$(".reportePresupuesto").fixedtableheader();

        //$(".reportePresupuesto .thead").sticky();
        
        Terminado();

    }



    function ajustarColumnas(){
        var direccion = 20;

        //$("#tablaReportePresupuesto th").css("width","45px");
        $("#tablaReportePresupuesto .direccion").css("width","75px");
        $("#tablaReportePresupuesto .mes").css("width","45px");

        $("#presupuesto_inicial .n").css("width","25px");
        $("#presupuesto_inicial .d").css("width","20px");
        $("#presupuesto_inicial .total_year").css("width","70px");

        $("#presupuesto_inicial .DIRECCION").css("width","20%");
        $("#presupuesto_inicial .AREA").css("width","7%");
        $("#presupuesto_inicial .CATEGORIA").css("width","9%");
        $("#presupuesto_inicial .PROYECTO").css("width","7%");
        $("#presupuesto_inicial .COMENTARIO").css("width","10%");
        $("#presupuesto_inicial .tipo").css("width","5%");
        $("#presupuesto_inicial .STRING").css("width","6%");
        $("#presupuesto_inicial .chr_string_id").css("width","6%");
        $("#presupuesto_inicial .chr_categoria_id").css("width","6%");

    }
    
    $("#descargarRegistroPresupuesto").on("click",function(){
        //$("#descargarRegistroPresupuesto").val( $("<div>").append( $("#tablaReportePresupuesto").eq(0).clone()).html());
        //$("#FormularioExportacion").submit();
        ////fnExcelReport();
        //exportarExcel();
        $("#modalReportePresupuesto").modal("show");
    });

    $("#reporteGeneralPresupuesto").on('click', function(){
        fnExcelReport();
    })

    function celdasBasicas(){

        var columnasEliminadas = [];

        //columnasTabla = columnasTabla  - 1;

        dq1 = $("#chk_q1_presupuesto:checked")["length"];
        dq2 = $("#chk_q2_presupuesto:checked")["length"];
        dq3 = $("#chk_q3_presupuesto:checked")["length"];
        dq4 = $("#chk_q4_presupuesto:checked")["length"];

        dmq1 = $("#chk_q1mes_presupuesto:checked")["length"];
        dmq2 = $("#chk_q2mes_presupuesto:checked")["length"];
        dmq3 = $("#chk_q3mes_presupuesto:checked")["length"];
        dmq4 = $("#chk_q4mes_presupuesto:checked")["length"];


        if(dq4 == 0){
            columnasEliminadas.push(columnasTabla + 16);
        }        
        if(dmq4 == 0){
            columnasEliminadas.push(columnasTabla + 15);
            columnasEliminadas.push(columnasTabla + 14);
            columnasEliminadas.push(columnasTabla + 13);
        }
        if(dq3 == 0){
            columnasEliminadas.push(columnasTabla + 12);
        }
        if(dmq3 == 0){
            columnasEliminadas.push(columnasTabla + 11);
            columnasEliminadas.push(columnasTabla + 10);
            columnasEliminadas.push(columnasTabla + 9);
        }
        if(dq2 == 0){
            columnasEliminadas.push(columnasTabla + 8);
        }
        if(dmq2 == 0){
            columnasEliminadas.push(columnasTabla + 7);
            columnasEliminadas.push(columnasTabla + 6);
            columnasEliminadas.push(columnasTabla + 5);
        }
        if(dq1 == 0){
            columnasEliminadas.push(columnasTabla + 4);
        }        

        if(dmq1 == 0){
            columnasEliminadas.push(columnasTabla + 3);
            columnasEliminadas.push(columnasTabla + 2);
            columnasEliminadas.push(columnasTabla + 1);
        }



        for(var i=0; i<columnasEliminadas.length ; i++){
            //console.log(columnasEliminadas[i]);
            //$('#tablaReportePresupuesto th:nth-child('+columnasEliminadas[i]+'),#tablaReportePresupuesto td:nth-child('+columnasEliminadas[i]+')').remove();
        }

        //$("#tablaReportePresupuesto").fixedtableheader();


    }
        //ELIMINAR COLUMNAS NO LISTADAS



    function exportarExcel(){
        /*
        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('tablaReportePresupuesto');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');

        var a = document.createElement('a');
        a.href = data_type + ', ' + table_html;
        a.download = 'PRESUPUESTO_REPORTE_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
        a.click();
        */

        var tmpElemento = document.createElement('a');
        // obtenemos la información desde el div que lo contiene en el html
        // Obtenemos la información de la tabla
        var data_type = 'data:application/vnd.ms-excel';
        var tabla_div = document.getElementById('tablaReportePresupuesto');
        var tabla_html = tabla_div.outerHTML.replace(/ /g, '%20');
        tmpElemento.href = data_type + ', ' + tabla_html;
        //Asignamos el nombre a nuestro EXCEL
        tmpElemento.download = 'Nombre_De_Mi_Excel.xls';
        // Simulamos el click al elemento creado para descargarlo
        tmpElemento.click();


    }

    function fnExcelReport()
    {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }

        var table = 'tablaReportePresupuesto';
        var name = 'REPORTE_PRESUPUESTER';

        if (!table.nodeType) table = document.getElementById(table)
         var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
         window.location.href = uri + base64(format(template, ctx))
        /*
        var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
        var textRange; var j=0;
        tab = document.getElementById('tablaReportePresupuesto'); // id of table

        for(j = 0 ; j < tab.rows.length ; j++) 
        {     
            tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text=tab_text+"</table>";
        tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
        tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE "); 

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html","replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus(); 
            sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
        }  
        else                 //other browser not tested on IE 11
            sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

        return (sa);
        */
    }

    function desmarca_filtros(){
        $("#chk_reporte_presupuesto").prop("checked", "");
        $("#chk_q1_presupuesto").prop("checked", "");
        $("#chk_q2_presupuesto").prop("checked", "");
        $("#chk_q3_presupuesto").prop("checked", "");
        $("#chk_q4_presupuesto").prop("checked", "");

        $(".q1").css("display", "none");
        $(".q2").css("display", "none");
        $(".q3").css("display", "none");
        $(".q4").css("display", "none");

        $("#chk_q1mes_presupuesto").prop("checked", "");
        $("#chk_q2mes_presupuesto").prop("checked", "");
        $("#chk_q3mes_presupuesto").prop("checked", "");
        $("#chk_q4mes_presupuesto").prop("checked", "");

        $(".q1mes").css("display", "none");
        $(".q2mes").css("display", "none");
        $(".q3mes").css("display", "none");
        $(".q4mes").css("display", "none");

    }


    function verRegistroPresupuesto(){
        var data = $(this).data("data");
        $("#direccionPresupuesto").val(data.descripcion);
        $("#direccionPresupuesto").prop('disabled', true);

        $("#presupuesto").addClass("active").show();
        $("#presupuesto_inicial").removeClass("active");

        $("#menu_principal li").removeClass("active");

        activaTab('listaIngresoGastoPresupuesto');
        $("#buscarSaldoPresupuesto").click();

        // var objDatosBusquedaRegistroPresupuesto = new Object()
        //    objDatosBusquedaRegistroPresupuesto.variable1 = "idPresupuesto";
        //    objDatosBusquedaRegistroPresupuesto.campo1 = data.idPresupuesto;
        //service.procesar("getListadeRegistrosdePresupuesto",objDatosBusquedaRegistroPresupuesto,cargaRegistrodePresupuesto);
    }

    function mostrarRegistroGastos(){
        var data = $(this).data("data");
        //console.log("deseas mostrar",data);
    }





    function activaTab(tab){
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    };



    // AGREGAR VALOR DE BASE DATOS $(".variableFiltro" ).append( "<option value=''>SELECCIONAR</option><option value='tipo'>TIPO</option><option value='area'>AREA</option><option value='categoria'>CATEGORIA</option><option value='proyecto'>PROTECTO</option><option value='comentarios'>COMENTARIOS</option>" );
    //$("#variable2" ).append( "<option value=''>TODOS</option><option value='chr_string_id'>CODIGO STRING</option><option value='area'>AREA</option><option value='tipo'>TIPO</option><option value='chr_categoria_id'>CUENTA CATEGORIA</option><option value='tipo_presupuesto'>TIPO PRESUPUESTO</option>" );
    //$("#variable3" ).append( "<option value=''>TODOS</option><option value='chr_string_id'>CODIGO STRING</option><option value='area'>AREA</option><option value='tipo'>TIPO</option><option value='chr_categoria_id'>CUENTA CATEGORIA</option><option value='tipo_presupuesto'>TIPO PRESUPUESTO</option>" );


    $("#buscarRegistroPresupuesto").on("click",function(){
        Procesando();
        var objeto = new Object()
            objeto.variable1 = $("#variable1").val();
            objeto.variable2 = $("#variable2").val();
            objeto.variable3 = $("#variable3").val();
            objeto.variable4 = $("#variable4").val();
            objeto.subvariable1 = $("#subvariable1").val();
            objeto.subvariable2 = $("#subvariable2").val();
            objeto.subvariable3 = $("#subvariable3").val();
            objeto.subvariable4 = $("#subvariable4").val();
            objeto.anio = nanio;
            objeto.texto = $("#buscarProveedorPersonaldeApoyo").val();

            objeto.q1 = $("#chk_q1_presupuesto:checked")["length"];
            objeto.q2 = $("#chk_q2_presupuesto:checked")["length"];
            objeto.q3 = $("#chk_q3_presupuesto:checked")["length"];
            objeto.q4 = $("#chk_q4_presupuesto:checked")["length"];

            objeto.mq1 = $("#chk_q1mes_presupuesto:checked")["length"];
            objeto.mq2 = $("#chk_q2mes_presupuesto:checked")["length"];
            objeto.mq3 = $("#chk_q3mes_presupuesto:checked")["length"];
            objeto.mq4 = $("#chk_q4mes_presupuesto:checked")["length"];


        service.procesar("getListaRegistrosPresupuesto",objeto,cargaListadeRegistrosdePresupuesto);

    });

    function procesarRegistrosPresupuesto(){
        Procesando();
        var objeto = new Object()
            objeto.variable1 = $("select#variable1").val();
            objeto.variable2 = $("select#variable2").val();
            objeto.variable3 = $("select#variable3").val();
            objeto.variable4 = $("select#variable4").val();
            objeto.subvariable1 = $("select#subvariable1").val();
            objeto.subvariable2 = $("select#subvariable2").val();
            objeto.subvariable3 = $("select#subvariable3").val();
            objeto.subvariable4 = $("select#subvariable4").val();
            objeto.anio = nanio;
            objeto.texto = $("#buscarProveedorPersonaldeApoyo").val();

            objeto.q1 = $("#chk_q1_presupuesto:checked")["length"];
            objeto.q2 = $("#chk_q2_presupuesto:checked")["length"];
            objeto.q3 = $("#chk_q3_presupuesto:checked")["length"];
            objeto.q4 = $("#chk_q4_presupuesto:checked")["length"];

            objeto.mq1 = $("#chk_q1mes_presupuesto:checked")["length"];
            objeto.mq2 = $("#chk_q2mes_presupuesto:checked")["length"];
            objeto.mq3 = $("#chk_q3mes_presupuesto:checked")["length"];
            objeto.mq4 = $("#chk_q4mes_presupuesto:checked")["length"];



        service.procesar("getListaRegistrosPresupuesto",objeto,cargaListadeRegistrosdePresupuesto);
    }

    function procesarDetalleRegistrosPresupuesto(){
        var objeto = new Object()
            objeto.variable1 = $("select#variable1").val();
            objeto.variable2 = $("select#variable2").val();
            objeto.variable3 = $("select#variable3").val();
            objeto.variable4 = $("select#variable4").val();
            objeto.subvariable1 = $("select#subvariable1").val();
            objeto.subvariable2 = $("select#subvariable2").val();
            objeto.subvariable3 = $("select#subvariable3").val();
            objeto.subvariable4 = $("select#subvariable4").val();
            objeto.anio = nanio;
            objeto.texto = $("#buscarProveedorPersonaldeApoyo").val();

            objeto.q1 = $("#chk_q1_presupuesto:checked")["length"];
            objeto.q2 = $("#chk_q2_presupuesto:checked")["length"];
            objeto.q3 = $("#chk_q3_presupuesto:checked")["length"];
            objeto.q4 = $("#chk_q4_presupuesto:checked")["length"];

            objeto.mq1 = $("#chk_q1mes_presupuesto:checked")["length"];
            objeto.mq2 = $("#chk_q2mes_presupuesto:checked")["length"];
            objeto.mq3 = $("#chk_q3mes_presupuesto:checked")["length"];
            objeto.mq4 = $("#chk_q4mes_presupuesto:checked")["length"];



        service.procesar("getListaDetalleRegistrosPresupuesto",objeto,cargaListaDetalleRegistrosPresupuesto);
    }

    function limpiar_datos_presupuesto(){

        $("#tipoPresupuesto").val("");
        $("#centrodecostoPresupuesto").val("");
        $("#cuentacategoriaPresupuesto").val("");

        $("#enero_presupuesto").val("0.00");
        $("#febrero_presupuesto").val("0.00");
        $("#marzo_presupuesto").val("0.00");
        $("#abril_presupuesto").val("0.00");
        $("#mayo_presupuesto").val("0.00");
        $("#junio_presupuesto").val("0.00");
        $("#julio_presupuesto").val("0.00");
        $("#agosto_presupuesto").val("0.00");
        $("#setiembre_presupuesto").val("0.00");
        $("#octubre_presupuesto").val("0.00");
        $("#noviembre_presupuesto").val("0.00");
        $("#diciembre_presupuesto").val("0.00");
        $("#q1_presupuesto").val("0.00");
        $("#q2_presupuesto").val("0.00");
        $("#q3_presupuesto").val("0.00");
        $("#q4_presupuesto").val("0.00");
        $("#total_presupuesto").val("0.00");

        $("#area_proyecto_programa").val("");
        $("#area_aprobacion").val("");
        $("#area_comentarios").val("");
    };


    $("#cancelar_datos_presupuesto").click(function(){
        limpiar_datos_presupuesto();
        service.procesar("getListaCentrodeCosto","cspsiste",cargaDetalleCentrodeCosto);
        service.procesar("getListaCuentaCategoria","cspsiste",cargaDetalleCuentaCategoria);
    });

    $("#nuevo_datos_presupuesto").click(function(){
        limpiar_datos_presupuesto();
        service.procesar("getListaCentrodeCosto","cspsiste",cargaDetalleCentrodeCosto);
        service.procesar("getListaCuentaCategoria","cspsiste",cargaDetalleCuentaCategoria);

    });

    $("#guardar_datos_presupuesto").click(function(){

    });


    //$("#chk_q1_presupuesto").click(procesarRegistrosPresupuesto);
    function chk_q1_presupuesto(){
        valor = $("#chk_q1_presupuesto:checked")["length"];
        if(valor == 1){
            $(".q1").css("display", "");
        }else{
            $(".q1").css("display", "none");
        }

    };

    //$("#chk_q2_presupuesto").click(procesarRegistrosPresupuesto);
    function chk_q2_presupuesto(){
        valor = $("#chk_q2_presupuesto:checked")["length"];
        if(valor == 1){
            $(".q2").css("display", "");
        }else{
            $(".q2").css("display", "none");
        }

    }

    //$("#chk_q3_presupuesto").click(procesarRegistrosPresupuesto);
    function chk_q3_presupuesto(){
        valor = $("#chk_q3_presupuesto:checked")["length"];
        if(valor == 1){
            $(".q3").css("display", "");
        }else{
            $(".q3").css("display", "none");
        }

    }

    //$("#chk_q4_presupuesto").click(procesarRegistrosPresupuesto);
    function chk_q4_presupuesto(){
        valor = $("#chk_q4_presupuesto:checked")["length"];
        if(valor == 1){
            $(".q4").css("display", "");
        }else{
            $(".q4").css("display", "none");
        }

    }

    //$("#chk_reporte_presupuesto").click(procesarRegistrosPresupuesto);
    function chk_reporte_presupuesto(){
        valor = $("#chk_reporte_presupuesto:checked")["length"];
        if(valor == 0){
            $(".reporte").css("display", "none");
            $("#botonDesplegarDetalledeGasto").html("");
            botonMostrarDetalleGasto = 0;
            
            $(".reporteFinalPendientes").css("display", "none");
            $(".reporteFinalEjecutados").css("display", "none");
            $(".reporteFinalSaldos").css("display", "none");
            $("#botonDesplegarDetalledeGasto").html("");
        
        }else{
            $(".reporte").css("display", "");
            $("#botonDesplegarDetalledeGasto").html("MOSTRAR DETALLE DE GASTOS");
            
            if( $("#variable1").val() != "" || $("#variable2").val() != "" || $("#variable3").val() != "" || $("#variable4").val() != "" ){
                $(".reporteFinalPendientes").css("display", "");
                $(".reporteFinalEjecutados").css("display", "");
                $(".reporteFinalSaldos").css("display", "");
            }

        }
    }

    //$("#chk_q1mes_presupuesto").click(procesarRegistrosPresupuesto);
    function chk_q1mes_presupuesto(){
        valor = $("#chk_q1mes_presupuesto:checked")["length"];
        if(valor == 1){
            $(".q1mes").css("display", "");
        }else{
            $(".q1mes").css("display", "none");
        }

    }

    //$("#chk_q2mes_presupuesto").click(procesarRegistrosPresupuesto);
    function chk_q2mes_presupuesto(){
        valor = $("#chk_q2mes_presupuesto:checked")["length"];
        if(valor == 1){
            $(".q2mes").css("display", "");
        }else{
            $(".q2mes").css("display", "none");
        }

    }

    //$("#chk_q3mes_presupuesto").click(procesarRegistrosPresupuesto);
    function chk_q3mes_presupuesto(){
        valor = $("#chk_q3mes_presupuesto:checked")["length"];
        if(valor == 1){
            $(".q3mes").css("display", "");
        }else{
            $(".q3mes").css("display", "none");
        }

    }

    //$("#chk_q4mes_presupuesto").click(procesarRegistrosPresupuesto);
    function chk_q4mes_presupuesto(){
        valor = $("#chk_q4mes_presupuesto:checked")["length"];
        if(valor == 1){
            $(".q4mes").css("display", "");
        }else{
            $(".q4mes").css("display", "none");
        }

    }


    function recorrer_marcas_reporte(){
        chk_q1_presupuesto();
        chk_q2_presupuesto();
        chk_q3_presupuesto();
        chk_q4_presupuesto();
        chk_q1mes_presupuesto();
        chk_q2mes_presupuesto();
        chk_q3mes_presupuesto();
        chk_q4mes_presupuesto();
        chk_reporte_presupuesto();
        boton_detalle_gasto();
        /*
        valor = $("#botonDesplegarDetalledeGasto").html();
        if(valor == "MOSTRAR DETALLE DE GASTOS"){
            $(".detalleReporte").css("display", "none");
            $("#buscarProveedorPersonaldeApoyo").val("");
            $("#buscarProveedorPersonaldeApoyo").css("display","none");
        }else if(valor == "OCULTAR DETALLE DE GASTOS"){
            $(".detalleReporte").css("display", "");
            $("#buscarProveedorPersonaldeApoyo").css("display","");
        }
        */
    }

     
    $("#tablaListadeRegistrosdePresupuesto").on("dblclick", "td" , function(){
        var column_num = parseInt( $(this).index() );
        var row_num = parseInt( $(this).parent().index() );
        var detallePresupuesto = "";

        datoCelda = $(this).text();
        datoCelda = datoCelda.replace(/,/gi,"");

        //SI EL VALOR SELECCIONADO TIENE DATO Y ES NUMERO
        if( isNaN(datoCelda) || datoCelda == "" ){
            //EL VALOR ESTA VACIO O NO ES NUMERICO
        }else{

            monto = datoCelda;

            var va1 = $("select#variable1").val();
            var va2 = $("select#variable2").val();
            var va3 = $("select#variable3").val();
            var va4 = $("select#variable4").val();
            var col = parseInt( $(this).index() );
            var row = parseInt( $(this).parent().index() ) + 1;
            var tipo = document.getElementById("tablaListadeRegistrosdePresupuesto").rows[row].cells[1].innerText;
            var variable = document.getElementById("tablaListadeRegistrosdePresupuesto").rows[0].cells[col].innerText;
            var variables = [];
            var filtros = [];

            if(va1 != ""){
                variables.push(va1);
                if( tipo == "I"){
                    var filtro = document.getElementById("tablaListadeRegistrosdePresupuesto").rows[row].cells[2].innerText;
                }else if( tipo == "E"){
                    var filtro = document.getElementById("tablaListadeRegistrosdePresupuesto").rows[row-1].cells[2].innerText;
                }else if( tipo == "S"){
                    var filtro = document.getElementById("tablaListadeRegistrosdePresupuesto").rows[row-2].cells[2].innerText;
                }
                filtros.push(filtro);                
            }
            if(va2 != ""){
                variables.push(va2);
                if( tipo == "I"){
                    var filtro = document.getElementById("tablaListadeRegistrosdePresupuesto").rows[row].cells[3].innerText;
                }else if( tipo == "E"){
                    var filtro = document.getElementById("tablaListadeRegistrosdePresupuesto").rows[row-1].cells[3].innerText;
                }else if( tipo == "S"){
                    var filtro = document.getElementById("tablaListadeRegistrosdePresupuesto").rows[row-2].cells[3].innerText;
                }
                filtros.push(filtro);
            }
            if(va3 != ""){
                variables.push(va3);
                if( tipo == "I"){
                    var filtro = document.getElementById("tablaListadeRegistrosdePresupuesto").rows[row].cells[4].innerText;
                }else if( tipo == "E"){
                    var filtro = document.getElementById("tablaListadeRegistrosdePresupuesto").rows[row-1].cells[4].innerText;
                }else if( tipo == "S"){
                    var filtro = document.getElementById("tablaListadeRegistrosdePresupuesto").rows[row-2].cells[4].innerText;
                }
                filtros.push(filtro);                
            }
            if(va4 != ""){
                variables.push(va4);
                if( tipo == "I"){
                    var filtro = document.getElementById("tablaListadeRegistrosdePresupuesto").rows[row].cells[5].innerText;
                }else if( tipo == "E"){
                    var filtro = document.getElementById("tablaListadeRegistrosdePresupuesto").rows[row-1].cells[5].innerText;
                }else if( tipo == "S"){
                    var filtro = document.getElementById("tablaListadeRegistrosdePresupuesto").rows[row-2].cells[5].innerText;
                }
                filtros.push(filtro);                
            }


            var pdenero = 0;
            var pdfebrero = 0;
            var pdmarzo = 0;
            var pdq1 = 0;
            var pdabril = 0;
            var pdmayo = 0;
            var pdjunio = 0;
            var pdq2 = 0;
            var pdjulio = 0;
            var pdagosto = 0;
            var pdseptiembre = 0;
            var pdq3 = 0;
            var pdoctubre = 0;
            var pdnoviembre = 0;
            var pddiciembre = 0;
            var pdq4 = 0;
            var pdtotal = 0;


            //CARGAR LO SELECCIONADO
            var detallePresupuesto = "DACA";
            for(var i=0; i<filtros.length ; i++){
                detallePresupuesto = detallePresupuesto + " - " + filtros[i];
            }
            //DATOS DEL FORMULARIO DETALLE
            $("#detallePresupuesto").html(detallePresupuesto);


            $("#tablaListaDetalledePresupuesto tbody").html("");
            var fila = $("<tr class='semaforo_verde'>");

            fila.html('<td>-</td><td class="">PRESUPUESTADO</td><td class="qd1mes" style="display: none;">'+
            pdenero +'</td><td class="qd2mes" style="display: none;">'+
            pdfebrero +'</td><td class="qd3mes" style="display: none;">'+
            pdmarzo +'</td><td class="celdamarcada qd1" style="display: none;">'+
            pdq1 +'</td><td class="qd4mes" style="display: none;">'+
            pdabril +'</td><td class="qd5mes" style="display: none;">'+
            pdmayo +'</td><td class="qd6mes" style="display: none;">'+
            pdjunio +'</td><td class="celdamarcada qd2" style="display: none;">'+
            pdq2 +'</td><td class="qd7mes" style="display: none;">'+
            pdjulio +'</td><td class="qd8mes" style="display: none;">'+
            pdagosto +'</td><td class="qd9mes" style="display: none;">'+
            pdseptiembre +'</td><td class="celdamarcada qd3" style="display: none;">'+
            pdq3 +'</td><td class="qd10mes" style="display: none;">'+
            pdoctubre +'</td><td class="qd11mes" style="display: none;">'+
            pdnoviembre +'</td><td class="qd12mes" style="display: none;">'+
            pddiciembre +'</td><td class="celdamarcada qd4" style="display: none;">'+
            pdq4 +'</td><td class="celdamarcada">'+
            pdtotal +'</td>');

            $("#tablaListaDetalledePresupuesto tbody").append(fila);






            var objDatosdeObjeto = new Object()
                objDatosdeObjeto.variable = variable;
                objDatosdeObjeto.anio = nanio;
                objDatosdeObjeto.variables = variables;
                objDatosdeObjeto.filtros = filtros;
                objDatosdeObjeto.monto = monto;
                objDatosdeObjeto.anio = nanio;
            service.procesar("getDetallePresupuesto",objDatosdeObjeto,cargaResultadoDetallePresupuesto);


        








            resetTablaDetallePresupuesto();

            if(variable == "ENERO"){
                 $(".qd1mes").css("display", "");
            }
            if(variable == "FEBRERO"){
                 $(".qd2mes").css("display", "");
            }
            if(variable == "MARZO"){
                 $(".qd3mes").css("display", "");
            }
            if(variable == "Q1"){
                mostrarQ1Completo();
                $("#mdtDetallePresupuesto").css("width","60%");
            }
            function mostrarQ1Completo(){
                 $(".qd1mes").css("display", "");
                 $(".qd2mes").css("display", "");
                 $(".qd3mes").css("display", "");
                 $(".qd1").css("display", "");
            }

            if(variable == "ABRIL"){
                 $(".qd4mes").css("display", "");
            }
            if(variable == "MAYO"){
                 $(".qd5mes").css("display", "");
            }
            if(variable == "JUNIO"){
                 $(".qd6mes").css("display", "");
            }
            if(variable == "Q2"){
                mostrarQ2Completo();
                $("#mdtDetallePresupuesto").css("width","60%");
            }
            function mostrarQ2Completo(){
                 $(".qd4mes").css("display", "");
                 $(".qd5mes").css("display", "");
                 $(".qd6mes").css("display", "");
                 $(".qd2").css("display", "");
            }

            if(variable == "JULIO"){
                 $(".qd7mes").css("display", "");
            }
            if(variable == "AGOSTO"){
                 $(".qd8mes").css("display", "");
            }
            if(variable == "SETIEMBRE"){
                 $(".qd9mes").css("display", "");
            }
            if(variable == "Q3"){
                mostrarQ3Completo();
                $("#mdtDetallePresupuesto").css("width","60%");
            }
            function mostrarQ3Completo(){
                 $(".qd7mes").css("display", "");
                 $(".qd8mes").css("display", "");
                 $(".qd9mes").css("display", "");
                 $(".qd3").css("display", "");
            }

            if(variable == "OCTUBRE"){
                 $(".qd10mes").css("display", "");
            }
            if(variable == "NOVIEMBRE"){
                 $(".qd11mes").css("display", "");
            }
            if(variable == "DICIEMBRE"){
                 $(".qd12mes").css("display", "");
            }
            if(variable == "Q4"){
                mostrarQ4Completo();
                $("#mdtDetallePresupuesto").css("width","60%");
            }
            function mostrarQ4Completo(){
                 $(".qd10mes").css("display", "");
                 $(".qd11mes").css("display", "");
                 $(".qd12mes").css("display", "");
                 $(".qd4").css("display", "");
            }

            if (variable == "TOTAL"){
                mostrarQ1Completo();
                mostrarQ2Completo();
                mostrarQ3Completo();
                mostrarQ4Completo();
                $("#mdtDetallePresupuesto").css("width","90%");
            }

            //var dato = document.getElementById("tablaListadeRegistrosdePresupuesto").rows[row].cells[col].innerText;
            
            //console.log(dato + " : " + column_num + " - " + row_num + " TIPO : " + tipo);

            //DIRECCIONAR EL CLICK A LA LINEA DE VARIABLES

            //console.log(variable);
            //console.log(variables);
            //console.log(filtros);
            
            //console.log( variable.textContent ); //CAPTURA DE TEXTO DE LA COLUMNA
            //console.log( variables );
            //console.log( tipo );
            //console.log( monto );


            //REVISAR SI SE TIENE VARIABLES FILTRADAS EN EL SISTEMA
            //if( $("select#variable1").val() == "" && $("select#variable2").val() == "" && $("select#variable3").val() == "" && $("select#variable4").val() == "" ){
                //console.log("vacio")
            //}else{
                //console.log("tiene dato");
            //}
            //FINAL DEL PROCESO

            $("#modalDetallePresupuesto").modal()

            //service.procesar("getDetallePresupuesto",cargaResponsablePresupuesto);

        }


    })

    function resetTablaDetallePresupuesto(){
        $(".qd1mes").css("display", "none");
        $(".qd2mes").css("display", "none");
        $(".qd3mes").css("display", "none");
        $(".qd1").css("display", "none");
        $(".qd4mes").css("display", "none");
        $(".qd5mes").css("display", "none");
        $(".qd6mes").css("display", "none");
        $(".qd2").css("display", "none");
        $(".qd7mes").css("display", "none");
        $(".qd8mes").css("display", "none");
        $(".qd9mes").css("display", "none");
        $(".qd3").css("display", "none");
        $(".qd10mes").css("display", "none");
        $(".qd11mes").css("display", "none");
        $(".qd12mes").css("display", "none");
        $(".qd4").css("display", "none");
        $("#mdtDetallePresupuesto").css("width","40%");

    }

    function cargaResultadoDetallePresupuesto(evt){
        console.log( evt );
    }



    $("#botonDesplegarDetalledeGasto").click(function(){
        var valor = $("#botonDesplegarDetalledeGasto").html();
        if(valor == "MOSTRAR DETALLE DE GASTOS"){
            botonMostrarDetalleGasto = 1;
            //ajustarColumnas();
        }else{
            botonMostrarDetalleGasto = 0;
        }
        boton_detalle_gasto();
    });

    function boton_detalle_gasto(){
        if(botonMostrarDetalleGasto == 1){
            $(".detalleReporte").css("display", "");
            //$("#buscarProveedorPersonaldeApoyo").val("");
            $("#buscarProveedorPersonaldeApoyo").css("display","");
            $("#botonDesplegarDetalledeGasto").html("OCULTAR DETALLE DE GASTOS");
        }else if(botonMostrarDetalleGasto == 0){
            $(".detalleReporte").css("display", "none");
            $("#buscarProveedorPersonaldeApoyo").val("");
            $("#buscarProveedorPersonaldeApoyo").css("display","none");
            $("#botonDesplegarDetalledeGasto").html("MOSTRAR DETALLE DE GASTOS");
        }        
    }

    //$("#buscarProveedorPersonaldeApoyo").keyup(function(){
    //    procesarRegistrosPresupuesto();
    //});













    function contarVariables(){
        if( $("select#variable1").val() == "" && $("select#variable2").val() == "" && $("select#variable3").val() == "" && $("select#variable4").val() == "" ){
            console.log("vacio")
        }else{
            console.log("tiene dato");
        }
    }



    $("#reporteAreaPresupuesto").on('click', function(){
        var area = $("#areaReportePresupuesto").val();
        var ampliacion = $("#chkpnuevoInicialPresupuesto:checked")["length"];

        if(area != ""){
            service.procesar("getReporteAreaPresupuesto",area,ampliacion,nanio,cargaReporteAreaPresupuesto);
            //alertify.success("ÁREA SELECCIONADA");
        }else{
            alertify.error("SELECCIONAR ÁREA");
        }
    })

    $("#reporteAreaPresupuestoSaldos").on('click', function(){
        var area = $("#areaReportePresupuesto").val();
        var ampliacion = $("#chkpnuevoInicialPresupuesto:checked")["length"];

        if(area != ""){
            service.procesar("getReporteAreaPresupuestoSaldos",area,ampliacion,nanio,cargaReporteAreaPresupuestoSaldos);
            //alertify.success("ÁREA SELECCIONADA");
        }else{
            alertify.error("SELECCIONAR ÁREA");
        }
    })

    function cargaReporteAreaPresupuesto(evt){
        //console.log(evt);

        resultado = evt;

        $("#tablaResultadoAreaReportePresupuesto tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");

            fila.html('<td>'+ (i + 1) +'</td><td>'+ corregirNull(resultado[i].carrera).toUpperCase() +'</td><td>'+ corregirNull(resultado[i].proyecto).toUpperCase() +'</td><td>'+ corregirNull(resultado[i].categoria).toUpperCase() +'</td><td>'+ corregirNull(resultado[i].comentario).toUpperCase() +'</td><td>'+ corregirNull(resultado[i].descripcion).toUpperCase() +'</td><td>'+ corregirNull(resultado[i].fecha).toUpperCase() +'</td><td>'+ corregirNull(resultado[i].proveedor).toUpperCase() +'</td><td>'+ corregirNumeroNull(resultado[i].presupuesto) +'</td><td>'+ corregirNumeroNull(resultado[i].gasto) +'</td><td>'+ formatNumber.new(parseFloat(corregirNumeroNull(resultado[i].saldo)).toFixed(2), "") +'</td>');

            $("#tablaResultadoAreaReportePresupuesto tbody").append(fila);

        }

        $("#modalResultadoAreaReporteProyecto").modal("show");
    }

    function cargaReporteAreaPresupuestoSaldos(evt){
        //console.log(evt);

        resultado = evt;

        $("#tablaResultadoAreaReportePresupuestoSaldos tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");

            fila.html('<td>'+ (i + 1) +'</td><td>'+ corregirNull(resultado[i].proyecto).toUpperCase() +'</td><td>'+ corregirNull(resultado[i].categoria).toUpperCase() +'</td><td>'+ corregirNull(resultado[i].comentario).toUpperCase() +'</td><td>'+ formatNumber.new(parseFloat(corregirNumeroNull(resultado[i].p_q1)).toFixed(2), "") +'</td><td>'+ formatNumber.new(parseFloat(corregirNumeroNull(resultado[i].g_q1)).toFixed(2), "") +'</td><td>'+ formatNumber.new(parseFloat(corregirNumeroNull(resultado[i].s_q1)).toFixed(2), "") +'</td><td>'+ formatNumber.new(parseFloat(corregirNumeroNull(resultado[i].p_q2)).toFixed(2), "") +'</td><td>'+ formatNumber.new(parseFloat(corregirNumeroNull(resultado[i].g_q2)).toFixed(2), "") +'</td><td>'+ formatNumber.new(parseFloat(corregirNumeroNull(resultado[i].s_q2)).toFixed(2), "") +'</td><td>'+ formatNumber.new(parseFloat(corregirNumeroNull(resultado[i].p_q3)).toFixed(2), "") +'</td><td>'+ formatNumber.new(parseFloat(corregirNumeroNull(resultado[i].g_q3)).toFixed(2), "") +'</td><td>'+ formatNumber.new(parseFloat(corregirNumeroNull(resultado[i].s_q3)).toFixed(2), "") +'</td><td>'+ formatNumber.new(parseFloat(corregirNumeroNull(resultado[i].p_q4)).toFixed(2), "") +'</td><td>'+ formatNumber.new(parseFloat(corregirNumeroNull(resultado[i].g_q4)).toFixed(2), "") +'</td><td>'+ formatNumber.new(parseFloat(corregirNumeroNull(resultado[i].s_q4)).toFixed(2), "") +'</td><td>'+ formatNumber.new(parseFloat(corregirNumeroNull(resultado[i].presupuesto)).toFixed(2), "") +'</td><td>'+ formatNumber.new(parseFloat(corregirNumeroNull(resultado[i].gasto)).toFixed(2), "") +'</td><td>'+ formatNumber.new(parseFloat(corregirNumeroNull(resultado[i].saldo)).toFixed(2), "") +'</td>');

            $("#tablaResultadoAreaReportePresupuestoSaldos tbody").append(fila);

        }

        $("#modalResultadoAreaReporteProyectoSaldos").modal("show");
    }

    $("#excelResultadoAreaReportePresupuesto").on('click', function(){
        fnExcelReportArea();
    })

    $("#excelResultadoAreaReportePresupuestoSaldos").on('click', function(){
        fnExcelReportAreaSaldos();
    })





    function fnExcelReportArea()
    {

        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }

        var table = 'tablaResultadoAreaReportePresupuesto';
        var name = 'REPORTE_POR_AREA_PRESUPUESTER';

        if (!table.nodeType) table = document.getElementById(table)
         var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
         window.location.href = uri + base64(format(template, ctx))

    }

    function fnExcelReportAreaSaldos()
    {

        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }

        var table = 'tablaResultadoAreaReportePresupuestoSaldos';
        var name = 'REPORTE_POR_AREA_PRESUPUESTER_SALDOS';

        if (!table.nodeType) table = document.getElementById(table)
         var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
         window.location.href = uri + base64(format(template, ctx))

    }




    function corregirNull(valor){
        var nuevovalor = "";
        if(valor == null){
          nuevovalor = "";
        } else {
          nuevovalor = valor;
        }
        return nuevovalor;
    }

    function corregirNumeroNull(valor){
        var nuevovalor = "";
        if(valor == null){
          nuevovalor = 0;
        } else {
          nuevovalor = valor;
        }
        return nuevovalor;
    }


    function distinct(array,indice){
      var uniques = []; //temporal
      return array.filter(function(item){
        //indexOf buscará el valor, si no existe retornará true, por lo cual se mantendrá en el arreglo 
        //false en caso el valor ya exista en la variable uniques
        return uniques.indexOf(item[indice]) < 0 ? uniques.push(item[indice]) : false
      })
    }

    function sinduplicados(arr) {
        var i,
            len=arr.length,
            out=[],
            obj={};

        for (i=0;i<len;i++) {
            obj[arr[i]]=0;
        }
        for (i in obj) {
            out.push(i);
        }
        return out;
    }

    var formatNumber = {
     separador: ",", // separador para los miles
     sepDecimal: '.', // separador para los decimales
     formatear:function (num){
      num +='';
      var splitStr = num.split(',');
      var splitLeft = splitStr[0];
      var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
      var regx = /(\d+)(\d{3})/;
      while (regx.test(splitLeft)) {
      splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
      }
      return this.simbol + splitLeft  +splitRight;
     },
     new:function(num, simbol){
      this.simbol = simbol ||'';
      return this.formatear(num);
     }
    }

    function exceltojson(e) {
        var files = e.target.files;
        var f = files[0];
        var X = XLSX;
        {
            var reader = new FileReader();
            //var name = f.name;
            reader.onload = function(e) {
                var data = e.target.result;
                wb = X.read(data, {type: 'binary'});
                var roa = "";
                wb.SheetNames.forEach(function(sheetName) {
                    roa = X.utils.sheet_to_json(wb.Sheets[sheetName]);
                });
                jsonExcel = roa;
                cargaPagoOep(jsonExcel);
            };
            reader.readAsBinaryString(f);
        }
    }

    function removeItemArray ( arr, item ) {
        var i = arr.indexOf( item );
        arr.splice( i, 1 );
    }

    function getLista (array,atributo){
        var nuevoArray = []
        for ( i = 0; i < array.length; i++) {
            valor = array[i][atributo];
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }

    function buscarProcedencia(valor,base){
      for(var i = 0; i < base.length; i++){
        if(base[i].procedencia == valor){
          return base[i].idProcedencia;
        }

      }
    }

    function buscarIdProcedencia(valor,base){
      for(var i = 0; i < base.length; i++){
        if(base[i].procedencia == valor){
          return base[i].idProcedencia;
        }

      }
    }

    function buscarCodigo(valor,base){
      for(var i = 0; i < base.length; i++){
        if(base[i].concatenado == valor){
          return i;
        }
      }
    }

    function uniqueArray(arr){
        let unique_array = []
        for(let i = 0;i < arr.length; i++){
            if(unique_array.indexOf(arr[i]) == -1){
                unique_array.push(arr[i])
            }
        }
        return unique_array
    }


    function goheadfixed(classtable) {
    
        if($(classtable).length) {
    
            $(classtable).wrap('<div class="fix-inner"></div>'); 
            $('.fix-inner').wrap('<div class="fix-outer" style="position:relative; margin:auto;"></div>');
            $('.fix-outer').append('<div class="fix-head"></div>');
            //$('.fix-head').prepend($('.fix-inner').html());
            //$('.fix-head table').find('caption').remove();
            //$('.fix-head table').css('width','100%');
    
            //$('.fix-outer').css('width', $('.fix-inner table').outerWidth(true)+'px');
            //$('.fix-head').css('width', $('.fix-inner table').outerWidth(true)+'px');
            //$('.fix-head').css('height', $('.fix-inner table thead').height()+'px');
    
            // If exists caption, calculte his height for then remove of total
            var hcaption = 0;
            if($('.fix-inner table caption').length != 0)
                hcaption = parseInt($('.fix-inner table').find('caption').height()+'px');

            // Table's Top
            var hinner = parseInt( $('.fix-inner').offset().top );

            // Let's remember that <caption> is the beginning of a <table>, it mean that his top of the caption is the top of the table
            $('.fix-head').css({'position':'absolute', 'overflow':'hidden', 'top': hcaption+'px', 'left':0, 'z-index':100 });
        
            $(window).scroll(function () {
                var vscroll = $(window).scrollTop();

                if(vscroll >= hinner + hcaption)
                    $('.fix-head').css('top',(vscroll-hinner)+'px');
                else
                    $('.fix-head').css('top', hcaption+'px');
            });
    
            /*  If the windows resize   */
            //$(window).resize(goresize);
    
        }
    }

    function goresize() {
        $('.fix-head').css('width', $('.fix-inner table').outerWidth(true)+'px');
        $('.fix-head').css('height', $('.fix-inner table thead').outerHeight(true)+'px');
    }

    function utf8_encode(s) {
      return unescape(encodeURIComponent(s));
    }

    function utf8_decode(s) {
      return decodeURIComponent(escape(s));
    }


}

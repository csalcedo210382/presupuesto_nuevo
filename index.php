<?php 
    error_reporting(0);
    session_start();
    require_once('php/config.php');
    require_once("php/services/ServiceAdministrativo.php");

    $produccion = PRODUCTION_SERVER;

    $usuario = $_SESSION["usuario"];
    $nombre_completo = $_SESSION["nombre_completo"];
    $cargo = $_SESSION["cargo"];
    $estado = $_SESSION["estado"];
 ?>
    
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">

    <title>SISTEMA INTEGRADO UPC</title>


    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <link href="dist/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">
    <!-- Bootstrap Core CSS -->

    <link href="dist/css/bootstrap-select.min.css" rel="stylesheet">
    
    <link href="dist/css/bootstrap-table.css" rel="stylesheet">



    <!--<link href="dist/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />-->
    <script src="dist/js/jquery.min.js"></script>

    <!--<script src="dist/js/fileinput.min.js" type="text/javascript"></script>-->

    <link href="dist/css/sb-admin-2.css" rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <link href="dist/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="dist/css/dataTables.responsive.css" rel="stylesheet">

    <!-- ICONO DE PAGINA -->
    <link rel="icon" type="image/png" href="img/favicon.ico" />

    <!-- Custom Fonts -->
    <link href="dist/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--<link href="dist/css/kendo.common.min.css" rel="stylesheet">  CSS PARA QUE FUNCIONE EL GRAFICO DE KENDO-->

    <link rel="stylesheet" href="dist/css/alertify.core.css" />
    <link rel="stylesheet" href="dist/css/alertify.bootstrap.css" id="toggleCSS" />

    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- ESTILO PERSONALIZADO UPC -->
    <link rel="stylesheet" href="dist/css/all.min.css" />

    <link rel="stylesheet" type="text/css" href="dist/css/style_daca.css">

    <link rel="stylesheet" type="text/css" href="dist/css/form.css">


    <?php if($estado != 1){ ?>
        
        <script type="text/javascript">

        window.open("login.php?r=1","_top");

        </script>

    <?php } ?>

    <script type="text/javascript">

        var produccion = <?php echo $produccion?1:0 ?>;

    </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php
                include("secciones/menu_principal.php");
            ?>
            <!-- /.navbar-top-links -->




            <!--<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">

                                        <h4>
                                        </h4>

                            </div>
                             /input-group -->
                    <!--    </li>
                    </ul>
                    <ul class="nav" id="lista_opciones">  id="side-menu" -->




               <!--     </ul>
                </div>
                 /.sidebar-collapse -->
            <!--</div>-->
             <!--/.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">


        <!-- Tab panes -->

            <div class="tab-content contenedor">
                <br>
                

                <div class="tab-pane active" id="principal">



                    <div class="row">
                        <div class="col-md-12 centro">
                            <img src="img/imagenMenu.jpg" usemap="#menumap">
                            <map name="menumap">
                                <area class="menu_presupuesto_solicitud" target="" alt="INGRESO DE GASTOS" title="INGRESO DE GASTOS" href="#presupuesto_solicitud" data-toggle="tab" coords="217,185,232,178,351,178,367,188,374,201,373,317,365,328,353,336,229,335,217,326,210,314,209,198" shape="poly">
                                <area class="menu_presupuesto_seguimiento" target="" alt="APROBACIONES" title="APROBACIONES" href="#presupuesto_seguimiento" data-toggle="tab" coords="392,185,409,180,527,180,540,189,546,202,546,313,538,327,523,336,405,336,389,327,383,312,382,200" shape="poly">
                                <area class="menu_presupuesto_inicial" target="" alt="REPORTES" title="REPORTES" href="#presupuesto_inicial" data-toggle="tab" data-toggle="tab" coords="563,187,579,180,700,179,713,188,719,205,719,312,712,328,699,336,576,337,563,327,556,313,555,200" shape="poly">
                                <area class="menu_presupuesto_transferencias" target="" alt="TRANSFERENCIAS" title="TRANSFERENCIAS" href="#presupuesto_transferencias" data-toggle="tab" coords="737,186,753,180,872,180,884,187,892,199,892,314,883,328,869,336,746,334,732,323,729,311,729,198" shape="poly">
                                <area class="menu_presupuesto_programacion" target="" alt="ALERTAS" title="ALERTAS" href="#presupuesto_programacion" data-toggle="tab" coords="907,189,924,180,1042,180,1058,189,1065,199,1065,313,1057,328,1043,336,920,335,907,326,901,312,901,203" shape="poly">
                            </map>


                        </div>
                    </div>
                    <!--        
                            <button id="#" type="button" class="btn btn-primary btn-lg botonMenu"> SOLICITUDES </button>
                        </div>
                        <div class="col-md-4 centro">
                            <button id="#" type="button" class="btn btn-primary btn-lg botonMenu"> APROBACIONES </button>
                        </div>
                        <div class="col-md-4 centro">
                            <button id="#" type="button" class="btn btn-primary btn-lg botonMenu"> REPORTES </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 centro texto">
                            <p>Ingreso de solicitudes de pago</p>
                            <p>Estados de solicitudes</p>
                            <p>Formatos predefinidos</p>
                        </div>
                        <div class="col-md-4 centro">

                        </div>
                        <div class="col-md-4 centro">

                        </div>
                    </div>-->

                    <!--<div class="panel panel-primary">
                        <div class="panel-heading">
                            BIENVENIDO AL SISTEMA INTEGRADO UPC
                        </div>-->
                        <!--<div class="panel-body titulo">

                        </div>
                    </div>-->

                </div>


                <?php if($estado == 1){

                    for($i=0 ; $i < $registros ; $i++){
                      include("secciones/modulo_".$data[$i]->enlace.".php");
                    }

                }?>




            </div>
            <!-- /#page-wrapper -->

        </div>
    <!-- /#wrapper -->




        <div class="modal fade" id="modalDesconectarse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">SISTEMA INTEGRAL UPC</h4>
              </div>
              <div class="modal-body">
                    ¿SEGURO DE CERRAR SESION?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
                <a href="login.php?r=1" type="button" class="btn btn-primary btn-sm">ACEPTAR</a>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="modalProcesando" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title centro" id="myModalLabel">PROCESANDO ...</h4>
                    </div>
                    <div class="modal-body">

                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
                        </div>
                    </div>

                    </div>
                    <!--<div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
                        <a href="login.php?r=1" type="button" class="btn btn-primary btn-sm">ACEPTAR</a>
                    </div>-->
                </div>
            </div>
        </div>

    
        <?php if($estado == 1){

            $data = $service->getControles($permisos);
            $registros = count($data); //[0]->idPerfil;

            for($i=0 ; $i < $registros ; $i++){
               include("ventanas/modal_".$data[$i]->control.".php");
            }

        }?>




    <div id="spinner"></div>
    <!-- jQuery -->
    <script src="dist/js/jquery.min.js"></script>
    <!--<script src="dist/js/jquery.fixedtableheader.min.js"></script>

     Bootstrap Core JavaScript -->
    <script src="dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript 
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>-->
    
    <script src="dist/js/alertify.min.js"></script>
    <!-- Custom Theme JavaScript 
    <script type="text/javascript" src="dist/js/sb-admin-2.js"></script>-->
    <script type="text/javascript" src="dist/js/moment.js"></script>
    <script type="text/javascript" src="dist/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="dist/js/bootstrap-datetimepicker.es.js" charset="UTF-8"></script>
    <script type="text/javascript" src="dist/js/date.js"></script>

    <script type="text/javascript" src="dist/js/jquery.auto-complete.js"></script>

    <script type="text/javascript" src="js/vendor/S.js"></script>
    <script type="text/javascript" src="js/vendor/service.js"></script>
    <script type="text/javascript" src="js/vendor/jquery.json-2.4.min.js"></script>



    <!-- <script type="text/javascript" src="js/vendor/kendo/kendo.all.min.js"></script>
    <script type="text/javascript" src="js/vendor/kendo/console.js"></script> -->
    
    <!--Cargar librería para validar formularios -->
    <script type="text/javascript" src="js/vendor/jqueryValidate/jquery.validate.min.js"></script>    
    <script type="text/javascript" src="js/vendor/jqueryValidate/additional-methods.min.js"></script>    
    <script type="text/javascript" src="js/vendor/sweetalert.min.js"></script>
    <script type="text/javascript" src="js/vendor/spin/js/spin.js"></script>

    <script type="text/javascript" src="js/main.js"></script>

    <script type="text/javascript" src="dist/js/cpexcel.js"></script>
    <script type="text/javascript" src="dist/js/shim.js"></script>
    <script type="text/javascript" src="dist/js/jszip.js"></script>
    <script type="text/javascript" src="dist/js/xlsx.js"></script>
    
    
    <script type="text/javascript" src="js/ControlAdministrativo.js"></script>

    <script type="text/javascript" src="dist/js/shieldui-all.min.js"></script>

    <?php if($estado == 1){

        $data = $service->getControles($permisos);
        $registros = count($data); //[0]->idPerfil;

        for($i=0 ; $i < $registros ; $i++){
           echo ('<script type="text/javascript" src="js/Control_'.$data[$i]->control.'.js"></script>');
        }

    }?>

    <script type="text/javascript" src="dist/js/bootstrap-select.min.js"></script>

    <script type="text/javascript" src="dist/js/bootstrap-table.js"></script>

    <!-- DataTables JavaScript -->
    <script type="text/javascript" src="dist/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="dist/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="dist/js/dataTables.responsive.js"></script>

    <script src="dist/js/jquery.maskMoney.js" type="text/javascript"></script>
    
    <!-- <script src="dist/js/jquery.fixedtableheader.js" type="text/javascript"></script>
    Page-Level Demo Scripts - Tables - Use for reference -->
    <script>

        var f = new Date();
        var nmes = (f.getMonth()+1);
        var nanio = f.getFullYear();
        var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
        var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
        var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;
        var decimales = 0;

        function detallePresupuestoTransferencia(valor){

            var objeto = new Object()
                objeto.variable1 = $("select#variable1").val();
                objeto.variable2 = $("select#variable2").val();
                objeto.variable3 = $("select#variable3").val();
                objeto.variable4 = $("select#variable4").val();
                objeto.concatenado = valor;
                objeto.anio = $("#anioPresupuesto").val();

            service.procesar("getDetallePresupuestoTransferencia",objeto,resultadoDetallePresupuestoTransferencia);


            $("#modalDetallePresupuestoTransferencia").modal("show");
        }

        function resultadoDetallePresupuestoTransferencia(evt){

            var presupuestoInicial = evt.presupuesto_inicial[0];
            var presupuestoInicialSuma = evt.presupuesto_inicial_suma;
            var presupuestoInicialResta = evt.presupuesto_inicial_resta;
            var saldo_total = presupuestoInicial.total_registro;
            var contador = 0;
            $("#tablaDetallePresupuestoTransferencia tbody").html("");
            $("#modalDetallePresupuestoTransferencia .cuentaDetallePresupuestoTransferencia").html("");
            $("#modalDetallePresupuestoTransferencia .saldoInicialDetallePresupuestoTransferencia").val("0.00");
            $("#modalDetallePresupuestoTransferencia .saldoFinalDetallePresupuestoTransferencia").val("0.00");

            $("#modalDetallePresupuestoTransferencia .cuentaDetallePresupuestoTransferencia").html( presupuestoInicial.concatenado_titulo.replace("|", " - ") );
            $("#modalDetallePresupuestoTransferencia .saldoInicialDetallePresupuestoTransferencia").val( formatNumber.new( parseFloat(presupuestoInicial.total_registro).toFixed(decimales), "") );

            if ( presupuestoInicialSuma != undefined ){
                for(var i=0; i<corregirNull(presupuestoInicialSuma).length ; i++){
                    saldo_total = parseFloat(saldo_total) + parseFloat(presupuestoInicialSuma[i].total_registro);
                    var fila = $("<tr>");
                    if(parseFloat(presupuestoInicialSuma[i].total_registro) > 0 ){
                        fila.html('<td>'+ (contador + 1) +'</td><td>INGRESO</td><td>'+ presupuestoInicialSuma[i].concatenado_titulo.replace("|", " - ") +'</td><td>'+presupuestoInicialSuma[i].chr_mes+'</td><td>'+ formatNumber.new( parseFloat(presupuestoInicialSuma[i].total_registro).toFixed(decimales), "") +'</td>');
                    }
                    $("#tablaDetallePresupuestoTransferencia tbody").append(fila);
                }
                $("#modalDetallePresupuestoTransferencia .saldoFinalDetallePresupuestoTransferencia").val( formatNumber.new( parseFloat(saldo_total).toFixed(decimales), "") );
            }

            if ( presupuestoInicialResta != undefined ){
                for(var i=0; i<corregirNull(presupuestoInicialResta).length ; i++){
                    saldo_total = parseFloat(saldo_total) - parseFloat(presupuestoInicialResta[i].total_registro);
                    var fila = $("<tr>");
                    if(parseFloat(presupuestoInicialResta[i].total_registro) > 0 ){
                        fila.html('<td>'+ (contador + 1) +'</td><td>EGRESO</td><td>'+ presupuestoInicialResta[i].concatenado_egreso.replace("|", " - ") +'</td><td>'+presupuestoInicialResta[i].chr_mes_egreso+'</td><td>'+ formatNumber.new( parseFloat(presupuestoInicialResta[i].total_registro).toFixed(decimales), "") +'</td>');
                    }
                    $("#tablaDetallePresupuestoTransferencia tbody").append(fila);
                }
                $("#modalDetallePresupuestoTransferencia .saldoFinalDetallePresupuestoTransferencia").val( formatNumber.new( parseFloat(saldo_total).toFixed(decimales), "") );
            }

        }

        var formatNumber = {
         separador: ",", // separador para los miles
         sepDecimal: '.', // separador para los decimales
         formatear:function (num){
          num +='';
          var splitStr = num.split(',');
          var splitLeft = splitStr[0];
          var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
          var regx = /(\d+)(\d{3})/;
          while (regx.test(splitLeft)) {
          splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
          }
          return this.simbol + splitLeft  +splitRight;
         },
         new:function(num, simbol){
          this.simbol = simbol ||'';
          return this.formatear(num);
         }
        }

        function corregirNull(valor){
            var nuevovalor = "";
            if(valor == null){
              nuevovalor = "";
            } else {
              nuevovalor = valor;
            }
            return nuevovalor;
        }



        function Procesando() {
            $("#modalProcesando").modal("show");
        }

        function Terminado() {
            $("#modalProcesando").modal("hide");
        }




    </script>

</body>

</html>

            <!-- PRESUPUESTO DEL AREA -->
            <div class="tab-pane" id="presupuesto_inicial">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                            PLANTILLA INICIAL DE PRESUPUESTO <span id="anioFormato">2017</span>
                    </div>
                    <div class="panel-body">
                        <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-2 texto titulo">RESPONSABLE </div>
                                    <div class="col-md-6"><select id="responsablePresupuesto" class="form-control input-sm"></select></div>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-2 texto titulo"><span id="anioFormato" class="anio">2017</span></div>
                                </div>                        
                                <div class="row">
                                    <div class="col-md-2 texto titulo">TIPO </div>
                                    <div class="col-md-6"><select id="tipoPresupuesto" class="form-control input-sm"></select></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 texto titulo">STRING </div>
                                    <div class="col-md-6"><select id="centrodecostoPresupuesto" class="form-control input-sm"></select></div>
                                    <div class="col-md-1 texto titulo">CATEGORIA </div>
                                    <div class="col-md-3">

                                        <div class="input-group">
                                            <input name="buscar_categoria" placeholder="Buscar" class="form-control input-sm">
                                            <div class="input-group-addon peq">
                                                <button id="modalBuscarCategoria" type="button" class="btn btn-default btn-xm btnBuscar" data-toggle="modal"><span class="glyphicon glyphicon-search"></span></button>
                                            </div>
                                        </div>

                                    </div>








                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                INFORMACIÓN GENERAL
                                            </div>
                                            <div class="panel-body">
                                                <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-4 texto titulo">CÓDIGO STRING </div>
                                                            <div class="col-md-8"><input id="string_sin_per_Presupuesto" class="form-control input-sm" disabled></div>
                                                        </div>                        
                                                        <div class="row">
                                                            <div class="col-md-4 texto titulo">CUENTA GLOBAL </div>
                                                            <div class="col-md-8"><input id="cuenta_categoria_Presupuesto" class="form-control input-sm" disabled></div>
                                                        </div>  
                                                        <div class="row">
                                                            <div class="col-md-4 texto titulo">CUENTA NODO </div>
                                                            <div class="col-md-8"><input id="cuenta_padre_nodo_Presupuesto" class="form-control input-sm" disabled></div>
                                                        </div>                                                          
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                DESCRIPCIÓN
                                            </div>
                                            <div class="panel-body">
                                                <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-4 texto titulo">DES. STRING </div>
                                                            <div class="col-md-8"><input id="nombre_categoria_Presupuesto" class="form-control input-sm" disabled></div>
                                                        </div>   
                                                        <div class="row">
                                                            <div class="col-md-4 texto titulo">DES. CTA GLOBAL </div>
                                                            <div class="col-md-8"><input id="desc_cuenta_categoria_Presupuesto" class="form-control input-sm" disabled></div>
                                                        </div>   
                                                        <div class="row">
                                                            <div class="col-md-4 texto titulo">DES. CTA NODO </div>
                                                            <div class="col-md-8"><input id="rubro_p_l_Presupuesto" class="form-control input-sm" disabled></div>
                                                        </div>                                                                             
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6">

                                        <div class="panel panel-default">

                                            <div class="panel-body">
                                                <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-4 texto titulo">APROBADOR </div>
                                                            <div class="col-md-8"><input id="aprobador_Presupuesto" class="form-control input-sm" disabled></div>
                                                        </div>                                                                              
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">

                                        <div class="panel panel-default">

                                            <div class="panel-body">
                                                <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-4 texto titulo">SEDE </div>
                                                            <div class="col-md-8"><input id="sedes_Presupuesto" class="form-control input-sm" disabled></div>
                                                        </div>                    
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="panel panel-success">
                                    <div class="panel-heading" align="center">
                                    MONTOS EN SOLES INCLUIDO IGV
                                    </div>
                                            <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-1 texto ">ENERO </div>
                                                            <div class="col-md-2"><input type="number" id="enero_presupuesto" placeholder="0.00" class="form-control input-sm" value="0.00"></div>
                                                            <div class="col-md-1 texto ">FEBRERO </div>
                                                            <div class="col-md-2"><input type="number" id="febrero_presupuesto" placeholder="0.00" class="form-control input-sm" value="0.00"></div>
                                                            <div class="col-md-1 texto ">MARZO </div>
                                                            <div class="col-md-2"><input type="number" id="marzo_presupuesto" placeholder="0.00" class="form-control input-sm" value="0.00"></div>
                                                            <div class="col-md-1 texto titulo">Q1 </div>
                                                            <div class="col-md-2"><input id="q1_presupuesto" placeholder="0.00" class="form-control input-sm" value="0.00" disabled></div>
                                                        </div>                        
                                                        <div class="row">
                                                            <div class="col-md-1 texto ">ABRIL </div>
                                                            <div class="col-md-2"><input type="number" id="abril_presupuesto" placeholder="0.00" class="form-control input-sm" value="0.00"></div>
                                                            <div class="col-md-1 texto ">MAYO </div>
                                                            <div class="col-md-2"><input type="number" id="mayo_presupuesto" placeholder="0.00" class="form-control input-sm" value="0.00"></div>
                                                            <div class="col-md-1 texto ">JUNIO </div>
                                                            <div class="col-md-2"><input type="number" id="junio_presupuesto" placeholder="0.00" class="form-control input-sm" value="0.00"></div>
                                                            <div class="col-md-1 texto titulo">Q2 </div>
                                                            <div class="col-md-2"><input id="q2_presupuesto" placeholder="0.00" class="form-control input-sm" value="0.00" disabled></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1 texto ">JULIO </div>
                                                            <div class="col-md-2"><input type="number" id="julio_presupuesto" placeholder="0.00" class="form-control input-sm" value="0.00"></div>
                                                            <div class="col-md-1 texto ">AGOSTO </div>
                                                            <div class="col-md-2"><input type="number" id="agosto_presupuesto" placeholder="0.00" class="form-control input-sm" value="0.00"></div>
                                                            <div class="col-md-1 texto ">SETIEMBRE </div>
                                                            <div class="col-md-2"><input type="number" id="setiembre_presupuesto" placeholder="0.00" class="form-control input-sm" value="0.00"></div>
                                                            <div class="col-md-1 texto titulo">Q3 </div>
                                                            <div class="col-md-2"><input id="q3_presupuesto" placeholder="0.00" class="form-control input-sm" value="0.00" disabled></div>
                                                        </div> 
                                                        <div class="row">
                                                            <div class="col-md-1 texto ">OCTUBRE </div>
                                                            <div class="col-md-2"><input type="number" id="octubre_presupuesto" placeholder="0.00" class="form-control input-sm" value="0.00"></div>
                                                            <div class="col-md-1 texto ">NOVIEMBRE </div>
                                                            <div class="col-md-2"><input type="number" id="noviembre_presupuesto" placeholder="0.00" class="form-control input-sm" value="0.00"></div>
                                                            <div class="col-md-1 texto ">DICIEMBRE </div>
                                                            <div class="col-md-2"><input type="number" id="diciembre_presupuesto" placeholder="0.00" class="form-control input-sm" value="0.00"></div>
                                                            <div class="col-md-1 texto titulo">Q4 </div>
                                                            <div class="col-md-2"><input id="q4_presupuesto" placeholder="0.00" class="form-control input-sm" value="0.00" disabled></div>
                                                        </div> 
                                                        <div class="row">
                                                            <div class="col-md-1 texto "> </div>
                                                            <div class="col-md-2"><button href="#modalLimpiarPresupuesto" type="button" class="btn btn-danger btn-xs" data-toggle="modal">LIMPIAR MESES</button></div>
                                                            <div class="col-md-1 texto "> </div>
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-1 texto "> </div>
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-1 texto titulo">TOTAL </div>
                                                            <div class="col-md-2"><input id="total_presupuesto" placeholder="0.00" class="form-control input-sm" disabled></div>
                                                        </div> 

                                            </div>


                                </div>


                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                    NOMBRE DEL PROYECTO
                                    </div>
                                    <div class="panel-body">                       
                                        <div class="row">
                                            <div class="col-md-12"><textarea class="form-control" rows="1"></textarea></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                    MES DE APROBACIÓN
                                    </div>
                                    <div class="panel-body">                       
                                        <div class="row">
                                            <div class="col-md-12"><textarea class="form-control" rows="1"></textarea></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                    COMENTARIOS
                                    </div>
                                    <div class="panel-body">                       
                                        <div class="row">
                                            <div class="col-md-12"><textarea class="form-control" rows="2"></textarea></div>
                                        </div>
                                    </div>
                                </div>


                                <br>    
                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-4"><button href="#modalGuardarPresupuesto" type="button" class="btn btn-success btn-sm" data-toggle="modal">GUARDAR REGISTRO</button></div>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-2"></div>
                                            <div class="col-md-4"><button href="#modalCancelarPresupuesto" type="button" class="btn btn-danger btn-sm" data-toggle="modal">CANCELAR REGISTRO</button></div>
                                        </div>


                        </div>
                    </div>
                </div>







            </div>
<div class="tab-pane" id="presupuesto_programacion">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-10">MÓDULO DE PROGRAMACIÓN DE TAREAS</div>
            </div>
        </div>
        <div class="panel-body">
            <div class="row" id="divFormTaskManager" style="display: none;">
                <div class="col-md-12" id="divContentFormTaskManager">

                </div>
            </div>
            <div id="divListaTaskManager">
                <div class="row">
                    <div class="col-md-12">&nbsp;</div>
                </div>
                <div class="row">
                    <div class="col-md-10"></div>
                    <div class="col-md-2 text-right"><a class="btn btn-primary" href="#" role="button" onclick="loadFormularioTask(0);return false;"><span class="glyphicon glyphicon-calendar"></span> NUEVA TAREA</a></div>
                </div>
                <div class="row">
                    <div class="col-md-12">&nbsp;</div>
                </div>            
                <div class="row" id="divTableTaskManager">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-hover" id="tableTaskManager" name="tableTaskManager">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>TAREA</th>
                                        <th>DESCRIPCIÓN</th>
                                        <th>TIPO</th>
                                        <th>USUARIO</th>
                                        <th>PERIOCIDAD</th>
                                        <th>FECHA Y HORA</th>
                                        <th>ASUNTO</th>
                                        <th>ESTADO</th>
                                        <th>ACCIONES</th>
                                    </tr>                            
                                </thead>
                                <tbody id="tableTaskManagerBody"></tbody>
                            </table>                        
                        </div>
                    </div>
                </div>                
            </div>            
        </div>
    </div>
</div>
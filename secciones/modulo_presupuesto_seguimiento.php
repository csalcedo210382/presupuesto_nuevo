            <!-- REPORTE DELEGADO -->
            <div class="tab-pane" id="presupuesto_seguimiento">

               <div class="panel panel-primary">
                        <div class="panel-heading">
                            LISTA DE GASTOS POR STRING - NODO - CATEGORIA - SUB CATEGORIA
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <br>
                            <div class="row">
                                <div class="col-md-1 texto negrita">PROVEEDOR</div>
                                <div class="col-md-4">
                                    <select class="form-control input-sm proveedorseguimiento">
                                        <option value="0">---SELECCIONAR---</option>
                                    </select>
                                </div>
                                <div class="col-md-1 texto negrita">FORMATO</div>
                                <div class="col-md-2">
                                    <select class="form-control input-sm formato">
                                        <option value="0">---SELECCIONAR---</option>
                                        <option value='PIN'>PEDIDO INTERNO</option>
                                        <option value='ACT'>ACTA</option>
                                        <option value='SOCA'>SOLICITUD ORDEN DE COMPRA ABIERTA</option>
                                        <option value='OCA'>ORDEN DE COMPRA ABIERTA</option>
                                        <option value='SPRO'>SOLICITUD PROVISION</option>
                                        <option value='PRO'>PROVISION</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control input-sm nformato">
                                        <option value="0">---SELECCIONAR---</option>
                                    </select>
                                </div>
                                <div class="col-md-2"></div>
<!--
                                <div class="col-md-1"></div>
                                <div class="col-md-1 texto negrita">DESDE</div>
                                <div class="col-md-2">
                                    <div class='input-group date' id='datetimepickerPresupuestoFechaReporteDesde'>
                                        <input type='text' id="presupuestoFechaReporteDesde" class="form-control input-sm"></input>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-1 texto negrita">HASTA</div>
                                <div class="col-md-2">
                                    <div class='input-group date' id='datetimepickerPresupuestoFechaReporteHasta'>
                                        <input type='text' id="presupuestoFechaReporteHasta" class="form-control input-sm"></input>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
-->
                                <div class="col-md-2">
                                    <button id="actualizarListaAdminGastosdePresupuesto" type="button" class="btn btn-primary btn-sm btn-block"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> ACTUALIZAR</button>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-1 texto negrita">ESTADO</div>
                                <div class="col-md-1">
                                    <select id="seleccionarEstadoPresupuestoAdm" class="form-control input-sm">
                                        <option value="0">TODOS</option>
                                        <option value="P">POR APROBAR</option>
                                        <option value="A">APROBADO</option>
                                        <option value="R">RECHAZADO</option>
                                    </select>
                                </div>
                                <div class="col-md-1 texto negrita derecha">BUSCAR</div>
                                <div class="col-md-2">
                                    <input placeholder="BUSCAR" class="form-control input-sm textoBuscado"></input>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="tablaListaAdminGastosdePresupuesto" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>AREA</th>
                                                <th>PROYECTO</th>
                                                <th>STRING</th>
                                                <th>PROVEEDOR</th>
                                                <th>COSTO SOLES</th>
                                                <th>FECHA GASTO</th>
                                                <th>DOCUMENTO</th>
                                                <th>FORMATO</th>
                                                <th>NUMERO</th>
                                                <th>ESTADO USUARIO</th>
                                                <th>ESTADO GENERAL</th>
                                                <th>VECES</th>
                                                <th>OPCIONES</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div>
                        <!-- /.panel-body -->
                </div>


                <div class="panel panel-info" id="historicoRegistroPresupuesto">
                        <div class="panel-heading">
                            DETALLE DE APROBACIONES POR REGISTRO
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                            <table id="tablaListaAprobacionesdePresupuesto" class="" cellspacing="0" width="100%">
                                <thead>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>


                        </div>
                        <!-- /.panel-body -->
                </div>





            </div>  

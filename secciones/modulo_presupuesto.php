            <!-- PRESUPUESTO DEL AREA -->
            <div class="tab-pane" id="presupuesto">

                <div class="panel panel-primary">
                        <div class="panel-heading">
                            CONSULTA E INGRESO DE GASTOS POR STRING Y NODO
                        </div>
                        <div class="panel-body" id="seccionesGastoPresupuesto">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs">
                                    <li class="menuGasto active "><a href="#listaIngresoGastoPresupuesto" data-toggle="tab" aria-expanded="false">REGISTROS</a>
                                    </li>
                                    <li class="menuGasto"><a href="#nuevoIngresoGastoPresupuesto" data-toggle="tab" aria-expanded="false">MANTENIMIENTO</a>
                                    </li>
                                    <!--<li class="menuGasto "><a href="#cargaIngresoGastoPresupuesto" data-toggle="tab" aria-expanded="false">INGRESO MASIVO</a>
                                    </li>-->
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="areaGasto tab-pane fade active in" id="listaIngresoGastoPresupuesto">
                                        <br>
                                        <div class="tab-pane">
                                            <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        LISTA DE GASTOS POR STRING - NODO - CATEGORIA - SUB CATEGORIA
                                                    </div>
                                                    <!-- /.panel-heading -->
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">STRING</div>    
                                                            <div class="col-md-3">
                                                                <select id="direccionPresupuesto" class="form-control input-sm">
                                                                    <option value="">---SELECCIONAR---</option>
                                                                </select>

                                                            </div>
                                                            <div class="col-md-1 texto negrita">PROVEEDOR</div>
                                                            <div class="col-md-3">
                                                                <input id="proveedorPresupuesto" name="buscar_proveedor" placeholder="BUSCAR PROVEEDOR" class="form-control input-sm"></input>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">NODO</div>
                                                            <div class="col-md-3">
                                                                <input id="cuentaNodoPresupuesto" name="buscar_nodo" placeholder="BUSCAR NODO" class="form-control input-sm"></input>
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">ÁREA</div>
                                                            <div class="col-md-3">
                                                                <select id="subAreaPresupuesto" class="form-control input-sm">
                                                                    <option value="">---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">S.CATEGORIA</div>    
                                                            <div class="col-md-3">
                                                                <input id="subCategoriaPresupuesto" name="buscar_sub_categoria" placeholder="BUSCAR SUB CATEGORIA" class="form-control input-sm"></input>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="checkbox">
                                                                    <input id="q1Presupuesto" type="checkbox">
                                                                    <label for="q1Presupuesto">Q1</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="checkbox">
                                                                    <input id="q2Presupuesto" type="checkbox">
                                                                    <label for="q2Presupuesto">Q2</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="checkbox">
                                                                    <input id="q3Presupuesto" type="checkbox">
                                                                    <label for="q3Presupuesto">Q3</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="checkbox">
                                                                    <input id="q4Presupuesto" type="checkbox">
                                                                    <label for="q4Presupuesto">Q4</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">ADICIONAL</div>
                                                            <div class="col-md-3"><input id="buscarPresupuesto" name="buscar" placeholder="BUSCAR" class="form-control input-sm"></input></div>
                                                            <div class="col-md-1 texto negrita">ESTADO</div>
                                                            <div class="col-md-2"><select id="seleccionarEstadoPresupuestoBusqueda" class="form-control input-sm">
                                                                <option value="">TODOS</option>
                                                                <option value="INGRESADO">INGRESADO</option>
                                                                <option value="POR APROBAR">POR APROBAR</option>
                                                                <option value="APROBADO">APROBADO</option>
                                                                <option value="CANCELADO">CANCELADO</option></select></div>
                                                            <div class="col-md-2 texto negrita"></div>
                                                            <div class="col-md-3"><button id="buscarSaldoPresupuesto" type="button" data-toggle="modal" class="btn btn-primary btn-sm btn-block">PROCESAR BUSQUEDA</button>
                                                            </div>
                                                        </div>

                                                        <br>


                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <table id="tablaListadeGastosdePresupuesto" class="table table-bordered table-hover">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th>CODIGO</th>
                                                                            <th>STRING</th>
                                                                            <th>NODO</th>
                                                                            <th>ORDEN</th>
                                                                            <th>AREA</th>
                                                                            <th>ITEM</th>
                                                                            <th>CLAVE</th>
                                                                            <th>COSTO_SOLES</th>
                                                                            <th>FECHA</th>
                                                                            <th>PROVEEDOR</th>
                                                                            <!--<th>DESCRIPCION</th>-->
                                                                            <th>ESTADO</th>
                                                                            <th>MOTIVO</th>
                                                                            <th>OPCIONES</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tfoot>
                                                                    </tfoot>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>00</td>
                                                                            <td>NO</td>
                                                                            <td>NO</td>
                                                                            <td>NO</td>
                                                                            <td>NO</td>
                                                                            <td>NO</td>
                                                                            <td>NO</td>
                                                                            <td>NO</td>
                                                                            <td>NO</td>
                                                                            <td>NO</td>
                                                                            <td>NO</td>
                                                                            <!--<td>NO</td>-->
                                                                            <td>NO</td>
                                                                            <td>NO</td>
                                                                            <td>NO</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /.panel-body -->
                                            </div>     
                                        </div>
                                        

                                    </div>
                                    <div class="areaGasto tab-pane fade" id="nuevoIngresoGastoPresupuesto">
                                        <br>
                                        <div class="tab-pane">

                                                  <div class="btn-group">
                                                    <button class="btn btn-primary btn-sm">OPCIONES DE MANTENIMIENTO </button>
                                                    <button class="btn dropdown-toggle btn-primary btn-sm" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                      <li><a class="input-sm" href="#">NUEVO</a></li>
                                                      <li><a class="input-sm" href="#">MODIFICAR</a></li>
                                                      <li><a class="input-sm" href="#">ELIMINAR</a></li>
                                                    </ul>
                                                  </div>

                                                <br><br>

                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        INGRESAR - MODIFICAR - ELIMINAR
                                                    </div>
                                                    <div class="panel-body">

                                                        <div class="row">
                                                            <div class="col-md-12 negrita" id="mensajeFormularioGastoPresupuesto">FORMULARIO INGRESO DE NUEVO REGISTRO</div>  <input id="idPresupuestoGastoPresupuesto" type="hidden">
                                                        </div>
                                                        <br>

                                                        <!--<div class="row saldoPresupuesto">
                                                            <div class="col-md-1 texto negrita">SALDO</div>    
                                                            <div class="col-md-2">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon negrita">Q1</div>
                                                                    <input id="saldoQ1Presupuesto" placeholder="0.00" class="form-control input-sm dinero" disabled>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">                                    
                                                                <div class="input-group">
                                                                    <div class="input-group-addon negrita">Q2</div>
                                                                    <input id="saldoQ2Presupuesto" placeholder="0.00" class="form-control input-sm dinero" disabled>
                                                                </div></div>
                                                            <div class="col-md-2">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon negrita">Q3</div>
                                                                    <input id="saldoQ3Presupuesto" placeholder="0.00" class="form-control input-sm dinero" disabled>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon negrita">Q4</div>
                                                                    <input id="saldoQ4Presupuesto" placeholder="0.00" class="form-control input-sm dinero" disabled>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon negrita">TOTAL</div>
                                                                    <input id="saldoTotalPresupuesto" placeholder="0.00" class="form-control input-sm dinero" disabled>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br><br>-->

                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">STRING</div>    
                                                            <div class="col-md-5">
                                                                <select id="datoDireccionPresupuesto" class="form-control input-sm">
                                                                    <option value="">---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="col-md-1 texto negrita">ÁREA</div>
                                                            <div class="col-md-3">
                                                                <select id="datoSubAreaPresupuesto" class="form-control input-sm">
                                                                    <option value="">---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">NODO</div>
                                                            <div class="col-md-3">
                                                                <input id="datoCuentaNodoPresupuesto" name="buscar_nodo" placeholder="BUSCAR NODO" class="form-control input-sm"></input>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">S.CATEGORIA</div>    
                                                            <div class="col-md-7">
                                                                <input id="datoSubCategoriaPresupuesto" name="buscar_sub_categoria" placeholder="BUSCAR SUB CATEGORIA" class="form-control input-sm"></input>
                                                            </div>
                                                        </div>
                                                        <br>


                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">N.ORDEN</div>    
                                                            <div class="col-md-2"><input id="numeroOrdenGastoPresupuesto" type="text" placeholder="N. ORDEN O PEDIDO" class="form-control input-sm"></div>
                                                            <div class="col-md-1 texto negrita">AUTOGENERADO</div>    
                                                            <div class="col-md-2"><input id="numeroAutogeneradoGastoPresupuesto" type="text" placeholder="CODIGO" class="form-control input-sm" disabled></div>
                                                            <div class="col-md-3"></div>    
                                                            <div class="col-md-3 saldoPresupuesto">
                                                                <!--<div class="input-group">
                                                                    <div class="input-group-addon">SALDO DE STRING</div>
                                                                    <input id="saldoPresupuesto" placeholder="0.00" class="form-control input-sm dinero" disabled>
                                                                </div>-->
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">MONTO</div>    
                                                            <div class="col-md-2"><input id="montoGastoPresupuesto" type="number" placeholder="0.00" class="form-control input-sm"></div>
                                                            <div class="col-md-1">
                                                                <select id="seleccionarMonedaGatoPresupuesto" class="form-control input-sm">
                                                                    <option value="SOLES">SOLES</option>
                                                                    <option value="DOLARES">DOLARES</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita tipocambio">TIPO CAMBIO</div>
                                                            <div class="col-md-1 tipocambio"><input id="tipoCambioGastoPresupuesto" type="number" placeholder="T.C" class="form-control input-sm"></div>
                                                            <div class="col-md-2 tipocambio"><button id="recalcularMontoGastoPresupuesto" type="button" name="nuevo" class="btn btn-primary btn-sm btn-block"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> RECALCULAR</button></div> 
                                                            <div class="col-md-2 texto negrita tipocambio">MONTO TOTAL EN SOLES S/.</div>
                                                            <div class="col-md-2 tipocambio"><input id="totalGastoPresupuesto" type="number" placeholder="NUEVO MONTO" class="form-control input-sm"></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">FECHA </div>
                                                            <div class="col-md-2">
                                                                <div class='input-group date' id='datetimepickerFechaOP'>
                                                                    <input type='text' id="fechaRegistroGastoPresupuesto" class="form-control input-sm"></input>
                                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">PROVEEDOR</div> 
                                                            <div class="col-md-4">
                                                                <input id="proveedorGastoPresupuesto" name="buscar_proveedor" placeholder="BUSCAR PROVEEDOR" class="form-control input-sm"></input>
                                                             </div>  
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">DESCRIPCION</div> 
                                                            <div class="col-md-6"><textarea id="descripcionGastoPresupuesto" placeholder="DESCRIPCION" class="form-control" rows="2"></textarea></div>
                                                            <div class="col-md-1 texto negrita">CLAVE</div>
                                                            <div class="col-md-4"><input id="palablaClavePresupuesto" placeholder="CLAVE" class="form-control input-sm"></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">SUB TIPO</div>
                                                            <div class="col-md-2">
                                                                <div class="input-group">
                                                                    <input id="seleccionarSubTipoPresupuesto" name="buscar_centro_de_costo" placeholder="BUSCAR TIPO" class="form-control input-sm"></input>
                                                                    <div class="input-group-addon">
                                                                    <span href="#modalBuscarSubTipoPresupuesto" type="button" data-toggle="modal" class="glyphicon glyphicon-search"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">ITEM</div>
                                                            <div class="col-md-5">
                                                                <div class="input-group">
                                                                    <input id="seleccionarItemPresupuesto" name="buscar_centro_de_costo" placeholder="BUSCAR ITEM" class="form-control input-sm"></input>
                                                                    <div class="input-group-addon">
                                                                    <span href="#modalBuscarItemPresupuesto" type="button" data-toggle="modal" class="glyphicon glyphicon-search"></span>
                                                                    </div>
                                                                </div>
                                                            </div>                                
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                            </div>
                                                            <div class="col-md-1 texto negrita"></div>
                                                            <div class="col-md-5">
                                                
                                                            </div>                                
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">ESTADO</div>
                                                            <div class="col-md-2"><select id="seleccionarEstadoPresupuesto" class="form-control input-sm"><option value="INGRESADO">INGRESADO</option>
                                                            <option value="APROBADO">APROBADO</option>
                                                            <option value="POR APROBAR">POR APROBAR</option>
                                                            <option value="CANCELADO">CANCELADO</option></select></div>
                                                            <div class="col-md-1 texto negrita motivo">MOTIVO</div>
                                                            <div class="col-md-8 motivo"><textarea id="motivoGastoPresupuesto" placeholder="MOTIVO" class="form-control" rows="2"></textarea></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-8"></div>
                                                            <div class="col-md-2"><button id="enviarDatosGastoPresupuesto" type="button" name="nuevo" class="btn btn-success btn-sm btn-block"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> <span id="procedimientoDatosGastoPresupuesto">GUARDAR</span></button></div>
                                                            <div class="col-md-2"><button id="cancelarDatosGastoPresupuesto" type="button" class="btn btn-danger btn-sm btn-block"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> CANCELAR</button></div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>

                                    <div class="areaGasto tab-pane fade" id="cargaIngresoGastoPresupuesto">
                                    
                                        

                                    </div>

                                </div>
                        </div>

                </div>


            </div>
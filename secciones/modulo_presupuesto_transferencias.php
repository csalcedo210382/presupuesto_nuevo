<div class="tab-pane" id="presupuesto_transferencias">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-2">MÓDULO DE TRANSFERENCIAS</div>
                <div class="col-md-2 text-left">
                    <select name="selectGlobalYear" id="selectGlobalYear" class="form-control" style="width: 150px;height: 20px;padding:0px;" >
                        <option value="2019" selected="selected">2019</option>
                        <option value="2018">2018</option>
                    </select>
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-2 text-right"><!--a href="#" title="Actualizar lista" onclick="actualizarListaCuentas();return false;" style="color:#fff;"><span class="glyphicon glyphicon-refresh"></span> ACTUALIZAR</a--></div>
            </div>
            
        </div>
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="menuSaldos active"><a href="#listaSaldos" data-toggle="tab" aria-expanded="false">SALDOS</a></li>
                <li class="menuMovimientos"><a href="#listaMovimientos" data-toggle="tab" aria-expanded="false">MOVIMIENTOS</a></li>
            </ul>
            <div class="tab-content"><br />
                <div class="tab-pane fade active in" id="listaSaldos">

                    <div class="row" id="divFormPresupuestoTransferencia">
                        <div class="col-md-12" id="divContentPresupuestoTransferencia">
                            


                    <div class="row">
                        <div class="col-md-1 texto negrita derecha">ÁREA</div>
                        <div class="col-md-2">
                            <select class="form-control input-sm area_trans">
                                <option value="0">---SELECCIONAR---</option>
                            </select>
                        </div>
                        <div class="col-md-1 texto negrita derecha">STRING</div>
                        <div class="col-md-3">
                            <select class="form-control input-sm string_trans">
                                <option value="0">---SELECCIONAR---</option>
                            </select>
                        </div>
                        <div class="col-md-1 texto negrita derecha">CUENTA</div>
                        <div class="col-md-2">
                            <select class="form-control input-sm categoria_trans">
                                <option value="0">---SELECCIONAR---</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="button" data-toggle="modal" class="btn btn-primary btn-sm btn-block procesarbusqueda_trans">PROCESAR</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1 texto negrita derecha">PROYECTO</div>    
                        <div class="col-md-2">
                            <select class="form-control input-sm proyecto_trans">
                                <option value="0">---SELECCIONAR---</option>
                            </select>
                        </div>
                        <div class="col-md-1 texto negrita derecha">DETALLE</div>    
                        <div class="col-md-3">
                            <select class="form-control input-sm detalle_trans">
                                <option value="0">---SELECCIONAR---</option>
                            </select>
                        </div>
                        
                    </div>


                        </div>
                    </div>
                    <br><br>
                    <div class="row" id="divTablePresupuestoTransferencia">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover" id="tablePresupuestoTransferencia" name="tablePresupuestoTransferencia">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ÁREA</th>
                                            <th>STRING</th>
                                            <th>CATEGORÍA</th>
                                            <th>PROYECTO</th>
                                            <th>COMENTARIO</th>
                                            <th>MES</th>
                                            <th>PRESUPUESTO</th>
                                            <th>GASTOS</th>
                                            <th>DIFERENCIA</th>
                                            <th>ACCIONES</th>
                                        </tr>                            
                                    </thead>
                                    <tbody id="tablePresupuestoTransferenciaBody"></tbody>
                                </table>                        
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="tab-pane fade" id="listaMovimientos">
                    
                </div>
            </div>
        </div>
    </div>
</div>
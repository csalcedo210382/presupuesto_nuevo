﻿            <!-- PRESUPUESTO DEL AREA -->
            <div class="tab-pane" id="presupuesto_solicitud">

                <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-2 texto">MODULO DE SOLICITUD DE GASTOS</div>
                                <div class="col-md-2">
                                    <select id="anioPresupuestoSolicitud" class="form-control input-sm" style="width: 150px">
                                        <option value="2019">AÑO 2019</option>
                                        <option value="2018">AÑO 2018</option>
                                        <option value="2017">AÑO 2017</option>
                                    </select>
                                </div>
                                <div class="col-md-6"></div>
                                <!--<div class="col-md-2 derecha texto">
                                    <a href="#" title="Actualizar lista" onclick="actualizarModuloSolicitud();return false;" style="color:#fff;"><span class="glyphicon glyphicon-refresh"></span> ACTUALIZAR</a>
                                </div>-->
                            </div>
                            
                        </div>
                        <div class="panel-body" id="seccionesGastoPresupuesto">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs">
                                    <!--<li class="menuGasto active "><a href="#formularioSolicitudGastoPresupuesto2" data-toggle="tab" aria-expanded="false">INGRESO DE SOLICITUDES DE PAGO</a>
                                    </li>-->
                                    <li class="menuGasto"><a href="#seguimientoSolicitudGastoPresupuesto" data-toggle="tab" aria-expanded="false">SOLICITUDES</a>
                                    </li>
                                    <li class="menuGasto"><a href="#formatosSolicitudGastoPresupuesto" data-toggle="tab" aria-expanded="false">FORMATOS</a>
                                    </li>
                                    <!--<li class="menuGasto"><a href="#provisionarSolicitudGastoPresupuesto" data-toggle="tab" aria-expanded="false">ORDEN DE COMPRA ABIERTA - PROVISION</a>
                                    </li>-->
                                </ul>

                                <!-- Tab panes --> 
                               <div class="tab-content">
<!--                                     <div class="areaGasto tab-pane fade active in" id="formularioSolicitudGastoPresupuesto2">
                                        <br>
                                        <div class="tab-pane">


                                                    <div class="panel-body">

                                                        <div class="row">
                                                            
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">CÓDIGO S-</div>    
                                                            <div class="col-md-1 texto negrita" id="idPresupuestoSolicitudGastoPresupuesto">
                                                            </div>
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-1 texto negrita">ESTADO</div>
                                                            <div class="col-md-2">
                                                                <select id="estadoFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                                                                    <option value='INGRESADO'>INGRESADO</option>
                                                                    <option value='POR APROBAR'>POR APROBAR</option>
                                                                    <option value='RECHAZADO'>RECHAZADO</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-1 texto negrita">FECHA :</div>
                                                            <div class="col-md-2 negrita">
                                                                <div class='input-group date' id='datetimepickerFechaSP'>
                                                                    <input type='text' id="fechaFormularioSolicitudGastoPresupuesto" class="form-control input-sm"></input>
                                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">ÁREA</div>    
                                                            <div class="col-md-2">
                                                                <select id="areaFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">STRING</div>    
                                                            <div class="col-md-4">
                                                                <select id="stringFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                                                                    <option value=''>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">CUENTA GLOBAL</div>    
                                                            <div class="col-md-3">
                                                                <select id="categoriaFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                                                                    <option value=''>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">PROYECTO</div>    
                                                            <div class="col-md-5">
                                                                <select id="proyectoFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                                                                    <option value=''>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">DETALLE</div>    
                                                            <div class="col-md-5">
                                                                <select id="detalleFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                                                                    <option value='0'>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>

                                                        </div>
                                                        <br>

                                                        <div class="row ndocumento">
                                                            <div class="col-md-1 texto negrita">N° DOCUMENTO</div>    
                                                            <div class="col-md-2">
                                                                <input type="text" id="ndocumentoFormularioSolicitudGastoPresupuesto" name="buscar_monto" placeholder="N° DE DOCUMENTO" class="form-control input-sm">
                                                            </div>
                                                            <div class="col-md-1 texto negrita datetimepickerFechaFF">FECHA FACTURA</div>
                                                            <div class="col-md-2 datetimepickerFechaFF">
                                                                <div class='input-group date' id='datetimepickerFechaFF'>
                                                                    <input type='text' id="fechaFacturaFormularioSolicitudGastoPresupuesto" class="form-control input-sm"></input>
                                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">PROVEEDOR</div>    
                                                            <div class="col-md-5">
                                                                <select id="proveedorFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                                                                    <option value='0'>---SELECCIONAR---</option>
                                                                </select>                                                                
                                                                <button id="cargarPagoOepSolicitud" href="#modal_cargar_pago_oep_solicitud" type="button" class="btn btn-primary btn-sm btn-block" data-toggle="modal" style="display:none">REGISTRO PERSONAL DE APOYO - OEP</button>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button id="nuevoProveedorPresupuesto" href="#modal_nuevo_proveedor_solicitud" type="button" class="btn btn-primary btn-sm btn-block" data-toggle="modal">NUEVO PROVEEDOR</button>
                                                            </div>
                                                            <div class="col-md-2 texto negrita">
                                                                <p id="tinProveedor"></p>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row">

                                                            <div class="col-md-1 texto negrita">DESCRIPCIÓN</div>    
                                                            <div class="col-md-6">
                                                                <textarea rows="2" id="descripcionFormularioSolicitudGastoPresupuesto" name="buscar_descripcion" placeholder="DESCRIPCION DE LA SOLICITUD" class="form-control input-sm"></textarea>
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">MONTO S/.</div>    
                                                            <div class="col-md-2">
                                                                <input type="number" id="montoFormularioSolicitudGastoPresupuesto" name="buscar_monto" placeholder="0" class="form-control input-sm">
                                                            </div>
                                                            <div class="col-md-1">
                                                                <select id="seleccionarMonedaFormularioSolicitudGastoPresupuesto" class="form-control input-sm">
                                                                    <option value="SOLES">SOLES</option>
                                                                    <option value="DOLARES">DOLARES</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita tipocambio">TIPO CAMBIO</div>
                                                            <div class="col-md-1 tipocambio"><input id="tipoCambioFormularioSolicitudGastoPresupuesto" type="number" placeholder="T.C" class="form-control input-sm"></div>
                                                            <div class="col-md-2 tipocambio"><button id="recalcularFormularioSolicitudGastoPresupuesto" type="button" name="nuevo" class="btn btn-primary btn-sm btn-block"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> RECALCULAR</button></div> 
                                                            <div class="col-md-2 texto negrita tipocambio">MONTO TOTAL EN SOLES S/.</div>
                                                            <div class="col-md-2 tipocambio"><input id="totalFormularioSolicitudGastoPresupuesto" type="number" placeholder="NUEVO MONTO" class="form-control input-sm"></div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
          
                                                            <div class="col-md-2">
                                                                <button id="botonGuardarFormularioSolicitud" href="#" type="button" class="btn btn-success btn-sm btn-block" data-toggle="modal"><span id="procedimientoSolicitudGastoPresupuesto">NUEVO</span></button>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <button id="cancelarGuardarFormularioSolicitud" type="button" class="btn btn-danger btn-sm btn-block" data-toggle="modal">CANCELAR</button>
                                                            </div> 
                                                           
                                                        </div>
                                                    </div>


                                        </div>
                                        

                                    </div>-->
                                    <div class="areaGasto tab-pane fade active in" id="seguimientoSolicitudGastoPresupuesto">
                                        <br>
                                        <div class="tab-pane">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <button id="botonFormularioSolicitudGastoPresupuesto" class="btn btn-primary btn-block btn-sm" href="#formularioSolicitudGastoPresupuesto" data-toggle="modal"> FORMULARIO SOLICITUD </button>
                                                        </div>
                                                        <div class="col-md-10 negrita" id="mensajeFormularioGastoPresupuesto" style="display:none">FORMULARIO INGRESO DE NUEVO REGISTRO</div>
                                                    </div>

                                                    <br>
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">ÁREA</div>
                                                            <div class="col-md-2">
                                                                <select class="form-control input-sm area">
                                                                    <option value="0">---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">STRING</div>
                                                            <div class="col-md-3">
                                                                <select class="form-control input-sm string">
                                                                    <option value="0">---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">CUENTA</div>
                                                            <div class="col-md-2">
                                                                <select class="form-control input-sm categoria">
                                                                    <option value="0">---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button type="button" data-toggle="modal" class="btn btn-primary btn-sm btn-block procesarbusqueda">PROCESAR BUSQUEDA</button>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">PROYECTO</div>    
                                                            <div class="col-md-2">
                                                                <select class="form-control input-sm proyecto">
                                                                    <option value="0">---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">DETALLE</div>    
                                                            <div class="col-md-3">
                                                                <select class="form-control input-sm detalle">
                                                                    <option value="0">---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">PROVEEDOR</div>
                                                            <div class="col-md-2">
                                                                <select class="form-control input-sm proveedor">
                                                                    <option value="0">---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">ESTADO</div>
                                                            <div class="col-md-1">
                                                                <select class="form-control input-sm estado">
                                                                    <option value="0">TODOS</option>
                                                                    <option value='INGRESADO'>INGRESADO</option>
                                                                    <option value='POR APROBAR'>POR APROBAR</option>
                                                                    <option value='APROBADO'>APROBADO</option>
                                                                    <option value='RECHAZADO'>RECHAZADO</option>
                                                                    <option value='CANCELADO'>CANCELADO</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">CARRERA</div>    
                                                            <div class="col-md-4">
                                                                <select class="form-control input-sm carrera">
                                                                    <option value="0">---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3 centro">
                                                                <div class="checkbox">
                                                                    <input id="chkpnuevoSolicitudPresupuesto" type="checkbox">
                                                                    <label for="chkpnuevoSolicitudPresupuesto">MARCAR SI ES DE AMPLIACIÓN DE PRESUPUESTO</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-1 texto negrita">N DOCUMENTO</div>
                                                            <div class="col-md-1">
                                                                <input placeholder="DOCUMENTO" class="form-control input-sm documento"></input>
                                                            </div>
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-1">
                                                                <div class="checkbox">
                                                                    <input id="seguimientoq1" type="checkbox">
                                                                    <label for="seguimientoq1">Q1</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="checkbox">
                                                                    <input id="seguimientoq2" type="checkbox">
                                                                    <label for="seguimientoq2">Q2</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="checkbox">
                                                                    <input id="seguimientoq3" type="checkbox">
                                                                    <label for="seguimientoq3">Q3</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="checkbox">
                                                                    <input id="seguimientoq4" type="checkbox">
                                                                    <label for="seguimientoq4">Q4</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1 texto negrita">FORMATO</div>
                                                            <div class="col-md-2">
                                                                <select class="form-control input-sm formato">
                                                                    <option value="0">---SELECCIONAR---</option>
                                                                    <option value='PIN'>PEDIDO INTERNO</option>
                                                                    <option value='ACT'>ACTA</option>
                                                                    <option value='OCA'>ORDEN DE COMPRA ABIERTA</option>
                                                                    <option value='PRO'>PROVISION</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <select class="form-control input-sm nformato">
                                                                    <option value="0">---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                        </div>                                                  

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <table id="tablaListadeSolicitudGastosdePresupuesto" class="table table-bordered">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th>AREA</th>
                                                                            <th>COD.STRING</th>
                                                                            <th>STRING</th>
                                                                            <th>CTA.GLOBAL</th>
                                                                            <th>C.GLOBAL</th>
                                                                            <th>N.DOCUMENTO</th>
                                                                            <th>MONTO</th>
                                                                            <th>PROVEEDOR</th>
									                                        <th>DESCRIPCION</th>
                                                                            <th>FORMATO</th>
                                                                            <th>ESTADO</th>
                                                                            <th>FECHA</th>
                                                                            <th>OPCIONES</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tfoot>
                                                                    </tfoot>
                                                                    <tbody>
                                                                        
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>




                                        </div>
                                    </div>
                                    <div class="areaGasto tab-pane fade" id="formatosSolicitudGastoPresupuesto">
                                        <div class="tab-pane">

                                            <div class="panel-body">
                                                <br>

                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <button type="button" href="#area_pedido_interno" data-toggle="tab" class="btn btn-default btn-sm btn-block botonGrande boton_pedido_interno">PEDIDO INTERNO</button>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <button type="button" href="#area_acta" data-toggle="tab" class="btn btn-default btn-sm btn-block botonGrande boton_acta">ACTA</button>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <button type="button" href="#provisionarSolicitudGastoPresupuesto" data-toggle="tab" class="btn btn-default btn-sm btn-block botonGrande boton_personal_apoyo">ORDEN DE COMPRA ABIERTA - PROVISION</button>

                                                        <!--<button type="button" href="#area_personal_apoyo" data-toggle="tab" class="btn btn-default btn-sm btn-block botonGrande boton_personal_apoyo">PERSONAL DE APOYO</button>-->
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">

                                                    <div class="tab-content">
                                                        <div class="tab-pane" id="area_pedido_interno">

                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        FORMATO DE PEDIDO INTERNO
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <br>
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <!--<button type="button" class="btn btn-primary btn-sm btn-block boton_nuevo_pedido_interno">NUEVO</button>-->
                                                                            </div>
                                                                        </div>
                                                                        <br>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <table class="table table-bordered table-hover listaFormatosPedidoInterno">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>#</th>
                                                                                            <th>N PEDIDO INTERNO</th>
                                                                                            <th>FECHA EMISIÓN</th>
                                                                                            <th>PROVEEDOR</th>
                                                                                            <th>MONEDA</th>
                                                                                            <th>MONTO</th>
                                                                                            <th>ESTADO</th>
                                                                                            <th>OPCIONES</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                        </div>

                                                        <div class="tab-pane" id="area_acta">

                                                             <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        FORMATO DE ACTAS
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <br>
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <!--<button type="button" class="btn btn-primary btn-sm btn-block boton_nueva_acta">NUEVO</button>-->
                                                                            </div>
                                                                        </div>
                                                                        <br>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <table class="table table-bordered table-hover listaFormatosActa">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>#</th>
                                                                                            <th>N ACTA</th>
                                                                                            <th>FECHA</th>
                                                                                            <th>PROVEEDOR</th>
                                                                                            <th>MONEDA</th>
                                                                                            <th>MONTO</th>
                                                                                            <th>ESTADO</th>
                                                                                            <th>OPCIONES</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                        </div>

                                                        <div class="tab-pane" id="provisionarSolicitudGastoPresupuesto">

                                                             <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        FORMULARIO ORDEN DE COMPRA ABIERTA - PROVISION
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <br>
                                                                        <div class="row">
                                                                            <div class="col-md-2">
                                                                                <button id="nuevoProvisionarSolicitudGastoPresupuesto" class="btn btn-primary btn-block btn-sm" href="#modalProvisionarSolicitudGastoPresupuesto" data-toggle="modal"> NUEVO REGISTRO </button>
                                                                            </div>
                                                                        </div>
                                                                        <br>
                                                                        <div class="row">
                                                                            <div class="col-md-1 texto negrita derecha">TIPO</div>
                                                                            <div class="col-md-2">
                                                                                <select class="form-control input-sm tipo">
                                                                                    <option value="0">---SELECCIONAR---</option>
                                                                                    <option value='SOCA'>SOLICITUD ORDEN DE COMPRA ABIERTA</option>
                                                                                    <option value='SPRO'>SOLICITUD PROVISION</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-1 texto negrita derecha">ESTADO</div>
                                                                            <div class="col-md-1">
                                                                                <select class="form-control input-sm estado">
                                                                                    <option value="0">---SELECCIONAR---</option>
                                                                                    <option value='INGRESADO'>INGRESADO</option>
                                                                                    <option value='POR APROBAR'>POR APROBAR</option>
                                                                                    <option value='APROBADO'>APROBADO</option>
                                                                                    <option value='RECHAZADO'>RECHAZADO</option>
                                                                                    <option value='LIQUIDADO'>LIQUIDADO</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <table class="table table-bordered table-hover tablaProvisionarSolicitudGastoPresupuesto">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>#</th>
                                                                                            <th>TIPO</th>
                                                                                            <th>NUMERO</th>
                                                                                            <th>AREA</th>
                                                                                            <th>STRING</th>
                                                                                            <th>CUENTA</th>
                                                                                            <th>PROYECTO</th>
                                                                                            <th>DETALLE</th>
                                                                                            <th>MONTO SOLICITADO</th>
                                                                                            <th>SALDO</th>
                                                                                            <th>FECHA SOLICITUD</th>
                                                                                            <th>SOLICITANTE</th>
                                                                                            <th>ESTADO</th>
                                                                                            <th>OPCIONES</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                        </div>
                                                    </div>





                                                </div>

                                            </div>

                                        </div>
                                    </div>



                                </div>
                        </div>

                </div>


            </div>
            <!-- PRESUPUESTO DEL AREA -->
            <div class="tab-pane" id="presupuesto_inicial">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        PLANTILLA INICIAL DE PRESUPUESTO <span id="anioFormato"></span>
                    </div>
                    <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" id="seccionesPresupuesto">
                                <!--<li class=""><a href="#ingresomasivopresupuesto" data-toggle="tab" aria-expanded="false">INGRESO MASIVO</a>
                                </li>-->

                                <li class="active"><a href="#listaderegistrospresupuesto" data-toggle="tab" aria-expanded="false">REPORTE DE PRESUPUESTO</a>
                                </li>
                                <li class=""><a href="#configuracionregistrospresupuesto" data-toggle="tab" aria-expanded="false">CONFIGURACIÓN PRESUPUESTO</a>
                                </li>                                
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="listaderegistrospresupuesto">
                                    <br>
                                    <!--INICIO DE LA PARTE ADMINISTRABLE-->

                                    <div class="panel panel-info" id="panelListadeRegistrosdePresupuesto">
                                        <div class="panel-heading">
                                            <select id="anioPresupuesto" class="form-control input-sm" style="width: 150px">
                                              <option value="2019">AÑO 2019</option>
                                              <option value="2018">AÑO 2018</option>
                                              <option value="2017">AÑO 2017</option>
                                            </select>
                                        </div>
                                        <div class="panel-body">

                                                            <div class="row">
                                                                <div class="col-md-1 texto negrita">VARIABLES</div>

                                                                <div class="col-md-1"><select id="variable1" class="form-control input-sm variableFiltro"></select></div>
                                                                <div class="col-md-1"><select id="variable2" class="form-control input-sm variableFiltro"><option value=''>SELECCIONAR</option></select></div>
                                                                <div class="col-md-1"><select id="variable3" class="form-control input-sm variableFiltro"><option value=''>SELECCIONAR</option></select></div>
                                                                <div class="col-md-1"><select id="variable4" class="form-control input-sm variableFiltro"><option value=''>SELECCIONAR</option></select></div>

                                                                <div class="col-md-1 texto derecha"></div> <!-- ESPACIO LIBRE PARA COLOCAR ALGUN BOTON -->

                                                                <div class="col-md-1">
                                                                  <div class="input-group">
                                                                    <span class="input-group-addon textoSistema">Q1</span>
                                                                    <span class="input-group-addon">
                                                                      <input id="chk_q1_presupuesto" type="checkbox" aria-label="Checkbox for following text input">
                                                                    </span>
                                                                  </div>
                                                                </div>

                                                                <div class="col-md-1">
                                                                  <div class="input-group">
                                                                    <span class="input-group-addon textoSistema">Q2</span>
                                                                    <span class="input-group-addon">
                                                                      <input id="chk_q2_presupuesto" type="checkbox" aria-label="Checkbox for following text input">
                                                                    </span>
                                                                  </div>
                                                                </div>

                                                                <div class="col-md-1">
                                                                  <div class="input-group">
                                                                    <span class="input-group-addon textoSistema">Q3</span>
                                                                    <span class="input-group-addon">
                                                                      <input id="chk_q3_presupuesto" type="checkbox" aria-label="Checkbox for following text input">
                                                                    </span>
                                                                  </div>
                                                                </div>

                                                                <div class="col-md-1">
                                                                  <div class="input-group">
                                                                    <span class="input-group-addon textoSistema">Q4</span>
                                                                    <span class="input-group-addon">
                                                                      <input id="chk_q4_presupuesto" type="checkbox" aria-label="Checkbox for following text input">
                                                                    </span>
                                                                  </div>
                                                                </div>

                                                                <div class="col-md-2">
                                                                <button id="buscarRegistroPresupuesto" type="button" class="btn btn-primary btn-sm btn-block">PROCESAR FILTROS</button>
                                                                </div>

                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-1"></div>

                                                                <div class="col-md-1"><select id="subvariable1" class="form-control input-sm variableFiltro" style="display:none"></select></div>
                                                                <div class="col-md-1"><select id="subvariable2" class="form-control input-sm variableFiltro" style="display:none"></select></div>
                                                                <div class="col-md-1"><select id="subvariable3" class="form-control input-sm variableFiltro" style="display:none"></select></div>
                                                                <div class="col-md-1"><select id="subvariable4" class="form-control input-sm variableFiltro" style="display:none"></select></div>

                                                                <div class="col-md-1">
                                                                  <div class="input-group">
                                                                    <input type="button" class="form-control input-sm texto negrita" value="REPORTE">
                                                                    <span class="input-group-addon">
                                                                      <input id="chk_reporte_presupuesto" type="checkbox" aria-label="Checkbox for following text input">
                                                                    </span>
                                                                  </div>
                                                                </div>

                                                                <div class="col-md-1">
                                                                  <div class="input-group">
                                                                    <span class="input-group-addon textoSistema">MESES</span>
                                                                    <span class="input-group-addon">
                                                                      <input id="chk_q1mes_presupuesto" type="checkbox" aria-label="Checkbox for following text input">
                                                                    </span>
                                                                  </div>
                                                                </div>

                                                                <div class="col-md-1">
                                                                  <div class="input-group">
                                                                    <span class="input-group-addon textoSistema">MESES</span>
                                                                    <span class="input-group-addon">
                                                                      <input id="chk_q2mes_presupuesto" type="checkbox" aria-label="Checkbox for following text input">
                                                                    </span>
                                                                  </div>
                                                                </div>

                                                                <div class="col-md-1">
                                                                  <div class="input-group">
                                                                    <span class="input-group-addon textoSistema">MESES</span>
                                                                    <span class="input-group-addon">
                                                                      <input id="chk_q3mes_presupuesto" type="checkbox" aria-label="Checkbox for following text input">
                                                                    </span>
                                                                  </div>
                                                                </div>

                                                                <div class="col-md-1">
                                                                  <div class="input-group">
                                                                    <span class="input-group-addon textoSistema">MESES</span>
                                                                    <span class="input-group-addon">
                                                                      <input id="chk_q4mes_presupuesto" type="checkbox" aria-label="Checkbox for following text input">
                                                                    </span>
                                                                  </div>
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <button id="descargarRegistroPresupuesto" type="button" class="btn btn-success btn-sm btn-block">DESCARGAR REPORTE</button></button>
                                                                </div>

                                                            </div>
                                        </div>
                                    </div>


                                    <div class="panel panel-default" id="panelListadeRegistrosdePresupuesto">
                                        <div class="panel-heading">
                                            (<span id="nregistrosPresupuesto">0</span>) REGISTROS
                                        </div>
                                        <div class="panel-body">

                                          <div class="row">
                                              <!--<div class="col-md-2 texto negrita izquierda reportePresupuesto">ASEGURAMIENTO DE LA CALIDAD ( DACA )</div>-->
                                              <div class="col-md-2"><button id="botonDesplegarDetalledeGasto" type="button" class="btn btn-info btn-sm reporte" style="display: none">MOSTRAR DETALLE DE GASTOS BLOQUEADO   </button></div>
                                              <div class="col-md-3"><input type="text" id="buscarProveedorPersonaldeApoyo" placeholder="BUSCAR PROVEEDOR O PERSONAL DE APOYO" class="form-control input-sm" style="display: none"></div>
                                              <div class="col-md-3"></div>
                                              <div class="col-md-1 texto negrita presupuestado">PRESUPUESTO</div>
                                              <div class="col-md-1 texto negrita ejecutadas">EJECUTADO</div>
                                              <div class="col-md-1 texto negrita pendientes">PENDIENTE</div>
                                              <div class="col-md-1 texto negrita saldo">SALDO</div>
                                          </div>

                                            <!--<table id="tablaListadeRegistrosdePresupuesto" class="table table-bordered table-hover header-fixed">
                                                <thead>
                                                    <tr class="fondoheader sinborde">
                                                        <th class="n">#</th>
                                                        <th class="d">-</th>
                                                        <th class="q1mes" style="display: none;">ENE</th>
                                                        <th class="q1mes" style="display: none;">FEB</th>
                                                        <th class="q1mes" style="display: none;">MAR</th>
                                                        <th class="q1"    style="display: none;">Q1</th>
                                                        <th class="q2mes" style="display: none;">ABR</th>
                                                        <th class="q2mes" style="display: none;">MAY</th>
                                                        <th class="q2mes" style="display: none;">JUN</th>
                                                        <th class="q2"    style="display: none;">Q2</th>
                                                        <th class="q3mes" style="display: none;">JUL</th>
                                                        <th class="q3mes" style="display: none;">AGO</th>
                                                        <th class="q3mes" style="display: none;">SEP</th>
                                                        <th class="q3"    style="display: none;">Q3</th>
                                                        <th class="q4mes" style="display: none;">OCT</th>
                                                        <th class="q4mes" style="display: none;">NOV</th>
                                                        <th class="q4mes" style="display: none;">DIC</th>
                                                        <th class="q4"    style="display: none;">Q4</th>
                                                        <th>TOTAL YEAR</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                </tbody>

                                                <tfoot>
                                                </tfoot>
                                            </table>-->

                                            <table id="tablaReportePresupuesto" class="table"> <!-- table-bordered table-hover header-fixed reportePresupuesto -->
                                                <thead>
                                                </thead>

                                                <tbody>
                                                </tbody>

                                                <tfoot>
                                                </tfoot>
                                            </table>



                                        </div>

                                    </div>


                                </div>


                                <div class="tab-pane fade" id="configuracionregistrospresupuesto">
                                    <br>
                                    <!--INICIO DE LA PARTE ADMINISTRABLE-->

                                    <div class="panel panel-info" id="panelListadeRegistrosdePresupuesto">
                                        <div class="panel-heading">
                                            CONFIGURACIÓN DEL PRESUPUESTO
                                        </div>
                                        <div class="panel-body">

                                        



                                        </div>


                                    </div>

                                </div>

                    </div>
                </div>
            </div>


</div>

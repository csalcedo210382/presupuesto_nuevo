<?php

ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_pdo.php');
require_once(INCDIR.'phpexcel/PHPExcel.php');
require_once("Service.php");

define('PERIOCIDAD_DIARIO', 1);
define('PERIOCIDAD_SEMANAL', 2);
define('PERIOCIDAD_MENSUAL', 3);
define('PERIOCIDAD_TRIMESTRAL', 4);

define('NOTIFICACION_GRUPAL', 1);
define('NOTIFICACION_AREA', 2);

define('NOTIFICACION_ESTADO_PENDIENTE_ENVIAR', 5);

define('TOKEN_DIRECTOR','MANUEL CORTES FONTCUBERTA ABUCCI');
define('TOKEN_ASISTENTE_DIRECTOR','MIRYAM SALAZAR');
//Envío de correos
if(!defined('EMAIL_URL_SERVICE')) {
    define('EMAIL_URL_SERVICE','http://ec2-54-205-192-212.compute-1.amazonaws.com/services/public/api/mail');
}
if(!defined('EMAIL_USER')) {
    define('EMAIL_USER','usrprddaca');
}
if(!defined('EMAIL_PASSWORD')) {
    define('EMAIL_PASSWORD','Da04102018#');
}
if(!defined('EMAIL_CUENTA')) {
    define('EMAIL_CUENTA','daca-reportes@upc.pe');
}
class ServicePresupuestoProgramacion extends Service {

    public function __construct() {
        parent::__construct();
    }

    public function getTareas($request) {
        $error = 0;
        $message = '';
        $html = '';
        $tareas = $this->getListTareas();
        //$this->vd($tareas);die();
        $cont = 0;
        if (is_array($tareas) && count($tareas) > 0) {
            foreach ($tareas as $index => $objTarea) {
                $html .= $this->getHtmlTareas($objTarea, $cont);
                $cont++;
            }
        }

        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                //'tareas' => $tareas,
                'html' => $html
            ]
        ];
    }

    public function loadFormularioTask($request) {
        $error = 0;
        $message = '';
        $html = $this->getHtmlFormularioTask($request->tareaId);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'form' => $html
            ]
        ];
    }

    public function loadDetallePeriocidad($request) {
        $error = 0;
        $message = '';
        $html = $this->getHtmlDetallePeriocidad($request->periocidadId);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'html' => $html,
                'periocidadId' => $request->periocidadId
            ]
        ];
    }

    public function saveAlerta($request) {
        $error = 0;
        $message = '';
        $html = '';
        $this->guardarNotificacion($request);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'html' => $html
            ]
        ];
    }

    public function testNotificacion($request) {
        $error = 0;
        $message = '';
        $html = '';
        $feedback = $this->sendNotificacionMovimiento($request);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'html' => $html,
                'feedback' => $feedback
            ]
        ];
    }

    private function getBodyMail($objRequest) {
        $returnValue = '';
        $returnValue .= '<!DOCTYPE html>';
        $returnValue .= '<html lang="es">';
        $returnValue .= '<body>';
        $returnValue .= '<table>';
        $returnValue .= '<tr>';
        $returnValue .= '<td width="10%"><img src="https://www.upc.edu.pe/sites/all/themes/upc/img/logo.png" /></td>';
        $returnValue .= '<td width="90%"></td>';
        $returnValue .= '</tr>';
        
        $returnValue .= '<tr>';
            $returnValue .= '<td colspan="2">&nbsp;</td>';
        $returnValue .= '</tr>';
        
        $returnValue .= '<tr>';
            $returnValue .= '<td colspan="2">';
            $returnValue .= '<p>'.$this->decodeBody($objRequest).'</p>';
            $returnValue .= '</td>';
        $returnValue .= '</tr>';

        $returnValue .= '</table>';
        $returnValue .= '</body>';
        $returnValue .= '</html>';
        return $returnValue;
    }
    
    private function decodeBody($objRequest){
        $returnValue = $objRequest->txt_body;
        $tokens = $this->getTokens();
        $meses = $this->getMeses();
        //listamos los tokens para buscarlos en el texto de contenido
        foreach($tokens as $indice=>$objToken){
            switch($objToken->chr_value){
                case '[[JEFE_AREA]]':
                    //obtenemos el valor del token
                    $value = $this->getNameJefeArea($objRequest->chr_area_id);
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue);
                    break;
                case '[[MES]]':
                    //obtenemos el nombre del mes
                    $m = date("m");
                    $value = date("F", mktime(0, 0, 0, $m, 10));
                    $returnValue = str_replace($objToken->chr_value,strtoupper($meses[$value]),$returnValue);
                    break;
                case '[[AREA]]':
                    //obtenemos el nombre del área
                    $value = $objRequest->chr_area_name;
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue);
                    break;
                case '[[PORCENTAJE_SALDO_AREA]]':
                    $m = date("m");
                    $value = $this->getSaldoMensualByArea($objRequest->chr_area_id).' %';
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue);                    
                    break;
                case '[[TRIMESTRE]]':
                    $current_month = date("m");
                    $trimestres = $this->getTrimestres();
                    $current_index_month = $this->getIndexMonthTrimestral($current_month);
                    $value = 'Q'.$current_index_month;
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue);                    
                    break;
                case '[[PORCENTAJE_SALDO_TRIMESTRAL_AREA]]':
                    //Indice del trimestre actual
                    $indice = $this->getIndexMonthTrimestral();
                    $value = $this->getSaldoTrimestralByArea($objRequest->chr_area_id, $indice).' %';
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue);                     
                    break;
                case '[[PORCENTAJE_EJECUTADO_MENSUAL]]':
                    $value = $this->getMontoEjecutadoMensual($objRequest->chr_area_id).' %';
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue);
                    break;
                case '[[PORCENTAJE_EJECUTADO_TRIMESTRAL]]':
                    $indice = $this->getIndexMonthTrimestral();
                    $value = $this->getMontoEjecutadoTrimestralByArea($objRequest->chr_area_id, $indice).' %';
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue); 
                    break;
                case '[[DIRECTOR_DACA]]':
                    $value = TOKEN_DIRECTOR;
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue);
                    break;
                 case '[[ASISTENTE_DACA]]':
                    $value = TOKEN_ASISTENTE_DIRECTOR;
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue);                    
                    break;
            }
        }
        return $returnValue;
    }

    private function getMontoEjecutadoMensual($chr_area_id, $m=0){
        $returnValue = 0;
        if($m==0){
            $m = sprintf("%02d", date("m"));
        }
        $objArea = $this->getAreaById($chr_area_id);
        //Calculamos el saldo mensual por area
        $ejecutado_mensual_area = 0;
        if(is_object($objArea)){
            $sql ="SELECT SUM(presupuesto_total) as presupuesto_total FROM mov_saldo WHERE area='".utf8_decode($objArea->area)."' AND mes='".$m."' AND anio='".date("Y")."'";
            $result = $this->db->get_results($sql);
            $objResult = array_shift($result);
            if(is_object($objResult)){
                $ejecutado_mensual_area = floatval($objResult->presupuesto_total);
            }
            //obtenemos lo sumatoria de la sumatoria de los gastado
            $gasto_total = 0; 
            $sql ="SELECT * FROM mov_saldo WHERE AREA='".utf8_decode($objArea->area)."' AND mes ='".$m."' AND anio='".date("Y")."'";
            $result = $this->db->get_results($sql);
            if(is_array($result) && count($result)>0){
                foreach($result as $ind=>$oItem){
                    $gasto_total+=floatval($oItem->total_gasto);
                }
            }
            if($ejecutado_mensual_area==0){
                $ejecutado_mensual_area=1;
            }
            //calculamos el porcentaje
            $returnValue = number_format((($gasto_total/$ejecutado_mensual_area)*100),1);

        }
        return $returnValue;
    }

    private function getMontoEjecutadoTrimestralByArea($chr_area_id, $index_trimestre=1){
        $returnValue=0;
        $trimestres = $this->getTrimestres();
        $meses = $trimestres[$index_trimestre];
        $objArea = $this->getAreaById($chr_area_id);
        //Calculamos el saldo mensual por area
        $presupuesto_trimestral_area = 0;
        if(is_object($objArea)){
            $sql ="SELECT SUM(presupuesto_total) as presupuesto_total FROM mov_saldo WHERE area='".utf8_decode($objArea->area)."' AND mes IN (".implode(',', $meses).") AND anio='".date("Y")."'";
            $result = $this->db->get_results($sql);
            $objResult = array_shift($result);
            if(is_object($objResult)){
                $presupuesto_trimestral_area = floatval($objResult->presupuesto_total);
            }
            //obtenemos lo gastado
            $gasto_total = 0; 
            $sql ="SELECT * FROM mov_saldo WHERE AREA='".utf8_decode($objArea->area)."' AND mes  IN (".implode(',',$meses).") AND anio ='".date("Y")."'";
            $result = $this->db->get_results($sql);
            if(is_array($result) && count($result)>0){
                foreach($result as $ind=>$oItem){
                    $gasto_total+=floatval($oItem->total_gasto);
                }
            }
            //calculamos el porcentaje
            if($presupuesto_trimestral_area==0){
                $presupuesto_trimestral_area= 1;
            }
            $returnValue = number_format((($gasto_total/$presupuesto_trimestral_area)*100),1);                       
           
        }
        return $returnValue;
    }    

    private function getSaldoTrimestralByArea($chr_area_id, $index_trimestre =1){
        $returnValue = 0;
        $trimestres = $this->getTrimestres();
        $meses = $trimestres[$index_trimestre];
        $objArea = $this->getAreaById($chr_area_id);   
        //Calculamos el saldo mensual por area
        $presupuesto_trimestral_area = 0;
        if(is_object($objArea)){
            $sql ="SELECT SUM(presupuesto_total) as presupuesto_total FROM mov_saldo WHERE area='".utf8_decode($objArea->area)."' AND mes IN (".implode(',', $meses).") AND anio='".date("Y")."'";
            $result = $this->db->get_results($sql);
            $objResult = array_shift($result);
            if(is_object($objResult)){
                $presupuesto_trimestral_area = floatval($objResult->presupuesto_total);
            }

            //obtenemos lo sumatoria de la sumatoria de los gastado
            $presupuesto_diferencia_total = 0; 
            $sql ="SELECT * FROM mov_saldo WHERE AREA='".utf8_decode($objArea->area)."' AND mes  IN (".implode(',',$meses).") AND anio ='".date("Y")."'";
            $result = $this->db->get_results($sql); 
            if(is_array($result) && count($result)>0){
                foreach($result as $ind=>$oItem){
                    $presupuesto_diferencia_total+=floatval($oItem->diferencia_total);
                }
            }
            //validamos que el presupuesto diferencial sea negativo
            if($presupuesto_diferencia_total <0){
                $returnValue = '0';
            }else{
                //calculamos el porcentaje
                if($presupuesto_trimestral_area==0){
                    $presupuesto_trimestral_area= 1;
                }
                $returnValue = number_format((($presupuesto_diferencia_total/$presupuesto_trimestral_area)*100),1);                       
            }
        }
                
        return $returnValue;
    }    

    private function getSaldoMensualByArea($chr_area_id,$m=0){
        $returnValue= 0;
        if($m==0){
            $m = sprintf("%02d", date("m"));
        }
        $objArea = $this->getAreaById($chr_area_id);
        //Calculamos el saldo mensual por area
        $presupuesto_mensual_area = 0;
        if(is_object($objArea)){
            $sql ="SELECT SUM(presupuesto_total) as presupuesto_total FROM mov_saldo WHERE area='".utf8_decode($objArea->area)."' AND mes='".$m."' AND anio='".date("Y")."'";
            $result = $this->db->get_results($sql);
            if(is_array($result) && count($result)>0){
                $objResult = array_shift($result);
                $presupuesto_mensual_area = floatval($objResult->presupuesto_total);
            }
            //obtenemos lo sumatoria de la sumatoria de los gastado
            $presupuesto_diferencia_total = 0; 
            $sql ="SELECT * FROM mov_saldo WHERE AREA='".utf8_decode($objArea->area)."' AND mes ='".$m."' AND anio='".date("Y")."'";
            $result = $this->db->get_results($sql);
            if(is_array($result) && count($result)>0){
                foreach($result as $ind=>$oItem){
                    $presupuesto_diferencia_total+=floatval($oItem->diferencia_total);
                }
            }
            //calculamos el porcentaje
            if($presupuesto_diferencia_total < 0){
                $returnValue = '0';
            }else{
                //calculamos el porcentaje
                $returnValue = number_format((($presupuesto_diferencia_total/$presupuesto_mensual_area)*100),1);
            }            
            
        }
        return $returnValue;
        
    }

    private function getAreaById($chr_area_id){
        $returnValue = null;
        $sql = "SELECT * FROM presupuesto_area WHERE chr_area_id='".$chr_area_id."'";
        $result = $this->db->get_results($sql); 
        if(is_array($result) && count($result)>0){
            $returnValue = array_shift($result);
        }
        return $returnValue;
    }

    private function getNameJefeArea($chr_area_id){
        $returnValue = '';
        $arrayValue = [];
        //obtenemos los usuarios que
        $sql ="SELECT u.* 
                FROM usuario u
                WHERE u.cargo='jefe de area' AND usuario IN (SELECT usuario FROM usuario_area WHERE AREA LIKE '%".$chr_area_id."%')";
        $usuarios = $this->db->get_results($sql);     
        if(is_array($usuarios) && count($usuarios)>0){
            foreach($usuarios as $index=>$objUsuario){
                array_push($arrayValue,$objUsuario->nombre_completo);
            }
        }
        $returnValue = strtoupper(implode(', ',$arrayValue));           
        return $returnValue;
    }

    private function getAreas(){
        $returnValue = [];
        $sql = "SELECT * FROM presupuesto_area";
        $returnValue = $this->db->get_results($sql);
        return $returnValue;
    }

    private function sendNotificacionMovimiento($objRequest) {
        //verificamos que tipo de notificación es:
        $url = EMAIL_URL_SERVICE;
        $from = EMAIL_CUENTA.',Presupuester,'.EMAIL_CUENTA.','.EMAIL_PASSWORD;        
        if($objRequest->int_categoryid == NOTIFICACION_GRUPAL){
            $areas = $this->getAreas();
            //itereamos las áreas para enviar una por cada área
            foreach($areas as $index=>$objArea){
                $objRequest->chr_area_id = $objArea->chr_area_id;
                $objRequest->chr_area_name = $objArea->area;
                $params = [
                    'server' => 'office365',
                    'from' => $from,
                    'to' => $objRequest->chr_email,
                    //'to' => $this->getCorreosByMovimiento($movimientoId), //'edmin.huamani@upc.pe,carlos.salcedo@upc.pe,jimy.paredes@upc.pe',
                    'subject' => $objRequest->chr_subject,
                    'body' => $this->getBodyMail($objRequest)
                ];
                //$this->vd($params);die();
                $this->curl_post_async($url, $params);   
                //rompemos en la primera iteración solo para test
                //break;             
            }
        }else{
            //obtenemos el código del área a enviar
            $objRequest->chr_area_id = 1;
            $objRequest->chr_area_name = '';

        }
    }

    private function vd($var){
        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }

    private function curl_post_async($url, $params) {
        foreach ($params as $key => &$val) {
            if (is_array($val))
                $val = implode(',', $val);
            $post_params[] = $key . '=' . urlencode($val);
        }
        $post_string = implode('&', $post_params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'curl');
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);
    }

    public function activeNotification($request) {
        $error = 0;
        $message = '';
        $html = '';
        $sql = "UPDATE ntf_tarea SET is_active='" . $request->is_active . "', date_timemodified='" . time() . "' WHERE id ='" . $request->id . "'";
        $this->db->query($sql);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'html' => $html
            ]
        ];
    }

    private function guardarNotificacion($request) {
        $returnValue = $request->tareaId;
        if ($request->tareaId > 0) {
            //update
            $sql = "UPDATE ntf_tarea  SET ";
            $sql .= "chr_name = '" . $request->chr_name . "', ";
            $sql .= "txt_description = '" . $request->chr_description . "', ";
            $sql .= "date_timemodified = '" . time() . "', ";
            $sql .= "int_periocidadId = '" . $request->int_periocidadId . "', ";
            $sql .= "chr_hora = '" . $request->chr_hora . "', ";
            $sql .= "chr_dia_semana = '" . $request->chr_dia_semana . "', ";
            $sql .= "chr_dia_mes = '" . $request->chr_dia_mes . "', ";
            $sql .= "chr_mes = '" . $request->chr_mes . "', ";
/*            $sql .= "chr_to = '" . $request->chr_to . "', ";
            $sql .= "chr_from = '" . $request->chr_from . "', ";*/
            $sql .= "chr_subject = '" . $request->chr_subject . "', ";
            $sql .= "txt_body = '" . $request->txt_body . "', ";
            $sql .= "int_categoryid = '" . $request->int_categoryid . "' ";
            $sql .= " WHERE id='" . $request->tareaId . "'";
            $res_nuevoregistro = $this->db->query($sql);
            $returnValue = $request->tareaId;
        } else {
            ///insert
            $sql = "INSERT INTO ntf_tarea (chr_name, txt_description, date_timecreated, int_creatorid, int_typeid, int_periocidadId, chr_hora, chr_dia_semana, chr_dia_mes, chr_mes, chr_subject, txt_body, int_categoryid, int_statusid) ";
            $sql .= "VALUES('" . $request->chr_name . "','" . $request->chr_description . "','" . time() . "','" . $this->getUserid() . "','" . $request->int_typeId . "','" . $request->int_periocidadId . "','" . $request->chr_hora . "','" . $request->chr_dia_semana . "','" . $request->chr_dia_mes . "', '" . $request->chr_mes . "','" . $request->chr_subject . "','" . $request->txt_body . "', '".$request->int_categoryid."','".NOTIFICACION_ESTADO_PENDIENTE_ENVIAR."')";
            $res_nuevoregistro = $this->db->query($sql);
            if ($res_nuevoregistro) {
                $returnValue = $this->db->insert_id;
            }
        }
        return $returnValue;
    }

    private function getHtmlDropdownHoras($h = 0) {
        $returnValue = '';
        $horas = $this->getHoras();
        $returnValue .= '<div class="form-group">
                            <label for="inputHora" class="control-label">HORA</label>
                            <div class="divFeedbackCustom">';
        $returnValue .= '<select name="inputHora" id="inputHora" class="form-control">';
        $returnValue .= '<option value="">[ SELECCCIONE UNA HORA ]</option>';
        foreach ($horas as $index => $hora) {
            if ($h == $hora) {
                $returnValue .= '<option value="' . $hora . '" selected="selected">' . $hora . '</option>';
            } else {
                $returnValue .= '<option value="' . $hora . '">' . $hora . '</option>';
            }
        }
        $returnValue .= '</select>';
        $returnValue .= '</div>
                            </div>';
        return $returnValue;
    }

    private function getHtmlDropdownDias($d = 0) {
        $returnValue = '';
        $dias = $this->getDias();
        $returnValue .= '<div class="form-group">
                                <label for="inputDia" class="control-label">DÍA</label>
                                <div class="divFeedbackCustom">';
        $returnValue .= '<select name="inputDia" id="inputDia"  class="form-control">';
        $returnValue .= '<option value="">[ SELECCCIONE UN DÍA ]</option>';
        foreach ($dias as $index => $dia) {
            if ($d == $index) {
                $returnValue .= '<option value="' . $index . '" selected="selected">' . $dia . '</option>';
            } else {
                $returnValue .= '<option value="' . $index . '">' . $dia . '</option>';
            }
        }
        $returnValue .= '</select>';
        $returnValue .= '</div></div>';
        return $returnValue;
    }

    private function getHtmlDropdownDiaMes($dm = 0) {
        $returnValue = '';
        $returnValue .= '<div class="form-group">
                            <label for="inputDiaMes" class="control-label">DÍA</label>
                            <div class="divFeedbackCustom">';
        $returnValue .= '<select name="inputDiaMes" id="inputDiaMes"  class="form-control">';
        $returnValue .= '<option value="">[ SELECCCIONE UN DÍA ]</option>';
        for ($i = 0; $i < 30; $i++) {
            if ($dm == ($i + 1)) {
                $returnValue .= '<option value="' . ($i + 1) . '" selected="selected">' . ($i + 1) . '</option>';
            } else {
                $returnValue .= '<option value="' . ($i + 1) . '">' . ($i + 1) . '</option>';
            }
        }
        $returnValue .= '</select>';
        $returnValue .= '</div>
                            </div>';
        return $returnValue;
    }

    private function getHtmlDropdownMes($m = 0) {
        $returnValue = '';
        $returnValue .= '<div class="form-group">
                                <label for="c" class="control-label">MES</label>
                                <div class="divFeedbackCustom">';
        $returnValue .= '<select name="inputMes" id="inputMes"  class="form-control">';
        $returnValue .= '<option value="">[ SELECCCIONE UN MES ]</option>';
        for ($i = 0; $i < 3; $i++) {
            if ($m == ($i + 1)) {
                $returnValue .= '<option value="' . ($i + 1) . '" selected="selected">Mes ' . ($i + 1) . '</option>';
            } else {
                $returnValue .= '<option value="' . ($i + 1) . '">Mes ' . ($i + 1) . '</option>';
            }
        }
        $returnValue .= '</select>';
        $returnValue .= '</div>
                                </div>';
        return $returnValue;
    }

    /**
     * genera el html para los inputs detalle del tipo de periodo que se selecciona
     * @param int $periocidadId
     * @return string
     */
    private function getHtmlDetallePeriocidad($periocidadId) {
        $returnValue = '';
        switch ($periocidadId) {
            case PERIOCIDAD_DIARIO:
                $returnValue .= $this->getHtmlDropdownHoras();
                break;
            case PERIOCIDAD_SEMANAL:
                $returnValue .= $this->getHtmlDropdownHoras();
                $returnValue .= $this->getHtmlDropdownDias();
                break;
            case PERIOCIDAD_MENSUAL:
                $returnValue .= $this->getHtmlDropdownHoras();
                $returnValue .= $this->getHtmlDropdownDiaMes();
                break;
            case PERIOCIDAD_TRIMESTRAL:
                $returnValue .= $this->getHtmlDropdownHoras();
                $returnValue .= $this->getHtmlDropdownDiaMes();
                $returnValue .= $this->getHtmlDropdownMes();
                break;
        }
        return $returnValue;
    }
    
    private function getCategories(){
        $returnValue = [];
        $sql = "SELECT * FROM ntf_category WHERE is_active=1";
        $returnValue = $this->db->get_results($sql);    
        return $returnValue;
    }

    /**
     * Genera el html del formulario para administrar una tarea
     * @param int $tareaId
     * @return string
     */
    private function getHtmlFormularioTask($tareaId) {
        $returnValue = '';
        $periodos = $this->getPeriodos();
        $tokens = $this->getTokens();
        //cargamos el regiistro de la  tarea
        $objTarea = $this->getTareaById($tareaId);
        $categories = $this->getCategories();
        $returnValue .= '</form>';
        $returnValue .= '<form name="formCrudTaskManager" id="formCrudTaskManager" action="" method="post" onsubmit="return false;">
                        <div class="form-group">
                            <h4>DATOS GENERALES</h4><hr>
                        </div>
                        <div class="form-group">
                          <label for="inputTitle" class="control-label">NOMBRE ALERTA</label>
                          <div class="divFeedbackCustom">
                            <input type="text" class="form-control" id="inputTitle" name="inputTitle" value="' . $objTarea->chr_name . '" />
                          </div>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="inputCategory" id="inputCategory" value="'.$objTarea->int_categoryid.'" />
                        </div>
                        <div class="form-group">
                          <label for="inputPeriodo" class="control-label">PERIODICIDAD</label>
                          <div class="divFeedbackCustom">
                            <select name="inputPeriodo" id="inputPeriodo" class="form-control">';
        $returnValue .= '<option value="">[ SELECCIONE UNA OPCIÓN ]</option>';
        if (is_array($periodos) && count($periodos) > 0) {
            foreach ($periodos as $index => $objPeriodo) {
                if ($objPeriodo->id == $objTarea->int_periocidadId) {
                    $returnValue .= '<option value="' . $objPeriodo->id . '" selected="selected">' . $objPeriodo->chr_name . '</option>';
                } else {
                    $returnValue .= '<option value="' . $objPeriodo->id . '">' . $objPeriodo->chr_name . '</option>';
                }
            }
        }
        $returnValue .= '</select>
                            </div>
                        </div>';
        $returnValue .= '<div id="divDetallePeriocidad">';
        //llamamos a los combos de los detalles de periocidad
        if ($tareaId > 0) {
            switch ($objTarea->int_periocidadId) {
                case PERIOCIDAD_DIARIO:
                    $returnValue .= $this->getHtmlDropdownHoras($objTarea->chr_hora);
                    //$returnValue .= $this->getHtmlDropdownDias($objTarea->chr_dia_semana);
                    break;
                case PERIOCIDAD_SEMANAL:
                    $returnValue .= $this->getHtmlDropdownHoras($objTarea->chr_hora);
                    $returnValue .= $this->getHtmlDropdownDias($objTarea->chr_dia_semana);
                    break;
                case PERIOCIDAD_MENSUAL:
                    $returnValue .= $this->getHtmlDropdownHoras($objTarea->chr_hora);
                    $returnValue .= $this->getHtmlDropdownDiaMes($objTarea->chr_dia_mes);
                    break;
                case PERIOCIDAD_TRIMESTRAL:
                    $returnValue .= $this->getHtmlDropdownHoras($objTarea->chr_hora);
                    $returnValue .= $this->getHtmlDropdownDiaMes($objTarea->chr_dia_mes);
                    $returnValue .= $this->getHtmlDropdownMes($objTarea->chr_mes);
                    break;
            }
        }
        //$returnValue .= '';
        $returnValue .= '</div>';
        $returnValue .= '<div class="form-group">
                          <label for="inputDescription" class="control-label">DESCRIPCIÓN</label>
                          <div class="divFeedbackCustom">
                            <textarea name="inputDescription" id="inputDescription" class="form-control">' . $objTarea->txt_description . '</textarea>
                          </div>
                        </div>';
        $returnValue .= '<div class="form-group">
                            <h4>PLANTILLA</h4><hr>
                        </div>';
/*        $returnValue .= '<div class="form-group">
                          <label for="inputFrom" class="control-label">De:</label>
                          <div class="divFeedbackCustom">
                            <input type="text" name="inputFrom" id="inputFrom" class="form-control" placeholder="[[JEFE_DACA]]" value="' . $objTarea->chr_from . '" />
                          </div>
                        </div>';*/
/*        $returnValue .= '<div class="form-group">
                          <label for="inputTo" class="control-label">Para:</label>
                          <div class="divFeedbackCustom">
                            <input type="text" name="inputTo" id="inputTo" class="form-control" placeholder="[[JEFE_SICA]]" value="' . $objTarea->chr_to . '" />
                          </div>
                        </div>';*/
        $returnValue .= '<div class="form-group">
                          <label for="inputSubject" class="control-label">Asunto:</label>
                          <div class="divFeedbackCustom">
                            <input type="text" name="inputSubject" id="inputSubject" class="form-control" value="' . $objTarea->chr_subject . '" />
                          </div>
                        </div>';
        $returnValue .= '<div class="form-group">
                          <label for="inputTemplate" class="control-label">PLANTILLA</label>
                          <div class="divFeedbackCustom">
                            <textarea name="inputTemplate" id="inputTemplate" class="form-control" rows="15">' . $objTarea->txt_body . '</textarea>
                          </div>
                        </div>';
        $returnValue .= '<div class="form-group">
                            <h4>TOKENS</h4><hr>
                        </div>';
        $returnValue .= '<ul class="list-group">';
        if (is_array($tokens) && count($tokens) > 0) {
            foreach ($tokens as $index => $objToken) {
                //var_dump($objToken->chr_name);
                $returnValue .= '<li class="list-group-item">' . utf8_encode($objToken->chr_shortname) . '</li>';
            }
        }

        $returnValue .= '</ul>';
        $returnValue .= '<input type="hidden" name="tareaId" id="tareaId" value="' . $tareaId . '" />
                        <input type="hidden" name="typeId" id="typeId" value="1" />
                        <a class="btn btn-danger" href="#" role="button" onclick="cancelarFormularioNotificacion();return false;">CANCELAR</a>
                        <a class="btn btn-primary" href="#" role="button" onclick="testNotificacion();return false;">VERIFICAR CORREO</a>
                        <button type="submit" class="btn btn-success">GUARDAR</button>
                        <br><br>
                      </form>';
        return $returnValue;
    }

    private function getTareaById($tareaId) {
        $returnValue = NULL;
        if ($tareaId > 0) {
            $sql = "SELECT * FROM ntf_tarea WHERE id='" . $tareaId . "'";
            $result = $this->db->get_results($sql);
            if (is_array($result) && count($result) > 0) {
                $result = array_shift($result);
                if (is_object($result)) {
                    $returnValue = $result;
                }
            }
        } else {
            $returnValue = new stdClass();
            $returnValue->id = 0;
            $returnValue->chr_name = '';
            $returnValue->txt_description = '';
            $returnValue->is_active = 1;
            $returnValue->is_deleted = 0;
            $returnValue->date_timecreated = time();
            $returnValue->date_timemodified = time();
            $returnValue->int_creatorid = $this->getUserid();
            $returnValue->int_typeid = 1;
            $returnValue->int_periocidadId = 0;
            $returnValue->chr_hora = '';
            $returnValue->chr_dia_semana = '';
            $returnValue->chr_dia_mes = '';
            $returnValue->chr_mes = '';
            $returnValue->chr_to = '';
            $returnValue->chr_from = '';
            $returnValue->chr_subject = '';
            $returnValue->txt_body = '';
            $returnValue->int_categoryid = 1;
        }
        return $returnValue;
    }

    /**
     * Lista los periodos activos para las notificaciones
     * @return array
     */
    private function getPeriodos() {
        $returnValue = [];
        $sql = "SELECT * FROM ntf_periodo WHERE is_active=1";
        $returnValue = $this->db->get_results($sql);
        return $returnValue;
    }

    private function getTokens() {
        $returnValue = [];
        $sql = "SELECT * FROM ntf_token WHERE is_active=1";
        $returnValue = $this->db->get_results($sql);
        return $returnValue;
    }

    private function getTrimestres(){
        return [
            '1'=>[1,2,3],
            '2'=>[4,5,6],
            '3'=>[7,8,9],
            '4'=>[10,11,12]
        ];
    }

    private function getIndexMonthTrimestral($current_month=0){
        $returnValue = 0;
        if($current_month == 0){
            $current_month = date("m");
        }
        $trimestres = $this->getTrimestres();
        //itereamos los trimestres para identiificar a que trimestre funciona
        foreach($trimestres as $index=>$arrayTrimestre){
            //ubicamos nuestro mes actual
            if(in_array($current_month,$arrayTrimestre)){
                $returnValue = $index;
                break;
            }
        }
        return $returnValue;
    }    

    /**
     * Contruir los rows para la tabla del listado de tareas en el dashboard
     * @param array $tareas
     * @param int $cont
     * @return string
     */
    private function getHtmlTareas($objRow, $cont) {
        $returnValue = '';
        $returnValue .= '<tr>';
        $returnValue .= '<td>' . ($cont + 1) . '</td>';
        $returnValue .= '<td>' . $objRow->chr_name . '</td>';
        $returnValue .= '<td>' . $objRow->txt_description . '</td>';
        $returnValue .= '<td>' . $objRow->tipo . '</td>';
        $returnValue .= '<td>' . $objRow->nombre_completo . '</td>';
        $returnValue .= '<td>' . $objRow->periocidad . '</td>';
        $fechaHora = '';
        switch($objRow->int_periocidadId){
            case PERIOCIDAD_TRIMESTRAL:
                $indice = $this->getIndexMonthTrimestral();
                $trimestres = $this->getTrimestres();
                $m = $trimestres[$indice][($objRow->chr_mes-1)];
                $m = sprintf("%02d", $m);
                $d = sprintf("%02d", $objRow->chr_dia_mes);
                $fechaHora = date("Y-".$m."-".$d." ".$objRow->chr_hora.":00:00");
                break;
            case PERIOCIDAD_MENSUAL:
                $d = sprintf("%02d", $objRow->chr_dia_mes);
                $h = sprintf("%02d", $objRow->chr_hora);
                $fechaHora = date("Y-m-".$d." ".$h.":00:00");
                break;
            case PERIOCIDAD_DIARIO:
                $h = sprintf("%02d", $objRow->chr_hora);
                $fechaHora = date("Y-m-d ".$h.":00:00");
                break;
        }
        $returnValue .= '<td>' . $fechaHora . '</td>';
        $returnValue .= '<td>' . $objRow->chr_subject . '</td>';
        if ($objRow->is_active) {
            $returnValue .= '<td><a href="#" onclick="activeNotification(' . $objRow->id . ',0);return false;"><span class="glyphicon glyphicon-eye-open"></span> ACTIVO</a></td>';
        } else {
            $returnValue .= '<td><a href="#" onclick="activeNotification(' . $objRow->id . ',1);return false;"><span class="glyphicon glyphicon-eye-close"></span> DESACTIVO</a></td>';
        }
        $returnValue .= '<td>';
        $returnValue .= '<a href="#" class="presupuestoTransferencia_transferir" onclick="loadFormularioTask(' . $objRow->id . ');return false;"><span class="glyphicon glyphicon-pencil"></span> Editar</a>';
        //$returnValue .= '<a href="#" class="presupuestoTransferencia_transferir" onclick="showLogs(' . $objRow->id . ');return false;"><span class="glyphicon glyphicon-list-alt"></span> Ver logs</a>';
        $returnValue .= '</td>';
        $returnValue .= '</tr>';
        return $returnValue;
    }

    /**
     * Listar todas las tareas registradas
     * @return array
     */
    private function getListTareas() {
        $returnValue = [];
        $sql = "SELECT t.* 
                , u.usuario as nombre_completo,
                p.chr_name AS periocidad,
                nt.chr_name AS tipo
                FROM ntf_tarea t
                INNER JOIN ntf_periodo p ON p.id = t.int_periocidadId
                INNER JOIN usuario u ON u.idUsuario = t.int_creatorid
                INNER JOIN ntf_type nt ON nt.id = t.int_typeid";
        $returnValue = $this->db->get_results($sql);
        return $returnValue;
    }

    /**
     * Método para obtener el ID del usuario en session
     * @return int
     */
    private function getUserid() {
        $returnValue = NULL;
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if (isset($_SESSION['logeado'])) {
            $returnValue = $_SESSION['logeado'];
        }
        return $returnValue;
    }

    private function getDias() {
        return [
            '1' => 'Lunes',
            '2' => 'Martes',
            '3' => 'Miércoles',
            '4' => 'Jueves',
            '5' => 'Viernes',
            '6' => 'Sábado',
            '7' => 'Domingo'
        ];
    }

    private function getHoras() {
        return [
            '01',
            '02',
            '03',
            '04',
            '05',
            '06',
            '07',
            '08',
            '09',
            '10',
            '11',
            '12',
            '13',
            '14',
            '15',
            '16',
            '17',
            '18',
            '19',
            '20',
            '21',
            '22',
            '23',
        ];
    }

    private function getMeses(){
        return [
            'January' => 'enero',
            'February' => 'febrero',
            'March' => 'marzo',
            'April' => 'abril',
            'May' => 'mayo',
            'June' => 'junio',
            'July' => 'julio',
            'August' => 'agosto',
            'September' => 'septiembre',
            'October' => 'octubre',
            'November' => 'noviembre',
            'December' => 'diciembre'
        ];
    }

}

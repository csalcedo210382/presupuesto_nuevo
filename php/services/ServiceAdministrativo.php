<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_pdo.php');
require_once("Service.php");

class ServiceAdministrativo extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}

//PARTE INICIAL DE ADMINISTRATIVO	

	function getMenu($permisos){

		$sql = "SELECT idPerfil,enlace,titulo FROM permisos WHERE permiso IN ($permisos)";
		$res = $this->db->get_results($sql);
		return $res;
	}

	function getControles($permisos){

		$sql = "SELECT distinct control FROM permisos WHERE permiso IN ($permisos)";
		$res = $this->db->get_results($sql);
		return $res;
	}

//PARTE DE DELEGADO
	function getEncuestasReporte($usuario){
		$sql = "SELECT ASIGNATURA, NOMBRE_ASIGNA, SECCION, DOCENTE, NOMBRES_DOCENTE,
		(SELECT IF(REPORTE_DELEGADO.COD_DELEGADO IS NOT NULL,'TERMINADO','') 
		FROM REPORTE_DELEGADO WHERE REPORTE_DELEGADO.COD_DELEGADO = base_delegados.USUARIO 
		AND REPORTE_DELEGADO.COD_CURSO = base_delegados.ASIGNATURA 
		AND REPORTE_DELEGADO.COD_DOCENTE = base_delegados.DOCENTE 
		AND REPORTE_DELEGADO.SECCION = base_delegados.SECCION) AS ESTADO 
		FROM base_delegados
		WHERE USUARIO = '".$usuario."'";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("NOMBRE_ASIGNA","NOMBRES_DOCENTE"));
		return $res;
	}


	function getEncuestasEnsayo01($usuario){
		$sql = "SELECT ASIGNATURA, NOMBRE_ASIGNA, SECCION, DOCENTE, NOMBRES_DOCENTE,
		(SELECT IF(ENSAYO_DELEGADO_PARTE1.COD_DELEGADO IS NOT NULL,'TERMINADO','') 
		FROM ENSAYO_DELEGADO_PARTE1 WHERE ENSAYO_DELEGADO_PARTE1.COD_DELEGADO = base_delegados.USUARIO 
		AND ENSAYO_DELEGADO_PARTE1.COD_CURSO = base_delegados.ASIGNATURA 
		AND ENSAYO_DELEGADO_PARTE1.COD_DOCENTE = base_delegados.DOCENTE 
		AND ENSAYO_DELEGADO_PARTE1.SECCION = base_delegados.SECCION) AS ESTADO 
		FROM base_delegados
		WHERE USUARIO = '".$usuario."'";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("NOMBRE_ASIGNA","NOMBRES_DOCENTE"));
		return $res;
	}
	function getEncuestasEnsayo02($usuario){
		$sql = "SELECT COD_DELEGADO FROM ensayo_delegado_parte2
		WHERE COD_DELEGADO = '".$usuario."'";
		$res = $this->db->get_results($sql);
		return $res;
	}
	function getEncuestasWeb($usuario){
		$sql = "SELECT COD_DELEGADO FROM encuesta_delegado_web
		WHERE COD_DELEGADO = '".$usuario."'";
		$res = $this->db->get_results($sql);
		return $res;
	}

	function getListaPeriodoEncuestaDelegados(){
		$sql = "SELECT COD_PERIODO FROM BASE_DELEGADOS GROUP BY COD_PERIODO ORDER BY COD_PERIODO DESC";
		$res = $this->db->get_results($sql);
		return $res;
	}

	function getReportes(){
		$sql0 = "SELECT COUNT(DISTINCT USUARIO) AS RESULTADO FROM BASE_DELEGADOS";
		$resTotal = $this->db->get_results($sql0);
		$sql1 = "SELECT COUNT(COD_DELEGADO) AS RESULTADO FROM ENSAYO_DELEGADO_PARTE1";
		$resEnsayo1 = $this->db->get_results($sql1);
		$sql2 = "SELECT COUNT(COD_DELEGADO) AS RESULTADO FROM ENSAYO_DELEGADO_PARTE2";
		$resEnsayo2 = $this->db->get_results($sql2);
		$sql3 = "SELECT COUNT(COD_DELEGADO) AS RESULTADO FROM ENCUESTA_DELEGADO_WEB";
		$resEncuesta = $this->db->get_results($sql3);
		    
		    $dato = new stdClass();
            $dato->total = $resTotal;
            $dato->ensayo1 = $resEnsayo1;
            $dato->ensayo2 = $resEnsayo2;
            $dato->encuesta = $resEncuesta;

		return $dato;
	}


	function getReporteJefes(){

		$meslargo = date("m");
		$mescorto = date("n");
		$numeromeses = array("01", "02", "03", "04","05","06","07","08","09","10","11","12");
		$nombremeses = array("ENERO", "FEBRERO", "MARZO", "ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SETIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE");

		$sqlUsuarios = "SELECT A.usuario, A.nombre, B.area FROM usuario A LEFT JOIN usuario_area B
						ON A.usuario = B.usuario
						WHERE A.cargo = 'JEFE DE AREA'";
		$resUsuarios = $this->db->get_results($sqlUsuarios);
		//return $res;

		$registros = array();

		for ($i=0; $i < count($resUsuarios); $i++) {

			$usuario = $resUsuarios[$i]->nombre;

			$arregloareas = explode(',', $resUsuarios[$i]->area);

			for ($x=0; $x < count($arregloareas); $x++) { 
				$area = $arregloareas[$x];
				$nomarea = $this->getDato("area","presupuesto_area","idArea = '$area' limit 1");

				$mes = strtolower($nombremeses[$mescorto-1]);
				
				$sqlIngreso = "SELECT SUM($mes) mtotal FROM presupuesto_ingreso
								WHERE area = '$nomarea'
								GROUP BY area";

				$resIngreso = $this->db->get_results($sqlIngreso);

				$sqlEgreso = "SELECT SUM(total) mtotal  FROM presupuesto_gasto
								WHERE area = '$nomarea' AND mes = '$meslargo'
								GROUP BY area";
				$resEgreso = $this->db->get_results($sqlEgreso);				

				$registro = new stdClass();
		        $registro->nombre = $usuario;
		        $registro->area = $nomarea;
		        $registro->mes = $nombremeses[$mescorto-1];
		        if(count($resIngreso) > 0){ $ingreso = $resIngreso[0]->mtotal; }else{ $ingreso = 0; }
		        if(count($resEgreso) > 0){ $egreso = $resEgreso[0]->mtotal;	}else{ $egreso = 0; }

		        if($ingreso > 0 && $egreso >= 0 && $ingreso >= $egreso){
					$registro->porcentaje = ROUND(100-(($egreso / $ingreso)*100),1);
					//echo $nomarea."ROUND(100-((".$egreso." / ".$ingreso.")*100),1)";
		        }else{
		        	$registro->porcentaje = 0;
		        }
		        

		        $registros[] = $registro;

			}


		}

		return $registros;
	}

	function getReporteJefesQ(){

		$meslargo = date("m");
		$mescorto = date("n");
		$numeromeses = array("01", "02", "03", "04","05","06","07","08","09","10","11","12");
		$nombremeses = array("ENERO", "FEBRERO", "MARZO", "ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SETIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE");

		$sqlUsuarios = "SELECT A.usuario, A.nombre, B.area FROM usuario A LEFT JOIN usuario_area B
						ON A.usuario = B.usuario
						WHERE A.cargo = 'JEFE DE AREA'";
		$resUsuarios = $this->db->get_results($sqlUsuarios);
		//return $res;

		$registros = array();

		if($mescorto == 3 || $mescorto == 6 || $mescorto == 9 || $mescorto == 12){

			$Q = $mescorto / 3;

			for ($i=0; $i < count($resUsuarios); $i++) {

				$usuario = $resUsuarios[$i]->nombre;

				$arregloareas = explode(',', $resUsuarios[$i]->area);

				for ($x=0; $x < count($arregloareas); $x++) { 
					$area = $arregloareas[$x];
					$nomarea = $this->getDato("area","presupuesto_area","idArea = '$area' limit 1");

					$mes1 = strtolower($nombremeses[$mescorto-1]);
					$mes2 = strtolower($nombremeses[$mescorto-2]);
					$mes3 = strtolower($nombremeses[$mescorto-3]);

					$sqlIngreso = "SELECT (SUM($mes1) + SUM($mes2) + SUM($mes3)) mtotal FROM presupuesto_ingreso
									WHERE area = '$nomarea'
									GROUP BY area";

					$resIngreso = $this->db->get_results($sqlIngreso);

					$nmes1 = strtolower($numeromeses[$mescorto-1]);
					$nmes2 = strtolower($numeromeses[$mescorto-2]);
					$nmes3 = strtolower($numeromeses[$mescorto-3]);

					$sqlEgreso = "SELECT SUM(total) mtotal  FROM presupuesto_gasto
									WHERE area = '$nomarea' AND mes IN ('$nmes1','$nmes2','$nmes3')
									GROUP BY area";

					$resEgreso = $this->db->get_results($sqlEgreso);				

					$registro = new stdClass();
			        $registro->nombre = $usuario;
			        $registro->area = $nomarea;
			        $registro->trimestre = "Q-".$Q;
			        if(count($resIngreso) > 0){ $ingreso = $resIngreso[0]->mtotal; }else{ $ingreso = 0; }
			        if(count($resEgreso) > 0){ $egreso = $resEgreso[0]->mtotal;	}else{ $egreso = 0; }

			        if($ingreso > 0 && $egreso >= 0 && $ingreso >= $egreso){
						$registro->porcentaje = ROUND(100-(($egreso / $ingreso)*100),1);
						//echo $nomarea."ROUND(100-((".$egreso." / ".$ingreso.")*100),1)";
			        }else{
			        	$registro->porcentaje = 0;
			        }
			        
			        $registros[] = $registro;

				}

			}

		}

		return $registros;
	}




}	
?>
<?php

ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_pdo.php');
require_once(INCDIR.'phpexcel/PHPExcel.php');
require_once("Service.php");

define('ESTADO_ORDEN_MOVIMIENTO_CREADO', 1);
define('ESTADO_ORDEN_MOVIMIENTO_EN_PROCESO', 2);
define('ESTADO_ORDEN_MOVIMIENTO_FINALIZADA', 6);
define('ESTADO_MOVIMIENTO_CREADA', 3);
define('ESTADO_MOVIMIENTO_ANULADA', 5);
define('ESTADO_MOVIMIENTO_PENDIENTE_POR_EJECUTAR', 7);
define('ESTADO_MOVIMIENTO_EJECUTADO', 8);
define('ESTADO_MOVIMIENTO_RECHAZADO', 9);

define('TABLE_INGRESO', 'mst_presupuesto_ingreso'); //mst_presupuesto_ingreso
define('TABLE_GASTO', 'presupuesto_gasto');
define('VISTA_SALDO', 'mov_saldo_presupuesto');

define('APROBADOR','MANUEL CORTES FONTCUBERTA ABUCCI');
define('CORREO_DIRECTOR','manuel.cortesfontcuberta@upc.pe');
define('CORREO_ASISTENTE_DIRECTOR','miryam.salazar@upc.pe');
//Envío de correos
if(!defined('EMAIL_URL_SERVICE')) {
    define('EMAIL_URL_SERVICE','http://ec2-54-205-192-212.compute-1.amazonaws.com/services/public/api/mail');
}
if(!defined('EMAIL_USER')) {
    define('EMAIL_USER','usrprddaca');
}
if(!defined('EMAIL_PASSWORD')) {
    define('EMAIL_PASSWORD','Da04102018#');
}
if(!defined('EMAIL_CUENTA')) {
    define('EMAIL_CUENTA','daca-reportes@upc.pe');
}
class ServicePresupuestoTransferencias extends Service {

    function __construct() {
        parent::__construct();
    }

    function getAreasTransferencias(){ 
        $sql = "SELECT chr_area, chr_area_id FROM presupuesto_area
                GROUP BY chr_area_id
                ORDER BY chr_area";
        $res = $this->db->get_results($sql);
        $this->_codificarObjeto($res,array("chr_area"));
        return $res;
    }

    function getStringTransferencias($data){
        $anio = $data->anio;
        $area = $data->area;

        $sql="SELECT chr_string_id, UPPER(string) string FROM presupuesto_ingreso
              WHERE anio = $anio AND chr_area_id = '$area'
              GROUP BY chr_string_id
              ORDER BY string ASC";

        $res = $this->db->get_results($sql);
        $this->_codificarObjeto($res,array("string"));
        return $res;
    }

    function getCategoriasTransferencias($data){
        $anio = $data->anio;
        $area = $data->area;
        $string = $data->string;

        $sql="SELECT chr_categoria_id, UPPER(categoria) categoria FROM presupuesto_ingreso
              WHERE anio = $anio AND chr_area_id = '$area' AND chr_string_id = '$string'
              GROUP BY chr_categoria_id
              ORDER BY categoria ASC";
        $res = $this->db->get_results($sql);
        $this->_codificarObjeto($res,array("categoria"));
        return $res;
    }

    function getProyectosTransferencias($data){
        $anio = $data->anio;
        $area = $data->area;
        $string = $data->string;
        $categoria = $data->categoria;
        $sql="SELECT UPPER(proyecto) proyecto, chr_proyecto_id FROM presupuesto_ingreso
                WHERE anio = $anio AND chr_area_id = '$area' AND chr_string_id = '$string' AND chr_categoria_id = '$categoria'
                GROUP BY chr_proyecto_id
                ORDER BY proyecto ASC";
        $res = $this->db->get_results($sql);
        $this->_codificarObjeto($res,array("proyecto"));
        return $res;
    }

    function getDetallesTransferencias($data){
        $anio = $data->anio;
        $area = $data->area;
        $string = $data->string;
        $categoria = $data->categoria;
        $proyecto = $data->proyecto;

        $sql="SELECT UPPER(PC.chr_comentario) chr_comentario, PI.chr_comentario_id FROM 
                presupuesto_ingreso PI LEFT JOIN presupuesto_comentario PC ON PC.chr_comentario_id = PI.chr_comentario_id
                WHERE PI.anio = $anio AND PI.chr_area_id = '$area' AND PI.chr_string_id = '$string' AND PI.chr_categoria_id = '$categoria' AND PI.chr_proyecto_id = '$proyecto'
                GROUP BY PC.chr_comentario_id
                ORDER BY PC.chr_comentario ASC";

        $res = $this->db->get_results($sql);
        $this->_codificarObjeto($res,array("chr_comentario"));
        return $res;
    }







    public function getCuentas($request) {

        $vanio = $request->year;

        $varea = $request->area;
        $vstring = $request->string;
        $vcategoria = $request->categoria;
        $vproyecto = $request->proyecto;
        $vdetalle = $request->detalle;

        $error = 0;
        $message = '';
        //if (!isset($request->year)) {
        //    $request->year = date("Y");
        //}
        //$areas = $this->getAreas($request->year);
        $sql="CALL proc_mov_saldo_presupuesto('$varea', '$vstring', '$vcategoria')";
        $res=$this->db->query($sql);

        $html = '';
        $cont = 0;
        //if (is_array($areas) && count($areas) > 0) {
            //foreach ($areas as $index => $objArea) {
                $presupuesto_mensual = $this->getPresupuestoMensualByArea($varea, $vstring, $vcategoria, $vproyecto, $vdetalle, $vanio);
                if (is_array($presupuesto_mensual) && count($presupuesto_mensual) > 0) {
                    foreach ($presupuesto_mensual as $indice => $presupuesto) {
                        $presupuesto->int_mes = $presupuesto->mes;
                        $presupuesto->mes = $this->monthToString($presupuesto->mes);   
                        //array_push($cuentas, $presupuesto);
                        $html .= $this->getHtmlRowTable($presupuesto, $cont);
                        $cont++;
                    }
                }
            //}
        //}
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                //'cuentas' => $cuentas,
                'html' => $html
            ]
        ];        

    }
    
    private function vd($var){
        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }

    public function listarMovimientos($request) {
        $error = 0;
        $message = '';
        if (!isset($request->year)) {
            $request->year = date("Y");
        }        
        $html = $this->getHtmlListMovimiento($request->year);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'html' => $html
            ]
        ];
    }

    /**
     * Método para registrar la ejecución de un movimiento
     * $request->movimientoId ID del movimiento
     * $request->status = 8;(Movimiento Ejecutado)
     * $request->status = 9;(Movimiento rechazado)
     * $request->feedback = algún mensaje del proceso de gasto
     * @param object $request
     * @return json
     */
    public function movimientoTransferenciaEjecutado($request) {
        $error = 0;
        $message = '';
        $html = '';
        //construimos los valores por defecto
        if (!isset($request->feedback)) {
            $request->feedback = '';
        }
        //construimos los valores por defecto
        if (!isset($request->status)) {
            $request->status = ESTADO_MOVIMIENTO_EJECUTADO;
        } else {
            //validar que el estado sea el ejecutado o rechazado
            if (!in_array($request->status, [ESTADO_MOVIMIENTO_EJECUTADO, ESTADO_MOVIMIENTO_RECHAZADO])) {
                $error = 1;
                $message = 'El código de estado debe ser 8(Ejecutado) o 9(Rerchazado)';
            }
        }
        if (!$error) {
            //validamos que todos parametros tengan datos
            if (is_numeric($request->movimientoId) && $request->movimientoId > 0) {
                //Actualizamos los movimientos al estado pendiente por ejecutar
                $sql = "UPDATE mov_movimiento SET int_statusid='" . $request->status . "', txt_feedback='" . $request->feedback . "' WHERE id='" . $request->movimientoId . "'";
                $this->db->query($sql);
                $message = 'Se ejecutó el movimiento satisfactoriamente';
                //Registramos los logs para el movimiento
                //$this->setLogMovimiento($request->movimientoId); PENDIENTE LOG
                //enviamos un correo a Carlos para notificar que hay movimiento en estado por ejecutar
                //$this->sendNotificacionMovimiento($request->movimientoId);            
            } else {
                $error = 1;
                $message = 'El ID del estado debe ser un númerico';
            }
        }
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'request' => $request,
            'data' => [
                'html' => $html
            ]
        ];
    }

    public function ejecutarTransferencia($request) {
        $error = 0;
        $message = '';
        $html = '';
        //Actualizamos los movimientos al estado ejecutado
        $sql = "UPDATE mov_movimiento SET int_statusid='" . ESTADO_MOVIMIENTO_EJECUTADO . "' WHERE id='" . $request->movimientoId . "'";
        $this->db->query($sql);
        //$message = 'Se envió el movimiento a un estado pendiente por ejecutar';
        $message = 'Se ejecutó la transferencia';
        //Registramos los logs para el movimiento
        //$this->setLogMovimiento($request->movimientoId); PENDIENTE LOG
        //enviamos un correo a Carlos para notificar que hay movimiento en estado por ejecutar
        //$this->sendNotificacionMovimiento($request->movimientoId);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'html' => $html
            ]
        ];
    }

    private function sendNotificacionMovimiento($movimientoId) {
        $objMovimiento = $this->getMovimientoById($movimientoId);
        $url = EMAIL_URL_SERVICE;
        $from = EMAIL_CUENTA.',Presupuester,'.EMAIL_CUENTA.','.EMAIL_PASSWORD;        
        switch ($objMovimiento->int_statusid) {
            case ESTADO_MOVIMIENTO_EJECUTADO:
                $params = [
                    'server' => 'office365',
                    'from' => $from,
                    //'to' => 'edmin.huamani@upc.pe,carlos.salcedo@upc.pe,jimy.paredes@upc.pe',
                    //'to' => 'edmin.huamani@upc.pe',
                    'to' => $this->getCorreosByMovimiento($movimientoId), //'edmin.huamani@upc.pe,carlos.salcedo@upc.pe,jimy.paredes@upc.pe',
                    'subject' => 'Reporte de transferencia - Presupuester',
                    'body' => $this->getBodyMail($objMovimiento)
                ];
                //var_dump($params);
                $this->curl_post_async($url, $params);
                break;
        }
    }

    /**
     * Método para obtener los correos que corresponde a cada jefe de área
     *
     * @param int $movimientoId
     * @return string
     */
    private function getCorreosByMovimiento($movimientoId){
        //$returnValue = 'edmin.huamani@upc.pe,carlos.salcedo@upc.pe,jimy.paredes@upc.pe';
        $returnValue = ['edmin.huamani@upc.pe','carlos.salcedo@upc.pe','jimy.paredes@upc.pe'];
//        $sql = "SELECT DISTINCT chr_correo 
//                FROM usuario u
//                WHERE u.cargo='jefe de area' AND usuario IN (SELECT usuario FROM usuario_area WHERE AREA IN (
//                SELECT idArea FROM presupuesto_area WHERE AREA IN (SELECT 
//                m.chr_area
//                FROM mov_movimiento m
//                WHERE m.id=".$movimientoId."
//                UNION
//                SELECT md.chr_area FROM mov_movimiento_detalle md
//                WHERE md.int_movimiento_id = ".$movimientoId.")
//                ))";
//        $correos = $this->db->get_results($sql);
        //obtenemos los ID de las áreas
        $sql_id_areas = "SELECT chr_area_id FROM presupuesto_area WHERE AREA IN (
			SELECT 
			m.chr_area
			FROM mov_movimiento m
			WHERE m.id='".$movimientoId."'
			UNION
			SELECT md.chr_area FROM mov_movimiento_detalle md
			WHERE md.int_movimiento_id = '".$movimientoId."'
		)";
        $array_id_areas = $this->db->get_results($sql_id_areas);
        //iteramos las areas para juntar los correos
//        if(is_array($array_id_areas) && count($array_id_areas)>0){
//            foreach($array_id_areas as $index=>$aArea){
//                $objArea = $this->getAreaById($aArea->idArea);
//                if(is_object($objArea)){
//                    $sql = "SELECT * FROM usuario WHERE cargo ='jefe de area' AND usuario IN (SELECT usuario FROM usuario_area WHERE AREA LIKE '%".$aArea->idArea."%')";
//                    $result = $this->mysql->query($sql)->fetchAll(PDO::FETCH_OBJ);
//                    if(is_array($result) && count($result)>0){
//                        foreach($result as $index=>$objUsuario){
//                            if(!is_null($objUsuario->chr_correo) && strlen(trim($objUsuario->chr_correo))>0){
//                                if(!in_array($objUsuario->chr_correo,$returnValue)){
//                                    array_push($returnValue,$objUsuario->chr_correo);
//                                }
//                            }
//                        }
//                    }
//                }                
//            }
//        }

        //verificamos si el movimiento contiene strings diferentes
        if($this->tieneAreasDiferentes($movimientoId)){
            //agregamos los correo del director y la asistente
            array_push($returnValue, CORREO_DIRECTOR);
            array_push($returnValue, CORREO_ASISTENTE_DIRECTOR);
        }
        //varificamos si el movimiento se realizó en diferentes proyectos
        if($this->tieneProyectosDiferentes($movimientoId)){
            //agregamos los correo del director y la asistente
            array_push($returnValue, CORREO_DIRECTOR);
            array_push($returnValue, CORREO_ASISTENTE_DIRECTOR);
        }
        //eliminamos los elementos repetidos
        if(is_array($returnValue) && count($returnValue)>0){
            $returnValue = array_unique($returnValue);
        }        
        return implode(',',$returnValue);
    }

    private function tieneProyectosDiferentes($movimientoId){
        $returnValue = FALSE;
        $arrayValue = [];
        $sql = "SELECT 
                DISTINCT m.chr_proyecto
                FROM mov_movimiento m
                WHERE m.id=".$movimientoId."
                UNION
                SELECT DISTINCT md.chr_proyecto FROM mov_movimiento_detalle md
                WHERE md.int_movimiento_id = ".$movimientoId;
        $areas = $this->db->get_results($sql);
        if(is_array($areas) && count($areas)>0){
            foreach($areas as $index=>$area){
                array_push($arrayValue, $area->chr_proyecto);
            }
        }
        if(is_array($arrayValue) && count($arrayValue)>0){
            $arrayValue = array_unique($arrayValue);
            if(is_array($arrayValue) && count($arrayValue)>1){
                $returnValue = TRUE;
            }
        }
        return $returnValue;        
    }

    /**
     * Verificamos si son de strings diferentes
     *
     * @param int $movimientoId
     * @return boolean
     */
    private function tieneAreasDiferentes($movimientoId){
        $returnValue = FALSE;
        $arrayValue = [];
        $sql = "SELECT 
                DISTINCT m.chr_string
                FROM mov_movimiento m
                WHERE m.id=".$movimientoId."
                UNION
                SELECT DISTINCT md.chr_string FROM mov_movimiento_detalle md
                WHERE md.int_movimiento_id = ".$movimientoId;
        $areas = $this->db->get_results($sql);
        if(is_array($areas) && count($areas)>0){
            foreach($areas as $index=>$area){
                array_push($arrayValue, $area->chr_area);
            }
        }
        if(is_array($arrayValue) && count($arrayValue)>0){
            $arrayValue = array_unique($arrayValue);
            if(is_array($arrayValue) && count($arrayValue)>1){
                $returnValue = TRUE;
            }
        }
        return $returnValue; 
    }

    private function getCorreosByMovimiento_old($movimientoId) {
        //$returnValue = 'edmin.huamani@upc.pe,carlos.salcedo@upc.pe,jimy.paredes@upc.pe';
        $returnValue = 'edmin.huamani@upc.pe';
        $array_id_area = [];
        $array_area = [];
        $objMovimiento = $this->getMovimientoById($movimientoId);
        if (is_object($objMovimiento)) {
            array_push($array_area, $objMovimiento->chr_area);
        }
        //buscamos en los detalles
        $detalle_movimientos = $this->getMovimientosById($movimientoId);
        if (is_array($detalle_movimientos) && count($detalle_movimientos) > 0) {
            foreach ($detalle_movimientos as $index => $oMovimiento) {
                if (!in_array($oMovimiento->chr_area, $array_area)) {
                    array_push($array_area, $oMovimiento->chr_area);
                }
            }
        }
        //obtenemos los ID's
        if (is_array($array_area) && count($array_area) > 0) {
            foreach ($array_area as $indice => $strArea) {
                $objArea = $this->getAreaByName($strArea);
                if (is_object($objArea)) {
                    array_push($array_id_area, $objArea->chr_area_id);
                }
            }
        }
        $array_usuario_temp = [];
        $array_usuario_temp2 = [];
        if (is_array($array_id_area) && count($array_id_area) > 0) {
            //listamos los registros de permisos
            $jefes_area = $this->getJefesArea();
            $array_usuario_jefes = [];
            if (is_array($jefes_area) && count($jefes_area) > 0) {
                foreach ($jefes_area as $index => $jefe) {
                    if (!in_array($jefe->usuario, $array_usuario_jefes)) {
                        array_push($array_usuario_jefes, $jefe->usuario);
                    }
                }
            }
            if (is_array($array_usuario_jefes) && count($array_usuario_jefes) > 0) {
                //obtenemos los accesos al area de los jefes
                foreach ($array_usuario_jefes as $puntero => $strUsuario) {
                    $objAcceso = $this->getUsuarioArea($strUsuario);
                    $array_temp = explode(',', $objAcceso->area);
                    foreach ($array_id_area as $index2 => $areaId) {
                        if (in_array($areaId, $array_temp)) {
                            if (!in_array($strUsuario, $array_usuario_temp)) {
                                array_push($array_usuario_temp, $strUsuario);
                                array_push($array_usuario_temp2, "'" . $strUsuario . "'");
                            }
                        }
                    }
                }
            }
            //obtenemos los correos de los jefes
            $correo = [];
            $sql = "SELECT * FROM usuario WHERE usuario IN (" . implode(',', $array_usuario_temp2) . ")";
            $correos = $this->db->get_results($sql);
            if (is_array($correos) && count($correos) > 0) {
                foreach ($correos as $index => $objUsuario) {
                    if (!in_array($objUsuario->chr_correo, $correo)) {
                        array_push($correo, $objUsuario->chr_correo);
                    }
                }
            }
            $returnValue = implode(',', $correo);
        }
        return $returnValue;
    }

    private function getUsuarioArea($strUsuario) {
        $returnValue = NULL;
        $sql = "SELECT * FROM usuario_area WHERE usuario like '" . $strUsuario . "'";
        $returnValue = $this->db->get_results($sql);
        if (is_array($returnValue) && count($returnValue) > 0) {
            $returnValue = array_shift($returnValue);
        }
        return $returnValue;
    }

    private function getJefesArea() {
        $returValue = [];
        $sql = "SELECT * FROM usuario WHERE cargo LIKE 'jefe de area'";
        $returValue = $this->db->get_results($sql);
        return $returValue;
    }

    private function getAreaByName($strArea) {
        $returnValue = '';
        $sql = "SELECT * FROM presupuesto_area WHERE area like '" . $strArea . "'";
        $result = $this->db->get_results($sql);
        if (is_array($result) && count($result) > 0) {
            $returnValue = array_shift($result);
        }
        return $returnValue;
    }

    private function curl_post_async($url, $params) {
        foreach ($params as $key => &$val) {
            if (is_array($val))
                $val = implode(',', $val);
            $post_params[] = $key . '=' . urlencode($val);
        }
        $post_string = implode('&', $post_params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'curl');
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);
    }

    private function getBodyMail($objMovimiento) {
        $returnValue = '';
        switch ($objMovimiento->int_statusid) {
            case ESTADO_MOVIMIENTO_EJECUTADO:
                $returnValue .= '<!DOCTYPE html>';
                $returnValue .= '<html lang="es">';
                $returnValue .= '<head>';
                    $returnValue.='<meta charset="utf-8">';
                    $returnValue.='<style>';
                    $returnValue.='body {
                                        font-family: Calibri;
                                    }';
                    $returnValue.='</style>';
                $returnValue .= '</head>';
                $returnValue .= '<body>';
                    $returnValue .= '<table>';

                        $returnValue .= '<tr>';
                            $returnValue .= '<td width="10%"><img src="https://www.upc.edu.pe/sites/all/themes/upc/img/logo.png" /></td>';
                            $returnValue .= '<td width="90%"></td>';
                        $returnValue .= '</tr>';

                        $returnValue .= '<tr>';
                            $returnValue .= '<td colspan="2">&nbsp;</td>';
                        $returnValue .= '</tr>';

                        $returnValue .= '<tr>';
                            $returnValue .= '<td colspan="2">';
                            $returnValue .= '<p>Estimados señores.</p>';
                            $returnValue .= '<p>Se ha realizado la siguiente transferencia:</p>';
                            $returnValue .= '<br />';
                            $returnValue .= '</td>';
                        $returnValue .= '</tr>';

                        $returnValue .= '<tr>';
                            $returnValue .= '<td colspan="2">';
                            $returnValue .= '<br /><br />';
                            $returnValue .= '</td>';
                        $returnValue .= '</tr>';

                        $returnValue .= '<tr>';
                            $returnValue .= '<td colspan="2"><h4>Cuenta receptora:</h4></td>';
                        $returnValue .= '</tr>';

                        $returnValue .= '<tr>';
                            $returnValue .= '<td colspan="2">';
                                $returnValue .= '<table>';
                                    $returnValue .= '<tr>';
                                        $returnValue .= '<td><strong>Transferencia:</strong></td><td colspan="3">' . $objMovimiento->chr_title . '</td>';
                                    $returnValue .= '</tr>';
                                    $returnValue .= '<tr>';
                                        $returnValue .= '<td><strong>Código movimiento:</strong></td><td>' . $objMovimiento->chr_code . '</td>';
                                        $returnValue .= '<td><strong>Fecha:</strong></td><td>' . $objMovimiento->fecha . '</td>';
                                    $returnValue .= '</tr>';
                                    $returnValue .= '<tr>';
                                        $returnValue .= '<td><strong>Área:</strong></td><td>' . $objMovimiento->chr_area . '</td>';
                                        $returnValue .= '<td><strong>String:</strong></td><td>' . $objMovimiento->chr_string . '</td>';
                                    $returnValue .= '</tr>';
                                    $returnValue .= '<tr>';
                                        $returnValue .= '<td><strong>Categoría:</strong></td><td>' . $objMovimiento->chr_categoria . '</td>';
                                        $returnValue .= '<td><strong>Proyecto:</strong></td><td>' . $objMovimiento->chr_proyecto . '</td>';
                                    $returnValue .= '</tr>';
                                    $returnValue .= '<tr>';
                                        $returnValue .= '<td><strong>Comentario:</strong></td><td>' . $objMovimiento->chr_comentario . '</td>';
                                        $returnValue .= '<td><strong>Monto total:</strong></td><td>' . $objMovimiento->dec_total . '</td>';
                                    $returnValue .= '</tr>';
                                    $returnValue .= '<tr>';
                                        $returnValue .= '<td><strong>Sustento:</strong></td><td colspan="3">' . $objMovimiento->txt_sustento . '</td>';
                                    $returnValue .= '</tr>';

                                $returnValue .= '</table>';
                            $returnValue .= '</td>';
                        $returnValue .= '</tr>';                        

                        $returnValue .= '<tr>';
                            $returnValue .= '<td colspan="2">';
                            $returnValue .= '<br /><br />';
                            $returnValue .= '</td>';
                        $returnValue .= '</tr>';

                        $returnValue .= '<tr>';
                            $returnValue .= '<td colspan="2"><h4>Cuentas proveedoras:</h4></td>';
                        $returnValue .= '</tr>';
                       
                        $returnValue .= '<tr>';
                            $returnValue .= '<td colspan="2">';
                                $returnValue .= '<table border="1">';
                                    $returnValue .= '<tr>';
                                        $returnValue .= '<th>Mes</th>';
                                        $returnValue .= '<th>Área</th>';
                                        $returnValue .= '<th>String</th>';
                                        $returnValue .= '<th>Categoría</th>';
                                        $returnValue .= '<th>Proyecto</th>';
                                        $returnValue .= '<th>Comentario</th>';
                                        $returnValue .= '<th>Monto</th>';
                                    $returnValue .= '</tr>';
                                    $movimientos = $this->getMovimientosById($objMovimiento->id);
                                    $arrayMes = $this->getMes();
                                    if (is_array($movimientos) && count($movimientos) > 0) {
                                        foreach ($movimientos as $index => $objDetalleMovimiento) {
                                            $returnValue .= '<tr>';
                                            $returnValue .= '<td>' . $arrayMes[$objDetalleMovimiento->chr_mes] . '</td>';
                                            $returnValue .= '<td>' . $objDetalleMovimiento->chr_area . '</td>';
                                            $returnValue .= '<td>' . $objDetalleMovimiento->chr_string . '</td>';
                                            $returnValue .= '<td>' . $objDetalleMovimiento->chr_categoria . '</td>';
                                            $returnValue .= '<td>' . $objDetalleMovimiento->chr_proyecto . '</td>';
                                            $returnValue .= '<td>' . $objDetalleMovimiento->chr_comentario . '</td>';
                                            $returnValue .= '<td>' . $objDetalleMovimiento->dec_subtotal . '</td>';
                                            $returnValue .= '</tr>';
                                        }
                                    }                                                                                                        
                                $returnValue .= '</table>';
                            $returnValue .= '</td>';
                        $returnValue .= '</tr>';

                    $returnValue .= '</table>';
                $returnValue .= '</body>';
                $returnValue .= '</html>';            
                break;
            case 1234455:
                $returnValue .= '<!DOCTYPE html>';
                $returnValue .= '<html lang="es">';
                $returnValue .= '<head>';
                    $returnValue.='<style>';
                    $returnValue.='body {
                                        font-family: Calibri;
                                    }';
                    $returnValue.='</style>';
                $returnValue .= '</head>';
                $returnValue .= '<body>';
                $returnValue .= '<table>';
                $returnValue .= '<tr>';
                $returnValue .= '<td width="10%"><img src="https://www.upc.edu.pe/sites/all/themes/upc/img/logo.png" /></td>';
                $returnValue .= '<td width="90%"></td>';
                $returnValue .= '</tr>';
                $returnValue .= '<tr>';
                $returnValue .= '<td colspan="2">&nbsp;</td>';
                $returnValue .= '</tr>';
                $returnValue .= '<tr>';
                $returnValue .= '<td colspan="2">';
                $returnValue .= '<p>Estimados señores.</p>';
                $returnValue .= '<p>Se ha realizado la siguiente transferencia:</p>';
                $returnValue .= '<br />';
                $returnValue .= '</td>';
                $returnValue .= '</tr>';

                $returnValue .= '<tr>';
                $returnValue .= '<td colspan="2">';
                $returnValue .= '<br /><br />';
                $returnValue .= '</td>';
                $returnValue .= '</tr>';

                $returnValue .= '<tr>';
                $returnValue .= '<td colspan="2"><h4>Cuenta receptora:</h4></td>';
                $returnValue .= '</tr>';
                $returnValue .= '<tr>';
                $returnValue .= '<td colspan="2">';
                $returnValue .= '<table>';
/*                $returnValue .= '<tr>';
                $returnValue .= '<td><strong>Transferencia:</strong></td><td colspan="3">' . $objMovimiento->cht_title . '</td>';
                $returnValue .= '</tr>'; */               
/*                $returnValue .= '<tr>';
                $returnValue .= '<td><strong>Código movimiento:</strong></td><td>' . $objMovimiento->chr_code . '</td>';
                $returnValue .= '<td><strong>Fecha:</strong></td><td>' . $objMovimiento->fecha . '</td>';
                $returnValue .= '</tr>';
                $returnValue .= '<tr>';
                $returnValue .= '<td><strong>Área:</strong></td><td>' . $objMovimiento->chr_area . '</td>';
                $returnValue .= '<td><strong>String:</strong></td><td>' . $objMovimiento->chr_string . '</td>';
                $returnValue .= '</tr>';
                $returnValue .= '<tr>';
                $returnValue .= '<td><strong>Categoría:</strong></td><td>' . $objMovimiento->chr_categoria . '</td>';
                $returnValue .= '<td><strong>Proyecto:</strong></td><td>' . $objMovimiento->chr_proyecto . '</td>';
                $returnValue .= '</tr>';
                $returnValue .= '<tr>';
                $returnValue .= '<td><strong>Comentario:</strong></td><td>' . $objMovimiento->chr_comentario . '</td>';
                $returnValue .= '<td><strong>Monto total:</strong></td><td>' . $objMovimiento->dec_total . '</td>';
                $returnValue .= '</tr>';*/
/*                $returnValue .= '<tr>';
                $returnValue .= '<td><strong>Sustento:</strong></td><td colspan="3">' . $objMovimiento->txt_sustento . '</td>';
                $returnValue .= '</tr>'; */               
                $returnValue .= '</table>';
                $returnValue .= '</td>';
                $returnValue .= '</tr>';

                $returnValue .= '<tr>';
                $returnValue .= '<td colspan="2">';
                $returnValue .= '<br /><br />';
                $returnValue .= '</td>';
                $returnValue .= '</tr>';

                $returnValue .= '<tr>';
                $returnValue .= '<td colspan="2"><h4>Cuentas proveedoras:</h4></td>';
                $returnValue .= '</tr>';

                $returnValue .= '<tr>';
                $returnValue .= '<td colspan="2">';
                $returnValue .= '<table border="1">';
/*                $returnValue .= '<tr>';
                $returnValue .= '<th>Mes</th>';
                $returnValue .= '<th>Área</th>';
                $returnValue .= '<th>String</th>';
                $returnValue .= '<th>Categoría</th>';
                $returnValue .= '<th>Proyecto</th>';
                $returnValue .= '<th>Comentario</th>';
                $returnValue .= '<th>Monto</th>';
                $returnValue .= '</tr>';*/
                $movimientos = $this->getMovimientosById($objMovimiento->id);
                $arrayMes = $this->getMes();
/*                if (is_array($movimientos) && count($movimientos) > 0) {
                    foreach ($movimientos as $index => $objDetalleMovimiento) {
                        $returnValue .= '<tr>';
                        $returnValue .= '<td>' . $arrayMes[$objDetalleMovimiento->chr_mes] . '</td>';
                        $returnValue .= '<td>' . $objDetalleMovimiento->chr_area . '</td>';
                        $returnValue .= '<td>' . $objDetalleMovimiento->chr_string . '</td>';
                        $returnValue .= '<td>' . $objDetalleMovimiento->chr_categoria . '</td>';
                        $returnValue .= '<td>' . $objDetalleMovimiento->chr_proyecto . '</td>';
                        $returnValue .= '<td>' . $objDetalleMovimiento->chr_comentario . '</td>';
                        $returnValue .= '<td>' . $objDetalleMovimiento->dec_subtotal . '</td>';
                        $returnValue .= '</tr>';
                    }
                }*/
                $returnValue .= '</table>';
                $returnValue .= '</td>';
                $returnValue .= '</tr>';
                $returnValue .= '</table>';
                $returnValue .= '</body>';
                $returnValue .= '</html>';
                break;
        }
        return $returnValue;
    }

    private function getHtmlListMovimiento($year=2019) {
        $returnValue = '';
        //obtenemos los movimientos

        //
        $movimientos = $this->getMovimientos($year);
        $returnValue .= '<table class="table table-hover table-striped" id="tableListaMovimientos" name="tableListaMovimientos">';
            $returnValue .= '<thead>';
                $returnValue .= '<tr>';
                    $returnValue .= '<th>#</th>';
                    $returnValue .= '<th>Código movimiento</th>';
                    $returnValue .= '<th>Título</th>';
                    //$returnValue .= '<th>Usuario</th>';
                    $returnValue .= '<th>Área</th>';
                    $returnValue .= '<th>String</th>';
                    $returnValue .= '<th>Categoría</th>';
                    $returnValue .= '<th>Proyecto</th>';
                    $returnValue .= '<th>Comentario</th>';
                    $returnValue .= '<th>Monto</th>';
                    $returnValue .= '<th>Fecha</th>';
                    $returnValue .= '<th>Estado</th>';
                    $returnValue .= '<th>Acciones</th>';
                $returnValue .= '</tr>';
            $returnValue .= '</thead>';
        $returnValue .= '<tbody>';
        $cont = 0;
        
        if (is_array($movimientos) && count($movimientos) > 0) {
            foreach ($movimientos as $index => $objMovimiento) {
                $returnValue .= '<tr>';
                    $returnValue .= '<td>' . ($cont + 1) . '</td>';
                    $returnValue .= '<td>' . $objMovimiento->chr_code . '</td>';
                    $returnValue .= '<td>' . $objMovimiento->chr_title . '</td>';
                    //$returnValue .= '<td>' . $objMovimiento->nombre_completo . '</td>';
                    $returnValue .= '<td>' . $objMovimiento->chr_area . '</td>';
                    $returnValue .= '<td>' . $objMovimiento->chr_string . '</td>';
                    $returnValue .= '<td>' . $objMovimiento->chr_categoria . '</td>';
                    $returnValue .= '<td>' . $objMovimiento->chr_proyecto . '</td>';
                    $returnValue .= '<td>' . $objMovimiento->chr_comentario . '</td>';
                    $returnValue .= '<td>' . $objMovimiento->dec_total . '</td>';
                    $returnValue .= '<td>' . $objMovimiento->fecha_creacion . '</td>';
                    $returnValue .= '<td>' . $objMovimiento->estado_movimiento . '</td>';
                    $returnValue .= '<td>';
                    $returnValue .= '<a href="#" onclick="showDetailMovimiento(' . $objMovimiento->id . '); return false;"><span class="glyphicon glyphicon-search"></span>Ver detalle</a>';
                    //mostrar si lo sí es diferente al estado anulado
                    if (!in_array($objMovimiento->int_statusid, [ESTADO_MOVIMIENTO_ANULADA,ESTADO_MOVIMIENTO_EJECUTADO])) {
                        $returnValue .= ' | <a href="#" onclick="anularMovimiento(' . $objMovimiento->id . '); return false;"><span class="glyphicon glyphicon-remove"></span>Anular</a>';
                    }
                    $returnValue .= '</td>';
                $returnValue .= '</tr>';
                $cont++;
            }
        }
        $returnValue .= '</tbody>';
        $returnValue .= '</table>';
        return $returnValue;
    }

    private function getMovimientos($year=2019) {   
        $sql = 'SELECT  
                m.*,
                u.usuario as nombre_completo,
                FROM_UNIXTIME(m.date_timecreated, "%d/%m/%Y") AS fecha_creacion,
                e.chr_name AS estado_movimiento
                FROM mov_movimiento m
                INNER JOIN usuario u ON u.idUsuario  = m.int_creatorid
                INNER JOIN mov_estado e ON e.id = m.int_statusid
                WHERE m.is_active=1 AND is_deleted=0 AND m.chr_year='.$year.' ORDER BY m.date_timecreated DESC';
                //var_dump($sql); die();
        $res = $this->db->get_results($sql);
        return $res;
    }

    public function mostrarDetalleMovimiento($request) {
        $error = 0;
        $message = '';
        $html = $this->getHtmlMovimientoDetail($request->movimiento_id);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'html' => $html,
                'movimiento_id' => $request->movimiento_id
            ]
        ];
    }

    public function getMovimientosById($movimiento_id) {
        $returnValue = [];
        $sql = "SELECT * FROM mov_movimiento_detalle WHERE int_movimiento_id='" . $movimiento_id . "' AND is_active=1 and is_deleted=0";
        $res = $this->db->get_results($sql);
        if (is_array($res) && count($res) > 0) {
            $returnValue = $res;
        }
        return $returnValue;
    }

    private function getHtmlMovimientoDetail($movimiento_id) {
        $returnValue = '';
        $objMovimiento = $this->getMovimientoById($movimiento_id);
        $movimientos = $this->getMovimientosById($movimiento_id);
        $meses = $this->getMes();
        $returnValue .= ' <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myModalLabel">CUENTA RECEPTORA</h4>
                        </div>';
        $returnValue .= '<div class="modal-body">';
        $returnValue .= '<div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="txtArea" class="col-sm-3 control-label">ÁREA</label>
                                  <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txtArea" name="txtArea" disabled="disabled" value="' . $objMovimiento->chr_area . '">
                                        <br />
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="txtCategoria" class="col-sm-3 control-label">CATEGORÍA</label>
                                  <div class="col-sm-9">
                                    <input type="text" class="form-control" id="txtCategoria" name="txtCategoria" disabled="disabled" value="' . $objMovimiento->chr_categoria . '">
                                        <br>
                                  </div>
                                </div>
                                <br />
                                <div class="form-group">
                                  <label for="txtMes" class="col-sm-3 control-label">MES</label>
                                  <div class="col-sm-9">
                                    <input type="text" class="form-control" id="txtMes" name="txtMes" disabled="disabled" value="' . $meses[$objMovimiento->chr_mes] . '">
                                  </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="txtString" class="col-sm-3 control-label">STRING</label>
                                  <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txtString" name="txtString" disabled="disabled" value="' . $objMovimiento->chr_string . '">
                                        <br />
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="txtProyecto" class="col-sm-3 control-label">PROYECTO</label>
                                  <div class="col-sm-9">
                                    <input type="text" class="form-control" id="txtProyecto" name="txtProyecto" disabled="disabled" value="' . $objMovimiento->chr_proyecto . '">
                                        <br>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="txtFecha" class="col-sm-3 control-label">FECHA</label>
                                  <div class="col-sm-9">
                                    <input type="text" class="form-control" id="txtFecha" name="txtFecha" disabled="disabled" value="' . $objMovimiento->fecha . '">
                                        <br>
                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <label for="txtMontoTotal" class="col-sm-3 control-label">MONTO TOTAL</label>
                                  <div class="col-sm-9">
                                    <input type="text" class="form-control" id="txtMontoTotal" name="txtMontoTotal" disabled="disabled" value="' . $objMovimiento->dec_total . '">
                                  </div>
                                </div>                                
                            </div>
                        </div>';
        $returnValue .= '<div class="row">';
        $returnValue .= '<div class="col-md-12">';
        $returnValue .= ' <div class="form-group">
                                    <label for="txtComentario" control-label">COMENTARIO</label>
                                    <textarea class="form-control" id="txtComentario" id="txtComentario" disabled="disabled">' . $objMovimiento->chr_comentario . '</textarea>
                                </div>';
        $returnValue .= '</div>';
        $returnValue .= '</div>';
        $returnValue .= '<div class="row">';
        $returnValue .= '<div class="col-md-12">';
        $returnValue .= ' <div class="form-group">
                                    <label for="txtSustento" control-label">SUSTENTO</label>
                                    <textarea class="form-control" id="txtSustento" id="txtSustento" disabled="disabled">' . $objMovimiento->txt_sustento . '</textarea>
                                </div>';
        $returnValue .= '</div>';
        $returnValue .= '</div>';
        $returnValue .= '<div class="row">';
        $returnValue .= '<div class="col-md-12">';
        $returnValue .= '<h4>CUENTAS PROVEEDORAS</h4><hr />';
        $returnValue .= '</div>';
        $returnValue .= '</div>';
        $returnValue .= '<div class="row">';
        $returnValue .= '<div class="md-col-12">';
        //pintamos el detalle del movimiento
        $returnValue .= '<table class="table table-striped table-hover" id="tableMovientoDetalle" name="tableMovimientoDetalle">';
        $returnValue .= '<thead>';
        $returnValue .= '<tr>';
        $returnValue .= '<th>#</th>';
        $returnValue .= '<th>AÑO</th>';
        $returnValue .= '<th>ÁREA</th>';
        $returnValue .= '<th>STRING</th>';
        $returnValue .= '<th>CATEGORÍA</th>';
        $returnValue .= '<th>PROYECTO</th>';
        $returnValue .= '<th>COMENTARIO</th>';
        $returnValue .= '<th>MES</th>';
        $returnValue .= '<th>MONTO</th>';
        $returnValue .= '</tr>';
        $returnValue .= '</thead>';
        $returnValue .= '<tbody>';
        $cont = 0;
        if (is_array($movimientos) && count($movimientos) > 0) {
            foreach ($movimientos as $index => $oMovimiento) {
                $returnValue .= '<tr>';
                $returnValue .= '<td>' . ($cont + 1) . '</td>';
                $returnValue .= '<td>' . $oMovimiento->chr_year . '</td>';
                $returnValue .= '<td>' . $oMovimiento->chr_area . '</td>';
                $returnValue .= '<td>' . $oMovimiento->chr_string . '</td>';
                $returnValue .= '<td>' . $oMovimiento->chr_categoria . '</td>';
                $returnValue .= '<td>' . $oMovimiento->chr_proyecto . '</td>';
                $returnValue .= '<td>' . $oMovimiento->chr_comentario . '</td>';
                $returnValue .= '<td>' . $meses[$oMovimiento->chr_mes] . '</td>';
                $returnValue .= '<td>' . $oMovimiento->dec_subtotal . '</td>';
                $returnValue .= '</tr>';
                $cont++;
            }
        }
        $returnValue .= '<tr>';
        $returnValue .= '<td colspan="9">&nbsp;</td>';
        $returnValue .= '</tr>';
        $returnValue .= '<tr>';
        $returnValue .= '<td colspan="9">&nbsp;</td>';
        $returnValue .= '</tr>';
        $returnValue .= '<tr>';
        $returnValue .= '<td colspan="9">&nbsp;</td>';
        $returnValue .= '</tr>';
        $returnValue .= '<tr>';
        $returnValue .= '<td colspan="9">&nbsp;</td>';
        $returnValue .= '</tr>';
        $returnValue .= '<tr>';
        $returnValue .= '<td colspan="9">&nbsp;</td>';
        $returnValue .= '</tr>';
        $returnValue .= '</tbody>';
        $returnValue .= '<tfoot>';
        $returnValue .= '<tr>';
        $returnValue .= '<th colspan="8" class="text-right">Total: </th>';
        $returnValue .= '<th>' . $objMovimiento->dec_total . '</th>';
        $returnValue .= '</tr>';
        $returnValue .= '</tfoot>';
        $returnValue .= '</table>';
        $returnValue .= '</div>';
        $returnValue .= '</div>';
        $returnValue .= '</div>';
        $returnValue .= ' <div class="modal-footer">';
        if ($objMovimiento->int_statusid != ESTADO_MOVIMIENTO_ANULADA) {
            $disabled = '';
            if ($objMovimiento->int_statusid == ESTADO_MOVIMIENTO_PENDIENTE_POR_EJECUTAR) {
                $disabled = ' disabled';
            }
            if ($objMovimiento->int_statusid != ESTADO_MOVIMIENTO_EJECUTADO) {
                $returnValue .= '<a href="#" onclick="ejecutarTransferencia(' . $objMovimiento->id . ');return false;" class="btn btn-primary btn-sm ' . $disabled . '"><span class="glyphicon glyphicon-transfer"></span> EJECUTAR TRANSFERENCIAS</a>';
            }
            $returnValue .= '<a href="webService/index.php?data={%22f%22:%22exportarMovimiento%22,%22a%22:[{%22subject%22:%22exportarMovimiento%22,%22movimiento_id%22:' . $objMovimiento->id . '}]}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-floppy-save"></span> EXPORTAR</a>';
        }
        $returnValue .= '<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> CERRAR</button>';
        $returnValue .= '</div>';
        return $returnValue;
    }

    public function exportarMovimiento($request) {
        $error = 0;
        $message = '';
        $html = '';
        $objMovimiento = $this->getMovimientoById($request->movimiento_id);
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("DACA")
                ->setLastModifiedBy("DACA")
                ->setTitle("Detalle de movimiento")
                ->setSubject("movimiento")
                ->setDescription("Detalle de movimientos entre strings")
                ->setKeywords("daca")
                ->setCategory("DACA");
        $array = [
            'Periodo',
            'Cuenta',
            'Nombre Cuenta',
            'Código Unid Expl - Sede',
            'Código Department (*)',
            'Degree Leve / Prod o Tit',
            'String Concatenado',
            'Nombre String',
            'Aprobador',
            'Moneda',
            'Monto (Colocar signo + o -)',
            'Sustento',
            'Solicitante'
        ];
        $row = 10;
        $col = 0;
        $styleArray = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => 'ffffff'),
                'size' => 10,
                'name' => 'Verdana'
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $styleRedArray = array(
            'font' => array(
                'color' => array('rgb' => 'FF0000'),
                'size' => 10,
                'name' => 'Verdana'
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );        
        $styleArrayContent = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        foreach ($array as $index => $title) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $title);
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => '82b3f2' //sky  93bdd6
                )
            ));
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->getColumnDimension($str_col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
            //$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            //$objPHPExcel->getActiveSheet()->getStyle($range)->getAlignment()->setWrapText(true);
            //$sheet->getCellByColumnAndRow($col, $row)->setValueExplicit($cell, PHPExcel_Cell_DataType::TYPE_STRING);
            $col++;
        }
        $row = 11;
        $col = 0;
        $movimientos = $this->getMovimientosById($request->movimiento_id);
        foreach ($movimientos as $puntero => $oMovimiento) {
            //registramos los movimientos negativos
            $col = 0;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = date("Y") . 'M' . $oMovimiento->chr_mes;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = $oMovimiento->chr_categoria_id;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = $oMovimiento->chr_categoria;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $array_chr_string_id = explode('-', $oMovimiento->chr_string);
            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = $array_chr_string_id[0];
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);


            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = $array_chr_string_id[1];
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = $array_chr_string_id[2];
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = $oMovimiento->chr_string;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = $oMovimiento->chr_nombre_string;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = APROBADOR;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = 'S/.';
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = '-' . $oMovimiento->dec_subtotal;
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->setValueExplicit($value, PHPExcel_Cell_DataType::TYPE_STRING);
            //$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleRedArray);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = '';
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = $this->getFullnameUserById($objMovimiento->int_creatorid);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $row++;

            //registramos los movimientos positivos con la cabecera y el subtotal de cada uno
            //-------------------------------------------------------------------------------;;
            $col = 0;
            //registramos al final el area que recibe la transferencia
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = date("Y") . 'M' . $objMovimiento->chr_mes;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = $objMovimiento->chr_categoria_id;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = $objMovimiento->chr_categoria;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $array_chr_string_id = explode('-', $objMovimiento->chr_string);
            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = $array_chr_string_id[0];
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = $array_chr_string_id[1];
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = $array_chr_string_id[2];
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = $objMovimiento->chr_string;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = $objMovimiento->chr_nombre_string;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = APROBADOR;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = 'S/.';
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = '+' . $oMovimiento->dec_subtotal;
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->setValueExplicit($value, PHPExcel_Cell_DataType::TYPE_STRING);
            //$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = '';
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

            $col++;
            $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
            $range = (string) $str_col . $row;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
            $value = $this->getFullnameUserById($objMovimiento->int_creatorid);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
            $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);            
            //-------------------------------------------------------------------------------
            $row++;
        }

        //agregamos la sumatoria del movimiento
        $col = 10;
        $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
        $range = (string) $str_col . $row;
        $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
        $value = '0.00';
        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->setValueExplicit($value, PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleRedArray);        

/*        $row = $row;
        $col = 0;
        //registramos al final el area que recibe la transferencia
        $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
        $range = (string) $str_col . $row;
        $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
        $value = date("Y") . 'M' . $objMovimiento->chr_mes;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

        $col++;
        $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
        $range = (string) $str_col . $row;
        $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
        $value = $objMovimiento->chr_categoria_id;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

        $col++;
        $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
        $range = (string) $str_col . $row;
        $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
        $value = $objMovimiento->chr_categoria;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

        $array_chr_string_id = explode('-', $objMovimiento->chr_string);
        $col++;
        $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
        $range = (string) $str_col . $row;
        $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
        $value = $array_chr_string_id[0];
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

        $col++;
        $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
        $range = (string) $str_col . $row;
        $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
        $value = $array_chr_string_id[1];
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

        $col++;
        $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
        $range = (string) $str_col . $row;
        $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
        $value = $array_chr_string_id[2];
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

        $col++;
        $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
        $range = (string) $str_col . $row;
        $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
        $value = $objMovimiento->chr_string;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

        $col++;
        $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
        $range = (string) $str_col . $row;
        $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
        $value = utf8_encode($objMovimiento->chr_nombre_string);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

        $col++;
        $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
        $range = (string) $str_col . $row;
        $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
        $value = APROBADOR;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

        $col++;
        $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
        $range = (string) $str_col . $row;
        $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
        $value = 'S/.';
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

        $col++;
        $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
        $range = (string) $str_col . $row;
        $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
        $value = '+' . $objMovimiento->dec_total;
        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->setValueExplicit($value, PHPExcel_Cell_DataType::TYPE_STRING);
        //$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

        $col++;
        $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
        $range = (string) $str_col . $row;
        $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
        $value = '';
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);

        $col++;
        $str_col = PHPExcel_Cell::stringFromColumnIndex($col);
        $range = (string) $str_col . $row;
        $objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'd7e6ea')));
        $value = $this->getFullnameUserById($objMovimiento->int_creatorid);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
        $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleArrayContent);*/

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        
        // We'll be outputting an excel file
        //header('Content-type: application/vnd.ms-excel');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        // It will be called file.xls
        header('Content-Disposition: attachment; filename="' . $objMovimiento->chr_code . '.xlsx"');
        $objWriter->save('php://output');
        die();
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'html' => $html,
                'movimiento_id' => $request->movimiento_id
            ]
        ];
    }

    private function getMovimientoById($movimiento_id) {
        $returnValue = NULL;
        $sql = "SELECT m.*, FROM_UNIXTIME(m.date_timecreated,\"%d/%m/%Y\") as fecha FROM mov_movimiento m WHERE m.id='" . $movimiento_id . "'";
        $res = $this->db->get_results($sql);
        if (is_array($res) && count($res) > 0) {
            $objResult = array_shift($res);
            if (is_object($objResult)) {
                $returnValue = $objResult;
            }
        }
        return $returnValue;
    }

    private function getMovimientoDetalleById($movimiento_detalle_id) {
        $returnValue = NULL;
        $sql = "SELECT * FROM mov_movimiento_detalle WHERE id='" . $movimiento_detalle_id . "'";
        $res = $this->db->get_results($sql);
        if (is_array($res) && count($res) > 0) {
            $objResult = array_shift($res);
            if (is_object($objResult)) {
                $returnValue = $objResult;
            }
        }
        return $returnValue;
    }

    public function solicitarTransferencia($request) {
        $error = 0;
        $message = '';
        if (!isset($request->year)) {
            $request->year = date("Y");
        }
        $monto_requerido = $request->monto_requerido;
        $transferido = 0;
        $diferencia = $request->monto_requerido;
        $orden_movimiento_id = $this->createMovimiento($request);
        $html = $this->getHtmlFormTransferencia($monto_requerido, $transferido, $diferencia, $orden_movimiento_id, $request->year);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'html' => $html,
                'orden_movimiento_id' => $orden_movimiento_id
            ]
        ];
    }

    public function loadFormCuenta($request) {
        $error = 0;
        $message = '';
        if (!isset($request->year)) {
            $request->year = date("Y");
        }
        $orden_movimiento_id = $request->orden_movimiento_id;
        $html = $this->getHtmlFormCuenta($orden_movimiento_id, 0, $request->year, $request);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'html' => $html
            ]
        ];
    }

    public function editarOrdeMovimientoDetalle($request) {
        $error = 0;
        $message = '';
        if(!isset($request->year)){
            $request->year = date("Y");
        }
        $orden_movimiento_id = $request->orden_movimiento_id;
        $orden_movimiento_detalle_id = $request->orden_movimiento_detalle_id;
        $objOrdenMovimiento = $this->getOrdenMovimientoById($orden_movimiento_id);
        $request->chr_area = $objOrdenMovimiento->chr_area;
        $request->chr_string = $objOrdenMovimiento->chr_string;
        $request->chr_categoria = $objOrdenMovimiento->chr_categoria;
        $request->chr_proyecto = $objOrdenMovimiento->chr_proyecto;
        $request->chr_comentario = $objOrdenMovimiento->chr_comentario;
        $request->chr_mes = $objOrdenMovimiento->chr_mes;
        $html = $this->getHtmlFormCuenta($orden_movimiento_id, $orden_movimiento_detalle_id, $request->year, $request);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'html' => $html
            ]
        ];
    }

    public function loadString($request) {
        $error = 0;
        $message = '';
        $area = $request->area;
        if(!isset($request->year)){
            $request->year = date("Y");
        }
        $html = $this->getHtmlDropDownString($area, $request->year);
        $proyecto = $this->getHtmlDropDownProyecto('','',$request->year);
        $categoria = $this->getHtmlDropDownCategoria('','','',$request->year);
        $comentario = $this->getHtmlDropDownComentario('','','','',$request->year);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'html' => $html,
                'proyecto' => $proyecto,
                'categoria' => $categoria,
                'comentario' => $comentario
            ]
        ];
    }

    public function loadProyecto($request) {
        $error = 0;
        $message = '';
        if(!isset($request->year)){
            $request->year = date("Y");
        }
        $html = $this->getHtmlDropDownProyecto($request->area, $request->string, $request->year);
        $categoria = $this->getHtmlDropDownCategoria('','','', $request->year);
        $comentario = $this->getHtmlDropDownComentario('','','','',$request->year);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'html' => $html,
                'categoria' => $categoria,
                'comentario' => $comentario
            ]
        ];
    }

    public function loadCategoria($request) {
        $error = 0;
        $message = '';
        if(!isset($request->year)){
            $request->year = date("Y");
        }
        $html = $this->getHtmlDropDownCategoria($request->area, $request->string, $request->proyecto, $request->year);
        $comentario = $this->getHtmlDropDownComentario('','','','',$request->year);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'html' => $html,
                'comentario' => $comentario
            ]
        ];
    }

    public function loadComentario($request) {
        $error = 0;
        $message = '';
        if(!isset($request->year)){
            $request->year = date("Y");
        }
        $html = $this->getHtmlDropDownComentario($request->area, $request->string, $request->proyecto, $request->categoria, $request->year);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'html' => $html
            ]
        ];
    }

    public function loadSaldo($request) {
        $error = 0;
        $message = '';
        if(!isset($request->year)){
            $request->year = date("Y");
        }
//        //obtenemos el el presupuesto
//        $presupuesto = $this->getPresupuesto($request->area, $request->string, $request->proyecto, $request->categoria, $request->comentario, $request->mes);
//        //obtenemos el gasto
//        $gasto = $this->getGasto($request->area, $request->string, $request->proyecto, $request->categoria, $request->comentario, $request->mes);
//        $saldo = $presupuesto - $gasto;
        $saldo = $this->getSaldoActual($request->area, $request->string, $request->categoria, $request->proyecto, $request->comentario, $request->mes, $request->int_orden_movimiento_id, $request->year);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'saldoActual' => $saldo,
                'chr_year'=>$request->year
            ]
        ];
    }

    private function getPresupuesto($area, $string, $proyecto, $categoria, $comentario, $mes, $year) {
        $returnValue = 0;
        $sql = "SELECT ROUND(SUM(total),3) as presupuesto FROM " . TABLE_INGRESO . " WHERE chr_area_id='" . $area . "' AND chr_string_id='" . $string . "' AND chr_proyecto_id='" . $proyecto . "' AND chr_categoria_id='" . $categoria . "' AND chr_comentario_id='" . $comentario . "' AND mes='" . $mes . "' AND anio='" . $year . "'";

        $res = $this->db->get_results($sql);
        if (is_array($res) && count($res) > 0) {
            $objResult = array_shift($res);
            if (is_object($objResult) && !is_null($objResult->presupuesto)) {
                $returnValue = $objResult->presupuesto;
            }
        }
        return $returnValue;
    }

    private function getGasto($area, $string, $proyecto, $categoria, $comentario, $mes, $year) {
        $returnValue = 0;
        $sql = "SELECT ROUND(SUM(total),3) as gasto FROM " . TABLE_GASTO . " WHERE chr_area_id='" . $area . "' AND chr_string_id='" . $string . "' AND chr_proyecto_id='" . $proyecto . "' AND chr_categoria_id='" . $categoria . "' AND chr_comentario_id='" . $comentario . "' AND mes='" . $mes . "' AND anio='" . $year . "'";
        $res = $this->db->get_results($sql);
        if (is_array($res) && count($res) > 0) {
            $objResult = array_shift($res);
            if (is_object($objResult) && !is_null($objResult->gasto)) {
                $returnValue = $objResult->gasto;
            }
        }
        return $returnValue;
    }

    /**
     * obtiene código html para pintar el dropdown
     * @param string $area
     * @return string
     */
    private function getHtmlDropDownString($area, $chr_year) {
        $returValue = '<label for="inputString" class="control-label">String</label>';
        $returValue .= '<div class="divFeedbackCustom">';
        $returValue .= '<select name="inputString" id="inputString" class="form-control">';
        $returValue .= '<option value="">[ SELECCIONE ]</option>';
        if (strlen(trim($area)) > 0) {
            $strings = $this->getStringByArea($area, $chr_year);
            if (is_array($strings) && count($strings) > 0) {
                foreach ($strings as $index => $string) {
                    $returValue .= '<option value="' . $string->chr_string_id . '">' . $string->chr_string_id . ' - ' . $string->chr_string . '</option>';
                }
            }
        }
        $returValue .= '</select></div>';
        return $returValue;
    }

    /**
     * obtiene código html para pintar el dropdown
     * @param string $area
     * @param string $string
     * @param string $chr_year
     * @return string
     */
    private function getHtmlDropDownProyecto($area = '', $string = '', $chr_year = '') {
        $returValue = '<label for="inputProyecto" class="control-label">Proyecto</label>';
        $returValue .= '<div class="divFeedbackCustom">';
        $returValue .= '<select name="inputProyecto" id="inputProyecto" class="form-control">';
        $returValue .= '<option value="">[ SELECCIONE ]</option>';
        if (strlen(trim($area)) > 0 && strlen(trim($string)) > 0) {
            $items = $this->getProyectoByAreaByString($area, $string, $chr_year);
            if (is_array($items) && count($items) > 0) {
                foreach ($items as $index => $item) {
                    $returValue .= '<option value="' . $item->chr_proyecto_id . '">' . $item->chr_proyecto . '</option>';
                }
            }
        }
        $returValue .= '</select></div>';
        return $returValue;
    }

    /**
     * obtiene código html para pintar el dropdown
     * @param string $area
     * @return string
     */
    private function getHtmlDropDownCategoria($area = '', $string = '', $proyecto = '', $chr_year = '') {
        $returValue = '<label for="inputCategoria" class="control-label">Categoría</label>';
        $returValue .= '<div class="divFeedbackCustom">';
        $returValue .= '<select name="inputCategoria" id="inputCategoria" class="form-control">';
        $returValue .= '<option value="">[ SELECCIONE ]</option>';
        if (strlen(trim($area)) > 0 && strlen(trim($string)) > 0) {
            $items = $this->getCategoriaByAreaByStringByProyecto($area, $string, $proyecto, $chr_year);
            if (is_array($items) && count($items) > 0) {
                foreach ($items as $index => $item) {
                    $returValue .= '<option value="' . $item->chr_categoria_id . '">' . $item->chr_categoria . '</option>';
                }
            }
        }
        $returValue .= '</select></div>';
        return $returValue;
    }

    public function addCuentaMovimiento($request) {

        $error = 0;
        $message = '';
        //creamos un movimiento
        $movimientoId = $request->orden_movimiento_id;
        $objMovimiento = new stdClass();
        $objMovimiento->int_orden_movimiento_id = $movimientoId;
        $objMovimiento->int_orden_movimiento_detalle_id = $request->orden_movimiento_detalle_id;
        $objMovimiento->area = $request->area;
        $objMovimiento->string = $request->string;
        //$objMovimiento->proyecto = $request->proyecto;
        //$objMovimiento->categoria = $request->categoria;
        //$objMovimiento->comentario = $request->comentario;

        $objMovimiento->chr_area_id = $request->chr_area_id;
        $objMovimiento->chr_string_id = $request->chr_string_id;
        $objMovimiento->chr_proyecto_id = $request->chr_proyecto_id;
        $objMovimiento->chr_categoria_id = $request->chr_categoria_id;
        $objMovimiento->chr_comentario_id = $request->chr_comentario_id;

        $objMovimiento->mes = $request->mes;
        $objMovimiento->saldo = $request->saldo;
        $objMovimiento->monto = $request->monto;
        $objMovimiento->chr_year = $request->chr_year;

        $objMovimiento->id = $this->addMovimiento($objMovimiento);

        $objMovimiento->acciones = '<a href="#" title="Editar" data-id="' . $objMovimiento->id . '" class="edit_movimiento" onclick="editarCuentaMovimiento(' . $objMovimiento->id . ');return false;"><span class="glyphicon glyphicon-pencil"></span> Editar</a> | <a href="#" title="Eliminar" data-id="' . $objMovimiento->id . '" class="delete_movimiento" onclick="eliminarMovimientoDetalle(' . $objMovimiento->id . '); return false"><span class="glyphicon glyphicon-trash"></span> Eliminar</a>';
        //actualizar el nuevo total de los movimientos
        $monto = $this->updateMontoTotalOrdenMovimiento($movimientoId);
        $objOrdenMovimiento = $this->getOrdenMovimientoById($movimientoId);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'movimiento' => $objMovimiento,
                'objOrdenMovimiento' => $objOrdenMovimiento,
                'orden_movimiento_detalle_id' => $request->orden_movimiento_detalle_id
            ]
        ];
    }

    private function updateMontoTotalOrdenMovimiento($movimientoId) {
        $returnValue = 0;
        $sql = "SELECT SUM(dec_subtotal) as sum_dec_total FROM mov_orden_movimiento_detalle WHERE int_orden_movimiento_id ='" . $movimientoId . "' AND is_active='1' AND is_deleted='0'";
        $res = $this->db->get_results($sql);
        if (is_array($res) && count($res) > 0) {
            $objSum = array_shift($res);
            if (is_object($objSum)) {
                $returnValue = $objSum->sum_dec_total;
            }
        }
        //actualizamos el monto del movimiento total
        $sql = "UPDATE mov_orden_movimiento SET dec_total = '" . $returnValue . "', date_timemodified='" . time() . "' WHERE id=" . $movimientoId;
        $this->db->query($sql);
        return $returnValue;
    }

    /**
     * Método para obtener el saldo actual
     * @param type $chr_area
     * @param type $chr_string
     * @param type $chr_categoria
     * @param type $chr_proyecto
     * @param type $chr_comentario
     * @param type $chr_mes
     * @param type $int_orden_movimiento_id
     * @param type $chr_year
     * @return float
     */
    private function getSaldoActual($chr_area = '', $chr_string = '', $chr_categoria = '', $chr_proyecto = '', $chr_comentario = '', $chr_mes = '01', $int_orden_movimiento_id = 0, $chr_year = '') {
        $returnValue = 0;
        //obtenemos el el presupuesto
        $presupuesto = $this->getPresupuesto($chr_area, $chr_string, $chr_proyecto, $chr_categoria, $chr_comentario, $chr_mes, $chr_year);
        //var_dump('presupuesto: '.$presupuesto);
        //obtenemos el gasto
        $gasto = $this->getGasto($chr_area, $chr_string, $chr_proyecto, $chr_categoria, $chr_comentario, $chr_mes, $chr_year);
        //var_dump('gasto: '.$gasto);
        //realizamos la diferencia para obtener el saldo
        $returnValue = floatval($presupuesto) - floatval($gasto);
        //var_dump('presupuesto - gasto: '.$returnValue);
        //obtenemos los gastos de transferencias en proceso
        $monto_en_proceso = $this->getMontoProceso($chr_area, $chr_string, $chr_proyecto, $chr_categoria, $chr_comentario, $chr_mes, $chr_year);
        //var_dump('movimientos: '.$monto_en_proceso);
        $returnValue = floatval($returnValue) + floatval($monto_en_proceso);
        //-+
//        if($returnValue <0){
//            if($monto_en_proceso<0){
//                $returnValue = floatval($returnValue) + floatval($monto_en_proceso);
//            }else{
//                $returnValue = floatval($returnValue) + floatval($monto_en_proceso);
//            }
//        }else{
//            //positivo
//            if($monto_en_proceso <0){
//                $returnValue = floatval($returnValue) + floatval($monto_en_proceso);
//            }else{
//                $returnValue = floatval($returnValue) - floatval($monto_en_proceso);   
//            }
//        }
        //var_dump('total: '.$returnValue);
        //obtenemos un saldo de un movimiento de la orden actual usada para hacer el resto si es que existiera
        $monto_orden_movimiento_actual = 0;
        if ($int_orden_movimiento_id > 0) {
            $monto_orden_movimiento_actual = $this->getMontoProcesoActual($chr_area, $chr_string, $chr_proyecto, $chr_categoria, $chr_comentario, $chr_mes, $int_orden_movimiento_id, $chr_year);
            
        }
        $returnValue = floatval($returnValue) + floatval($monto_orden_movimiento_actual);
//        if($monto_orden_movimiento_actual >0){
//            $returnValue = floatval($returnValue) - floatval($monto_orden_movimiento_actual);
//        }else{
//            $returnValue = floatval($returnValue) + floatval($monto_orden_movimiento_actual);
//        }
        return $returnValue;
    }

    /**
     * Método para obtener el monto usado en el movimiento de orden actual
     * @param type $chr_area
     * @param type $chr_string
     * @param type $chr_proyecto
     * @param type $chr_categoria
     * @param type $chr_comentario
     * @param type $chr_mes
     * @param type $int_orden_movimiento_id
     * @param type $chr_year
     * @return float
     */
    private function getMontoProcesoActual($chr_area, $chr_string, $chr_proyecto, $chr_categoria, $chr_comentario, $chr_mes, $int_orden_movimiento_id, $chr_year) {
        $returnValue = 0;
        //Obtenemos la sumatoria de los saldos en estado en proceso de transferencia
        $sql = "SELECT ROUND(SUM(dec_subtotal),3) as gasto FROM mov_orden_movimiento_detalle WHERE is_active='1' AND is_deleted='0' AND chr_area='" . $chr_area . "' AND chr_string='" . $chr_string . "' AND chr_proyecto='" . $chr_proyecto . "' AND chr_categoria='" . $chr_categoria . "' AND chr_comentario='" . $chr_comentario . "' AND chr_mes='" . $chr_mes . "' AND int_orden_movimiento_id IN (" . $int_orden_movimiento_id . ")";
        $detalle_movimientos = $this->db->get_results($sql);
        if (is_array($detalle_movimientos) && count($detalle_movimientos) > 0) {
            $objResultadoDetalleMovimiento = array_shift($detalle_movimientos);
            if (is_object($objResultadoDetalleMovimiento)) {
                $returnValue = $objResultadoDetalleMovimiento->gasto;
            }
        }
        return $returnValue;
    }
    /**
     * 
     * @param type $chr_area
     * @param type $chr_string
     * @param type $chr_proyecto
     * @param type $chr_categoria
     * @param type $chr_comentario
     * @param type $chr_mes
     * @param type $chr_year
     * @return float
     */
    private function getMontoProceso($chr_area, $chr_string, $chr_proyecto, $chr_categoria, $chr_comentario, $chr_mes, $chr_year) {
        $returnValue = 0;
        $array_id_movimiento = [];
        //Listamos los movimientos en estado pendiente por ejecutar
//        $sql = "SELECT * FROM mov_movimiento WHERE int_statusid IN('" . ESTADO_MOVIMIENTO_CREADA . "','" . ESTADO_MOVIMIENTO_PENDIENTE_POR_EJECUTAR . "','".ESTADO_MOVIMIENTO_EJECUTADO."') AND is_active=1 AND is_deleted=0 AND chr_year='".$chr_year."'";
//        //var_dump($sql);
//        $movimientos = $this->db->get_results($sql);
//        if (is_array($movimientos) && count($movimientos) > 0) {
//            foreach ($movimientos as $index => $objMovimiento) {
//                if (!in_array($objMovimiento->id, $array_id_movimiento)) {
//                    array_push($array_id_movimiento, $objMovimiento->id);
//                }
//            }
//            if (is_array($array_id_movimiento) && count($array_id_movimiento) > 0) {
//                //Obtenemos la sumatoria de los saldos en estado en proceso de transferencia
//                $sql = "SELECT ROUND(SUM(dec_subtotal),3) as gasto FROM mov_movimiento_detalle WHERE chr_area='" . $chr_area . "' AND chr_string='" . $chr_string . "' AND chr_proyecto='" . $chr_proyecto . "' AND chr_categoria='" . $chr_categoria . "' AND chr_comentario='" . $chr_comentario . "' AND int_movimiento_id IN (" . implode(",", $array_id_movimiento) . ")";
//                //var_dump($sql);
//                $detalle_movimientos = $this->db->get_results($sql);
//                if (is_array($detalle_movimientos) && count($detalle_movimientos) > 0) {
//                    $objResultadoDetalleMovimiento = array_shift($detalle_movimientos);
//                    if (is_object($objResultadoDetalleMovimiento)) {
//                        $returnValue = $objResultadoDetalleMovimiento->gasto;
//                    }
//                }
//            }
//        }
        
        $egreso = 0;
        $ingreso = 0;
        //obtenemos los egresos
        $sql ="SELECT ROUND(SUM(dec_subtotal),3) as gasto 
        FROM mov_movimiento_detalle md
        INNER JOIN mov_movimiento m ON m.id = md.int_movimiento_id AND m.is_active =1 AND m.is_deleted=0 AND m.int_statusid IN ('" . ESTADO_MOVIMIENTO_CREADA . "','" . ESTADO_MOVIMIENTO_PENDIENTE_POR_EJECUTAR . "','".ESTADO_MOVIMIENTO_EJECUTADO."')
        WHERE md.chr_string_id='".$chr_string."' 
        AND md.chr_mes= '".$chr_mes."'
        AND md.chr_area_id = '".$chr_area."'
        AND md.chr_proyecto_id = '".$chr_proyecto."'
        AND md.chr_categoria_id = '".$chr_categoria."'
        AND md.chr_comentario_id = '".$chr_comentario."'";
        //var_dump($sql);
        $detalle_movimientos = $this->db->get_results($sql);
        if (is_array($detalle_movimientos) && count($detalle_movimientos) > 0) {
            $objResultadoDetalleMovimiento = array_shift($detalle_movimientos);
            if (is_object($objResultadoDetalleMovimiento)) {
                //$returnValue = $objResultadoDetalleMovimiento->gasto;
                $egreso = $objResultadoDetalleMovimiento->gasto;
            }
        }
        //obtenemos los ingresos
        $sql_ingresos="SELECT ROUND(SUM(m.dec_total),3) AS gasto  
FROM mov_movimiento m
WHERE m.is_active=1 AND m.is_deleted =0 AND m.chr_mes='".$chr_mes."' 
AND m.chr_string='".$chr_string."'
AND m.int_statusid IN ('" . ESTADO_MOVIMIENTO_CREADA . "','" . ESTADO_MOVIMIENTO_PENDIENTE_POR_EJECUTAR . "','".ESTADO_MOVIMIENTO_EJECUTADO."')
        AND m.chr_area_id  = '".$chr_area."'
        AND m.chr_proyecto_id = '".$chr_proyecto."'
        AND m.chr_categoria_id = '".$chr_categoria."'
        AND m.chr_comentario_id = '".$chr_comentario."'"; 
        $detalle_ingresos = $this->db->get_results($sql_ingresos);
        if (is_array($detalle_ingresos) && count($detalle_ingresos) > 0) {
            $objResultadoDetalleMovimientoIngreso = array_shift($detalle_ingresos);
            if (is_object($objResultadoDetalleMovimientoIngreso)) {
                //$returnValue = $objResultadoDetalleMovimiento->gasto;
                $ingreso = $objResultadoDetalleMovimientoIngreso->gasto;
            }
        }
        $returnValue = $ingreso - $egreso;
        return $returnValue;
    }

    private function getOrdenMovimientoDetalleById($movimientoDetalleId) {
        $returnValue = NULL;
        if ($movimientoDetalleId > 0) {
            $sql = "SELECT * FROM mov_orden_movimiento_detalle WHERE id =" . $movimientoDetalleId;
            $res = $this->db->get_results($sql);
            if (is_array($res) && count($res) > 0) {
                $returnValue = array_shift($res);
                if (is_object($returnValue)) {
                    $objOrdenMovimiento = $this->getOrdenMovimientoById($returnValue->int_orden_movimiento_id);
                    //obtenemos el saldo actual
                    $returnValue->dec_saldo_actual = $this->getSaldoActual($returnValue->chr_area, $returnValue->chr_string, $returnValue->chr_categoria, $returnValue->chr_proyecto, $returnValue->chr_comentario, $returnValue->chr_mes, $returnValue->int_orden_movimiento_id, $objOrdenMovimiento->chr_year);
                }
            }
        } else {
            $returnValue = new stdClass();
            $returnValue->id = 0;
            $returnValue->int_orden_movimiento_id = 0;
            $returnValue->int_creatorid = 0;
            $returnValue->date_timecreated = time();
            $returnValue->date_timemodified = time();
            $returnValue->is_active = 1;
            $returnValue->is_deleted = 0;
            $returnValue->chr_area = '';
            $returnValue->chr_string = '';
            $returnValue->chr_proyecto = '';
            $returnValue->chr_categoria = '';
            $returnValue->chr_comentario = '';
            $returnValue->chr_mes = '';
            $returnValue->dec_saldo = 0;
            $returnValue->dec_subtotal = 0;
            $returnValue->dec_saldo_actual = 0;
            $returnValue->chr_year = date("Y");
        }
        return $returnValue;
    }

    private function getOrdenMovimientoById($movimientoId) {
        $returnValue = NULL;
        $sql = "SELECT * FROM mov_orden_movimiento WHERE id =" . $movimientoId;
        $res = $this->db->get_results($sql);
        if (is_array($res) && count($res) > 0) {
            $returnValue = array_shift($res);
        }
        return $returnValue;
    }

    /**
     * Método para crear el detalle de un movimiento
     * @param object $objMovimiento
     * @return int
     */
    private function addMovimiento($objMovimiento) {

        $returnValue = 0;
        $objOrderMovimiento = new stdClass();
        $objOrderMovimiento->int_orden_movimiento_id = $objMovimiento->int_orden_movimiento_id;
        $objOrderMovimiento->int_creatorid = $this->getUserid();
        $objOrderMovimiento->date_timecreated = time();
        $objOrderMovimiento->area = $objMovimiento->area;
        $objOrderMovimiento->string = $objMovimiento->string;
        $objOrderMovimiento->proyecto = $objMovimiento->proyecto;
        $objOrderMovimiento->categoria = $objMovimiento->categoria;
        $objOrderMovimiento->comentario = $objMovimiento->comentario;

        $objOrderMovimiento->chr_area_id = $objMovimiento->chr_area_id;
        $objOrderMovimiento->chr_string_id = $objMovimiento->chr_string_id;
        $objOrderMovimiento->chr_proyecto_id = $objMovimiento->chr_proyecto_id;
        $objOrderMovimiento->chr_categoria_id = $objMovimiento->chr_categoria_id;
        $objOrderMovimiento->chr_comentario_id = $objMovimiento->chr_comentario_id;

        $objOrderMovimiento->mes = $objMovimiento->mes;
        $objOrderMovimiento->saldo = $objMovimiento->saldo;
        $objOrderMovimiento->monto = $objMovimiento->monto;
        $objOrderMovimiento->chr_year = $objMovimiento->chr_year;
        //validamos si es un insert o un update
        if ($objMovimiento->int_orden_movimiento_detalle_id > 0) {
            //update
            $sql = "UPDATE mov_orden_movimiento_detalle  SET ";
            
            $sql .= "chr_area_id = '" . $objOrderMovimiento->chr_area_id . "', ";
            $sql .= "chr_string_id = '" . $objOrderMovimiento->chr_string_id . "', ";
            $sql .= "chr_proyecto_id = '" . $objOrderMovimiento->chr_proyecto_id . "', ";
            $sql .= "chr_categoria_id = '" . $objOrderMovimiento->chr_categoria_id . "', ";
            $sql .= "chr_comentario_id = '" . $objOrderMovimiento->chr_comentario_id . "', ";

            $sql .= "chr_area = '" . $objOrderMovimiento->area . "', ";
            $sql .= "chr_string = '" . $objOrderMovimiento->string . "', ";
            $sql .= "chr_proyecto = '" . $objOrderMovimiento->proyecto . "', ";
            $sql .= "chr_categoria = '" . $objOrderMovimiento->categoria . "', ";
            $sql .= "chr_comentario = '" . $objOrderMovimiento->comentario . "', ";
            $sql .= "chr_mes = '" . $objOrderMovimiento->mes . "', ";
            $sql .= "dec_saldo = '" . $objOrderMovimiento->saldo . "', ";
            $sql .= "dec_subtotal = '" . $objOrderMovimiento->monto . "', ";
            $sql .= "chr_year = '" . $objOrderMovimiento->chr_year . "' ";
            $sql .= " WHERE id='" . $objMovimiento->int_orden_movimiento_detalle_id . "'";
            //var_dump($sql);
            $res_nuevoregistro = $this->db->query($sql);
            $returnValue = $objMovimiento->int_orden_movimiento_detalle_id;
        } else {
            //insert
            $sql = "INSERT INTO mov_orden_movimiento_detalle (int_orden_movimiento_id, int_creatorid, date_timecreated, chr_area_id, chr_string_id, chr_proyecto_id, chr_categoria_id, chr_comentario_id, chr_area, chr_string, chr_proyecto, chr_categoria, chr_comentario, chr_mes, dec_saldo, dec_subtotal, chr_year) ";
            $sql .= "VALUES('$objOrderMovimiento->int_orden_movimiento_id','$objOrderMovimiento->int_creatorid','$objOrderMovimiento->date_timecreated', '$objOrderMovimiento->chr_area_id','$objOrderMovimiento->chr_string_id','$objOrderMovimiento->chr_proyecto_id', '$objOrderMovimiento->chr_categoria_id','$objOrderMovimiento->chr_comentario_id', '$objOrderMovimiento->area','$objOrderMovimiento->string','$objOrderMovimiento->proyecto', '$objOrderMovimiento->categoria','$objOrderMovimiento->comentario','$objOrderMovimiento->mes','$objOrderMovimiento->saldo','$objOrderMovimiento->monto','$objOrderMovimiento->chr_year')";
            $res_nuevoregistro = $this->db->query($sql);
            if ($res_nuevoregistro) {
                $returnValue = $this->db->insert_id;
            }
        }
        //actualizamos el estado de la orden de movimiento a en proceso para el restado de los saldos
        $sql_update = "UPDATE mov_orden_movimiento SET int_statusid='" . ESTADO_ORDEN_MOVIMIENTO_EN_PROCESO . "' WHERE id='" . $objMovimiento->int_orden_movimiento_id . "'";
        $this->db->query($sql_update);
        return $returnValue;
    }

    /**
     * Método para crear un nuevo movimiento
     * @return int
     */
    private function createMovimiento($request) {
        $returnValue = 0;
        $objOrderMovimiento = new stdClass();
        $objOrderMovimiento->int_creatorid = $this->getUserid();
        $objOrderMovimiento->date_timecreated = time();
        $objOrderMovimiento->area = $request->area;
        $objOrderMovimiento->string = $request->string;
        $objOrderMovimiento->categoria = $request->categoria;
        $objOrderMovimiento->proyecto = $request->proyecto;
        $objOrderMovimiento->comentario = $request->comentario;
        $objOrderMovimiento->chr_area_id = $request->chr_area_id;
        $objOrderMovimiento->chr_string_id = $request->chr_string_id;
        $objOrderMovimiento->chr_categoria_id = $request->chr_categoria_id;
        $objOrderMovimiento->chr_proyecto_id = $request->chr_proyecto_id;
        $objOrderMovimiento->chr_comentario_id = $request->chr_comentario_id;
        $objOrderMovimiento->mes = $request->mes;
        $objOrderMovimiento->chr_year = $request->year;
        $sql = "INSERT INTO mov_orden_movimiento (int_creatorid, date_timecreated, chr_area_id, chr_string_id, chr_categoria_id, chr_proyecto_id, chr_comentario_id, chr_area, chr_string, chr_categoria, chr_proyecto, chr_comentario, chr_mes, chr_year) VALUES('$objOrderMovimiento->int_creatorid','$objOrderMovimiento->date_timecreated','$objOrderMovimiento->chr_area_id','$objOrderMovimiento->chr_string_id','$objOrderMovimiento->chr_categoria_id','$objOrderMovimiento->chr_proyecto_id','$objOrderMovimiento->chr_comentario_id','$objOrderMovimiento->area','$objOrderMovimiento->string','$objOrderMovimiento->categoria','$objOrderMovimiento->proyecto','$objOrderMovimiento->comentario','$objOrderMovimiento->mes','$objOrderMovimiento->chr_year')";
        $res_nuevoregistro = $this->db->query($sql);
        if ($res_nuevoregistro) {
            $returnValue = $this->db->insert_id;
        }
        //generar un código de orden de movimiento
        $code = date("Y") . '-' . str_pad($this->db->insert_id, 6, '0', STR_PAD_LEFT);
        $sql = "UPDATE mov_orden_movimiento SET chr_code='" . $code . "' WHERE id =" . $this->db->insert_id;
        $this->db->query($sql);
        return $returnValue;
    }

    public function anularMovimiento($request) {
        $error = 0;
        $message = '';
        $this->anularMovimientoById($request->id);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'orden_movimiento_id' => $request->id
            ]
        ];
    }

    private function anularMovimientoById($movimientoId) {
        $returnValue = TRUE;
        $sql = "UPDATE mov_movimiento SET int_statusid='" . ESTADO_MOVIMIENTO_ANULADA . "' WHERE id ='" . $movimientoId . "'";
        $this->db->query($sql);
        return $returnValue;
    }

    public function eliminarMovimiento($request) {
        $error = 0;
        $message = '';
        $objOrdenMoviemientoDetalleById = $this->getOrdenMovimientoDetalleById($request->id);
        $this->eliminarOrdenMovimientoDetalle($request->id);
        $monto = $this->updateMontoTotalOrdenMovimiento($objOrdenMoviemientoDetalleById->int_orden_movimiento_id);
        $objOrdenMovimiento = $this->getOrdenMovimientoById($objOrdenMoviemientoDetalleById->int_orden_movimiento_id);
        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'orden_movimiento_detalle_id' => $request->id,
                'objOrdenMovimiento' => $objOrdenMovimiento
            ]
        ];
    }

    public function saveMovimiento($request) {

        $error = 0;
        $message = '';
        if (!isset($request->year)) {
            $request->year = date("Y");
        }

        $idMovimiento = $this->generarMovimiento($request);

        return [
            'error' => $error,
            'message' => $message,
            'subject' => $request->subject,
            'data' => [
                'movimientoid' => ''
            ]
        ];
    }

    public function generarMovimiento($request) {

        $returnValue = NULL;
        $orden_movimiento_id = $request->orden_movimiento_id;
        //obtenemos el objeto de la orden de movimiento
        $objOrdenMovimiento = $this->getOrdenMovimientoById($orden_movimiento_id);

        if (is_object($objOrdenMovimiento)) {
            $objOrdenMovimiento->txt_sustento = $request->txt_sustento;
            $objOrdenMovimiento->dec_total = $request->dec_total;
            $objOrdenMovimiento->dec_new_saldo = $request->dec_new_saldo;
            $objOrdenMovimiento->chr_title = $request->name;
            $objOrdenMovimiento->chr_year = $request->year;
            $objOrdenMovimiento->chr_categoria_id = $objOrdenMovimiento->chr_categoria_id; //$this->getCuentaCategoriaByCategoria($objOrdenMovimiento->chr_categoria);
            $objOrdenMovimiento->chr_nombre_string = $objOrdenMovimiento->chr_string_id; //$this->getNombreStringByCategoria($objOrdenMovimiento->chr_string);
            
            $returnValue = $this->setMovimiento($objOrdenMovimiento);
        }

        return $returnValue;
    }

    private function getCuentaCategoriaByCategoria($chr_categoria, $year) {
        $returnValue = '';
        $sql = "SELECT chr_categoria_id FROM " . TABLE_INGRESO . " WHERE chr_categoria_id='" . $chr_categoria . "' AND anio='" . $year . "' GROUP BY chr_categoria_id";
        $result = $this->db->get_results($sql);
        if (is_array($result) && count($result) > 0) {
            $oResult = array_shift($result);
            if (is_object($oResult)) {
                $returnValue = $oResult->chr_categoria_id;
            }
        }
        return $returnValue;
    }

    private function getNombreStringByCategoria($cod_string, $year) {
        $returnValue = '';
        $sql = "SELECT chr_string_id FROM " . TABLE_INGRESO . " WHERE chr_string_id='" . $cod_string . "' AND anio='" . $year . "' GROUP BY chr_string_id";
        $result = $this->db->get_results($sql);
        if (is_array($result) && count($result) > 0) {
            $oResult = array_shift($result);
            if (is_object($oResult)) {
                $returnValue = $oResult->string;
            }
        }
        return $returnValue;
    }

    private function setMovimiento($objOrdenMovimiento) {
        $sql = "INSERT INTO mov_movimiento (chr_code_orden_movimiento, int_creatorid, date_timecreated, dec_total, txt_sustento, dec_new_saldo, chr_area_id, chr_string_id, chr_categoria_id, chr_proyecto_id, chr_comentario_id,chr_area, chr_string, chr_categoria, chr_proyecto, chr_comentario, chr_mes, chr_title, chr_year, chr_cuenta_categoria, chr_nombre_string)";
        $sql .= " VALUES('".$objOrdenMovimiento->chr_code."','".$objOrdenMovimiento->int_creatorid."','" . time() . "','".$objOrdenMovimiento->dec_total."','" . $objOrdenMovimiento->txt_sustento . "','".$objOrdenMovimiento->dec_new_saldo."','".$objOrdenMovimiento->chr_area_id."','".$objOrdenMovimiento->chr_string_id."','".$objOrdenMovimiento->chr_categoria_id."','".$objOrdenMovimiento->chr_proyecto_id."','".$objOrdenMovimiento->chr_comentario_id."','".$objOrdenMovimiento->chr_area."','".$objOrdenMovimiento->chr_string_id."','".$objOrdenMovimiento->chr_categoria."','".$objOrdenMovimiento->chr_proyecto."','".$objOrdenMovimiento->chr_comentario."','".$objOrdenMovimiento->chr_mes."','".$objOrdenMovimiento->chr_title."','".$objOrdenMovimiento->chr_year."','".$objOrdenMovimiento->chr_categoria_id."','".$objOrdenMovimiento->chr_string."')";

        //var_dump($sql); die();

        $res_nuevoregistro = $this->db->query($sql);
        if ($res_nuevoregistro) {
            $returnValue = $this->db->insert_id;
        }
        //generar un código de orden de movimiento
        $code = date("Y") . '-' . str_pad($this->db->insert_id, 6, '0', STR_PAD_LEFT);
        $sql = "UPDATE mov_movimiento SET chr_code='" . $code . "', date_timemodified='" . time() . "', int_statusid='" . ESTADO_MOVIMIENTO_CREADA . "' WHERE id =" . $this->db->insert_id;
        $this->db->query($sql);
        //copiamos los detalles de la orden de movimiento        
        $detalle_orden_movimiento = $this->getOrdenMovimientoDetalleByOrdenId($objOrdenMovimiento->id);
        //var_dump($detalle_orden_movimiento); die();
        if (is_array($detalle_orden_movimiento) && count($detalle_orden_movimiento) > 0) {
            foreach ($detalle_orden_movimiento as $index => $objDetalleOrdenMovimiento) {

                $objDetalleOrdenMovimiento->chr_categoria_id = $objDetalleOrdenMovimiento->chr_categoria_id; //$this->getCuentaCategoriaByCategoria($objDetalleOrdenMovimiento->chr_categoria_id);
                $objDetalleOrdenMovimiento->chr_nombre_string = $objDetalleOrdenMovimiento->chr_string_id; //$this->getNombreStringByCategoria($objDetalleOrdenMovimiento->chr_string_id);

                $sql = "INSERT INTO mov_movimiento_detalle (int_movimiento_id, int_creatorid, date_timecreated, chr_area_id, chr_string_id, chr_categoria_id, chr_proyecto_id, chr_comentario_id, chr_area, chr_string, chr_proyecto, chr_categoria, chr_comentario, chr_mes, dec_saldo, dec_subtotal,chr_nombre_string, chr_year)";
                $sql .= " VALUES('" . $returnValue . "', '" . $objDetalleOrdenMovimiento->int_creatorid . "', '" . time() . "','".$objDetalleOrdenMovimiento->chr_area_id."','".$objDetalleOrdenMovimiento->chr_string_id."','".$objDetalleOrdenMovimiento->chr_categoria_id."','".$objDetalleOrdenMovimiento->chr_proyecto_id."','".$objDetalleOrdenMovimiento->chr_comentario_id."','" . $objDetalleOrdenMovimiento->chr_area . "','" . $objDetalleOrdenMovimiento->chr_string_id . "','" . $objDetalleOrdenMovimiento->chr_proyecto . "','" . $objDetalleOrdenMovimiento->chr_categoria . "','" . $objDetalleOrdenMovimiento->chr_comentario . "','" . $objDetalleOrdenMovimiento->chr_mes . "','" . $objDetalleOrdenMovimiento->dec_saldo . "','" . $objDetalleOrdenMovimiento->dec_subtotal . "','" . $objDetalleOrdenMovimiento->chr_string . "','".$objDetalleOrdenMovimiento->chr_year."')";

                $res_nuevoregistro = $this->db->query($sql);

            }
        }

        //finalizamos la orden de movimiento
        $sql = "UPDATE mov_orden_movimiento SET int_statusid='" . ESTADO_ORDEN_MOVIMIENTO_FINALIZADA . "' WHERE id='" . $objOrdenMovimiento->id . "'";

        $this->db->query($sql);
        //guardamos los logs para los movimientos
        //$this->setLogMovimiento($returnValue); PENDIENTE LOG


        return $returnValue;
    }

    /**
     * Registrar el movimiento en la tabla de logs
     * @param int $movimiento_id
     */
    private function setLogMovimiento($movimiento_id) {
        $objMovimiento = $this->getMovimientoById($movimiento_id);

        $sql = "INSERT INTO log_movimiento (int_movimientoid, chr_code, chr_code_orden_movimiento, int_creatorid, date_timecreated, date_timemodified, is_deleted, is_active, dec_total, txt_sustento, dec_new_saldo, chr_area, chr_string, chr_categoria, chr_proyecto, chr_comentario, chr_categoria_id, chr_nombre_string, chr_mes, date_timeregistered, int_userid, chr_title, txt_feedback, chr_year)";
        $sql .= " VALUES('$objMovimiento->id','$objMovimiento->chr_code', '$objMovimiento->chr_code_orden_movimiento','$objMovimiento->int_creatorid','" . $objMovimiento->date_timecreated . "','" . $objMovimiento->date_timemodified . "','" . $objMovimiento->is_deleted . "','" . $objMovimiento->is_active . "','$objMovimiento->dec_total','" . $objMovimiento->txt_sustento . "','$objMovimiento->dec_new_saldo','$objMovimiento->chr_area','$objMovimiento->chr_string','$objMovimiento->chr_categoria','$objMovimiento->chr_proyecto','$objMovimiento->chr_comentario','$objMovimiento->chr_categoria_id','$objMovimiento->chr_nombre_string','$objMovimiento->chr_mes','" . time() . "','" . $this->getUserid() . "','" . $objMovimiento->chr_title . "','" . $objMovimiento->txt_feedback . "','" . $objMovimiento->chr_year . "')";
        
        //var_dump($sql);

        $res_nuevoregistro = $this->db->query($sql);
        if ($res_nuevoregistro) {
            $id = $this->db->insert_id;
        }
        //registramos el detalle del movimiento en los logs
        $movimientos = $this->getMovimientosById($movimiento_id);
        if (is_array($movimientos) && count($movimientos) > 0) {
            foreach ($movimientos as $index => $objDetalleMovimiento) {
                $sql = "INSERT INTO log_movimiento_detalle (int_movimiento_detalle_id,int_movimiento_id, int_creatorid, date_timecreated, date_timemodified, is_active, is_deleted, chr_area, chr_string, chr_proyecto, chr_categoria, chr_comentario, chr_mes, dec_saldo, dec_subtotal, chr_categoria_id, chr_nombre_string, chr_year)";
                $sql .= " VALUES('" . $id . "', '" . $objDetalleMovimiento->int_movimiento_id . "','" . $objDetalleMovimiento->int_creatorid . "', '" . $objDetalleMovimiento->date_timecreated . "','" . $objDetalleMovimiento->date_timemodified . "','" . $objDetalleMovimiento->is_active . "','" . $objDetalleMovimiento->is_deleted . "','" . $objDetalleMovimiento->chr_area . "','" . $objDetalleMovimiento->chr_string . "','" . $objDetalleMovimiento->chr_proyecto . "','" . $objDetalleMovimiento->chr_categoria . "','" . $objDetalleMovimiento->chr_comentario . "','" . $objDetalleMovimiento->chr_mes . "','" . $objDetalleMovimiento->dec_saldo . "','" . $objDetalleMovimiento->dec_subtotal . "','" . $objDetalleMovimiento->chr_categoria_id . "','" . $objDetalleMovimiento->chr_nombre_string . "', '".$objDetalleMovimiento->chr_year."')";
                $res_nuevoregistro = $this->db->query($sql);
            }
        }
    }

    private function getOrdenMovimientoDetalleByOrdenId($orden_movimiento_id) {
        $returnValue = [];
        $sql = "SELECT * FROM mov_orden_movimiento_detalle WHERE int_orden_movimiento_id='" . $orden_movimiento_id . "' AND is_deleted='0' AND is_active='1'";
        $returnValue = $this->db->get_results($sql);
        return $returnValue;
    }

    private function eliminarOrdenMovimientoDetalle($ordenMovimientoDetalleId) {
        $sql = "UPDATE mov_orden_movimiento_detalle SET is_active = '0', is_deleted='1', date_timemodified='" . time() . "' WHERE id=" . $ordenMovimientoDetalleId;
        $this->db->query($sql);
    }

    /**
     * obtiene código html para pintar el dropdown
     * @param string $area
     * @return string
     */
    private function getHtmlDropDownComentario($area = '', $string = '', $proyecto = '', $categoria = '', $chr_year = '') {
        $returValue = '<label for="inputComentario" class="control-label">Comentario</label>';
        $returValue .= '<div class="divFeedbackCustom">';
        $returValue .= '<select name="inputComentario" id="inputComentario" class="form-control">';
        $returValue .= '<option value="">[ SELECCIONE ]</option>';
        if (strlen(trim($area)) > 0 && strlen(trim($string)) > 0) {
            $items = $this->getComentarioByAreaByStringByProyectoByCategoria($area, $string, $proyecto, $categoria, $chr_year);
            if (is_array($items) && count($items) > 0) {
                foreach ($items as $index => $item) {
                    $returValue .= '<option value="' . $item->chr_comentario_id . '">' . $item->chr_comentario . '</option>';
                }
            }
        }
        $returValue .= '</select></div>';
        return $returValue;
    }
    /**
     * 
     * @param type $orden_movimiento_id
     * @param type $orden_movimiento_detalle_id
     * @param type $year
     * @return string
     */
    private function getHtmlFormCuenta($orden_movimiento_id = 0, $orden_movimiento_detalle_id = 0, $year, $request=array()) {
        $returnValue = '';
        //obtenemos el objeto de la orden de movimiento detalle
        $objOrdenMovimientoDetalle = $this->getOrdenMovimientoDetalleById($orden_movimiento_detalle_id);
        $months = $this->getMes();
        $areas = $this->getAreas($year);
        //var_dump($areas);
        $returnValue .= '<form name="formModalTransferenciaCuenta" id="formModalTransferenciaCuenta" action="" method="post" onsubmit="return false;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">FORMULARIO DE CUENTA</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inputArea" class="control-label">ÁREA</label>
                                <div class="divFeedbackCustom">
                                <select name="inputArea" id="inputArea" class="form-control">
                                    <option value="">[ SELECCIONE ]</option>';
        if (is_array($areas) && count($areas) > 0) {
            foreach ($areas as $indice => $area) {
                if ($area->chr_area_id == $objOrdenMovimientoDetalle->chr_area_id) {
                    $returnValue .= '<option value="' . $area->chr_area_id . '" selected="selected">' . $area->chr_area . '</option>';
                } else {
                    $returnValue .= '<option value="' . $area->chr_area_id . '">' . $area->chr_area . '</option>';
                }
            }
        }
        $returnValue .= '</select>
                        </div>
                            </div>
                            <div class="form-group" id="divDropDownString">
                                <label for="inputString" class="control-label">STRING</label>
                                <div class="divFeedbackCustom">
                                <select name="inputString" id="inputString" class="form-control">
                                    <option value="">[ SELECCIONE ]</option>';
        //obtenemos los código strings
        if ($orden_movimiento_detalle_id > 0) {
            $strings = $this->getStringByArea($objOrdenMovimientoDetalle->chr_area, $year);
            if (is_array($strings) && count($strings) > 0) {
                foreach ($strings as $indice => $string) {
                    if ($string->chr_string_id == $objOrdenMovimientoDetalle->chr_string) {
                        $returnValue .= '<option value="' . $string->chr_string_id . '" selected="selected">' . $string->chr_string_id . ' - ' . $string->string . '</option>';
                    } else {
                        $returnValue .= '<option value="' . $string->chr_string_id . '">' . $string->chr_string_id . ' - ' . $string->chr_string . '</option>';
                    }
                }
            }
        }
        $returnValue .= '</select>
                            </div>
                            </div>
                            <div class="form-group" id="divDropDownProyecto">
                                <label for="inputProyecto" class="control-label">PROYECTO</label>
                                <select name="inputProyecto" id="inputProyecto" class="form-control">
                                    <option value="">[ SELECCIONE ]</option>';
        //obtenemos los proyectos
        if ($orden_movimiento_detalle_id > 0) {
            $proyectos = $this->getProyectoByAreaByString($objOrdenMovimientoDetalle->chr_area, $objOrdenMovimientoDetalle->chr_string, $year);
            if (is_array($proyectos) && count($proyectos) > 0) {
                foreach ($proyectos as $indice => $proyecto) {
                    if ($proyecto->proyecto == $objOrdenMovimientoDetalle->chr_proyecto) {
                        $returnValue .= '<option value="' . $proyecto->chr_proyecto_id . '" selected="selected">' . $proyecto->chr_proyecto . '</option>';
                    } else {
                        $returnValue .= '<option value="' . $proyecto->chr_proyecto_id . '">' . $proyecto->chr_proyecto . '</option>';
                    }
                }
            }
        }
        $returnValue .= '</select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="divDropDownCategoria">
                                <label for="inputCategoria" class="control-label">CATEGORÍA</label>
                                <div class="divFeedbackCustom">
                                <select name="inputCategoria" id="inputCategoria" class="form-control">
                                    <option value="">[ SELECCIONE ]</option>';
        //listamos las categorias
        if ($orden_movimiento_detalle_id > 0) {
            $categorias = $this->getCategoriaByAreaByStringByProyecto($objOrdenMovimientoDetalle->chr_area, $objOrdenMovimientoDetalle->chr_string, $objOrdenMovimientoDetalle->chr_proyecto, $year);
            foreach ($categorias as $index => $categoria) {
                if ($categoria->categoria == $objOrdenMovimientoDetalle->chr_categoria) {
                    $returnValue .= '<option value="' . $categoria->chr_categoria_id . '" selected="selected">' . $categoria->chr_categoria . '</option>';
                } else {
                    $returnValue .= '<option value="' . $categoria->chr_categoria_id . '">' . $categoria->chr_categoria . '</option>';
                }
            }
        }
        $returnValue .= '</select></div>
                            </div>
                            <div class="form-group" id="divDropDownComentario">
                                <label for="inputComentario" class="control-label">COMENTARIO</label>
                                <div class="divFeedbackCustom">
                                <select name="inputComentario" id="inputComentario" class="form-control">
                                    <option value="">[ SELECCIONE ]</option>';
        //listamos los comentarios
        if ($orden_movimiento_detalle_id > 0) {
            $comentarios = $this->getComentarioByAreaByStringByProyectoByCategoria($objOrdenMovimientoDetalle->chr_area, $objOrdenMovimientoDetalle->chr_string, $objOrdenMovimientoDetalle->chr_proyecto, $objOrdenMovimientoDetalle->chr_categoria,  $year);
            foreach ($comentarios as $index => $comentario) {
                if ($comentario->comentario == $objOrdenMovimientoDetalle->chr_comentario) {
                    $returnValue .= '<option value="' . $comentario->chr_comentario_id . '" selected="selected">' . $comentario->chr_comentario . '</option>';
                } else {
                    $returnValue .= '<option value="' . $comentario->chr_comentario_id . '">' . $comentario->chr_comentario . '</option>';
                }
            }
        }
        $returnValue .= '</select></div>
                            </div>
                            <div class="form-group">
                                <label for="inputMes" class="control-label">MES</label>
                                <div class="divFeedbackCustom">
                                <select name="inputMes" id="inputMes" class="form-control">
                                    <option value="">[ SELECCIONE ]</option>';
        if (is_array($months) && count($months) > 0) {
            foreach ($months as $index => $value) {
                if ($index == $objOrdenMovimientoDetalle->chr_mes) {
                    $returnValue .= '<option value="' . $index . '" selected="selected">' . $value . '</option>';
                } else {
                    $returnValue .= '<option value="' . $index . '">' . $value . '</option>';
                }
            }
        }
        $returnValue .= '</select></div>
                            </div>                            
                            <div class="form-group">
                                <label for="inputSaldoActual" class="control-label">SALDO ACTUAL</label>';
                                if ($orden_movimiento_detalle_id > 0) {
                                    $returnValue .= '<input type="text" class="form-control" id="inputSaldoActual" name="inputSaldoActual" readonly="readonly" value="' . $objOrdenMovimientoDetalle->dec_saldo_actual . '">';
                                } else {
                                    $returnValue .= '<input type="text" class="form-control" id="inputSaldoActual" name="inputSaldoActual" readonly="readonly">';
                                }
                                $returnValue .= '</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="inputMontoTransferir" class="control-label">MONTO A TRANSFERIR</label>
                                <div class="divFeedbackCustom">';
                                if ($orden_movimiento_detalle_id > 0) {
                                    $returnValue .= '<input type="hidden" id="chr_year_movimiento_detalle" name="chr_year_movimiento_detalle" value="' . $objOrdenMovimientoDetalle->chr_year . '">';
                                    $returnValue .= '<input type="text" class="form-control" id="inputMontoTransferir" name="inputMontoTransferir" value="' . $objOrdenMovimientoDetalle->dec_subtotal . '">';
                                } else {
                                    $returnValue .= '<input type="hidden" id="chr_year_movimiento_detalle" name="chr_year_movimiento_detalle" value="">';
                                    $returnValue .= '<input type="text" class="form-control" id="inputMontoTransferir" name="inputMontoTransferir" readonly="readonly">';
                                }
                                $returnValue .= '</div>
                            </div>                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="chr_area_cuenta" id="chr_area_cuenta" value="' . $request->chr_area . '" />
                    <input type="hidden" name="chr_string_cuenta" id="chr_string_cuenta" value="' . $request->chr_string . '" />
                    <input type="hidden" name="chr_categoria_cuenta" id="chr_categoria_cuenta" value="' . $request->chr_categoria . '" />
                    <input type="hidden" name="chr_proyecto_cuenta" id="chr_proyecto_cuenta" value="' . $request->chr_proyecto . '" />
                    <input type="hidden" name="chr_comentario_cuenta" id="chr_comentario_cuenta" value="' . $request->chr_comentario . '" />
                    <input type="hidden" name="chr_mes_cuenta" id="chr_mes_cuenta" value="' . $request->chr_mes . '" />
                    <input type="hidden" name="chr_year_cuenta" id="chr_year_cuenta" value="' . $year . '" />
                    <input type="hidden" name="orden_movimiento_id" id="orden_movimiento_id" value="' . $orden_movimiento_id . '" />
                    <input type="hidden" name="orden_movimiento_id" id="orden_movimiento_detalle_id" value="' . $orden_movimiento_detalle_id . '" />
                    <button type="button" class="btn btn-danger" data-dismiss="modal" name="btnCancelCuenta" id="btnCancelCuenta"><span class="glyphicon glyphicon-floppy-remove"></span> CERRAR</button>
                    <button type="submit" class="btn btn-success" name="btnAddCuenta" id="btnAddCuenta"><span class="glyphicon glyphicon-floppy-saved"></span> AGREGAR MONTO</button>
                </div>';
        return $returnValue;
    }

    private function getHtmlFormTransferencia($monto_requerido, $transferencia, $diferencia, $orden_movimiento_id = 0, $chr_year = '') {
        $returnValue = '';
        $returnValue .= '<div class="row">';
        $returnValue .= '<div class="col-md-2">&nbsp;';
        $returnValue .= '</div>';
        $returnValue .= '<div class="col-md-8">';
        $objOrdenMovimiento = $this->getOrdenMovimientoById($orden_movimiento_id);
        $returnValue .= '<form class="form-horizontal" name="formMovimientosPresupuesto" id="formMovimientosPresupuesto" action="" method="POST" onsubmit="return false;">
                    <div class="form-group">
                        <h5>CUENTA RECEPTORA</h5>
                    </div>            
                    <ol class="breadcrumb">
                        <li><a href="#">' . $objOrdenMovimiento->chr_area . '</a></li>
                        <li><a href="#">' . $objOrdenMovimiento->chr_string . '</a></li>
                        <li><a href="#">' . $objOrdenMovimiento->chr_categoria . '</a></li>
                        <li><a href="#">' . $objOrdenMovimiento->chr_proyecto . '</a></li>
                        <li class="active">' . $objOrdenMovimiento->chr_comentario . '</li>
                    </ol>
                    <div class="form-group">
                      <label for="inputMontoRequerido" class="col-sm-2 control-label">SALDO ACTUAL</label>
                      <div class="col-sm-10">
                        <input type="hidden" name="chr_area" id="chr_area" value="'.$objOrdenMovimiento->chr_area.'" />
                        <input type="hidden" name="chr_string" id="chr_string" value="'.$objOrdenMovimiento->chr_string.'" />
                        <input type="hidden" name="chr_categoria" id="chr_categoria" value="'.$objOrdenMovimiento->chr_categoria.'" />
                        <input type="hidden" name="chr_proyecto" id="chr_proyecto" value="'.$objOrdenMovimiento->chr_proyecto.'" />
                        <input type="hidden" name="chr_comentario" id="chr_comentario" value="'.$objOrdenMovimiento->chr_comentario.'" />
                        <input type="hidden" name="chr_mes" id="chr_mes" value="'.$objOrdenMovimiento->chr_mes.'" />
                        <input type="text" class="form-control" id="inputMontoRequerido" name="inputMontoRequerido" disabled="disabled" value="' . number_format($monto_requerido, 3) . '">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputNameTransferencia" class="col-sm-2 control-label">NOMBRE DE LA TRANSFERENCIA</label>
                      <div class="col-sm-10">
                            <div class="divFeedbackCustom">
                                <input type="text" class="form-control" id="inputNameTransferencia" name="inputNameTransferencia" >
                            </div>
                      </div>
                    </div>
                    <div class="form-group">
                        <h5>CUENTAS PROVEEDORAS</h5>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-10">&nbsp;</div>
                            <div class="col-md-2 text-right"><button type="button" id="btnAgregarCuenta" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> CUENTA</button></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-hover" id="tableListMovimientoPresupuesto" name="tableListMovimientoPresupuesto">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ÁREA</th>
                                            <th>CÓDIGO STRING</th>
                                            <th>CATEGORÍA</th>
                                            <th>PROYECTO</th>
                                            <th>COMENTARIO</th>
                                            <th>MONTO A TRANSFERIR</th>
                                            <th>ACCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="5" class="text-right">&nbsp;</th>
                                            <th colspan="2" class="text-right">SUBTOTAL</th>
                                            <th>
                                                <input type="hidden" name="orden_movimientoid" id="orden_movimientoid" value="' . $orden_movimiento_id . '" />
                                                <input type="hidden" name="chr_year" id="chr_year" value="' . $chr_year . '" />
                                                <div class="divFeedbackCustom">
                                                    <input type="text" class="form-control" readonly="readonly" value="' . $transferencia . '" name="inputMontoTotalTransferido" id="inputMontoTotalTransferido"></th>
                                                </div>
                                        </tr>
                                        <tr>
                                            <th colspan="5" class="text-right">&nbsp;</th>
                                            <th colspan="2" class="text-right">TOTAL SALDO</th>
                                            <th><input type="text" class="form-control" disabled="disabled" name="inputDiferenciaTransferido" id="inputDiferenciaTransferido" value="' . number_format($diferencia, 3) . '"></th>
                                        </tr>
                                        <tr>
                                            <th colspan="8">
                                                <div class="form-group">
                                                    <label for="inputSustento" class="control-label">SUSTENTO</label>
                                                    <div class="divFeedbackCustom">
                                                        <textarea name="inputSustento" id="inputSustento" class="form-control"></textarea>
                                                    </div>
                                                </div>                                             
                                            </th>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10 text-center">
                        <a class="btn btn-warning btn-lg" id="btnCancelarProcesarTransferencia" name="btnCancelarProcesarTransferencia"><span class="glyphicon glyphicon-floppy-remove"></span> CANCELAR</a>
                        <button type="submit" class="btn btn-success btn-lg"><span class="glyphicon glyphicon-floppy-saved"></span> GENERAR MOVIMIENTO</button>
                      </div>
                    </div>
                  </form>';
        $returnValue .= '</div>';
        $returnValue .= '<div class="col-md-2">&nbsp;';
        $returnValue .= '</div>';
        $returnValue .= '</div>';
        return $returnValue;
    }

    private function getHtmlRowTable($objRow, $cont) {
          //DATO QUE NO SE USARA EN EL SISTEMA DE VISTA
//        $returnValue = '';
//        $returnValue .= '<tr>';
//        $returnValue .= '<td>' . ($cont + 1) . '</td>';
//        $returnValue .= '<td>' . utf8_encode($objRow->area) . '</td>';
//        $returnValue .= '<td>' . utf8_encode($objRow->chr_string_id) . '</td>';
//        $returnValue .= '<td>' . utf8_encode($objRow->categoria) . '</td>';
//        $returnValue .= '<td>' . utf8_encode($objRow->proyecto) . '</td>';
//        $returnValue .= '<td>' . utf8_encode($objRow->comentario) . '</td>';
//        $returnValue .= '<td>' . utf8_encode($objRow->mes) . '</td>';
//        $returnValue .= '<td>' . number_format($objRow->presupuesto_mensual, 3) . '</td>';
//        $returnValue .= '<td>' . number_format($objRow->gasto, 3) . '</td>';
//        $returnValue .= '<td>' . number_format($objRow->diferencia, 3) . '</td>';
//        $returnValue .= '<td><a href="#" class="presupuestoTransferencia_transferir" data-year="' . $objRow->chr_year . '" data-mes="' . $objRow->int_mes . '" data-comentario="' . utf8_encode($objRow->comentario) . '" data-proyecto="' . utf8_encode($objRow->proyecto) . '" data-categoria="' . $objRow->categoria . '" data-string="' . $objRow->chr_string_id . '" data-area="' . utf8_encode($objRow->area) . '" data-requerido="' . ($objRow->presupuesto_mensual - $objRow->gasto) . '">Solicitar</a></td>';
//        $returnValue .= '</tr>';
        $returnValue = '';
        $returnValue .= '<tr>';
        $returnValue .= '<td>' . ($cont + 1) . '</td>';
        $returnValue .= '<td>' . utf8_encode($objRow->chr_area) . '</td>';
        $returnValue .= '<td>' . utf8_encode($objRow->chr_string_id) . '</td>';
        $returnValue .= '<td>' . utf8_encode($objRow->chr_categoria) . '</td>';
        $returnValue .= '<td>' . utf8_encode($objRow->chr_proyecto) . '</td>';
        $returnValue .= '<td>' . utf8_encode($objRow->chr_comentario) . '</td>';
        $returnValue .= '<td>' . utf8_encode($objRow->mes) . '</td>';
        $returnValue .= '<td>' . number_format($objRow->presupuesto_mensual, 3) . '</td>';
        $returnValue .= '<td>' . number_format($objRow->gasto, 3) . '</td>';
        $returnValue .= '<td>' . number_format($objRow->diferencia_total, 3) . '</td>';
        $returnValue .= '<td><a href="#" class="presupuestoTransferencia_transferir" data-year="' . $objRow->chr_year . '" data-mes="' . $objRow->int_mes . '" data-comentario-id="' . $objRow->chr_comentario_id . '" data-proyecto-id="' . $objRow->chr_proyecto_id . '" data-categoria-id="' . $objRow->chr_categoria_id . '" data-string-id="' . $objRow->chr_string_id . '" data-area-id="' . $objRow->chr_area_id . '" data-area="' . utf8_encode($objRow->chr_area) . '" data-string="' . utf8_encode($objRow->chr_string) . '" data-categoria="' . utf8_encode($objRow->chr_categoria) . '" data-proyecto="' . utf8_encode($objRow->chr_proyecto) . '" data-comentario="' . utf8_encode($objRow->chr_comentario) . '" data-requerido="' . ($objRow->presupuesto_mensual - $objRow->gasto) . '">Solicitar</a></td>';
        $returnValue .= '</tr>';
        return $returnValue;
    }

    /**
     * Método para listar el presupuesto mensual por cada área
     * @param string $area
     * @param string $year
     * @return array
     */
    public function getPresupuestoMensualByArea($area,$string,$categoria,$proyecto,$detalle,$year) {
        $qarea = $qstring = $qcategoria = $qproyecto = $qdetalle = "";

        if($area != "0"){ $qarea = " AND pa.chr_area_id = '$area'"; };
        if($string != "0"){ $qstring = " AND pa.chr_string_id = '$string'"; };
        if($categoria != "0"){ $qcategoria = " AND pa.chr_categoria_id = '$categoria'"; };
        if($proyecto != "0"){ $qproyecto = " AND pa.chr_proyecto_id = '$proyecto'"; };
        if($detalle != "0"){ $qdetalle = " AND pa.chr_comentario_id = '$detalle'"; };

        $returnValue = [];
//        $sql = "SELECT pa.mes, ROUND(pa.total,3) AS 'presupuesto_mensual', pa.area, pa.chr_string_id, pa.categoria, pa.proyecto, pa.comentario, pa.anio as chr_year, pa.mes 
//                FROM " . TABLE_INGRESO . " pa
//                WHERE pa.area = '" . $area . "' AND pa.anio='" . $year . "' 
//                ORDER BY pa.mes";
        $sql = "SELECT pa.mes, ROUND(pa.presupuesto_total,3) AS 'presupuesto_mensual', pa.total_gasto as gasto , pa.chr_area_id, pa.chr_string_id, pa.chr_categoria_id, pa.chr_proyecto_id, pa.chr_comentario_id, pa.chr_area, pa.chr_string, pa.chr_categoria, pa.chr_proyecto, pa.chr_comentario, pa.anio as chr_year, pa.diferencia, pa.diferencia_total
                FROM " . VISTA_SALDO . " pa
                WHERE pa.anio='" . $year . "' $qarea $qstring $qcategoria $qproyecto $qdetalle
                ORDER BY pa.mes";
                //WHERE pa.chr_area_id = '" . $area . "' AND pa.anio='" . $year . "'
        $returnValue = $this->db->get_results($sql);
        //iteramos el resultado para obtener las transferencias 
//        if(is_array($returnValue) && count($returnValue)>0){
//            foreach($returnValue as $index=>$item){
////                $item->transferencia = $this->getMontoTransferencia($item->area, $item->chr_string_id, $item->categoria, $item->proyecto, $item->comentario, $item->mes, $item->chr_year);
////                $item->presupuesto_mensual = floatval($item->presupuesto_mensual) + floatval($item->transferencia);
//                //$item->transferencia = $this->getMontoTransferencia($item->area, $item->chr_string_id, $item->categoria, $item->proyecto, $item->comentario, $item->mes, $item->chr_year);
//                //$item->presupuesto_mensual = floatval($item->presupuesto_mensual) + floatval($item->transferencia);
//                $returnValue[$index] =  $item;
//            }
//        }
        return $returnValue;
    }
    
    private function getIdMovimientos($area, $string, $categoria, $proyecto, $comentario, $mes, $chr_year){
        $returnValue = [0];
        $sql ="SELECT id FROM mov_movimiento WHERE chr_mes='".$mes."' AND chr_year='".$chr_year."' AND is_active=1 AND is_deleted=0 AND int_statusid IN(".ESTADO_MOVIMIENTO_CREADA.",".ESTADO_MOVIMIENTO_EJECUTADO.")";
        $saldo = $this->db->get_results($sql);   
        if(is_array($saldo) && count($saldo)>0){
            foreach($saldo as $index=>$item){
                if(!in_array($item->id, $returnValue)){
                    array_push($returnValue, $item->id);
                }
            }
        }
        return $returnValue;
    }
    /**
     * 
     * @param type $area
     * @param type $string
     * @param type $categoria
     * @param type $proyecto
     * @param type $comentario
     * @param type $mes
     * @param type $chr_year
     * @retur float
     */
    private function getMontoTransferencia($area, $string, $categoria, $proyecto, $comentario, $mes, $chr_year){
        $returnValue = 0;
        //obtenemos los ID's de los movimientos para restar los detalles
        $array_id_movimiento = $this->getIdMovimientos($area, $string, $categoria, $proyecto, $comentario, $mes, $chr_year);
        //obtenemos el valor de la cabecera de un movimiento
        $sql ="SELECT SUM(dec_total) as dec_total FROM mov_movimiento WHERE chr_area='".$area."' AND chr_string ='".$string."' AND chr_categoria = '".$categoria."' AND chr_proyecto='".$proyecto."' AND chr_comentario='".$comentario."' AND chr_mes='".$mes."' AND chr_year='".$chr_year."' AND is_active=1 AND is_deleted=0 AND int_statusid IN(".ESTADO_MOVIMIENTO_CREADA.",".ESTADO_MOVIMIENTO_EJECUTADO.")";
        $saldo = $this->db->get_results($sql);
        if(is_array($saldo) && count($saldo)>0){
            $saldo = array_shift($saldo);
            $saldo = $saldo->dec_total;
        }
        //restamos con movimientos en las que fue proveedor
        $sql = "SELECT SUM(dec_subtotal) as dec_subtotal FROM mov_movimiento_detalle WHERE chr_area='".$area."' AND chr_string='".$string."' AND chr_categoria ='".$categoria."' AND chr_proyecto='".$proyecto."' AND chr_comentario='".$comentario."' AND chr_mes ='".$mes."' AND int_movimiento_id IN (".implode(',',$array_id_movimiento).")";
        //$sql = "SELECT SUM(dec_subtotal) as dec_subtotal FROM mov_movimiento_detalle WHERE chr_area='".$area."' AND chr_string='".$string."' AND chr_categoria ='".$categoria."' AND chr_proyecto='".$proyecto."' AND chr_comentario='".$comentario."' AND chr_mes ='".$mes."'";
        $gasto = $this->db->get_results($sql);
        if(is_array($gasto) && count($gasto)>0){
            $gasto = array_shift($gasto);
            $gasto = $gasto->dec_subtotal;
        }
        $returnValue = floatval($saldo) - floatval($gasto);
        return $returnValue;
    }
    /**
     * Método para obtener el gasto por cada comenario en el año
     * @param type $area
     * @param type $chr_string_id
     * @param type $proyecto
     * @param type $categoria
     * @param type $comentario
     * @param type $year
     * @return float
     */
    public function getGastostoMensualByArea($area, $chr_string_id, $proyecto, $categoria, $comentario, $year) {
        $returnValue = [];
        $sql = "SELECT pg.mes, ROUND(SUM(pg.total),3) AS 'gasto_mensual' 
                FROM " . TABLE_GASTO . " pg
                WHERE pg.area = '" . $area . "'
                AND pg.chr_string_id='" . $chr_string_id . "'
                AND pg.proyecto = '" . $proyecto . "'
                AND pg.categoria = '" . $categoria . "'
                AND pg.comentario = '" . $comentario . "'
                AND pg.anio='" . $year . "'
                GROUP BY pg.mes";
        $result = $this->db->get_results($sql);
        if (is_array($result) && count($result) > 0) {
            foreach ($result as $index => $gasto) {
                $returnValue[$gasto->mes] = $gasto->gasto_mensual;
            }
        }
        return $returnValue;
    }

    /**
     * Método para listar las area
     * @param object $request
     * @return array
     */
    public function getAreas($year) {
        $sql = "SELECT IP.chr_area_id, AP.chr_area 
                FROM " . TABLE_INGRESO . " IP
                LEFT JOIN presupuesto_area AP ON AP.chr_area_id = IP.chr_area_id
                WHERE anio='" . $year . "' 
                GROUP BY AP.chr_area 
                ORDER BY AP.chr_area"; //var_dump($sql);
        $res = $this->db->get_results($sql);
        $this->_codificarObjeto($res,array("chr_area"));
        return $res;
    }

    /**
     * 
     * @param type $area
     * @param type $year
     * @return array
     */
    public function getStringByArea($area, $year) {
        $sql = "SELECT pia.chr_string_id, SP.chr_string FROM " . TABLE_INGRESO . " pia
        LEFT JOIN presupuesto_string SP ON SP.chr_string_id = pia.chr_string_id
        WHERE pia.chr_area_id='" . $area . "' AND pia.anio='" . $year . "'
        GROUP BY pia.chr_string_id
        ORDER BY SP.chr_string";
        $res = $this->db->get_results($sql);
        $this->_codificarObjeto($res,array("chr_string"));
        return $res;
    }

    /**
     * 
     * @param type $area
     * @param type $string
     * @param type $year
     * @return array
     */
    public function getProyectoByAreaByString($area, $string, $year) {
        $sql = "SELECT pia.chr_proyecto_id, PP.chr_proyecto FROM " . TABLE_INGRESO . " pia
        LEFT JOIN presupuesto_proyecto PP ON PP.chr_proyecto_id = pia.chr_proyecto_id
        WHERE pia.chr_area_id='" . $area . "' AND pia.chr_string_id='" . $string . "' AND pia.anio='" . $year . "'
        GROUP BY pia.chr_proyecto_id
        ORDER BY PP.chr_proyecto";
        $res = $this->db->get_results($sql);
        $this->_codificarObjeto($res,array("chr_proyecto"));
        return $res;
    }

    /**
     * 
     * @param type $area
     * @param type $string
     * @param type $proyecto
     * @param type $inputCategoria
     * @return array
     */
    public function getCategoriaByAreaByStringByProyecto($area, $string, $proyecto, $year) {
        $sql = "SELECT pia.chr_categoria_id, CP.chr_categoria  
                FROM " . TABLE_INGRESO . " pia
                LEFT JOIN presupuesto_categoria CP ON CP.chr_categoria_id = pia.chr_categoria_id
                WHERE pia.chr_area_id='" . $area . "' AND pia.chr_string_id='" . $string . "' AND chr_proyecto_id ='" . $proyecto . "' AND pia.anio='" . $year . "'
                GROUP BY pia.chr_categoria_id
                ORDER BY CP.chr_categoria";
        $res = $this->db->get_results($sql);
        $this->_codificarObjeto($res,array("chr_categoria"));
        return $res;
    }
    /**
     * 
     * @param type $area
     * @param type $string
     * @param type $proyecto
     * @param type $categoria
     * @param type $year
     * @return array
     */
    public function getComentarioByAreaByStringByProyectoByCategoria($area, $string, $proyecto, $categoria, $year) {
        $sql = "SELECT pia.chr_comentario_id, CP.chr_comentario 
        FROM " . TABLE_INGRESO . " pia
        LEFT JOIN presupuesto_comentario CP ON CP.chr_comentario_id = pia.chr_comentario_id
        WHERE pia.chr_area_id='" . $area . "' AND pia.chr_string_id='" . $string . "' AND chr_proyecto_id ='" . $proyecto . "' AND chr_categoria_id = '" . $categoria . "' AND anio='" . $year . "'
        GROUP BY pia.chr_comentario_id
        ORDER BY CP.chr_comentario";
        $res = $this->db->get_results($sql);
        $this->_codificarObjeto($res,array("chr_comentario"));
        return $res;
    }

    private function monthToString($numMes) {
        $returnValue = $numMes;
        $months = $this->getMes();
        if (isset($months[$numMes])) {
            $returnValue = $months[$numMes];
        }
        return $returnValue;
    }

    private function getMes() {
        $months = [
            '01' => 'Enero',
            '02' => 'Febrero',
            '03' => 'Marzo',
            '04' => 'Abril',
            '05' => 'Mayo',
            '06' => 'Junio',
            '07' => 'Julio',
            '08' => 'Agosto',
            '09' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre'
        ];
        return $months;
    }

    private function getFullnameUserById($userid) {
        $returnValue = '';
        $sql = "SELECT * FROM usuario WHERE idUsuario='" . $userid . "'";
        $res = $this->db->get_results($sql);
        if (is_array($res) && count($res) > 0) {
            $result = array_shift($res);
            if (is_object($result)) {
                $returnValue = $result->nombre_completo;
            }
        }
        return $returnValue;
    }

    /**
     * Método para obtener el ID del usuario en session
     * @return int
     */
    private function getUserid() {
        $returnValue = NULL;
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if (isset($_SESSION['logeado'])) {
            $returnValue = $_SESSION['logeado'];
        }
        return $returnValue;
    }

}

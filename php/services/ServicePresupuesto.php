<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_pdo.php');
require_once(INCDIR.'phpexcel/PHPExcel.php');
require_once("Service.php");

class ServicePresupuesto extends Service
{

	function __construct()
	{
		parent::__construct();
	}



	function getPresupuesto($data){
		$desde = $data->desde;
		$hasta = $data->hasta;
		$codigo = $data->codigo;

		if ($codigo <> ""){
			$adicional = " AND codigo IN '$codigo'";
		}


		$sql = "SELECT * FROM presupuesto WHERE fecha BETWEEN '$desde' AND '$hasta' AND codigo LIKE '%$codigo%'";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("proveedor","concepto","descripcion","clave"));
		return $res;
	}


	function getPresupuestoString(){
		$sql = "SELECT * FROM presupuesto_string";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("codigo","string"));
		return $res;
	}


	function getPresupuestoCategoria(){
		$sql = "SELECT * FROM presupuesto_categoria";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("categoria"));
		return $res; //data
	}

	function getListaCuentaNodo($dato){
		$sql = "SELECT DISTINCT cuenta_nodo FROM presupuesto_ingreso
				WHERE codigo_string = '$dato'
				ORDER BY cuenta_nodo asc";
		$res = $this->db->get_results($sql);
		return $res;
	}



	function getListaNodogEncontradas($dato){

		$sql = "SELECT distinct cuenta FROM pre_glosario
				WHERE cuenta like '%$dato%'
				ORDER BY cuenta ASC";
		$res = $this->db->get_results($sql);
		return $res;
	}





	function getSaldoStringNodo($data){

		$condicion = "";
		$agrupamiento = "";

		if ($data != ""){
			$string = $data->string;
			$scategoria = $data->scategoria;
			$nodo = $data->nodo;
			$area = $data->area;
			$proveedor = $data->proveedor;
			$texto = $data->texto;
			$estado = $data->estado;

			$q1 = $data->q1;
			$q2 = $data->q2;
			$q3 = $data->q3;
			$q4 = $data->q4;

			$condicion = "";
			if ($string != ""){
				$condicion = $condicion." AND codigo_string like '%$string%' ";
			}

			if ($scategoria != ""){
				$nscategoria = $this->getDato("Codigo_Subcategoria","pre_glosario","Nombre_Sub_Categoria = '$scategoria'");
				$condicion = $condicion." AND scategoria like '%$nscategoria%' ";
			}
			if ($nodo != ""){ $condicion = $condicion." AND cuenta_nodo like '%$nodo%' "; }
			if ($area != ""){ $condicion = $condicion." AND subarea like '%$area%' "; }
			if ($proveedor != ""){ $condicion = $condicion." AND proveedor like '%$proveedor%' "; }

			if ($texto != ""){
				$condicion = $condicion." AND (codigo_string like '%$texto%' OR cuenta_nodo like '%$texto%' OR orden like '%$texto%' OR proveedor like '%$texto%' OR a.descripcion like '%$texto%' OR a.clave like '%$texto%')";
			}

			if ($estado != ""){
				$condicion = $condicion." AND estado = '$estado'";
			}

			if ($q1 != 0 OR $q2 != 0 OR $q3 != 0 OR $q4 != 0){
				if($q1 != 0){ $q1 = 1; }
				if($q2 != 0){ $q2 = 2; }
				if($q3 != 0){ $q3 = 3; }
				if($q4 != 0){ $q4 = 4; }

				$condicion = $condicion." AND a.trimestre IN ($q1,$q2,$q3,$q4)";
			}
		}



		$sql_gasto = "SELECT a.*, b.descripcion as string, p.Nombre_Sub_Categoria as nscategoria FROM presupuesto_gastos a LEFT JOIN presupuesto_string b
						ON a.codigo_string = b.codigo LEFT JOIN pre_glosario p
						ON a.scategoria = p.Codigo_Subcategoria
					WHERE a.idPresupuesto > 0 $condicion
					ORDER BY a.idPresupuesto desc "; //AQUI AGREGAR SUB CATEGORIA CUANDO SE DEFINA



		$res_gasto = $this->db->get_results($sql_gasto);
		$this->_codificarObjeto($res_gasto,array("proveedor","string","nscategoria","descripcion","clave","subarea","subtipo","item","estado","motivo"));

		$resultado = new stdClass();
        $resultado->gasto = $res_gasto;

        return $resultado;

	}

	function getListaCuentaNodoPresupuesto(){
		$sql = "SELECT cuenta FROM pre_glosario ORDER BY cuenta ASC";
		$res = $this->db->get_results($sql);
		return $res;
	}

	function deleteRegistroGastoPresupuesto($dato){
		$sql="DELETE FROM presupuesto_gastos WHERE orden = '$dato'";
		$res=$this->db->query($sql);
		return $res;
	}

	function saveGastoPresupuesto($data){

		if($data){
			$procedimiento = $data->procedimiento;

			$usuario = $data->usuario;

			$codigo_string = $data->codigo_string;
			//$codigo_string = $this->getDato("codigo","presupuesto_string","descripcion = '$ncodigo_string'");

			$nsubCategoria = $data->subCategoria;
			$subCategoria = $this->getDato("Codigo_Subcategoria","pre_glosario","Nombre_Sub_Categoria = '".$nsubCategoria."'");

			$direccion = $this->getDato("direccion","pre_glosario","Nombre_Sub_Categoria = '".$nsubCategoria."'");
			$tipo = $this->getDato("tipo","pre_glosario","Nombre_Sub_Categoria = '".$nsubCategoria."'");
			$area = $this->getDato("area","pre_glosario","Nombre_Sub_Categoria = '".$nsubCategoria."'");

			$cuenta_nodo = $data->cuenta_nodo;


			$idPresupuesto = $data->idPresupuesto;
			$norden = $data->norden;
			$ti_string = $data->ti_string;
			$ti_nodo = $data->ti_nodo;
			$monto = $data->monto;
			$moneda = $data->moneda;
			$tipocambio = $data->tipocambio;
			$total = $data->total;
			$fecha = $data->fecha;
			$mes = $data->mes;
			$trimestre = $data->trimestre;
			$proveedor = $data->proveedor;
			$descripcion = $data->descripcion;
			$clave = $data->clave;
			$subarea = $data->subarea;
			$subtipo = $data->subtipo;
			$item = $data->item;
			$estado = $data->estado;
			$motivo = $data->motivo;

			if($procedimiento == "GUARDAR"){

				$sql_nuevoregistro="INSERT INTO presupuesto_gastos (usuario,direccion,tipo,area, codigo_string, scategoria, cuenta_nodo, orden, monto, moneda, tipocambio, total, fecha, mes, trimestre, proveedor, descripcion, clave, subarea, subtipo, item, estado, motivo)
				values ('$usuario','$direccion','$tipo','$area','$codigo_string','$subCategoria','$cuenta_nodo',UPPER('$norden'),'$monto','$moneda','$tipocambio','$total','$fecha','$mes','$trimestre',UPPER('$proveedor'),UPPER('$descripcion'),UPPER('$clave'),UPPER('$subarea'),UPPER('$subtipo'),UPPER('$item'),'$estado',UPPER('$motivo'))";

				$res_nuevoregistro=$this->db->query($sql_nuevoregistro);

			}else{

				$sql_actualizacion="UPDATE presupuesto_gastos SET
					usuario = '$usuario',
					direccion = '$direccion',
					tipo = '$tipo',
					area = '$area',
					codigo_string = '$codigo_string',
					scategoria = '$subCategoria',
					cuenta_nodo = '$cuenta_nodo',
					orden = UPPER('$norden'),
					monto = '$monto',
					moneda = '$moneda',
					tipocambio = '$tipocambio',
					total = '$total',
					fecha = '$fecha',
					mes = '$mes',
					trimestre = '$trimestre',
					proveedor = UPPER('$proveedor'),
					descripcion = UPPER('$descripcion'),
					clave = UPPER('$clave'),
					subarea = UPPER('$subarea'),
					subtipo = UPPER('$subtipo'),
					item = UPPER('$item'),
					estado = '$estado',
					motivo = '$motivo'
					WHERE idPresupuesto = '$idPresupuesto'";
				$res_actualizacion=$this->db->query($sql_actualizacion);

			}


	        if($res_nuevoregistro <> "" OR $res_actualizacion <> ""){

	        	$sql_consulta_total="SELECT * FROM presupuesto_gastos WHERE codigo_string = '$codigo_string' AND scategoria = '$subCategoria' AND cuenta_nodo='$cuenta_nodo'";
				$res_consulta_total = $this->db->get_results($sql_consulta_total);
				$this->_codificarObjeto($res_consulta_total,array("codigo_string","cuenta_nodo","orden","scategoria","proveedor","descripcion","estado","motivo"));
	            return $res_consulta_total;

	        }else{
	            return "ERROR";
	        }
	    }



	}



	function getListaSubTipoEncontradas($dato){
		$sql = "SELECT subTipo FROM presupuesto_subtipo
				WHERE subTipo like '%$dato%'
				ORDER BY subTipo ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("subTipo"));
		return $res;
	}

	function getListaItemEncontradas($dato){
		$sql = "SELECT item FROM presupuesto_item
				WHERE item like '%$dato%'
				ORDER BY item ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("item"));
		return $res;
	}


	function getListaString(){
		$sql = "SELECT descripcion FROM presupuesto_string ORDER BY descripcion ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("descripcion"));
		return $res;
	}




	function getListaStringPresupuesto($dato){

		$sql = "SELECT DISTINCT codigo, UPPER(descripcion) descripcion FROM presupuesto_string
				ORDER BY descripcion ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("descripcion"));
		return $res;

	}

	function getListaSubcategoriasPresupuesto($dato){

		$sql = "SELECT codigo_subcategoria,cuenta,UPPER(nombre_sub_categoria) nombre_sub_categoria FROM pre_glosario
				ORDER BY nombre_sub_categoria ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("codigo_subcategoria","nombre_sub_categoria"));
		return $res;
	}

	function getCuentaNodoPresupuesto($dato){

		$sql = "SELECT DISTINCT UPPER(Cuenta) cuenta FROM pre_glosario
				ORDER BY Cuenta ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("cuenta"));
		return $res;
	}

	function getListaTipoPresupuesto($dato){
		$sql = "SELECT tipo FROM presupuesto_tipo
				ORDER BY tipo ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("tipo"));
		return $res;
	}

	function getListaProveedorPresupuesto($dato){
		$sql = "SELECT DISTINCT proveedor FROM presupuesto_gastos
				ORDER BY proveedor asc";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("proveedor"));
		return $res;
	}









	function getListadeRegistrosdePresupuesto($data){

		$cabeceraIngreso = "";
		$agrupamientoIngreso = "";
		$condicionIngreso = "";
		$cabeceraGasto = "";
		$agrupamientoGasto = "";
		$condicionGasto = "";

		if ($data != ""){

			$variable1 = $data->variable1;
			$variable2 = $data->variable2;
			$variable3 = $data->variable3;
			$variable4 = $data->variable4;

			if ($variable1 != ""){
				$cabeceraIngreso = $cabeceraIngreso.",'-',a.".$variable1;
				$variableIngreso = $variableIngreso.",a.".$variable1;
				$agrupamientoIngreso = $agrupamientoIngreso." ,a.".$variable1;
				$cabeceraGasto = $cabeceraGasto.",'-',".$variable1;
				$variableGasto = $variableGasto.",".$variable1;
				$agrupamientoGasto = $agrupamientoGasto." ,".$variable1;
			}
			if ($variable2 != ""){
				$cabeceraIngreso = $cabeceraIngreso.",'-',a.".$variable2;
				$variableIngreso = $variableIngreso.",a.".$variable2;
				$agrupamientoIngreso = $agrupamientoIngreso." ,a.".$variable2;
				$cabeceraGasto = $cabeceraGasto.",'-',".$variable2;
				$variableGasto = $variableGasto.",".$variable2;
				$agrupamientoGasto = $agrupamientoGasto." ,".$variable2;
			}
			if ($variable3 != ""){
				$cabeceraIngreso = $cabeceraIngreso.",'-',a.".$variable3;
				$variableIngreso = $variableIngreso.",a.".$variable3;
				$agrupamientoIngreso = $agrupamientoIngreso." ,a.".$variable3;
				$cabeceraGasto = $cabeceraGasto.",'-',".$variable3;
				$variableGasto = $variableGasto.",".$variable3;
				$agrupamientoGasto = $agrupamientoGasto." ,".$variable3;
			}
			if ($variable4 != ""){
				$cabeceraIngreso = $cabeceraIngreso.",'-',a.".$variable4;
				$variableIngreso = $variableIngreso.",a.".$variable4;
				$agrupamientoIngreso = $agrupamientoIngreso." ,a.".$variable4;
				$cabeceraGasto = $cabeceraGasto.",'-',".$variable4;
				$variableGasto = $variableGasto.",".$variable4;
				$agrupamientoGasto = $agrupamientoGasto." ,".$variable4;
			}
		}

		$sqlPresupuesto = "SELECT direccion $variableIngreso,CONCAT(a.direccion $cabeceraIngreso) agrupacion,
				sum(a.enero) enero,
				sum(a.febrero) febrero,
				sum(a.marzo) marzo,
				sum(a.abril) abril,
				sum(a.mayo) mayo,
				sum(a.junio) junio,
				sum(a.julio) julio,
				sum(a.agosto) agosto,
				sum(a.septiembre) septiembre,
				sum(a.octubre) octubre,
				sum(a.noviembre) noviembre,
				sum(a.diciembre) diciembre
				from presupuesto_ingreso a left join presupuesto_string b
				ON a.codigo_string = b.codigo
				GROUP BY a.direccion $agrupamientoIngreso";

		$resPresupuesto = $this->db->get_results($sqlPresupuesto);
		$this->_codificarObjeto($resPresupuesto,array("agrupacion"));

		$sqlEjecutado = "SELECT direccion $variableGasto,CONCAT(direccion $cabeceraGasto) agrupacion, sum(total) monto, mes
						FROM presupuesto_gastos
						GROUP BY direccion $agrupamientoGasto ,mes";

		$resEjecutado = $this->db->get_results($sqlEjecutado);
		$this->_codificarObjeto($resEjecutado,array("agrupacion"));

		$dato = new stdClass();
        $dato->presupuestado = $resPresupuesto;
        $dato->ejecutado = $resEjecutado;

		return $dato;



	}



}
?>

<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_pdo.php');
require_once(INCDIR.'phpexcel/PHPExcel.php');
require_once("Service.php");

class ServicePresupuestoInicial extends Service
{

	function __construct()
	{
		parent::__construct();
	}


	function getFiltroReporte($data){
		$sql = "SELECT codigo,filtro FROM filtros WHERE codigo NOT IN ('$data')";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("codigo","filtro"));
		return $res;
	}

	function getAreaReportePresupuesto(){
		$sql = "SELECT chr_area_id, chr_area FROM presupuesto_area";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("chr_area"));
		return $res;
	}

	function getSubVariable($variable,$anio){
		if($variable == "chr_area_id"){ $nvariable = "chr_area"; $nunion = "LEFT JOIN presupuesto_area vp ON vp.chr_area_id = ip.chr_area_id"; }
		if($variable == "chr_string_id"){ $nvariable = "chr_string"; $nunion = "LEFT JOIN presupuesto_string vp ON vp.chr_string_id = ip.chr_string_id"; }
		if($variable == "chr_categoria_id"){ $nvariable = "chr_categoria"; $nunion = "LEFT JOIN presupuesto_categoria vp ON vp.chr_categoria_id = ip.chr_categoria_id"; }
		if($variable == "chr_proyecto_id"){ $nvariable = "chr_proyecto"; $nunion = "LEFT JOIN presupuesto_proyecto vp ON vp.chr_proyecto_id = ip.chr_proyecto_id"; }
		if($variable == "chr_comentario_id"){ $nvariable = "chr_comentario"; $nunion = "LEFT JOIN presupuesto_comentario vp ON vp.chr_comentario_id = ip.chr_comentario_id"; }
		if($variable == "chr_carrera_id"){ $nvariable = "chr_carrera"; $nunion = "LEFT JOIN carrera vp ON vp.chr_carrera_id = ip.chr_carrera_id"; }

		$sql = "SELECT ip.$variable AS codigosubvariable, vp.$nvariable AS listasubvariable 
				FROM presupuesto_ingreso ip
				$nunion
				WHERE ip.anio = '$anio'
				GROUP BY ip.$variable
				ORDER BY vp.$nvariable ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("listasubvariable","$nvariable"));

		return $res;
	}

	function getSubVariable2($variable1,$subvariable1,$variable2,$anio){
		if($variable2 == "chr_area_id"){ $nvariable = "chr_area"; $nunion = "LEFT JOIN presupuesto_area vp ON vp.chr_area_id = ip.chr_area_id"; }
		if($variable2 == "chr_string_id"){ $nvariable = "chr_string"; $nunion = "LEFT JOIN presupuesto_string vp ON vp.chr_string_id = ip.chr_string_id"; }
		if($variable2 == "chr_categoria_id"){ $nvariable = "chr_categoria"; $nunion = "LEFT JOIN presupuesto_categoria vp ON vp.chr_categoria_id = ip.chr_categoria_id"; }
		if($variable2 == "chr_proyecto_id"){ $nvariable = "chr_proyecto"; $nunion = "LEFT JOIN presupuesto_proyecto vp ON vp.chr_proyecto_id = ip.chr_proyecto_id"; }
		if($variable2 == "chr_comentario_id"){ $nvariable = "chr_comentario"; $nunion = "LEFT JOIN presupuesto_comentario vp ON vp.chr_comentario_id = ip.chr_comentario_id"; }
		if($variable2 == "chr_carrera_id"){ $nvariable = "chr_carrera"; $nunion = "LEFT JOIN carrera vp ON vp.chr_carrera_id = ip.chr_carrera_id"; }
		$nvariable1f = "";

		if($subvariable1 != ""){
			$nvariable1f = " AND $variable1 = '$subvariable1'";
		}

		$sql = "SELECT ip.$variable2 AS codigosubvariable, vp.$nvariable AS listasubvariable 
				FROM presupuesto_ingreso ip
				$nunion
				WHERE ip.anio = '$anio' $nvariable1f
				GROUP BY ip.$variable2
				ORDER BY vp.$nvariable ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("listasubvariable","$nvariable"));

		return $res;
	}

	function getSubVariable3($variable1,$subvariable1,$variable2,$subvariable2,$variable3,$anio){
		if($variable3 == "chr_area_id"){ $nvariable = "chr_area"; $nunion = "LEFT JOIN presupuesto_area vp ON vp.chr_area_id = ip.chr_area_id"; }
		if($variable3 == "chr_string_id"){ $nvariable = "chr_string"; $nunion = "LEFT JOIN presupuesto_string vp ON vp.chr_string_id = ip.chr_string_id"; }
		if($variable3 == "chr_categoria_id"){ $nvariable = "chr_categoria"; $nunion = "LEFT JOIN presupuesto_categoria vp ON vp.chr_categoria_id = ip.chr_categoria_id"; }
		if($variable3 == "chr_proyecto_id"){ $nvariable = "chr_proyecto"; $nunion = "LEFT JOIN presupuesto_proyecto vp ON vp.chr_proyecto_id = ip.chr_proyecto_id"; }
		if($variable3 == "chr_comentario_id"){ $nvariable = "chr_comentario"; $nunion = "LEFT JOIN presupuesto_comentario vp ON vp.chr_comentario_id = ip.chr_comentario_id"; }
		if($variable3 == "chr_carrera_id"){ $nvariable = "chr_carrera"; $nunion = "LEFT JOIN carrera vp ON vp.chr_carrera_id = ip.chr_carrera_id"; }
		$nvariable1f = "";
		$nvariable2f = "";

		if($subvariable1 != ""){
			$nvariable1f = " AND $variable1 = '$subvariable1'";
		}
		if($subvariable2 != ""){
			$nvariable2f = " AND $variable2 = '$subvariable2'";
		}

		$sql = "SELECT ip.$variable3 AS codigosubvariable, vp.$nvariable AS listasubvariable 
				FROM presupuesto_ingreso ip
				$nunion
				WHERE ip.anio = '$anio' $nvariable1f $nvariable2f
				GROUP BY ip.$variable3
				ORDER BY vp.$nvariable ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("listasubvariable","$nvariable"));

		return $res;
	}

	function getSubVariable4($variable1,$subvariable1,$variable2,$subvariable2,$variable3,$subvariable3,$variable4,$anio){
		if($variable4 == "chr_area_id"){ $nvariable = "chr_area"; $nunion = "LEFT JOIN presupuesto_area vp ON vp.chr_area_id = ip.chr_area_id"; }
		if($variable4 == "chr_string_id"){ $nvariable = "chr_string"; $nunion = "LEFT JOIN presupuesto_string vp ON vp.chr_string_id = ip.chr_string_id"; }
		if($variable4 == "chr_categoria_id"){ $nvariable = "chr_categoria"; $nunion = "LEFT JOIN presupuesto_categoria vp ON vp.chr_categoria_id = ip.chr_categoria_id"; }
		if($variable4 == "chr_proyecto_id"){ $nvariable = "chr_proyecto"; $nunion = "LEFT JOIN presupuesto_proyecto vp ON vp.chr_proyecto_id = ip.chr_proyecto_id"; }
		if($variable4 == "chr_comentario_id"){ $nvariable = "chr_comentario"; $nunion = "LEFT JOIN presupuesto_comentario vp ON vp.chr_comentario_id = ip.chr_comentario_id"; }
		if($variable4 == "chr_carrera_id"){ $nvariable = "chr_carrera"; $nunion = "LEFT JOIN carrera vp ON vp.chr_carrera_id = ip.chr_carrera_id"; }
		$nvariable1f = "";
		$nvariable2f = "";
		$nvariable3f = "";

		if($subvariable1 != ""){
			$nvariable1f = " AND $variable1 = '$subvariable1'";
		}
		if($subvariable2 != ""){
			$nvariable2f = " AND $variable2 = '$subvariable2'";
		}
		if($subvariable3 != ""){
			$nvariable3f = " AND $variable3 = '$subvariable3'";
		}

		$sql = "SELECT ip.$variable4 AS codigosubvariable, vp.$nvariable AS listasubvariable 
				FROM presupuesto_ingreso ip
				$nunion
				WHERE ip.anio = '$anio' $nvariable1f $nvariable2f $nvariable3f
				GROUP BY ip.$variable4
				ORDER BY vp.$nvariable ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("listasubvariable","$nvariable"));

		return $res;
	}

	function getSubVariable_anterior($data){
		$agrupamiento = "";

		if ($data != ""){
			$variable1 = $data->variable1;
			$variable2 = $data->variable2;
			$variable3 = $data->variable3;
			$variable4 = $data->variable4;
			$anio = $data->anio;
			$filtro = "";

			$variableactivada = $data->variableactivada;

			if ($variable1 != ""){
				$agrupamiento = $agrupamiento.",".$variable1;
			}
			if ($variable2 != ""){
				$agrupamiento = $agrupamiento.",".$variable2;
			}
			if ($variable3 != ""){
				$agrupamiento = $agrupamiento.",".$variable3;
			}
			if ($variable4 != ""){
				$agrupamiento = $agrupamiento.",".$variable4;
			}

			$subvariable1 = $data->subvariable1;
			$subvariable2 = $data->subvariable2;
			$subvariable3 = $data->subvariable3;
			$subvariable4 = $data->subvariable4;

			if ($subvariable1 != ""){
				$filtro = $filtro." AND ".$variable1." = '".$subvariable1."'";
			}
			if ($subvariable2 != ""){
				$filtro = $filtro." AND ".$variable2." = '".$subvariable2."'";
			}
			if ($subvariable3 != ""){
				$filtro = $filtro." AND ".$variable3." = '".$subvariable3."'";
			}
			if ($subvariable4 != ""){
				$filtro = $filtro." AND ".$variable4." = '".$subvariable4."'";
			}


		}

		$sql = "SELECT DISTINCT $variableactivada AS listasubvariable 
				FROM presupuesto_ingreso
				WHERE anio = '$anio' $filtro
				GROUP BY direccion $agrupamiento
				ORDER BY $variableactivada ASC";

		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("listasubvariable"));
		return $res;

	}




	function getListaCuentaCategoria($dato){
		$nombrecat = "";
		$sql = "SELECT UPPER(nombre_categoria) nombre_categoria, UPPER(rubro_p_l) rubro_p_l, UPPER(chr_categoria_id) chr_categoria_id, UPPER(cuenta_padre_nodo) cuenta_padre_nodo FROM pre_glosario_gastos
				WHERE categoria LIKE '%$dato%'";
		$res = $this->db->get_results($sql);

		$nombrecat = $res[0]->nombre_categoria;

		$this->_codificarObjeto($res,array("nombre_categoria", "rubro_p_l", "chr_categoria_id", "cuenta_padre_nodo"));

		$nombre_categoria = $res[0]->nombre_categoria;

		$sql2 = "SELECT UPPER(cuenta_global) cuenta_global FROM pre_lista_data
				WHERE lista_de_gastos = '$nombrecat'";

		$res2 = $this->db->get_results($sql2);
		$this->_codificarObjeto($res2,array("cuenta_global","cuenta")); /*BUSCAR LA CUENTA GLOBAL*/

		$dato = new stdClass();
        $dato->descripcion = $res;
        $dato->cglobal = $res2;

		return $dato;
	}



	function getListaCategoriasEncontradas($dato){

		$sql = "SELECT DISTINCT UPPER(categoria) categoria, UPPER(nombre_categoria) nombre_categoria
				FROM pre_glosario_gastos
				WHERE categoria like '%$dato%' or nombre_categoria like '%$dato%'
				ORDER BY nombre_categoria ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("categoria","nombre_categoria"));
		return $res;

	}


	function getListaStringEncontradas($dato){

		$sql = "SELECT DISTINCT string_sin_PER, UPPER(centro_de_costo) centro_de_costo FROM pre_lista_string
				WHERE string_sin_PER like '%$dato%' or centro_de_costo like '%$dato%'
				ORDER BY centro_de_costo ASC";

		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("string_sin_PER","centro_de_costo"));
		return $res;

	}



	function guardaRegistroPresupuesto(){




	}













	function getListaRegistrosPresupuesto($data){
		if ($data != ""){

			$variable1 = $data->variable1;
			$variable2 = $data->variable2;
			$variable3 = $data->variable3;
			$variable4 = $data->variable4;
			$anio = $data->anio;
			$variables = "";
			$variables_upper = "";
			$variables_trans = "";
			$variables_trans_upper = "";
			$concatenado_trans = "";
			$q1 = "";
			$q2 = "";
			$q3 = "";
			$q4 = "";
			$mq1 = "";
			$mq2 = "";
			$mq3 = "";
			$mq4 = "";
			$filtro = "";
			$trimestre = "";
			$variable1trans = "";
			$variable2trans = "";
			$variable3trans = "";
			$variable4trans = "";

			$concatenado = "";
			$concatenadov = "";

			if ($variable1 != ""){
				$concatenado = $concatenado.",'|',".$variable1;
				//$concatenadov = $concatenadov.",'|',".str_replace('_id', '', $variable1);
				//$variables = $variables.",".$variable1;
				$variables = $variables.",".str_replace('_id', '', $variable1);
				//$variables_upper = $variables_upper.",UPPER(".$variable1.") ".$variable1;

				if($variable1 == "chr_string_id"){
					$variable1trans = "chr_string_id";
				}
				if($variable1 == "string"){
					$variable1trans = "nombre_string";
				}
				if($variable1 != "chr_string_id" && $variable1 != "string"){
					$variable1trans = $variable1;
				}
				/*
				$concatenado_trans = $concatenado_trans.",'|',chr_".$variable1trans;
				$variables_trans = $variables_trans.",chr_".$variable1trans;
				$variables_trans_upper = $variables_trans_upper.",UPPER(chr_".$variable1trans.") chr_".$variable1trans;
				*/
				$concatenado_trans = $concatenado_trans.",'|',".$variable1;
				$variables_trans = $variables_trans.",".$variable1;
				$variables_trans_upper = $variables_trans_upper.",".$variable1;
				//$concatenado_socaspro = $concatenado_socaspro.",'|',A.".$variable1;
				//$variables_socaspro = $variables_socaspro.",A.".$variable1;
			}
			if ($variable2 != ""){
				$concatenado = $concatenado.",'|',".$variable2;
				//$concatenadov = $concatenadov.",'|',".str_replace('_id', '', $variable2);
				//$variables = $variables.",".$variable2;
				$variables = $variables.",".str_replace('_id', '', $variable2);
				//$variables_upper = $variables_upper.",UPPER(".$variable2.") ".$variable2;

				if($variable2 == "chr_string_id"){
					$variable2trans = "string";
				}
				if($variable2 == "string"){
					$variable2trans = "nombre_string";
				}
				if($variable2 != "chr_string_id" && $variable2 != "string"){
					$variable2trans = $variable2;
				}

				$concatenado_trans = $concatenado_trans.",'|',".$variable2;
				$variables_trans = $variables_trans.",".$variable2;
				$variables_trans_upper = $variables_trans_upper.",".$variable2;
				//$concatenado_socaspro = $concatenado_socaspro.",'|',A.".$variable2;
				//$variables_socaspro = $variables_socaspro.",A.".$variable2;
			}
			if ($variable3 != ""){
				$concatenado = $concatenado.",'|',".$variable3;
				//$concatenadov = $concatenadov.",'|',".str_replace('_id', '', $variable3);
				//$variables = $variables.",".$variable3;
				$variables = $variables.",".str_replace('_id', '', $variable3);
				//$variables_upper = $variables_upper.",UPPER(".$variable3.") ".$variable3;



				$concatenado_trans = $concatenado_trans.",'|',".$variable3;
				$variables_trans = $variables_trans.",".$variable3;
				$variables_trans_upper = $variables_trans_upper.",".$variable3;
				//$concatenado_socaspro = $concatenado_socaspro.",'|',A.".$variable3;
				//$variables_socaspro = $variables_socaspro.",A.".$variable3;
			}
			if ($variable4 != ""){
				$concatenado = $concatenado.",'|',".$variable4;
				//$concatenadov = $concatenadov.",'|',".str_replace('_id', '', $variable4);
				//$variables = $variables.",".$variable4;
				$variables = $variables.",".str_replace('_id', '', $variable4);
				//$variables_upper = $variables_upper.",UPPER(".$variable4.") ".$variable4;

				if($variable4 == "chr_string_id"){
					$variable4trans = "string";
				}
				if($variable4 == "string"){
					$variable4trans = "nombre_string";
				}
				if($variable4 != "chr_string_id" && $variable4 != "string"){
					$variable4trans = $variable4;
				}

				$concatenado_trans = $concatenado_trans.",'|',".$variable4;
				$variables_trans = $variables_trans.",".$variable4;
				$variables_trans_upper = $variables_trans_upper.",".$variable4;
				//$concatenado_socaspro = $concatenado_socaspro.",'|',A.".$variable4;
				//$variables_socaspro = $variables_socaspro.",A.".$variable4;
			}

			$subvariable1 = $data->subvariable1;
			$subvariable2 = $data->subvariable2;
			$subvariable3 = $data->subvariable3;
			$subvariable4 = $data->subvariable4;

			if ($subvariable1 != ""){
				$filtro = $filtro." AND ".$variable1." = '".$subvariable1."'";
				$filtro_trans = $filtro_trans." AND ".$variable1." = '".$subvariable1."'";
				//$filtro_socaspro = $filtro_socaspro." AND A.".$variable1." = '".$subvariable1."'";
			}
			if ($subvariable2 != ""){
				$filtro = $filtro." AND ".$variable2." = '".$subvariable2."'";
				$filtro_trans = $filtro_trans." AND ".$variable2." = '".$subvariable2."'";
				//$filtro_socaspro = $filtro_socaspro." AND A.".$variable2." = '".$subvariable2."'";
			}
			if ($subvariable3 != ""){
				$filtro = $filtro." AND ".$variable3." = '".$subvariable3."'";
				$filtro_trans = $filtro_trans." AND ".$variable3." = '".$subvariable3."'";
				//$filtro_socaspro = $filtro_socaspro." AND A.".$variable3." = '".$subvariable3."'";
			}
			if ($subvariable4 != ""){
				$filtro = $filtro." AND ".$variable4." = '".$subvariable4."'";
				$filtro_trans = $filtro_trans." AND ".$variable4." = '".$subvariable4."'";
				//$filtro_socaspro = $filtro_socaspro." AND A.".$variable4." = '".$subvariable4."'";
			}

			$texto = $data->texto;

			if ($texto != ""){
				//$filtroD = $filtroD." AND (PG.documento LIKE '%".$texto."%' OR PR.nombre_razon_social LIKE '%".$texto."%' OR PO.nombre LIKE '%".$texto."%' OR PO.dni LIKE '%".$texto."%')";
				$filtro_detalle = $filtro_detalle." AND (PG.nformato LIKE '%".$texto."%' OR PG.idFormato LIKE '%".$texto."%' OR PG.documento LIKE '%".$texto."%' OR PR.nombre_razon_social LIKE '%".$texto."%' OR PO.nombre LIKE '%".$texto."%' OR PO.dni LIKE '%".$texto."%')";
			}

			if($data->q1 > 0){ 
				$q1 = 1; 
			};
			if($data->q2 > 0){ 
				$q2 = 2; 
			};
			if($data->q3 > 0){ 
				$q3 = 3; 
			};
			if($data->q4 > 0){ 
				$q4 = 4; 
			};

			if($data->mq1 > 0 || $data->q1 > 0){ 
				$mq1 = 1;
				$mes = $mes.", SUM( IF(mes = '01', total,0) ) '01', SUM( IF(mes = '02', total,0) ) '02', SUM( IF(mes = '03', total,0) ) '03', SUM( IF(mes IN ('01','02','03'), total,0) ) Q1";
				$mes_gasto = $mes_gasto.", SUM( IF(mes = '01', (total - devoluciones),0) ) '01', SUM( IF(mes = '02', (total - devoluciones),0) ) '02', SUM( IF(mes = '03', (total - devoluciones),0) ) '03', SUM( IF(mes IN ('01','02','03'), (total - devoluciones),0) ) Q1";
				$mes_trans = $mes_trans.", SUM( IF(chr_mes = '01', dec_total,0) ) '01', SUM( IF(chr_mes = '02', dec_total,0) ) '02', SUM( IF(chr_mes = '03', dec_total,0) ) '03', SUM( IF(chr_mes IN ('01','02','03'), dec_total,0) ) Q1";
				$mes_trans_resta = $mes_trans_resta.", SUM( IF(chr_mes = '01', dec_subtotal,0) ) '01', SUM( IF(chr_mes = '02', dec_subtotal,0) ) '02', SUM( IF(chr_mes = '03', dec_subtotal,0) ) '03', SUM( IF(chr_mes IN ('01','02','03'), dec_subtotal,0) ) Q1";
				//$mes_socaspro = $mes_socaspro.", SUM( IF(A.mes = '01', A.total,0) ) '01', SUM( IF(A.mes = '02', A.total,0) ) '02', SUM( IF(A.mes = '03', A.total,0) ) '03', SUM( IF(A.mes IN ('01','02','03'), A.total,0) ) Q1";
			};
			if($data->mq2 > 0 || $data->q2 > 0){ 
				$mq2 = 2;
				$mes = $mes.", SUM( IF(mes = '04', total,0) ) '04', SUM( IF(mes = '05', total,0) ) '05', SUM( IF(mes = '06', total,0) ) '06', SUM( IF(mes IN ('04','05','06'), total,0) ) Q2";
				$mes_gasto = $mes_gasto.", SUM( IF(mes = '04', (total - devoluciones),0) ) '04', SUM( IF(mes = '05', (total - devoluciones),0) ) '05', SUM( IF(mes = '06', (total - devoluciones),0) ) '06', SUM( IF(mes IN ('04','05','06'), (total - devoluciones),0) ) Q2";
				$mes_trans = $mes_trans.", SUM( IF(chr_mes = '04', dec_total,0) ) '04', SUM( IF(chr_mes = '05', dec_total,0) ) '05', SUM( IF(chr_mes = '06', dec_total,0) ) '06', SUM( IF(chr_mes IN ('04','05','06'), dec_total,0) ) Q2";
				$mes_trans_resta = $mes_trans_resta.", SUM( IF(chr_mes = '04', dec_subtotal,0) ) '04', SUM( IF(chr_mes = '05', dec_subtotal,0) ) '05', SUM( IF(chr_mes = '06', dec_subtotal,0) ) '06', SUM( IF(chr_mes IN ('04','05','06'), dec_subtotal,0) ) Q2";
				//$mes_socaspro = $mes_socaspro.", SUM( IF(A.mes = '04', A.total,0) ) '04', SUM( IF(A.mes = '05', A.total,0) ) '05', SUM( IF(A.mes = '06', A.total,0) ) '06', SUM( IF(A.mes IN ('04','05','06'), A.total,0) ) Q2";
			};
			if($data->mq3 > 0 || $data->q3 > 0){ 
				$mq3 = 3;
				$mes = $mes.", SUM( IF(mes = '07', total,0) ) '07', SUM( IF(mes = '08', total,0) ) '08', SUM( IF(mes = '09', total,0) ) '09', SUM( IF(mes IN ('07','08','09'), total,0) ) Q3";
				$mes_gasto = $mes_gasto.", SUM( IF(mes = '07', (total - devoluciones),0) ) '07', SUM( IF(mes = '08', (total - devoluciones),0) ) '08', SUM( IF(mes = '09', (total - devoluciones),0) ) '09', SUM( IF(mes IN ('07','08','09'), (total - devoluciones),0) ) Q3";
				$mes_trans = $mes_trans.", SUM( IF(chr_mes = '07', dec_total,0) ) '07', SUM( IF(chr_mes = '08', dec_total,0) ) '08', SUM( IF(chr_mes = '09', dec_total,0) ) '09', SUM( IF(chr_mes IN ('07','08','09'), dec_total,0) ) Q3";
				$mes_trans_resta = $mes_trans_resta.", SUM( IF(chr_mes = '07', dec_subtotal,0) ) '07', SUM( IF(chr_mes = '08', dec_subtotal,0) ) '08', SUM( IF(chr_mes = '09', dec_subtotal,0) ) '09', SUM( IF(chr_mes IN ('07','08','09'), dec_subtotal,0) ) Q3";
				//$mes_socaspro = $mes_socaspro.", SUM( IF(A.mes = '07', A.total,0) ) '07', SUM( IF(A.mes = '08', A.total,0) ) '08', SUM( IF(A.mes = '09', A.total,0) ) '09', SUM( IF(A.mes IN ('07','08','09'), A.total,0) ) Q3";
			};
			if($data->mq4 > 0 || $data->q4 > 0){ 
				$mq4 = 4;
				$mes = $mes.", SUM( IF(mes = '10', total,0) ) '10', SUM( IF(mes = '11', total,0) ) '11', SUM( IF(mes = '12', total,0) ) '12', SUM( IF(mes IN ('10','11','12'), total,0) ) Q4";
				$mes_gasto = $mes_gasto.", SUM( IF(mes = '10', (total - devoluciones),0) ) '10', SUM( IF(mes = '11', (total - devoluciones),0) ) '11', SUM( IF(mes = '12', (total - devoluciones),0) ) '12', SUM( IF(mes IN ('10','11','12'), (total - devoluciones),0) ) Q4";
				$mes_trans = $mes_trans.", SUM( IF(chr_mes = '10', dec_total,0) ) '10', SUM( IF(chr_mes = '11', dec_total,0) ) '11', SUM( IF(chr_mes = '12', dec_total,0) ) '12', SUM( IF(chr_mes IN ('10','11','12'), dec_total,0) ) Q4";
				$mes_trans_resta = $mes_trans_resta.", SUM( IF(chr_mes = '10', dec_subtotal,0) ) '10', SUM( IF(chr_mes = '11', dec_subtotal,0) ) '11', SUM( IF(chr_mes = '12', dec_subtotal,0) ) '12', SUM( IF(chr_mes IN ('10','11','12'), dec_subtotal,0) ) Q4";
				//$mes_socaspro = $mes_socaspro.", SUM( IF(A.mes = '10', A.total,0) ) '10', SUM( IF(A.mes = '11', A.total,0) ) '11', SUM( IF(A.mes = '12', A.total,0) ) '12', SUM( IF(A.mes IN ('10','11','12'), A.total,0) ) Q4";
			};

			if($q1 > 0 || $q2 > 0 || $q3 > 0 || $q4 > 0 || $mq1 > 0 || $mq2 > 0 || $mq3 > 0 || $mq4 > 0){
				$trimestre = " AND trimestre IN ('$q1','$q2','$q3','$q4','$mq1','$mq2','$mq3','$mq4')";
				//$trimestre_socaspro = " AND A.trimestre IN ('$q1','$q2','$q3','$q4','$mq1','$mq2','$mq3','$mq4')";
			}

		}



		$sql_presupuesto_reporte = "
						SELECT *
						FROM
						(
						SELECT UPPER(CONCAT(direccion $concatenado)) concatenado, direccion $variables FROM presupuesto_ingreso_act_var
						WHERE idPresupuestoIngreso > 0 $filtro $trimestre AND anio = $anio
						GROUP BY direccion $variables

						UNION DISTINCT

						SELECT UPPER(CONCAT(direccion $concatenado)) concatenado, direccion $variables FROM presupuesto_gasto_var
						WHERE idPresupuesto > 0 AND anio = $anio AND estado NOT IN ('INGRESADO','CANCELADO','RECHAZADO') $filtro $trimestre
						GROUP BY direccion $variables
						) AS reporte
						ORDER BY direccion $variables ASC";

		$res_presupuesto_reporte = $this->db->get_results($sql_presupuesto_reporte);
		$this->_codificarObjeto($res_presupuesto_reporte,array("concatenado","direccion","chr_area","chr_categoria","chr_proyecto","chr_comentario","tipo","chr_string_id","chr_categoria","chr_string"));

		
		$sql_presupuesto_reporte_buscador = "
						SELECT UPPER(CONCAT(direccion $concatenado)) concatenado,direccion $variables
						FROM presupuesto_gasto_var PG LEFT JOIN presupuesto_oep PO
						ON PG.documento = PO.idOepSolicitud LEFT JOIN proveedor PR
						ON PG.proveedor = PR.dni_ruc
						WHERE idPresupuesto > 0 AND PG.anio = $anio AND PG.estado NOT IN ('INGRESADO','CANCELADO','RECHAZADO') $filtro $trimestre
						$filtro_detalle
						GROUP BY direccion $variables";

		$res_presupuesto_reporte_buscador = $this->db->get_results($sql_presupuesto_reporte_buscador);
		$this->_codificarObjeto($res_presupuesto_reporte_buscador,array("concatenado","direccion","chr_area","chr_categoria","chr_proyecto","chr_comentario","tipo","chr_string_id","chr_categoria_id","chr_string"));
		

		$sql_presupuesto_total = "	SELECT UPPER(CONCAT(direccion $concatenado)) concatenado,direccion  ,SUM(total) total_registro 
									FROM presupuesto_ingreso_act_var
									WHERE idPresupuestoIngreso > 0 AND anio = $anio $filtro
									GROUP BY direccion $variables";
									
		$res_presupuesto_total = $this->db->get_results($sql_presupuesto_total);
		$this->_codificarObjeto($res_presupuesto_total,array("concatenado"));


		$sql_presupuesto_ingreso = "SELECT UPPER(CONCAT(direccion $concatenado)) concatenado,direccion $variables $mes  ,SUM(total) total_registro 
									FROM presupuesto_ingreso_act_var
									WHERE idPresupuestoIngreso > 0 AND anio = $anio $filtro
									GROUP BY direccion $variables";
									
		$res_presupuesto_ingreso = $this->db->get_results($sql_presupuesto_ingreso);
		$this->_codificarObjeto($res_presupuesto_ingreso,array("concatenado","direccion","chr_area","chr_categoria","chr_proyecto","chr_comentario","tipo","chr_string_id","chr_categoria_id","chr_string"));


		/* CUANDO SE TERMINE MOVIMIENTOS Y TRANSFERENCIAS */
		$sql_presupuesto_ingreso_suma = "SELECT UPPER(CONCAT('DACA' $concatenado_trans)) concatenado,'DACA' direccion $variables_trans_upper $mes_trans  ,SUM(dec_total) total_registro 
									FROM mov_movimiento
									WHERE id > 0 AND chr_year = '$anio' AND int_statusid = 8
									$filtro_trans
									GROUP BY direccion $variables_trans";
									
		$res_presupuesto_ingreso_suma = $this->db->get_results($sql_presupuesto_ingreso_suma);

		$this->_codificarObjeto($res_presupuesto_ingreso_suma,array("concatenado","chk_area","chk_categoria","chk_proyecto","chk_comentario","chk_tipo","chr_string","chr_chr_categoria_id","chr_nombre_string"));

		$sql_presupuesto_ingreso_resta = "SELECT UPPER(CONCAT('DACA' $concatenado_trans)) concatenado,'DACA' direccion $variables_trans_upper $mes_trans_resta  ,SUM(dec_subtotal) total_registro 
									FROM mov_movimiento_detalle
									WHERE id > 0 AND chr_year = '$anio' AND int_movimiento_id IN (select X.id from mov_movimiento X WHERE X.int_statusid = 8)
									$filtro_trans
									GROUP BY direccion $variables_trans";

		$res_presupuesto_ingreso_resta = $this->db->get_results($sql_presupuesto_ingreso_resta);
		$this->_codificarObjeto($res_presupuesto_ingreso_resta,array("concatenado","chk_area","chk_categoria","chk_proyecto","chk_comentario","chk_tipo","chr_string","chr_chr_categoria_id","chr_nombre_string"));


		$sql_presupuesto_egreso = "	SELECT UPPER(CONCAT(direccion $concatenado)) concatenado,direccion $variables $mes_gasto  ,(SUM(total) - SUM(devoluciones)) total_registro
									FROM presupuesto_gasto_var
									WHERE idPresupuesto > 0 AND anio = $anio $filtro AND estado IN ('APROBADO')
									GROUP BY direccion $variables";

		$res_presupuesto_egreso = $this->db->get_results($sql_presupuesto_egreso);
		$this->_codificarObjeto($res_presupuesto_egreso,array("concatenado","direccion","chr_area","chr_categoria","chr_proyecto","chr_comentario","tipo","chr_string_id","chr_categoria_id","chr_string"));

		/*
		$sql_presupuesto_socaspro = "	SELECT A.idFormato, A.nformato, UPPER(CONCAT(A.direccion $concatenado_socaspro)) concatenado,A.direccion $variables_socaspro $mes_socaspro  ,SUM(B.total) total_registro
										FROM presupuesto_gasto A LEFT JOIN presupuesto_gasto B
										ON CONCAT(SUBSTR(A.idFormato,2,3),'-',A.nformato) = B.nformato
										WHERE A.idPresupuesto > 0 AND A.idFormato IN ('SOCA','SPRO') $filtro_socaspro AND A.estado IN ('APROBADO')
										GROUP BY A.nformato, A.direccion $variables_socaspro";

		$res_presupuesto_socaspro = $this->db->get_results($sql_presupuesto_socaspro);
		$this->_codificarObjeto($res_presupuesto_socaspro,array("concatenado","area","categoria","proyecto","comentario","tipo"));
		*/

		$sql_presupuesto_egreso_pendiente = "SELECT UPPER(CONCAT(direccion $concatenado)) concatenado,direccion $variables $mes_gasto  ,(SUM(total) - SUM(devoluciones)) total_registro
									FROM presupuesto_gasto_var
									WHERE idPresupuesto > 0 AND anio = $anio $filtro  AND estado IN ('POR APROBAR')
									GROUP BY direccion $variables";

		$res_presupuesto_egreso_pendiente = $this->db->get_results($sql_presupuesto_egreso_pendiente);
		$this->_codificarObjeto($res_presupuesto_egreso_pendiente,array("concatenado","direccion","chr_area","chr_categoria","chr_proyecto","chr_comentario","tipo","chr_string_id","chr_categoria_id","chr_string"));

		$sql_presupuesto_detalle_egreso = "SELECT UPPER(CONCAT(direccion $concatenado)) concatenado, idPresupuesto, documento, PO.dni, PO.nombre, PO.monto monto_oep, nombre_razon_social, proveedor, direccion $variables $mes_gasto  ,(SUM(total) - SUM(devoluciones)) total_registro, PG.estado, PG.idFormato
									FROM presupuesto_gasto_var PG LEFT JOIN presupuesto_oep PO
									ON PG.documento = PO.idOepSolicitud LEFT JOIN proveedor PR
									ON PG.proveedor = PR.dni_ruc
									WHERE idPresupuesto > 0 AND PG.anio = $anio $filtro_detalle $trimestre AND PG.estado IN ('APROBADO','POR APROBAR','LIQUIDADO') /*,'RECHAZADO','CANCELADO'*/
									GROUP BY idPresupuesto, direccion, id_Oep";

		$res_presupuesto_detalle_egreso = $this->db->get_results($sql_presupuesto_detalle_egreso);
		$this->_codificarObjeto($res_presupuesto_detalle_egreso,array("concatenado","direccion","chr_area","chr_categoria","chr_proyecto","chr_comentario","tipo","nombre","nombre_razon_social","chr_string_id","chr_categoria_id","chr_string"));


		$resultado = new stdClass();
        $resultado->reporte = $res_presupuesto_reporte;
        $resultado->total = $res_presupuesto_total;
        $resultado->reporte_buscador = $res_presupuesto_reporte_buscador;
        //$resultado->egreso_socaspro = $res_presupuesto_socaspro;
        $resultado->ingreso = $res_presupuesto_ingreso;
        $resultado->ingreso_suma = $res_presupuesto_ingreso_suma;
        $resultado->ingreso_resta = $res_presupuesto_ingreso_resta;
        $resultado->egreso = $res_presupuesto_egreso;
        $resultado->egreso_pendiente = $res_presupuesto_egreso_pendiente;
        $resultado->detalle_egreso = $res_presupuesto_detalle_egreso;

		return $resultado;

	}






	function getDetallePresupuesto($data){
		$variable = $data->variable;
		$variables = $data->variables;
		$filtros = $data->filtros;
		$nvariables = "";
		$nfiltros = "";
		$anio = $data->anio;

        $registros = count($variables); //[0]->idPerfil;

        for($i=0 ; $i < $registros ; $i++){
        	$nvariables = $nvariables.",".$variables[$i];
        	$nfiltros = $nfiltros." AND ".$variables[$i]." ='".$filtros[$i]."'";
        }


		$sqlEjecutado = "SELECT * FROM presupuesto_gasto_var
						 WHERE anio = '$anio' $nfiltros";
						 
		$resEjecutado = $this->db->get_results($sqlEjecutado);
		//$this->_codificarObjeto($resPresupuestado,array("agrupacion"));

		return $resEjecutado;

	}






	function getDetallePresupuestoTransferencia($data){
		if ($data != ""){
			$variable1 = $data->variable1;
			$variable2 = $data->variable2;
			$variable3 = $data->variable3;
			$variable4 = $data->variable4;
			$anio = $data->anio;

			$valorRecibido = $data->concatenado;
			$valorSeleccionado = explode('|', $valorRecibido);


			if ($variable1 != ""){
				$concatenado = $concatenado.",'|',".$variable1;
				$concatenado_titulo = $concatenado_titulo.",' - ',".str_replace('_id', '', $variable1);
				//$variables = $variables.",".$variable1;
				$variables = $variables.",".str_replace('_id', '', $variable1);
				$variables_upper = $variables_upper.",".$variable1;
				$concatenado_trans = $concatenado_trans.",'|',".$variable1;
				$concatenado_trans_var = $concatenado_trans_var.",' - ',".str_replace('_id', '', $variable1);
				$variables_trans = $variables_trans.",".$variable1;
				$variables_trans_upper = $variables_trans_upper.",".$variable1;

				$filtro = $filtro." AND ".$variable1." = '".$valorSeleccionado[1]."'";
				$filtro_trans = $filtro_trans." AND ".$variable1." = '".$valorSeleccionado[1]."'";
			}
			if ($variable2 != ""){
				$concatenado = $concatenado.",'|',".$variable2;
				$concatenado_titulo = $concatenado_titulo.",' - ',".str_replace('_id', '', $variable2);
				$variables = $variables.",".str_replace('_id', '', $variable2);
				$variables_upper = $variables_upper.",".$variable2;
				$concatenado_trans = $concatenado_trans.",'|',".$variable2;
				$concatenado_trans_var = $concatenado_trans_var.",' - ',".str_replace('_id', '', $variable2);
				$variables_trans = $variables_trans.",".$variable2;
				$variables_trans_upper = $variables_trans_upper.",".$variable2;

				$filtro = $filtro." AND ".$variable2." = '".$valorSeleccionado[2]."'";
				$filtro_trans = $filtro_trans." AND ".$variable2." = '".$valorSeleccionado[2]."'";
			}
			if ($variable3 != ""){
				$concatenado = $concatenado.",'|',".$variable3;
				$concatenado_titulo = $concatenado_titulo.",' - ',".str_replace('_id', '', $variable3);
				$variables = $variables.",".str_replace('_id', '', $variable3);
				$variables_upper = $variables_upper.",".$variable3;
				$concatenado_trans = $concatenado_trans.",'|',".$variable3;
				$concatenado_trans_var = $concatenado_trans_var.",' - ',".str_replace('_id', '', $variable3);
				$variables_trans = $variables_trans.",".$variable3;
				$variables_trans_upper = $variables_trans_upper.",".$variable3;

				$filtro = $filtro." AND ".$variable3." = '".$valorSeleccionado[3]."'";
				$filtro_trans = $filtro_trans." AND ".$variable3." = '".$valorSeleccionado[3]."'";
			}
			if ($variable4 != ""){
				$concatenado = $concatenado.",'|',".$variable4;
				$concatenado_titulo = $concatenado_titulo.",' - ',".str_replace('_id', '', $variable4);
				$variables = $variables.",".str_replace('_id', '', $variable4);
				$variables_upper = $variables_upper.",".$variable4;
				$concatenado_trans = $concatenado_trans.",'|',".$variable4;
				$concatenado_trans_var = $concatenado_trans_var.",' - ',".str_replace('_id', '', $variable4);
				$variables_trans = $variables_trans.",".$variable4;
				$variables_trans_upper = $variables_trans_upper.",".$variable4;

				$filtro = $filtro." AND ".$variable4." = '".$valorSeleccionado[4]."'";
				$filtro_trans = $filtro_trans." AND ".$variable4." = '".$valorSeleccionado[4]."'";
			}

			$mes = $mes.", SUM( IF(mes = '".end($valorSeleccionado)."', total,0) ) 'total_registro'";
			$mes_gasto = $mes_gasto.", SUM( IF(mes = '".end($valorSeleccionado)."', (total - devoluciones),0) ) 'total_registro'";
			$mes_trans = $mes_trans.", SUM( IF(chr_mes = '".end($valorSeleccionado)."', dec_total,0) ) 'total_registro'";
			$mes_trans_resta = $mes_trans_resta.", SUM( IF(chr_mes = '".end($valorSeleccionado)."', dec_subtotal,0) ) 'total_registro'";

		}

		$sqlPresupuestoInicial = "SELECT UPPER(CONCAT(direccion $concatenado)) concatenado,UPPER(CONCAT('DACA' $concatenado_titulo)) concatenado_titulo,direccion $variables $mes
									FROM presupuesto_ingreso_act_var
									WHERE idPresupuestoIngreso > 0 AND anio = $anio $filtro 
									GROUP BY direccion $variables"; 

		$resPresupuestoInicial = $this->db->get_results($sqlPresupuestoInicial);
		$this->_codificarObjeto($resPresupuestoInicial,array("concatenado","concatenado_titulo","chr_area","chr_categoria","chr_proyecto","chr_comentario","tipo"));

		$sqlPresupuestoInicialSuma = "SELECT UPPER(CONCAT('DACA' $concatenado_trans)) concatenado,UPPER(CONCAT('DACA' $concatenado_trans_var)) concatenado_titulo,'DACA' direccion $variables , chr_mes, dec_subtotal total_registro
									FROM mov_movimiento_detalle_var
									WHERE id > 0 AND chr_year = '$anio' AND int_movimiento_id IN ( SELECT id FROM mov_movimiento_var WHERE id > 0 $filtro_trans AND int_statusid = 8 AND chr_mes = '".end($valorSeleccionado)."' GROUP BY id )
									GROUP BY direccion $variables_trans ,chr_mes";
									
		$resPresupuestoInicialSuma = $this->db->get_results($sqlPresupuestoInicialSuma);
		$this->_codificarObjeto($resPresupuestoInicialSuma,array("concatenado","concatenado_titulo","chr_area","chr_categoria","chr_proyecto","chr_comentario"));
		/*
		$sqlPresupuestoInicialResta = "SELECT UPPER(CONCAT('DACA' $concatenado_trans)) concatenado,'DACA' direccion $variables_trans ,chr_mes, dec_subtotal total_registro 
									FROM mov_movimiento_detalle
									WHERE id > 0 $filtro_trans AND chr_year = '$anio' AND chr_mes = '".end($valorSeleccionado)."' AND int_movimiento_id IN (select id from mov_movimiento WHERE int_statusid = 8)
									GROUP BY direccion $variables_trans ,chr_mes, int_movimiento_id";
		*/
		$sqlPresupuestoInicialResta = "	SELECT UPPER(CONCAT('DACA' $concatenado_trans)) concatenado,UPPER(CONCAT('DACA' $concatenado_trans_var)) concatenado_titulo,'DACA' direccion $variables ,chr_mes, dec_subtotal total_registro ,
										(SELECT CONCAT('DACA'".$concatenado_trans_var.") FROM mov_movimiento_var WHERE id = mov_movimiento_detalle_var.int_movimiento_id) concatenado_egreso,
										(SELECT chr_mes FROM mov_movimiento WHERE id = mov_movimiento_detalle_var.int_movimiento_id) chr_mes_egreso
										FROM mov_movimiento_detalle_var
										WHERE id > 0 $filtro_trans AND chr_year = '$anio' AND chr_mes = '".end($valorSeleccionado)."' AND int_movimiento_id IN (select id from mov_movimiento_var WHERE int_statusid = 8)
										GROUP BY direccion $variables_trans ,chr_mes, int_movimiento_id";

		$resPresupuestoInicialResta = $this->db->get_results($sqlPresupuestoInicialResta);
		$this->_codificarObjeto($resPresupuestoInicialResta,array("concatenado","concatenado_titulo","chr_area","chr_categoria","chr_proyecto","chr_comentario","concatenado_egreso"));




		$resultado = new stdClass();
        $resultado->presupuesto_inicial = $resPresupuestoInicial;
        $resultado->presupuesto_inicial_suma = $resPresupuestoInicialSuma;
        $resultado->presupuesto_inicial_resta = $resPresupuestoInicialResta;


		return $resultado;

	}

	function getReporteAreaPresupuesto($area,$ampliacion,$anio){

		$sql = "SELECT chr_area_id,chr_area, ROUND(SUM(total),2) AS total
				FROM presupuesto_ingreso_act_var
				WHERE anio = $anio AND chr_area_id = $area $condicion
				ORDER BY chr_proyecto,chr_categoria,chr_comentario ASC";
		$resTotal = $this->db->get_results($sql);
		$this->_codificarObjeto($resTotal,array("chr_area"));

			$registros = Array();
			foreach ($resTotal as $registrot) {
				$fila = new stdClass();
		        $fila->carrera = "";
		        $fila->proyecto = "";
		        $fila->categoria = "";
		        $fila->comentario = "";
		        $fila->descripcion = "";
		        $fila->fecha = "";
		        $fila->proveedor = "";
		        $fila->presupuesto = $registrot->total;
		        $fila->gasto = $this->getGastoAreaTotalPresupuesto($registrot->chr_area_id,$anio,$ampliacion);
		        $fila->saldo = $registrot->total - $this->getGastoAreaTotalPresupuesto($registrot->chr_area_id,$anio,$ampliacion);

		        $registros[] = $fila;		        

			}

		$sql = "SELECT chr_area_id,chr_area,chr_proyecto_id,chr_proyecto,chr_categoria_id,chr_categoria,chr_comentario,chr_comentario_id, ROUND(SUM(total),2) AS total
				FROM presupuesto_ingreso_act_var
				WHERE anio = $anio AND chr_area_id = $area
				GROUP BY chr_proyecto_id,chr_categoria_id,chr_comentario_id
				ORDER BY chr_proyecto,chr_categoria,chr_comentario ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("chr_area","chr_proyecto","chr_categoria","chr_comentario"));

		//$registros = Array();
		foreach ($res as $registro) {
			$fila = new stdClass();
	        $fila->carrera = "";
	        $fila->proyecto = $registro->chr_proyecto;
	        $fila->categoria = $registro->chr_categoria;
	        $fila->comentario = $registro->chr_comentario;
	        $fila->descripcion = "";
	        $fila->fecha = "";
	        $fila->proveedor = "";
	        $fila->presupuesto = $registro->total;
	        $fila->gasto = $this->getGastoAreaPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion);
	        $fila->saldo = $registro->total - $this->getGastoAreaPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion);

	        $registros[] = $fila;

	        //var_dump($registro->chr_area_id," - ",$registro->chr_proyecto_id," - ",$registro->chr_categoria_id," - ",$registro->chr_comentario_id," - ",$anio);

	        $gastos = $this->getGastoDetalladoAreaPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion);


			foreach ($gastos as $registrof) {
		       	//AQUI UN FOREACH DE OEP DE ACUERDO AL MONTO TOTAL
		       	if($registrof->chr_categoria_id == "OEP"){

		       		$colaboradores = $this->getOepDetalladoAreaPresupuesto($registrof->documento);

					foreach ($colaboradores as $registroc) {
						$nombre = "";
						$fila = new stdClass();
				        $fila->carrera = $this->getDescripcionCarreraPresupuesto($registrof->chr_carrera_id);
				        $fila->proyecto = $registrof->chr_proyecto;
				        $fila->categoria = "";
				        $fila->comentario = "";
				        $fila->descripcion = $registrof->descripcion;
				        $fila->fecha = $registrof->fechaGasto;

				        if($registroc->nombreColaborador == NULL){
				        	$nombre = $registroc->nombreOep;
				        }else{
				        	$nombre = $registroc->nombreColaborador;
				        }

				        $fila->proveedor = $nombre;
				        $fila->presupuesto = 0;
				        $fila->gasto = $registroc->monto;
				        $fila->saldo = 0;

				        $registros[] = $fila;
				    }

		       	}else{

					$fila = new stdClass();
			        $fila->carrera = $this->getDescripcionCarreraPresupuesto($registrof->chr_carrera_id);
			        $fila->proyecto = $registrof->chr_proyecto;
			        $fila->categoria = "";
			        $fila->comentario = "";
			        $fila->descripcion = $registrof->descripcion;
			        $fila->fecha = $registrof->fechaGasto;
			        $fila->proveedor = $this->getDescripcionProveedorPresupuesto($registrof->proveedor);
			        $fila->presupuesto = 0;
			        $fila->gasto = $registrof->total;
			        $fila->saldo = 0;

			        $registros[] = $fila;

		       	}


			} 

		}

        return $registros;

	}

	function getDescripcionCarreraPresupuesto($carrera){
		$sql = "SELECT chr_carrera FROM carrera WHERE chr_carrera_id = '$carrera'";
		$res = $this->db->get_var($sql);
		$this->_codificarObjeto($res,array("chr_carrera"));
		return $res;
	}

	function getDescripcionProveedorPresupuesto($ruc){
		$sql = "SELECT nombre_razon_social FROM proveedor WHERE dni_ruc = '$ruc'";
		$res = $this->db->get_var($sql);
		$this->_codificarObjeto($res,array("nombre_razon_social"));
		return $res;
	}

	function getGastoAreaTotalPresupuesto($chr_area_id,$anio,$ampliacion){
		$condicion = "";
		if($ampliacion == "1"){
			$condicion = " AND ampliacion = '1'";
		}

		$sql = "SELECT ROUND(SUM(total),2) AS total
				FROM presupuesto_gasto_var
				WHERE anio = '$anio' AND chr_area_id = '$chr_area_id' AND estado IN ('APROBADO','POR APROBAR') $condicion
				ORDER BY chr_area_id ASC";
		$res = $this->db->get_var($sql);
		$this->_codificarObjeto($res,array("chr_area"));
		return $res;
	}

	function getGastoAreaTotalPresupuestoSaldos($chr_area_id,$anio,$ampliacion,$trimestre){
		$condicion = "";
		if($ampliacion == "1"){
			$condicion = " AND ampliacion = '1'";
		}

		$sql = "SELECT ROUND(SUM(total),2) AS total
				FROM presupuesto_gasto_var
				WHERE anio = '$anio' AND chr_area_id = '$chr_area_id' AND estado IN ('APROBADO','POR APROBAR') $condicion
				AND trimestre = $trimestre
				ORDER BY chr_area_id ASC";
		$res = $this->db->get_var($sql);
		$this->_codificarObjeto($res,array("chr_area"));
		return $res;
	}

	function getGastoAreaPresupuesto($chr_area_id,$chr_proyecto_id,$chr_categoria_id,$chr_comentario_id,$anio,$ampliacion){
		$condicion = "";
		if($ampliacion == "1"){
			$condicion = " AND ampliacion = '1'";
		}

		$sql = "SELECT ROUND(SUM(total),2) AS total
				FROM presupuesto_gasto_var
				WHERE anio = '$anio' AND chr_area_id = '$chr_area_id' AND chr_proyecto_id = '$chr_proyecto_id' AND chr_categoria_id = '$chr_categoria_id' AND chr_comentario_id = '$chr_comentario_id' AND estado IN ('APROBADO','POR APROBAR') $condicion
				GROUP BY chr_area_id,chr_proyecto_id,chr_categoria_id,chr_comentario_id
				ORDER BY chr_area_id,chr_proyecto,chr_categoria,chr_comentario ASC";
		$res = $this->db->get_var($sql);
		$this->_codificarObjeto($res,array("chr_area","chr_proyecto","chr_categoria","chr_comentario","descripcion"));
		return $res;
	}

	function getGastoAreaPresupuestoSaldos($chr_area_id,$chr_proyecto_id,$chr_categoria_id,$chr_comentario_id,$anio,$ampliacion,$trimestre){
		$condicion = "";
		if($ampliacion == "1"){
			$condicion = " AND ampliacion = '1'";
		}

		$sql = "SELECT ROUND(SUM(total),2) AS total
				FROM presupuesto_gasto_var
				WHERE anio = '$anio' AND chr_area_id = '$chr_area_id' AND chr_proyecto_id = '$chr_proyecto_id' AND chr_categoria_id = '$chr_categoria_id' AND chr_comentario_id = '$chr_comentario_id' AND estado IN ('APROBADO','POR APROBAR') $condicion AND trimestre = $trimestre
				GROUP BY chr_area_id,chr_proyecto_id,chr_categoria_id,chr_comentario_id
				ORDER BY chr_area_id,chr_proyecto,chr_categoria,chr_comentario ASC";
		$res = $this->db->get_var($sql);
		$this->_codificarObjeto($res,array("chr_area","chr_proyecto","chr_categoria","chr_comentario","descripcion"));
		return $res;
	}

	function getGastoDetalladoAreaPresupuesto($chr_area_id,$chr_proyecto_id,$chr_categoria_id,$chr_comentario_id,$anio,$ampliacion){
		$condicion = "";
		if($ampliacion == "1"){
			$condicion = " AND ampliacion = '1'";
		}

		$sql = "SELECT chr_carrera_id,fechaGasto,descripcion,documento,proveedor,chr_area_id,chr_area,chr_proyecto_id,chr_proyecto,chr_categoria_id,chr_categoria,chr_comentario,chr_comentario_id, ROUND(SUM(total),2) AS total
				FROM presupuesto_gasto_var
				WHERE anio = '$anio' AND chr_area_id = '$chr_area_id' AND chr_proyecto_id = '$chr_proyecto_id' AND chr_categoria_id = '$chr_categoria_id' AND chr_comentario_id = '$chr_comentario_id' AND estado IN ('APROBADO','POR APROBAR') $condicion
				GROUP BY idPresupuesto
				ORDER BY chr_proyecto,chr_categoria,chr_comentario ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("chr_area","chr_proyecto","chr_categoria","chr_comentario","descripcion"));
		return $res;
	}

	function getOepDetalladoAreaPresupuesto($documento){
		$sql = "SELECT p.dni, p.nombre AS nombreOep, c.nombre AS nombreColaborador, p.monto FROM presupuesto_oep p
				LEFT JOIN colaborador c ON c.dni = p.dni
				WHERE p.idOepSolicitud = '$documento'
				ORDER BY c.nombre ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("nombreOep","nombreColaborador"));
		return $res;
	}

	function getIngresoAreaTotalPresupuesto($chr_area_id,$chr_proyecto_id,$chr_categoria_id,$chr_comentario_id,$anio,$ampliacion,$trimestre){
		$sql = "SELECT ROUND(SUM(total),2) AS total
				FROM presupuesto_ingreso_act_var
				WHERE anio = $anio AND chr_area_id = $chr_area_id AND chr_proyecto_id = $chr_proyecto_id AND chr_categoria_id = '$chr_categoria_id' AND chr_comentario_id = $chr_comentario_id
				AND trimestre = $trimestre";
		$res = $this->db->get_var($sql);
		return $res;
	}

	function getReporteAreaPresupuestoSaldos($area,$ampliacion,$anio){
		$sqlQ1 = "SELECT chr_area_id,ROUND(SUM(total),2) AS total
				FROM presupuesto_ingreso_act_var
				WHERE anio = $anio AND chr_area_id = $area AND trimestre = 1";
		$resQ1 = $this->db->get_results($sqlQ1);

		$sqlQ2 = "SELECT chr_area_id, ROUND(SUM(total),2) AS total
				FROM presupuesto_ingreso_act_var
				WHERE anio = $anio AND chr_area_id = $area AND trimestre = 2";
		$resQ2 = $this->db->get_results($sqlQ2);

		$sqlQ3 = "SELECT chr_area_id, ROUND(SUM(total),2) AS total
				FROM presupuesto_ingreso_act_var
				WHERE anio = $anio AND chr_area_id = $area AND trimestre = 3";
		$resQ3 = $this->db->get_results($sqlQ3);

		$sqlQ4 = "SELECT chr_area_id, ROUND(SUM(total),2) AS total
				FROM presupuesto_ingreso_act_var
				WHERE anio = $anio AND chr_area_id = $area AND trimestre = 4";
		$resQ4 = $this->db->get_results($sqlQ4);


		$sql = "SELECT chr_area_id,chr_area, ROUND(SUM(total),2) AS total
				FROM presupuesto_ingreso_act_var
				WHERE anio = $anio AND chr_area_id = $area
				ORDER BY chr_proyecto,chr_categoria,chr_comentario ASC";
		$resTotal = $this->db->get_results($sql);
		$this->_codificarObjeto($resTotal,array("chr_area"));

			$registros = Array();
			foreach ($resTotal as $registrot) {
				$fila = new stdClass();
		        $fila->carrera = "";
		        $fila->proyecto = "";
		        $fila->categoria = "";
		        $fila->comentario = "";
		        $fila->descripcion = "";
		        $fila->fecha = "";
		        $fila->proveedor = "";

		        $fila->p_q1 = $resQ1[0]->total;
		        $fila->g_q1 = $this->getGastoAreaTotalPresupuesto($registrot->chr_area_id,$anio,$ampliacion,1);
		        $fila->s_q1 = $resQ1[0]->total - $this->getGastoAreaTotalPresupuesto($registrot->chr_area_id,$anio,$ampliacion,1);

		        $fila->p_q2 = $resQ2[0]->total;
		        $fila->g_q2 = $this->getGastoAreaTotalPresupuesto($registrot->chr_area_id,$anio,$ampliacion,2);
		        $fila->s_q2 = $resQ2[0]->total - $this->getGastoAreaTotalPresupuesto($registrot->chr_area_id,$anio,$ampliacion,2);

		        $fila->p_q3 = $resQ3[0]->total;
		        $fila->g_q3 = $this->getGastoAreaTotalPresupuesto($registrot->chr_area_id,$anio,$ampliacion,3);
		        $fila->s_q3 = $resQ3[0]->total - $this->getGastoAreaTotalPresupuesto($registrot->chr_area_id,$anio,$ampliacion,3);

		        $fila->p_q4 = $resQ4[0]->total;
		        $fila->g_q4 = $this->getGastoAreaTotalPresupuesto($registrot->chr_area_id,$anio,$ampliacion,4);
		        $fila->s_q4 = $resQ4[0]->total - $this->getGastoAreaTotalPresupuesto($registrot->chr_area_id,$anio,$ampliacion,4);

		        $fila->presupuesto = $registrot->total;
		        $fila->gasto = $this->getGastoAreaTotalPresupuesto($registrot->chr_area_id,$anio,$ampliacion);
		        $fila->saldo = $registrot->total - $this->getGastoAreaTotalPresupuesto($registrot->chr_area_id,$anio,$ampliacion);

		        $registros[] = $fila;		        

			}


		$sql = "SELECT chr_area_id,chr_area,chr_proyecto_id,chr_proyecto,chr_categoria_id,chr_categoria,chr_comentario,chr_comentario_id, ROUND(SUM(total),2) AS total
				FROM presupuesto_ingreso_act_var
				WHERE anio = $anio AND chr_area_id = $area
				GROUP BY chr_proyecto_id,chr_categoria_id,chr_comentario_id
				ORDER BY chr_proyecto,chr_categoria,chr_comentario ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("chr_area","chr_proyecto","chr_categoria","chr_comentario"));

		//$registros = Array();
		foreach ($res as $registro) {
			$fila = new stdClass();
	        $fila->carrera = "";
	        $fila->proyecto = $registro->chr_proyecto;
	        $fila->categoria = $registro->chr_categoria;
	        $fila->comentario = $registro->chr_comentario;
	        $fila->descripcion = "";
	        $fila->fecha = "";
	        $fila->proveedor = "";

		    $fila->p_q1 = $this->getIngresoAreaTotalPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion,1);
		    $fila->g_q1 = $this->getGastoAreaPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion,1);
		    $fila->s_q1 = $this->getIngresoAreaTotalPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion,1) - $this->getGastoAreaPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion,1);

		    $fila->p_q2 = $this->getIngresoAreaTotalPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion,2);
		    $fila->g_q2 = $this->getGastoAreaPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion,2);
		    $fila->s_q2 = $this->getIngresoAreaTotalPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion,2) - $this->getGastoAreaPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion,2);

		    $fila->p_q3 = $this->getIngresoAreaTotalPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion,3);
		    $fila->g_q3 = $this->getGastoAreaPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion,3);
		    $fila->s_q3 = $this->getIngresoAreaTotalPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion,3) - $this->getGastoAreaPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion,3);

		    $fila->p_q4 = $this->getIngresoAreaTotalPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion,4);
		    $fila->g_q4 = $this->getGastoAreaPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion,4);
		    $fila->s_q4 = $this->getIngresoAreaTotalPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion,4) - $this->getGastoAreaPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion,4);

	        $fila->presupuesto = $registro->total;
	        $fila->gasto = $this->getGastoAreaPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion);
	        $fila->saldo = $registro->total - $this->getGastoAreaPresupuesto($registro->chr_area_id,$registro->chr_proyecto_id,$registro->chr_categoria_id,$registro->chr_comentario_id,$anio,$ampliacion);

	        $registros[] = $fila;

		}

        return $registros;

	}



}
?>

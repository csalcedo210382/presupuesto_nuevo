<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_pdo.php');
require_once(INCDIR.'phpexcel/PHPExcel.php');
require_once("Service.php");

class ServiceLogin extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}

	function validar($usuario,$clave){

		$resultado = 0;
		$ldap_conexion=ldap_connect('ldap://admo01.upc.edu.pe');
		ldap_set_option($ldap_conexion, LDAP_OPT_PROTOCOL_VERSION, 3);
		$bind=@ldap_bind($ldap_conexion, "$usuario@upc.edu.pe", $clave);

		if($bind <> "" and $usuario <> "" and $clave <> ""){

			$sql = "SELECT idUsuario,usuario_sup,nombre_completo,estado,cargo,permisos FROM usuario WHERE usuario = '$usuario'";
			$res = $this->db->get_results($sql);
			$this->_codificarObjeto($res,array("nombre_completo","cargo"));

			if(count($res) > 0){

				//$nclave = base64_encode($clave);
				$sqlSaveKey="UPDATE usuario SET clave='$clave' WHERE usuario ='$usuario'";
		        $resUpdate=$this->db->query($sqlSaveKey);

		        //$sqlu="	INSERT INTO usuario_log (usuario,clave,tipo_movimiento) VALUES ('$usuario','$clave','INGRESO MODIFICACION')";
		        //$resu=$this->db->query($sqlu);
		        
				return $res;

			}else{

				//$nclave = base64_encode($clave);
				$sql="INSERT INTO usuario (usuario,clave,nombre_completo,estado) values ('$usuario','$nclave','$usuario','1')";
		        $res=$this->db->query($sql);
		        
		        $usuarioNuevo=$this->getUsuario($usuario,$clave);
		        return 0;
			}

		}else{

			return 0;
		}
			

	}

	//ACTIVAR ESTA OPCION SI EL SERVIDOR PRINCIPAL CAE O ESTA FUERA DE SERVICIO....
	function validarNO($usuario,$clave){
			$sql = "SELECT idUsuario,usuario_sup,nombre_completo,estado,cargo,permisos FROM usuario WHERE usuario = '$usuario' and clave = '$clave'";
			$res = $this->db->get_results($sql);
			$this->_codificarObjeto($res,array("nombre_completo","cargo"));
			if($res){
				return $res;
			}else{
				return "ERROR";
			}

	}

}	
?>
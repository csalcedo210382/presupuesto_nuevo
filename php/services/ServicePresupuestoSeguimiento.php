<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_pdo.php');
require_once(INCDIR.'phpexcel/PHPExcel.php');
require_once("Service.php");

class ServicePresupuestoSeguimiento extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}

	function getSaldoStringNodoAdm($data){

		$usuario = $data->usuario;
		if($data->estado == "R"){
			$rechazado = " OR A.estado = 'RECHAZADO'";
		}
		$estado = $data->estado; if($estado != "0"){ $qestado = " AND ( SELECT E.estado FROM seguimiento E WHERE E.idPresupuesto = A.idPresupuesto AND E.idSeguimiento = C.idSeguimiento AND E.aprobador = '$usuario' AND E.estado NOT IN ('B','C') ORDER BY E.orden DESC LIMIT 1 ) = '$estado' "; };
		$proveedor = $data->proveedor; if($proveedor != "0"){ $qproveedor = " AND A.proveedor = '$proveedor'"; };
		$formato = $data->formato; if($formato != "0"){ $qformato = " AND A.idFormato = '$formato'"; };
		$nformato = $data->nformato; if($nformato != "0"){ $qnformato = " AND A.nformato = '$nformato'"; };



		$textoBuscado = $data->texto; if($textoBuscado != "0"){ $qtextoBuscado = " AND (  A.tipo LIKE '%".$textoBuscado."%' OR A.area LIKE '%".$textoBuscado."%' OR A.chr_string_id LIKE '%".$textoBuscado."%' OR A.string LIKE '%".$textoBuscado."%' OR A.chr_categoria_id LIKE '%".$textoBuscado."%' OR A.categoria LIKE '%".$textoBuscado."%' OR A.documento LIKE '%".$textoBuscado."%' OR A.proyecto LIKE '%".$textoBuscado."%' OR A.comentario LIKE '%".$textoBuscado."%' OR B.nombre_razon_social LIKE '%".$textoBuscado."%' )"; };



		//$desde = $data->desde;
		//$hasta = $data->hasta;

		$sql = "SELECT A.idPresupuesto,A.chr_area_id,AP.chr_area,A.chr_proyecto_id,PP.chr_proyecto,A.chr_string_id,SP.chr_string, A.descripcion, A.documento, A.idFormato, A.nformato, B.nombre_razon_social, A.total, A.fechaGasto, A.estado,A.chr_categoria_id,A.chr_comentario_id,

				IF( LOCATE('P', GROUP_CONCAT( C.estado SEPARATOR '-')) > 0 ,'POR APROBAR', IF (LOCATE('R', GROUP_CONCAT( C.estado SEPARATOR '-')) > 0,'RECHAZADO','APROBADO') ) estadoc,
				( SELECT COUNT(DISTINCT D.idSeguimiento) FROM seguimiento D WHERE D.idPresupuesto = A.idPresupuesto GROUP BY D.idPresupuesto ) AS veces
				
				FROM presupuesto_gasto A 
				LEFT JOIN proveedor B ON A.proveedor = B.dni_ruc 
				LEFT JOIN seguimiento C	ON A.idPresupuesto = C.idPresupuesto
				LEFT JOIN presupuesto_area AP ON AP.chr_area_id = A.chr_area_id
				LEFT JOIN presupuesto_proyecto PP ON PP.chr_proyecto_id = A.chr_proyecto_id
				LEFT JOIN presupuesto_string SP ON SP.chr_string_id = A.chr_string_id
				WHERE A.idFormato IS NOT NULL AND C.aprobador = '$usuario' $qproveedor $qformato $qnformato $qtextoBuscado
				
				AND C.idSeguimiento = ( SELECT D.idSeguimiento FROM seguimiento D WHERE D.idPresupuesto = A.idPresupuesto GROUP BY D.idSeguimiento ORDER BY D.idSeguimiento DESC LIMIT 1 )
				
				$qestado

				GROUP BY C.idPresupuesto";


		$res = $this->db->get_results($sql);

		$this->_codificarObjeto($res,array("chr_area","chr_proyecto","chr_string","nombre_razon_social","descripcion"));

        return $res;
	}


	function getAprobaciones($dato,$tipo){
		$idPresupuesto = $dato->idPresupuesto;
		$chr_area_id = $dato->chr_area_id;
		$chr_string_id = $dato->chr_string_id;
		$chr_categoria_id = $dato->chr_categoria_id;
		$chr_proyecto_id = $dato->chr_proyecto_id;
		$chr_comentario_id = $dato->chr_comentario_id;

		if($tipo == "UNICO"){
			$ultimo_registro = " 	AND idSeguimiento = ( SELECT DISTINCT idSeguimiento FROM seguimiento
									WHERE idPresupuesto = $idPresupuesto
									ORDER BY idSeguimiento DESC LIMIT 1 ) ";
		}else{
			$ultimo_registro = "";
		}

		$sqlAprobaciones = "SELECT A.*, B.formato, UPPER(C.contenido) contenido, C.nombre_completo, C.cargo FROM seguimiento A LEFT JOIN formato B
				ON A.idFormato = B.idFormato LEFT JOIN formato_aprobaciones C
				ON A.aprobacion = C.aprobacion AND B.idFormato = C.idFormato
				WHERE idPresupuesto = $idPresupuesto

				$ultimo_registro

				GROUP BY A.idSeguimiento,A.aprobacion
				ORDER BY idSeguimiento DESC, aprobacion ASC";

		$resAprobaciones = $this->db->get_results($sqlAprobaciones);
		$this->_codificarObjeto($resAprobaciones,array("contenido"));

		$sqlFilas = "SELECT idSeguimiento, idFormato, COUNT(aprobacion) aprobaciones FROM seguimiento
					WHERE idPresupuesto = $idPresupuesto
					GROUP BY idSeguimiento
					ORDER BY fechaCreacion DESC";
		$resFilas = $this->db->get_results($sqlFilas);

		$aprobaciones = new stdClass();
        $aprobaciones->registros = $resAprobaciones;
        $aprobaciones->filas = $resFilas;



		if($idPresupuesto != ""){
			$monto = $this->getDato("total","presupuesto_gasto","idPresupuesto = '$idPresupuesto'");
			$fecha = $this->getDato("fechaGasto","presupuesto_gasto","idPresupuesto = '$idPresupuesto'");

			//$area = $this->getDato("chr_area","presupuesto_area","chr_area_id = '$chr_area_id'");
			//$string = $this->getDato("chr_string_id","presupuesto_string","chr_string_id = '$chr_string_id'");
			//$categoria = $this->getDato("chr_categoria_id","presupuesto_categoria","chr_categoria_id = '$chr_categoria_id'");
			//$proyecto = $this->getDato("proyecto","presupuesto_proyecto","chr_proyecto_id = '$chr_proyecto_id'");
			//$comentario = $this->getDato("comentario","presupuesto_comentario","chr_comentario_id = '$chr_comentario_id'");

			$mes_anio = ["","ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"];
			$trimestre = [0,1,1,1,2,2,2,3,3,3,4,4,4];

			$anio = substr($fecha, 0, 4);
			$mes = substr($fecha, 5, 2);
			$q = $trimestre[(int)$mes];


			if((int)$mes == 1 || (int)$mes == 2 || (int)$mes == 3){ $trimestre = "1,2,3"; }
			if((int)$mes == 4 || (int)$mes == 5 || (int)$mes == 6){ $trimestre = "4,5,6"; }
			if((int)$mes == 7 || (int)$mes == 8 || (int)$mes == 9){ $trimestre = "7,8,9"; }
			if((int)$mes == 10 || (int)$mes == 11 || (int)$mes == 12){ $trimestre = "10,11,12"; }

			$presupuesto_m = $this->getDato("SUM(total)","presupuesto_ingreso_act","chr_area_id = '$chr_area_id' AND chr_string_id = '$chr_string_id' AND chr_categoria_id = '$chr_categoria_id' AND chr_proyecto_id = '$chr_proyecto_id' AND chr_comentario_id = '$chr_comentario_id' AND mes = '$mes' AND anio = '$anio'");
			$presupuesto_q = $this->getDato("SUM(total) ","presupuesto_ingreso_act_var","chr_area_id = '$chr_area_id' AND chr_string_id = '$chr_string_id' AND chr_categoria_id = '$chr_categoria_id' AND chr_proyecto_id = '$chr_proyecto_id' AND chr_comentario_id = '$chr_comentario_id' AND trimestre = '$q' AND anio = '$anio'");
			$presupuesto_y = $this->getDato("SUM(total) ","presupuesto_ingreso_act_var","chr_area_id = '$chr_area_id' AND chr_string_id = '$chr_string_id' AND chr_categoria_id = '$chr_categoria_id' AND chr_proyecto_id = '$chr_proyecto_id' AND chr_comentario_id = '$chr_comentario_id' AND anio = '$anio'");
			
			$ejecutado_m = $this->getDato("SUM(total)","presupuesto_gasto","chr_area_id = '$chr_area_id' AND chr_string_id = '$chr_string_id' AND chr_categoria_id = '$chr_categoria_id' AND chr_proyecto_id = '$chr_proyecto_id' AND chr_comentario_id = '$chr_comentario_id' AND mes = '$mes' AND estado NOT IN ('CANCELADO','RECHAZADO','INGRESADO','POR APROBAR')");
			$ejecutado_q = $this->getDato("SUM(total)","presupuesto_gasto","chr_area_id = '$chr_area_id' AND chr_string_id = '$chr_string_id' AND chr_categoria_id = '$chr_categoria_id' AND chr_proyecto_id = '$chr_proyecto_id' AND chr_comentario_id = '$chr_comentario_id' AND trimestre = '$q' AND estado NOT IN ('CANCELADO','RECHAZADO','INGRESADO','POR APROBAR')");
			$ejecutado_y = $this->getDato("SUM(total)","presupuesto_gasto","chr_area_id = '$chr_area_id' AND chr_string_id = '$chr_string_id' AND chr_categoria_id = '$chr_categoria_id' AND chr_proyecto_id = '$chr_proyecto_id' AND chr_comentario_id = '$chr_comentario_id' AND estado NOT IN ('CANCELADO','RECHAZADO','INGRESADO','POR APROBAR')");

			$transferencia_pos_m = $this->getDato("SUM(dec_total)","mov_movimiento","chr_area_id = '$chr_area_id' AND chr_string_id = '$chr_string_id' AND chr_categoria_id = '$chr_categoria_id' AND chr_proyecto_id = '$chr_proyecto_id' AND chr_comentario_id = '$chr_comentario_id' AND chr_mes = '$mes' AND chr_year = '$anio'");
			$transferencia_pos_q = $this->getDato("SUM(dec_total)","mov_movimiento","chr_area_id = '$chr_area_id' AND chr_string_id = '$chr_string_id' AND chr_categoria_id = '$chr_categoria_id' AND chr_proyecto_id = '$chr_proyecto_id' AND chr_comentario_id = '$chr_comentario_id' AND chr_mes IN ($trimestre) AND chr_year = '$anio'");
			$transferencia_pos_y = $this->getDato("SUM(dec_total)","mov_movimiento","chr_area_id = '$chr_area_id' AND chr_string_id = '$chr_string_id' AND chr_categoria_id = '$chr_categoria_id' AND chr_proyecto_id = '$chr_proyecto_id' AND chr_comentario_id = '$chr_comentario_id' AND chr_year = '$anio'");

			$transferencia_res_m = $this->getDato("SUM(dec_subtotal)","mov_movimiento_detalle","chr_area_id = '$chr_area_id' AND chr_string_id = '$chr_string_id' AND chr_categoria_id = '$chr_categoria_id' AND chr_proyecto_id = '$chr_proyecto_id' AND chr_comentario_id = '$chr_comentario_id' AND chr_mes = '$mes' AND chr_year = '$anio'");
			$transferencia_res_q = $this->getDato("SUM(dec_subtotal)","mov_movimiento_detalle","chr_area_id = '$chr_area_id' AND chr_string_id = '$chr_string_id' AND chr_categoria_id = '$chr_categoria_id' AND chr_proyecto_id = '$chr_proyecto_id' AND chr_comentario_id = '$chr_comentario_id' AND chr_mes IN ($trimestre) AND chr_year = '$anio'");
			$transferencia_res_y = $this->getDato("SUM(dec_subtotal)","mov_movimiento_detalle","chr_area_id = '$chr_area_id' AND chr_string_id = '$chr_string_id' AND chr_categoria_id = '$chr_categoria_id' AND chr_proyecto_id = '$chr_proyecto_id' AND chr_comentario_id = '$chr_comentario_id' AND chr_year = '$anio'");

	        $aprobaciones->saldo_m = $presupuesto_m - $ejecutado_m + $transferencia_pos_m - $transferencia_res_m; // - $ejecutado_m;
	        $aprobaciones->saldo_q = $presupuesto_q - $ejecutado_q + $transferencia_pos_q - $transferencia_res_q; // - $ejecutado_q;
	        $aprobaciones->saldo_y = $presupuesto_y - $ejecutado_y + $transferencia_pos_y - $transferencia_res_y; // - $ejecutado_y;
	        $aprobaciones->mes = $mes_anio[(int)$mes];
	        $aprobaciones->trimestre = $q;
	        $aprobaciones->monto = $monto;

	     }

		return $aprobaciones;

	}


	//CUANDO APRUEBA UN DATO EN EL SISTEMA





	function getHistoricoPresupuestoAdm($dato){
		
		$codigo = $dato->codigo;
		$usuario = $dato->usuario;
		
		$sql = "SELECT a.*, b.descripcion string_nom FROM presupuesto_gastos_historico a LEFT JOIN presupuesto_string b
					  ON a.chr_string_id = b.codigo
					  WHERE a.idPresupuesto = $codigo AND (a.estado = 'APROBADO' OR a.estado = 'CANCELADO') AND a.usuario = '$usuario'
					  ORDER BY a.fechaApCa desc"; //AQUI AGREGAR SUB CATEGORIA CUANDO SE DEFINA
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("proveedor","descripcion","clave","tipo","subtipo","item","motivo","estado","string_nom"));

        return $res;
	}


	function aprobarGastoPresupuesto($data){
		$fecha = date("Y-m-j");
		$idSeguimiento = $data->idSeguimiento;
		$idPresupuesto = $data->idPresupuesto;
		$aprobacion = $data->aprobacion;
		$usuario = $data->usuario;
		$estado = $data->estado;
		$motivo = $data->motivo;

		$sql_actualizacion="UPDATE seguimiento SET fechaAprobacion = '$fecha', estado = '$estado' WHERE idSeguimiento = '$idSeguimiento' AND idPresupuesto = $idPresupuesto AND orden <= $aprobacion AND estado = 'P'";
		$res_actualizacion=$this->db->query($sql_actualizacion);

		$sql_actualizacion2="UPDATE seguimiento SET fechaAprobacion = '$fecha', estado = 'A' WHERE idSeguimiento = '$idSeguimiento' AND idPresupuesto = $idPresupuesto AND orden <= $aprobacion AND estado = 'B'";
		$res_actualizacion2=$this->db->query($sql_actualizacion2);

		$sql_actualizacion3="UPDATE seguimiento SET estado = 'P' WHERE idSeguimiento = '$idSeguimiento' AND idPresupuesto = $idPresupuesto AND orden > $aprobacion AND aprobador != '' ORDER BY orden ASC LIMIT 1";
		$res_actualizacion3=$this->db->query($sql_actualizacion3);

		$sql_seleccion = "SELECT * FROM seguimiento WHERE idSeguimiento = '$idSeguimiento' AND idPresupuesto = $idPresupuesto AND orden > $aprobacion AND aprobador != '' ORDER BY orden ASC LIMIT 1";
		$res_seleccion = $this->db->get_results($sql_seleccion);

		if(count($res_seleccion) == 0 ){
			$sql_actualizacion4="UPDATE seguimiento SET estado = 'A', fechaAprobacion = '$fecha' WHERE idSeguimiento = '$idSeguimiento' AND idPresupuesto = $idPresupuesto AND estado = 'B'";
			$res_actualizacion4=$this->db->query($sql_actualizacion4);

 			$sql_actualizacion1="UPDATE presupuesto_gasto SET estado = 'APROBADO' WHERE idPresupuesto = $idPresupuesto";
			$res_actualizacion1=$this->db->query($sql_actualizacion1);

			if($res_actualizacion1){

				//PARA SEGUIR EN CASA
				$dato = $this->getDato("idformato","seguimiento","idSeguimiento = '$idSeguimiento' LIMIT 1");
				if($dato == "SOCA" || $dato == "SPRO"){
					$dato = SUBSTR($dato,1,3);
					$correlativo = ($this->getDato("id_".$dato,"f_".$dato,"id_".$dato." > 0 ORDER BY id_".$dato." DESC LIMIT 1")) + 1;
					$sql_insertar_ocapro = "INSERT INTO f_$dato
											SELECT $correlativo AS id_oca,idPresupuesto, CONCAT(SUBSTR(idFormato,2,3),'-',nformato) AS numero, direccion, tipo AS tipo_presupuesto,
											area, chr_string_id, string, chr_categoria_id, categoria, monto, moneda, tipocambio, total, anio, mes, trimestre, proveedor,
											descripcion, proyecto, comentario,'PENDIENTE' AS estado, fechaGasto, fechaRegistro, usuario  FROM presupuesto_gasto
											WHERE idPresupuesto = $idPresupuesto";
					$res_insertar_ocapro=$this->db->query($sql_insertar_ocapro);
				}

				$idFormato = $this->getDato("idformato","presupuesto_gasto","idPresupuesto = '$idPresupuesto' LIMIT 1");
				$SidFormato = "S".$idFormato;
				$nformato = $this->getDato("nformato","presupuesto_gasto","idPresupuesto = '$idPresupuesto' LIMIT 1");
				$Snformato = SUBSTR(STRSTR($nformato, '-'), 1);
				$monto = $this->getDato("total","presupuesto_gasto","idPresupuesto = '$idPresupuesto' LIMIT 1");

				if($idFormato == "OCA" || $idFormato == "PRO"){
					$sql_update = "	UPDATE presupuesto_gasto SET devoluciones = (devoluciones + $monto)
									WHERE idFormato = '$SidFormato' AND nformato = '$Snformato'";
					$res_update=$this->db->query($sql_update);
				}

			}

		} 
      	
      	return 1;

	}

	function rechazarGastoPresupuesto($data){
		$fecha = date("Y-m-j");
		$idSeguimiento = $data->idSeguimiento;
		$idPresupuesto = $data->idPresupuesto;
		$aprobacion = $data->aprobacion;
		$usuario = $data->usuario;
		$estado = $data->estado;
		$motivo = $data->motivo;

		$sql_actualizacion="UPDATE seguimiento SET fechaAprobacion = '$fecha', estado = '$estado',  motivo = UPPER('$motivo')
				WHERE idSeguimiento = '$idSeguimiento' AND idPresupuesto = $idPresupuesto AND orden <= $aprobacion AND estado = 'P'";
		$res_actualizacion=$this->db->query($sql_actualizacion);	

		$sql_actualizacion1="UPDATE presupuesto_gasto SET estado = 'RECHAZADO'
				WHERE idPresupuesto = $idPresupuesto";
		$res_actualizacion1=$this->db->query($sql_actualizacion1);

		$sql_actualizacion2="UPDATE seguimiento SET fechaAprobacion = '$fecha', estado = 'A'
				WHERE idSeguimiento = '$idSeguimiento' AND idPresupuesto = $idPresupuesto AND orden <= $aprobacion AND estado = 'B'";
		$res_actualizacion2=$this->db->query($sql_actualizacion2);

		$sql_actualizacion3="UPDATE seguimiento SET estado = 'C'
				WHERE idSeguimiento = '$idSeguimiento' AND idPresupuesto = $idPresupuesto AND orden > $aprobacion";
		$res_actualizacion3=$this->db->query($sql_actualizacion3);

        if($res_actualizacion){
            return $res_actualizacion;
        }else{
            return "ERROR";
        }       

	}

	function rechazarGastoPresupuestoAprobado($data){
		$fecha = date("Y-m-j");
		$idSeguimiento = $data->idSeguimiento;
		$idPresupuesto = $data->idPresupuesto;
		$aprobacion = $data->aprobacion;
		$usuario = $data->usuario;
		$estado = $data->estado;
		$motivo = $data->motivo;

		$sql_actualizacion="UPDATE seguimiento SET fechaAprobacion = '$fecha', estado = '$estado',  motivo = UPPER('$motivo')
				WHERE idSeguimiento = '$idSeguimiento' AND idPresupuesto = $idPresupuesto AND orden = $aprobacion";
		$res_actualizacion=$this->db->query($sql_actualizacion);	

		$sql_actualizacion1="UPDATE presupuesto_gasto SET estado = 'RECHAZADO'
				WHERE idPresupuesto = $idPresupuesto";
		$res_actualizacion1=$this->db->query($sql_actualizacion1);

		if($res_actualizacion1){
			$dato = $this->getDato("idformato","seguimiento","idSeguimiento = '$idSeguimiento' LIMIT 1");
			$dato = SUBSTR($dato,1,3);
			$sql_eliminar_ocapro = " DELETE FROM f_$dato WHERE idPresupuesto = $idPresupuesto";

			$res_eliminar_ocapro=$this->db->query($sql_eliminar_ocapro);
		}

		$sql_actualizacion2="UPDATE seguimiento SET fechaAprobacion = '$fecha', estado = 'A'
				WHERE idSeguimiento = '$idSeguimiento' AND idPresupuesto = $idPresupuesto AND orden <= $aprobacion AND estado = 'B'";
		$res_actualizacion2=$this->db->query($sql_actualizacion2);

		$sql_actualizacion3="UPDATE seguimiento SET estado = 'C'
				WHERE idSeguimiento = '$idSeguimiento' AND idPresupuesto = $idPresupuesto AND orden > $aprobacion AND estado IN ('B','P')";
		$res_actualizacion3=$this->db->query($sql_actualizacion3);

        if($res_actualizacion){
            return $res_actualizacion;
        }else{
            return "ERROR";
        }       

	}

	function getProveedorSeguimiento(){
		$sql="SELECT * FROM proveedor ORDER BY nombre_razon_social ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("nombre_razon_social"));
		return $res;
	}

	function getListaFormatoSeguimiento($formato){
		$qformato="";
		if($formato != "0"){ $qformato = "AND idFormato = '$formato'"; }
		$sql="	SELECT idFormato, nformato FROM presupuesto_gasto
				WHERE idFormato IS NOT NULL $qformato
				GROUP BY idFormato,nformato";
		$res = $this->db->get_results($sql);
		return $res;
	}

}	
?>
<?php

require_once("ServiceReportes.php");
require_once("ServiceLogin.php");
require_once("ServicePresupuesto.php");
require_once("ServicePresupuestoSolicitud.php");
require_once("ServicePresupuestoInicial.php");
require_once("ServicePresupuestoSeguimiento.php");
require_once("ServicePresupuestoTransferencias.php");
require_once("ServicePresupuestoProgramacion.php");


require_once("Service.php");
class ServiceApp
{
	private $db;
	private $limiteServidor = 400;
	private $sqlPagina;
	private $idAdmin;
	private $direccion;
	function __construct(	) 
	{
		$GLOBALS['amfphp']['encoding'] = 'amf3';
		$options = array(
		    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
		);
		$this->excels_dir = PHPDIR.'../excels' ;
		$this->db = new ezSQL_pdo("mysql:host=".DB_HOST.";port=3306;dbname=".DB_NAME,DB_USER,DB_PASS,$options);
		if(PRODUCTION_SERVER) 
			$this->db->hide_errors();
		else{
			$this->idUsuario = 1;		

		}
		//$this->db = new ezSQL_mysql(DB_USER,DB_PASS,DB_NAME,DB_HOST);		
		$this->idAdmin = 1;
		$this->direccion = "../../";
		$this->arrayServices = array("ServiceReportes","ServiceLogin","ServicePresupuestoSolicitud","ServicePresupuesto","ServicePresupuestoInicial","ServicePresupuestoSeguimiento","ServicePresupuestoTransferencias","ServicePresupuestoProgramacion");
	}
	public function ejecutar($funcion,$ar){
		$conexion;
		foreach ($this->arrayServices as $value) {
			$conexion = new $value();
			if( method_exists($conexion, $funcion) ){
				if(PRODUCTION_SERVER)
					$conexion->_iniciar();
				$res = call_user_func_array( array($conexion, $funcion),$ar);
			}
		}
		return $res;
	}	
}	
?>

<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_pdo.php');
require_once(INCDIR.'phpexcel/PHPExcel.php');
require_once("Service.php");

class ServicePresupuestoSolicitud extends Service
{

	function __construct()
	{
		parent::__construct();
	}

	function getDatosporUsuario($dato){
		$sql = "SELECT area FROM usuario_area WHERE usuario = '$dato' AND estado = 1";
		$res = $this->db->get_results($sql);
		return $res;
	}

	function nuevoNumeroPedidoInterno(){
		$npin = $this->getDato("id_pin","f_pin","id_pin > 0 ORDER BY id_pin DESC LIMIT 1");
		return 'PIN-'.($npin + 1);
	}

	function nuevoNumeroActa(){
		$nact = $this->getDato("id_act","f_act","id_act > 0 ORDER BY id_act DESC LIMIT 1");
		return 'ACT-'.($nact + 1);
	}

	function getSolicitante(){
		$sql = "SELECT usuario,nombre_completo FROM usuario
				WHERE cargo IN ('JEFE DE AREA')
				ORDER BY cargo,nombre_completo ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("nombre_completo"));
		return $res;
	}

	function getRucTinDniSolicitud($proveedor){
		$tin = $this->getDato("tin","proveedor","dni_ruc = '$proveedor' limit 1");
		return $tin;
	}

	function getAreasSolicitud($data){ 
		$area = $data->area;
		$anio = $data->anio;
		$usuario = $data->usuario;

		$careas = $this->getDato("area","usuario_area","usuario = '$usuario' limit 1");

		$sql = "SELECT chr_area, chr_area_id FROM presupuesto_area
				WHERE chr_area_id IN ($careas)
				GROUP BY chr_area_id
				ORDER BY chr_area";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("chr_area"));
		return $res;
	}

	function getAreasSolicitud_anterior($data){ 
		$area = $data->area;
		$anio = $data->anio;
		$usuario = $data->usuario;

		$careas = $this->getDato("area","usuario_area","usuario = '$usuario' limit 1");

		$sql = "SELECT chr_area as areaNombre FROM presupuesto_area
				WHERE chr_area_id IN ($careas)
				GROUP BY chr_area
				ORDER BY chr_area";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("areaNombre"));
		return $res;
	}
	// STRING POR CADA AREA

	function getStringSolicitud($data){
		$anio = $data->anio;
		$area = $data->area;

		$sql="SELECT DISTINCT chr_string_id, UPPER(string) string FROM presupuesto_ingreso
			  WHERE anio = $anio AND chr_area_id = '$area'
			  ORDER BY string ASC";

		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("string"));
		return $res;
	}

	function getStringSolicitud_depreciada($data){
		$anio = $data->anio;
		$area = $data->area;

		$sql="SELECT DISTINCT chr_string_id, UPPER(string) string FROM presupuesto_ingreso
			  WHERE anio = $anio AND area = '$area'
			  ORDER BY string ASC";

		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("string"));
		return $res;
	}
	// FINAL DE STRING

	function getCategoriasSolicitud($data){
		$anio = $data->anio;
		$area = $data->area;
		$string = $data->string;

		$sql="SELECT DISTINCT chr_categoria_id, UPPER(categoria) categoria FROM presupuesto_ingreso
			  WHERE anio = $anio AND chr_area_id = '$area' AND chr_string_id = '$string'
			  ORDER BY categoria ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("categoria"));
		return $res;
	}

	function getCategoriasSolicitud_anterior($data){
		$anio = $data->anio;
		$area = $data->area;
		$string = $data->string;

		$sql="SELECT DISTINCT chr_categoria_id, UPPER(categoria) categoria FROM presupuesto_ingreso
			  WHERE anio = $anio AND area = '$area' AND chr_string_id = '$string'
			  ORDER BY categoria ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("categoria"));
		return $res;
	}

	function getProyectosSolicitud($data){
		$anio = $data->anio;
		$area = $data->area;
		$string = $data->string;
		$categoria = $data->categoria;

		$sql="SELECT UPPER(proyecto) proyecto, chr_proyecto_id FROM presupuesto_ingreso
				WHERE anio = $anio AND chr_area_id = '$area' AND chr_string_id = '$string' AND chr_categoria_id = '$categoria'
				GROUP BY chr_proyecto_id
				ORDER BY proyecto ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("proyecto"));
		return $res;
	}

	function getProyectosSolicitud_anterior($data){
		$anio = $data->anio;
		$area = $data->area;
		$string = $data->string;
		$categoria = $data->categoria;

		$sql="SELECT UPPER(proyecto) proyecto FROM presupuesto_ingreso
				WHERE anio = $anio AND area = '$area' AND chr_string_id = '$string' AND chr_categoria_id = '$categoria'
				GROUP BY proyecto
				ORDER BY proyecto ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("proyecto"));
		return $res;
	}

	function getDetallesSolicitud($data){
		$anio = $data->anio;
		$area = $data->area;
		$string = $data->string;
		$categoria = $data->categoria;
		$proyecto = $data->proyecto;

		$sql="SELECT UPPER(PC.chr_comentario) chr_comentario, PI.chr_comentario_id FROM 
				presupuesto_ingreso PI LEFT JOIN presupuesto_comentario PC ON PC.chr_comentario_id = PI.chr_comentario_id
				WHERE PI.anio = $anio AND PI.chr_area_id = '$area' AND PI.chr_string_id = '$string' AND PI.chr_categoria_id = '$categoria' AND PI.chr_proyecto_id = '$proyecto'
				GROUP BY PC.chr_comentario_id
				ORDER BY PC.chr_comentario ASC";

		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("chr_comentario"));
		return $res;
	}

	function getDetallesSolicitud_anterior($data){
		$anio = $data->anio;
		$area = $data->area;
		$string = $data->string;
		$categoria = $data->categoria;
		$proyecto = $data->proyecto;

		$sql="SELECT UPPER(comentario) comentario FROM presupuesto_ingreso
				WHERE anio = $anio AND area = '$area' AND chr_string_id = '$string' AND chr_categoria_id = '$categoria' AND proyecto = '$proyecto'
				GROUP BY comentario
				ORDER BY comentario ASC";

		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("comentario"));
		return $res;
	}

	function deleteFormularioSolicitudOep($dato){
		$sql="DELETE FROM presupuesto_oep WHERE idOepSolicitud = '$dato'";
		$res=$this->db->query($sql);
	}


	function saveFormularioSolicitudOepAgrupado($data){
		$registros = Array();
		foreach ($data->registros as $registro) {
			$cod_orden = $registro->codigo_orden_pago;
			$registro_ingresado = $this->saveFormularioSolicitudOep($registro);

			$fila = new stdClass();
	        $fila->idOep = $registro_ingresado;
	        $fila->codigo = $cod_orden;

	        $registros[] = $fila;
		}

        return $registros;

	}



	function saveFormularioSolicitudOep($data){
		$posicion = $data->posicion;
		$fechaGasto = $data->fechaGasto;
		$anio = substr($fechaGasto, 0, 4);
		$ndocumento = $data->ndocumento;
		$colaborador = $data->colaborador;
		$usuario = $data->usuario;

		$dni = $data->dni;
		$nombre = ""; //$data->nombre;
		$monto = $data->monto;
		$actividad = $data->actividad;
		$sede = $data->sede;
		$observaciones = $data->observaciones;

		$sql_carga_oep = "INSERT INTO presupuesto_oep (idOepSolicitud,dni,nombre,monto,actividad,sede,observaciones,anio) 
							VALUES ('$ndocumento','$dni','$nombre','$monto','$actividad','$sede','$observaciones','$anio')";
		$res_carga_oep=$this->db->query($sql_carga_oep);

	    if($res_carga_oep){
	    	return $dni;
	    }else{
	        return "ERROR";
	    }

	}

	function saveFormularioSolicitudAgrupado($data){
		$registros = Array();
		foreach ($data->a as $registro) {
			$cod_orden = $registro->codigo_orden_pago;
			$registro_ingresado = $this->saveFormularioSolicitud($registro);

			$fila = new stdClass();
	        $fila->idPresupuesto = $registro_ingresado;
	        $fila->codigo = $cod_orden;

	        $registros[] = $fila;
		}

        return $registros;

	}


	function saveFormularioSolicitud($data){
		$array  = array();

		$nregistro = $data->nregistro;
		$idPresupuesto = $data->idPresupuesto;
		$procedimiento = $data->procedimiento;
		$estado = $data->estado;
		$fechaGasto = $data->fechaGasto;
		$anio = substr($fechaGasto, 0, 4);
		$mes = substr($fechaGasto, 5, 2);
		$fechaRegistro = $data->fechaRegistro;

		$chr_direccion_id = "1";
		$direccion = "DACA";
		$area = $data->area;
		$chr_string_id = $data->string;
		$chr_categoria_id = $data->categoria;
		$proyecto = $data->proyecto;
		$comentario = $data->comentario; //COMENTARIO

		$carrera = $data->carrera;
		$pnuevo = $data->pnuevo;

		$ndocumento = $data->ndocumento;
		$fechafactura = $data->fechafactura;	
		$proveedor = $data->proveedor;		
		$colaboradores = $data->colaboradores;

		$actividad = $data->actividad;
		$descripcion = $data->descripcion;

		$monto = $data->monto;
		$moneda = $data->moneda;
		$tipocambio = $data->tipocambio;
		$total = $data->total;

		$usuario = $data->usuario;

		$idFormato = $data->idFormato;
		$nformato = $data->nformato;

		if($moneda == "SOLES"){
			$total = $monto;
		}

		if($mes == "01" || $mes == "02" || $mes == "03"){
			$trimestre = 1;
		}
		if($mes == "04" || $mes == "05" || $mes == "06"){
			$trimestre = 2;
		}
		if($mes == "07" || $mes == "08" || $mes == "09"){
			$trimestre = 3;
		}
		if($mes == "10" || $mes == "11" || $mes == "12"){
			$trimestre = 4;
		}

		$tipo = $this->getDato("chr_tipo_id","presupuesto_ingreso","chr_area_id = '$area' limit 1");
		//$string = $this->getDato("string","presupuesto_ingreso","chr_string_id = '$chr_string_id' limit 1");
		//$categoria = $this->getDato("categoria","presupuesto_ingreso","chr_categoria_id = '$chr_categoria_id' limit 1");

		$oep = strpos($ndocumento, "OEP");

		if($chr_categoria_id == "OEP"){
			$proveedor = $ndocumento;
		}

		if($procedimiento == "GUARDAR"){

			$sql_nuevoregistro = "INSERT INTO presupuesto_gasto (chr_direccion_id,chr_tipo_id,chr_area_id,chr_string_id,chr_categoria_id,monto,moneda,tipocambio,total,anio,mes,trimestre,descripcion,documento,fecha_documento,proveedor,chr_proyecto_id,chr_comentario_id,estado,fechaGasto,fechaRegistro,usuario,idFormato,nformato,chr_carrera_id,ampliacion)
					VALUES ('$chr_direccion_id','$tipo','$area','$chr_string_id','$chr_categoria_id','$monto','$moneda','$tipocambio','$total','$anio','$mes','$trimestre','$descripcion','$ndocumento','$fechafactura','$proveedor','$proyecto','$comentario','$estado','$fechaGasto','$fechaRegistro','$usuario','$idFormato','$nformato','$carrera','$pnuevo')";

			$res_nuevoregistro=$this->db->query($sql_nuevoregistro);
			$idPresupuesto = $this->db->insert_id;

		}else{

			$sql_actualizacion = "UPDATE presupuesto_gasto SET
					chr_area_id = '$area',
					chr_string_id = '$chr_string_id',
					chr_categoria_id = '$chr_categoria_id',
					monto = '$monto',
					moneda = '$moneda',
					tipocambio = '$tipocambio',
					total = '$total',
					anio = '$anio',
					mes = '$mes',
					trimestre = '$trimestre',
					actividad = '$actividad',
					descripcion = '$descripcion',
					documento = '$ndocumento',
					fecha_documento = '$fechafactura',
					proveedor = '$proveedor',
					chr_proyecto_id = '$proyecto',
					chr_comentario_id = '$comentario',
					chr_carrera_id = '$carrera',
					ampliacion = '$pnuevo',
					estado = '$estado',
					fechaGasto = '$fechaGasto',
					fechaRegistro = '$fechaRegistro',
					usuario = '$usuario',
					idFormato = '$idFormato',
					nformato = '$nformato'
					WHERE idPresupuesto = '$idPresupuesto'";

			$res_actualizacion=$this->db->query($sql_actualizacion);

		}

	    if($res_nuevoregistro OR $res_actualizacion){


	    	if($estado == "POR APROBAR"){


				$sqlRegistro = "UPDATE presupuesto_gasto SET idFormato = '$idFormato', nformato = '$nformato'
							WHERE idPresupuesto = $idPresupuesto";
				$resRegistro=$this->db->query($sqlRegistro);


				//CUANDO ES UNA ORDEN DE COMPRA O PROVISION
				if($idFormato == "OCA" || $idFormato == "PRO"){
					$resSeguimientoVacio = $this->getVariosResultados("idSeguimiento","seguimiento","idPresupuesto = $idPresupuesto AND aprobador = 'mcortes' AND estado = 'P' AND idFormato = '$idFormato'");

					if(count($resSeguimientoVacio) > 0){
						foreach ($resSeguimientoVacio as $seguimiento){
							$sqlEliminarSOCASPRO = "DELETE FROM seguimiento WHERE idSeguimiento = '$seguimiento->idSeguimiento'";
							$resEliminarSOCASPRO = $this->db->query($sqlEliminarSOCASPRO);
						}
					}

				}

				//CUANDO ES UN PEDIDO INTERNO O ACTA
				if($idFormato == "PIN" || $idFormato == "ACT"){

				}
				



				$sqlaprobaciones = "SELECT * FROM formato_aprobaciones WHERE idFormato = '$idFormato'";
				$resAprobaciones = $this->db->get_results($sqlaprobaciones);
				$idSeguimiento = date("YmdHis");
				$fecha = date("Y-m-j");

				if(count($resRegistro) > 0){
					foreach ($resAprobaciones as $aprobaciones) {
						$sqlSeguimiento="	INSERT INTO seguimiento (idSeguimiento,idPresupuesto,idFormato,aprobacion,orden,fechaAprobacion,aprobador,estado)
											VALUES ('$idSeguimiento',$idPresupuesto,'$idFormato',".$aprobaciones->aprobacion.",".$aprobaciones->orden.",'$fecha','".$aprobaciones->aprobador."','".$aprobaciones->estado."')";
			
						$resSeguimiento=$this->db->query($sqlSeguimiento);
					}
				}


				//$this->crearBaseReporteDaca();



	    	}	

	    	return $idPresupuesto;
	    }else{
	        return "ERROR";
	    }

	}

	function saveFormularioSolicitud_anterior($data){
		$array  = array();

		$nregistro = $data->nregistro;
		$idPresupuesto = $data->idPresupuesto;
		$procedimiento = $data->procedimiento;
		$estado = $data->estado;
		$fechaGasto = $data->fechaGasto;
		$anio = substr($fechaGasto, 0, 4);
		$mes = substr($fechaGasto, 5, 2);
		$fechaRegistro = $data->fechaRegistro;

		$direccion = "DACA";
		$area = $data->area;
		$chr_string_id = $data->string;
		$chr_categoria_id = $data->categoria;
		$proyecto = $data->proyecto;
		$comentario = $data->comentario; //COMENTARIO

		$ndocumento = $data->ndocumento;
		$fechafactura = $data->fechafactura;	
		$proveedor = $data->proveedor;		
		$colaboradores = $data->colaboradores;

		$actividad = $data->actividad;
		$descripcion = $data->descripcion;

		$monto = $data->monto;
		$moneda = $data->moneda;
		$tipocambio = $data->tipocambio;
		$total = $data->total;

		$usuario = $data->usuario;

		$idFormato = $data->idFormato;
		$nformato = $data->nformato;

		if($moneda == "SOLES"){
			$total = $monto;
		}

		if($mes == "01" || $mes == "02" || $mes == "03"){
			$trimestre = 1;
		}
		if($mes == "04" || $mes == "05" || $mes == "06"){
			$trimestre = 2;
		}
		if($mes == "07" || $mes == "08" || $mes == "09"){
			$trimestre = 3;
		}
		if($mes == "10" || $mes == "11" || $mes == "12"){
			$trimestre = 4;
		}

		$tipo = $this->getDato("tipo","presupuesto_ingreso","area = '$area' limit 1");
		$string = $this->getDato("string","presupuesto_ingreso","chr_string_id = '$chr_string_id' limit 1");
		$categoria = $this->getDato("categoria","presupuesto_ingreso","chr_categoria_id = '$chr_categoria_id' limit 1");

		$oep = strpos($ndocumento, "OEP");

		if($chr_categoria_id == "OEP"){
			$proveedor = $ndocumento;
		}

		if($procedimiento == "GUARDAR"){

			$sql_nuevoregistro = "INSERT INTO presupuesto_gasto (direccion,tipo,area,chr_string_id,string,chr_categoria_id,categoria,monto,moneda,tipocambio,total,anio,mes,trimestre,actividad,descripcion,documento,fecha_documento,proveedor,proyecto,comentario,estado,fechaGasto,fechaRegistro,usuario,idFormato,nformato)
					VALUES ('$direccion','$tipo','$area','$chr_string_id','$string','$chr_categoria_id','$categoria','$monto','$moneda','$tipocambio','$total','$anio','$mes','$trimestre','$actividad','$descripcion','$ndocumento','$fechafactura','$proveedor','$proyecto','$comentario','$estado','$fechaGasto','$fechaRegistro','$usuario','$idFormato','$nformato')";

			$res_nuevoregistro=$this->db->query($sql_nuevoregistro);
			$idPresupuesto = $this->db->insert_id;

		}else{

			$sql_actualizacion = "UPDATE presupuesto_gasto SET
					area = '$area',
					chr_string_id = '$chr_string_id',
					string = '$string',
					chr_categoria_id = '$chr_categoria_id',
					categoria = '$categoria',
					monto = '$monto',
					moneda = '$moneda',
					tipocambio = '$tipocambio',
					total = '$total',
					anio = '$anio',
					mes = '$mes',
					trimestre = '$trimestre',
					actividad = '$actividad',
					descripcion = '$descripcion',
					documento = '$ndocumento',
					fecha_documento = '$fechafactura',
					proveedor = '$proveedor',
					proyecto = '$proyecto',
					comentario = '$comentario',
					estado = '$estado',
					fechaGasto = '$fechaGasto',
					fechaRegistro = '$fechaRegistro',
					usuario = '$usuario',
					idFormato = '$idFormato',
					nformato = '$nformato'
					WHERE idPresupuesto = '$idPresupuesto'";

			$res_actualizacion=$this->db->query($sql_actualizacion);

		}

	    if($res_nuevoregistro OR $res_actualizacion){


	    	if($estado == "POR APROBAR"){


				$sqlRegistro = "UPDATE presupuesto_gasto SET idFormato = '$idFormato', nformato = '$nformato'
							WHERE idPresupuesto = $idPresupuesto";
				$resRegistro=$this->db->query($sqlRegistro);


				//CUANDO ES UNA ORDEN DE COMPRA O PROVISION
				if($idFormato == "OCA" || $idFormato == "PRO"){
					$resSeguimientoVacio = $this->getVariosResultados("idSeguimiento","seguimiento","idPresupuesto = $idPresupuesto AND aprobador = 'mcortes' AND estado = 'P' AND idFormato = '$idFormato'");

					if(count($resSeguimientoVacio) > 0){
						foreach ($resSeguimientoVacio as $seguimiento){
							$sqlEliminarSOCASPRO = "DELETE FROM seguimiento WHERE idSeguimiento = '$seguimiento->idSeguimiento'";
							$resEliminarSOCASPRO = $this->db->query($sqlEliminarSOCASPRO);
						}
					}

				}

				//CUANDO ES UN PEDIDO INTERNO O ACTA
				if($idFormato == "PIN" || $idFormato == "ACT"){

				}
				



				$sqlaprobaciones = "SELECT * FROM formato_aprobaciones WHERE idFormato = '$idFormato'";
				$resAprobaciones = $this->db->get_results($sqlaprobaciones);
				$idSeguimiento = date("YmdHis");
				$fecha = date("Y-m-j");

				if(count($resRegistro) > 0){
					foreach ($resAprobaciones as $aprobaciones) {
						$sqlSeguimiento="	INSERT INTO seguimiento (idSeguimiento,idPresupuesto,idFormato,aprobacion,orden,fechaAprobacion,aprobador,estado)
											VALUES ('$idSeguimiento',$idPresupuesto,'$idFormato',".$aprobaciones->aprobacion.",".$aprobaciones->orden.",'$fecha','".$aprobaciones->aprobador."','".$aprobaciones->estado."')";
			
						$resSeguimiento=$this->db->query($sqlSeguimiento);
					}
				}


				//$this->crearBaseReporteDaca();



	    	}	

	    	return $idPresupuesto;
	    }else{
	        return "ERROR";
	    }

	}

	function activarFormularioSolicitud($data){

		$nregistro = $data->nregistro;
		$procedimiento = $data->procedimiento;
		$estado = $data->estado;

		if($procedimiento == "GUARDAR"){

		}else{

			$sql_actualizacion = "UPDATE presupuesto_gasto SET
					estado = '$estado'
					WHERE idPresupuesto = '$nregistro'";

			$res_actualizacion=$this->db->query($sql_actualizacion);

		}

	    if($res_actualizacion){
	    	//$this->crearBaseReporteDaca();
	    	return $nregistro;
	    }else{
	        return "ERROR";
	    }

	}

	function deleteRegistroSolicitudGastoPresupuesto($dato){
		$sql="DELETE FROM presupuesto_gasto WHERE idPresupuesto = '$dato'";
		$res=$this->db->query($sql);
	    if($res){
	    	//$this->crearBaseReporteDaca_eliminar();
	    	return $res;
	    }else{
	        return "ERROR";
	    }
	}

	function getListaSolicitudGasto($data,$usuario){
		$qarea = "";
		$qstring = "";
		$qcategoria = "";
		$qproyecto = "";
		$qdetalle = "";
		$qproveedor = "";
		$qdocumento = "";
		$qestado = "";
		$qcarrera = "";
		$qampliacion = "";
		$trimestre = "";
		$qformato = "";
		$qnformato = "";

		if($data != 0){
			$area = $data->area; if($area != "0"){ $qarea = " AND A.chr_area_id = '$area'"; };
			$string = $data->string; if($string != "0"){ $qstring = " AND A.chr_string_id = '$string'"; };
			$categoria = $data->categoria; if($categoria != "0"){ $qcategoria = " AND A.chr_categoria_id = '$categoria'"; };
			$proyecto = $data->proyecto; if($proyecto != "0"){ $qproyecto = " AND A.chr_proyecto_id = '$proyecto'"; };
			$detalle = $data->detalle; if($detalle != "0"){ $qdetalle = " AND A.chr_comentario_id = '$detalle'"; };

			$proveedor = $data->proveedor; if($proveedor != "0"){ $qproveedor = " AND A.proveedor = '$proveedor'"; };
			$documento = $data->documento; if($documento != ""){ $qdocumento = " AND A.documento LIKE '%$documento%'"; };
			$estado = $data->estado; if($estado != "0"){ $qestado = " AND A.estado = '$estado'"; };

			$carrera = $data->carrera; if($carrera != "0"){ $qcarrera = " AND CA.chr_carrera_id = '$carrera'"; };
			$ampliacion = $data->ampliacion; if($ampliacion != "0"){ $qampliacion = " AND A.ampliacion = '$ampliacion'"; };

			$formato = $data->formato; if($formato != "0"){ $qformato = " AND A.idFormato = '$formato'"; };
			$nformato = $data->nformato; if($nformato != "0"){ $qnformato = " AND A.nformato = '$nformato'"; };

			$q1 = $data->sq1;
			$q2 = $data->sq2;
			$q3 = $data->sq3;
			$q4 = $data->sq4;

			$anio = $data->anio; if($anio != "0"){ $qanio = " AND A.anio = $anio"; };

			if ($q1 != 0 OR $q2 != 0 OR $q3 != 0 OR $q4 != 0){
				if($q1 != 0){ $q1 = 1; }
				if($q2 != 0){ $q2 = 2; }
				if($q3 != 0){ $q3 = 3; }
				if($q4 != 0){ $q4 = 4; }
				$trimestre = " AND A.trimestre IN ($q1,$q2,$q3,$q4)";
			}
		}

		$careas = $this->getDato("area","usuario_area","usuario = '$usuario' limit 1");

		$sql = "SELECT A.*, B.nombre_razon_social, B.tin, S.chr_string, AR.chr_area, C.chr_categoria, P.chr_proyecto, CO.chr_comentario
				FROM presupuesto_gasto A 
				LEFT JOIN proveedor B ON A.proveedor = B.dni_ruc
				LEFT JOIN presupuesto_area AR ON AR.chr_area_id = A.chr_area_id
				LEFT JOIN presupuesto_string S ON S.chr_string_id = A.chr_string_id
				LEFT JOIN presupuesto_categoria C ON C.chr_categoria_id = A.chr_categoria_id
				LEFT JOIN presupuesto_proyecto P ON P.chr_proyecto_id = A.chr_proyecto_id
				LEFT JOIN presupuesto_comentario CO ON CO.chr_comentario_id = A.chr_comentario_id
				LEFT JOIN carrera CA ON CA.chr_carrera_id = A.chr_carrera_id

				WHERE A.idPresupuesto > 0 $qarea $qstring $qcategoria $qproyecto $qdetalle $qproveedor $qdocumento $qestado $qcarrera $qampliacion $trimestre $qformato $qnformato $qanio AND A.chr_area_id IN ($careas)
				AND A.idFormato NOT IN ('SOCA','SPRO')
				ORDER BY A.idPresupuesto DESC";

		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("direccion","tipo","chr_area","area","chr_string","string","chr_categoria","categoria","actividad","descripcion","documento","proveedor","chr_proyecto","proyecto","chr_comentario","comentario","nombre_razon_social"));
		return $res;
	}

	function getListaSolicitudGasto_anterior($data,$usuario){
		$qarea = "";
		$qstring = "";
		$qcategoria = "";
		$qproyecto = "";
		$qdetalle = "";
		$qproveedor = "";
		$qdocumento = "";
		$qestado = "";
		$trimestre = "";
		$qformato = "";
		$qnformato = "";

		if($data != 0){
			$area = $data->area; if($area != "0"){ $qarea = " AND A.area = '$area'"; };
			$string = $data->string; if($string != "0"){ $qstring = " AND A.chr_string_id = '$string'"; };
			$categoria = $data->categoria; if($categoria != "0"){ $qcategoria = " AND A.chr_categoria_id = '$categoria'"; };
			$proyecto = $data->proyecto; if($proyecto != "0"){ $qproyecto = " AND A.proyecto = '$proyecto'"; };
			$detalle = $data->detalle; if($detalle != "0"){ $qdetalle = " AND A.comentario = '$detalle'"; };

			$proveedor = $data->proveedor; if($proveedor != "0"){ $qproveedor = " AND A.proveedor = '$proveedor'"; };
			$documento = $data->documento; if($documento != ""){ $qdocumento = " AND A.documento LIKE '%$documento%'"; };
			$estado = $data->estado; if($estado != "0"){ $qestado = " AND A.estado = '$estado'"; };

			$formato = $data->formato; if($formato != "0"){ $qformato = " AND A.idFormato = '$formato'"; };
			$nformato = $data->nformato; if($nformato != "0"){ $qnformato = " AND A.nformato = '$nformato'"; };

			$q1 = $data->sq1;
			$q2 = $data->sq2;
			$q3 = $data->sq3;
			$q4 = $data->sq4;

			$anio = $data->anio; if($anio != "0"){ $qanio = " AND A.anio = $anio"; };

			if ($q1 != 0 OR $q2 != 0 OR $q3 != 0 OR $q4 != 0){
				if($q1 != 0){ $q1 = 1; }
				if($q2 != 0){ $q2 = 2; }
				if($q3 != 0){ $q3 = 3; }
				if($q4 != 0){ $q4 = 4; }
				$trimestre = " AND A.trimestre IN ($q1,$q2,$q3,$q4)";
			}
		}

		$careas = $this->getDato("area","usuario_area","usuario = '$usuario' limit 1");

		$sql = "SELECT A.*, B.nombre_razon_social, B.tin FROM presupuesto_gasto A LEFT JOIN proveedor B
				ON A.proveedor = B.dni_ruc
				WHERE A.idPresupuesto > 0 $qarea $qstring $qcategoria $qproyecto $qdetalle $qproveedor $qdocumento $qestado $trimestre $qformato $qnformato $qanio AND A.area IN (SELECT area FROM presupuesto_area WHERE chr_area_id IN ($careas))
				AND A.idFormato NOT IN ('SOCA','SPRO')
				ORDER BY A.idPresupuesto DESC";

		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("direccion","tipo","area","string","categoria","actividad","descripcion","documento","proveedor","proyecto","comentario","nombre_razon_social"));
		return $res;
	}	
		
	function getListaDetalleSolicitudPagoOep($data){
		$idOep = $data->idOep;
		$anio = $data->anio;
		$sql = "SELECT A.*,B.nombre, C.actividad desActividad FROM presupuesto_oep A
				LEFT JOIN colaborador B ON A.dni = B.dni
				LEFT JOIN actividad C ON A.actividad = C.idActividad
				WHERE A.idOepSolicitud = '$idOep'
				ORDER BY id_oep ASC";

		$res = $this->db->get_results($sql);		
		$this->_codificarObjeto($res,array("nombre","sede","observaciones"));
		return $res;	
	}
		// and A.anio = '$anio'
	
	function getCodigoCorrelativoSolicitud(){
		$sql = "SELECT idPresupuesto FROM presupuesto_gasto 
				ORDER BY idPresupuesto DESC LIMIT 1";
		$res = $this->db->get_results($sql);		
		return $res;
	}

















		
		
		
		
		
		
		
		
		
		
		
		
	function getProyectosSolicitud2($data){
				$anio = $data->anio;
				$area = $data->area;
				$cuenta = $data->cuenta;
				$categoria = $data->categoria;
				
				$condicion = "";
		
				if($cuenta == "NODO"){
					$condicion = " AND chr_categoria_id != 'OPE'";
				}else if($cuenta == "OEP"){
					$condicion = " AND chr_categoria_id = 'OPE'";
				}
		
				$sql = "SELECT DISTINCT UPPER(proyecto) proyecto, UPPER(comentario) comentario FROM presupuesto_ingreso
						WHERE anio = $anio AND area = $area $condicion";
				$res = $this->db->get_results($sql);
				$this->_codificarObjeto($res,array("proyecto","comentario"));
				return $res;
		
	}
		
	function getCategoriasSolicitud2($data){
		$anio = $data->anio;
		$area = $data->area;
		$cuenta = $data->cuenta;
		$condicion = "";

		if($cuenta == "NODO"){
			$condicion = " AND chr_categoria_id != 'OEP'";
		}else if($cuenta == "OEP"){
			$condicion = " AND chr_categoria_id = 'OEP'";
		}

		$sql="SELECT DISTINCT chr_categoria_id, UPPER(categoria) categoria FROM presupuesto_ingreso
			  WHERE anio = $anio AND area = $area $condicion";

		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("categoria"));
		return $res;
	}

	function getSaldoCategoriasSolicitud($data){
		$anio = $data->anio;
		$area = $data->area;
		$categoria = $data->categoria;

		$sql="SELECT * FROM presupuesto_ingreso
			  WHERE anio = $anio AND area = $area AND chr_categoria_id = '$categoria'";

		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("categoria","tipo_presupuesto","proyecto","comentario"));
		return $res;
	}


	function getColaboradorOepPresupuesto(){
		$sql="SELECT * FROM colaborador ORDER BY nombre ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("nombre"));
		return $res;	
	}

	function getActividadOepPresupuesto(){
		$sql="SELECT * FROM actividad ORDER BY actividad ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("actividad"));
		return $res;	
	}

	function getProveedorPresupuesto(){
		$sql="SELECT * FROM proveedor ORDER BY nombre_razon_social ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("nombre_razon_social"));
		return $res;
	}

	function getCarreraPresupuesto(){
		$sql="SELECT * FROM carrera ORDER BY chr_carrera ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("chr_carrera"));
		return $res;
	}

	function getSedePresupuesto(){
		$sql="SELECT * FROM sede ORDER BY sede ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("sede"));
		return $res;
	}

	function getTinProveedor($dato){
		$ruc = $dato;
		$sql="SELECT * FROM proveedor WHERE dni_ruc = '$ruc'";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("dni_ruc","nombre_razon_social"));
		return $res;
	}



	function saveColaboradorOepPresupuesto($data){
		$dni = $data->dni;
		$nombre = $data->nombre;
		$estado = $data->estado;
		$proceso = $data->proceso;
		$codigo = $data->codigo;

		if($proceso == "0"){
			$sql="INSERT INTO colaborador (dni,nombre,estado)
				VALUES ('$dni',UPPER('$nombre'),'$estado')";
			$resNuevo=$this->db->query($sql);
		}else if($proceso == "1"){
			$sql="UPDATE colaborador SET dni = '$dni',nombre = UPPER('$nombre'),estado = '$estado'
					WHERE idColaborador = $codigo";
			$resEditado=$this->db->query($sql);
		}

		if($resNuevo){
			return 1;
		}elseif($resEditado){
			return 2;
		}else{
			return 0;
		}

	}

	function saveProveedorPresupuesto($data){
		$dni_ruc = $data->dni;
		$nombre = $data->nombre;
		$tin = $data->tin;
		$estado = $data->estado;
		$proceso = $data->proceso;
		$codigo = $data->codigo;

		if($proceso == "0"){
			$sql="INSERT INTO proveedor (dni_ruc,nombre_razon_social,tin,estado)
				VALUES ('$dni_ruc',UPPER('$nombre'),'$tin','$estado')";
			$resNuevo=$this->db->query($sql);
		}else if($proceso == "1"){
			$sql="UPDATE proveedor SET dni_ruc = '$dni_ruc',nombre_razon_social = UPPER('$nombre'),tin = '$tin',estado = '$estado'
					WHERE idProveedor = $codigo";
			$resEditado=$this->db->query($sql);
		}

		if($resNuevo){
			return 1;
		}elseif($resEditado){
			return 2;
		}else{
			return 0;
		}

	}

	function saveCarreraPresupuesto($data){
		$id = $data->id;
		$codigo = $data->codigo;
		$carrera = $data->carrera;
		$estado = $data->estado;
		$proceso = $data->proceso;
		$codigo = $data->codigo;

		if($proceso == "0"){
			$sql="INSERT INTO carrera (codigo,chr_carrera)
				VALUES ('$codigo',UPPER('".utf8_decode($carrera)."'))";
			$resNuevo=$this->db->query($sql);
		}else if($proceso == "1"){
			$sql="UPDATE carrera SET codigo = '$codigo',chr_carrera = UPPER('".utf8_decode($carrera)."') 
					WHERE chr_carrera_id = $id";
			$resEditado=$this->db->query($sql);
		}

		if($resNuevo){
			return 1;
		}elseif($resEditado){
			return 2;
		}else{
			return 0;
		}

	}

	function saveActividadOepPresupuesto($data){
		$actividad = $data->actividad;
		$proceso = $data->proceso;
		$codigo = $data->codigo;
		$estado = 1;

		if($proceso == "0"){
			$sql="INSERT INTO actividad (actividad,estado)
				VALUES (UPPER('$actividad'),'$estado')";
			$resNuevo=$this->db->query($sql);
		}else if($proceso == "1"){
			$sql="UPDATE actividad SET actividad = UPPER('$actividad'),estado = '$estado'
					WHERE idActividad = $codigo";
			$resEditado=$this->db->query($sql);
		}

		if($resNuevo){
			return 1;
		}elseif($resEditado){
			return 2;
		}else{
			return 0;
		}

	}


	function deleteColaboradorOepPresupuesto($dato){
		$sql="DELETE FROM colaborador WHERE idColaborador = $dato";
		$res=$this->db->query($sql);
		

		if($res){
			return 1;
		}else{
			return 0;
		}

	}

	function deleteProveedorPresupuesto($dato){
		$sql="DELETE FROM proveedor WHERE idProveedor = $dato";
		$res=$this->db->query($sql);
		

		if($res){
			return 1;
		}else{
			return 0;
		}

	}

	function deleteActividadOepPresupuesto($dato){
		$sql="DELETE FROM actividad WHERE idActividad = $dato";
		$res=$this->db->query($sql);
		

		if($res){
			return 1;
		}else{
			return 0;
		}

	}
				

	function savePedidoInterno($data){
		$numeropedidointerno = $data->numeropedidointerno;
		$ructindni = $data->ructindni;
		$grupopagos = $data->grupopagos;
		$personaquesolicita = $data->personaquesolicita;
		$direccion = $data->direccion;
		$moneda = $data->moneda;
		$total = $data->total;
		$fechaemision = $data->fechaemision;
		$anio = SUBSTR($fechaemision,0,4);
		$tipodocumento = $data->tipodocumento;
		$realizadopor = $data->realizadopor;
		$ubicacion = $data->ubicacion;
		$proveedor = $data->proveedor;
		$observacion = $data->observacion;
		$condicion = $data->condicion;
		$firmasolicitante = $data->firmasolicitante;
		$firmavistobueno = $data->firmavistobueno;
		$registro_pago = $data->registro_pago;
		$proceso = $data->proceso;

		if($proceso == "NUEVO"){
			$sql="INSERT INTO f_pin (numero, ructindni,grupo_pagos,persona_solicita,direccion,moneda,total,fecha_emision,tipo_documento,realizado,ubicacion,rucempresa,observacion,condicion,persona_firma,persona_visto_bueno, estado, anio)
				VALUES ('$numeropedidointerno', '$ructindni','$grupopagos','$personaquesolicita','$direccion','$moneda','$total','$fechaemision','$tipodocumento','$realizadopor','$ubicacion','$proveedor','$observacion','$condicion','$firmasolicitante','$firmavistobueno','PENDIENTE',$anio)";

			$resNuevo=$this->db->query($sql);

			if($resNuevo){ //SI SE GUARDA EL REGISTRO DE FORMATO

				$sqlRegistro="UPDATE presupuesto_gasto SET idFormato = 'PIN', nformato = '$numeropedidointerno' /*, estado = 'POR APROBAR'*/
					WHERE idPresupuesto = $registro_pago";
				$resRegistro=$this->db->query($sqlRegistro);

/*
				$sqlaprobaciones="SELECT * FROM formato_aprobaciones WHERE idFormato = 'PIN'";
				$resAprobaciones = $this->db->get_results($sqlaprobaciones);
				$idSeguimiento = date("YmdHis");

			    if(count($resAprobaciones) > 0){
			        foreach ($resAprobaciones as $aprobaciones) {
			            $sqlSeguimiento="	INSERT INTO seguimiento (idSeguimiento,idPresupuesto,idFormato,aprobacion,orden,aprobador,estado)
								VALUES ('$idSeguimiento',$registro_pago,'PIN',".$aprobaciones->aprobacion.",".$aprobaciones->orden.",'".$aprobaciones->aprobador."','".$aprobaciones->estado."')";
						$resSeguimiento=$this->db->query($sqlSeguimiento);
			        }
			    }
*/

			}

			if($resNuevo){
				return 1;
			}else{
				return 0;
			}

		}elseif($proceso == "MODIFICAR"){
			$sqlEditado="	UPDATE f_pin SET 
							grupo_pagos='$grupopagos',
							persona_solicita='$personaquesolicita',
							direccion='$direccion',
							fecha_emision='$fechaemision',
							tipo_documento='$tipodocumento',
							realizado='$realizadopor',
							ubicacion='$ubicacion',
							observacion='$observacion',
							condicion='$condicion',
							persona_firma='$firmasolicitante',
							persona_visto_bueno='$firmavistobueno',
							anio = $anio
							WHERE numero = '$numeropedidointerno'";
			$resEditado=$this->db->query($sqlEditado);

			if($resEditado){
				return 2;
			}else{
				return 0;
			}
		}

	}

	function saveActa($data){
		$numero_acta = $data->numero_acta;
		$ruc_proveedor = $data->ruc_proveedor;
		$orden_de_servicio = $data->orden_de_servicio;
		$moneda = $data->moneda;
		$importe = $data->importe;
		$fecha_de_inicio = $data->fecha_de_inicio;
		$anio = SUBSTR($fecha_de_inicio,0,4);
		$fecha_de_entrega = $data->fecha_de_entrega;
		$solicitante = $data->solicitante;
		$observaciones = $data->observaciones;
		$recibido = $data->recibido;
		$fecha_de_recepcion = $data->fecha_de_recepcion;
		$visto_bueno = $data->visto_bueno;
		$direccion = $data->direccion;
		$registro_pago = $data->registro_pago;
		$proceso = $data->proceso;


		if($proceso == "NUEVO"){
			$sql="INSERT INTO f_act (numero,ruc,orden_servicio,moneda,total,fecha_inicio,fecha_entrega,solicitante,observacion,recibido,fecha_recepcion,persona_visto_bueno,direccion,estado,anio)
				VALUES ('$numero_acta','$ruc_proveedor','$orden_de_servicio','$moneda','$importe','$fecha_de_inicio','$fecha_de_entrega','$solicitante','$observaciones','$recibido','$fecha_de_recepcion','$visto_bueno','$direccion','PENDIENTE',$anio)";
			$resNuevo=$this->db->query($sql);

			if($resNuevo){ //SI SE GUARDA EL REGISTRO DE FORMATO

				$sqlRegistro="UPDATE presupuesto_gasto SET idFormato = 'ACT', nformato = '$numero_acta' /*, estado = 'POR APROBAR'*/
					WHERE idPresupuesto = $registro_pago";
				$resRegistro=$this->db->query($sqlRegistro);

/*
				$sqlaprobaciones="SELECT * FROM formato_aprobaciones WHERE idFormato = 'ACT'";
				$resAprobaciones = $this->db->get_results($sqlaprobaciones);
				$idSeguimiento = date("YmdHis");

			    if(count($resAprobaciones) > 0){
			        foreach ($resAprobaciones as $aprobaciones) {
			            $sqlSeguimiento="	INSERT INTO seguimiento (idSeguimiento,idPresupuesto,idFormato,aprobacion,orden,aprobador,estado)
								VALUES ('$idSeguimiento',$registro_pago,'ACT',".$aprobaciones->aprobacion.",".$aprobaciones->orden.",'".$aprobaciones->aprobador."','".$aprobaciones->estado."')";
						$resSeguimiento=$this->db->query($sqlSeguimiento);
			        }
			    }
*/

			}

			if($resNuevo){
				return 1;
			}else{
				return 0;
			}

		}elseif($proceso == "MODIFICAR"){
			$sqlEditado="	UPDATE f_act SET 
							orden_servicio='$orden_de_servicio',
							fecha_inicio='$fecha_de_inicio',
							fecha_entrega='$fecha_de_entrega',
							solicitante='$solicitante',
							observacion='$observaciones',
							recibido='$recibido',
							fecha_recepcion='$fecha_de_recepcion',
							persona_visto_bueno='$visto_bueno',
							direccion='$direccion',
							anio=$anio
							WHERE numero = '$numero_acta'";
			$resEditado=$this->db->query($sqlEditado);

			if($resEditado){
				return 2;
			}else{
				return 0;
			}
		}

	}






	function getTablaFormatos($dato,$anio){
		$tabla = "";
		if($dato == "ACT"){ 
			$tabla = "f_act"; 
			$sql="	SELECT F.id_act codigo,F.numero,F.fecha_inicio emision,F.ruc rucempresa,P.nombre_razon_social,F.moneda,F.total,F.estado,'ACT' formato
					FROM $tabla F LEFT JOIN proveedor P
					ON F.ruc = P.dni_ruc
					WHERE F.anio = $anio";
			$res = $this->db->get_results($sql);
		}

		if($dato == "PIN"){ 
			$tabla = "f_pin"; 
			$sql="	SELECT F.id_pin codigo,F.numero,F.fecha_emision emision,F.rucempresa,P.nombre_razon_social,F.moneda,F.total,F.estado,'PIN' formato
					FROM $tabla F LEFT JOIN proveedor P
					ON F.rucempresa = P.dni_ruc
					WHERE F.anio = $anio";
			$res = $this->db->get_results($sql);
		}

		if($dato == "OCA"){ 
			$tabla = "f_oca"; 
			$sql="	SELECT F.id_oca codigo,F.numero,F.fechaRegistro emision,F.proveedor,P.nombre_razon_social,F.moneda,F.total,F.estado,'OCA' formato
					FROM $tabla F LEFT JOIN proveedor P
					ON F.proveedor = P.dni_ruc
					WHERE F.estado = 'PENDIENTE' AND F.anio = $anio";
			$res = $this->db->get_results($sql);
		}

		if($dato == "PRO"){ 
			$tabla = "f_pro"; 
			$sql="	SELECT F.id_pro codigo,F.numero,F.fechaRegistro emision,'' proveedor,'' nombre_razon_social,F.moneda,F.total,F.estado,'PRO' formato
					FROM $tabla F
					WHERE F.estado = 'PENDIENTE' AND F.anio = $anio";
			$res = $this->db->get_results($sql);
		}


		return $res;
	}

	function saveAgregarRegistroFormato($data){
		$fecha = date("Y-m-j");
		$formato = $data->formato;
		$numero = $data->numero;
		$registro = $data->registro;
		$nanterior = $data->nanterior;

		$sqlRegistro="UPDATE presupuesto_gasto SET idFormato = '$formato', nformato = '$numero' /*, estado = 'POR APROBAR'*/
					WHERE idPresupuesto = $registro";
		$resRegistro=$this->db->query($sqlRegistro);

/*
		if($nanterior != ""){
			$sqlEliminar="DELETE FROM seguimiento WHERE idPresupuesto = $registro";
			$resEliminar=$this->db->query($sqlEliminar);		
		}

		$sqlaprobaciones="SELECT * FROM formato_aprobaciones WHERE idFormato = '$formato'";
		$resAprobaciones = $this->db->get_results($sqlaprobaciones);
		$idSeguimiento = date("YmdHis");

		if(count($resRegistro) > 0){
			foreach ($resAprobaciones as $aprobaciones) {
				$sqlSeguimiento="	INSERT INTO seguimiento (idSeguimiento,idPresupuesto,idFormato,aprobacion,orden,fechaAprobacion,aprobador,estado)
									VALUES ('$idSeguimiento',$registro,'$formato',".$aprobaciones->aprobacion.",".$aprobaciones->orden.",'$fecha','".$aprobaciones->aprobador."','".$aprobaciones->estado."')";
				$resSeguimiento=$this->db->query($sqlSeguimiento);
			}
		}
*/

	}

	function removeAgregarRegistroFormato($registro){
		$sqlModificar="UPDATE presupuesto_gasto SET idFormato = (NULL), nformato = (NULL), estado = 'INGRESADO' WHERE idPresupuesto = $registro";
		$resModificar=$this->db->query($sqlModificar);
		
		$sqlEliminar="DELETE FROM seguimiento WHERE idPresupuesto = $registro";
		$resEliminar=$this->db->query($sqlEliminar);

		return $sqlEliminar;
	}

	function getFormularioPedidoInterno($npedido){
		$sqlPIN="	SELECT F.*,P.nombre_razon_social FROM f_pin F LEFT JOIN proveedor P
					ON F.rucempresa = P.dni_ruc
					WHERE F.numero = '$npedido'";
		$resPIN = $this->db->get_results($sqlPIN);
		$this->_codificarObjeto($resPIN,array("numero","ructindni","grupo_pagos","persona_solicita","direccion","moneda","tipo_documento","realizado","ubicacion","rucempresa","nombre_razon_social","observacion","condicion","persona_firma","persona_visto_bueno"));

		$sqlRegistrosPIN="	SELECT idPresupuesto,chr_categoria_id,chr_string_id,monto,total,descripcion,documento,fecha_documento FROM presupuesto_gasto
							WHERE nFormato = '$npedido'";
		$resRegistrosPIN = $this->db->get_results($sqlRegistrosPIN);
		$this->_codificarObjeto($resRegistrosPIN,array("descripcion","documento"));

		$resultado = new stdClass();
        $resultado->formato = $resPIN;
        $resultado->registros = $resRegistrosPIN;

        return $resultado;

	}

	function getFormularioActa($nacta){
		$sqlACT="	SELECT F.*,P.nombre_razon_social FROM f_act F LEFT JOIN proveedor P
					ON F.ruc = P.dni_ruc
					WHERE F.numero = '$nacta'";
		$resACT = $this->db->get_results($sqlACT);
		$this->_codificarObjeto($resACT,array("numero","ruc","solicitante","observacion","recibido","persona_visto_bueno","direccion"));

        return $resACT;

	}


	function deletePedidoInternoCompleto($npedido){
		$sqlEliminarPedido="DELETE FROM f_pin WHERE numero = '$npedido'";
		$resEliminarPedido=$this->db->query($sqlEliminarPedido);

		$sqlEliminarSeguimiento="DELETE FROM seguimiento WHERE idPresupuesto IN ( SELECT idPresupuesto FROM presupuesto_gasto WHERE nformato = '$npedido' )";
		$resEliminarSeguimiento=$this->db->query($sqlEliminarSeguimiento);

		$sqlModificar="UPDATE presupuesto_gasto SET idFormato = (NULL), nformato = (NULL), estado = 'INGRESADO' WHERE nformato = '$npedido'";
		$resModificar=$this->db->query($sqlModificar);

		return $sqlModificar; //MENSAJE SI SE HAN MODIFICADO LOS REGISTROS RELACIONADOS
	}

	function deleteActaCompleto($nacta){
		$sqlEliminarActa="DELETE FROM f_act WHERE numero = '$nacta'";
		$resEliminarActa=$this->db->query($sqlEliminarActa);

		$sqlEliminarSeguimiento="DELETE FROM seguimiento WHERE idPresupuesto IN ( SELECT idPresupuesto FROM presupuesto_gasto WHERE nformato = '$nacta' )";
		$resEliminarSeguimiento=$this->db->query($sqlEliminarSeguimiento);

		$sqlModificar="UPDATE presupuesto_gasto SET idFormato = (NULL), nformato = (NULL), estado = 'INGRESADO' WHERE nformato = '$nacta'";
		$resModificar=$this->db->query($sqlModificar);

		return $sqlModificar; //MENSAJE SI SE HAN MODIFICADO LOS REGISTROS RELACIONADOS
	}

	function getListaFormato($formato,$anio){
		$formatoocapro = "";
		$nformato = $formato;
		if($formato == "SOCA" || $formato == "SPRO"){
			$formato = SUBSTR($formato,1,3);
			$formatoocapro = "SUBSTR(numero,5,10)";
		}
		$sql="	SELECT '$nformato' AS idFormato,$formatoocapro numero FROM f_".$formato."
				WHERE id_".$formato." > 0 AND estado = 'PENDIENTE' AND anio = $anio
				ORDER BY id_".$formato." ASC";

		$res = $this->db->get_results($sql);
		return $res;
	}


	function getExcelPedidoInterno($npedido){
		$hora = date("h:i:s");
    	$fecha = date("Y-m-j");
    	$monto_total = 0.00;

		$sqlPIN="	SELECT F.*,P.nombre_razon_social,U.nombre_completo FROM f_pin F LEFT JOIN proveedor P
					ON F.rucempresa = P.dni_ruc LEFT JOIN usuario U
					ON F.persona_firma = U.usuario
					WHERE F.numero = '$npedido'";
		$resPIN = $this->db->get_results($sqlPIN);
		$this->_codificarObjeto($resPIN,array("numero","ructindni","grupo_pagos","persona_solicita","direccion","moneda","tipo_documento","realizado","ubicacion","rucempresa","nombre_razon_social","nombre_completo","observacion","condicion","persona_firma","persona_visto_bueno"));

		$sqlRegistrosPIN="	SELECT chr_categoria_id,chr_string_id,monto,total,descripcion,documento,fecha_documento FROM presupuesto_gasto
							WHERE nFormato = '$npedido'";
		$resRegistrosPIN = $this->db->get_results($sqlRegistrosPIN);
		$this->_codificarObjeto($resRegistrosPIN,array("descripcion","documento"));

		if($resPIN && $resRegistrosPIN){
							
			date_default_timezone_set('America/Mexico_City');

			if (PHP_SAPI == 'cli')
				die('Este archivo solo se puede ver desde un navegador web');

			/** Se agrega la libreria PHPExcel */
			//require_once 'lib/PHPExcel/PHPExcel.php';

			// Se crea el objeto PHPExcel
			$objPHPExcel = new PHPExcel();

			//Seleccionar Formato
			$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			$objPHPExcel = $objReader->load("formatos/pedido_interno.xlsx");

			// Se asignan las propiedades del libro
			/*$objPHPExcel->getProperties()->setCreator("CSPSISTEMAS") //Autor
								 ->setLastModifiedBy("CSPSISTEMAS") //Ultimo usuario que lo modificó
								 ->setTitle("Reporte Excel con PHP y MySQL")
								 ->setSubject("Reporte Excel con PHP y MySQL")
								 ->setDescription("Reporte de alumnos")
								 ->setKeywords("reporte alumnos carreras")
								 ->setCategory("Reporte excel"); */


			$tituloReporte = "REPORTE DE DIFERENCIAS POR SKU - LIMITE ".$limite;
			$titulosColumnas = array('N','SKU','BARRA','DESCRIPCION','STOCK','CONTEO','DIF UNIT','DIF SOL');
			
			$objPHPExcel->setActiveSheetIndex(0);
	        		    /*->mergeCells('A1:H1');*/
							
			// Se agregan los titulos del reporte
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('I1',$resPIN[0]->numero)
						->setCellValue('I4',$resPIN[0]->ructindni)
						->setCellValue('I6',$resPIN[0]->grupo_pagos)
						->setCellValue('F9',$resPIN[0]->persona_solicita)
						->setCellValue('F10',$resPIN[0]->direccion)
						->setCellValue('H10',$resPIN[0]->moneda)
						->setCellValue('F11',$resPIN[0]->fecha_emision)
						->setCellValue('I11',$resPIN[0]->tipo_documento)
						->setCellValue('F12',$resPIN[0]->realizado)
						->setCellValue('I12',$resPIN[0]->ubicacion)
						->setCellValue('F13',$resPIN[0]->nombre_razon_social)
						->setCellValue('F14',$resPIN[0]->observacion)
						->setCellValue('G51',$resPIN[0]->condicion)
						->setCellValue('B51',$resPIN[0]->nombre_completo)
						->setCellValue('B57',$resPIN[0]->persona_visto_bueno);
			
			//Se agregan los datos de los alumnos
			$i = 21;

			for( $x = 0; $x < count($resRegistrosPIN); $x++)
			{
				$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValue('B'.$i,  $resRegistrosPIN[$x]->chr_categoria_id)
	        		    ->setCellValue('C'.$i,  $resRegistrosPIN[$x]->chr_string_id)
	        		    ->setCellValue('F'.$i,  $resRegistrosPIN[$x]->monto)
	        		    ->setCellValue('G'.$i,  $resRegistrosPIN[$x]->descripcion)
	        		    ->setCellValue('H'.$i,  $resPIN[0]->nombre_razon_social)
	        		    ->setCellValue('I'.$i,  $resRegistrosPIN[$x]->documento)
	        		    ->setCellValue('J'.$i,  $resRegistrosPIN[$x]->fecha_documento);
						$i++;

				$monto_total = $monto_total + $resRegistrosPIN[$x]->monto;
			}

			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('I10',$monto_total)
						->setCellValue('F50',$monto_total);
			
			/*$estiloTituloReporte = array(
	        	'font' => array(
		        	'name'      => 'Verdana',
	    	        'bold'      => true,
	        	    'italic'    => false,
	                'strike'    => false,
	               	'size' =>16,
		            	'color'     => array(
	    	            	'rgb' => 'FFFFFF'
	        	       	)
	            ),
		        'fill' => array(
					'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
					'color'	=> array('argb' => 'FF220835')
				),
	            'borders' => array(
	               	'allborders' => array(
	                	'style' => PHPExcel_Style_Border::BORDER_NONE                    
	               	)
	            ), 
	            'alignment' =>  array(
	        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        			'rotation'   => 0,
	        			'wrap'          => TRUE
	    		)
	        );

			$estiloTituloColumnas = array(
	            'font' => array(
	                'name'      => 'Arial',
	                'bold'      => true,                          
	                'color'     => array(
	                    'rgb' => 'FFFFFF'
	                )
	            ),
	            'fill' 	=> array(
					'type'		=> PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
					'rotation'   => 90,
	        		'startcolor' => array(
	            		'rgb' => 'c47cf2'
	        		),
	        		'endcolor'   => array(
	            		'argb' => 'FF431a5d'
	        		)
				),
	            'borders' => array(
	            	'top'     => array(
	                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	                    'color' => array(
	                        'rgb' => '143860'
	                    )
	                ),
	                'bottom'     => array(
	                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	                    'color' => array(
	                        'rgb' => '143860'
	                    )
	                )
	            ),
				'alignment' =>  array(
	        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        			'wrap'          => TRUE
	    		));
				
			$estiloInformacion = new PHPExcel_Style();
			$estiloInformacion->applyFromArray(
				array(
	           		'font' => array(
	               	'name'      => 'Arial',               
	               	'color'     => array(
	                   	'rgb' => '000000'
	               	)
	           	),
	           	'fill' 	=> array(
					'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
					'color'		=> array('argb' => 'FFd9b7f4')
				),
	           	'borders' => array(
	               	'left'     => array(
	                   	'style' => PHPExcel_Style_Border::BORDER_THIN ,
		                'color' => array(
	    	            	'rgb' => '3a2a47'
	                   	)
	               	)             
	           	)
	        ));*/
			
			$estiloColorAmarillo =  array(
			        	'fill' => array(
			            	'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            	'color' => array('rgb' => 'FFF933')
			        	)
			);

			$estiloColorPlomo =  array(
			        	'fill' => array(
			            	'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            	'color' => array('rgb' => 'E5E2E2')
			        	)
			);

			$estiloColorRosa =  array(
			        	'fill' => array(
			            	'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            	'color' => array('rgb' => 'FFCCCC')
			        	)
			);

			$objPHPExcel->getActiveSheet()->getStyle('B17:E19')->applyFromArray($estiloColorAmarillo);
			$objPHPExcel->getActiveSheet()->getStyle('B16:E16')->applyFromArray($estiloColorPlomo);
			$objPHPExcel->getActiveSheet()->getStyle('H4:H7')->applyFromArray($estiloColorRosa);
			$objPHPExcel->getActiveSheet()->getStyle('H11:H12')->applyFromArray($estiloColorRosa);
			//$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->applyFromArray($estiloTituloColumnas);		
			//$objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:R".($i-1));
					
			//for($i = 'A'; $i <= 'H'; $i++){
			//	$objPHPExcel->setActiveSheetIndex(0)			
			//		->getColumnDimension($i)->setAutoSize(TRUE);
			//}
			
			// Se asigna el nombre a la hoja
			$objPHPExcel->getActiveSheet()->setTitle('Reporte');

			// Se activa la hoja para que sea la que se muestre cuando el archivo se abre
			$objPHPExcel->setActiveSheetIndex(0);
			// Inmovilizar paneles 
			//$objPHPExcel->getActiveSheet(0)->freezePane('A4');
			/*$objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,4);*/

			// Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="pedidoInterno_'.$npedido.'.xlsx"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('reportes/pedidoInterno_'.$npedido.'.xlsx');

			return "ok";
			
		}
		else{
			print_r('No hay resultados para mostrar');
		}
	}

	function getExcelActa($nacta){
		$hora = date("h:i:s");
    	$fecha = date("Y-m-j");
    	$monto_total = 0.00;

		$sqlACT="	SELECT F.*,P.nombre_razon_social
					FROM f_act F LEFT JOIN proveedor P
					ON F.ruc = P.dni_ruc
					WHERE F.numero = '$nacta'";
		$resACT = $this->db->get_results($sqlACT);
		$this->_codificarObjeto($resACT,array("numero","ruc","solicitante","observacion","recibido","persona_visto_bueno","direccion"));

		if($resACT){
							
			date_default_timezone_set('America/Mexico_City');

			if (PHP_SAPI == 'cli')
				die('Este archivo solo se puede ver desde un navegador web');


			$objPHPExcel = new PHPExcel();

			$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			$objPHPExcel = $objReader->load("formatos/acta.xlsx");

			
			$objPHPExcel->setActiveSheetIndex(0);


	        if($resACT[0]->moneda == "SOLES"){
	        	$monto_moneda = "S/. ".number_format($resACT[0]->total,2);
	        }else{
	        	$monto_moneda = "$ ".number_format($resACT[0]->total,2);
	        }


			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('D1',$resACT[0]->numero)
						->setCellValue('B8',$resACT[0]->nombre_razon_social)
						->setCellValue('B9',$resACT[0]->ruc)
						->setCellValue('B10',$resACT[0]->orden_servicio)
						->setCellValue('B11',$monto_moneda)
						->setCellValue('B12',$resACT[0]->fecha_inicio)
						->setCellValue('B13',$resACT[0]->fecha_entrega)
						->setCellValue('B14',$resACT[0]->solicitante)
						->setCellValue('A17',$resACT[0]->observacion)
						->setCellValue('B21',$resACT[0]->recibido)
						->setCellValue('B22',$resACT[0]->fecha_recepcion)
						->setCellValue('D28',"V°B° ".$resACT[0]->persona_visto_bueno)
						->setCellValue('D29',$resACT[0]->direccion);
			
			$i = 21;


			$objPHPExcel->getActiveSheet()->setTitle('Reporte');

			$objPHPExcel->setActiveSheetIndex(0);

			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="acta_'.$nacta.'.xlsx"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('reportes/acta_'.$nacta.'.xlsx');

			return "ok";
			
		}
		else{
			print_r('No hay resultados para mostrar');
		}
	}



	function getNumeroAutogeneradoPedidoInterno(){
		$sql="	SELECT id_pin FROM f_pin
				ORDER BY id_pin DESC LIMIT 1";
		$res = $this->db->get_var($sql);

		return $res;
	}

	function consultarSolicitudSaldoPresupuesto($data){

		$fecha = $data->fecha;
		$area = $data->area;
		$string = $data->string;
		$categoria = $data->categoria;
		$proyecto = $data->proyecto;
		$comentario = $data->comentario;
		$anio = $data->anio;
		$mes = substr($fecha, 5, 2); //$data->mes;


		$mes_anio = ["","ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"];
		$trimestre = [0,1,1,1,2,2,2,3,3,3,4,4,4]; //arreglo de trimestre por mes
		//$trimestre_mes = ['0','01','02','03','04','05','06','07','08','09','10','11','12'];
		
		$q = $trimestre[(int)$mes];

		if((int)$mes == 1 || (int)$mes == 2 || (int)$mes == 3){ $trimestre = "1,2,3"; }
		if((int)$mes == 4 || (int)$mes == 5 || (int)$mes == 6){ $trimestre = "4,5,6"; }
		if((int)$mes == 7 || (int)$mes == 8 || (int)$mes == 9){ $trimestre = "7,8,9"; }
		if((int)$mes == 10 || (int)$mes == 11 || (int)$mes == 12){ $trimestre = "10,11,12"; }

		//$q_mes = $trimestre_mes[(int)$mes];
                //$where = "area = '$area' AND chr_string_id = '$string' AND chr_categoria_id = '$categoria' AND proyecto = '$proyecto' AND comentario = '$comentario' AND mes = '$mes' AND anio = '$anio'";
                $where = " chr_string_id = '$string' AND chr_categoria_id = '$categoria' AND chr_area_id = '$area' AND chr_proyecto_id = '$proyecto' AND chr_comentario_id = '$comentario'";
                $date=" AND mes = '$mes' AND anio = '$anio' ";
                $query = $where.$date;

		$presupuesto_m = $this->getDato("SUM(total)","presupuesto_ingreso_act_var",$query);

                //$where="area = '$area' AND chr_string_id = '$string' AND chr_categoria_id = '$categoria' AND proyecto = '$proyecto' AND comentario = '$comentario' AND trimestre = '$q' AND anio = '$anio'";
                $date=" AND trimestre = '$q' AND anio = '$anio'";
                $query = $where.$date;
		$presupuesto_q = $this->getDato("SUM(total) ","presupuesto_ingreso_act_var",$query);

                //$where="area = '$area' AND chr_string_id = '$string' AND chr_categoria_id = '$categoria' AND proyecto = '$proyecto' AND comentario = '$comentario' AND anio = '$anio'";
                $date=" AND anio = '$anio'";
                $query = $where.$date;
		$presupuesto_y = $this->getDato("SUM(total) ","presupuesto_ingreso_act_var",$query);

                //$where="area = '$area' AND chr_string_id = '$string' AND chr_categoria_id = '$categoria' AND proyecto = '$proyecto' AND comentario = '$comentario' AND mes = '$mes' AND estado NOT IN ('CANCELADO','RECHAZADO','INGRESADO')";
                $date=" AND mes = '$mes' AND estado NOT IN ('CANCELADO','RECHAZADO','INGRESADO') ";
                $query = $where.$date;
		$ejecutado_m = $this->getDato("SUM(total)","presupuesto_gasto",$query);

                //$where="area = '$area' AND chr_string_id = '$string' AND chr_categoria_id = '$categoria' AND proyecto = '$proyecto' AND comentario = '$comentario' AND trimestre = '$q' AND estado NOT IN ('CANCELADO','RECHAZADO','INGRESADO')";
                $date=" AND trimestre = '$q' AND estado NOT IN ('CANCELADO','RECHAZADO','INGRESADO') ";
                $query = $where.$date;
		$ejecutado_q = $this->getDato("SUM(total)","presupuesto_gasto",$query);

                //$where="area = '$area' AND chr_string_id = '$string' AND chr_categoria_id = '$categoria' AND proyecto = '$proyecto' AND comentario = '$comentario' AND estado NOT IN ('CANCELADO','RECHAZADO','INGRESADO')";
                $date=" AND estado NOT IN ('CANCELADO','RECHAZADO','INGRESADO') AND anio='".$anio."' ";
                $query = $where.$date;
		$ejecutado_y = $this->getDato("SUM(total)","presupuesto_gasto",$query);
        

                
                //$where="chr_area = '$area' AND chr_string = '$string' AND chr_categoria_id = '$categoria' AND chr_proyecto = '$proyecto' AND chr_comentario = '$comentario' AND chr_mes = '$mes' AND chr_year = '$anio'";
                $where=" chr_string = '$string' AND chr_categoria_id = '$categoria' AND chr_area_id = '$area' AND chr_proyecto_id = '$proyecto' AND chr_comentario_id = '$comentario'";
                $date=" AND chr_mes = '$mes' AND chr_year = '$anio' ";
                $query = $where.$date;
		$transferencia_pos_m = $this->getDato("SUM(dec_total)","mov_movimiento",$query);
                //$where="chr_area = '$area' AND chr_string = '$string' AND chr_categoria_id = '$categoria' AND chr_proyecto = '$proyecto' AND chr_comentario = '$comentario' AND chr_mes IN ($trimestre) AND chr_year = '$anio'";
                $date=" AND chr_mes IN ($trimestre) AND chr_year = '$anio' ";
                $query = $where.$date;
		$transferencia_pos_q = $this->getDato("SUM(dec_total)","mov_movimiento",$query);
                //$where="chr_area = '$area' AND chr_string = '$string' AND chr_categoria_id = '$categoria' AND chr_proyecto = '$proyecto' AND chr_comentario = '$comentario' AND chr_year = '$anio'";
                $date=" AND chr_year = '$anio' ";
                $query = $where.$date;
		$transferencia_pos_y = $this->getDato("SUM(dec_total)","mov_movimiento",$query);

                //$where="chr_area = '$area' AND chr_string = '$string' AND chr_categoria_id = '$categoria' AND chr_proyecto = '$proyecto' AND chr_comentario = '$comentario' AND chr_mes = '$mes' AND chr_year = '$anio'";
                $date=" AND chr_mes = '$mes' AND chr_year = '$anio' ";
                $query = $where.$date;
		$transferencia_res_m = $this->getDato("SUM(dec_subtotal)","mov_movimiento_detalle",$query);
                //$where="chr_area = '$area' AND chr_string = '$string' AND chr_categoria_id = '$categoria' AND chr_proyecto = '$proyecto' AND chr_comentario = '$comentario' AND chr_mes IN ($trimestre) AND chr_year = '$anio'";
                $date=" AND chr_mes IN ($trimestre) AND chr_year = '$anio' ";
                $query = $where.$date;
		$transferencia_res_q = $this->getDato("SUM(dec_subtotal)","mov_movimiento_detalle",$query);
                //$where="chr_area = '$area' AND chr_string = '$string' AND chr_categoria_id = '$categoria' AND chr_proyecto = '$proyecto' AND chr_comentario = '$comentario' AND chr_year = '$anio'";
                $date=" AND chr_year = '$anio' ";
                $query = $where.$date;
		$transferencia_res_y = $this->getDato("SUM(dec_subtotal)","mov_movimiento_detalle",$query);

		$resultado = new stdClass();
        $resultado->saldo_m = $presupuesto_m - $ejecutado_m + $transferencia_pos_m - $transferencia_res_m;
        $resultado->saldo_q = $presupuesto_q - $ejecutado_q + $transferencia_pos_q - $transferencia_res_q;
        $resultado->saldo_y = $presupuesto_y - $ejecutado_y + $transferencia_pos_y - $transferencia_res_y;
        $resultado->mes = $mes_anio[(int)$mes];
        $resultado->trimestre = $q;

        return $resultado;


	}

	function consultarSolicitudSaldoPresupuesto_anterior($data){
		
		$fecha = $data->fecha;
		$area = $data->area;
		$string = $data->string;
		$categoria = $data->categoria;
		$proyecto = $data->proyecto;
		$comentario = $data->comentario;
		$mes_anio = ["","ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"];
		$trimestre = [0,1,1,1,2,2,2,3,3,3,4,4,4];
		//$trimestre_mes = ['0','01','02','03','04','05','06','07','08','09','10','11','12'];
		
		$anio = substr($fecha, 0, 4);

		$mes = substr($fecha, 5, 2);
		$q = $trimestre[(int)$mes];

		if((int)$mes == 1 || (int)$mes == 2 || (int)$mes == 3){ $trimestre = "1,2,3"; }
		if((int)$mes == 4 || (int)$mes == 5 || (int)$mes == 6){ $trimestre = "4,5,6"; }
		if((int)$mes == 7 || (int)$mes == 8 || (int)$mes == 9){ $trimestre = "7,8,9"; }
		if((int)$mes == 10 || (int)$mes == 11 || (int)$mes == 12){ $trimestre = "10,11,12"; }

		//$q_mes = $trimestre_mes[(int)$mes];
                //$where = "area = '$area' AND chr_string_id = '$string' AND chr_categoria_id = '$categoria' AND proyecto = '$proyecto' AND comentario = '$comentario' AND mes = '$mes' AND anio = '$anio'";
                $where = " chr_string_id = '$string' AND chr_categoria_id = '$categoria' ";
                $where.=" AND  (
                        CONVERT(
                          CAST(
                            CONVERT(area USING latin1) AS CHAR CHARSET BINARY
                          ) USING utf8
                        ) = CONVERT('".$area."' USING utf8)
                    ) ";
                $where.=" AND  (
                        CONVERT(
                          CAST(
                            CONVERT(proyecto USING latin1) AS CHAR CHARSET BINARY
                          ) USING utf8
                        ) = CONVERT('".$proyecto."' USING utf8)
                    ) ";
                $where.=" AND  (
                        CONVERT(
                          CAST(
                            CONVERT(comentario USING latin1) AS CHAR CHARSET BINARY
                          ) USING utf8
                        ) = CONVERT('".$comentario."' USING utf8)
                    ) ";
                $date=" AND mes = '$mes' AND anio = '$anio' ";
                $query = $where.$date;
		$presupuesto_m = $this->getDato("SUM(total)","presupuesto_ingreso_act",$query);
                //$where="area = '$area' AND chr_string_id = '$string' AND chr_categoria_id = '$categoria' AND proyecto = '$proyecto' AND comentario = '$comentario' AND trimestre = '$q' AND anio = '$anio'";
                $date=" AND trimestre = '$q' AND anio = '$anio'";
                $query = $where.$date;
		$presupuesto_q = $this->getDato("SUM(total) ","presupuesto_ingreso_act",$query);
                //$where="area = '$area' AND chr_string_id = '$string' AND chr_categoria_id = '$categoria' AND proyecto = '$proyecto' AND comentario = '$comentario' AND anio = '$anio'";
                $date=" AND anio = '$anio'";
                $query = $where.$date;
		$presupuesto_y = $this->getDato("SUM(total) ","presupuesto_ingreso_act",$query);

                //$where="area = '$area' AND chr_string_id = '$string' AND chr_categoria_id = '$categoria' AND proyecto = '$proyecto' AND comentario = '$comentario' AND mes = '$mes' AND estado NOT IN ('CANCELADO','RECHAZADO','INGRESADO')";
                $date=" AND mes = '$mes' AND estado NOT IN ('CANCELADO','RECHAZADO','INGRESADO') ";
                $query = $where.$date;
		$ejecutado_m = $this->getDato("SUM(total)","presupuesto_gasto",$query);
                //$where="area = '$area' AND chr_string_id = '$string' AND chr_categoria_id = '$categoria' AND proyecto = '$proyecto' AND comentario = '$comentario' AND trimestre = '$q' AND estado NOT IN ('CANCELADO','RECHAZADO','INGRESADO')";
                $date=" AND trimestre = '$q' AND estado NOT IN ('CANCELADO','RECHAZADO','INGRESADO') ";
                $query = $where.$date;
		$ejecutado_q = $this->getDato("SUM(total)","presupuesto_gasto",$query);
                //$where="area = '$area' AND chr_string_id = '$string' AND chr_categoria_id = '$categoria' AND proyecto = '$proyecto' AND comentario = '$comentario' AND estado NOT IN ('CANCELADO','RECHAZADO','INGRESADO')";
                $date=" AND estado NOT IN ('CANCELADO','RECHAZADO','INGRESADO') AND anio='".$anio."' ";
                $query = $where.$date;
		$ejecutado_y = $this->getDato("SUM(total)","presupuesto_gasto",$query);
                
                
                //$where="chr_area = '$area' AND chr_string = '$string' AND chr_categoria_id = '$categoria' AND chr_proyecto = '$proyecto' AND chr_comentario = '$comentario' AND chr_mes = '$mes' AND chr_year = '$anio'";
                $where=" chr_string = '$string' AND chr_categoria_id = '$categoria' ";
                $where.=" AND  (
                        CONVERT(
                          CAST(
                            CONVERT(chr_area USING latin1) AS CHAR CHARSET BINARY
                          ) USING utf8
                        ) = CONVERT('".$area."' USING utf8)
                    ) ";                
                $where.=" AND  (
                        CONVERT(
                          CAST(
                            CONVERT(chr_proyecto USING latin1) AS CHAR CHARSET BINARY
                          ) USING utf8
                        ) = CONVERT('".$proyecto."' USING utf8)
                    ) ";                
                $where.=" AND  (
                        CONVERT(
                          CAST(
                            CONVERT(chr_comentario USING latin1) AS CHAR CHARSET BINARY
                          ) USING utf8
                        ) = CONVERT('".$comentario."' USING utf8)
                    ) ";
                $date=" AND chr_mes = '$mes' AND chr_year = '$anio' ";
                $query = $where.$date;
		$transferencia_pos_m = $this->getDato("SUM(dec_total)","mov_movimiento",$query);
                //$where="chr_area = '$area' AND chr_string = '$string' AND chr_categoria_id = '$categoria' AND chr_proyecto = '$proyecto' AND chr_comentario = '$comentario' AND chr_mes IN ($trimestre) AND chr_year = '$anio'";
                $date=" AND chr_mes IN ($trimestre) AND chr_year = '$anio' ";
                $query = $where.$date;
		$transferencia_pos_q = $this->getDato("SUM(dec_total)","mov_movimiento",$query);
                //$where="chr_area = '$area' AND chr_string = '$string' AND chr_categoria_id = '$categoria' AND chr_proyecto = '$proyecto' AND chr_comentario = '$comentario' AND chr_year = '$anio'";
                $date=" AND chr_year = '$anio' ";
                $query = $where.$date;
		$transferencia_pos_y = $this->getDato("SUM(dec_total)","mov_movimiento",$query);

                //$where="chr_area = '$area' AND chr_string = '$string' AND chr_categoria_id = '$categoria' AND chr_proyecto = '$proyecto' AND chr_comentario = '$comentario' AND chr_mes = '$mes' AND chr_year = '$anio'";
                $date=" AND chr_mes = '$mes' AND chr_year = '$anio' ";
                $query = $where.$date;
		$transferencia_res_m = $this->getDato("SUM(dec_subtotal)","mov_movimiento_detalle",$query);
                //$where="chr_area = '$area' AND chr_string = '$string' AND chr_categoria_id = '$categoria' AND chr_proyecto = '$proyecto' AND chr_comentario = '$comentario' AND chr_mes IN ($trimestre) AND chr_year = '$anio'";
                $date=" AND chr_mes IN ($trimestre) AND chr_year = '$anio' ";
                $query = $where.$date;
		$transferencia_res_q = $this->getDato("SUM(dec_subtotal)","mov_movimiento_detalle",$query);
                //$where="chr_area = '$area' AND chr_string = '$string' AND chr_categoria_id = '$categoria' AND chr_proyecto = '$proyecto' AND chr_comentario = '$comentario' AND chr_year = '$anio'";
                $date=" AND chr_year = '$anio' ";
                $query = $where.$date;
		$transferencia_res_y = $this->getDato("SUM(dec_subtotal)","mov_movimiento_detalle",$query);

		$resultado = new stdClass();
        $resultado->saldo_m = $presupuesto_m - $ejecutado_m + $transferencia_pos_m - $transferencia_res_m;
        $resultado->saldo_q = $presupuesto_q - $ejecutado_q + $transferencia_pos_q - $transferencia_res_q;
        $resultado->saldo_y = $presupuesto_y - $ejecutado_y + $transferencia_pos_y - $transferencia_res_y;
        $resultado->mes = $mes_anio[(int)$mes];
        $resultado->trimestre = $q;

        return $resultado;


	}
	//AREA DE FORMULARIO DE COMPRA ABIERTA

	function getCorrelativoTipoOCA($tipo){
		$sql = "SELECT numero FROM f_provisionar
				WHERE tipo = '$tipo'
				ORDER BY numero DESC LIMIT 1";
		$res = $this->db->get_var($sql);

		return $res;
	}

	function saveProvisionarSolicitudGastoPresupuesto($data){
		$direccion = "DACA";
		$tipoOCA = $data->tipoOCA;
		$numeroOCA = $data->numeroOCA;
		$estadoOCA = $data->estadoOCA;
		$fechaOCA = $data->fechaOCA;
		$areaOCA = $data->areaOCA;
		$stringOCA = $data->stringOCA;
		$categoriaOCA = $data->categoriaOCA;
		$proyectoOCA = $data->proyectoOCA;
		$detalleOCA = $data->detalleOCA;
		$proveedorOCA = $data->proveedorOCA;
		$descripcionOCA = $data->descripcionOCA;
		$montoOCA = $data->montoOCA;
		$monedaOCA = $data->monedaOCA;
		$tipocambioOCA = $data->tipocambioOCA;
		$tipoOCA = $data->tipoOCA;
		$totalOCA = $data->totalOCA;
		$procedimiento = $data->proceso;
		$usuario = "csalcedo";
		$anio = substr($fechaOCA, 0, 4);
		$mes = substr($fechaOCA, 5, 2);

		$tipo = $this->getDato("tipo","presupuesto_ingreso","area = '$areaOCA' limit 1");
		$string = $this->getDato("string","presupuesto_ingreso","chr_string_id = '$stringOCA' limit 1");
		$categoria = $this->getDato("categoria","presupuesto_ingreso","chr_categoria_id = '$categoriaOCA' limit 1");

		if($mes == "01" || $mes == "02" || $mes == "03"){
			$trimestre = 1;
		}
		if($mes == "04" || $mes == "05" || $mes == "06"){
			$trimestre = 2;
		}
		if($mes == "07" || $mes == "08" || $mes == "09"){
			$trimestre = 3;
		}
		if($mes == "10" || $mes == "11" || $mes == "12"){
			$trimestre = 4;
		}

		if($procedimiento == "GUARDAR FORMULARIO"){

			$sql_nuevoregistro = "INSERT INTO f_provisionar (tipo,numero,direccion,tipo_presupuesto,area,chr_string_id,string,chr_categoria_id,categoria,monto,moneda,tipocambio,total,anio,mes,trimestre,descripcion,proyecto,comentario,estado,fechaGasto,usuario)
					VALUES ('$tipoOCA',$numeroOCA,'$direccion','$tipo','$areaOCA','$stringOCA','$string','$categoriaOCA','$categoria','$montoOCA','$monedaOCA','$tipocambioOCA','$totalOCA','$anio','$mes','$trimestre','$descripcionOCA','$proyectoOCA','$detalleOCA','$estadoOCA','$fechaOCA','$usuario')";

			$res_nuevoregistro=$this->db->query($sql_nuevoregistro);

		}else{

			$sql_actualizacion = "UPDATE f_provisionar SET
					area = '$area',
					chr_string_id = '$chr_string_id',
					string = '$string',
					chr_categoria_id = '$categoriaOCA',
					categoria = '$categoria',
					monto = '$montoOCA',
					moneda = '$monedaOCA',
					tipocambio = '$tipocambioOCA',
					total = '$totalOCA',
					anio = '$anio',
					mes = '$mes',
					trimestre = '$trimestre',
					descripcion = '$descripcionOCA',
					proyecto = '$proyectoOCA',
					comentario = '$detalleOCA',
					estado = '$estadoOCA',
					fechaGasto = '$fechaOCA',
					usuario = '$usuario'
					WHERE idOca = '$idOca'";

			$res_actualizacion=$this->db->query($sql_actualizacion);

		}

	    if($res_nuevoregistro <> "" OR $res_actualizacion <> ""){
	    	return "OK";
	    }else{
	        return "ERROR";
	    }

	}

	function getConsultarProvisionarSolicitudGastoPresupuesto($data,$anio){
		$tipo = "";
		$estado = "";
		if($data != "0"){	
			if($data->tipo != "0"){
				$tipo = "AND A.idFormato = '$data->tipo'";
			}

			if($data->estado != "0"){
				$estado = "AND A.estado = '$data->estado'";
			}
		}

		//CONCAT(SUBSTR(A.idFormato,2,3),'-',A.nformato)
		
		$sql = "SELECT A.idPresupuesto idOca, A.idFormato tipo, A.nformato numero,A.tipo tipo_presupuesto,A.area,A.chr_string_id,A.string,A.chr_categoria_id,A.categoria,A.proyecto,A.comentario,A.proveedor,A.monto,A.moneda,A.tipocambio,A.total,A.anio,A.mes,A.trimestre,A.descripcion,A.estado,A.fechaGasto,A.fechaRegistro,A.usuario,SUM(IF(B.estado IN ('APROBADO','POR APROBAR','LIQUIDACION'),B.total,0)) gastos 
				FROM presupuesto_gasto A LEFT JOIN presupuesto_gasto B
				ON CONCAT(SUBSTR(A.idFormato,2,3),'-',A.nformato) = B.nformato
				WHERE A.idFormato IN ('SOCA','SPRO') $tipo $estado AND A.anio = $anio
				GROUP BY A.idFormato,A.nformato
				ORDER BY A.idPresupuesto ASC";

		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("area","string","categoria","descripcion","proyecto","comentario"));
		return $res;		
	}

	//FINAL DE COMPRA ABIERTA

	function getDetallesSolicitudSOCASPRO($tipo,$idPresupuesto){
		$codigo = $this->getDato("numero","f_".$tipo,"idPresupuesto = $idPresupuesto");
		$sql = "SELECT * FROM presupuesto_gasto
				WHERE nformato = '$codigo' AND nformato != ''
				ORDER BY idPresupuesto DESC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("direccion","tipo","area","string","categoria","actividad","descripcion","documento","proveedor","proyecto","comentario"));
		return $res;
	}

	function liquidarRegistroSolicitudGastoPresupuesto($data){
		$fechaRegistro = $data->fechaRegistro;
		$total = $data->monto;
		$usuario = $data->usuario;
		$estado = "LIQUIDACION";
		$descripcion = "RETORNO POR LIQUIDACION";
		$mes = $data->mes;
		$idFormato = SUBSTR($data->tipo,1,3);
		$nformato = $idFormato."-".$data->numero;

		$sql_nuevoregistro = "INSERT INTO presupuesto_gasto (comentario,mes,fechaRegistro,idFormato,nformato,estado,total,usuario,area)
					VALUES ('$descripcion','$mes','$fechaRegistro','$idFormato','$nformato','$estado',$total,'$usuario',' ')";
		$res_nuevoregistro=$this->db->query($sql_nuevoregistro);

		if($res_nuevoregistro){
			$sql_update_gasto = "	UPDATE presupuesto_gasto SET devoluciones = (devoluciones + $total)
									WHERE idFormato = '$data->tipo' AND nformato = '$data->numero'";
			$res_update_gasto=$this->db->query($sql_update_gasto);

			$sql_update_soca_gasto = "	UPDATE presupuesto_gasto SET estado='LIQUIDADO'
									WHERE idFormato = '$data->tipo' AND nformato = '$data->numero'";
			$res_update_soca_gasto=$this->db->query($sql_update_soca_gasto);

			$sql_update_soca = "	UPDATE f_$idFormato SET estado='LIQUIDADO'
									WHERE numero = '$nformato'";
			$res_update_soca=$this->db->query($sql_update_soca);

		}


	}


	function liquidarFormatoActa($valor){
		$codigo = $this->getVariosResultados("idPresupuesto","presupuesto_gasto","nformato = '$valor' AND estado!= 'APROBADO'");
		if(count($codigo) > 0){
			return 2;
		}else{
			$sql_update = "	UPDATE f_act SET estado = 'APROBADO' WHERE numero = '$valor'";
			$res_update = $this->db->query($sql_update);
			return 1;			
		}
	}

	function liquidarFormatoPedidoInterno($valor){
		$codigo = $this->getVariosResultados("idPresupuesto","presupuesto_gasto","nformato = '$valor' AND estado!= 'APROBADO'");
		if(count($codigo) > 0){
			return 2;
		}else{
			$sql_update = "	UPDATE f_pin SET estado = 'APROBADO' WHERE numero = '$valor'";
			$res_update = $this->db->query($sql_update);
			return 1;			
		}

	}





	function crearBaseReporteDaca_eliminar(){
		//TRUNCAR LA TABLA EN BASE DE DATOS
		$sql_truncate = "TRUNCATE daca_report.reporte_presupuesto";
		$res_truncate = $this->db->query($sql_truncate);

		$sql_truncate_marcacion = "TRUNCATE marcacion.reporte_presupuesto";
		$res_truncate_marcacion = $this->db->query($sql_truncate_marcacion);


		$sql_reporte = "
	
		INSERT INTO daca_report.reporte_presupuesto

		SELECT 0 idReporte, 'DACA' chr_direccion, area chr_area,chr_string_id, string chr_string, chr_categoria_id, categoria chr_categoria, proyecto chr_proyecto, comentario chr_comentario,
		tipo chr_tipo,
		SUM(IF(mes = '01', total, 0)) val_enero,
		SUM(IF(mes = '02', total, 0)) val_febrero,
		SUM(IF(mes = '03', total, 0)) val_marzo,
		SUM(IF(mes = '01' OR mes = '02' OR mes = '03',total,0)) val_q1,
		SUM(IF(mes = '04', total, 0)) val_abril,
		SUM(IF(mes = '05', total, 0)) val_mayo,
		SUM(IF(mes = '06', total, 0)) val_junio,
		SUM(IF(mes = '04' OR mes = '05' OR mes = '06',total,0)) val_q2,
		SUM(IF(mes = '07', total, 0)) val_julio,
		SUM(IF(mes = '08', total, 0)) val_agosto,
		SUM(IF(mes = '09', total, 0)) val_setiembre,
		SUM(IF(mes = '07' OR mes = '08' OR mes = '09',total,0)) val_q3,
		SUM(IF(mes = '10', total, 0)) val_octubre,
		SUM(IF(mes = '11', total, 0)) val_noviembre,
		SUM(IF(mes = '12', total, 0)) val_diciembre,
		SUM(IF(mes = '10' OR mes = '11' OR mes = '12',total,0)) val_q4,
		SUM(total) val_total,
		anio
		FROM dev_presupuesto.presupuesto_gasto
		WHERE estado IN ('POR APROBAR','APROBADO')
		GROUP BY tipo,area,chr_string_id,chr_categoria_id,proyecto,comentario,anio
		ORDER BY anio,area ASC

		";
		$res_reporte =$this->db->query($sql_reporte);


		$sql_reporte_marcacion = "
	
		INSERT INTO marcacion.reporte_presupuesto

		SELECT 0 idReporte, 'DACA' chr_direccion, area chr_area,chr_string_id, string chr_string, chr_categoria_id, categoria chr_categoria, proyecto chr_proyecto, comentario chr_comentario,
		tipo chr_tipo,
		SUM(IF(mes = '01', total, 0)) val_enero,
		SUM(IF(mes = '02', total, 0)) val_febrero,
		SUM(IF(mes = '03', total, 0)) val_marzo,
		SUM(IF(mes = '01' OR mes = '02' OR mes = '03',total,0)) val_q1,
		SUM(IF(mes = '04', total, 0)) val_abril,
		SUM(IF(mes = '05', total, 0)) val_mayo,
		SUM(IF(mes = '06', total, 0)) val_junio,
		SUM(IF(mes = '04' OR mes = '05' OR mes = '06',total,0)) val_q2,
		SUM(IF(mes = '07', total, 0)) val_julio,
		SUM(IF(mes = '08', total, 0)) val_agosto,
		SUM(IF(mes = '09', total, 0)) val_setiembre,
		SUM(IF(mes = '07' OR mes = '08' OR mes = '09',total,0)) val_q3,
		SUM(IF(mes = '10', total, 0)) val_octubre,
		SUM(IF(mes = '11', total, 0)) val_noviembre,
		SUM(IF(mes = '12', total, 0)) val_diciembre,
		SUM(IF(mes = '10' OR mes = '11' OR mes = '12',total,0)) val_q4,
		SUM(total) val_total,
		anio
		FROM dev_presupuesto.presupuesto_gasto
		WHERE estado IN ('POR APROBAR','APROBADO')
		GROUP BY tipo,area,chr_string_id,chr_categoria_id,proyecto,comentario,anio
		ORDER BY anio,area ASC

		";
		$res_reporte_marcacion =$this->db->query($sql_reporte_marcacion);


		return 'OK';
	}









}


?>

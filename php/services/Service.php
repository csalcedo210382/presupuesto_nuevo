<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_pdo.php');
require_once(INCDIR.'phpexcel/PHPExcel.php');

class Service
{
	public $db;
	public $idUsuario = 0;
	public $fechaCreacion;
	private $inicio = false;
	private $excels_dir;




	function __construct() 
	{
		//if(!PRODUCTION_SERVER)
			$this->_iniciar();
			
        	if(PRODUCTION_SERVER) $this->db->hide_errors();
    		$this->excels_dir = PHPDIR.'../excels' ;
	}
	function _iniciar(){
		if($this->inicio)
			return;
		$this->inicio = true;
		$GLOBALS['amfphp']['encoding'] = 'amf3';
		$options = array(
		    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
		);
		$this->excels_dir = PHPDIR.'../excels' ;
		$this->db = new ezSQL_pdo("mysql:host=".DB_HOST.";port=3306;dbname=".DB_NAME,DB_USER,DB_PASS,$options);
		if(PRODUCTION_SERVER) 
			$this->db->hide_errors();
		else{
			$this->idUsuario = 1;		

		}
		$this->fechaCreacion = uc_sqlDate(ZONA_HORARIA);
	}
	function _codificarObjeto($dato,$array){
		if($dato == null){
			return null;
		}
		else if($array == null || COUNT($array) == 0){
			return $dato;
		}
		else if(is_array($dato)){
			foreach ($dato as $value) {
				foreach ($array as $val) {
					$value->{$val} = $this->_codificarPalabra($value->{$val});
				}
			}
		}
		else{
			foreach ($array as $val) {
				$dato->{$val} = $this->_codificarPalabra($dato->{$val});
			}
		}
	}
	
	function _getAccessToken($at=null){
		if($_SESSION["at"] == null){
			if($at)
				$sql = "SELECT fbUser,access_token FROM access_token WHERE access_token!='$at' AND access_token != '' ORDER BY usado ASC LIMIT 1";
			else
				$sql = "SELECT fbUser,access_token FROM access_token WHERE access_token != '' ORDER BY usado ASC LIMIT 1";
			$res = $this->db->get_row($sql);
			$at = $res->access_token;
			$fbUser = $res->fbUser;
			$sql = "UPDATE access_token SET usado = usado + 1 WHERE fbUser = '$fbUser' ";
			$this->db->query($sql);
			$_SESSION["at"] = $at;
		}
		return $_SESSION["at"];
	}
	

	function _leerUrl($url,$data=null,$tipo=""){
		$ch = curl_init();
		if($tipo == "GET"){

		}
		else if($tipo == "POST"){
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, COUNT($data));
			curl_setopt($ch, CURLOPT_POST,true);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
		}
		else{
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
		}
		$result = curl_exec($ch);
		return $result;
	}
	function _runSuperQuery($sql,$db){
		$array = explode(";",$sql);
		foreach($array as $d){
			try{
				$db->query($d);
			}
			catch(Exception $e){

			}
		}
		return "termino";
	}
	
	function _decodificarPalabra($label){
		return $this->db->escape(mb_check_encoding($label,'UTF-8')?utf8_decode($label):$label);
	}
	function _codificarPalabra($label){
		return mb_check_encoding ( $label ,  'UTF-8' )  ? $label : utf8_encode ( $label);
	}
	function _fechaTimeToDate($tiempo){
		$cd = $this->getStringForNumber($tiempo);
		$cd = new DateTime("@$cd");
		return $this->utcToLocalTime($cd->format('Y-m-d H:i:s' ));
	}
	function _utcToLocalTime( $utcTime ){
		$date = new DateTime($utcTime,new DateTimeZone('UTC'));
		$date->setTimezone(new DateTimeZone(ZONA_HORARIA));
		return $date->format('Y-m-d H:i:s');
	}
	function _getStringForNumber($string){
		return number_format($string, 0, '', '');
	}
	
	function _clave_aleatoria(){ 
		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		$len = rand(20,strlen($str)-1);
		$cad = "";
		for($i=0;$i<$len;$i++) {
			$cad .= substr($str,rand(0,62),1);
		}
	    $date = new DateTime();
	    $date = $date->getTimestamp();
	    $str = $date."-".$cad;
	    return $str;
	}
	function _validarLlave($llave){
		$sql = "SELECT idSession,idCliente FROM session_cliente WHERE estado = 1 AND llave = '$llave'";
		$dato = $this->db->get_row($sql);
		if($dato == null)
			return false;
		$idSession = $dato->idSession;
		$idAdmin = $dato->idAdmin;
		$sql = "UPDATE session SET conexiones = conexiones + 1 WHERE idSession = $idSession";
		$this->db->query($sql);
		$this->direccion = "";
		return true;
	}
	function _limpiarPalabra($texto){
      	$textoLimpio = strtolower(preg_replace('([^A-Za-z0-9])', '', $texto));	     					
      	return $textoLimpio;
	}
	function _getSoloNumero($texto){
		$result = "";
		$i = 0;
		while(strlen($texto)>$i){
			$letra = substr($texto,$i,1);			
			if(is_numeric( $letra )){
				$result .= $letra;
			}
			$i++;
		}
		return $result;
	}






	public function getTotalRegistros($variable,$tabla){
		$sql = "SELECT count($variable) FROM $tabla";
		$res = $this->db->get_var($sql);
		return $res;
	}

	public function getVarTabla($variable,$tabla){
		$sql = "SELECT $variable FROM $tabla";
		$res = $this->db->get_var($sql);
		return $res;
	}

	public function getResultados($campos,$tabla){
		$sql = "SELECT $campos FROM $tabla";
		$res = $this->db->get_results($sql);
  		return $res;
	}

	public function getDato($variable,$tabla,$condicion){
		$sql = "SELECT $variable FROM $tabla WHERE $condicion";
		$res = $this->db->get_var($sql);
		return $res;
	}

	public function getVariosResultados($variable,$tabla,$condicion){
		$sql = "SELECT $variable FROM $tabla WHERE $condicion";
		$res = $this->db->get_results($sql);
		return $res;
	}

	public function crearBaseReporteDaca(){
		//TRUNCAR LA TABLA EN BASE DE DATOS
		$sql_truncate = "TRUNCATE daca_report.reporte_presupuesto";
		$res_truncate = $this->db->query($sql_truncate);

		$sql_reporte = "
	
		INSERT INTO daca_report.reporte_presupuesto

		SELECT 0 idReporte, 'DACA' chr_direccion, area chr_area,codigo_string, string chr_string, cuenta_categoria, categoria chr_categoria, proyecto chr_proyecto, comentario chr_comentario,
		tipo chr_tipo,
		SUM(IF(mes = '01', total, 0)) val_enero,
		SUM(IF(mes = '02', total, 0)) val_febrero,
		SUM(IF(mes = '03', total, 0)) val_marzo,
		SUM(IF(mes = '01' OR mes = '02' OR mes = '03',total,0)) val_q1,
		SUM(IF(mes = '04', total, 0)) val_abril,
		SUM(IF(mes = '05', total, 0)) val_mayo,
		SUM(IF(mes = '06', total, 0)) val_junio,
		SUM(IF(mes = '04' OR mes = '05' OR mes = '06',total,0)) val_q2,
		SUM(IF(mes = '07', total, 0)) val_julio,
		SUM(IF(mes = '08', total, 0)) val_agosto,
		SUM(IF(mes = '09', total, 0)) val_setiembre,
		SUM(IF(mes = '07' OR mes = '08' OR mes = '09',total,0)) val_q3,
		SUM(IF(mes = '10', total, 0)) val_octubre,
		SUM(IF(mes = '11', total, 0)) val_noviembre,
		SUM(IF(mes = '12', total, 0)) val_diciembre,
		SUM(IF(mes = '10' OR mes = '11' OR mes = '12',total,0)) val_q4,
		SUM(total) val_total,
		anio
		FROM presupuesto.presupuesto_gasto
		WHERE estado IN ('POR APROBAR','APROBADO')
		GROUP BY tipo,area,codigo_string,cuenta_categoria,proyecto,comentario,anio
		ORDER BY anio,area ASC

		";
		$res_reporte =$this->db->query($sql_reporte);

		$sql_truncate_marcacion = "TRUNCATE marcacion.reporte_presupuesto";
		$res_truncate_marcacion = $this->db->query($sql_truncate_marcacion);
		
		$sql_reporte_marcacion = "
	
		INSERT INTO marcacion.reporte_presupuesto

		SELECT 0 idReporte, 'DACA' chr_direccion, CAST(BINARY(area) AS CHAR CHARACTER SET utf8) chr_area,codigo_string, string chr_string, cuenta_categoria, categoria chr_categoria, proyecto chr_proyecto, comentario chr_comentario,
		tipo chr_tipo,
		SUM(IF(mes = '01', total, 0)) val_enero,
		SUM(IF(mes = '02', total, 0)) val_febrero,
		SUM(IF(mes = '03', total, 0)) val_marzo,
		SUM(IF(mes = '01' OR mes = '02' OR mes = '03',total,0)) val_q1,
		SUM(IF(mes = '04', total, 0)) val_abril,
		SUM(IF(mes = '05', total, 0)) val_mayo,
		SUM(IF(mes = '06', total, 0)) val_junio,
		SUM(IF(mes = '04' OR mes = '05' OR mes = '06',total,0)) val_q2,
		SUM(IF(mes = '07', total, 0)) val_julio,
		SUM(IF(mes = '08', total, 0)) val_agosto,
		SUM(IF(mes = '09', total, 0)) val_setiembre,
		SUM(IF(mes = '07' OR mes = '08' OR mes = '09',total,0)) val_q3,
		SUM(IF(mes = '10', total, 0)) val_octubre,
		SUM(IF(mes = '11', total, 0)) val_noviembre,
		SUM(IF(mes = '12', total, 0)) val_diciembre,
		SUM(IF(mes = '10' OR mes = '11' OR mes = '12',total,0)) val_q4,
		SUM(total) val_total,
		anio
		FROM presupuesto.presupuesto_gasto
		WHERE estado IN ('POR APROBAR','APROBADO')
		GROUP BY tipo,area,codigo_string,cuenta_categoria,proyecto,comentario,anio
		ORDER BY anio,area ASC

		";
		$res_reporte_marcacion =$this->db->query($sql_reporte_marcacion);

		if($res_reporte_marcacion){
			$sql_update_marcacion = "UPDATE marcacion.reporte_presupuesto SET chr_area =  REPLACE(chr_area,'Ã“','Ó')";
			$res_update_marcacion =$this->db->query($sql_update_marcacion);			
		}


		return 'OK';
	}

}
?>

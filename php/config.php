<?php

define('ZONA_HORARIA','America/Lima');

define("PRODUCTION_SERVER", false);

if(PRODUCTION_SERVER){
	
	define('CANVAS_URL', 'http://acreditacion.upc.edu.pe/presupuesto' );
	define('PHPDIR', dirname(__FILE__).'/');
	define('INCDIR', PHPDIR.'includes/');
	define('REPDIR', 'webservice/reportes/');

}else{
	define('CANVAS_URL', 'https://presupuester-daca.upc.edu.pe' );
	//define('CANVAS_URL', 'localhost/presupuesto_nuevo' ); //https://presupuester-daca.upc.edu.pe   
	define('PHPDIR', dirname(__FILE__).'/');
	define('INCDIR', PHPDIR.'includes/');
	define('REPDIR', 'webservice/reportes/');
}


//--
//-- AMFPHP stuff
//--

define('AMFCORE', INCDIR.'amfphp/core/'); //  directorio de las clases de amfphp
define('SRVPATH', PHPDIR.'services/'); // carpeta donde estan los servicios del amfphp
define('VOPATH', PHPDIR.'services/vo/'); // carpeta donde estan los vo que usan los servicios

//--
//-- MySQL stuff
//--

if(PRODUCTION_SERVER)
{

	define("DB_HOST", "10.10.2.252");
	define("DB_USER", "csalcedo");
	define("DB_PASS", "123456");
	define("DB_NAME", "presupuesto");

}
else
{
	
	define("DB_HOST", "localhost");
	define("DB_USER", "root");
	define("DB_PASS", "Admin123#"); //Admin123#
	define("DB_NAME", "presupuesto");
}

//--
//-- Log stuff
//--

// la carpeta logs debe tener permisos de escritura si se va utilizar un archivo de log
define('LOG_DIR', PHPDIR.'logs' );
define('LOG_BASE', 'log_app' );

?>
<?php

define('MYSQL_HOST', '10.10.2.252');
define('MYSQL_USER', 'edhuaman');
define('MYSQL_PASSWORD', '123456');
define('MYSQL_DATABASE', 'presupuesto');
define('PERSONAL_TIEMPO_ALERTA', 5);
define('NOTIFICACION_CORREO_FROM', 'daca-reportes@upc.pe,DACA,daca-reportes@upc.pe,Da04102018#');
define('NOTIFICACION_SERVER', 'http://ec2-54-205-192-212.compute-1.amazonaws.com/services/public/api/mail');

define('NOTIFICACION_GRUPO_AREAS',1);
define('NOTIFICACION_AREA',2);

define('NOTIFICACION_ESTADO_PENDIENTE_ENVIAR', 5);
define('NOTIFICACION_ESTADO_CREADO', 1);
define('NOTIFICACION_ESTADO_ENVIADO', 3);
define('NOTIFICACION_ESTADO_FALLIDA', 4);

define('NOTIFICACION_DIARIO', 1);
define('NOTIFICACION_SEMANAL', 2);
define('NOTIFICACION_MENSUAL', 3);
define('NOTIFICACION_TRIMESTRAL', 4);

define('TOKEN_DIRECTOR','MANUEL CORTES FONTCUBERTA ABUCCI');
define('TOKEN_ASISTENTE_DIRECTOR','MIRYAM SALAZAR');

class Alerta {

    private $mysql;

    public function __construct() {
        $this->mysql = new PDO('mysql:host=' . MYSQL_HOST . ';dbname=' . MYSQL_DATABASE, MYSQL_USER, MYSQL_PASSWORD);
    }

    public function run() {
        //obtenemos todas las alertas activas
        $alertas = $this->getAlertas();
        //obtenemos las áreas del sistema
        $areas = $this->getAreas();
        //iteramos las alertas y ejecutamos de acuerdo al tipo
        if(is_array($alertas) && count($alertas)>0){
            foreach($alertas as $index=>$objAlerta){
                //es una notificación para todas las areas
                if($objAlerta->int_categoryid == NOTIFICACION_GRUPO_AREAS){
                    $arrayAlertaFeedback = [];
                    //iteramos toas las areas para que sea el envío para cada una de ellas
                    foreach($areas as $indice=>$objArea){
                        $objAlerta->idArea = $objArea->idArea;
                        $objAlerta->chr_name_area = $objArea->area;
                        $resultSend = $this->executeAlerta($objAlerta);
                        if(!is_null($resultSend)){
                            $arrayAlertaFeedback[] = $resultSend;
                        }
                    }
                    //Guardamos los logs de cada uno de los envíos por cada área
                    //iteramos los retornos de cada envío
                    if(is_array($arrayAlertaFeedback) && count($arrayAlertaFeedback)>0){
                        foreach($arrayAlertaFeedback as $pun=>$objFeedback){
                            $objAlerta->txt_contenido = $objFeedback->txt_contenido;
                            $objAlerta->txt_feedback = $objFeedback->txt_feedback;
                            $this->setLog($objAlerta, NOTIFICACION_ESTADO_ENVIADO);
                        }
                    }
                }else{
                    //notificación para una área en específica
                }
            }
        }
    }

    private function executeAlerta($objAlerta){
        $feedback = null;
        //verificamos que tipo de correo es
        if($objAlerta->int_statusid == NOTIFICACION_ESTADO_PENDIENTE_ENVIAR){
            //verificamos si la alerta si cumple con la configuración de fecha de envío
            if($this->isReadyToSend($objAlerta)){
                //Verificamos si existe un correo enviado el día de hoy
                if(!$this->existeCorreo($objAlerta)){
                    //enviamos el correo
                    $feedback = $this->sendAlerta($objAlerta);
                }
            }
        }
        return $feedback;
    }

    /**
     * Método para validar si la configuración de fecha de envío cumple para ejecutarse el envío
     *
     * @param object $objAlerta
     * @return boolean
     */
    private function isReadyToSend($objAlerta){
        date_default_timezone_set('America/Lima');
        $returnValue = FALSE;
        switch($objAlerta->int_periocidadId){
            case NOTIFICACION_MENSUAL:
                $current_day = date("d");
                $current_hour = date("H");
                if($objAlerta->chr_hora == $current_hour && $objAlerta->chr_dia_mes == abs($current_day)){
                    $returnValue = TRUE;
                }
                break;
            case NOTIFICACION_TRIMESTRAL:
                $current_hour = date("H");
                $current_day = date("d");
                $current_index_month = $this->getIndexMonthTrrimestral();
                //verificamos si cumple con la fecha de lanzamiento
                if($objAlerta->chr_hora == $current_hour &&
                   $objAlerta->chr_dia_mes == abs($current_day) && 
                   $objAlerta->chr_mes == $current_index_month){
                       $returnValue = TRUE;
                   }
                break;
        }
        return $returnValue;
    }

    private function getTrimestres(){
        return [
            '1'=>[1,2,3],
            '2'=>[4,5,6],
            '3'=>[7,8,9],
            '4'=>[10,11,12]
        ];
    }

    private function getIndexMonthTrrimestral($current_month=0){
        $returnValue = 0;
        if($current_month == 0){
            $current_month = date("m");
        }
        $trimestres = $this->getTrimestres();
        //itereamos los trimestres para identiificar a que trimestre funciona
        foreach($trimestres as $index=>$arrayTrimestre){
            //ubicamos nuestro mes actual
            if(in_array($current_month,$arrayTrimestre)){
                $returnValue = $index;
                break;
            }
        }
        return $returnValue;
    }

    /**
     * Método para enviar las alertas,
     * retona la alerta con el contenido interpretado y el feedback
     * @param object $objAlerta
     * @return object
     */
    private function sendAlerta($objAlerta){
        $returnValue = $objAlerta;
        $params = [
            'server' => 'office365',
            'from' => NOTIFICACION_CORREO_FROM,
            //'to' => 'edmin.huamani@upc.pe,carlos.salcedo@upc.pe,jimy.paredes@upc.pe',
            'to' => $this->getCorreosByArea($objAlerta->idArea), //'edmin.huamani@upc.pe,carlos.salcedo@upc.pe,jimy.paredes@upc.pe',
            'subject' => $objAlerta->chr_subject,
            'body' => $this->getBodyMail($objAlerta)
        ];

        //registramos en los logs la participación de la alerta
                        
        $objAlerta->txt_contenido = $params['body'];
        $this->setLog($objAlerta, NOTIFICACION_ESTADO_CREADO);
        $feedback = $this->curl_post_async(NOTIFICACION_SERVER, $params); 
        $objAlerta->txt_feedback = $feedback; 
        //$this->setLog($objAlerta,NOTIFICACION_ESTADO_ENVIADO);
        $returnValue = $objAlerta;
        return $returnValue;
    }

    private function getCorreosByArea($idArea){
        //$returnValue = 'edmin.huamani@upc.pe,carlos.salcedo@upc.pe,jimy.paredes@upc.pe';
        $arrayValue = ['edmin.huamani@upc.pe','carlos.salcedo@upc.pe','jimy.paredes@upc.pe'];
        //$arrayValue = ['edmin.huamani@upc.pe'];
        $objArea = $this->getAreaById($idArea);
        if(is_object($objArea)){
            $sql = "SELECT * FROM usuario WHERE cargo ='jefe de area' AND usuario IN (SELECT usuario FROM usuario_area WHERE AREA LIKE '%".$idArea."%')";
            $result = $this->mysql->query($sql)->fetchAll(PDO::FETCH_OBJ);
            if(is_array($result) && count($result)>0){
                foreach($result as $index=>$objUsuario){
                    if(!is_null($objUsuario->chr_correo) && strlen(trim($objUsuario->chr_correo))>0){
                        if(!in_array($objUsuario->chr_correo,$arrayValue)){
                            array_push($arrayValue,$objUsuario->chr_correo);
                        }
                    }
                }
            }
        }
        $returnValue = implode(',', $arrayValue);
        return $returnValue;
    }    

    private function curl_post_async($url, $params) {
        $returnValue = '';
        foreach ($params as $key => &$val) {
            if (is_array($val))
                $val = implode(',', $val);
            $post_params[] = $key . '=' . urlencode($val);
        }
        $post_string = implode('&', $post_params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'curl');
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        //$result = curl_exec($ch);
        if( ! $result = curl_exec($ch)){ 
            $returnValue = curl_error($ch); 
        }         
        curl_close($ch);
        return $returnValue;
    }    

    private function getBodyMail($objRequest) {
        $returnValue = '';
        $returnValue .= '<!DOCTYPE html>';
        $returnValue .= '<html lang="es">';
        $returnValue .= '<body>';
        $returnValue .= '<table>';
        $returnValue .= '<tr>';
        $returnValue .= '<td width="10%"><img src="https://www.upc.edu.pe/sites/all/themes/upc/img/logo.png" /></td>';
        $returnValue .= '<td width="90%"></td>';
        $returnValue .= '</tr>';
        
        $returnValue .= '<tr>';
            $returnValue .= '<td colspan="2">&nbsp;</td>';
        $returnValue .= '</tr>';
        
        $returnValue .= '<tr>';
            $returnValue .= '<td colspan="2">';
            $returnValue .= '<p>'.$this->decodeBody($objRequest).'</p>';
            $returnValue .= '</td>';
        $returnValue .= '</tr>';

        $returnValue .= '</table>';
        $returnValue .= '</body>';
        $returnValue .= '</html>';
        return $returnValue;
    }

    private function decodeBody($objRequest){
        $returnValue = $objRequest->txt_body;
        $tokens = $this->getTokens();
        $meses = $this->getMeses();
        //listamos los tokens para buscarlos en el texto de contenido
        foreach($tokens as $indice=>$objToken){
            switch($objToken->chr_value){
                case '[[JEFE_AREA]]':
                    //obtenemos el valor del token
                    $value = $this->getNameJefeArea($objRequest->idArea);
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue);
                    break;
                case '[[MES]]':
                    //obtenemos el nombre del mes
                    $m = date("m");
                    $value = date("F", mktime(0, 0, 0, $m, 10));
                    $returnValue = str_replace($objToken->chr_value,strtoupper($meses[$value]),$returnValue);
                    break;
                case '[[AREA]]':
                    //obtenemos el nombre del área
                    $value = $objRequest->chr_name_area;
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue);
                    break;
                case '[[PORCENTAJE_SALDO_AREA]]':
                    $m = date("m");
                    $value = $this->getSaldoMensualByArea($objRequest->idArea).' %';
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue);                    
                    break;
                case '[[TRIMESTRE]]':
                    $current_month = date("m");
                    $trimestres = $this->getTrimestres();
                    $current_index_month = $this->getIndexMonthTrrimestral($current_month);
                    $value = 'Q'.$current_index_month;
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue);                    
                    break;
                case '[[PORCENTAJE_SALDO_TRIMESTRAL_AREA]]':
                    //Indice del trimestre actual
                    $indice = $this->getIndexMonthTrrimestral();
                    $value = $this->getSaldoTrimestralByArea($objRequest->idArea, $indice).' %';
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue);                     
                    break;
                case '[[PORCENTAJE_EJECUTADO_MENSUAL]]':
                    $value = $this->getMontoEjecutadoMensual($objRequest->idArea).' %';
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue);
                    break;
                case '[[PORCENTAJE_EJECUTADO_TRIMESTRAL]]':
                    $indice = $this->getIndexMonthTrrimestral();
                    $value = $this->getMontoEjecutadoTrimestralByArea($objRequest->idArea, $indice).' %';
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue); 
                    break;
                case '[[DIRECTOR_DACA]]':
                    $value = TOKEN_DIRECTOR;
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue);
                    break;
                 case '[[ASISTENTE_DACA]]':
                    $value = TOKEN_ASISTENTE_DIRECTOR;
                    $returnValue = str_replace($objToken->chr_value,$value,$returnValue);                    
                    break;                                        
            }
        }
        return $returnValue;
    }

    private function getMontoEjecutadoMensual($idArea, $m=0){
        $returnValue = 0;
        if($m==0){
            $m = sprintf("%02d", date("m"));
        }
        $objArea = $this->getAreaById($idArea);
        //Calculamos el saldo mensual por area
        $ejecutado_mensual_area = 0;
        if(is_object($objArea)){
            $sql ="SELECT SUM(presupuesto_total) as presupuesto_total FROM mov_saldo WHERE area='".utf8_decode($objArea->area)."' AND mes='".$m."' AND anio='".date("Y")."'";
            $objResult = $this->mysql->query($sql)->fetch(PDO::FETCH_OBJ); 
            if(is_object($objResult)){
                $ejecutado_mensual_area = floatval($objResult->presupuesto_total);
            }
            //obtenemos lo sumatoria de la sumatoria de los gastado
            $gasto_total = 0; 
            $sql ="SELECT * FROM mov_saldo WHERE AREA='".utf8_decode($objArea->area)."' AND mes ='".$m."' AND anio='".date("Y")."'";
            $result = $this->mysql->query($sql)->fetchAll(PDO::FETCH_OBJ); 
            if(is_array($result) && count($result)>0){
                foreach($result as $ind=>$oItem){
                    $gasto_total+=floatval($oItem->total_gasto);
                }
            }
            if($ejecutado_mensual_area==0){
                $ejecutado_mensual_area=1;
            }
            //calculamos el porcentaje
            $returnValue = number_format((($gasto_total/$ejecutado_mensual_area)*100),1);

        }
        return $returnValue;
    }

    private function getMontoEjecutadoTrimestralByArea($idArea, $index_trimestre=1){
        $returnValue=0;
        $trimestres = $this->getTrimestres();
        $meses = $trimestres[$index_trimestre];
        $objArea = $this->getAreaById($idArea);
        //Calculamos el saldo mensual por area
        $presupuesto_trimestral_area = 0;
        if(is_object($objArea)){
            $sql ="SELECT SUM(presupuesto_total) as presupuesto_total FROM mov_saldo WHERE area='".utf8_decode($objArea->area)."' AND mes IN (".implode(',', $meses).") AND anio='".date("Y")."'";
            $objResult = $this->mysql->query($sql)->fetch(PDO::FETCH_OBJ); 
            if(is_object($objResult)){
                $presupuesto_trimestral_area = floatval($objResult->presupuesto_total);
            }
            //obtenemos lo gastado
            $gasto_total = 0; 
            $sql ="SELECT * FROM mov_saldo WHERE AREA='".utf8_decode($objArea->area)."' AND mes  IN (".implode(',',$meses).") AND anio ='".date("Y")."'";
            $result = $this->mysql->query($sql)->fetchAll(PDO::FETCH_OBJ); 
            if(is_array($result) && count($result)>0){
                foreach($result as $ind=>$oItem){
                    $gasto_total+=floatval($oItem->total_gasto);
                }
            }
            //calculamos el porcentaje
            if($presupuesto_trimestral_area==0){
                $presupuesto_trimestral_area= 1;
            }
            $returnValue = number_format((($gasto_total/$presupuesto_trimestral_area)*100),1);                       
           
        }
        return $returnValue;
    }

    private function getSaldoTrimestralByArea($idArea, $index_trimestre =1){
        $returnValue = 0;
        $trimestres = $this->getTrimestres();
        $meses = $trimestres[$index_trimestre];
        $objArea = $this->getAreaById($idArea);   
        //Calculamos el saldo mensual por area
        $presupuesto_trimestral_area = 0;
        if(is_object($objArea)){
            $sql ="SELECT SUM(presupuesto_total) as presupuesto_total FROM mov_saldo WHERE area='".utf8_decode($objArea->area)."' AND mes IN (".implode(',', $meses).") AND anio='".date("Y")."'";
            //print_r($sql."\n");
            $objResult = $this->mysql->query($sql)->fetch(PDO::FETCH_OBJ); 
            if(is_object($objResult)){
                $presupuesto_trimestral_area = floatval($objResult->presupuesto_total);
            }

            //obtenemos lo sumatoria de la sumatoria de los gastado
            $presupuesto_diferencia_total = 0; 
            $sql ="SELECT * FROM mov_saldo WHERE AREA='".utf8_decode($objArea->area)."' AND mes  IN (".implode(',',$meses).") AND anio ='".date("Y")."'";
            $result = $this->mysql->query($sql)->fetchAll(PDO::FETCH_OBJ); 
            if(is_array($result) && count($result)>0){
                foreach($result as $ind=>$oItem){
                    $presupuesto_diferencia_total+=floatval($oItem->diferencia_total);
                }
            }
            //validamos que el presupuesto diferencial sea negativo
            if($presupuesto_diferencia_total <0){
                $returnValue = '0';
            }else{
                //calculamos el porcentaje
                if($presupuesto_trimestral_area==0){
                    $presupuesto_trimestral_area= 1;
                }
                $returnValue = number_format((($presupuesto_diferencia_total/$presupuesto_trimestral_area)*100),1);                       
            }
        }
                
        return $returnValue;
    }

    private function getSaldoMensualByArea($idArea,$m=0){
        $returnValue= 0;
        if($m==0){
            $m = sprintf("%02d", date("m"));
        }
        $objArea = $this->getAreaById($idArea);
        //Calculamos el saldo mensual por area
        $presupuesto_mensual_area = 0;
        if(is_object($objArea)){
            $sql ="SELECT SUM(presupuesto_total) as presupuesto_total FROM mov_saldo WHERE area='".utf8_decode($objArea->area)."' AND mes='".$m."' AND anio='".date("Y")."'";
            $objResult = $this->mysql->query($sql)->fetch(PDO::FETCH_OBJ); 
            if(is_object($objResult)){
                $presupuesto_mensual_area = floatval($objResult->presupuesto_total);
            }
            //obtenemos lo sumatoria de la sumatoria de los gastado
            $presupuesto_diferencia_total = 0; 
            $sql ="SELECT * FROM mov_saldo WHERE AREA='".utf8_decode($objArea->area)."' AND mes ='".$m."' AND anio='".date("Y")."'";
            $result = $this->mysql->query($sql)->fetchAll(PDO::FETCH_OBJ); 
            if(is_array($result) && count($result)>0){
                foreach($result as $ind=>$oItem){
                    $presupuesto_diferencia_total+=floatval($oItem->diferencia_total);
                }
            }
            //validamos que el saldo sea positivo
            if($presupuesto_diferencia_total < 0){
                $returnValue = '0';
            }else{
                //calculamos el porcentaje
                $returnValue = number_format((($presupuesto_diferencia_total/$presupuesto_mensual_area)*100),1);
            }
        }
        return $returnValue;
        
    }    

    private function getNameJefeArea($idArea){
        $returnValue = '';
        $arrayValue = [];
        //obtenemos los usuarios que
        $sql ="SELECT u.* 
                FROM usuario u
                WHERE u.cargo='jefe de area' AND usuario IN (SELECT usuario FROM usuario_area WHERE AREA LIKE '%".$idArea."%')";
        $usuarios = $this->mysql->query($sql)->fetchAll(PDO::FETCH_OBJ);      
        if(is_array($usuarios) && count($usuarios)>0){
            foreach($usuarios as $index=>$objUsuario){
                array_push($arrayValue,$objUsuario->nombre_completo);
            }
        }
        $returnValue = strtoupper(implode(', ',$arrayValue));           
        return $returnValue;
    }    

    private function getTokens() {
        $returnValue = [];
        $sql = "SELECT * FROM ntf_token WHERE is_active='1'";
        $returnValue = $this->mysql->query($sql)->fetchAll(PDO::FETCH_OBJ);          
        return $returnValue;
    }

    private function getMeses(){
        return [
            'January' => 'enero',
            'February' => 'febrero',
            'March' => 'marzo',
            'April' => 'abril',
            'May' => 'mayo',
            'June' => 'junio',
            'July' => 'julio',
            'August' => 'agosto',
            'September' => 'septiembre',
            'October' => 'octubre',
            'November' => 'noviembre',
            'December' => 'diciembre'
        ];
    }                

    /**
    * Método para verificar si existe ya un correo enviado en la periocidad actual
    */
    private function existeCorreo($objAlerta){
        $returnValue = FALSE;
            switch($objAlerta->int_periocidadId){
                case NOTIFICACION_DIARIO:
                    //Verificamos si existe un correo enviado el día de hoy
                    $hora_fin = time();
                    $date = date("Y-m-d").' 00:00:00';
                    $hora_inicio = strtotime($date);
                    $sql = "SELECT * 
                    FROM log_tarea 
                    WHERE 
                    lt.int_tareaid = '".$objAlerta->id."' AND  
                    int_new_statusid = '".NOTIFICACION_ESTADO_ENVIADO."' AND 
                    date_timeexecuted BETWEEN '".$hora_inicio."' AND '".$hora_fin."'";
                    $returnValue = $this->mysql->query($sql)->fetchAll(PDO::FETCH_OBJ);
                    if(is_array($returnValue) && count($returnValue)>0){
                        $returnValue = TRUE;
                    }                    
                    break;
                case NOTIFICACION_SEMANAL:
                    break;
                case NOTIFICACION_MENSUAL:
                    //DATE_FORMAT(FROM_UNIXTIME(lt.date_timeexecuted),'%h')='".$objAlerta->chr_hora."' AND
                    $hora_fin = time();
                    $date = date("Y-m-01").' 00:00:00';
                    $hora_inicio = strtotime($date);
                    $sql = "SELECT lt.*, DATE_FORMAT(FROM_UNIXTIME(lt.date_timeexecuted),'%h') AS hora_ejecutada 
                    FROM log_tarea lt 
                    WHERE 
                    (DATE_FORMAT(FROM_UNIXTIME(lt.date_timeexecuted),'%h')='".sprintf("%02d", $objAlerta->chr_hora)."' AND
                    DATE_FORMAT(FROM_UNIXTIME(lt.date_timeexecuted),'%d')='".sprintf("%02d", $objAlerta->chr_dia_mes)."'
                    )
                    AND
                    lt.int_tareaid = '".$objAlerta->id."' AND 
                    lt.int_new_statusid = '".NOTIFICACION_ESTADO_ENVIADO."' AND 
                    lt.date_timeexecuted BETWEEN '".$hora_inicio."' AND '".$hora_fin."'";                
                    $returnValue = $this->mysql->query($sql)->fetchAll(PDO::FETCH_OBJ);
                    if(is_array($returnValue) && count($returnValue)>0){
                        $returnValue = TRUE;   
                    }                    
                    break;
                case NOTIFICACION_TRIMESTRAL:
                    $hora_fin = time();
                    $current_index_month = $this->getIndexMonthTrrimestral();
                    $trimestres = $this->getTrimestres();
                    $first_month_of_trimestre = $trimestres[$current_index_month][0];
                    $first_month_of_trimestre = sprintf("%02d", $first_month_of_trimestre);
                    $date = date("Y-".$first_month_of_trimestre."-01").' 00:00:00';
                    $hora_inicio = strtotime($date);
                    //obtenemos el mes que debió ejecutarse
                    $month_executed = $trimestres[$current_index_month][(intval($objAlerta->chr_mes)-1)];

                    $sql = "SELECT lt.*, DATE_FORMAT(FROM_UNIXTIME(lt.date_timeexecuted),'%h') AS hora_ejecutada 
                    FROM log_tarea lt 
                    WHERE 
                    (DATE_FORMAT(FROM_UNIXTIME(lt.date_timeexecuted),'%h')='".sprintf("%02d", $objAlerta->chr_hora)."' AND 
                    DATE_FORMAT(FROM_UNIXTIME(lt.date_timeexecuted),'%d')='".sprintf("%02d", $objAlerta->chr_dia_mes)."' AND 
                    DATE_FORMAT(FROM_UNIXTIME(lt.date_timeexecuted),'%m')='".sprintf("%02d", $month_executed)."') AND
                    lt.int_tareaid = '".$objAlerta->id."' AND  lt.int_new_statusid = '".NOTIFICACION_ESTADO_ENVIADO."' AND 
                    lt.date_timeexecuted BETWEEN '".$hora_inicio."' AND '".$hora_fin."'";                   
                    $returnValue = $this->mysql->query($sql)->fetchAll(PDO::FETCH_OBJ);
                    if(is_array($returnValue) && count($returnValue)>0){
                        $returnValue = TRUE;
                    }                
                    break;
            }        
        return $returnValue;
    }

    /**
    * Método para registrar en los logs
    */
    private function setLog($objAlerta, $status=1){
        $statement = $this->mysql->prepare("INSERT INTO log_tarea(int_tareaid,chr_name,txt_description,is_active, is_deleted, date_timecreated, date_timemodified, int_creatorid, int_typeid, int_periocidadId, chr_hora, chr_dia_semana, chr_dia_mes, chr_mes, chr_to, chr_from, chr_subject, txt_body, int_categoryid, chr_username, chr_password, int_statusid, txt_contenido, int_new_statusid, date_timeexecuted,txt_feedback)
            VALUES(:int_tareaid,:chr_name,:txt_description,:is_active, :is_deleted, :date_timecreated, :date_timemodified, :int_creatorid, :int_typeid, :int_periocidadId, :chr_hora, :chr_dia_semana, :chr_dia_mes, :chr_mes, :chr_to, :chr_from, :chr_subject, :txt_body, :int_categoryid, :chr_username, :chr_password, :int_statusid, :txt_contenido, :int_new_statusid, :date_timeexecuted, :txt_feedback)");
        $statement->execute(array(
                "int_tareaid"=>$objAlerta->id,
                "chr_name"=>$objAlerta->chr_name,
                "txt_description"=>$objAlerta->txt_description,
                "is_active"=>$objAlerta->is_active,
                "is_deleted"=>$objAlerta->is_deleted,
                "date_timecreated"=>$objAlerta->date_timecreated,
                "date_timemodified"=>$objAlerta->date_timemodified,
                "int_creatorid"=>$objAlerta->int_creatorid,
                "int_typeid"=>$objAlerta->int_typeid,
                "int_periocidadId"=>$objAlerta->int_periocidadId,
                "chr_hora"=>$objAlerta->chr_hora,
                "chr_dia_semana"=>$objAlerta->chr_dia_semana,
                "chr_dia_mes"=>$objAlerta->chr_dia_mes,
                "chr_mes"=>$objAlerta->chr_mes,
                "chr_to"=>$objAlerta->chr_to,
                "chr_from"=>$objAlerta->chr_from,
                "chr_subject"=>$objAlerta->chr_subject,
                "txt_body"=>$objAlerta->txt_body,
                "int_categoryid"=>$objAlerta->int_categoryid,
                "chr_username"=>$objAlerta->chr_username,
                "chr_password"=>$objAlerta->chr_password,
                "int_statusid"=>$objAlerta->int_statusid,
                "txt_contenido"=>$objAlerta->txt_contenido,
                "txt_feedback"=>$objAlerta->txt_feedback,
                "int_new_statusid"=>$status,
                "date_timeexecuted"=>time()
        ));
    }

    /**
     * Método para retornar todas las áreas
     *
     * @return array
     */
    private function getAreas(){
        $returnValue = [];
        $sql = "SELECT * FROM presupuesto_area";
        $returnValue = $this->mysql->query($sql)->fetchAll(PDO::FETCH_OBJ);        
        return $returnValue;
    }

    /**
    * Método para listar las alertas activas
    */
    private function getAlertas(){
        $returnValue = [];
        $sql = "SELECT * FROM ntf_tarea WHERE is_active='1' AND is_deleted='0'";
        $returnValue = $this->mysql->query($sql)->fetchAll(PDO::FETCH_OBJ);        
        return $returnValue;
    }

    private function getAreaById($idArea){
        $returnValue = NULL;
        $sql = "SELECT * FROM presupuesto_area WHERE idArea='".$idArea."'";
        $returnValue = $this->mysql->query($sql)->fetch(PDO::FETCH_OBJ);
        return $returnValue;
    }


}

$objCronAlerta = new Alerta();
$objCronAlerta->run();

<?php
    include("plantilla_informe.php");
    error_reporting(0);
    session_start();
    require_once('../php/config.php');
    require_once('../php/services/ServiceReportes.php');
   
    //LLENADO DE DATOS
    $hora = date("h:i:s");
    $fecha = date("Y-m-j");

    $formato = $_GET["formato"];
    $codigo = $_GET["codigo"];
    $numero = $_GET["numero"];

    if($formato == "OCA"){
        $tipo_formato = "SOLICITUD ORDEN DE COMPRA ABIERTA";
    }else{
        $tipo_formato = "SOLICITUD PROVISION";
    }

    $service = new ServiceReportes();

    $data = $service->getDetalleSolicitud($formato,$codigo,$numero);
    $solicitud = $data->solicitud;
    $detalle = $data->detalle;




    $tamanoLetra = 7;
    $borde = 0;
    $alineacion = "L";
    $altoFila = 4;



    $pdf = new PDF( 'L', 'mm', 'A4' );


    $pdf->AddPage();

    $pdf->Ln(5);

    $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
    $pdf->Cell( 10, $altoFila, '', $borde, 0, $alineacion);
    $pdf->Cell( 20, $altoFila, 'FORMATO : ', $borde, 0, 'R');
    $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 110, $altoFila, $tipo_formato, $borde, 0, $alineacion);
    $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
    $pdf->Cell( 20, $altoFila, 'MONTO SOLICITADO S/. : ', $borde, 0, 'R');
    $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 60, $altoFila, $solicitud[0]->total, $borde, 0, $alineacion);
    $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
    $pdf->Cell( 20, $altoFila, 'ESTADO : ', $borde, 0, 'R');
    $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 30, $altoFila, $solicitud[0]->estado, $borde, 0, $alineacion);
    $pdf->Ln(5);

    $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
    $pdf->Cell( 10, $altoFila, '', $borde, 0, $alineacion);
    $pdf->Cell( 20, $altoFila, 'NUMERO : ', $borde, 0, 'R');
    $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 110, $altoFila,  $solicitud[0]->numero, $borde, 0, $alineacion);
    $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
    $pdf->Cell( 20, $altoFila, 'SALDO S/. : ', $borde, 0, 'R');
    $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 60, $altoFila, "500", $borde, 0, $alineacion);
    $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
    $pdf->Cell( 20, $altoFila, 'FECHA : ', $borde, 0, 'R');
    $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 30, $altoFila, $solicitud[0]->fechaGasto, $borde, 0, $alineacion);
    $pdf->Ln(10);






    $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
    $pdf->Cell( 10, $altoFila, '', $borde, 0, $alineacion);
    $pdf->Cell( 20, $altoFila, 'AREA : ', $borde, 0, 'R');
    $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 40, $altoFila, utf8_decode($solicitud[0]->area), $borde, 0, $alineacion);
    $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
    $pdf->Ln(5);

    $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
    $pdf->Cell( 10, $altoFila, '', $borde, 0, $alineacion);
    $pdf->Cell( 20, $altoFila, 'STRING : ', $borde, 0, 'R');
    $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 30, $altoFila, strtoupper(utf8_decode($solicitud[0]->string)), $borde, 0, $alineacion);
    $pdf->Ln(5);

    $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
    $pdf->Cell( 10, $altoFila, '', $borde, 0, $alineacion);
    $pdf->Cell( 20, $altoFila, 'CATEGORIA : ', $borde, 0, 'R');
    $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 30, $altoFila, strtoupper(utf8_decode($solicitud[0]->categoria)), $borde, 0, $alineacion);
    $pdf->Ln(5);

    $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
    $pdf->Cell( 10, $altoFila, '', $borde, 0, $alineacion);
    $pdf->Cell( 20, $altoFila, 'PROYECTO : ', $borde, 0, 'R');
    $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 30, $altoFila, strtoupper(utf8_decode($solicitud[0]->proyecto)), $borde, 0, $alineacion);
    $pdf->Ln(5);

    $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
    $pdf->Cell( 10, $altoFila, '', $borde, 0, $alineacion);
    $pdf->Cell( 20, $altoFila, 'DETALLE : ', $borde, 0, 'R');
    $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 30, $altoFila, strtoupper(utf8_decode($solicitud[0]->comentario)), $borde, 0, $alineacion);
    $pdf->Ln(10);


    $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
    $pdf->Cell( 10, $altoFila, '', $borde, 0, $alineacion);
    $pdf->Cell( 20, $altoFila, 'PROVEEDOR : ', $borde, 0, 'R');
    $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 30, $altoFila, "EMPRESA", $borde, 0, $alineacion);
    $pdf->Ln(5);

    $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
    $pdf->Cell( 10, $altoFila, '', $borde, 0, $alineacion);
    $pdf->Cell( 20, $altoFila, 'DESCRIPCION', $borde, 0, $alineacion);
    $pdf->Ln(5);


    $pdf->SetFont( 'Arial', '', $tamanoLetra);
    $pdf->Cell( 10, $altoFila, '', $borde, 0, $alineacion);
        $pdf->MultiCell( 250, $altoFila, strtoupper(utf8_decode($solicitud[0]->descripcion)));
    $pdf->Ln(5);

    $borde = 1;

    $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
    $pdf->Cell( 10, $altoFila, '',0, 0, $alineacion);
    $pdf->Cell( 5, $altoFila, 'N', $borde, 0, $alineacion);
    $pdf->Cell( 35, $altoFila, 'AREA', $borde, 0, $alineacion);
    $pdf->Cell( 20, $altoFila, 'STRING', $borde, 0, $alineacion);
    $pdf->Cell( 20, $altoFila, 'CATEGORIA', $borde, 0, $alineacion);
    $pdf->Cell( 35, $altoFila, 'PROYECTO', $borde, 0, $alineacion);
    $pdf->Cell( 80, $altoFila, 'COMENTARIO', $borde, 0, $alineacion);
    $pdf->Cell( 10, $altoFila, 'MES', $borde, 0, $alineacion);
    $pdf->Cell( 15, $altoFila, 'MONTO', $borde, 0, $alineacion);
    $pdf->Cell( 15, $altoFila, 'USUARIO', $borde, 0, $alineacion);
    $pdf->Cell( 20, $altoFila, 'DETALLE', $borde, 0, $alineacion);
    $pdf->Ln($altoFila);

    $pdf->SetFont( 'Arial', '', $tamanoLetra );
    $i = 0;
    foreach ($detalle as $fila) {
        $i++;
        $pdf->Cell( 10, $altoFila, '', 0, 0, $alineacion);
        $pdf->Cell( 5, $altoFila, $i, $borde, 0, $alineacion);
        $pdf->Cell( 35, $altoFila, $fila->area, $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, $fila->codigo_string, $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, $fila->cuenta_categoria, $borde, 0, $alineacion);
        $pdf->Cell( 35, $altoFila, $fila->proyecto, $borde, 0, $alineacion);
        $pdf->Cell( 80, $altoFila, $fila->comentario, $borde, 0, $alineacion);
        $pdf->Cell( 10, $altoFila, $fila->mes, $borde, 0, $alineacion);
        $pdf->Cell( 15, $altoFila, $fila->total, $borde, 0, $alineacion);
        $pdf->Cell( 15, $altoFila, $fila->usuario, $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, $fila->estado, $borde, 0, $alineacion);
        $pdf->Ln($altoFila);
    }
    







  $pdf->Output( "reporteSolicitudOrdenProvision.pdf", "I" );



?>
<?php
    include("plantilla_informe.php");
    error_reporting(0);
    session_start();
    require_once('../php/config.php');
    require_once('../php/services/ServiceReportes.php');
   
    //LLENADO DE DATOS
    $hora = date("h:i:s");
    $fecha = date("Y-m-j");

    /*
    $service = new ServiceReportes();

    $data = $service->getDatosInforme(2);
    $informe = $data[0];

    $resultadoTienda = $service->getListaReporteTienda();
    $numeroTienda = $resultadoTienda[0]->numeroTienda;
    $nombreTienda = $resultadoTienda[0]->nombreTienda;

    $tamanoLetra = 8;


    $borde = 0;
    $alineacion = "L";
    $altoFila = 4;

    $pdf = new PDF( 'P', 'mm', 'A4' );

    //foreach ($areas as $area) {


        $pdf->AddPage();

        $pdf->SetFont( 'Arial', 'B', 10 );
        $pdf->Cell( 40, $altoFila, 'INVENTARIO - PISO DE VENTA', $borde, 0, $alineacion);
        $pdf->Ln(9);

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 40, $altoFila, 'ENCARGADO CLIENTE : ', $borde, 0, $alineacion);
        $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 90, $altoFila, $informe->encargadoCliente, $borde, 0, $alineacion);
        $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
        $pdf->Cell( 30, $altoFila, 'INICIO CONTEO : ', $borde, 0, 'R');
        $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 40, $altoFila, $informe->inicioConteo, $borde, 0, $alineacion);
        $pdf->Ln(5);
        $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
        $pdf->Cell( 40, $altoFila, 'SUPERVISOR IGROUP : ', $borde, 0, $alineacion);
        $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 90, $altoFila, $informe->supervisorIgroup, $borde, 0, $alineacion);
        $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
        $pdf->Cell( 30, $altoFila, 'FIN DE CONTEO : ', $borde, 0, 'R');
        $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 40, $altoFila, $informe->finConteo, $borde, 0, $alineacion);
        $pdf->Ln(5);
        $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
        $pdf->Cell( 40, $altoFila, 'MESA DE CONTROL : ', $borde, 0, $alineacion);
        $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 90, $altoFila, $informe->mesaControl, $borde, 0, $alineacion);
        $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
        $pdf->Cell( 30, $altoFila, 'CIERRE INVENTARIO : ', $borde, 0, 'R');
        $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 40, $altoFila, $informe->cierreInventario, $borde, 0, $alineacion);
        $pdf->Ln(10);

        $pdf->SetFont( 'Arial', 'B', 8);
        $pdf->Cell( 40, $altoFila, utf8_decode('1. LOTIZACION - PISO VENTA'), $borde, 0, $alineacion);
        $pdf->Ln(6);
        $pdf->Cell( 170, $altoFila, utf8_decode('DESCRIPCIÓN'), 1, 0,'C');
        $pdf->Cell( 20, $altoFila, utf8_decode('RESPUESTA'), 1, 0, 'C');
        $pdf->Ln($altoFila);

        $data = $service->getPreguntasCuestionario(2,3);
        $registros = $data;
        $pdf->SetFont( 'Arial', '', $tamanoLetra );

        foreach ($registros as $fila) {

            $i++;
            $x = $pdf->GetX();
            $y = $pdf->GetY();
            $pdf->MultiCell(170,$altoFila,utf8_decode($fila->pregunta),1,'LRB');
            $x += 170;
            $pdf->SetXY($x, $y);
            $pdf->MultiCell(20,$altoFila,utf8_decode($fila->respuesta),1,'C');
            $pdf->Ln($altoFila);

        }

        $pdf->Ln(1);
        $pdf->SetFont( 'Arial', 'B', 8);
        $pdf->Cell( 40, $altoFila, '2. CAPTURA - PISO VENTA', $borde, 0, $alineacion);
        $pdf->Ln(6);
        $pdf->Cell( 170, $altoFila, utf8_decode('DESCRIPCIÓN'), 1, 0,'C');
        $pdf->Cell( 20, $altoFila, utf8_decode('RESPUESTA'), 1, 0, 'C');
        $pdf->Ln($altoFila);

        $data = $service->getPreguntasCuestionario(2,4);
        $registros = $data;
        $pdf->SetFont( 'Arial', '', $tamanoLetra );

        foreach ($registros as $fila) {

            $i++;
            $x = $pdf->GetX();
            $y = $pdf->GetY();
            $pdf->MultiCell(170,$altoFila,utf8_decode($fila->pregunta),1,'LRB');
            $x += 170;
            $pdf->SetXY($x, $y);
            $pdf->MultiCell(20,$altoFila,utf8_decode($fila->respuesta),1,'C');
            $pdf->Ln($altoFila);

        }


        $pdf->Ln(1);
        $pdf->SetFont( 'Arial', 'B', 8);
        $pdf->Cell( 40, $altoFila, 'OBSERVACIONES : ', $borde, 0, $alineacion);
        $pdf->SetFont( 'Arial', '', 8);
        $pdf->Ln(5);
        $pdf->MultiCell( 190, $altoFila, $informe->observaciones,0,'LRB');
        $pdf->Ln(5);


        $pdf->SetFont( 'Arial', 'B', 8);

        $pdf->SetXY(20, 268);
        $pdf->MultiCell(60,$altoFila,"_________________________________",0,'C');
        $pdf->SetXY(20, 272);
        $pdf->MultiCell(60,$altoFila,"ENCARGADO CLIENTE",0,'C');

        $pdf->SetXY(130, 268);
        $pdf->MultiCell(60,$altoFila,"_________________________________",0,'C');
        $pdf->SetXY(130, 272);
        $pdf->MultiCell(60,$altoFila,"SUPERVISOR IGROUP",0,'C');
        */

  $pdf->Output( "reporteProvision.pdf", "I" );



?>